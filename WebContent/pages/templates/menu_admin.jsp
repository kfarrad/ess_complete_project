<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="navmenu">
	<ul class="regroupeMenu">
		<li class="menuTitle">ADMINISTRATION</li>
		<li><a class="lien" href="<c:url value="/holidays"/>">Fixer
				les jours fériés</a></li>
		<li><a class="lien" href="<c:url value="/listFormType"/>">
				Voir les formulaires types</a></li>
		<li><a class="lien" href="<c:url value="/bilanFinancier"/>">
				Voir le bilan financier</a></li>
		<li><a class="lien" href="<c:url value="/ajouterContenu"/>">
				Ajouter une publication</a></li>
		<li><a class="lien" href="<c:url value="/voirContenu"/>">
				Voir les publications</a></li>
	</ul>
	<ul class="regroupeMenu">
		<li class="menuTitle">GESTION SALARIES</li>
		<li><a class="lien" href="<c:url value="/inscription"/>">Créer
				un nouveau utilisateur</a></li>
		<li><a class="lien" href="<c:url value="/userProfilList"/>">Voir
				les utilisateurs existants</a></li>
		<li><a class="lien"
			href="<c:url value="gestionAutoEvaluations"/>"> Gestion
				auto-Evaluation</a></li>
		<li><a class="lien" href="<c:url value="/listCV"/>"> Liste
				des CV</a></li>
	</ul>
	<ul class="regroupeMenu">
		<li class="menuTitle">GESTION CANDIDATS</li>
		<li><a class="lien" href="<c:url value="/creationCV"/>">Créer
				un nouveau CV</a></li>
		<li><a class="lien" href="<c:url value="/listeCandidats"/>">Lister
				les candidatures</a></li>
	</ul>
	<ul class="regroupeMenu">
		<li class="menuTitle">GESTION CLIENTS</li>
		<li><a class="lien" href="<c:url value="/clientCreation"/>">Créer
				un nouveau client</a></li>
		<li><a class="lien" href="<c:url value="/clientList"/>">Voir
				les clients existants</a></li>
		<li><a class="lien" href="<c:url value="/projetList"/>">Voir
				les projets existants</a></li>

	</ul>
</div>
