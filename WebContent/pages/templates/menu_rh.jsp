<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="navmenu">
	<ul>
		<li><a class="lien" href="<c:url value="/contactMail"/>">Contacter
				un salarié</a></li>

	</ul>
	
	<ul class="regroupeMenu">
		<li class="menuTitle">GESTION CANDIDATS</li>
		<li><a class="lien" href="<c:url value="/creationCV"/>">Créer
				un nouveau CV</a></li>
		<li><a class="lien" href="<c:url value="/listeCandidats"/>">Liste des candidats</a></li>
	</ul>
</div>
