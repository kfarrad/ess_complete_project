<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.clinkast.cra.config.appCra" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 "
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="${language}">
<head profile="http://www.w3.org/2005/10/profile">
<title>${title}:: MenuSalarié</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<link rel="icon"
 href="<c:url value="/resources/images/logo_clinkast.png"/>" 
 type="image/png"/>

<link rel="stylesheet"
	href="<c:url value="/resources/css/jquery-te-1.4.0.css"/>" media="all" />

<link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"
	media="all" />
<link rel="stylesheet"
	href="<c:url value="/resources/css/jquery.tablesorter.css"/>"
	media="all" />
<link rel="stylesheet" href="<c:url value="/resources/css/table.css"/>"
	media="all" />
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/msg.css"/>" media="all" />
<link rel="stylesheet"
	href="<c:url value="/resources/css/jquery-ui-1.10.4.custom.css"/>"
	media="all" />
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery-1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery-ui.min.js" />"></script>

<script type="text/javascript"
	src="<c:url value="/resources/javascript/infobulle.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/msg.js" />"></script>
<%-- <script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery-1.7.2.min.js" />"></script> --%>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery.validate.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/orangehrm.validate.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery.tablesorter.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery.mtz.monthpicker.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery-te-1.4.0.min.js" />"></script>
<!-- L'import du plugin NOTIFY pour gerer les notifications dans l'application -->
<script type="text/javascript"
	src="<c:url value="/resources/javascript/notify.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/notify.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery.ui.datepicker-fr.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery.vticker.min.js" />"></script>

<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
</head>

<script type="text/javascript">
	$(document).ready(function() {
		$('#userLogo').click(function(event) {
			event.stopPropagation();
			$("#userLogOut").slideToggle("fast");
		});
		$("#userLogOut").on("click", function(event) {
			event.stopPropagation();
		});
	});

	$(document).on("click", function() {
		$("#userLogOut").hide();
	});
</script>



<body id="${active}">
	<div id="container">
		<div id="header">
			<div class="logo">
				<a href="<c:url value="/acceuil" />"><img
					src="<c:url  value="/resources/images/logo_clinkast.png" />"
					alt="activer" /> </a>
			</div>
			<!-- 	<h1>Clinkast Member Ma"WebContent/pages/templates/header.jsp"nagement</h1> -->
			<div id="user" class="weather">
				<form>
					<select id="language" name="language" onchange="submit()">
						<option value="fr" ${language == 'fr' ? 'selected' : ''}>Français</option>
						<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
					</select>
				</form>
				<p>
					<c:choose>
						<c:when test="${!empty sessionScope.sessionUser}">
							<%-- <c:import url="/pages/templates/welcome.jsp" /> --%>
							<%-- <c:if test="${!empty sessionScope.sessionUser}"> --%>
							<%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>
							<%-- <p class="succes"><fmt:message key="user.bienvenue"/> ${sessionScope.nom}</p> --%>
							<a href="#"><img id="userLogoStatic" alt=""
								src="<c:url value="/resources/images/user.png"/>" /> 
								<c:choose>
									<c:when test="${!empty sessionScope.sessionUser.profil.photoProfil}">
								<img
								id="userLogo" src="<c:url value="/download/${sessionScope.sessionUser.profil.photoProfil}"/>"
								title="${sessionScope.sessionUser.profil.getName()}">
								</c:when>
								<c:otherwise>
									<img
								id="userLogo" src="<c:url value="/resources/images/defaultUser.png"/>"
								title="${sessionScope.sessionUser.profil.getName()}">
								</c:otherwise>
								</c:choose>
							</a>
							<div id="userLogOut">
								<fieldset class="details">
									<legend>${sessionScope.sessionUser.profil.getName()}</legend>
									<!-- <div id="navmenu"> -->
									<!-- <ul class="regroupeMenu">
										<li> -->
									<a class="lien"
										href="<c:url value="/userProfil"><c:param name="idProfil" value="${sessionScope.sessionUser.profil.id}" /></c:url>">Mon
										compte</a>
									<!-- </li> -->
									<!-- <li> -->
									<a class="lien" href="<c:url value="/userUpdate"/>">Changer
										mot de passe</a>
									<!-- </li> -->
									<!-- <li> -->
									<c:if test="${sessionScope.sessionUser.isAdmin()}">
									<a class="lien" href="<c:url value="/substitution"/>">Substitution
									</a>
									</c:if>
									<a class="lien" href="<c:url value="/deconnexion"/>">Déconnexion
									</a>
									<!-- </li> -->
									<!-- </ul> -->
									<!-- </div> -->
								</fieldset>
							</div>
							<%-- </c:if> --%>
							<%-- <a href="<c:url value="/deconnexion" />"><fmt:message
									key="user.deconnexion" /></a> --%>
						</c:when>
						<c:otherwise>
							<a href="<c:url value="/connexion" />"><fmt:message
									key="user.connexion" /></a>
						</c:otherwise>
					</c:choose>
				</p>

			</div>
		</div>
		<div id="navigation">

			<a href="<c:url value="/acceuil" />"><img
				alt="<fmt:message
							key="page.acceuil" />"
				src="<c:url  value="/resources/images/home168.png" />" class="home"></a>

		</div>

		<div id="content">

			<div id="menu">
				<c:if test="${!empty sessionScope.sessionUser}">

					<c:import url="/pages/templates/menu.jsp" />
					<c:if test="${sessionScope.sessionUser.isUser()}">

						<c:import url="/pages/templates/menu_user.jsp" />
					</c:if>

					<c:if test="${sessionScope.sessionUser.isAdmin()}">

						<c:import url="/pages/templates/menu_admin.jsp" />
					</c:if>

					<c:if test="${sessionScope.sessionUser.isRh()}">
						<c:import url="/pages/templates/menu_rh.jsp" />

					</c:if>
					<c:import url="/pages/templates/menu_connexion.jsp" />
					<div class="fichier">
						<a
							href="<c:url value="/resources/Manuel_utilisation_Clinkast_cra_2.0.docx" />">
							<fmt:message key="page.manuel" />
						</a>
					</div>
				</c:if>
			</div>
			<div id="dynamic">
				<h2 class="titre">${title}</h2>