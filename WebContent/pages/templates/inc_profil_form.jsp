<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
$(document).ready(function(){
	
	$( "#dobProfil" ).datepicker({
	    dateFormat: 'dd/mm/yy',
	    defaultDate: "+1w",
	    changeMonth: true,
	    changeYear: true,
	    yearRange : "-65:+0",
	    numberOfMonths: 1
	  });
	  
});
</script>

<label for="nomProfil">Nom <span class="requis">*</span></label>
<input type="text" id="nomProfil" name="nomProfil" required title="Ce champ est requis"
	value="<c:out value="${profil.nom}"/>" size="30" maxlength="30" placeholder="Entrez le nom de l'utilisateur"/>
<span class="erreur">${form.erreurs['nomProfil']}</span>
<br />

<label for="prenomProfil">Prénom <span class="requis">*</span></label>
<input type="text" id="prenomProfil" name="prenomProfil" required
	value="<c:out value="${profil.prenom}"/>" size="30" maxlength="30" placeholder="Entrez le prénom de l'utilisateur" />
<span class="erreur">${form.erreurs['prenomProfil']}</span>
<br />

<label for="adresseProfil">Adresse <span class="requis">*</span></label>
<input type="text" id="adresseProfil" name="adresseProfil" required title="Ce champ est requis"
	value="<c:out value="${profil.adresse}"/>" size="30" maxlength="60" placeholder="Entrez l'adresse de l'utilisateur"/>
<span class="erreur">${form.erreurs['adresseProfil']}</span>
<br />

<label for="telephoneProfil">Numéro de téléphone <span
	class="requis">*</span></label>
<input type="text" id="telephoneProfil" name="telephoneProfil" pattern="[+\d{2}]?\d{9,11}$"
required title="Numéro de téléphone n'est pas valide entre 9 et 11 chiffres"
	value="<c:out value="${profil.telephone}"/>" size="30" maxlength="30" placeholder="Entrez le téléphone de l'utilisateur"/>
<span class="erreur">${form.erreurs['telephoneProfil']}</span>
<br />

<label for="emailProfil">Adresse email<span class="requis">*</span></label>
<input type="email" id="emailProfil" name="emailProfil" required title="Ce champ est requis"
	value="<c:out value="${profil.email}"/>" size="30" maxlength="60" placeholder="Entrez l'émail de l'utilisateur"/>
<span class="erreur">${form.erreurs['emailProfil']}</span>
<br />

<label for="emailProfil">Date de naissance</label>
<input type="text" id="dobProfil" name="dobProfil" size="30"
	value="<c:out value="${profil.dob}"/>" placeholder="Entrez la date de naissance"/>
<span class="erreur">${form.erreurs['dobProfil']}</span>
<br />

<label for="emailProfil">Numéro sécurité sociale</label>
<input type="text" id="numero_snnProfil" name="numero_snnProfil" pattern="\d{15}$"
	title="Le n° de sécurité social doit contenir 15 chiffres"
	value="<c:out value="${profil.numero_ssn}"/>" size="30" maxlength="15" placeholder="Entrez le SNN de l'utilisateur"/>
<span class="erreur">${form.erreurs['numero_snnProfil']}</span>
<br />

<label for="file">Importer la carte vitale</label>
<input type="file" name="carteVitaleFile" /><span class="erreur">${form.erreurs['carte_vitaleProfil']}</span>
<br/>

<label for="file">Importer la pièce d'identité</label>
<input type="file" name="CNIfile" /><span class="erreur">${form.erreurs['cniProfil']}</span>
<br/>

<label for="file">Importer la photo de profil</label>
<input type="file" name="profilFile" /><span class="erreur">${form.erreurs['cniProfil']}</span>
<br/>
