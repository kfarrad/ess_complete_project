<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
#notification {
    border-radius: 100%;
    margin-bottom: 10px;
    margin-top: -7px;
}

#nombreNotif {
       color: white;
    margin-left: 4px;
    margin-top: -24px;
}

#notif {
    display: inline;
    position: absolute;
}
</style>

<div id="navmenu">
	<ul class="regroupeMenu">
		<li class="menuTitle">CRA</li>
		<li><a class="lien" href="<c:url value="/userCalendar" />"> <c:choose>
					<c:when test="${sessionScope.sessionUser.isRh() }">  
    			Consulter jour de travail
   				</c:when>
					<c:otherwise>
    			Valider jour de travail
				</c:otherwise>
				</c:choose>
		</a></li>

		<li><a class="lien" href="<c:url value="/calendarBilan"/>">Bilan</a></li>
		<li><a class="lien" href="<c:url value="/userResume"/>">Rediger
				vos C.V.</a></li>


	</ul>

	<ul class="regroupeMenu">
		<li class="menuTitle">GESTION CONGES</li>
		<li><a class="lien" href="<c:url value="/gestionCongesDemande"/>">Demande
				de congés</a></li>
		<c:choose>
			<c:when test="${ sessionScope.sessionUser.isAdmin() || sessionScope.sessionUser.isRh() }">
				<c:choose>
					<c:when test="${sessionScope.nombreDemandeCongeNonValide != 0 }">
						<li><a class="lien"
							href="<c:url value="/gestionCongesListe"/>">Liste des demandes des congés  <div id="notif"><img id="notification" src="<c:url value="/resources/images/notification_icon.png"/>"> <p id="nombreNotif">${sessionScope.nombreDemandeCongeNonValide}</p></div></a></li>
					</c:when>
					<c:otherwise>
						<li><a class="lien"
							href="<c:url value="/gestionCongesListe"/>">Liste des demandes des congés</a></li>
					</c:otherwise>
				</c:choose>

			</c:when>
			<c:otherwise>
				<li><a class="lien" href="<c:url value="/gestionCongesListe"/>">Liste de congés</a></li>
			</c:otherwise>
		</c:choose>
		<c:if test="${!sessionScope.sessionUser.isUser()}">
			<li><a class="lien" href="<c:url value="/CalendarCongesAll"/>">Bilan
				des congés</a></li>
		</c:if>
	<li>
	<c:choose>
	<c:when test="${ sessionScope.sessionUser.isAdmin() || sessionScope.sessionUser.isRh() }">
				<a class="lien" href="<c:url value="/HistoriqueConges"/>">Historique</a>
				</c:when>
				</c:choose>
	</li>
	
	</ul>
</div>
