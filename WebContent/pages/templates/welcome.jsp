<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setBundle basename="com.clinkast.cra.config.appCra" />
<c:if test="${!empty sessionScope.sessionUser}">
	<%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>
	<p class="suc&ces"><fmt:message key="user.bienvenue"/> ${sessionScope.nom}</p>
</c:if>