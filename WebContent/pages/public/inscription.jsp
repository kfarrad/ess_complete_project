<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Inscription" />
<c:set var="active" value="Inscription" />
<%@ include file="../templates/header.jsp"%>
<form method="post" action="<c:url value="/inscription"/>" enctype="multipart/form-data" >
	<fieldset>
		<p>Vous pouvez vous inscrire via ce formulaire.</p>

		<c:import url="/pages/templates/inc_profil_form.jsp" />

	</fieldset>
	<p class="info">${ form.resultat }</p>
	<input type="submit" value="Inscription" /> <input type="reset"
		value="Remettre à zéro" /> <br />
</form>
<%@ include file="../templates/footer.jsp"%>