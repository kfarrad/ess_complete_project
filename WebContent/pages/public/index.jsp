<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Bienvenue" />
<c:set var="active" value="bienvenue" />
<%@ include file="../templates/header.jsp"%>

<style>
#userBD {
	height: 200px;
	width: 300px;
}

#actualite {
	overflow: hidden;
	border: 3px solid #F2F2F2;
}

</style>

<script type="text/javascript">
	$(function() {
		$('#actualite').vTicker();
	});
</script>

<fieldset>
	<c:choose>
		<c:when test="${sessionScope.sessionUser != null}">
			<p>Bienvenue dans l'outil de gestion des Plannings Clinkast</p>
		</c:when>
		<c:otherwise>
			<p>Bienvenue dans l'outil de gestion des Plannings Clinkast</p>
			<br />
			<p>Veuillez vous connecter en cliquant sur</p>
			<a href="<c:url value="/connexion" />"><fmt:message
					key="user.connexion" /></a>
		</c:otherwise>
	</c:choose>
</fieldset>
<br />
<c:if test="${sessionScope.sessionUser.profil.isBirthDay()}">
	<div id="happyBD">
		<p>Toute l'équipe Clinkast te souhaite un Joyeux Anniversaire !!</p>
		<br /> <img id="userBD"
			src="<c:url value="/resources/images/happyBD1.jpg"/>"
			title="Joyeux anniversaire ${sessionScope.sessionUser.profil.getName()}">
	</div>
</c:if>

<c:if test="${!empty contenus}">
	<fieldset>
	
	<legend> Actualités </legend>
	<div id="actualite">
		<ul>
			<c:forEach items="${contenus}" var="contenu" varStatus="myIndex">
				<li><article>
						<header>
							<h5>${contenu.titre}</h5>
							<p>
								<small>publié: <time pubdate="pubdate">${contenu.dateFormat()}</time></small>
							</p>
						</header>
						<p>${contenu.contenu}</p>
					</article></li>
			</c:forEach>
		</ul>
	</div>
	</fieldset>
</c:if>


<%@ include file="../templates/footer.jsp"%>