<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Page unavailable" />
<c:set var="active" value="Page unavailable" />
<%@ include file="../templates/header.jsp"%>
<h1>Error 500</h1>
<div id="error">
	<p>The server encountered an internal error or misconfiguration and
		was unable to complete your request. We're working on resolving this
		error. Please check back in a bit.</p>
</div>


<%@ include file="../templates/footer.jsp"%>

