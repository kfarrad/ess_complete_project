<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Access Denied" />
<c:set var="active" value="Access Denied" />
<%@ include file="../templates/header.jsp"%>
<fieldset>
	<p>
		<img src="<c:url value="/resources/images/access-denied.png"/>">
	</p>
</fieldset>

<%@ include file="../templates/footer.jsp"%>

