<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Connexion" />
<c:set var="active" value="Connexion" />
<%@ include file="../templates/header.jsp"%>

<form method="post" action="<c:url value="/connexion" />">
	<fieldset>
		<p>Vous pouvez vous connecter via ce formulaire.</p>

		<label for="login">Adresse email <span class="requis">*</span></label>
		<input type="text" id="login" name="login"
			value="<c:out value="${user.login}"/>" size="20" maxlength="60" /> <span
			class="erreur">${form.erreurs['login']}</span> <br /> <label
			for="motdepasse">Mot de passe <span class="requis">*</span></label> <input
			type="password" id="motdepasse" name="motdepasse" value="" size="20"
			maxlength="20" /> <span class="erreur">${form.erreurs['motdepasse']}</span>
		<br /> <input id="signup" type="submit" value="Connexion" class="sansLabel" /> <br />
		<p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
	</fieldset>
</form>
<%@ include file="../templates/footer.jsp"%>