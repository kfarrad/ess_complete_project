<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>AUTO EVALUATION</title>
<link rel="stylesheet" href="<c:url value="/resources/css/table.css"/> " />
<link rel="stylesheet" href="<c:url value="/resources/css/autoEvaluation.css"/> " />
</head>
<body>
	<!-- <div class="container"> -->
	<c:choose>
		<c:when test="${ empty gestionEvaluations }">
			<span> PAS DE FORMULAIRE D'AUTO-EVALUATION</span>
		</c:when>
		<c:otherwise>
			<c:set scope="session" var="gestionEvaluations" value="${gestionEvaluations}"></c:set>
			<div class="header" id="pageHeader">
				<table style="border:0;">
					<tr>
						<td class="logo"><img src="<c:url  value="/resources/images/logo_clinkast.png" />" alt="CLINKAST" /></td>
						<td class="entete">
							<span class="head">AUTO-EVALUATION DE LA PROGRESSION</span>
							<br/>
							<b class="subtitle">Période du ${gestionEvaluations.dates.datesPeriodes }</b>
						</td>
					</tr>					
				</table>
			</div>
			<div id=""><p><b class="subtitle">${gestionEvaluations.user.profil.prenom} ${gestionEvaluations.user.profil.nom}</b>
							<br/>
							<i>Consultant SI/Manager</i>
						</p>
			</div>
			<div class="format">

				<div id="activite">
					<span class="titre"> ACTIVITÉ À LA PERIODE ECOULÉE </span>
					
					<p><span class="subtitle">1.1. &nbsp;Synthèse de l’activité durant cette periode (Pour chaque mission quelles sont mes réalisations ?)</span></p>
						<table class="table">
							<thead>
								<tr>
									<th>Période</th>
									<th>Missions ou réalisation</th>
									<th>Responsable</th>
									<th>Entretien de mission</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
								<%--Si la variable gestionEvaluation est null, affiche le message par defaut --%>
								<c:when test="${empty gestionEvaluations.autoEvaluations.systemesActiviteses}">
									<tr>
										<td colspan="5" class="name">Aucune donnée</td>
									</tr>
								</c:when>
								<%-- Sinon, affichage du tableau --%>
								<c:otherwise>
									<%-- Parcours le tableau et affiche la liste des activités  --%>
									<c:set scope="session" var="systemesActiviteses" value="${gestionEvaluations.autoEvaluations.systemesActiviteses}"></c:set>
									<c:forEach items="${systemesActiviteses}" var="mapActivites" varStatus="boucle">
									<tr class="data">
										<td><c:out value="${mapActivites.systemesActivitesPeriodeDe} au ${mapActivites.systemesActivitesPeriodeAu}" /></td>
										<td><c:out value="${mapActivites.systemesActivitesMissions }" /></td>
										<td><c:out value="${mapActivites.systemesActivitesResponsable }" /></td>
										<td><c:out value="${mapActivites.systemesActivitesEntretien == true ? 'Oui' : 'Non' }" /></td>
									</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
							</tbody>
						</table>
					
					<p><span class="subtitle">1.2. &nbsp; Bilan sur l’apport de moyens pendant l’année écoulée (Formation, accompagnement, suivi de mission</span>
						<br/><span class="puce">- Formations suivies</span></p>						
						<table class="table">
							<thead>
								<tr>
									<th>Intitule de la formation</th>
									<th>Commentaire sur l'objectif</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty gestionEvaluations.autoEvaluations.formationsSuivieses}">
										<tr>
											<td colspan="3" class="name">Aucune donnée</td>
										</tr>
									</c:when>
									<c:otherwise>
										<%-- Parcours le tableau et affiche la liste des activités  --%>
									
									<c:forEach items="${gestionEvaluations.autoEvaluations.formationsSuivieses}" var="mapFormationSuivie" varStatus="boucle">
									<tr class="dataFms">
										<td><c:out value="${mapFormationSuivie.formationsSuiviesIntitule}" /></td>
										<td><c:out value="${mapFormationSuivie.formationsSuiviesCommentaires }" /></td>
									</tr>
									</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
						<br/><span class="puce">- Autres moyens</span><br/>
						<div class="box">
							<p>${gestionEvaluations.autoEvaluations.autoEvaluationsAutresMoyens}</p>
						</div>
						
						<p><span class="subtitle">1.3.&nbsp; Atteinte de mes objectifs (Pour chaque objectif quel niveau de résultat j’estime avoir atteint ?)</span>
						</p>	
						<table class="table">
						<thead>
							<tr>
								<th>Objectif initial</th>
								<th>Estimation</th>
							</tr>
						</thead>
						<tbody>
						<c:choose>
							<%--Si la variable gestionEvaluation est null, affiche le message par defaut --%>
							<c:when test="${empty gestionEvaluations.autoEvaluations.objectifses}">
								<tr>
									<td colspan="5" class="name">Aucune donnée</td>
								</tr>
							</c:when>
							<%-- Sinon, affichage du tableau --%>
							<c:otherwise>
								<%-- Parcours le tableau et affiche la liste des activités  --%>
								
								<c:forEach items="${gestionEvaluations.autoEvaluations.objectifses}" var="mapObjectifs" varStatus="boucle">
								<tr>
									<td><c:out value="${mapObjectifs.objectifsLibelle}" /></td>
									<td><c:out value="${mapObjectifs.objectifsEstimation}" /></td>						
								</tr>
								</c:forEach>
								</c:otherwise>
						</c:choose>
						</tbody>
					</table>					
					
					<p class="puce">- Eléments de contexte significatifs</p>
					<div class="box">
						<p>${gestionEvaluations.autoEvaluations.autoEvaluationsElementsSignifications}</p>
					</div>	
					<p class="puce">- Par rapport à ce que Clinkast attendait de moi, quel bilan fais-je de cette période ?</p>
					<div class="box">
						<p>${gestionEvaluations.autoEvaluations.autoEvaluationsBilanClinkastAttentes}</p>
					</div>
					<p class="puce">- Par rapport à ce que j’attendais de Clinkast, quel bilan fais-je de cette période ?</p>
					<div class="box">
						<p>${gestionEvaluations.autoEvaluations.autoEvaluationsBilanMesAttentes}</p>
					</div>		
				</div>
				<div id="exigence">
					<span class="titre"> EXIGENCES DE LA FONCTION </span>					
					<p class="subtitle">2.1. &nbsp;Exigences de la fonction : Par rapport au Guide de Compétences sur quels critères ai-je évolué,progressé ?</p>
					<table class="table">
						<thead>
							<tr><th colspan="3">Metier du consultant</th></tr>
							<tr>
								<th style=" width:35%;">Compétences attendues</th>
								<th style=" width:30%;">Niveau</th>
								<th style=" width:35%;">Commentaire ou exemple</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
								<c:if test="${rapport.competences.competencesCategorie == 'Metier du consultant'}">									
									<tr>									
										<td style=" width:35%;text-align: left">${rapport.competences.competencesLibelle}</td>
										<td style=" width:30%;">${rapport.rapportsNiveau}</td>
										<td style=" width:35%;">${rapport.rapportsCommentaire}</td>
									</tr>							
								</c:if>								
							</c:forEach>
						</tbody>
					</table>
					
					<table class="table">
						<thead>
							<tr><th colspan="3">Client</th></tr>
							<tr>
								<th style=" width:35%;">Compétences attendues</th>
								<th style=" width:30%;">Niveau</th>
								<th style=" width:35%;">Commentaire ou exemple</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
								<c:if test="${rapport.competences.competencesCategorie == 'Client'}">									
									<tr>									
										<td style="text-align: left; width:35%">${rapport.competences.competencesLibelle}</td>
										<td style="width:30%">${rapport.rapportsNiveau}</td>
										<td style=" width:35%;">${rapport.rapportsCommentaire}</td>
									</tr>							
								</c:if>								
							</c:forEach>
						</tbody>
					</table>
					
					<table class="table">
						<thead>
							<tr><th colspan="3">Engagement et Qualités Individuelles</th></tr>
							<tr>
								<th style=" width:35%;">Compétences attendues</th>
								<th style=" width:30%;">Niveau</th>
								<th style=" width:35%;">Commentaire ou exemple</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
								<c:if test="${rapport.competences.competencesCategorie == 'Engagement et Qualités Individuelles'}">									
									<tr>									
										<td style=" width:35%;text-align: left">${rapport.competences.competencesLibelle}</td>
										<td style=" width:30%;">${rapport.rapportsNiveau}</td>
										<td style=" width:35%;">${rapport.rapportsCommentaire}</td>
									</tr>							
								</c:if>								
							</c:forEach>
						</tbody>
					</table>
					
					<table class="table">
						<thead>
							<tr><th colspan="3">Développement des Hommes et Management</th></tr>
							<tr>
								<th style=" width:35%;">Compétences attendues</th>
								<th style=" width:30%;">Niveau</th>
								<th style=" width:35%;">Commentaire ou exemple</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
								<c:if test="${rapport.competences.competencesCategorie == 'Développement des Hommes et Management'}">									
									<tr>									
										<td style=" width:35%;text-align: left">${rapport.competences.competencesLibelle}</td>
										<td style=" width:30%;">${rapport.rapportsNiveau}</td>
										<td style=" width:35%;">${rapport.rapportsCommentaire}</td>
									</tr>							
								</c:if>								
							</c:forEach>
						</tbody>
					</table>
					
					<p class="puce">- Eléments de contexte significatifs</p>
					<table class="table">
						<thead>
							<tr>
								<th>Quels sont mes atouts pour progresser ?</th>
								<th>Sur quels axes j’estime devoir progresser ?</th>
							</tr>
						</thead>
						<tbody>
							<tr>									
								<td style="width:50%;">${gestionEvaluations.autoEvaluations.autoEvaluationsAtoutPourProgresser}</td>
								<td style="width:50%;">${gestionEvaluations.autoEvaluations.autoEvaluationsAxesAprogresser}</td>
							</tr>							
						</tbody>
					</table>
				</div>
				<div id="evolution">
					<span class="titre"> EVOLUTION </span>					
					<p><span class="subtitle">3.1. &nbsp;Quels sont mes souhaits d’évolution à court/moyen terme (2/3 ans) ? (Orientations métier, type de mission, sujets abordés, spécialisation</span></p>
					<div class="box">
						<p>${gestionEvaluations.autoEvaluations.autoEvaluationsSouhaitsCourtMoyen}</p>
					</div>
					<p><span class="subtitle">3.2. &nbsp;Actions de formation demandées</span></p>
					<table class="table">
						<thead>
							<tr>
								<th>Intitule de la formation</th>
								<th>Objectifs visés</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty gestionEvaluations.autoEvaluations.formationsDemandes}">
									<tr>
										<td colspan="3" class="name">Aucune donnée</td>
									</tr>
								</c:when>
								<c:otherwise>
									<%-- Parcours le tableau et affiche la liste des activités  --%>
								
								<c:forEach items="${gestionEvaluations.autoEvaluations.formationsDemandes}" var="mapFormationDemande" varStatus="boucle">
								<tr class="dataFdd">
									<td><c:out value="${mapFormationDemande.formationsDemandesLibelle}" /></td>
									<td><c:out value="${mapFormationDemande.formationsDemandesObjectifs }" /></td>
								</tr>
								</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<p><span class="subtitle">3.3. &nbsp;Quelles satisfactions je retire de mon métier de consultant? Qu’est-ce qui me motive ? Quels sont les éléments auxquels j’attache de l’importance ?</span></p>
					<div class="box">
						<p>${gestionEvaluations.autoEvaluations.autoEvaluationsElemetsImportant}</p>
					</div>
				</div>
				
				<div id="resume">
					<span class="titre"> RESUME </span>	
					<div class="box">
						<p>${gestionEvaluations.autoEvaluations.autoEvaluationsResume}</p>
					</div>
				</div>
			</div>	
			<div class="footer" id="pageFooter">
				<span class="titre"> CLINKAST </span><br /> <span>32 rue de la République 92190 MEUDON</span><br /> <span>Votre contact :<b>+33 1 46 31 44 25</b></span>
			
				<div class="footerLink">
				<span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><a
					class="lien" href="www.clinkast.fr">www.clinkast.fr</a>
			</div>
		</div>	
				
		</c:otherwise>
	</c:choose>
</body>
</html>