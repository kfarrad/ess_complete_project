<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="title" value="Formulaire de contact" />
<c:set var="active" value="Contact form" />
<%@ include file="../../templates/header.jsp"%>

<script type="text/javascript">
function checkfile(sender){
    var invalidExts = new Array(".ade", ".adp", ".bat", ".chm", ".cmd", ".com", ".cpl", ".exe", ".hta", ".ins", ".isp", ".jse",
    		".lib", ".lnk", ".mde", ".msc", ".msp", ".mst", ".pif", ".scr", ".sct", ".shb", ".sys", ".vb", ".vbe", ".vbs"
    		, ".vxd", ".wsc", ".wsf", ".wsh");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    //For Indexof() -IE < 9
    if (!Array.prototype.indexOf){
        Array.prototype.indexOf = function (elt /*, from*/)
        {
            var len = this.length >>> 0;
            var from = Number(arguments[1]) || 0;
            from = (from < 0) ? Math.ceil(from) : Math.floor(from);
            if (from < 0)
                from += len;
            for (; from < len; from++){
                if (from in this && this[from] === elt)
                    return from;
                }
            return -1;
        };
    }

    if (invalidExts.indexOf(fileExt) >= 0)
    {
    	alert("Invalid file selected,files of this type cannot be sent.");
        sender.value = '';
        return false;
    }
    else
    {
        return true;

    }
}

</script>
<table>
	<tr class="odd">
		<td>
			<form method="post" action="<c:url value="/contactMail"></c:url>">
				<table>
					<h2>Mail API</h2>
					<tr>
						<td><b>To:</b></td>
						<td><select id="to" name="to" onchange="gotoCalendar();">
								<option value="">--</option>
								<c:forEach items="${listeUtilisateurs }" var="user"
									varStatus="boucle">
									<option value="${user.login}"
										<c:if test="${user.login==login}"> selected="selected" </c:if>><c:out
											value="${user.login}" /></option>
								</c:forEach>
						</select> <span class="erreur">${form.erreurs['to']}</span></td>
					</tr>
					<tr>
						<td><b>Subject:</b></td>
						<td><input type="text" name="subject" size="70"> <span
							class="erreur">${form.erreurs['subject']}</span></td>
					</tr>
					<tr>
						<td><b>Description:</b></td>
						<td><textarea name="body" type="text" cols="90" rows="15"
								size=100>
  </textarea> <span class="erreur">${form.erreurs['body']}</span></td>
					</tr>
					<tr>
						<td><b>Joindre des fichiers:</b></td>
						<td id="filestojoint"><input type="file" name="attachment"
							id="attachment" size="30" onchange="checkfile(this);" /><br>
						</td>
					</tr>
					<tr>
						<td><p>
								<input type="submit" value="Send Mail" name="sendMail"></td>
					</tr>
				</table>

			</form>
		</td>
	</tr>
</table>
<%@ include file="../../templates/footer.jsp"%>