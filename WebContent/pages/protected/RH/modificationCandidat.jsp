<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Modification du CV" />
<c:set var="active" value="Create resume" />
<%@ include file="../../templates/header.jsp"%>

<script type="text/javascript">
	var counterD = 1;
	var counterS = 1;
	var result = "";
	function GetDynamicDiplome(value) {
		return '<label for="diplomeCandidat">Diplôme </label><input type="text" id="diplomeCandidat" name="diplomeCandidat" pattern="[(\\)\\-_çéèàêôùâa-zA-Z0-9\\s]+" title="Pas de caractères speciaux" placeholder="Entrez le diplôme du candidat" value="'
				+ value
				+ '" size="30" maxlength="60" /> <span class="erreur">${form.erreurs["diplomeCandidat"]}</span>'
				+ '<a class="DeleteDiplome" onclick = "RemoveDiplome(this)" ><img class="DeleteDiplomeImg" alt="Supprimer le diplôme" src="resources/images/icon_remove.png"> </a>'
	}

	function GetDynamicSpecialite(value) {
		return '<label for="specialiteCandidat">Spécialité </label><input type="text" id="specialiteCandidat" name="specialiteCandidat" pattern="[(\\)\\-_çéèàêôùâa-zA-Z0-9\\s]+" title="Pas de caractères speciaux" placeholder="Entrez la spécialité du candidat" value="'
				+ value
				+ '" size="30" maxlength="60" /> <span class="erreur">${form.erreurs["specialiteCandidat"]}</span>'
				+ '<a class="DeleteSpecialite" onclick = "RemoveSpecialite(this)" ><img class="DeleteSpecialiteImg" alt="Supprimer la spécialité" src="resources/images/icon_remove.png"> </a>'
	}
	function AddDiplome(value) {
		if (counterD < 5) {
			counterD++;
			var div = document.createElement('DIV');
			div.innerHTML = GetDynamicDiplome(value);
			document.getElementById("DiplomeGroup").appendChild(div);
		}
	}

	function RemoveDiplome(div) {
		document.getElementById("DiplomeGroup").removeChild(div.parentNode);
		counterD--;
	}

	function AddSpecialite(value) {
		if (counterS < 10) {
			counterS++;
			var div = document.createElement('DIV');
			div.innerHTML = GetDynamicSpecialite(value);
			document.getElementById("SpecialiteGroup").appendChild(div);
		}
	}

	function RemoveSpecialite(div) {
		document.getElementById("SpecialiteGroup").removeChild(div.parentNode);
		counterS--;
	}

	function validateForm() {
		var diplomes = "";
		var specialites = "";
		for (var i = 0; i < counterD; i++) {
			diplomes = diplomes
					+ document.getElementsByName('diplomeCandidat')[i].value
					+ ';';
		}
		var diplome = document.createElement('DIV');
		diplome.innerHTML = '<input name="diplomes" type="hidden" value="'+diplomes+'">';
		document.getElementById("results").appendChild(diplome);

		for (var i = 0; i < counterS; i++) {
			specialites = specialites
					+ document.getElementsByName('specialiteCandidat')[i].value
					+ ';';
		}
		var specialite = document.createElement('DIV');
		specialite.innerHTML = '<input name="specialites" type="hidden" value="'+specialites+'">';
		document.getElementById("results").appendChild(specialite);

	}
</script>

<form id="candidatForm" method="post"
	action="<c:url value="/updateCandidature"/>"
	enctype="multipart/form-data" onsubmit="return validateForm()">
	<fieldset>

		<label for="nomCandidat">Nom <span class="requis">*</span></label> <input
			type="text" id="nomProfil" name="nomCandidat" required
			value="<c:out value="${candidat.nom}"/>" size="30" maxlength="30"
			pattern="[-_çéèàêôùâa-zA-Z0-9\s]+" title="Pas de caractères speciaux"
			placeholder="Entrez le nom du candidat" /> <span class="erreur">${form.erreurs['nomCandidat']}</span>
		<br /> <label for="prenomCandidat">Prénom <span
			class="requis">*</span></label> <input type="text" id="prenomProfil"
			name="prenomCandidat" required
			value="<c:out value="${candidat.prenom}"/>" size="30" maxlength="30"
			pattern="[-_çéèàêôùâa-zA-Z0-9\s]+" title="Pas de caractères speciaux"
			placeholder="Entrez le prénom du candidat" /> <span class="erreur">${form.erreurs['prenomCandidat']}</span>
		<br /> <label for="telephoneCandidat">Numéro de téléphone <span
			class="requis">*</span></label> <input type="tel"
			pattern="[+\d{2}]?\d{9,11}$" id="telephoneProfil"
			name="telephoneCandidat"
			value="<c:out value="${candidat.telephone}"/>" size="30"
			maxlength="30" required
			title="Numéro de téléphone n'est pas valide entre 9 et 11 chiffres"
			placeholder="Entrez le téléphone du candidat" /> <span class="erreur"
			id="erreurTelephone">${form.erreurs['telephoneCandidat']}</span> <br />

		<label for="emailCandidat">Adresse email <span class="requis">*</span></label>
		<input type="email" id="emailProfil" name="emailCandidat"
			value="<c:out value="${candidat.mail}"/>" size="30" maxlength="60"
			required placeholder="Entrez l'émail du candidat" /> <span
			class="erreur">${form.erreurs['emailCandidat']}</span> <br />

		<div class='Diplome' id="Diplome">
			<label for="diplomeCandidat">Diplôme <span class="requis">*</span></label>
			<input type="text" id="diplomeCandidat" name="diplomeCandidat"
				pattern="[-_çéèàêôùâa-zA-Z0-9\s]+"
				title="Pas de caractères speciaux"
				placeholder="Entrez le diplôme du candidat"
				value="<c:out value="${diplomes[0]}"/>" size="30" maxlength="60" />
			<span class="erreur">${form.erreurs['diplomeCandidat']}</span> <a
				class='AddDiplomee' onclick="AddDiplome('')"> <img
				id="AddDiplomeimage" class='AddDiplomeImg' alt="Ajouter un diplôme"
				src="resources/images/icon_add.png">
			</a>
		</div>
		<div id="DiplomeGroup">
			<c:forEach items="${diplomes}" var="diplome" varStatus="loop">
				<c:if test="${loop.index != 0}">
					<script>
						AddDiplome("${diplome}");
					</script>
				</c:if>
			</c:forEach>
		</div>



		<div class='Specialite'>
			<label for="specialiteCandidat">Spécialité <span
				class="requis">*</span></label> <input type="text" id="specialiteCandidat"
				name="specialiteCandidat" pattern="[-_çéèàêôùâa-zA-Z0-9\s]+"
				title="Pas de caractères speciaux"
				placeholder="Entrez la spécialité du candidat"
				value="<c:out value="${specialites[0]}"/>" size="30" maxlength="60" />
			<span class="erreur">${form.erreurs['specialiteCandidat']}</span> <a
				class='AddSpecialite' onclick="AddSpecialite('')"> <img
				id="AddSpecialitee" class='AddSpecialiteImg'
				alt="Ajouter une spécialité" src="resources/images/icon_add.png">
			</a>
		</div>
		<div id="SpecialiteGroup">
			<c:forEach items="${specialites}" var="specialite" varStatus="loop">
				<c:if test="${loop.index != 0}">
					<script>
						AddSpecialite("${specialite}");
					</script>
				</c:if>
			</c:forEach>
		</div>
		<div id="results"></div>

		<div id="commentaireCandidat">
			<label for="commentaireCandidat">Commentaires: </label>
			<textarea rows="3" cols="50" name="commentaireCandidat"
				form="candidatForm" id="specialiteCandidat"
				placeholder="Entrez un commentaire">
</textarea>
			<span class="erreur">${form.erreurs['commentaireCandidat']}</span>
		</div>

		<label for="file">Importer le CV <span class="requis">*</span></label>
		<input type="file" name="file"
			value="<c:out value="${candidat.pathCV}"/>" /><span class="erreur">${form.erreurs['CVCandidat']}</span>
		<c:if test="${ ! empty candidat.pathCV}">
			<a href="<c:url value="/download/${candidat.getPathCV()}"/>"
				target="_blank" title="${candidat.getNomCV()}"><img
				src="<c:url value="/resources/images/cv_icon.png"/>"
				alt="<c:out value="${candidat.getNomCV()}"></c:out>" /></a>
		</c:if>
		<br> <input type="submit" value="Modifier la candidature"
			onClick="" />

	</fieldset>
	<p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
</form>
<%@ include file="../../templates/footer.jsp"%>