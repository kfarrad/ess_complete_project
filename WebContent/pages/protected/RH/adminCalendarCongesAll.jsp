<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<c:set var="title" value="Bilan de ce mois" />
<c:set var="active" value="bilan" />
<%@ include file="../../templates/header.jsp"%>
	<%
		UserDaoImpl utilisateurDao =(UserDaoImpl)(request.getAttribute("utilisateurDao"));
	
		List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
		List<Utilisateur> utilisateurs =utilisateurDao.lister();
		int w=0;
		
		// listons uniquement les utilisateurs de role user
		if (utilisateurs !=null) {
			for(Utilisateur elt : utilisateurs){
			 if(elt.isUser() && elt.isActif()) listeUtilisateurs.add(w++,elt);
			}
		}
		
		
		Calendar ca = new GregorianCalendar();
		int iTYear = ca.get(Calendar.YEAR);
		int iTMonth = ca.get(Calendar.MONTH);
		int iMonth = -1;
		int iYear = -1;



		try {
			iYear = (Integer) request.getAttribute("annee");
			iMonth = (Integer) request.getAttribute("mois");

		} catch (Exception e) {

			e.printStackTrace();
		}
		if (iYear == -1) {
			iYear = iTYear;

		}
		if (iMonth == -1) {
			iMonth = iTMonth;
		}

		Integer annee = (Integer) iYear;
		Integer mois = (Integer) iMonth;

		//Récupération de la liste conges et parcours des jours pour chaque congé
	%>
	

<script type="text/javascript">
	// variable globale
	var id = 1;
	

$(document).ready(function() {
		
		$("#dynamic").scroll(function(){
			  $("#scrolableT").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		
			});
		
});
</script>



	<form name="frm" method="get"
		action="<c:url value="/calendarConges"></c:url>">
		<input type="hidden" name="isvalid" value="0">
		<div id="divSelect" class="TabDyn">
			<table class="table" id="selection">
				<tbody>
					<tr class="odd">
						<td>Year</td>
						<td><select id="annee" name="annee" onchange="goTo()">
								<%
									// start year and end year in combo box to change year in calendar
									for (int iy = iTYear - 20; iy <= iTYear + 20; iy++) {
										if (iy == iYear) {
								%>
								<option value="<%=iy%>" selected="selected"><%=iy%></option>
								<%
									} else {
								%>
								<option value="<%=iy%>"><%=iy%></option>
								<%
									}
									}
								%>
						</select></td>
						<td align="center"><h3>
								<%
									Calendar calend = Calendar.getInstance();
									calend.set(Calendar.YEAR, 2008);
									calend.set(Calendar.MONTH, iMonth);
									calend.set(Calendar.DAY_OF_MONTH, 01);
								%>
								<%=new SimpleDateFormat("MMMM").format(calend.getTime())%>
								<%=iYear%></h3></td>

						<td>Month</td>
						<td><select id="mois" name="mois" onchange="goTo()">
								<%
									// print month in combo box to change month in calendar
									for (int im = 0; im <= 11; im++) {
										if (im == iMonth) {
											calend.set(Calendar.MONTH, im);
								%>
								<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend.getTime())%></option>
								<%
									} else {
											calend.set(Calendar.MONTH, im);
								%>
								<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(calend.getTime())%></option>
								<%
									}
									}
								%>
						</select></td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
	
	<div class="TabDyn ">
		<div id="scrolableT">
	<h1 style="font-family: Verdana, Arial; font-weight: normal">Calendrier
		du mois</h1>
		
		
	<script type="text/javascript">
		var d = new Date();
		var dm = d.getMonth() + 1;
		var dan = d.getYear();
		if (dan < 999)
			dan += 1900;

		mois = <%=mois%>;
		an = <%=annee%>;
		nom_mois = new Array("Janvier", "F&eacute;vrier", "Mars", "Avril",
				"Mai", "Juin", "Juillet", "Ao&ucirc;t", "Septembre",
				"Octobre", "Novembre", "D&eacute;cembre");
		jour = new Array("Lu", "Ma", "Me", "Je", "Ve", "Sa", "Di");

		var police_entete = "Verdana,Arial"; /* police entÃªte de calendrier */
		var taille_pol_entete = 1; /* taille de police 1-7 entÃªte de calendrier */
		var couleur_pol_entete = "#000000"; /* couleur de police entÃªte de calendrier */
		var arrplan_entete = "#FFFFFF"; /* couleur d'arriÃ¨re plan entÃªte de calendrier */
		var police_jours = "Verdana,Arial"; /* police affichage des jours */
		var taille_pol_jours = 1; /* taille de police 1-7 affichage des jours */
		var coul_pol_jours = "#000000"; /* couleur de police affichage des jours */
		var arrplan_jours = "#D0F0F0"; /* couleur d'arriÃ¨re plan affichage des jours */
		var couleur_cejour = "#FFFF00"; /* couleur d'arriÃ¨re plan pour aujourd'hui */
		var couleur_weekend = "7B8E8C"

		var maintenant = new Date(); 
		var ce_mois = maintenant.getMonth();
		var cette_annee = maintenant.getYear();
		if (cette_annee < 999)
			cette_annee += 1900;
		var ce_jour = maintenant.getDate();
		var temps = new Date(an, mois, 1);
		
		var firstDayOfWeek = (new Date(an,mois,0)).getDay();

		
		console.log("firstDayOfWeek :"+firstDayOfWeek);

		document
				.write('<table align="center" border="3" cellpadding="1" cellspacing="1">');
		var entete_mois = nom_mois[mois] + " " + an;
		inscrit_entete(entete_mois,firstDayOfWeek,arrplan_entete, couleur_pol_entete,
				taille_pol_entete, police_entete);
		//document.write("<\/table>");
		

		function inscrit_entete(titre_mois,f,couleurAP, couleurpolice,
				taillepolice, police) {
			document.write("<tr>");
			document
					.write('<td align="center" colspan="32" valign="middle" bgcolor="'+couleurAP+'">');
			document
					.write('<font size="'+taillepolice+'" color="'+couleurpolice+'" face="'+police+'"><b>');
			document.write(titre_mois);
			document.write("<\/b><\/font><\/td><\/tr>");
			document.write("<tr>");
			
			document.write("<td>");
			
			document.write('<b>Utilisateurs</b>');
			document.write("<\/td>");
			   
		    var finTabl = (new Date(an,mois+1,0)).getDate();
		    console.log(" Valeur f: "+ f);
		    console.log(" finTabl : "+ finTabl);
		
			for (var i = f; i <f+finTabl; i++) {
				
				var k = 0;
				if(i<7) k=i;
				else k=i%7;
			
					inscrit_cellule(jour[k], couleurAP, couleurpolice,
							taillepolice, police);
							}
			document.write("<\/tr>");
		}

		function inscrit_cellule(contenu, couleurAP, couleurpolice,
				taillepolice, police) {
			document
					.write('<td align="center" valign="middle" bgcolor="'+couleurAP+'">');
			document
					.write('<font size="'+taillepolice+'" color="'+couleurpolice+'" face="'+police+'"><b>');
			document.write(contenu);
			document.write("<\/b><\/font><\/td>");
		}
		
		function JoursFeries (an){
			var JourAn = new Date(an, "00", "01")
			var FeteTravail = new Date(an, "04", "01")
			var Victoire1945 = new Date(an, "04", "08")
			var FeteNationale = new Date(an,"06", "14")
			var Assomption = new Date(an, "07", "15")
			var Toussaint = new Date(an, "10", "01")
			var Armistice = new Date(an, "10", "11")
			var Noel = new Date(an, "11", "25")
			var SaintEtienne = new Date(an, "11", "26")
			
			var G = an%19
			var C = Math.floor(an/100)
			var H = (C - Math.floor(C/4) - Math.floor((8*C+13)/25) + 19*G + 15)%30
			var I = H - Math.floor(H/28)*(1 - Math.floor(H/28)*Math.floor(29/(H + 1))*Math.floor((21 - G)/11))
			var J = (an*1 + Math.floor(an/4) + I + 2 - C + Math.floor(C/4))%7
			var L = I - J
			var MoisPaques = 3 + Math.floor((L + 40)/44)
			var JourPaques = L + 28 - 31*Math.floor(MoisPaques/4)
			var Paques = new Date(an, MoisPaques-1, JourPaques)
			var VendrediSaint = new Date(an, MoisPaques-1, JourPaques-2)
			var LundiPaques = new Date(an, MoisPaques-1, JourPaques+1)
			var Ascension = new Date(an, MoisPaques-1, JourPaques+39)
			var Pentecote = new Date(an, MoisPaques-1, JourPaques+49)
			var LundiPentecote = new Date(an, MoisPaques-1, JourPaques+50)
			
			return new Array(JourAn, VendrediSaint, Paques, LundiPaques, FeteTravail, Victoire1945, Ascension, Pentecote, LundiPentecote, FeteNationale, Assomption, Toussaint, Armistice, Noel, SaintEtienne)
		}

	</script>
	
<%
if (listeUtilisateurs != null) {
	if (listeUtilisateurs.size() != 0) {
		for (Utilisateur userElt : listeUtilisateurs) {
			UserProfil profil = userElt.getProfil();
			%>

<!-- -- Debut include -->


	<%
		 Long idUser=userElt.getId(); 
	CongesDaoImpl congesDao = (CongesDaoImpl) (request.getAttribute("congesDao"));
	List<Conges> congesList = congesDao.listerConges(idUser,iMonth, iYear);
	


		String couleur = new String();
		String couleur_RTT = "#00FF00";
		String couleur_CP = "#87CEEB";
		String couleur_CSS = "#FFA500";
		String couleur_AM = "#9932CC";
		couleur = "#D0F0F0";

		//Récupération de la liste conges et parcours des jours pour chaque congé
	%>
	<script type="text/javascript">
 tableauCouleurJours = new Array(30);
 <%List<String> tmpTabCouleur = new ArrayList<String>();
			for (int i = 0; i < 31; i++) {
				tmpTabCouleur.add(i, "#D0F0F0");
			}
			
		if (congesList != null) {
			for (Conges c : congesList) {
				List<Float> jours = c.getJours();
				Long i = c.getId_motif();
				int k = 0;
				for (Float f : jours) {
					if (f.equals(1f)) {
						if (i.equals(5l)) {
							//RTT
							couleur = couleur_RTT;
							tmpTabCouleur.set(k, couleur_RTT);
						}
						if (i.equals(6l)) {
							// CSS
							couleur = couleur_CSS;
							tmpTabCouleur.set(k, couleur_CSS);
						}
						if (i.equals(7l)) {
							//CP
							couleur = couleur_CP;
							tmpTabCouleur.set(k, couleur_CP);
						}
						if (i.equals(18l)) {
							//Arret maladie
							couleur = couleur_AM;
							tmpTabCouleur.set(k, couleur_AM);
						}
					}
					k++;
				}
			}
		}

			for (int i = 0; i < tmpTabCouleur.size(); i++) {%>
	 tableauCouleurJours[<%=i%>] = "<%=tmpTabCouleur.get(i)%>";
	<%}%>
		
	</script>

	<script type="text/javascript">
		var d = new Date();
		var dm = d.getMonth() + 1;
		var dan = d.getYear();
		if (dan < 999)
			dan += 1900;
		calendrier(dm, dan);

		function calendrier(mois, an) {
			mois = <%=mois%>;
			an = <%=annee%>;

			var maintenant = new Date(); 
			var ce_mois = maintenant.getMonth();
			var cette_annee = maintenant.getYear();
			if (cette_annee < 999)
				cette_annee += 1900;
			var ce_jour = maintenant.getDate();
			var temps = new Date(an, mois, 1);
			var firstDayOfWeek = (new Date(an,mois,0)).getDay();

			var finTab = (new Date(an,mois+1,0)).getDate();

			var nombre_jours = 1;
				document.write("<tr>");
				
				
				document.write("<td>");				
				document.write('<%= profil.getNom() + " " + profil.getPrenom()%>');
				document.write("<\/td>");
				
			
				for (var j = 0; j < finTab; j++) {
						if (nombre_jours <= finTab) {
							var  date = new Date(an,mois,nombre_jours);
							var tabFerier = JoursFeries(an);
									if ((an == cette_annee) && (mois == ce_mois)
											&& (nombre_jours == ce_jour))
										inscrit_cellule(nombre_jours, couleur_cejour,
												coul_pol_jours, taille_pol_jours,
												police_jours);
									else if (date.getDay()==6||date.getDay()==0)
										inscrit_cellule(nombre_jours, couleur_weekend,
												coul_pol_jours, taille_pol_jours,
												police_jours);
									else {
										inscrit_cellule(nombre_jours,
												tableauCouleurJours[nombre_jours - 1],
												coul_pol_jours, taille_pol_jours,
												police_jours);

									  }							
									nombre_jours++;

					}
				}
				document.write("<\/tr>");
			//}
		}

	//-->
	</script>

<!-- -- Fin include -->

<%} 
					}
				}%>
				<script>

				document.write("<\/table><\/div><\/div>");
var lignes=5;
var colonnes=2;
var tableau='<table border="2" align="right">';
tableau+='<tr>';
	tableau+='<td>Code<\/td>';
	tableau+='<td>Couleur<\/td>';
tableau+='</tr>';


	tableau+='<tr>';
		tableau+='<td>RTT</td>';
		tableau+='<td bgcolor="#00FF00"><\/td>';
	tableau+='</tr>';

	tableau+='<tr>';
		tableau+='<td>CSS<\/td>';
		tableau+='<td bgcolor="#FFA500"><\/td>';
	tableau+='</tr>';

	tableau+='<tr>';
		tableau+='<td>CP<\/td>';
		tableau+='<td bgcolor="#87CEEB"><\/td>';
	tableau+='</tr>';

	tableau+='<tr>';
		tableau+='<td>AM<\/td>';
		tableau+='<td bgcolor="#9932CC"><\/td>';
	tableau+='</tr>';
	
	tableau+='<tr>';
	tableau+='<td>Weekend & Férié<\/td>';
	tableau+='<td bgcolor="#7B8E8C"><\/td>';
	
	tableau+='<tr>';
	tableau+='<td>Jour Actuel<\/td>';
	tableau+='<td bgcolor="#FFFF00"><\/td>';
tableau+='</tr>';

tableau+='</table>';
//ecriture dans la page
document.write(tableau);
</script>

<script>

function goToCalendar() {
	document.location.href = "CalendarConges?id="+$("#id").val()+"&annee="
			+ <%=iYear%> + "&mois=" + <%=iMonth%>;
}

function goTo() {
	document.location.href="CalendarCongesAll?annee="+$("#annee").val()+"&mois="+$("#mois").val();
	 return false;
}
</script>


<%@ include file="../../templates/footer.jsp"%>