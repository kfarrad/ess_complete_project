<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Bienvenue" />
<c:set var="active" value="bienvenue" />
<%@ include file="../../templates/header.jsp"%>
<fieldset>
	<p class="info">${ form.resultat }</p>

</fieldset>
<%@ include file="../../templates/footer.jsp"%>
<script>
$(document).ready(function(){
    setTimeout(function() {
     window.location.href = "contactMail";
    }, 1200);
  });
</script>