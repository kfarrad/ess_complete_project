<%@page import="org.kohsuke.rngom.ast.builder.Include"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Liste des candidatures" />
<c:set var="active" value="Resume list" />
<%@ include file="../../templates/header.jsp"%>
<style>
.search {
	margin-left: 150px;
	padding: 10px;
}

#searchForm {
	display: inline;
	margin-left: 300px;
}

#listeCandidats {
	display: inline;
	margin-right: 140px;
}

#checkAdvcSearch {
	margin-left: 305px;
}

.searchInput {
	width: 230px;
}

#formAdvancedSearch {
	display: none;
	width: 300px;
	margin-left: 305px;
}

.msgBox .details {
	font-size: x-small;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
/* 	$( "#datepicker" ).datepicker();
 */	$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
});

/* $(function() {
    $( "#from" ).datepicker({
      dateFormat: 'dd-mm-yy',
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      dateFormat: 'dd-mm-yy',
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  }); */

function display() {
	if (document.getElementById('checkAdvcSearch').checked) {
	    var div = document.createElement('DIV');
	    div.id = 'formAdvancedSearch';
	    div.innerHTML = 'Trouvez que les candidatures entre : <br/> <label for="from">Date de début</label><input type="text" id="from" name="from"><label for="to">Date de fin</label><input type="text" id="to" name="to"><br/><input type="checkbox" id="searchInDocs" name="searchInDocuments">Chercher dans les fichiers (.pdf, .docx) ';
	    document.getElementById("searchForm").appendChild(div);
	    document.getElementById('formAdvancedSearch').style.display = 'block';
	    //Date picker 
	    $( "#from" ).datepicker({
	        dateFormat: 'dd-mm-yy',
	        defaultDate: "+1w",
	        changeMonth: true,
	        numberOfMonths: 3,
	        onClose: function( selectedDate ) {
	          $( "#to" ).datepicker( "option", "minDate", selectedDate );
	        }
	      });
	      $( "#to" ).datepicker({
	        dateFormat: 'dd-mm-yy',
	        defaultDate: "+1w",
	        changeMonth: true,
	        numberOfMonths: 3,
	        onClose: function( selectedDate ) {
	          $( "#from" ).datepicker( "option", "maxDate", selectedDate );
	        }
	      });
	} else {
	    document.getElementById('formAdvancedSearch').style.display = 'none';
	    document.getElementById("formAdvancedSearch").remove();
	}
}
function checkBoxValue() {
	alert("clic");
	if(document.getElementById('detailsForm').checked) {
		document.getElementById('detailsForm').value = false;
	} else {
		document.getElementById('detailsForm').value = true;
	}
	
	
}

function displayDetails(id) {
	msg.open(document.getElementById('detail_'+id));
}

function confirmDelete() {
	return confirm("Voulez vous vraiment supprimer cette condidature ?");
}

function updateFormSearchCSS() {
	 document.getElementById('searchForm').style.marginLeft = "0px";
}
	
</script>

<c:if test="${statutUpdate}">
	<script type="text/javascript">
$(document).ready(function(){
	$(".titre").notify("La modification a été éffectuée avec succès", "success");
});
</script>
</c:if>
<c:choose>
	<%-- Si aucun candidat n'existe , affichage d'un message par d�faut. --%>
	<c:when test="${ empty candidats }">
		<c:choose>
			<c:when test="${ search }">
				<p class="succes">Aucune candidature ne correspond aux critères
					de recherche renseignèes.</p>
				<br />
				<a href="<c:url value="/listeCandidats"/>" id="listeCandidats">Revenir
					à la liste des candidatures</a>
			</c:when>
			<c:otherwise>
				<p class="erreur">Aucune candidature enregistrée.</p>
			</c:otherwise>
		</c:choose>

	</c:when>
	<%-- Sinon, affichage du tableau. --%>
	<c:otherwise>
		<div class="search">
			<c:if test="${ search }">
				<a href="<c:url value="/listeCandidats"/>" id="listeCandidats">Liste
					des candidatures</a>
				<script type="text/javascript">
					$(document).ready(function(){
						updateFormSearchCSS();
					});
				</script>
			</c:if>
			<form method="get" action="<c:url value="/listeCandidats"/>"
				id="searchForm">
				<c:choose>
					<c:when test="${not empty searchTextBox}">
						<input type="text" name="searchTextBox" class="searchInput"
							value="${searchTextBox}" />
					</c:when>
					<c:when test="${empty searchTextBox}">
						<input type="text" name="searchTextBox" class="searchInput"
							value="" required />
					</c:when>
				</c:choose>
				<input type="hidden" name="search" value="<%="true"%>" /> <input
					type="submit" value="Chercher" />
			</form>
			<br /> <input type="checkbox" id="checkAdvcSearch"
				onclick="javascript:display();">Recherche avancée
		</div>
		<table class="table">
			<tr>
				<th>Nom</th>
				<th>Prénom</th>
				<th>Numéro de téléphone</th>
				<th>E-Mail</th>
				<th>Diplômes</th>
				<th>Spécialités</th>
				<th>CV</th>
				<th>Modification</th>
				<th>Suppression</th>
				<th>Détails</th>
				<c:if test="${search == true}">
					<th>Pertinence</th>
				</c:if>

			</tr>
			<c:forEach items="${candidats}" var="candidat" varStatus="myIndex">
				<c:choose>
					<c:when test="${candidat.pertinence == 6}">
						<tr class="pertNiveau1">
					</c:when>
					<c:when test="${candidat.pertinence == 5}">
						<tr class="pertNiveau2">
					</c:when>
					<c:when test="${candidat.pertinence == 4}">
						<tr class="pertNiveau3">
					</c:when>
					<c:when test="${candidat.pertinence == 3}">
						<tr class="pertNiveau4">
					</c:when>
					<c:when test="${candidat.pertinence == 2}">
						<tr class="pertNiveau5">
					</c:when>
					<c:when test="${candidat.pertinence == 1}">
						<tr class="pertNiveau6">
					</c:when>
				</c:choose>
				<td>${candidat.getNom()}</td>
				<td>${candidat.getPrenom()}</td>
				<td>${candidat.getTelephone()}</td>
				<td>${candidat.getMail()}</td>
				<td>${candidat.getDiplomesHtml()}</td>
				<td>${candidat.getSpecialitessHtml()}</td>
				<td><a
					href="<c:url value="/download/${candidat.getPathCV()}"/>"
					target="_blank" title="${candidat.getNomCV()}"><img
						src="<c:url value="/resources/images/cv_icon.png"/>"
						alt="<c:out value="${candidat.getNomCV()}"></c:out>" /></a></td>
				<td><a
					href="<c:url value="/updateCandidature"> <c:param name="idCandidature" value="${candidat.id}"/></c:url> ">
						<img src="<c:url value="/resources/images/modification.png"/>"
						alt="modification" class="infobulle" title="Modification" />
				</a></td>
				<td><form id="frmTitle"
						action="<c:url value="/deleteCandidature">
									<c:param name="idCandidature" value="${candidat.id}" /></c:url>"
						method="post">

						<a class="infobulle" title="Suppression"><input type="submit"
							class="" id="btnResumeDel" value=""
							onclick="return confirmDelete()"></a>
					</form></td>
				<td><a href="#"><img
						src="<c:url value="/resources/images/details.png"/>"
						onclick="displayDetails(${myIndex.index})"
						title="Afficher l'historique"></a>
					<div id="detail_${myIndex.index}" style="display: none;">
						<fieldset class="details">
							<legend>Détails du candidat ${candidat.nom}
								${candidat.prenom}</legend>
							${candidat.commentaires}
						</fieldset>
					</div></td>



				<c:if test="${search == true}">
					<c:choose>
						<c:when test="${candidat.pertinence == 6}">
							<td>Spécialité</td>
						</c:when>
						<c:when test="${candidat.pertinence == 5}">
							<td>Diplôme</td>
						</c:when>
						<c:when test="${candidat.pertinence == 4}">
							<td>Nom</td>
						</c:when>
						<c:when test="${candidat.pertinence == 3}">
							<td>Prénom</td>
						</c:when>
						<c:when test="${candidat.pertinence == 2}">
							<td>Téléphone</td>
						</c:when>
						<c:when test="${candidat.pertinence == 1}">
							<td>Mail</td>
						</c:when>
						<c:when test="${candidat.pertinence == 0}">
							<td>Fichiers (.pdf, .docx)</td>
						</c:when>
					</c:choose>
				</c:if>

				</tr>
			</c:forEach>
		</table>

		<%--For displaying Previous link except for the 1st page --%>
		<c:if test="${currentPage != 1}">
			<c:choose>
				<c:when test="${search}">
					<td><a
						href="/ess/listeCandidats?searchTextBox=${searchTextBox}&search=true&page=${currentPage - 1}">Précédent</a></td>
				</c:when>
				<c:when test="${!search}">
					<td><a href="/ess/listeCandidats?page=${currentPage - 1}">Précédent</a></td>
				</c:when>
			</c:choose>
			<%-- <td><a href="/ess/listeCandidats?page=${currentPage - 1}">Pr�c�dent</a></td> --%>
		</c:if>

		<%--For displaying Page numbers. 
    The when condition does not display a link for the current page--%>
		<c:if test="${noOfPages > 1}">
			<table cellpadding="5" cellspacing="5">
				<tr>
					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<td>${i}</td>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${search}">
										<td><a
											href="/ess/listeCandidats?searchTextBox=${searchTextBox}&search=true&page=${i}">${i}</a></td>
									</c:when>
									<c:when test="${!search}">
										<td><a href="/ess/listeCandidats?page=${i}">${i}</a></td>
									</c:when>
								</c:choose>
								<%-- <td><a href="/ess/listeCandidats?page=${i}">${i}</a></td> --%>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</table>
		</c:if>
		<%--For displaying Next link --%>

		<c:if test="${currentPage lt noOfPages}">
			<c:choose>
				<c:when test="${search}">
					<td><a
						href="/ess/listeCandidats?searchTextBox=${searchTextBox}&search=true&page=${currentPage + 1}">Suivant</a></td>
				</c:when>
				<c:when test="${!search}">
					<td><a href="/ess/listeCandidats?page=${currentPage + 1}">Suivant</a></td>
				</c:when>
			</c:choose>
			<%-- <c:if test="${search}">
        <td><a href="/ess/listeCandidats?page=${currentPage + 1}">Suivant</a></td>
        </c:if> --%>
		</c:if>

	</c:otherwise>
</c:choose>


<%@ include file="../../templates/footer.jsp"%>


