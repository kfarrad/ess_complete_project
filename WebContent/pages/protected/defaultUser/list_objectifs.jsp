<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Section objectifs -->
<form id = "formWorkObjectifsDelete" action="<c:url value="/autoEvaluation"><c:param name="formType" value="OBJ"/></c:url>" method="post" novalidate = "novalidate">
	<input type="hidden" name="objectifs[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
	<input type="hidden" name="champs[action]" id = "objectifs_delete" value="delete"/>
	<table class="table">
			<thead>
				<tr>
					<th><input type="checkbox" id = "allObjectifs"></th>
					<th>Objectif initial</th>
					<th>Estimation</th>
				</tr>
			</thead>
			<tbody>
			<c:choose>
				<%--Si la variable gestionEvaluation est null, affiche le message par defaut --%>
				<c:when test="${empty gestionEvaluations.autoEvaluations.objectifses}">
					<tr>
						<td colspan="5" class="name">Aucune donn�e</td>
					</tr>
				</c:when>
				<%-- Sinon, affichage du tableau --%>
				<c:otherwise>
					<%-- Parcours le tableau et affiche la liste des activit�s  --%>
					
					<c:forEach items="${gestionEvaluations.autoEvaluations.objectifses}" var="mapObjectifs" varStatus="boucle">
					<!-- <input type="hidden" name="objectifs_id_objectif" value="${mapObjectifs.objectifsId}"/> -->
					<tr class="dataObj">
						<!-- <td><c:out value="${boucle.index + 1}" /></td> -->
						<td><input type="checkbox" name="objectifs_id_objectif" value="${mapObjectifs.objectifsId}"></td>
						<td><c:out value="${mapObjectifs.objectifsLibelle}" /></td>
						<td><c:out value="${mapObjectifs.objectifsEstimation}" /></td>						
					</tr>
					</c:forEach>
					</c:otherwise>
			</c:choose>
			</tbody>
		</table>
	   </form>
				