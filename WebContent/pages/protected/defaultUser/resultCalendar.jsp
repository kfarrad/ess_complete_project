<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%!public int nullIntconv(String inv) {
	int conv = -1;

	try {
		conv = Integer.parseInt(inv);
	} catch (Exception e) {
	}
	return conv;
}%>

<%

/* int iYear=nullIntconv(request.getParameter("annee"));

int iMonth=nullIntconv(request.getParameter("mois")); */

Calendar ca = new GregorianCalendar();
int iTDay=ca.get(Calendar.DATE);
int iTYear=ca.get(Calendar.YEAR);
int iTMonth=ca.get(Calendar.MONTH);

int iMonth=-1;
int iYear=-1;

try {
	 iYear= ( Integer)request.getAttribute("annee");
	 iMonth= ( Integer)request.getAttribute("mois");
} catch (Exception e) {

	 e.printStackTrace();
}
if(iYear==-1)
{
	  iYear=iTYear;
	  
}
if(iMonth==-1)
{
	 iMonth=iTMonth;
}

List<Projet> menu = new ArrayList<Projet>();
List<Calendrier> listeCalendrier = new ArrayList<Calendrier>();
List<Calendrier> calendriers = null;
List<Float> total = new ArrayList<Float>();
// traitement pour admin
String id_user = request.getParameter("id"); 
Long idUser=0L;
if(id_user!=null) idUser=Long.parseLong(id_user);
else{
	 Long id_user2 = (Long)request.getAttribute("id");
	 if(id_user2!=null){
			idUser= id_user2;
		}
}
Utilisateur utilisateur = (Utilisateur)session.getAttribute(Constants.ATT_SESSION_USER);

Double total_jours =0.0;
	 try {
		 menu = ( List<Projet>)request.getAttribute("projets");
		 calendriers = (List<Calendrier>)request.getAttribute("calendriers");
	} catch (Exception e) {
		e.printStackTrace();
 }
	 if(menu==null) menu = new ArrayList<Projet>();
	 if(calendriers==null) listeCalendrier = new ArrayList<Calendrier>();
	 
	 if( calendriers.size() != 0) {
		 for(Projet projet: menu){
			 for(Calendrier calendrier: calendriers){				 	
		 		if(calendrier.getId_projet()==projet.getId()) listeCalendrier.add(calendrier);
		 	}
		 
		 }
	 }
	 Map<String,Double> bilan =  new HashMap<String,Double>();
	 Double total_bilan=0.0;
	 Double total_presence=0.0;
	 
	 
	 
	 if(listeCalendrier.size()!=0 && menu.size()!=0){
		 for(int j=0;j<listeCalendrier.size();j++){
			 Long id_projet = listeCalendrier.get(j).getId_projet();
			 for(Projet projet: menu){
				 String nom_projet = projet.getNomProjet(id_projet);
				 if(nom_projet!=null){
					 if(!nom_projet.contains("CP") && !nom_projet.contains("RTT")&& !nom_projet.contains("CSS")) 
						 total_presence +=listeCalendrier.get(j).getTotal_jour();
					 else bilan.put(nom_projet,listeCalendrier.get(j).getTotal_jour());
					 total_bilan+=listeCalendrier.get(j).getTotal_jour();
				 }
			 }
			
		 }
		 bilan.put("PRESENCE", total_presence);
		 bilan.put("TOTAL", total_bilan);
	 }
	 
%>

<script>
function goTo() {
	<%if(utilisateur!=null){ 
		  if(utilisateur.isUser()||(idUser==0L)){%>
	document.location.href="calendarBilan?annee="+$("#annee").val()+"&mois="+$("#mois").val();
	  <%}else {%>
	 document.location.href="userCalendar?id=<%=idUser%>&annee="+$("#annee").val()+"&mois="+$("#mois").val();
	<%}
		  }%>	
	 return false;
}

jQuery(document).ready(function() {
	$("#dynamic").scroll(function(){
		  $("#divSelect").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  			
		});	
});

</script>
<div id="divSelect" class="TabDyn">
	<table class="table" id="selection">
		<tbody>
			<tr class="odd">
				<td>Year</td>
				<td><select id="annee" name="annee" onchange="goTo()">
						<%
								// start year and end year in combo box to change year in calendar
								for (int iy = iTYear - 20; iy <= iTYear + 20; iy++) {
									if (iy == iYear) {
							%>
						<option value="<%=iy%>" selected="selected"><%=iy%></option>
						<%
								} else {
							%>
						<option value="<%=iy%>"><%=iy%></option>
						<%
								}
								}
							%>
				</select></td>
				<td align="center"><h3>
						<%
								Calendar calend = Calendar.getInstance();
								calend.set(Calendar.YEAR, 2008);
								calend.set(Calendar.MONTH, iMonth);
								calend.set(Calendar.DAY_OF_MONTH, 01);
							%>
						<%=new SimpleDateFormat("MMMM").format(calend.getTime())%>
						<%=iYear%></h3></td>
				<td>Month</td>
				<td><select id="mois" name="mois" onchange="goTo()">
						<%
								// print month in combo box to change month in calendar
								for (int im = 0; im <= 11; im++) {
									if (im == iMonth) {
										calend.set(Calendar.MONTH, im);
							%>
						<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
						<%
								} else {
										calend.set(Calendar.MONTH, im);
							%>
						<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
						<%
								}
								}
							%>
				</select></td>				
				<td><h2>
						Status:
						<%
						if (listeCalendrier.size() == 0) {
					%>
						<span id="status" class="nonSoumis"> Non soumis</span>
						<%
								} else {
									if (listeCalendrier.get(0).getStatus() == 0) {
							%>
						<span id="status" class="soumis"> soumis</span>
						<%
								} else {
										if (listeCalendrier.get(0).getStatus() == 1) {
							%>
						<span id="status" class="traitement"> En traitement</span>
						<%
								} else {
							%>
						<span id="status" class="approuve"> Approuve </span>
						<%
								}
									}
								}
							%>
					</h2></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="TabDyn">
	<table id="affichage" class="table">
		<tr>
			<%calend.set(Calendar.MONTH, iMonth); %>
			<%=new SimpleDateFormat("MMMM").format(calend.getTime())%>
			<%=iYear%>
		</tr>
		<tr>
			<th>cra</th>
			<th align="center" height="15"><span>Jours</span></th>
		</tr>
		<%if(utilisateur.isAdmin()){
			if(menu.size()!=0){
			for(int i=0; i<menu.size(); i++){ %>
		<tr <% if(i%2==0){ %> class="odd" <%} %>>
			<td><span><%=menu.get(i).getNom()%></span></td>
			<td><span> <%if((listeCalendrier.size()!=0)&&(i<listeCalendrier.size())){
								%> <%=listeCalendrier.get(i).getTotal_jour() %> <%}else{ %> 0.0 <%} %>
			</span></td>
		</tr>
		<%}
		}
		}else{%>

		<% int k=0;
			for(Map.Entry<String,Double> e : bilan.entrySet()) {
	        String key = e.getKey();
	        Double value = e.getValue();%>
		<tr <% if(k%2==0){ %> class="odd" <%} %>>
			<td><span><%=key %></span></td>
			<td><span><%=value %></span></td>
		</tr>
		<%k++;}} %>
		<tr><td>RTT restant</td><td><span>${gestionConges.rTTInitial}</span></td></tr>
		<tr><td>CP restant</td><td><span>${gestionConges.cPInitial}</span></td></tr>
	</table>
</div>