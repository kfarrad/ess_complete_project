<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="changeActivites" style="display: none;">
<div class="head">
	<h1 id="headChangeActivites">Ajouter une activité</h1>
 </div>
 <div class="inner">
<form id = "formWorkActivites" action="<c:url value="/autoEvaluation"><c:param name="formType" value="ACT"/></c:url>" method="post" novalidate = "novalidate">
<input type="hidden" name="activites[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
<input type="hidden" name="activites[id_activite]" id="activites_id_activite" value="0"/>
<input type="hidden" name="champs[action]" id = "activites_sauve" value="sauve"/>
<fieldset>
<ul>
	<li class="largeTextBox">
	 	<label for="activites_periode_de">Date de debut <em>*</em></label>
		<input id="activites_periode_de" type="text" name="activites[periode_de]" class="monthpicker">
	</li>
	<li class="largeTextBox">
	 	<label for="activites_periode_au">Date de fin <em>*</em></label>
		<input id="activites_periode_au" type="text" name="activites[periode_au]" class="monthpicker">
	</li>
	<li class="largeTextBox">
	 	<label for="activites_missions">Missions ou réalisations <em>*</em></label>
		<textarea maxlength="10" class="jqte-test" name="activites[missions]" id="activites_missions"></textarea><br />
Il vous reste <span id="carac_reste_textarea_1"></span> caractères
 <script type="text/javascript">
<!--
maxlength_textarea('activites_missions','carac_reste_textarea_1',150);
-->
</script>
	</li>
	<li class="largeTextBox">
	 	<label for="activites_responsable">Responsable <em>*</em></label>
		<input id="activites_responsable" type="text" name="activites[responsable]" required>
	</li>
	<li class="largeTextBox">
	 	<label for="activites_entretien">Entretien de mission <em>*</em></label>
		<input type="radio" id="activites_entretien_oui" name="activites[entretien]" value="1" /> Oui
		<input type="radio" id="activites_entretien_non" name="activites[entretien]" value="0" /> Non
	</li>
	</ul>
	<p>
		<input type="button" class="" id="btnWorkActvititeSauve" value="Sauver">
		<input type="button" class="reset" id="btnWorkActvititeCancel" value="Annuler">
	</p>
	</fieldset>	
</form>
</div>
</div>