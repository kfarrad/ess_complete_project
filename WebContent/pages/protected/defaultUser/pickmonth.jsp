<html>
<head>
<title>Date Picker</title>
<script type="text/javascript">
ReturnFunc = '';
function Calendar(iYear, iMonth, iDay, ContainerId, ClassName){
    MonthNames = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    oDate = new Date();
    Year = (iYear == null) ? oDate.getFullYear() : iYear;
    Month = (iMonth == null) ? oDate.getMonth() : iMonth - 1;
    while(Month < 0){Month += 12;Year--}
    while(Month >= 12){Month -= 12;Year++}
    Day = (iDay == null) ? 0 : iDay;
    oDate = new Date(Year, Month, 1);
    NextMonth = new Date(Year, Month + 1, 1);
    WeekStart = oDate.getDay();
    MonthDays = Math.round((NextMonth.getTime() - oDate.getTime()) / 86400000) + 1;
    if(ContainerId != null){
        ContainerId = ContainerId;
        Container = document.getElementById(ContainerId);
        if(!Container)
            document.write('<div id="' + ContainerId + '"> </div>');
    }
    else{
        do{
            ContainerId = 'tblCalendar' + Math.round(Math.random() * 1000);
        }
        while(document.getElementById(ContainerId));
        document.write('<div id="' + ContainerId + '"> </div>');
    }
    Container = document.getElementById(ContainerId);
    ClassName = (ClassName == null) ? 'tblCalendar' : ClassName;
    HTML = '<table border=1>';
    HTML += '<tr bgcolor="red"><td ></td><td colspan="5" class="Title">' + MonthNames[Month] + ' ' + Year + '</td><td ></td></tr>';
    HTML += '<tr ><td>S</td><td>M</td><td>T</td><td>W</td><td>T</td><td>F</td><td>S</td></tr>';
    HTML += '<tr>';
    for(DayCounter = 0; DayCounter < WeekStart; DayCounter++){
        HTML += '<td> </td>';
    }
    for(DayCounter = 1; DayCounter < MonthDays; DayCounter++){
        if((DayCounter + WeekStart) % 7 == 1) HTML += '<tr class="Days">';
        if(DayCounter == Day)
            HTML += '<td ><a href="javascript:ReturnDate(' + DayCounter + ')">' + DayCounter + '</a></td>';
        else HTML += '<td><a href="javascript:ReturnDate(' + DayCounter + ')">' + DayCounter + '</a></td>';
        if((DayCounter + WeekStart) % 7 == 0) HTML += '</tr>';
    }
    for(j = (42 - (MonthDays + WeekStart)), DayCounter = 0; DayCounter <= j; DayCounter++){
        HTML += '<td> </td>';
        if((j - DayCounter) % 7 == 0) HTML += '</tr>';
    }
    HTML += '</table>';
    Container.innerHTML = HTML;
    return ContainerId;
}
function ReturnDate(Day){
    opener.SetDate(Day, Month+1, Year);
    window.close();
}
function MakeDate(iYear, iMonth, iDay, fn){
    D = new Date();
    Year = (typeof(iYear) != 'undefined') ? iYear : D.getFullYear();
    Month = (typeof(iMonth) != 'undefined') ? iMonth : D.getMonth();
    Day = (typeof(iDay) != 'undefined') ? iDay : D.getDate();
    ReturnFunc = fn;
    id = Calendar(Year, Month+1, Day, 'cal', 'CalendarRed');
}
</script>
</head>
<body onLoad="MakeDate();">
<div id='cal'> </div>
</body>
</html>