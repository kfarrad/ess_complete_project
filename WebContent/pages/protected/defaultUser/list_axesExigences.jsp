<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="inner">
<ul>
	<li>
		<label for="axes">Axe d'exigences &nbsp;</label>
		<select name="axe" id="axe">
			<option value="" selected="selected">Choisir un axe ...</option>
			<option value="Clt">Client</option>
			<option value="Ctt">Metier du consultant</option>
			<option value="Egt">Engagement et Qualités Individuelles</option>
			<option value="Dev">Développement des Hommes et Management</option>			
		</select>
	</li>
</ul>
</div>
