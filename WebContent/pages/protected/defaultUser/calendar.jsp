<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%!public int nullIntconv(String inv) {
		int conv = -1;

		try {
			conv = Integer.parseInt(inv);
		} catch (Exception e) {
		}
		return conv;
	}%>

<%-- <%

	int iYear=nullIntconv(request.getParameter("annee"));
 int iMonth=nullIntconv(request.getParameter("mois"));

 Calendar ca = new GregorianCalendar();
 int iTDay=ca.get(Calendar.DATE);
 int iTYear=ca.get(Calendar.YEAR);
 int iTMonth=ca.get(Calendar.MONTH);

 if(iYear==-1)
 {
	  iYear=iTYear;
 }
 if(iMonth==-1)
 {
	  iMonth=iTMonth;
 }
 %> --%>

<%
	Calendar ca = new GregorianCalendar();
 int iTDay=ca.get(Calendar.DATE);
 int iTYear=ca.get(Calendar.YEAR);
 int iTMonth=ca.get(Calendar.MONTH);
 int iMonth=-1;
 int iYear=-1;
 
 try {
	 iYear= ( Integer)request.getAttribute("annee");
	 iMonth= ( Integer)request.getAttribute("mois");
 } catch (Exception e) {

	 e.printStackTrace();
 }
 if(iYear==-1)
 {
	  iYear=iTYear;
	  
 }
 if(iMonth==-1)
 {

	 iMonth=iTMonth;
 } 
%>
<%
 GregorianCalendar cal = new GregorianCalendar (iYear, iMonth, 1); 

 int days=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
 int weekStartDay=cal.get(Calendar.DAY_OF_WEEK); 
 cal = new GregorianCalendar (iYear, iMonth, days); 
 
 int iTotalweeks=cal.get(Calendar.WEEK_OF_MONTH);
 ArrayList<Integer> list = new ArrayList<Integer>();
 List<Projet> menu = new ArrayList<Projet>();
 List<Float> total = new ArrayList<Float>();
 List<Calendrier> listeCalendrier = new ArrayList<Calendrier>();
 String id_user = request.getParameter("id"); 
 Long idUser=0L;
 if(id_user!=null) idUser=Long.parseLong(id_user);
 else{
	 Long id_user2 = (Long)request.getAttribute("id");
	 if(id_user2!=null){
			idUser= id_user2;
		}
 }
 Utilisateur utilisateur = (Utilisateur)session.getAttribute(Constants.ATT_SESSION_USER);
 Double total_jours =0.0;
	 try {
		 menu = ( List<Projet>)request.getAttribute(Constants.ATT_PROJETS);		 
		 List<Calendrier> calendriers = (List<Calendrier>)request.getAttribute(Constants.ATT_CALENDRIER);		 
		 List<Integer> feriers = (List<Integer>)request.getAttribute(Constants.ATT_FERIERS);
		 //traitement d'une mauvaise reception
		 if(menu==null)	 menu = new ArrayList<Projet>();
		 if(calendriers==null)calendriers = new ArrayList<Calendrier>();
		 if(feriers==null) feriers = new ArrayList<Integer>();
			
		 if (calendriers!=null){			 
			 if( calendriers.size() != 0) {
				 for(Projet projet: menu){
					 for(Calendrier calendrier: calendriers){				 	
				 		if(calendrier.getId_projet()==projet.getId()) listeCalendrier.add(calendrier);
				 	}
				 
				 }
				 
				
				 for (int j = 0; j < 31; j++) {
				 	Float sum=0.0F;
			 		for(int i=0; i<listeCalendrier.size();i++){
						sum+=listeCalendrier.get(i).getJours().get(j);
			 		}
			 		total.add(j, sum);
				 }
			 	 for(int i=0; i<listeCalendrier.size();i++){
			 	 total_jours+=listeCalendrier.get(i).getTotal_jour();
			 	}
			}
		}

		 for(Integer ferier: feriers){
			 list.add(ferier);
		 }
		 
		 
	 } catch (Exception e) {
		 e.printStackTrace();
	 }
	 
	 
	 
 cal = new GregorianCalendar (iYear, iMonth, 1); 
 do {
	    // get the day of the week for the current day
	    int day = cal.get(Calendar.DAY_OF_WEEK);
	    // check if it is a Saturday or Sunday
	    if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
	        // print the day - but you could add them to a list or whatever
	        list.add(cal.get(Calendar.DAY_OF_MONTH));	   }
	    // advance to the next day
	    cal.add(Calendar.DAY_OF_YEAR, 1);
	}  while (cal.get(Calendar.MONTH) == iMonth);
 
 
%>


<style>

/* .TabDyn {
    margin: auto;
    padding: 10px;
} */




</style>

<script type="text/javascript">
/* function goTo() {
	$('table[id="affichage"]').hide();
	$("#sauver").hide();
	$("#soumettre").hide();
	$("#status").text("");
	
} */
function goTo() {
	<%if(utilisateur!=null){ 
		  if(utilisateur.isUser()||(idUser==0L)){%>
	document.location.href="userCalendar?annee="+$("#annee").val()+"&mois="+$("#mois").val();
	  <%}else {%>
	 document.location.href="userCalendar?id=<%=idUser%>&annee="+$("#annee").val()+"&mois="+$("#mois").val();
	<%}
		  }%>	
	 return false;
}

function setStatus(){
	 document.frm.isvalid.value="true";
	 document.frm.submit();
	
}
function disableTab(){
	$("#affichage").find("select").each(function() {
		$(this).attr("disabled", "disabled");
		$('input[id="sauver"]').hide();
		$('input[id="soumettre"]').hide();
	});
}

function buttonSubmit(){
	 var m=0;
	 var valid = 0;
	while(m<=<%=days%>){
		if($("#td_total_"+m).attr("class")!="totalbad") valid++;
		m++;
	}
	if(valid==<%=days%>+1) 	{
		$('input[id="soumettre"]').show();
		
	}
		
	else  {$('input[id="soumettre"]').hide();
	
	}
}

function remplirUn(lig){
	//mettre à 1 toute une ligne
	for (var j = 1; j <= <%=days%>; j++) {
		var chaineLig="#projet_"+j+"_"+lig;
		
		$(chaineLig).val(1);
		calculTotal(j,lig);
	}
}

function remplirZero(lig){
	//mettre à 1 toute une ligne
	for (var j = 1; j <= <%=days%>; j++) {
		var chaineLig="#projet_"+j+"_"+lig;
		
		$(chaineLig).val("0.0");
		calculTotal(j,lig);
	}
}

function calculTotal(col,lig){
	var totalId ="#total_"+col;
	var totalJour="#jour_projet_"+lig;
	var tdTotalId ="#td_total_"+col;
	var sommeCol = 0.0;
	var sommeLig = 0.0;
	
	//Affichage du boutton !sauver
/* 	$('input[id="sauver"]').show(); */
	$('#sauver').prop('disabled', false);
	//Calcul somme sur une colonne d'une journee
	for (var i = 1; i < id+<%=menu.size()%>; i++) {
		var chaineCol="#projet_"+col+"_"+i;
		if($(chaineCol).val()!=null){
		sommeCol += parseFloat($(chaineCol).val());
		}
		
	}		
	//Calcul somme sur une ligne d'un projet
	for (var j = 1; j <= <%=days%>; j++) {
		var chaineLig="#projet_"+j+"_"+lig;
		if($(chaineLig).val()!=null){
			sommeLig += parseFloat($(chaineLig).val());
		}
		//alert (i);
	}
	
	//jeu de couleur sur le resultat
	if(sommeCol==1.0) {
		$(tdTotalId).removeClass('totalbad');
		$(tdTotalId).addClass('totalgood');
	}
	else{
		$(tdTotalId).removeClass('totalgood');
		$(tdTotalId).addClass('totalbad');
	}
	
	//modification du total final
	print(totalId,sommeCol);
	print(totalJour,sommeLig);
	
	//total jours
	sommeCol = 0;
	for (var i = 1; i < id+<%=menu.size()%>; i++) {
		var chaine="#jour_projet_"+i;
		if($(chaine).val()!=null){
			sommeCol += parseFloat($(chaine).text());
		}
		//alert (i);
	}
	print("#totalJ", sommeCol);
	
	//affichage du bouton de validation finale
	buttonSubmit();
	
}

function print(elt, value){
	if(value == Math.round(value)) $(elt).text(value+".0");
	else $(elt).text(value);
	
}
	// variable globale
	var id = 1;
	

$(document).ready(function() {
		//$('tr[id="projet"]').hide();		
		$('input[id="soumettre"]').hide();
		
		//affichage du bouton de validation finale
		buttonSubmit();		
		
		<%if(utilisateur.isRh()){ %> disableTab(); 
		$('input[id="sauver"]').hide(); 
		<%}
		
		if (listeCalendrier!=null){ if( listeCalendrier.size() != 0) {
			
			if (((listeCalendrier.get(0).getStatus() == 2))||(listeCalendrier.get(0).getStatus() == 1)&&(iTDay>20 )) {%>
				disableTab();
				<%}
		}}%>
		
		$("#dynamic").scroll(function(){
			  $("#divSelect").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
			  $("#scrolableT").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
			  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		
			});

	
		setTimeout(function() {
			$('#slickbox').hide('slow');
		}, 3000);
		$("#editT").hide();
		$("#btnCancel").hide();

		$("#btnEditConges").click(function() {
			$("#editT").show();
			$("#viewT").hide();
			$("#btnEditConges").hide();
			$("#btnCancel").show();
		});

		$("#btnCancel").click(function() {
			$("#viewT").show();
			$("#editT").hide();
			$("#btnEditConges").show();
		});

		$("#btnSauveConges").click(function() {
			$("#frmConges").submit();
		});
		
		hideHolidaysDiv();
		function hideHolidaysDiv(){
			if(!$('#viewT').length && !$('#editT').length ){
				$('#dd_ajax_float').hide();
			}
		}
});
function submittedForm() {
    $('select').removeAttr('disabled');
    $('#sauver').prop('disabled', true);
}
</script>
		 
		<%-- $('button[name="button"]').click(function(){
			<%if(utilisateur!=null){ %>
			$('table[id="affichage"]').show();
			<% if(utilisateur.isUser()||(idUser==0L)){ %>
			document.location.href="userCalendar?annee="+$("#annee").val()+"&mois="+$("#mois").val();
			  <%}else { %>
			 document.location.href="userCalendar?id=<%=idUser%>&annee="+$("#annee").val()+"&mois="+$("#mois").val();
			<% }
			}
			%>	
			 return false;
		}); --%>
	<div 
		style="position: absolute; top: 194px; right: 20px; display: block; text-align: center;"
		id="dd_ajax_float">
	<c:if test="${not empty gestionConges}">
		<div id="viewT">
			<fieldset>
				<legend style="text-align: center;">Infos congés</legend>
				<!-- <h2 class="titre" style="text-align:center;">Infos congés</h2> -->
				<p>
					<B>RTT restant :</B>
				<p />
				<p>
					<strong> <c:out value="${gestionConges.rTTInitial}" /> <c:if
							test="${gestionConges.getValide()!=1}">+(<span
								style="color: blue;"><c:out
									value="${gestionConges.rttAcquis}" /></span>)</c:if> jrs
					</strong>
					<c:if test="${gestionConges.getValide()==0}">
						<br />
						<span style="color: red;">- <c:out
								value="${gestionConges.rTTAttente}" /></span>
					</c:if>
				</p>
				<br />
				<p>
					<B>CP restant :</B>
				</p>
				<p>
					<strong> <c:out value="${gestionConges.cPInitial}" /> <c:if
							test="${gestionConges.getValide()!=1}">+(<span
								style="color: blue;"><c:out
									value="${gestionConges.cpAcquis}" /></span>)</c:if> jrs
					</strong>&nbsp;
					<c:if test="${gestionConges.getValide()==0}">
						<br />
						<span style="color: red;">- <c:out
								value="${gestionConges.cPAttente}" /></span>
					</c:if>
				</p>
				<br />
				<c:if test="${gestionConges.getValide()==0}">
					<p style="color: red;">* La modification sera effective une
						fois que la direction aura validé</p>
				</c:if>
			</fieldset>
		</div>
		</c:if>
		<c:if test="${sessionScope.sessionUser.isAdmin()}">
			<div id="editT">
				<form id="frmConges"
					action="<c:url value="/gestionsConges"></c:url>" method="post"
					novalidate="novalidate">
					<fieldset style="text-align: center;">
						<legend>Initialisation</legend>
						<!-- <h2 class="titre" style="text-align: center;">Initialisation</h2> -->
						<p>
							<label style="width: 95%;" for="txtRTT">RTT restant<em>*</em><br />
								<input style="width: 50%;" type="number" name="txtRTT"
								id="txtRTT" step="0.25" min="-10" max="10"
								value="<c:out value="${gestionConges.rTTInitial}"/>"> <%-- <select name="txtRTT" id="txtRTT">
						<option value="0" selected></option>
						<c:forEach var="i" begin="1" end="10">
							<option value="<c:out value="${i}"/>" ${i==gestionConges.rTTInitial?'selected':''}><c:out value="${i}"/></option>
						</c:forEach>
					</select> --%>
							</label>
						</p>
						<p>&nbsp;</p>
						<p>
							<label for="txtCP" style="width: 95%;">CP restant<em>*</em><br />
								<input style="width: 50%;" type="number" name="txtCP" id="txtCP"
								step="0.5" min="-40" max="40"
								value="<c:out value="${gestionConges.cPInitial}"/>"> <%-- <select name="txtCP" id="txtCP">
						<option value="0"></option>
						<c:forEach var="i" begin="1" end="40">
							<option value="<c:out value="${i}"/>" ${i==gestionConges.cPInitial?'selected="selected"':''}><c:out value="${i}"/></option>
						</c:forEach>
					</select> --%>
							</label>
						</p>
						<p>
							<input type="hidden" id="idUser" name="idUser"
								value="${idProfilUser}"> <input type="button" class=""
								id="btnSauveConges" value="Sauver"> <input type="button"
								id="btnCancel" value="Annuler">
						</p>
					</fieldset>
				</form>

			</div>
			<p>&nbsp;</p>
			<form novalidate="novalidate">
				<p>
					<input type="button" id="btnEditConges" value="Editer Conges">
				</p>
			</form>
		</c:if>

	</div>


<form name="frm" method="post" id="frmCra" onsubmit="submittedForm()"
	action="<c:url value="/userCalendar"></c:url>">
	<input type="hidden" name="isvalid" value="0">
<%-- 	<input type="hidden" name="userId" value="<%=idUser%>"> --%>
	<div id="divSelect" class="TabDyn box">
		<table class="table" id="selection">
			<tbody>
				<tr class="odd">
					<td>Year</td>
					<td><select id="annee" name="annee" onchange="goTo()">
							<%
								// start year and end year in combo box to change year in calendar
								for (int iy = iTYear - 20; iy <= iTYear + 20; iy++) {
									if (iy == iYear) {
							%>
							<option value="<%=iy%>" selected="selected"><%=iy%></option>
							<%
								} else {
							%>
							<option value="<%=iy%>"><%=iy%></option>
							<%
								}
								}
							%>
					</select></td>
					<td align="center"><h3>
							<%
								Calendar calend = Calendar.getInstance();
								calend.set(Calendar.YEAR, 2008);
								calend.set(Calendar.MONTH, iMonth);
								calend.set(Calendar.DAY_OF_MONTH, 01);
							%>
							<%=new SimpleDateFormat("MMMM").format(calend.getTime())%>
							<%=iYear%></h3></td>
					<td>Month</td>
					<td><select id="mois" name="mois" onchange="goTo()">
							<%
								// print month in combo box to change month in calendar
								for (int im = 0; im <= 11; im++) {
									if (im == iMonth) {
										calend.set(Calendar.MONTH, im);
							%>
							<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
							<%
								} else {
										calend.set(Calendar.MONTH, im);
							%>
							<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
							<%
								}
								}
							%>
					</select></td>
					<td><h2>
							Statut:
							<%
						if (listeCalendrier.size() == 0) {
							%>
							<span id="status" class="nonSoumis"> Non soumis </span>
							<%
								} else {
									if (listeCalendrier.get(0).getStatus() == 0) {
							%><span id="status" class="soumis"> Sauvé </span>
							<%
								} else {
										if (listeCalendrier.get(0).getStatus() == 1) {
							%><span id="status" class="traitement"> Soumis </span>
							<%
								} else {
							%><span id="status" class="approuve"> Approuvé </span>
							<%
								}
									}
								}
							%>
						</h2></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="TabDyn ">
		<div id="scrolableT">
		<table class="table" id="affichage1">

			<thead>

				<tr>
					<th>cra</th>
					<th><span>Jours</span></th>

					<c:if test="${!sessionScope.sessionUser.isRh()}">
						<%
					if ((listeCalendrier.size() == 0)||((!listeCalendrier.isEmpty())&&(listeCalendrier.get(0).getStatus() < 1))) {
			    	%>
						<th><span>Remplir</span></th>
						<%}%>
					</c:if>
				</tr>
			</thead>
			
			<tbody>
				<%
					if (menu.size() != 0) {
						int k = 1;
						for (Projet elt : menu) {
				%>
				<tr class="odd" id="projet_<%=k%>">
					<td><span id="projetId_<%=k%>"><%=elt.getNom()%></span></td>
					<td><span id="jour_projet_<%=k%>"> <%
							if ((listeCalendrier.size() != 0)
											&& (k <= listeCalendrier.size())) {
						 %> <%=listeCalendrier.get(k - 1).getTotal_jour()%> <%
							} else {
						 %> 0.0 <%
							}
						 %>
					</span></td>
					<c:if test="${!sessionScope.sessionUser.isRh()}">
						<%
						if ((listeCalendrier.size() == 0)||((!listeCalendrier.isEmpty())&&(listeCalendrier.get(0).getStatus() < 1))) {
						    	
							if ((!listeCalendrier.isEmpty())
									&& (Constants.getIdProjetsNonFactures().contains(listeCalendrier.get(k - 1).getId_projet().intValue()))
									&& (listeCalendrier.get(k - 1).getTotal_jour() != 0.0F))
						    	{%>
						<td nowrap><input disabled class="fill" name="autoLine_1" type="button"
							value="1" onclick="remplirUn(<%=k %>)" /> <input disabled class="fill"
							name="autoLine_0" type="button" value="0"
							onclick="remplirZero(<%=k %>)" /></td>
						<%} else {%>
						<td nowrap><input class="fill" name="autoLine_1" type="button"
							value="1" onclick="remplirUn(<%=k %>)" /> <input class="fill"
							name="autoLine_0" type="button" value="0"
							onclick="remplirZero(<%=k %>)" /></td> <% } } %>
					</c:if>
				</tr>
				<%
					k++;
						}
					}
				%>
				
				<tr class="total" id="total">
					<td>total</td>
					<td id="totalJ"><%=total_jours%></td>

					<c:if test="${!sessionScope.sessionUser.isRh()}">
						<%
						if ((listeCalendrier.size() == 0)||((!listeCalendrier.isEmpty())&&(listeCalendrier.get(0).getStatus() < 1))) {
						%><td></td>
						<%
					}%>
					</c:if>
				</tr>
				
			</tbody>
			
		</table>
		</div>
		<div id="nonScrolableT">
		<table class="table" id="affichage">

			<thead>

				<tr>

					<%	for (int i = 1; i <= days; i++) {
							if (i == iTDay && iMonth==iTMonth && iYear==iTYear) {
					%>
					<th class="today" id="day_<%=i%>"><span>
							<%if(i<10){%>0<%} %><%=i%></span></th>
					<%
						} else {
								if (list.contains(i)) {
					%>
					<th class="weekend" id="day_<%=i%>"><span>
							<%if(i<10){%>0<%} %><%=i%></span></th>
					<%
						} else {
					%>
					<th align="center" id="day_<%=i%>"><span>
							<%if(i<10){%>0<%} %><%=i%></span></th>
					<%
						}

							}

						}
					%>


				</tr>
			</thead>
			<tbody>
				<%
					if (menu.size() != 0) {
						int k = 1;
						for (Projet elt : menu) {
				%>
				<tr class="odd" id="projet_<%=k%>">

					<%
						for (int i = 1; i <= days; i++) {
									if (list.contains(i)) {
					%>
					<td class="weekend">&nbsp;</td>
					<%
						} else {

										if (i == iTDay) {
					%>
					<td class="today" id="tJour">
					
					<%
								if ((listeCalendrier.size() != 0)
															&& (k <= listeCalendrier.size())) {
									if ((Constants.getIdProjetsNonFactures().contains(listeCalendrier.get(k - 1).getId_projet().intValue())) 
											&& (listeCalendrier.get(k - 1).getJours().get(i - 1) != 0.0F)) {%> 
										<select enabled id="projet_<%=i%>_<%=k%>"
										name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
											<%} else { %>
												<select id="projet_<%=i%>_<%=k%>"
										name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
											<%}
							%>
							<option value="0">--</option>

							<option value="0.0"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.0F) {%>
								selected="selected" <%}%>>0</option>
							<option value="0.5"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.5F) {%>
								selected="selected" <%}%>>0.5</option>
							<option value="1"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 1.0F) {%>
								selected="selected" <%}%>>1</option>
							<%
								} else {
							%>
							<select id="projet_<%=i%>_<%=k%>"
										name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
							<option value="0">--</option>
							<option value="0.0">0</option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
							<%
								}
							%>
					</select></td>
					<%
						} else {
					%>
					<td><%
								if ((listeCalendrier.size() != 0)
															&& (k <= listeCalendrier.size())) {
									if ((Constants.getIdProjetsNonFactures().contains(listeCalendrier.get(k - 1).getId_projet().intValue())) 
											&& (listeCalendrier.get(k - 1).getJours().get(i - 1) != 0.0F)) {%> 
										<select enabled id="projet_<%=i%>_<%=k%>"
										name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
											<%} else { %>
												<select id="projet_<%=i%>_<%=k%>"
										name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
											<%}
							%>
							<option value="0">--</option>
							<option value="0.0"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.0F) {%>
								selected="selected" <%}%>>0</option>
							<option value="0.5"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.5F) {%>
								selected="selected" <%}%>>0.5</option>
							<option value="1"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 1.0F) {%>
								selected="selected" <%}%>>1</option>
							<%
								} else {
							%>
							<select id="projet_<%=i%>_<%=k%>"
										name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
							<option value="0">--</option>
							<option value="0.0">0</option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
							<%
								}
							%>
					</select></td>
					<%
						}
									}
								}
					%>
				</tr>
				<%
					k++;
						}
					}
				%>


				<tr class="total" id="total">
					<%for (int i = 1; i <= days; i++) {
							if (list.contains(i)) {
					%>
					<td class="weekend">&nbsp;</td>
					<%
						} else {
					%>
					<%
						if (total.size() != 0) {
					%>
					<td <%if (total.get(i - 1) == 1) {%> class="totalgood" <%} else {%>
						class="totalbad" <%}%> id="td_total_<%=i%>"><span
						id="total_<%=i%>"><%=total.get(i - 1)%> </span></td>
					<%
						} else {
					%>
					<td class="totalbad" id="td_total_<%=i%>"><span
						id="total_<%=i%>">0.0 </span></td>
					<%
						}
					%>
					<%
						}

						}
					%>
				</tr>


			</tbody>
		</table>
		</div>
	</div>
	<c:if test="${!sessionScope.sessionUser.isRh()}">
		<div id="corps" class="box">

			<%
			if ((listeCalendrier.size() == 0)||((!listeCalendrier.isEmpty())&&(listeCalendrier.get(0).getStatus() < 1))) {
		    /* ||((listeCalendrier.get(0).getStatus() == 1)&&(iTDay<=20))) { */				
		%>
			<input id="sauver" name="sauver" type="submit" value="Sauver"/> <input
				id="soumettre" name="soumettre" type="button" value="Soumettre"
				onclick="setStatus();" />
			<%
			} 
		%>

		</div>
	</c:if>
</form>












<!-- 
<div>

	<button name="button1">+</button>
</div> -->



