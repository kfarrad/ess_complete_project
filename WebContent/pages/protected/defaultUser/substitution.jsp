<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<c:set var="title" value="Valider le jour de travail" />
<c:set var="active" value="substitution" />
<%@ include file="../../templates/header.jsp"%>
<c:if test="${!empty message }">
	<div id="slickbox">
		<div class="successs">
			<p>${message}</p>
		</div>
	</div>
</c:if>

<%
	UserDaoImpl utilisateurDao =(UserDaoImpl)(request.getAttribute("utilisateurDao"));
	List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
	List<Utilisateur> utilisateurs =utilisateurDao.lister();
	int i=0;
	
	// listons uniquement les utilisateur de role user
	for(Utilisateur elt : utilisateurs){
		 if(elt.isUser() && elt.isActif()) listeUtilisateurs.add(i++,elt);
	}
	Long id_user = (Long)request.getAttribute("id");
	Long idUser=0L;
	if(id_user!=null){
		idUser= id_user;
	}

    if (!listeUtilisateurs.isEmpty()) {
		/* if (idUser == 0)
		    idUser = listeUtilisateurs.get(0).getId_profil(); */
		Utilisateur user = utilisateurDao.trouver(idUser);
		
		
		
		
    }
%>
<form action="<c:url value="/substitution"></c:url>" method="post">
<div id="divSelectSalarier" class="TabDyn box">
	<span> Selection salarié: <select id="id" name="id">
		<option value="">Veuillez selectionner un utilisateur</option>
			<%
				if (listeUtilisateurs != null) {
					if (listeUtilisateurs.size() != 0) {
						for (Utilisateur userElt : listeUtilisateurs) {
							UserProfil profil = userElt.getProfil();
			%>
			<option value="<%=userElt.getId_profil()%>"
				<%if (userElt.getId_profil() == idUser) {%> selected="selected"
				<%}%>>
				<%=profil.getNom()%>
				<%
					if (profil.getPrenom() != null)
				%><%=profil.getPrenom()%></option>

			<%
				}
					}
				}
			%>
	</select>
	</span>
	<input type="submit" name="valider" id="valider" value="Valider" />
</div>

</form>

<%@ include file="../../templates/footer.jsp"%>