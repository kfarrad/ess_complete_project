<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>C.V</title>
<link rel="stylesheet" href="<c:url value="/resources/css/cv.css"/> " />
</head>
<body>
	<!-- <div class="container"> -->
	<c:choose>
		<c:when test="${ empty resume }">
			<span> PAS DE C.V ENREGISTRE</span>
		</c:when>
		<c:otherwise>
		<c:set scope="session" var="titreCV" value="${resume.titre}"></c:set>
		<c:set scope="session" var="workExperiences" value="${resume.experiences}"></c:set>
		<c:set scope="session" var="formations" value="${resume.formations}"></c:set>
		<c:set scope="session" var="langues" value="${resume.langues}"></c:set>
		<c:set scope="session" var="competences" value="${resume.competences}"></c:set>
			<div class="footer" id="pageFooter">
				<span class="titre"> CLINKAST </span><br /> <span>32 rue de
					la République 92190 MEUDON</span><br /> <span>Votre contact :
					Stéphane KAMGAING</span><br /> <span> <b>+33 1 46 31 44 25</b></span>
			</div>
			<div class="header" id="pageHeader">
				<table>
					<tr>
						<td class="logo"><img
							src="<c:url  value="/resources/images/logo_clinkast.png" />"
							alt="activer" /></td>
						<td class="entete"><c:if test="${!empty titreCV }">

								<span class="titre">${titreCV.initiales}- <c:out
										value="${titreCV.nom}" /></span>
								<br />

								<b class="subtitle"><c:out value="${titreCV.sous_titre}" /></b>

							</c:if></td>
					</tr>
				</table>
			</div>
			<div class="footerLink">
				<span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><a
					class="lien" href="www.clinkast.fr">www.clinkast.fr</a>
			</div>
			<div class="format">

				<div id="education">
					<span class="titre"> Formation </span>
					<div class="box">
						<table>
							<c:choose>
								<c:when test="${ empty formations }">
									<tr></tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${ formations }" var="formation"
										varStatus="boucle">
										<tr>
											<td class="colOne">${formation.value.annee}:</td>
											<td class="colTwo"><b>${ formation.value.institut }</b>
												${ formation.value.specialisation }<%-- <b>${ formation.value.moyenne }</b> --%></td>
										</tr>

									</c:forEach>
								</c:otherwise>
							</c:choose>
						</table>
					</div>
				</div>

				<div id="experience">
					<span class="titre"> Expériences Professionnelles </span>
					<div class="box">
						<table>
							<c:choose>
								<c:when test="${ empty workExperiences }">
									<tr></tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${ workExperiences }" var="workExp"
										varStatus="boucle">
										<tr>
											<td class="colOne" rowspan="2"><span>${workExp.value.date_debut}
													à </span> <span>${workExp.value.date_fin} :</span></td>

											<td class="colTwo"><b>${workExp.value.titre_poste}</b>
												chez <b>${workExp.value.entreprise}</b><br /> ${ workExp.value.description }
											</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</table>
					</div>
				</div>

				<div id="competences">
					<span class="titre"> Compétences </span>
					<div class="box">
						<table>
							<c:choose>
								<c:when test="${ empty competences }">
									<tr>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${ competences }" var="competence"
										varStatus="boucle">

										<tr>
											<td class="colOne">${ competence.value.nom }:</td>
											<td class="colTwo">${ competence.value.liste }</td>

										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</table>
					</div>
				</div>
				
				<div id="langues">
					<span class="titre"> Langue </span>
					<div class="box">
						<table>
							<c:choose>
								<c:when test="${ empty langues }">
									<tr>
										<td>Aucun résultat</td>
										<td></td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${ langues }" var="langue" varStatus="boucle">
										<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
											<td>${ langue.value.nom }:</td>
											<td>${ langue.value.aisance }</td>

										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</table>
					</div>
				</div>


			</div>
			<!-- </div> -->
		</c:otherwise>
	</c:choose>
</body>
</html>