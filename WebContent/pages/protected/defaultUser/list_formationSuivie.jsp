<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Section activit�s -->
<form id = "formWorkFormationsSuiviesDelete" action="<c:url value="/autoEvaluation"><c:param name="formType" value="FMS"/></c:url>" method="post" novalidate = "novalidate">
	<input type="hidden" name="formationSuivies[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
	<input type="hidden" name="champs[action]" id = "formationsSuivies_delete" value="delete"/>
	<table class="table">
			<thead>
				<tr>
					<th><input type="checkbox" id = "allFormationSuivies"></th>
					<th>Intitule de la formation</th>
					<th>Commentaire sur l'objectif</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty gestionEvaluations.autoEvaluations.formationsSuivieses}">
						<tr>
							<td colspan="3" class="name">Aucune donn�e</td>
						</tr>
					</c:when>
					<c:otherwise>
						<%-- Parcours le tableau et affiche la liste des activit�s  --%>
					
					<c:forEach items="${gestionEvaluations.autoEvaluations.formationsSuivieses}" var="mapFormationSuivie" varStatus="boucle">
					<tr class="dataFms">
						<td><input type="checkbox" name="formation_id_formations" value="${mapFormationSuivie.formationsSuiviesId}"></td>
						<td><c:out value="${mapFormationSuivie.formationsSuiviesIntitule}" /></td>
						<td><c:out value="${mapFormationSuivie.formationsSuiviesCommentaires }" /></td>
					</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	   </form>
				