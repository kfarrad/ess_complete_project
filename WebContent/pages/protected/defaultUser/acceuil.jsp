<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Acceuil" />
<c:set var="active" value="acceuil" />
<%@ include file="../../templates/header.jsp"%>

<c:import url="/pages/templates/menu_acceuil_user.jsp" />

<%@ include file="../../templates/footer.jsp"%>