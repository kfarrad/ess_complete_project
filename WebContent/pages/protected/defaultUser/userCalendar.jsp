<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	Calendar ca = new GregorianCalendar();
 int iTDay=ca.get(Calendar.DATE);
 int iTYear=ca.get(Calendar.YEAR);
 int iTMonth=ca.get(Calendar.MONTH);
 int iMonth=-1;
 int iYear=-1;
 
 try {
	 iYear= ( Integer)request.getAttribute("annee");
	 iMonth= ( Integer)request.getAttribute("mois");
 } catch (Exception e) {

	 e.printStackTrace();
 }
 if(iYear==-1)
 {
	  iYear=iTYear;
	  
 }
 if(iMonth==-1)
 {
	 iMonth=iTMonth;
 } 
%>
<%
 GregorianCalendar cal = new GregorianCalendar (iYear, iMonth, 1); 

 int days=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
 int weekStartDay=cal.get(Calendar.DAY_OF_WEEK); 
 cal = new GregorianCalendar (iYear, iMonth, days); 
 
 int iTotalweeks=cal.get(Calendar.WEEK_OF_MONTH);
 ArrayList<Integer> list = new ArrayList<Integer>();
 List<Projet> menu = new ArrayList<Projet>();
 List<Float> total = new ArrayList<Float>();
 List<Calendrier> listeCalendrier = new ArrayList<Calendrier>();
 String id_user = request.getParameter("id"); 
 Long idUser=0L;
 if(id_user!=null) idUser=Long.parseLong(id_user);
 else{
	 Long id_user2 = (Long)request.getAttribute("id");
	 if(id_user2!=null){
			idUser= id_user2;
		}
 }
 Utilisateur utilisateur = (Utilisateur)session.getAttribute(Constants.ATT_SESSION_USER);
 Double total_jours =0.0;
	 try {
		 menu = ( List<Projet>)request.getAttribute(Constants.ATT_PROJETS);		 
		 listeCalendrier = (List<Calendrier>)request.getAttribute(Constants.ATT_CALENDRIER);
		 if (listeCalendrier!=null){			 
			 if( listeCalendrier.size() != 0) {
				 for (int j = 0; j < 31; j++) {
				 	Float sum=0.0F;
			 		for(int i=0; i<listeCalendrier.size();i++){
						sum+=listeCalendrier.get(i).getJours().get(j);
			 		}
			 		total.add(j, sum);
				 }
			 	 for(int i=0; i<listeCalendrier.size();i++){
			 	 total_jours+=listeCalendrier.get(i).getTotal_jour();
			 	}
			}
		}

		 //traitement d'une mauvaise reception
		 if(menu==null)	 menu = new ArrayList<Projet>();
		 if(listeCalendrier==null)listeCalendrier = new ArrayList<Calendrier>();
		 
		 
	 } catch (Exception e) {
		 e.printStackTrace();
	 }
	 
	 
	 
 cal = new GregorianCalendar (iYear, iMonth, 1); 
 do {
	    // get the day of the week for the current day
	    int day = cal.get(Calendar.DAY_OF_WEEK);
	    // check if it is a Saturday or Sunday
	    if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
	        // print the day - but you could add them to a list or whatever
	        list.add(cal.get(Calendar.DAY_OF_MONTH));	   }
	    // advance to the next day
	    cal.add(Calendar.DAY_OF_YEAR, 1);
	}  while (cal.get(Calendar.MONTH) == iMonth);
%>

<script>
function goTo() {
	$('table[id="affichage"]').hide();
	$("#sauver").hide();
	$("#soumettre").hide();
	$("#status").text("");
}
function setStatus(){
	 document.frm.isvalid.value="true";
	 document.frm.submit();
	
}
function disableTab(){
	$("#affichage").find("select").each(function() {
		$(this).attr("disabled", "disabled");
		$('input[id="sauver"]').hide();
		$('input[id="soumettre"]').hide();
	});
}

function buttonSubmit(){
	 var m=0;
	 var valid = 0;
	while(m<=<%=days%>){
		if($("#td_total_"+m).attr("class")!="totalbad") valid++;
		m++;
	}
	if(valid==<%=days%>+1) 	{
		$('input[id="soumettre"]').show();
		$('input[id="sauver"]').hide();
	}
		
	else  {$('input[id="soumettre"]').hide();
	$('input[id="sauver"]').show();
	}
}

function calculTotal(col,lig){
	var totalId ="#total_"+col;
	var totalJour="#jour_projet_"+lig;
	var tdTotalId ="#td_total_"+col;
	var sommeCol = 0.0;
	var sommeLig = 0.0;
	
	//Calcul somme sur une colonne d'une journee
	for (var i = 1; i < id+<%=menu.size()%>; i++) {
		var chaineCol="#projet_"+col+"_"+i;
		if($(chaineCol).val()!=null){
		sommeCol += parseFloat($(chaineCol).val());
		}
		
	}		
	//Calcul somme sur une ligne d'un projet
	for (var j = 1; j <= <%=days%>; j++) {
		var chaineLig="#projet_"+j+"_"+lig;
		if($(chaineLig).val()!=null){
			sommeLig += parseFloat($(chaineLig).val());
		}
	
	}
	
	//jeu de couleur sur le resultat
	if(sommeCol==1.0) {
		$(tdTotalId).removeClass('totalbad');
		$(tdTotalId).addClass('totalgood');
	}
	else{
		$(tdTotalId).removeClass('totalgood');
		$(tdTotalId).addClass('totalbad');
		
	}
	
	//modification du total final
	$(totalId).text(sommeCol);
	$(totalJour).text(sommeLig);
	
	//total jours
	sommeCol = 0;
	for (var i = 1; i < id+<%=menu.size()%>; i++) {
		var chaine="#jour_projet_"+i;
		if($(chaine).val()!=null){
			sommeCol += parseFloat($(chaine).text());
		}
		//alert (i);
	}
	$("#totalJ").text(sommeCol);
	
	//affichage du bouton de validation finale
	buttonSubmit();
	
}
	// variable globale
	var id = 1;
	
	jQuery(document).ready(function() {
		$('tr[id="projet"]').hide();		
		$('input[id="soumettre"]').hide();
		//affichage du bouton de validation finale
		buttonSubmit();
		
		
		<%if(utilisateur.isRh()){ %> disableTab(); 
		$('input[id="sauver"]').hide(); 
		<%}
		
		if (listeCalendrier!=null){ if( listeCalendrier.size() != 0) {
			
			if (((listeCalendrier.get(0).getStatus() == 2))||(listeCalendrier.get(0).getStatus() == 1)&&(iTDay>20 )) {%>
				disableTab();
				<%}
		}}%>
		
		 
		$('button[name="button"]').click(function(){
			<%if(utilisateur!=null){ %>
			$('table[id="affichage"]').show();
			<% if(utilisateur.isUser()||(idUser==0L)){ %>
			document.location.href="userCalendar?annee="+$("#annee").val()+"&mois="+$("#mois").val();
			  <%}else { %>
			 document.location.href="userCalendar?id=<%=idUser%>&annee="+$("#annee").val()+"&mois="+$("#mois").val();
			<% }
			}
			%>	
			 return false;
		});
			
		$('button[name="button1"]').click(function() {

			var tr = $('tr[id="projet"]');
			var myClone = tr.clone();
			var indexJ = id + <%=menu.size()%>
			
			myClone.find('span[id="jour_projet"]').each(function() {
				 var mySelectId = $(this).attr("id");
				 $(this).attr("id", mySelectId+"_"+indexJ);
					 
		        });
			 myClone.find("select").each(function() {
				 var mySelectId = $(this).attr("id");
				 $(this).attr("id", mySelectId+"_"+indexJ);
				 $(this).attr("name", mySelectId+"_"+indexJ);
				 var myOnChange = $(this).attr("onchange");
				 if(mySelectId!="projetId") $(this).attr("onchange",myOnChange+indexJ+");");
				 	 
		        });
			 varTrG="<tr id="+"projet_"+indexJ+" >";
			 var r=$("#test").length;
			//$('tbody > tr').append(varTrG+myClone.html()+"</tr>");
			$('tbody > tr').eq( r-2 ).after(varTrG+myClone.html()+"</tr>");
			
			//var target = document.getElementById("total");
		   // var newElement = document.createElement('tr');
		   // target.parentNode.insertBefore(varTrG+myClone.html()+"</tr>", target);
			id++;
		});
	});
	
	
	$("#dynamic").scroll(function(){
		  $("#divSelect").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  $("#divSelectSalarier").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  
			
		});
</script>

<form name="frm" method="post"
	action="<c:url value="/userCalendar"></c:url>">
	<input type="hidden" name="isvalid" value="0">
	<div id="divSelect" class="TabDyn">
		<table class="table" id="selection">
			<tbody>
				<tr class="odd">
					<td>Year</td>
					<td><select id="annee" name="annee" onchange="goTo()">
							<%
								// start year and end year in combo box to change year in calendar
								for (int iy = iTYear - 20; iy <= iTYear + 20; iy++) {
									if (iy == iYear) {
							%>
							<option value="<%=iy%>" selected="selected"><%=iy%></option>
							<%
								} else {
							%>
							<option value="<%=iy%>"><%=iy%></option>
							<%
								}
								}
							%>
					</select></td>
					<td align="center"><h3>
							<%
								Calendar calend = Calendar.getInstance();
								calend.set(Calendar.YEAR, 2008);
								calend.set(Calendar.MONTH, iMonth);
								calend.set(Calendar.DAY_OF_MONTH, 01);
							%>
							<%=new SimpleDateFormat("MMMM").format(calend.getTime())%>
							<%=iYear%></h3></td>
					<td>Month</td>
					<td><select id="mois" name="mois" onchange="goTo()">
							<%
								// print month in combo box to change month in calendar
								for (int im = 0; im <= 11; im++) {
									if (im == iMonth) {
										calend.set(Calendar.MONTH, im);
							%>
							<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
							<%
								} else {
										calend.set(Calendar.MONTH, im);
							%>
							<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
							<%
								}
								}
							%>
					</select></td>
					<td>
						<button name="button">afficher</button>
					</td>
					<td><h2>
							Statut:
							<%
						if (listeCalendrier.size() == 0) {
							%>
							<span id="status" class="nonSoumis"> Non soumis </span>
							<%
								} else {
									if (listeCalendrier.get(0).getStatus() == 0) {
							%><span id="status" class="soumis"> soumis </span>
							<%
								} else {
										if (listeCalendrier.get(0).getStatus() == 1) {
							%><span id="status" class="traitement"> En traitement </span>
							<%
								} else {
							%><span id="status" class="approuve"> Approuve </span>
							<%
								}
									}
								}
							%>
						</h2></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="TabDyn">

		<table class="table" id="affichage">

			<thead>

				<tr>
					<th>cra</th>
					<th align="center"><span>Jours</span></th>

					<%
						for (int i = 1; i <= days; i++) {
							if (i == iTDay) {
					%>
					<th style="width: 100px;" class="today" id="day_<%=i%>"><span>
							<%if(i<10){%>0<%} %><%=i%></span></th>
					<%
						} else {
								if (list.contains(i)) {
					%>
					<th style="width: 100px;" class="weekend" id="day_<%=i%>"><span>
							<%if(i<10){%>0<%} %><%=i%></span></th>
					<%
						} else {
					%>
					<th style="width: 100px;" align="center" id="day_<%=i%>"><span>
							<%if(i<10){%>0<%} %><%=i%></span></th>
					<%
						}

							}

						}
					%>


				</tr>
			</thead>
			<tbody>
				<%
					if (menu.size() != 0) {
						int k = 1;
						for (Projet elt : menu) {
				%>
				<tr class="odd" id="projet_<%=k%>">
					<td><span id="projetId_<%=k%>"><%=elt.getNom()%></span></td>
					<td align="center"><span id="jour_projet_<%=k%>"> <%
 	if ((listeCalendrier.size() != 0)
 					&& (k <= listeCalendrier.size())) {
 %> <%=listeCalendrier.get(k - 1).getTotal_jour()%> <%
 	} else {
 %> 0.0 <%
 	}
 %>
					</span></td>

					<%
						for (int i = 1; i <= days; i++) {
									if (list.contains(i)) {
					%>
					<td class="weekend" align="center">&nbsp;</td>
					<%
						} else {

										if (i == iTDay) {
					%>
					<td class="today" align="center" id="tJour"><select
						id="projet_<%=i%>_<%=k%>" name="projet_<%=i%>_<%=k%>"
						onchange="calculTotal(<%=i%>,<%=k%>);">
							<%
								if ((listeCalendrier.size() != 0)
															&& (k <= listeCalendrier.size())) {
							%>
							<option value="0.0">--</option>

							<option value="0.0"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.0F) {%>
								selected="selected" <%}%>>0</option>
							<option value="0.5"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.5F) {%>
								selected="selected" <%}%>>0.5</option>
							<option value="1"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 1.0F) {%>
								selected="selected" <%}%>>1</option>
							<%
								} else {
							%>
							<option value="0.0">--</option>
							<option value="0.0">0</option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
							<%
								}
							%>
					</select></td>
					<%
						} else {
					%>
					<td align="center"><select id="projet_<%=i%>_<%=k%>"
						name="projet_<%=i%>_<%=k%>" onchange="calculTotal(<%=i%>,<%=k%>);">
							<%
								if ((listeCalendrier.size() != 0)
															&& (k <= listeCalendrier.size())) {
							%>
							<option value="0.0">--</option>
							<option value="0.0"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.0F) {%>
								selected="selected" <%}%>>0</option>
							<option value="0.5"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 0.5F) {%>
								selected="selected" <%}%>>0.5</option>
							<option value="1"
								<%if (listeCalendrier.get(k - 1).getJours()
											.get(i - 1) == 1.0F) {%>
								selected="selected" <%}%>>1</option>
							<%
								} else {
							%>
							<option value="0.0">--</option>
							<option value="0.0">0</option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
							<%
								}
							%>
					</select></td>
					<%
						}
									}
								}
					%>
				</tr>
				<%
					k++;
						}
					}
				%>

				<tr class="odd" id="projet">
					<td align="center"><select id="projetId">
							<option value="">--</option>
							<option value="CRA">CRA</option>
							<option value="Android">Android</option>
							<option value="Site">Site</option>
							<option value="Java">Java</option>
					</select></td>
					<td align="center"><span id="jour_projet">0</span></td>

					<%
						for (int i = 1; i <= days; i++) {
							if (list.contains(i)) {
					%>
					<td class="weekend" align="center">&nbsp;</td>
					<%
						} else {
								if (i == iTDay) {
					%>
					<td class="today" align="center"><select id="projet_<%=i%>"
						onchange="calculTotal(<%=i%>,">
							<option value="0.0">--</option>
							<option value="0">0</option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
					</select></td>
					<%
						} else {
					%>
					<td align="center"><select id="projet_<%=i%>"
						onchange="calculTotal(<%=i%>,">
							<option value="0.0">--</option>
							<option value="0">0</option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
					</select></td>
					<%
						}
							}
						}
					%>
				</tr>

				<tr class="total" id="total">
					<td align="center">total</td>
					<td align="center" id="totalJ"><%=total_jours%></td>
					<%
						for (int i = 1; i <= days; i++) {
							if (list.contains(i)) {
					%>
					<td class="weekend" align="center">&nbsp;</td>
					<%
						} else {
					%>
					<%
						if (total.size() != 0) {
					%>
					<td align="center" <%if (total.get(i - 1) == 1) {%>
						class="totalgood" <%} else {%> class="totalbad" <%}%>
						id="td_total_<%=i%>"><span id="total_<%=i%>"><%=total.get(i - 1)%>
					</span></td>
					<%
						} else {
					%>
					<td align="center" class="totalbad" id="td_total_<%=i%>"><span
						id="total_<%=i%>">0.0 </span></td>
					<%
						}
					%>
					<%
						}

						}
					%>
				</tr>


			</tbody>



		</table>
	</div>
	<!-- <div class="TabDyn">
		<fieldset>
		 <p>Legende: <p>
		 <p>CP*: CONGES PAYE<p>
		 <p>RTT*: REPOS<p>
		 <p>CSS*: CONGES SANS SOLDE<p>
		 
		 </fieldset>
	</div> -->
	<div id="corps">

		<%
			if (listeCalendrier.size() == 0) {
		%>
		<input id="sauver" name="sauver" type="submit" value="Sauver" />
		<%
			} else {
				if ((listeCalendrier.get(0).getStatus() == 0)||((listeCalendrier.get(0).getStatus() == 1)&&(iTDay<=20))) {
		%>
		<input id="sauver" name="sauver" type="submit" value="Sauver" /> <input
			id="soumettre" name="soumettre" type="button" value="Soumettre"
			onclick="setStatus();" />
		<%
			}
			}
		%>
	</div>
</form>












<!-- 
<div>

	<button name="button1">+</button>
</div> -->



