<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="changeFormationSuivies" style="display: none;">
<div class="head">
	<h1 id="headChangeFormationSuivies">Ajouter une formation suivie</h1>
 </div>
 <div class="inner">
<form id = "formWorkFormationSuivies" action="<c:url value="/autoEvaluation"><c:param name="formType" value="FMS"/></c:url>" method="post" novalidate = "novalidate">
<input type="hidden" name="formationSuivies[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
<input type="hidden" name="formationSuivies[id_formationSuivie]" id="formationSuivies_id_formationSuivie" value="0"/>
<input type="hidden" name="champs[action]" id = "formationSuivies_sauve" value="sauve"/>
<fieldset>
<ul>
	<li>
	 	<label for="formationSuivies_intitule">Intitulé de la formation <em>*</em></label>
		<input id="formationSuivies_intitule" type="text" name="formationSuivies[intitule]">
	</li>
	<li class="largeTextBox">
	 	<label for="formationSuivies_commanetaire">Commentaires sur l'attente des objectifs<em>*</em></label>
		<textarea rows="10" cols="28" name="formationSuivies[commanetaire]" class="formInputText" id="formationSuivies_commanetaire"></textarea>
	</li>
	
	</ul>
	<p>
		<input type="button" class="" id="btnWorkFormationSuiviesSauve" value="Sauver">
		<input type="button" class="reset" id="btnWorkFormationSuiviesCancel" value="Annuler">
	</p>
	</fieldset>	
</form>
</div>
</div>