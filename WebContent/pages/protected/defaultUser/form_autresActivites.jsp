<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul>
	<li class="largeTextBox">
	 	<label for="autresActivites_autesMoyens">Autres moyens:</label>
		<textarea rows="4" cols="28" name="autresActivites[autesMoyens]" class="formInputText" id="autresActivites_autesMoyens"></textarea>
	</li>
	<li class="largeTextBox">
	 	<label for="autresActivites_elementsSignificatifs">Eléments de contexte significatifs:</label>
		<textarea rows="4" cols="28" name="autresActivites[elementsSignificatifs]" class="formInputText" id="autresActivites[elementsSignificatifs]"></textarea>
	</li>
	<li class="largeTextBox">
	 	<label for="autresActivites_bilanClinkastAttentes">Par rapport à ce que Clinkast attendait de moi, quel bilan fais-je de cette année ?</label>
		<textarea rows="4" cols="28" name="autresActivites[bilanClinkastAttentes]" class="formInputText" id="autresActivites[bilanClinkastAttentes]"></textarea>
	</li>
	<li class="largeTextBox">
	 	<label for="autresActivites_bilanMesAttentes">Par rapport à ce que j’attendais de Clinkast, quel bilan fais-je de cette année ?</label>
		<textarea rows="4" cols="28" name="autresActivites[bilanMesAttentes]" class="formInputText" id="autresActivites[bilanMesAttentes]"></textarea>
	</li>
	
	</ul>
	<p>
		<input type="button" class="" id="btnWorkFormationSauve" value="Sauver">
		<input type="button" class="reset" id="btnWorkFormationCancel" value="Annuler">
	</p>	
