<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="changeObjectifs" style="display: none;">
	<div class="head">
		<h1 id="headChangeObjectifs">Ajouter une formation suivie</h1>
	</div>
	<div class="inner">
		<form id = "formWorkObjectifs" action="<c:url value="/autoEvaluation"><c:param name="formType" value="OBJ"/></c:url>" method="post" novalidate = "novalidate">
			<input type="hidden" name="objectifs[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
			<input type="hidden" name="objectifs[id_objectif]" id="objectifs_id_objectif" value="0"/>
			<input type="hidden" name="champs[action]" id = "objectifs_sauve" value="sauve"/>
			<fieldset>
				<ul>
					<li>
		 				<label for="objectif_name">Objectif suivi</label>
						<input id="objectif_name" type="text" name="objectif[name]">
					<li>
					<li><label for="objectif_estimation">Estimation</label>
						<select name="objectif[estimation]" id="objectif_estimation">
							<option value="" selected>J'estime ...</option>
							<option value="Dépassé">Dépassé</option>
							<option value="Atteint">Atteint</option>
							<option value="Partiellement atteint">Partiellement</option>
							<option value="Pas réalisé">Pas réalisé</option>
						</select>
					</li>
				</ul>
				<p>
					<input type="button" class="" id="btnWorkObjectifSauve" value="Sauver">
					<input type="button" class="reset" id="btnWorkObjectifCancel" value="Annuler">
				</p>	
			</fieldset>	
		</form>
	</div>
</div>