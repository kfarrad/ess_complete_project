<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Rédiger votre C.V." />
<c:set var="active" value="Complete your resume" />
<%@ include file="../../templates/header.jsp"%>

<style>
.ui-datepicker-calendar {
	/*display: none; */
	
}
</style>
<script type="text/javascript">


var fileModified = 0;
var lang_addWorkExperience = "Ajouter une expérience professionnelle";
var lang_editWorkExperience = "Modifier l'expérience professionnelle";
var lang_companyRequired = "Requis";
var lang_jobTitleRequired = "Requis";
var lang_invalidDate = 'Doit être une date valide au format mm/yy';
var lang_commentLength = "Doit être inférieur à niveau %montant%";
var lang_fromDateLessToDate = "À ce jour, devrait avoir lieu après la date de";
var lang_selectWrkExprToDelete = "Sélectionnez les enregistrements à supprimer";
var lang_jobTitleMaxLength = "Doit être inférieur à niveau %montant%";
var lang_companyMaxLength = "Doit être inférieur à niveau %montant%";
var canEdit = '1';

var datepickerDateFormat = 'mm/yy';
var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

	
$(document).ready(function(){
		$('.monthpicker').monthpicker();
		if($("#title_name").val()!='') $("#btnTitleSave").val("Modifier");
		$("#btnTitleSave").click(function() {
	        clearMessageBar();
	        $("#saveTitle").val("true"); 
	        $("#frmTitle").submit();
	    });
		
		$("#btnTitleCancel").click(function() {
			 $("#changeTitle").hide();
		});
		
		$("#editTitle").click(function() {
			$("#changeTitle").show();
		});
		
		 var TitleValidator =
			 $("#frmTitle").validate({
		     rules: {
		         'title[name]': {
		             required: true,
		             maxlength: 100
		         },
		         'title[sub]': {
		             required: true,
		             maxlength: 100
		         }		        
		     },
		     messages: {
		         'title[name]': {
		             required: lang_companyRequired,
		             maxlength: lang_companyMaxLength
		         },
		         'title[sub]': {
		             required: lang_jobTitleRequired,
		             maxlength: lang_jobTitleMaxLength
		         }
		     }
		 });
}); 
			
</script>
<div class="box pimPane">

	<!-- this is title section -->
	<div class="module">
		<div id="changeTitle" style="display: none;">
			<div class="head">
				<h1 id="headChangeTitle">Modifier Titre</h1>
			</div>

			<div class="inner">
				<form id="frmTitle"
					action="<c:url value="/resume">
			<c:param name="title" value="true" /></c:url>"
					method="post" novalidate="novalidate">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="title[id]" id="title_id" value="<c:out value="${resume.titre.id}"/>" > <input
						type="hidden" name="saveTitle" id="saveTitle" value="0" /> <input
						type="hidden" name="title[initial]"
						value="<c:out value="${resume.titre.initiales}"/>" />
					<fieldset>
						<ul>
							<li><label for="title_initial">Initiales <em>*</em></label>
								<input type="text" value="<c:out value="${resume.titre.initiales}"/>"
								class="formInputText" maxlength="100" id="title_initial"
								disabled="disabled" /></li>
							<li><label for="title_name">Entete <em>*</em></label> <input
								type="text" name="title[name]"
								value="<c:out value="${resume.titre.nom}"/>"
								class="formInputText" maxlength="100" id="title_name" /></li>
							<li><label for="title_sub">Sous-Titre <em>*</em></label> <input
								type="text" name="title[sub]"
								value="<c:out value="${resume.titre.sous_titre}"/>"
								class="formInputText" maxlength="100" id="title_sub" /></li>

							<li class="required"><em>*</em> Required field</li>
						</ul>
						<p>
							<input type="button" class="" id="btnTitleSave" value="Sauver">
							<input type="button" class="" id="btnTitleCancel" value="Annuler">
						</p>
					</fieldset>
				</form>

			</div>
		</div>
		<!-- changeTitle  -->
		<div class="miniList" id="sectionTitle">

			<div class="head">
				<h1>Titre</h1>
			</div>

			<div class="inner">
				<table class="table hover">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Titre</th>
							<th>Sous-titre</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="name"><a id="editTitle" href="#"> <c:out
										value="${ resume.titre.initiales}" />
							</a></td>
							<td><c:out value="${ resume.titre.nom }" /></td>
							<td><c:out value="${ resume.titre.sous_titre }" /></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- this is work experience section -->
	<div class="module">
		<!-- Javascrpit for Experience -->
		<script type="text/javascript"
			src="<c:url value="/resources/javascript/workExp.js" />"></script>
		<!-- End js -->

		<div id="changeWorkExperience" style="display: none;">
			<div class="head">
				<h1 id="headChangeWorkExperience">Ajouter une expérience
					professionnelle</h1>
			</div>

			<div class="inner">
				<form id="frmWorkExperience"
					action="<c:url value="/resume">
			<c:param name="experience" value="true" /></c:url>"
					method="post" novalidate="novalidate">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="experience[id]" id="experience_id"> <input
						type="hidden" name="saveWorkExperience" id="saveWorkExperience"
						value="0" />
					<fieldset>
						<ul>
							<li><label for="experience_employer">Entreprise <em>*</em></label>
								<input type="text" name="experience[employer]"
								class="formInputText" maxlength="100" id="experience_employer" />
							</li>
							<li><label for="experience_jobtitle">Titre du poste
									<em>*</em>
							</label> <input type="text" name="experience[jobtitle]"
								class="formInputText" maxlength="100" id="experience_jobtitle" />
							</li>
							<li><label for="experience_from_date">À partir de</label> <input
								id="experience_from_date" type="text"
								name="experience[from_date]" class="monthpicker" /></li>

							<li><label for="experience_to_date">À</label> <input
								id="experience_to_date" type="text" name="experience[to_date]"
								class="monthpicker" /></li>
							<li class="largeTextBox"><label for="experience_comments">Commenter</label>
								<textarea rows="4" cols="28" name="experience[comments]"
									class="formInputText" id="experience_comments"></textarea></li>
							<li class="required"><em>*</em> Required field</li>
						</ul>
						<p>
							<input type="button" class="" id="btnWorkExpSave" value="Sauver">
							<input type="button" class="reset" id="btnWorkExpCancel"
								value="Annuler">
						</p>
					</fieldset>
				</form>

			</div>
		</div>
		<!-- changeWorkExperience  -->

		<div class="miniList" id="sectionWorkExperience">

			<div class="head">
				<h1>Expérience professionnelle</h1>
			</div>

			<div class="inner">
				<form id="frmDelWorkExperience"
					action="<c:url value="/resume">
				<c:param name="experience" value="true" /></c:url>"
					method="post">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="experience[id]" id="experience_id"> <input
						type="hidden" name="deleteWorkExperience"
						id="deleteWorkExperience" value="0" />
					<p id="actionWorkExperience">
						<input type="button" value="Ajouter" class=""
							id="addWorkExperience">
						<c:if test="${ !empty resume.experiences }">
							<input type="button" value="Effacer" class="delete"
								id="delWorkExperience" />
						</c:if>
					</p>
					<table id="" class="table hover">
						<thead>
							<tr>
								<c:if test="${ !empty resume.experiences }">
									<th class="check" style="width: 2%;"><input
										type="checkbox" id="workCheckAll"></th>
								</c:if>
								<th>Entreprise</th>
								<th>Titre du poste</th>
								<th>À partir de</th>
								<th>À</th>
								<th>Commenter</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${ empty resume.experiences  }">
									<tr>
										<td class="check" style="display: none;"></td>
										<td>Aucun résultat</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${ resume.experiences  }" var="workExp"
										varStatus="boucle">
										<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
											<td class="check"><input type="hidden"
												id="employer_${ workExp.key }"
												value="${workExp.value.entreprise }" /> <input
												type="hidden" id="jobtitle_${ workExp.key }"
												value="${workExp.value.titre_poste }" /> <input
												type="hidden" id="fromDate_${ workExp.key }"
												value="${workExp.value.date_debut }" /> <input
												type="hidden" id="toDate_${ workExp.key }"
												value="${workExp.value.date_fin }" /> <input type="hidden"
												id="comment_${ workExp.key }"
												value="${workExp.value.description }" /> <input
												type="checkbox" class="chkbox1" value="${ workExp.key }"
												name="delWorkExp" /></td>
											<td class="name"><a class="edit" href="#"><c:out
														value="${ workExp.value.entreprise}" /></a></td>
											<td><c:out value="${ workExp.value.titre_poste}" /></td>
											<td><c:out value="${ workExp.value.date_debut}" /></td>
											<td><c:out value="${ workExp.value.date_fin}" /></td>
											<td class="comments"><c:out
													value="${ workExp.value.description }" /></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</form>


			</div>
			<span id="workExpMessagebar"></span>
		</div>
	</div>

	<div class="module">
		<!-- this is education section -->
		<!-- Javascrpit for Education -->
		<script type="text/javascript">
	    //<![CDATA[
	    var lang_addEducation = "Ajouter formation";
	    var lang_editEducation = "Modifier formation";
	    var lang_educationRequired = 'Requis';
	    var lang_selectEducationToDelete = "Sélectionnez les enregistrements à supprimer";
	    var lang_instituteMaxLength = "Doit être inférieur à niveau %montant%";
	    var lang_majorMaxLength = "Doit être inférieur à niveau %montant%";
	    var lang_gpaMaxLength = "Doit être inférieur à niveau %montant%";
	    var lang_yearShouldBeNumber = "Doit être un nombre";
	    //]]>
	</script>


		<script type="text/javascript"
			src="<c:url value="/resources/javascript/education.js" />"></script>
		<!-- End js -->

		<div id="changeEducation" style="display: none;">
			<div class="head">
				<h1 id="headChangeEducation">Ajouter une formation</h1>
			</div>
			<div class="inner">
				<form id="frmEducation"
					action="<c:url value="/resume">
	        		<c:param name="education" value="true" /></c:url>"
					method="post" novalidate="novalidate">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="saveEducation" id="saveEducation" value="0" />
					<input type="hidden" name="education[id]" id="education_id">
					<fieldset>
						<ul>
							<li><label for="education_code">Domaine <em>*</em></label> <input
								type="text" name="education[code]" class="formInputText"
								maxlength="100" id="education_code"> <span> (ex:
									Ecole d'ingénieure)</span></li>
							<li><label for="education_institute">Institut <em>*</em></label>
								<input type="text" name="education[institute]"
								class="formInputText" maxlength="100" id="education_institute">
							</li>
							<li><label for="education_major">Major /
									Spécialisation</label> <input type="text" name="education[major]"
								class="formInputText" maxlength="100" id="education_major">
							</li>
							<li><label for="education_year">Année (ex : 3)</label> <input
								type="text" name="education[year]" class="formInputText"
								maxlength="4" id="education_year"></li>
							<li><label for="education_gpa">GPA / Score</label> <input
								type="text" name="education[gpa]" class="formInputText"
								maxlength="25" id="education_gpa"></li>
							<li><label for="education_start_date">Date de début</label>
								<input id="education_start_date" type="text"
								name="education[start_date]" class="monthpicker"></li>
							<li><label for="education_end_date">Date de fin</label> <input
								id="education_end_date" type="text" name="education[end_date]"
								class="monthpicker"></li>
							<li class="required line"><em>*</em> Required field</li>
						</ul>
						<p>
							<input type="button" class="" id="btnEducationSave"
								value="Sauver"> <input type="button" class="reset"
								id="btnEducationCancel" value="Annuler">
						</p>
					</fieldset>
				</form>
			</div>
		</div>
		<!-- changeEducation -->

		<div class="miniList" id="">
			<div class="head">
				<h1>Formation</h1>
			</div>
			<div class="inner">

				<form id="frmDelEducation"
					action="<c:url value="/resume">
    				<c:param name="education" value="true" /></c:url>"
					method="post">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="education[id]" id="education_id"> <input
						type="hidden" name="deleteEducation" id="deleteEducation"
						value="0" />
					<div id="tblEducation">

						<p id="actionEducation">
							<input type="button" value="Ajouter" class="" id="addEducation">
							<c:if test="${ !empty resume.formations }">
								<input type="button" value="Effacer" class="delete"
									id="delEducation">
							</c:if>
						</p>
						<table class="table hover">
							<thead>
								<tr>
									<c:if test="${ !empty resume.formations }">
										<th class="check" width="2%"><input type="checkbox"
											id="educationCheckAll"></th>
									</c:if>
									<th>Institut</th>
									<th>Domaine</th>
									<th>Année</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${ empty resume.formations}">
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
											<td></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${ resume.formations }" var="formation"
											varStatus="boucle">
											<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
												<td class="check"><input type="hidden"
													id="code_${ formation.key }"
													value="${ formation.value.domaine }"> <input
													type="hidden" id="id_${ formation.key }"
													value="${ formation.key }"> <input type="hidden"
													id="institute_${ formation.key }"
													value="${ formation.value.institut }"> <input
													type="hidden" id="major_${ formation.key }"
													value="${ formation.value.specialisation }"> <input
													type="hidden" id="year_${ formation.key }"
													value="${ formation.value.annee }"> <input
													type="hidden" id="gpa_${ formation.key }"
													value="${ formation.value.moyenne }"> <input
													type="hidden" id="start_date_${ formation.key }"
													value="${ formation.value.date_debut }"> <input
													type="hidden" id="end_date_${ formation.key }"
													value="${ formation.value.date_fin }"> <input
													type="checkbox" class="chkbox" value="${ formation.key }"
													name="delEdu"></td>
												<td class="program"><a href="#" class="edit"><c:out
															value="${ formation.value.institut }" /></a></td>
												<td><c:out value="${ formation.value.domaine}" /></td>
												<td><c:out value="${ formation.value.annee}" /></td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</form>


			</div>
			<!-- inner -->

		</div>
		<!-- miniList -->
	</div>
	<!-- End Education section -->

	<!-- this is skills section -->
	<div class="module">
		<script type="text/javascript">
	    //<![CDATA[
	    var lang_addSkill = "Ajouter formation";
	    var lang_editSkill = "Modifier formation";
	    var lang_educationRequired = 'Requis';
	    var lang_selectSkillToDelete = "Sélectionnez les enregistrements à supprimer";
	    var lang_instituteMaxLength = "Doit être inférieur à niveau %montant%";
	    var lang_majorMaxLength = "Doit être inférieur à niveau %montant%";
	    var lang_gpaMaxLength = "Doit être inférieur à niveau %montant%";
	    var lang_yearShouldBeNumber = "Doit être un nombre";
	    //]]>
	</script>


		<script type="text/javascript"
			src="<c:url value="/resources/javascript/skill.js" />"></script>
		<!-- End js -->

		<div id="changeSkill" style="display: none;">
			<div class="head">
				<h1 id="headChangeSkill">Ajouter une compétence</h1>
			</div>
			<div class="inner">
				<form id="frmSkill"
					action="<c:url value="/resume">
	        	<c:param name="skill" value="true" /></c:url>"
					method="post" novalidate="novalidate">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="saveSkill" id="saveSkill" value="0" /> <input
						type="hidden" name="skill[id]" id="skill_id">
					<fieldset>
						<ul>
							<li><label for="skill_name">Titre <em>*</em></label> <input
								type="text" name="skill[name]" class="formInputText"
								maxlength="100" id="skill_name"></li>
							<li class="largeTextBox"><label for="skill_list">Liste</label>
								<textarea rows="4" cols="30" name="skill[list]"
									class="formInputText" id="skill_list"></textarea></li>
							<li><label for="skill_years_of_exp">Années
									d'expérience (ex : 3)</label> <input type="text" name="skill[years_of_exp]"
								class="formInputText" maxlength="100" id="skill_years_of_exp">
							</li>

							<li class="required"><em>*</em> Required field</li>
						</ul>
						<p>
							<input type="button" class="" id="btnSkillSave" value="Sauver">
							<input type="button" class="reset" id="btnSkillCancel"
								value="Annuler">
						</p>
					</fieldset>
				</form>
			</div>
		</div>
		<!-- changeEducation -->

		<div class="miniList" id="">
			<div class="head">
				<h1>Compétences</h1>
			</div>
			<div class="inner">
				<form id="frmDelSkill"
					action="<c:url value="/resume">
					<c:param name="skill" value="true" /></c:url>"
					method="post">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="skill[id]" id="skill_id"> <input
						type="hidden" name="deleteSkill" id="deleteSkill" value="0" />

					<div id="tblSkill">
						<p id="actionSkill">
							<input type="button" value="Ajouter" class="" id="addSkill">
							<c:if test="${ !empty resume.competences }">
								<input type="button" value="Effacer" class="delete"
									id="delSkill">
							</c:if>
						</p>
						<table class="table hover">
							<thead>
								<tr>
									<c:if test="${ !empty resume.competences }">
										<th class="check" width="2%"><input type="checkbox"
											id="skillCheckAll"></th>
									</c:if>
									<th>Nom</th>
									<th>Liste</th>
									<th>Années d'expérience</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${ empty resume.competences }">
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
											<td></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${ resume.competences }" var="competence"
											varStatus="boucle">
											<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
												<td class="check"><input type="hidden"
													id="skill_name_${ competence.key }"
													value="${ competence.value.nom }"> <input
													type="hidden" id="skill_list_${ competence.key }"
													value="${ competence.value.liste }"> <input
													type="hidden" id="years_of_exp_${ competence.key }"
													value="${ competence.value.annees_experience}"> <input
													type="checkbox" class="chkbox" value="${ competence.key }"
													name="delSkill"></td>
												<td class="name"><a href="#" class="edit"><c:out
															value="${ competence.value.nom}" /></a></td>
												<td><c:out value="${ competence.value.liste }" /></td>
												<td><c:out
														value="${ competence.value.annees_experience}" /></td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</form>
			</div>
			<!-- inner -->
		</div>
		<!-- miniList -->
	</div>



	<!-- this is Languages section -->
	<div class="module">
		<script type="text/javascript">
	    //<![CDATA[
	     var lang_addLanguage = "Ajouter une langue";
		 var lang_editLanguage = "Modifier langue";
		 var lang_languageRequired = 'Requis';
		 var lang_languageTypeRequired = 'Requis';
		 var lang_competencyRequired = 'Requis';
		 var lang_selectLanguageToDelete = "Sélectionnez les enregistrements à supprimer";
		 var lang_commentsMaxLength = "Doit être inférieur à niveau %montant%";
		  //]]>
	</script>


		<script type="text/javascript"
			src="<c:url value="/resources/javascript/language.js" />"></script>
		<!-- End js -->

		<div id="changeLanguage" style="display: none;">
			<div class="head">
				<h1 id="headChangeLanguage">Ajouter une langue</h1>
			</div>
			<div class="inner">
				<form id="frmLanguage"
					action="<c:url value="/resume">
	        		<c:param name="language" value="true" /></c:url>"
					method="post" novalidate="novalidate">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="saveLanguage" id="saveLanguage" value="0" /> <input
						type="hidden" name="language[id]" id="language_id">
					<fieldset>
						<ul>
							<li><label for="language_name">Nom <em>*</em></label> <input
								type="text" name="language[name]" class="formInputText"
								maxlength="100" id="language_name"></li>
							<li class="largeTextBox"><label for="language_fluency">Aisance
									<em>*</em>
							</label> <input type="text" name="language[fluency]"
								class="formInputText" maxlength="100" id="language_fluency">
								<span> (ex: Lu, parlé, ecrit : debutant,bilingue)</span></li>

							<li class="required"><em>*</em> Required field</li>
						</ul>
						<p>
							<input type="button" class="" id="btnLanguageSave" value="Sauver">
							<input type="button" class="reset" id="btnLanguageCancel"
								value="Annuler">
						</p>
					</fieldset>
				</form>
			</div>
		</div>
		<!-- changeLanguage -->

		<div class="miniList" id="">
			<div class="head">
				<h1>Langues</h1>
			</div>
			<div class="inner">

				<form id="frmDelLanguage"
					action="<c:url value="/resume">
					<c:param name="language" value="true" /></c:url>"
					method="post">
					<input type="hidden" name="id_resume" value="${resume.id }" /> <input
						type="hidden" name="language[id]" id="language_id"> <input
						type="hidden" name="deleteLanguage" id="deleteLanguage" value="0" />

					<div id="tblLanguage">
						<p id="actionLanguage">
							<input type="button" value="Ajouter" class="" id="addLanguage">
							<c:if test="${ !empty resume.langues }">
								<input type="button" value="Effacer" class="delete"
									id="delLanguage">
							</c:if>
						</p>
						<table class="table hover">
							<thead>
								<tr>
									<c:if test="${ !empty resume.langues }">
										<th class="check" width="2%"><input type="checkbox"
											id="languageCheckAll"></th>
									</c:if>
									<th>Nom</th>
									<th>Aisance</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${ empty resume.langues }">
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${ resume.langues }" var="langue"
											varStatus="boucle">
											<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
												<td class="check"><input type="hidden"
													id="language_name_${ langue.key }"
													value="${ langue.value.nom }"> <input type="hidden"
													id="language_fluency_${ langue.key }"
													value="${ langue.value.aisance }"> <input
													type="checkbox" class="chkbox" value="${ langue.key }"
													name="delLang"></td>
												<td class="name"><a href="#" class="edit"><c:out
															value="${ langue.value.nom}" /></a></td>
												<td><c:out value="${ langue.value.aisance }" /></td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</form>
			</div>
			<!-- inner -->
		</div>
	</div>
	<c:if test="${ !empty resume.id }">
		<input type="button" value="Create pdf"
			onclick="document.location.href='xmlResume?idResume=${resume.id }'" />
	</c:if>
	<input type="button" value="Liste C.V."
		onclick="document.location.href='userResume'" />
</div>


<%@ include file="../../templates/footer.jsp"%>