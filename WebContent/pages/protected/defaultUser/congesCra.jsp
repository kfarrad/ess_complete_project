<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Interface Conges" />
<c:set var="active" value="Resume list" />
<%@ include file="../../templates/header.jsp"%>

<style>
.formConges fieldset input[type=submit] {
	float: right;
}

.formConges fieldset input[type=reset] {
	float: right;
}

.formConges fieldset input {
	height: 26px;
	margin-bottom: 5px;
	line-height: 26px;
	border-radius: 5px;
	border-right-width: 7px;
	width: 170px;
}

.formConges select, label {
	height: 26px;
	margin-bottom: 5px;
	line-height: 26px;
	border-radius: 5px;
	border-right-width: 7px;
	width: 170px;
}

#advancedSelectionDay {
	display: none;
}
</style>

<script>
	$(document).ready(function() {

		$("#formConges").validate({

			rules : {
				fromDate : {
					required : true,

				},
				toDate : {
					required : true
				},
				motif : {
					required : true
				},
				journee : {
					required : true
				},

			},
			messages : {

				fromDate : "Date de debut est obligatoire",
				toDate : "Date de fin est obligatoire",
				motif : "Motif est obligatoire",
				journee : "Journée ou demi-journée est obligatoire"

			}

		});

	});
</script>

<script>
	$(function() {
		$("#fromDate").datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			numberOfMonths : 1,
			ChangeYear : true,
			onClose : function(selectedDate) {
				$("#toDate").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#toDate").datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			numberOfMonths : 1,
			ChangeYear : true,
			maxDate : '+1Y',
			/* yearRange: yrRange, */
			onClose : function(selectedDate) {
				$("#fromDate").datepicker("option", "maxDate", selectedDate);
				/* var dateToday= new Date ();
				var yrRange=dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 2); */
			}
		});
	});
	
	function display() {
		if (document.getElementById("fromDate").value == document.getElementById("toDate").value) {
		    var div = document.createElement('DIV');
		    div.id = 'advancedSelectionDay';
		    div.innerHTML = '<label> Une journée ou demi-journée ? : </label><select name="journee" class="required" id="journee"><option value="" selected>Journée ou demi-journée</option><option value="0.5">0.5</option><option value="1">1</option></select>';
		    document.getElementById("advancedSelection").insertBefore(div, document.getElementById('toDate').nextSibling);
		    if (document.getElementById('advancedSelectionDay') != null)
		    	document.getElementById('advancedSelectionDay').style.display = 'block';
		  
		} else {
			if (document.getElementById('advancedSelectionDay') != null) {
			    document.getElementById('advancedSelectionDay').style.display = 'none'; 
				document.getElementById("advancedSelectionDay").remove(); 
			}
		
		}
	}
	
</script>

<c:if test="${statutAjout}">
	<script type="text/javascript">
		$(document).ready(function() {
			msg.open(document.getElementById('detail'));
		});
	</script>
</c:if>


<div id="detail" style="display: none;">
	<fieldset class="details">
		<legend>Confirmation de demande de congé</legend>
		<p>Votre demande de congés a bien été prise en compte, vous allez
			être notifié, une fois l'administrateur valide la demande</p>
	</fieldset>
</div>
<form method="post" action="<c:url value="/gestionCongesDemande"/>"
	id="formConges">
	<fieldset id="advancedSelection">
		<legend>Veuillez Entrer Vos dates De Congés</legend>



<c:if test="${sessionScope.sessionUser.isAdmin()}">
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<%
	UserDaoImpl utilisateurDao =(UserDaoImpl)(request.getAttribute("utilisateurDao"));
	List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
	List<Utilisateur> utilisateurs =utilisateurDao.lister();
	int i=0;
	
	// listons uniquement les utilisateur de role user
	for(Utilisateur elt : utilisateurs){
		 if(elt.isActif()) listeUtilisateurs.add(i++,elt);
	}
	Long id_user = (Long)request.getAttribute("salarie");
	Long idUser=0L;
	if(id_user!=null){
		idUser= id_user;
	}

    if (!listeUtilisateurs.isEmpty()) {
		/* if (idUser == 0)
		    idUser = listeUtilisateurs.get(0).getId_profil(); */
		Utilisateur user = utilisateurDao.trouver(idUser);
		
		
		
		
    }
%>
		<label
			for="salarie"> Salarié : </label> <select name="salarie"
			class="required" id="salarie">

			<option value="" selected>Selectionnez salarié</option>
<%
				if (listeUtilisateurs != null) {
					if (listeUtilisateurs.size() != 0) {
						for (Utilisateur userElt : listeUtilisateurs) {
							UserProfil profil = userElt.getProfil();
			%>
			<option value="<%=userElt.getId_profil()%>"
				<%if (userElt.getId_profil() == idUser) {%> selected="selected"
				<%}%>>
				<%=profil.getNom()%>
				<%
					if (profil.getPrenom() != null)
				%><%=profil.getPrenom()%></option>

			<%
				}
					}
				}
			%>

		</select> <br>  <br> 
		
		
</c:if>
		
		
		<label for="fromDate">Date de debut :</label> <input type="text" id="fromDate"
			name="fromDate" onchange="javascript:display();"> <br> <label for="toDate">Date de fin :</label>
		<input type="text" id="toDate" name="toDate" onchange="javascript:display();"> <br> 
		
		<!-- <div id="advancedSelection"></div> -->
		
		<label
			for="motif"> Motif Conges : </label> <select name="motif"
			class="required" id="motif">

			<option value="" selected>Type Conges</option>

			<c:forEach items="${projetConges}" var="projetCra"
				varStatus="myIndex">
				<option value="${projetCra.id}">${projetCra.nom}</option>

			</c:forEach>

		</select> <br> <input type="hidden" name="statut" id="statut" value="1" /><br>

		<input type="submit" value="Valider" name="bouton_ok" id="bouton_ok" />
		&nbsp;&nbsp; <input type="reset" value="Effacer" name="bouton_reset"
			id="bouton_reset" />
	</fieldset>
	
	<!-- <input type="hidden" name="to" value="farid.chkirrou@clinkast.fr">
	<input type="hidden" name="subject" value="Demande de congé">
	<input type="hidden" name="body" value="Bonjour, Veuillez vous connecter à votre compte via le lien suivant :http://intranet.clinkast.fr:81/ess/ afin de valider cette demande de congé"> -->
	
	
</form>

<%@ include file="../../templates/footer.jsp"%>