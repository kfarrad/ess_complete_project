<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Imprévu" />
<c:set var="active" value="Imprévu" />
<%@ include file="../../templates/header.jsp"%>

<div>
	<fieldset>
		<legend>Erreurs</legend>
		<label for="imprevus">Message :<span class="requis">*</span></label> <span
			class="erreur">${form.erreurs['imprevus']}</span> <br />
	</fieldset>
</div>

<%@ include file="../../templates/footer.jsp"%>