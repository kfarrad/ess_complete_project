<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8"%>
<%-- Axes Consultant --%>
<div class ="view" style='display:true;'>
	<table class="table">
		<thead>
			<tr>
				<th width="60%">Libellé</th>
				<th width="40%">Remarque</th>
			</tr>
		</thead>
		<tbody>
			<tr>									
				<td width="60%" style="text-align: left">Autres moyens :</td>
				<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsAutresMoyens}</td>
				</tr>
				<tr>									
					<td width="60%" style="text-align: left">Eléments de contexte significatifs</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsElementsSignifications}</td>
				</tr>	
				<tr>									
					<td width="60%" style="text-align: left">Par rapport à ce que Clinkast attendait de moi, quel bilan fais-je de cette année ?</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsBilanClinkastAttentes}</td>
				</tr>		
				<tr>									
					<td width="60%" style="text-align: left">Par rapport à ce que j’attendais de Clinkast, quel bilan fais-je de cette année ?</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsBilanMesAttentes}</td>
				</tr>
				<tr>									
					<td width="60%" style="text-align: left">En synthèse, Quels sont mes atouts pour progresser ?</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsAtoutPourProgresser}</td>
				</tr>
				<tr>									
					<td width="60%" style="text-align: left">En synthèse, Sur quels axes j’estime devoir progresser ?</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsAxesAprogresser}</td>
				</tr>
				<tr>									
					<td width="60%" style="text-align: left">Quels sont mes souhaits d’évolution à court/moyen terme (2/3 ans) ? (Orientations métier, type de mission, sujets abordés, spécialisation</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsSouhaitsCourtMoyen}</td>
				</tr>
				<tr>									
					<td width="60%" style="text-align: left">Quelles satisfactions je retire de mon métier de consultant? Qu’est-ce qui me motive ? Quels sont les éléments auxquels j’attache de l’importance ?</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsElemetsImportant}</td>
				</tr>
				<tr>									
					<td width="60%" style="text-align: left">En resumé ( Quels sont les 2-3 points à faire absolument comprendre lors de cet entretien ?)</td>
					<td width="40%">${gestionEvaluations.autoEvaluations.autoEvaluationsResume}</td>
				</tr>					
			</tbody>
		</table>
</div>
<div class="edit" style='display:none;'>
	<form id= "formRes" action="<c:url value="/autoEvaluation"><c:param name="formType" value="RES"/></c:url>" method="post" novalidate = "novalidate">							
		<input type="hidden" name="autoEvaluations[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
		<p>&nbsp;</p>
		<p>Autres moyens :</p>
		<textarea class="jqte-test" id="autoEvaluationsAutresMoyens" name="resume[autoEvaluationsAutresMoyens]">${gestionEvaluations.autoEvaluations.autoEvaluationsAutresMoyens}</textarea>
		
		<p>Eléments de contexte significatifs</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsElementsSignifications]" id="resume_autoEvaluationsElementsSignifications">${gestionEvaluations.autoEvaluations.autoEvaluationsElementsSignifications}</textarea>
				
		<p>Par rapport à ce que Clinkast attendait de moi, quel bilan fais-je de cette année ?</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsBilanClinkastAttentes]" id="resume_autoEvaluationsBilanClinkastAttentes">${gestionEvaluations.autoEvaluations.autoEvaluationsBilanClinkastAttentes}</textarea>
		
		<p>Par rapport à ce que j’attendais de Clinkast, quel bilan fais-je de cette année ?</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsBilanMesAttentes]" id="resume_autoEvaluationsBilanMesAttentes">${gestionEvaluations.autoEvaluations.autoEvaluationsBilanMesAttentes}</textarea>
		
		<p>En synthèse, Quels sont mes atouts pour progresser ?</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsAtoutPourProgresser]" id="resume_autoEvaluationsAtoutPourProgresser">${gestionEvaluations.autoEvaluations.autoEvaluationsAtoutPourProgresser}</textarea>
		
		<p>En synthèse, Sur quels axes j’estime devoir progresser ?</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsAxesAprogresser]" id="resume_autoEvaluationsAxesAprogresser">${gestionEvaluations.autoEvaluations.autoEvaluationsAxesAprogresser}</textarea>
		
		<p>Quels sont mes souhaits d’évolution à court/moyen terme (2/3 ans) ? (Orientations métier, type de mission, sujets abordés, spécialisation</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsSouhaitsCourtMoyen]" id="resume_autoEvaluationsSouhaitsCourtMoyen">${gestionEvaluations.autoEvaluations.autoEvaluationsSouhaitsCourtMoyen}</textarea>
													
		<p>Quelles satisfactions je retire de mon métier de consultant? Qu’est-ce qui me motive ? Quels sont les éléments auxquels j’attache de l’importance ?</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsElemetsImportant]" id="resume_autoEvaluationsElemetsImportant">${gestionEvaluations.autoEvaluations.autoEvaluationsElemetsImportant}</textarea>
															
		<p>En resumé ( Quels sont les 2-3 points à faire absolument comprendre lors de cet entretien ?)</p>
		<textarea class="jqte-test" name="resume[autoEvaluationsResume]" id="resume_autoEvaluationsResume">${gestionEvaluations.autoEvaluations.autoEvaluationsResume}</textarea>
			
	</form>
</div>

	<script>
	$('.jqte-test').jqte();
	
	// settings of status
	var jqteStatus = true;
	$(".status").click(function()
	{
		jqteStatus = jqteStatus ? false : true;
		$('.jqte-test').jqte({"status" : jqteStatus})
	});
</script>
