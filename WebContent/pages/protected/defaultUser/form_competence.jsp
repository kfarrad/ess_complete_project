<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul>
	<li>
		<label for="competence_attendues">Sait cadrer et animer un atelier de réflexion ouverte sur un thème sensible du projet</label>
		<select name="competence_attendues" id="competence[attendues]">
			<option value="" selected>J'ai une évolution ...</option>
			<option value="1">Significativement supérieur</option>
			<option value="2">Régulièrement supérieur</option>
			<option value="3">Conforme</option>
			<option value="4">Rejoignant partiellement</option>
			<option value="5">En deçà</option>
			<option value="6">Très inférieur</option>
		</select>
		</li>
		<li class="largeTextBox">
	 	<label for="competence_commentaires">Commentaires, Exemples</label>
		<textarea rows="4" cols="28" name="competence[commentaires]" class="formInputText" id="competence[commentaires]"></textarea>
	</li>
</ul>
