<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul>
	<li class="largeTextBox">
	 	<label for="autresExigences_atoutPourProgresser">Quels sont mes atouts de progression ?</label>
		<textarea rows="4" cols="28" name="autresExigences[atoutPourProgresser]" class="formInputText" id="autresExigences_atoutPourProgresser"></textarea>
	</li>
	<li class="largeTextBox">
	 	<label for="autresExigences_axesAProgresser">Eléments de contexte significatifs:</label>
		<textarea rows="4" cols="28" name="autresExigences[axesAProgresser]" class="formInputText" id="autresExigences_axesAProgresser"></textarea>
	</li>
</ul>
		<p>
		<input type="button" class="" id="btnWorkAutresExigenceSauve" value="Sauver">
		<input type="button" class="reset" id="btnWorkAutresExigenceCancel" value="Annuler">
	</p>	
