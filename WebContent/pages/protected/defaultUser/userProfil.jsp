<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Consultation du profil" />
<%@ include file="../../templates/header.jsp"%>

<form>
	<fieldset>

		<label>Nom </label> <label> ${user.profil.nom}</label> <br /> <br />

		<label>Prénom </label> <label> ${user.profil.prenom}</label> <br /> <br />

		<label>Adresse </label> <label> ${user.profil.adresse}</label> <br />
		<br /> <label>Numéro de téléphone </label> <label>
			${user.profil.telephone}</label> <br /> <br /> <label>Adresse email
		</label> <label> ${user.profil.email}</label> <br /> <br /> <label>Date
			de naissance </label> <label> ${user.profil.dob}</label> <br /> <br /> <label>Numéro
			sécurité sociale </label> <label>${user.profil.numero_ssn}</label> <br /> <br />

		<label>Carte vitale </label> <label><c:if
				test="${!empty user.profil.carte_vitale}">
				<a href="<c:url value="/download/${user.profil.carte_vitale}"/>"
					target="_blank"
					title="Carte vitale de <c:out value="${user.profil.nom}"></c:out> <c:out value="${user.profil.prenom}"></c:out>"><img
					src="<c:url value="/resources/images/carte_vitale.png"/>" /></a>
			</c:if></label> <br /> <br /> <label>CNI </label> <label><c:if
				test="${!empty user.profil.cni}">
				<a href="<c:url value="/download/${user.profil.cni}"/>"
					target="_blank"
					title="Carte d'identité de <c:out value="${user.profil.nom}"></c:out> <c:out value="${user.profil.prenom}"></c:out>"><img
					src="<c:url value="/resources/images/cni.png"/>" /></a>
			</c:if></label> <br /> <br />
		<c:choose>
			<c:when test="${user.role == 'ADMIN'}">
			<input type="button" value="Modifier"
					onclick="javascript:document.location.href='<c:url value="/userProfilUpdate"><c:param name="idProfil" value="${user.profil.id}" /></c:url>';" />
			</c:when>
			<c:otherwise>
				<input type="button" value="Modifier"
					onclick="javascript:document.location.href='<c:url value="/contactMail"/>';" />
			</c:otherwise>
		</c:choose>

		<input type="button" value="Changer mot de passe"
			onclick="javascript:document.location.href='<c:url value="/userUpdate"/>';" />
	</fieldset>
</form>
<%@ include file="../../templates/footer.jsp"%>