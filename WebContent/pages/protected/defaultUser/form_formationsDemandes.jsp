<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="changeFormationDemandes" style="display: none;">
<div class="head">
	<h1 id="headChangeFormationDemandes">Ajouter une formation suivie</h1>
 </div>
 <div class="inner">
<form id = "formWorkFormationDemandesSauve" action="<c:url value="/autoEvaluation"><c:param name="formType" value="FDD"/></c:url>" method="post" novalidate = "novalidate">
<input type="hidden" name="formationDemandes[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
<input type="hidden" name="formationDemandes[id_formationDemande]" id="formationDemandes_id_formationDemande" value="0"/>
<input type="hidden" name="champs[action]" id = "formationDemandes_sauve" value="sauve"/>
<fieldset>
<ul>
	<li>
	 	<label for="formationDemandes_intitule">Intitulé de la formation <em>*</em></label>
		<input id="formationDemandes_intitule" type="text" name="formationDemandes[intitule]">
	</li>
	<li class="largeTextBox">
	 	<label for="formationDemandes_objectif">Objectifs visés<em>*</em></label>
		<textarea rows="10" cols="28" name="formationDemandes[objectif]" class="formInputText" id="formationDemandes_objectif"></textarea>
	</li>
	
	</ul>
	<p>
		<input type="button" class="" id="btnWorkFormationDemandesSauve" value="Sauver">
		<input type="button" class="reset" id="btnWorkFormationDemandesCancel" value="Annuler">
	</p>
	</fieldset>	
</form>
</div>
</div>