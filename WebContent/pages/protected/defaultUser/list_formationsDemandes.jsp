<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Section activit�s -->
<form id = "formWorkFormationsDemandesDelete" action="<c:url value="/autoEvaluation"><c:param name="formType" value="FDD"/></c:url>" method="post" novalidate = "novalidate">
	<input type="hidden" name="formationDemandes[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
	<input type="hidden" name="champs[action]" id = "formationsDemandes_delete" value="delete"/>
	<table class="table">
			<thead>
				<tr>
					<th><input type="checkbox" name="allFormationDemandes" id = "allFormationDemandes"></th>
					<th>Intitule de la formation</th>
					<th>Objectif vis�</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty gestionEvaluations.autoEvaluations.formationsDemandes}">
						<tr>
							<td colspan="3" class="name">Aucune donn�e</td>
						</tr>
					</c:when>
					<c:otherwise>
						<%-- Parcours le tableau et affiche la liste des activit�s  --%>
					
					<c:forEach items="${gestionEvaluations.autoEvaluations.formationsDemandes}" var="mapFormationDemande" varStatus="boucle">
					<tr class="dataFdd">
						<td><input type="checkbox" name="formationDemande_id_formations" value="${mapFormationDemande.formationsDemandesId}"></td>
						<td><c:out value="${mapFormationDemande.formationsDemandesLibelle}" /></td>
						<td><c:out value="${mapFormationDemande.formationsDemandesObjectifs }" /></td>
					</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	   </form>
				