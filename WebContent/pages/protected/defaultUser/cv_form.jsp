<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>

<title>CLINKAST HRM</title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-style-type" content="text/css" />

<!-- Library CSS files -->
<link href="<c:url value="/resources/webSymphony/theme/reset.css"/>"
	rel="stylesheet" media="all" />
<link href="<c:url value="/resources/webSymphony/theme/tipTip.css"/>"
	rel="stylesheet" media="all" />
<link
	href="<c:url value="/resources/webSymphony/theme/jquery/jquery-ui-1.8.21.custom.css"/>"
	rel="stylesheet" media="all" />
<link
	href="<c:url value="/resources/webSymphony/theme/jquery/jquery.autocomplete.css"/>"
	rel="stylesheet" media="all" />

<!-- Custom CSS files -->
<link href="<c:url value="/resources/webSymphony/theme/main.css" />"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<c:url value="/resources/javascript/jquery-1.11.0.min.js" />"></script>
<%-- <script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery-1.7.2.min.js"/>"></script>  --%>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/validate/jquery.validate.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery.ui.core.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery.autocomplete.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/orangehrm.autocomplete.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery.ui.datepicker.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery.form.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery.tipTip.minified.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/bootstrap-modal.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/jquery/jquery.clickoutside.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/orangehrm.validate.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/archive.js"/>"></script>


<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->



<script type="text/javascript"
	src="<c:url value="/resources/webSymphony/js/orangehrm.datepicker.js"/>"></script>
<style type="text/css"></style>
<link rel="stylesheet" type="text/css" media="all"
	href="<c:url value="/resources/webSymphony/theme/orangehrm.datepicker.css"/>" />
</head>
<body>
	<div id="content">


		<script type="text/javascript"
			src="<c:url value="/resources/webSymphony/orange/viewQualificationsSuccess.js"/>"></script>


		<div class="box pimPane">

			<div id="sidebar">

				<!-- sidebar -->
				<!--    
    <div class="head">
        <h1>Diplômes</h1>
    </div>
    -->

				<a name="workexperience"></a>
				<!-- this is work experience section -->
				<div id="changeWorkExperience" style="display: none;">
					<div class="head">
						<h1 id="headChangeWorkExperience">Ajouter expérience
							professionnelle</h1>
					</div>

					<div class="inner">
						<form id="frmWorkExperience"
							action="/symfony/web/index.php/pim/saveDeleteWorkExperience/empNumber/1/option/save"
							method="post" novalidate="novalidate">
							<input type="hidden" name="experience[_csrf_token]"
								value="9f4b3895dd2a1fbf567b6cbfc2d627a4"
								id="experience__csrf_token"/> 
							<input value="1"
								type="hidden" name="experience[emp_number]"
								id="experience_emp_number"/>
						    <input type="hidden"
									name="experience[seqno]" id="experience_seqno"/>
										<fieldset>
											<ol>
												<li><label for="experience_employer">Entreprise
														<em>*</em>
												</label> <input type="text" name="experience[employer]"
													class="formInputText" maxlength="100"
													id="experience_employer"/></li>
												<li><label for="experience_jobtitle">Titre du
														poste <em>*</em>
												</label> <input type="text" name="experience[jobtitle]"
													class="formInputText" maxlength="100"
													id="experience_jobtitle"/></li>
												<li><label for="experience_from_date">À partir
														de</label> <input id="experience_from_date" type="text"
													name="experience[from_date]"
													class="formInputText calendar hasDatepicker"/>
													<img
														class="ui-datepicker-trigger"
														src="<c:url value="/resources/images/calendar.png"/>"
														alt="" title=""/> 
<script type="text/javascript">

    var datepickerDateFormat = 'D, dd M yy';
    var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

    $(document).ready(function(){
        
        var dateFieldValue = $.trim($("#experience_from_date").val());
        if (dateFieldValue == '') {
            $("#experience_from_date").val(displayDateFormat);
        }

        daymarker.bindElement("#experience_from_date",
        {
            showOn: "both",
            dateFormat: datepickerDateFormat,
            buttonImage: "/resources/images/calendar.png",
            buttonText:"",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+100",
            firstDay: 1,
            onClose: function() {
                $("#experience_from_date").trigger('blur');
            }            
        });
        
        //$("img.ui-datepicker-trigger").addClass("editable");
        
        $("#experience_from_date").click(function(){
            daymarker.show("#experience_from_date");
            if ($(this).val() == displayDateFormat) {
                $(this).val('');
            }
        });
    
    });

</script></li>
<li><label for="experience_to_date">À</label> <input
	id="experience_to_date" type="text"
	name="experience[to_date]"
	class="formInputText calendar hasDatepicker"/><img
		class="ui-datepicker-trigger"
		src="<c:url value="/resources/images/calendar.png"/>"
		alt="" title=""/> <script type="text/javascript">

    var datepickerDateFormat = 'D, dd M yy';
    var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

    $(document).ready(function(){
        
        var dateFieldValue = $.trim($("#experience_to_date").val());
        if (dateFieldValue == '') {
            $("#experience_to_date").val(displayDateFormat);
        }

        daymarker.bindElement("#experience_to_date",
        {
            showOn: "both",
            dateFormat: datepickerDateFormat,
            buttonImage: "/resources/images/calendar.png",
            buttonText:"",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+100",
            firstDay: 1,
            onClose: function() {
                $("#experience_to_date").trigger('blur');
            }            
        });
        
        //$("img.ui-datepicker-trigger").addClass("editable");
        
        $("#experience_to_date").click(function(){
            daymarker.show("#experience_to_date");
            if ($(this).val() == displayDateFormat) {
                $(this).val('');
            }
        });
    
    });

</script></li>
												<li class="largeTextBox"><label
													for="experience_comments">Commenter</label> <textarea
														rows="4" cols="30" name="experience[comments]"
														class="formInputText" id="experience_comments"></textarea>
												</li>
												<li class="required"><em>*</em> Required field</li>
											</ol>
											<p>
												<input type="button" class="" id="btnWorkExpSave"
													value="Sauver"> <input type="button" class="reset"
													id="btnWorkExpCancel" value="Annuler">
											</p>
										</fieldset>
						</form>
					</div>
				</div>
				<!-- changeWorkExperience  -->

				<div class="miniList" id="sectionWorkExperience">

					<div class="head">
						<h1>Expérience professionnelle</h1>
					</div>

					<div class="inner">






						<form id="frmDelWorkExperience"
							action="/symfony/web/index.php/pim/saveDeleteWorkExperience/empNumber/1/option/delete"
							method="post">
							<input type="hidden" name="defaultList[_csrf_token]"
								value="54f2b73f8b611384e7659cb2f5be4eed"
								id="defaultList__csrf_token"/>
								<p id="actionWorkExperience">
									<input type="button" value="Ajouter" class=""
										id="addWorkExperience"> <input type="button"
										value="Effacer" class="delete" id="delWorkExperience"
										style="display: none;"/>
								</p>
								<table id="" class="table hover">
									<thead>
										<tr>
											<th class="check" style="width: 2%; display: none;"><input
												type="checkbox" id="workCheckAll"/></th>
											<th>Entreprise</th>
											<th>Titre du poste</th>
											<th>À partir de</th>
											<th>À</th>
											<th>Commenter</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
						</form>


					</div>

				</div>
				<!-- miniList-sectionWorkExperience -->

				<!-- this is education section -->

				<a name="education"></a>
				<div id="changeEducation" style="display: none;">
					<div class="head">
						<h1 id="headChangeEducation">Ajouter formation</h1>
					</div>
					<div class="inner">
						<form id="frmEducation"
							action="/symfony/web/index.php/pim/saveDeleteEducation/empNumber/1/option/save"
							method="post" novalidate="novalidate">
							<input type="hidden" name="education[_csrf_token]"
								value="f77f3db9cd6ced258ee1aa9c8b32889b"
								id="education__csrf_token"/> <input type="hidden"
								name="education[id]" id="education_id"> <input value="1"
									type="hidden" name="education[emp_number]"
									id="education_emp_number"/>
										<fieldset>
											<ol>
												<li><label for="education_code">Niveau <em>*</em></label>
													<select name="education[code]" class="formSelect"
													id="education_code">
														<option value="" selected="selected">--
															Sélectionner --</option>
														<option value="1">Ecole d'ingénieur</option>
														<option value="2">Ecole de commerce</option>
												</select><span id="static_education_code" style="display: none;"></span>
												</li>
												<li><label for="education_institute">Institut</label> <input
													type="text" name="education[institute]"
													class="formInputText" maxlength="100"
													id="education_institute"></li>
												<li><label for="education_major">Major /
														Spécialisation</label> <input type="text" name="education[major]"
													class="formInputText" maxlength="100" id="education_major"></li>
												<li><label for="education_year">Année</label> <input
													type="text" name="education[year]" class="formInputText"
													maxlength="4" id="education_year"></li>
												<li><label for="education_gpa">GPA / Score</label> <input
													type="text" name="education[gpa]" class="formInputText"
													maxlength="25" id="education_gpa"></li>
												<li><label for="education_start_date">Date de
														début</label> <input id="education_start_date" type="text"
													name="education[start_date]"
													class="formInputText calendar hasDatepicker"><img
														class="ui-datepicker-trigger"
														src="<c:url value="/resources/images/calendar.png"/>"
														alt="" title=""/> <script type="text/javascript">

    var datepickerDateFormat = 'D, dd M yy';
    var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

    $(document).ready(function(){
        
        var dateFieldValue = $.trim($("#education_start_date").val());
        if (dateFieldValue == '') {
            $("#education_start_date").val(displayDateFormat);
        }

        daymarker.bindElement("#education_start_date",
        {
            showOn: "both",
            dateFormat: datepickerDateFormat,
            buttonImage:"/resources/images/calendar.png",
            buttonText:"",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+100",
            firstDay: 1,
            onClose: function() {
                $("#education_start_date").trigger('blur');
            }            
        });
        
        //$("img.ui-datepicker-trigger").addClass("editable");
        
        $("#education_start_date").click(function(){
            daymarker.show("#education_start_date");
            if ($(this).val() == displayDateFormat) {
                $(this).val('');
            }
        });
    
    });

</script></li>
												<li><label for="education_end_date">Date de fin</label>
													<input id="education_end_date" type="text"
													name="education[end_date]"
													class="formInputText calendar hasDatepicker"><img
														class="ui-datepicker-trigger"
														src="<c:url value="/resources/images/calendar.png"/>"
														alt="" title=""/> <script type="text/javascript">

    var datepickerDateFormat = 'D, dd M yy';
    var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

    $(document).ready(function(){
        
        var dateFieldValue = $.trim($("#education_end_date").val());
        if (dateFieldValue == '') {
            $("#education_end_date").val(displayDateFormat);
        }

        daymarker.bindElement("#education_end_date",
        {
            showOn: "both",
            dateFormat: datepickerDateFormat,
            buttonImage: "/resources/images/calendar.png",
            buttonText:"",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+100",
            firstDay: 1,
            onClose: function() {
                $("#education_end_date").trigger('blur');
            }            
        });
        
        //$("img.ui-datepicker-trigger").addClass("editable");
        
        $("#education_end_date").click(function(){
            daymarker.show("#education_end_date");
            if ($(this).val() == displayDateFormat) {
                $(this).val('');
            }
        });
    
    });

</script></li>
												<li class="required line"><em>*</em> Required field</li>
											</ol>
											<p>
												<input type="button" class="" id="btnEducationSave"
													value="Sauver"> <input type="button" class="reset"
													id="btnEducationCancel" value="Annuler">
											</p>
										</fieldset>
						</form>
					</div>
				</div>
				<!-- changeEducation -->

				<div class="miniList" id="">

					<div class="head">
						<h1>Formation</h1>
					</div>

					<div class="inner">






						<form id="frmDelEducation"
							action="/symfony/web/index.php/pim/saveDeleteEducation/empNumber/1/option/delete"
							method="post">
							<input type="hidden" name="defaultList[_csrf_token]"
								value="54f2b73f8b611384e7659cb2f5be4eed"
								id="defaultList__csrf_token">
								<div id="tblEducation">
									<p id="actionEducation">
										<input type="button" value="Ajouter" class=""
											id="addEducation"> <input type="button"
											value="Effacer" class="delete" id="delEducation">
									</p>
									<table width="100%" cellspacing="0" cellpadding="0"
										class="table tablesorter">
										<thead>
											<tr>
												<th class="check" width="2%"><input type="checkbox"
													id="educationCheckAll"></th>
												<th>Niveau</th>
												<th>Année</th>
												<th>GPA / Score</th>
											</tr>
										</thead>
										<tbody>

											<tr class="odd">
												<td class="check"><input type="hidden" id="code_3"
													value="1"> <input type="hidden" id="id_3" value="3">
															<input type="hidden" id="code_desc_3"
															value="Ecole d'ingénieur"> <input type="hidden"
																id="institute_3" value="sqsq"> <input
																	type="hidden" id="major_3" value="ssqsqs"> <input
																		type="hidden" id="year_3" value="2014"> <input
																			type="hidden" id="gpa_3" value=""> <input
																				type="hidden" id="start_date_3"
																				value="Fri, 13 Jun 2014"> <input
																					type="hidden" id="end_date_3"
																					value="Fri, 27 Jun 2014"> <input
																						type="checkbox" class="chkbox" value="3"
																						name="delEdu[]"></td>
												<td class="program"><a href="#" class="edit">Ecole
														d'ingénieur</a></td>
												<td>2014</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
						</form>


					</div>
					<!-- inner -->

				</div>
				<!-- miniList -->

				<script type="text/javascript">
    //<![CDATA[
    var fileModified = 0;
    var lang_addEducation = "Ajouter formation";
    var lang_editEducation = "Modifier formation";
    var lang_educationRequired = 'Requis';
    var lang_invalidDate = 'Doit être une date valide au format D, dd M yyyy';
    var lang_EndDateBeforeSatrtDate = "Date de fin doit être postérieure à la date de début";
    var lang_selectEducationToDelete = "Sélectionnez les enregistrements à supprimer";
    var lang_instituteMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_majorMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_gpaMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_yearShouldBeNumber = "Doit être un nombre";
    var datepickerDateFormat = 'D, dd M yy';
    //]]>
</script>

				<script type="text/javascript">
    //<![CDATA[
    var startDate = "";
    $(document).ready(function() {
        
        $('#education_code').after('<span id="static_education_code" style="display:none;"></span>');

        //hide add section
        $("#changeEducation").hide();
        $("#educationRequiredNote").hide();

        //hiding the data table if records are not available
        if($("div#tblEducation .chkbox").length == 0) {
            $('div#tblEducation .check').hide();
            $("#editEducation").hide();
            $("#delEducation").hide();
        }

        //if check all button clicked
        $("#educationCheckAll").click(function() {
            $("div#tblEducation .chkbox").removeAttr("checked");
            if($("#educationCheckAll").attr("checked")) {
                $("div#tblEducation .chkbox").attr("checked", "checked");
            }
        });

        //remove tick from the all button if any checkbox unchecked
        $("div#tblEducation .chkbox").click(function() {
            $("#educationCheckAll").removeAttr('checked');
            if($("div#tblEducation .chkbox").length == $("div#tblEducation .chkbox:checked").length) {
                $("#educationCheckAll").attr('checked', 'checked');
            }
        });

        $("#addEducation").click(function() {
            removeEditLinks();
            clearMessageBar();
            $('div#changeEducation label.error').hide();
            
            //changing the headings
            $("#headChangeEducation").text(lang_addEducation);
            $("div#tblEducation .chkbox").hide();
            $("#educationCheckAll").hide();

            //hiding action button section
            $("#actionEducation").hide();
            $("#education_id").val("");
            $('#static_education_code').hide().val("");
            $("#education_code").show().val("");
            $("#education_institute").val("");
            $("#education_major").val("");
            $("#education_year").val("");
            $("#education_gpa").val("");
            $("#education_start_date").val(displayDateFormat);
            $("#education_end_date").val(displayDateFormat);

            //show add form
            $("#changeEducation").show();
            $("#educationRequiredNote").show();
        });

        //clicking of delete button
        $("#delEducation").click(function(){
            clearMessageBar();
            if ($("div#tblEducation .chkbox:checked").length > 0) {
                $("#frmDelEducation").submit();
            } else {
                $("#educationMessagebar").attr('class', 'messageBalloon_notice').text(lang_selectEducationToDelete);
            }
        });

        $("#btnEducationSave").click(function() {
            clearMessageBar();
            startDate = $('#education_start_date').val();
            $("#frmEducation").submit();
        });
        
        //form validation
        var educationValidator =
            $("#frmEducation").validate({
            rules: {
                'education[code]': {required: true},
                'education[institute]': {required: false, maxlength: 100},
                'education[major]': {required: false, maxlength: 100},
                'education[year]': {required: false, digits: true},
                'education[gpa]': {required: false, maxlength: 25},
                'education[start_date]': {valid_date: function(){return {format: datepickerDateFormat, required:false, displayFormat:displayDateFormat}}},
                'education[end_date]': {valid_date: function(){return {format:datepickerDateFormat, required:false, displayFormat:displayDateFormat}}, date_range: function() {return {format:datepickerDateFormat, displayFormat:displayDateFormat, fromDate:startDate}}}
            },
            messages: {
                'education[code]': {required: lang_educationRequired},
                'education[institute]': {maxlength: lang_instituteMaxLength},
                'education[major]': {maxlength: lang_majorMaxLength},
                'education[year]': {digits: lang_yearShouldBeNumber},
                'education[gpa]': {maxlength: lang_gpaMaxLength},
                'education[start_date]': {valid_date: lang_invalidDate},
                'education[end_date]': {valid_date: lang_invalidDate, date_range:lang_EndDateBeforeSatrtDate }
            }
        });
    
        function addEditLinks() {
            // called here to avoid double adding links - When in edit mode and cancel is pressed.
            removeEditLinks();
            $('div#tblEducation table tbody td.program').wrapInner('<a class="edit" href="#"/>');
        }

        function removeEditLinks() {
            $('div#tblEducation table tbody td.program a').each(function(index) {
                $(this).parent().text($(this).text());
            });
        }

        $("#btnEducationCancel").click(function() {
            clearMessageBar();
                        addEditLinks();
                        educationValidator.resetForm();
            $('div#changeEducation label.error').hide();
            $("div#tblEducation .chkbox").removeAttr("checked").show();

            //hiding action button section
            $("#actionEducation").show();
            $("#changeEducation").hide();
            $("#educationRequiredNote").hide();
            $("#educationCheckAll").show();

            // remove any options already in use
            $('#static_education_code').hide().val("");

            //remove if disabled while edit
            $('#education_code').removeAttr('disabled');
        });
   
        $('form#frmDelEducation a.edit').live('click', function(event) {
            event.preventDefault();
            clearMessageBar();

            //changing the headings
            $("#headChangeEducation").text(lang_editEducation);
            educationValidator.resetForm();
            $('div#changeEducation label.error').hide();

            //hiding action button section
            $("#actionEducation").hide();

            //show add form
            $("#changeEducation").show();
            var code = $(this).closest("tr").find('input.chkbox:first').val();
            $('#static_education_code').text($("#code_desc_" + code).val()).show();
            $('#education_code').val($("#code_" + code).val()).hide();
            $("#education_id").val(code);
            $("#education_institute").val($("#institute_" + code).val());
            $("#education_major").val($("#major_" + code).val());
            $("#education_year").val($("#year_" + code).val());
            $("#education_gpa").val($("#gpa_" + code).val());
            $("#education_start_date").val($("#start_date_" + code).val());
            $("#education_end_date").val($("#end_date_" + code).val());
            if ($("#education_start_date").val() == '') {
                $("#education_start_date").val(displayDateFormat);
            }
            if ($("#education_end_date").val() == '') {
                $("#education_end_date").val(displayDateFormat);
            }
            $("#educationRequiredNote").show();
            $("div#tblEducation .chkbox").hide();
            $("#educationCheckAll").hide();
        });
    });

    //]]>
</script>
				<!-- this is skills section -->

				<a name="skill"></a>
				<div id="changeSkill" style="display: none;">
					<div class="head">
						<h1 id="headChangeSkill">Ajouter compétences</h1>
					</div>

					<div class="inner">
						<form id="frmSkill"
							action="/symfony/web/index.php/pim/saveDeleteSkill/empNumber/1/option/save"
							method="post" novalidate="novalidate">
							<input type="hidden" name="skill[_csrf_token]"
								value="62cd566bac9e48bf0d9228221f597f9c" id="skill__csrf_token">
								<input value="1" type="hidden" name="skill[emp_number]"
								id="skill_emp_number">
									<fieldset>
										<ol>
											<li><label for="skill_code">Compétence <em>*</em></label>
												<select name="skill[code]" class="formSelect"
												id="skill_code">
													<option value="" selected="selected">--
														Sélectionner --</option>
													<option value="1">Java</option>
													<option value="2">PMO</option>
											</select><span id="static_skill_code" style="display: none;"></span></li>
											<li><label for="skill_years_of_exp">Années
													d'expérience</label> <input type="text" name="skill[years_of_exp]"
												class="formInputText" maxlength="100"
												id="skill_years_of_exp"></li>
											<li class="largeTextBox"><label for="skill_comments">Commentaires</label>
												<textarea rows="4" cols="30" name="skill[comments]"
													class="formInputText" id="skill_comments"></textarea></li>
											<li class="required"><em>*</em> Required field</li>
										</ol>
										<p>
											<input type="button" class="" id="btnSkillSave"
												value="Sauver"> <input type="button" class="reset"
												id="btnSkillCancel" value="Annuler">
										</p>
									</fieldset>
						</form>
					</div>
				</div>
				<!-- changeSkill -->

				<div class="miniList" id="tblSkill">
					<div class="head">
						<h1>Compétences</h1>
					</div>

					<div class="inner">





						<form id="frmDelSkill"
							action="/symfony/web/index.php/pim/saveDeleteSkill/empNumber/1/option/delete"
							method="post">
							<input type="hidden" name="defaultList[_csrf_token]"
								value="54f2b73f8b611384e7659cb2f5be4eed"
								id="defaultList__csrf_token">
								<p id="actionSkill">
									<input type="button" value="Ajouter" class="" id="addSkill">
										<input type="button" value="Effacer" class="delete"
										id="delSkill" style="display: none;">
								</p>
								<table id="" cellpadding="0" cellspacing="0" width="100%"
									class="table tablesorter">
									<thead>
										<tr>
											<th class="check" width="2%" style="display: none;"><input
												type="checkbox" id="skillCheckAll"></th>
											<th>Compétence</th>
											<th>Années d'expérience</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
										</tr>
									</tbody>
								</table>
						</form>


					</div>
				</div>
				<!-- miniList-tblSkill -->

				<script type="text/javascript">
    //<![CDATA[
    var fileModified = 0;
    var lang_addSkill = "Ajouter compétences";
    var lang_editSkill = "Modifier des compétences";
    var lang_skillRequired = 'Requis';
    var lang_selectSkillToDelete = "Sélectionnez les enregistrements à supprimer";
    var lang_commentsMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_yearsOfExpShouldBeNumber = "Doit être un nombre";
    var lang_yearsOfExpMax = "Doit être inférieur à %montant%";
    var canUpdate = '1';
    //]]>
</script>

				<script type="text/javascript">
    //<![CDATA[
    
    $(document).ready(function() {
        //To hide unchanged element into hide and show the value in span while editing
        $('#skill_code').after('<span id="static_skill_code" style="display:none;"></span>');

        function addEditLinks() {
            // called here to avoid double adding links - When in edit mode and cancel is pressed.
            removeEditLinks();
            $('form#frmDelSkill table tbody td.name').wrapInner('<a class="edit" href="#"/>');
        }
        
        function removeEditLinks() {
            $('form#frmDelSkill table tbody td.name a').each(function(index) {
                $(this).parent().text($(this).text());
            });
        }
        
        //hide add section
        $("#changeSkill").hide();
        $("#skillRequiredNote").hide();
        
        //hiding the data table if records are not available
        if($("div#tblSkill .chkbox").length == 0) {
            //$("#tblSkill").hide();
            $('div#tblSkill .check').hide();
            $("#editSkill").hide();
            $("#delSkill").hide();
        }
        
        //if check all button clicked
        $("#skillCheckAll").click(function() {
            $("div#tblSkill .chkbox").removeAttr("checked");
            if($("#skillCheckAll").attr("checked")) {
                $("div#tblSkill .chkbox").attr("checked", "checked");
            }
        });
        
        //remove tick from the all button if any checkbox unchecked
        $("div#tblSkill .chkbox").click(function() {
            $("#skillCheckAll").removeAttr('checked');
            if($("div#tblSkill .chkbox").length == $("div#tblSkill .chkbox:checked").length) {
                $("#skillCheckAll").attr('checked', 'checked');
            }
        });
        
        $("#addSkill").click(function() {
            
            removeEditLinks();
            clearMessageBar();
            $('div#changeSkill label.error').hide();        
            
            //changing the headings
            $("#headChangeSkill").text(lang_addSkill);
            $("div#tblSkill .chkbox").hide();
            $("#skillCheckAll").hide();
            
            //hiding action button section
            $("#actionSkill").hide();
            
            $('#static_skill_code').hide().val("");
            $("#skill_code").show().val("");
            $("#skill_code option[class='added']").remove();
            $("#skill_major").val("");
            $("#skill_year").val("");
            $("#skill_gpa").val("");
            
            //show add form
            $("#changeSkill").show();
            $("#skillRequiredNote").show();
        });
        
        //clicking of delete button
        $("#delSkill").click(function(){
            
            clearMessageBar();
            
            if ($("div#tblSkill .chkbox:checked").length > 0) {
                $("#frmDelSkill").submit();
            } else {
                $("#skillMessagebar").attr('class', 'messageBalloon_notice').text(lang_selectSkillToDelete);
            }
            
        });
        
        $("#btnSkillSave").click(function() {
            clearMessageBar();
            
            $("#frmSkill").submit();
        });
        
        //form validation
        var skillValidator =
            $("#frmSkill").validate({
            rules: {
                'skill[code]': {required: true},
                'skill[years_of_exp]': {required: false, digits: true, max: 99},
                'skill[comments]': {required: false, maxlength:100}
            },
            messages: {
                'skill[code]': {required: lang_skillRequired},
                'skill[years_of_exp]': {digits: lang_yearsOfExpShouldBeNumber, max: lang_yearsOfExpMax},
                'skill[comments]': {maxlength: lang_commentsMaxLength}
            }
        });
        
        $("#btnSkillCancel").click(function() {
            clearMessageBar();
            if(canUpdate){
                addEditLinks();
            }
            
            skillValidator.resetForm();
            
            $('div#changeSkill label.error').hide();
            
            $("div#tblSkill .chkbox").removeAttr("checked").show();
            
            //hiding action button section
            $("#actionSkill").show();
            $("#changeSkill").hide();
            $("#skillRequiredNote").hide();        
            $("#skillCheckAll").show();
            
            // remove any options already in use
            $("#skill_code option[class='added']").remove();
            $('#static_skill_code').hide().val("");
            
            //remove if disabled while edit
            $('#skill_code').removeAttr('disabled');
        });
        
        $('form#frmDelSkill a.edit').live('click', function(event) {
            event.preventDefault();
            clearMessageBar();
            
            //changing the headings
            $("#headChangeSkill").text(lang_editSkill);
            
            skillValidator.resetForm();
            
            $('div#changeSkill label.error').hide();
            
            //hiding action button section
            $("#actionSkill").hide();
            
            //show add form
            $("#changeSkill").show();
            var code = $(this).closest("tr").find('input.chkbox:first').val();
            
            $('#static_skill_code').text($("#skill_name_" + code).val()).show();
            
            
            
            // remove any options already in use
            $("#skill_code option[class='added']").remove();
            
            $('#skill_code').
                append($("<option class='added'></option>").
                attr("value", code).
                text($("#skill_name_" + code).val())); 
            $('#skill_code').val(code).hide();
            
            $("#skill_years_of_exp").val($("#years_of_exp_" + code).val());
            $("#skill_comments").val($("#comments_" + code).val());
            
            $("#skillRequiredNote").show();
            
            $("div#tblSkill .chkbox").hide();
            $("#skillCheckAll").hide();        
        });
    });
    
    //]]>
</script>
				<!-- this is Languages section -->


				<a name="language"></a>
				<div id="changeLanguage" style="display: none;">
					<div class="head">
						<h1 id="headChangeLanguage">Ajouter une langue</h1>
					</div>

					<div class="inner">
						<form id="frmLanguage"
							action="/symfony/web/index.php/pim/saveDeleteLanguage/empNumber/1/option/save"
							method="post" novalidate="novalidate">
							<fieldset>
								<ol>
									<li><label for="language_code">Langue<em> *</em></label> <select
										name="language[code]" id="language_code">
											<option value="" selected="selected">-- Sélectionner
												--</option>
											<option value="1">Anglais</option>
											<option value="2">Francais</option>
											<option value="3">Russe</option>
									</select><span id="static_language_code" style="display: none;"></span>
									</li>
									<li><label for="language_lang_type">Aisance<em>
												*</em></label> <select name="language[lang_type]" id="language_lang_type">
											<option value="" selected="selected">-- Sélectionner
												--</option>
											<option value="1">Écriture</option>
											<option value="2">Parlant</option>
											<option value="3">Lecture</option>
									</select><span id="static_lang_type" style="display: none;"></span></li>
									<li><label for="language_competency">Compétence<em>
												*</em></label> <select name="language[competency]"
										id="language_competency">
											<option value="" selected="selected">-- Sélectionner
												--</option>
											<option value="1">Rares</option>
											<option value="2">De base</option>
											<option value="3">Bon</option>
											<option value="4">Langue maternelle</option>
									</select></li>
									<li class="largeTextBox"><label for="language_comments">Commentaires</label>
										<textarea rows="4" cols="30" name="language[comments]"
											id="language_comments"></textarea> <input value="1"
										type="hidden" name="language[emp_number]"
										id="language_emp_number"> <input type="hidden"
											name="language[_csrf_token]"
											value="7db3458be14cf812928e2c23d33078e6"
											id="language__csrf_token"></li>

									<li class="required"><em>*</em> Required field</li>
								</ol>
								<p>
									<input type="button" class="" id="btnLanguageSave"
										value="Sauver"> <input type="button" class="reset"
										id="btnLanguageCancel" value="Annuler">
								</p>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- changeLanguage -->

				<div class="miniList" id="tblLanguage">
					<div class="head">
						<h1>Langues</h1>
					</div>

					<div class="inner">






						<form id="frmDelLanguage"
							action="/symfony/web/index.php/pim/saveDeleteLanguage/empNumber/1/option/delete"
							method="post">
							<input type="hidden" name="defaultList[_csrf_token]"
								value="54f2b73f8b611384e7659cb2f5be4eed"
								id="defaultList__csrf_token">
								<p id="actionLanguage">
									<input type="button" value="Ajouter" class="" id="addLanguage">&nbsp;
										<input type="button" value="Effacer" class="delete"
										id="delLanguage" style="display: none;">
								</p>
								<table id="lang_data_table" cellpadding="0" cellspacing="0"
									width="100%" class="table tablesorter">
									<thead>
										<tr>
											<th class="check" width="2%" style="display: none;"><input
												type="checkbox" id="languageCheckAll"></th>
											<th>Langue</th>
											<th>Aisance</th>
											<th>Compétence</th>
											<th>Commentaires</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
						</form>


					</div>
				</div>
				<!-- miniList-tblLanguage sectionLanguage -->

				<script type="text/javascript">
    //<![CDATA[

    var fileModified = 0;
    var lang_addLanguage = "Ajouter une langue";
    var lang_editLanguage = "Modifier langue";
    var lang_languageRequired = 'Requis';
    var lang_languageTypeRequired = 'Requis';
    var lang_competencyRequired = 'Requis';
    var lang_selectLanguageToDelete = "Sélectionnez les enregistrements à supprimer";
    var lang_commentsMaxLength = "Doit être inférieur à niveau %montant%";
    //]]>
</script>

				<script type="text/javascript">
//<![CDATA[

$(document).ready(function() {
    
    $('#language_code').after('<span id="static_language_code" style="display:none;"></span>');
    $('#language_lang_type').after('<span id="static_lang_type" style="display:none;"></span>');
    
    function addEditLinks() {
        // called here to avoid double adding links - When in edit mode and cancel is pressed.
        removeEditLinks();
        $('form#frmDelLanguage table tbody td.name').wrapInner('<a class="edit" href="#"/>');
    }

    function removeEditLinks() {
        $('form#frmDelLanguage table tbody td.name a').each(function(index) {
            $(this).parent().text($(this).text());
        });
    }
    
    //hide add section
    $("#changeLanguage").hide();
    $("#languageRequiredNote").hide();

    //hiding the data table if records are not available
    if($("div#tblLanguage .chkbox").length == 0) {
        //$("#tblLanguage").hide();
        $('div#tblLanguage .check').hide();
        $("#editLanguage").hide();
        $("#delLanguage").hide();
    }

    //if check all button clicked
    $("#languageCheckAll").click(function() {
        $("div#tblLanguage .chkbox").removeAttr("checked");
        if($("#languageCheckAll").attr("checked")) {
            $("div#tblLanguage .chkbox").attr("checked", "checked");
        }
    });

    //remove tick from the all button if any checkbox unchecked
    $("div#tblLanguage .chkbox").click(function() {
        $("#languageCheckAll").removeAttr('checked');
        if($("div#tblLanguage .chkbox").length == $("div#tblLanguage .chkbox:checked").length) {
            $("#languageCheckAll").attr('checked', 'checked');
        }
    });
    
    //hide already added languages and fluencys
    $("#language_code").change(function() {
        //show all the options to reseting hide options
        $("#language_lang_type option").each(function() {
            $(this).show();
        });
        $("#language_lang_type").val("");
        var $table_tr = $("#lang_data_table tr");
        var i=0;
        //hide already added optons for selected language
        $table_tr.each(function() {
            i++;
            if (i != 1) {           // skip heading tr
                if ($('#language_code').val() == $(this).find('td:eq(0)').find('input[class="code"]').val()){
                    $td = $(this).find('td:eq(0)').find('input[class="lang_type"]').val();
                    $("#language_lang_type option[value=" + $td + "]").hide();
                }
            }
        });        
    });
    
    $("#addLanguage").click(function() {
        removeEditLinks();
        clearMessageBar();
        $('div#changeLanguage label.error').hide();        
        

        //changing the headings
        $("#headChangeLanguage").text(lang_addLanguage);
        $("div#tblLanguage .chkbox").hide();
        $("#languageCheckAll").hide();

        //hiding action button section
        $("#actionLanguage").hide();

        $('#static_language_code').hide().val("");        
        $('#static_lang_type').hide().val("");
        $("#language_code").show().val("");
        $("#language_lang_type").show().val("");
        $("#language_comptency").val("");

        //show add form
        $("#changeLanguage").show();
        $("#languageRequiredNote").show();
        
        //show all the options to reseting hide options
        $("#language_lang_type option").each(function() {
            $(this).show();
        });
    });

    //clicking of delete button
    $("#delLanguage").click(function(){

        clearMessageBar();

        if ($("div#tblLanguage .chkbox:checked").length > 0) {
            $("#frmDelLanguage").submit();
        } else {
            $("#languageMessagebar").attr('class', 'messageBalloon_notice').text(lang_selectLanguageToDelete);
        }

    });

    $("#btnLanguageSave").click(function() {
        clearMessageBar();

        $("#frmLanguage").submit();
    });

    //form validation
    var languageValidator =
        $("#frmLanguage").validate({
        rules: {
            'language[code]': {required: true},
            'language[lang_type]': {required: true},
            'language[competency]': {required: true},
            'language[comments]' : {required: false, maxlength:100}
        },
        messages: {
            'language[code]': {required: lang_languageRequired},
            'language[lang_type]': {required: lang_languageTypeRequired},
            'language[competency]': {required: lang_competencyRequired},
            'language[comments]' : {maxlength: lang_commentsMaxLength}
        }
    });

    $("#btnLanguageCancel").click(function() {
        clearMessageBar();
                    addEditLinks();
                languageValidator.resetForm();
        
        $('div#changeLanguage label.error').hide();

        $("div#tblLanguage .chkbox").removeAttr("checked").show();
        
        //hiding action button section
        $("#actionLanguage").show();
        $("#changeLanguage").hide();
        $("#languageRequiredNote").hide();        
        $("#languageCheckAll").show();
        $('#static_language_code').hide().val("");
        $('#static_lang_type').hide().val("");
        
        //remove if disabled while edit
        $('#language_code').removeAttr('disabled');
        $('#language_lang_type').removeAttr('disabled');
    });
    
    $('form#frmDelLanguage a.edit').live('click', function(event) {
        event.preventDefault();
        clearMessageBar();

        //changing the headings
        $("#headChangeLanguage").text(lang_editLanguage);

        languageValidator.resetForm();

        $('div#changeLanguage label.error').hide();

        //hiding action button section
        $("#actionLanguage").hide();

        //show add form
        $("#changeLanguage").show();
        var parentRow = $(this).closest("tr");
                                
        var code = parentRow.find('input.code:first').val();

        var langType = parentRow.find('input.lang_type').val();
        var comments = $(this).closest("tr").find('td:last').html();
        
        $('#language_code').val(code).hide();
        $("#language_lang_type").val(langType).hide();
        var langTypeText = $("#language_lang_type option:selected").text();
        
        $('#static_language_code').text(parentRow.find('input.language_name').val()).show();
        $('#static_lang_type').text(langTypeText).show();
        
        $("#language_competency").val(parentRow.find('input.competency').val());
        $('#language_comments').val(comments);

        $("#languageRequiredNote").show();

        $("div#tblLanguage .chkbox").hide();
        $("#languageCheckAll").hide();        
    });
});

//]]>
</script>
				<!-- this is Licenses section -->

				<a name="license"></a>
				<div id="changeLicense" style="display: none;">
					<div class="head">
						<h1 id="headChangeLicense">Ajouter un permis</h1>
					</div>

					<div class="inner">
						<form id="frmLicense"
							action="/symfony/web/index.php/pim/saveDeleteLicense/empNumber/1/option/save"
							method="post" novalidate="novalidate">
							<fieldset>
								<ol>
									<li><label for="license_code">Type de permis<em>
												*</em></label> <select name="license[code]" id="license_code">
											<option value="" selected="selected">-- Sélectionner
												--</option>
									</select><span id="static_license_code" style="display: none;"></span></li>
									<li><label for="license_license_no">Numéro de
											permis</label> <input type="text" name="license[license_no]"
										id="license_license_no"></li>
									<li><label for="license_date">Date d'émission</label> <input
										id="license_date" type="text" name="license[date]"
										class="calendar hasDatepicker"><img
											class="ui-datepicker-trigger"
											src="<c:url value="/resources/images/calendar.png"/>"
											alt="" title=""/> <script type="text/javascript">

    var datepickerDateFormat = 'D, dd M yy';
    var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

    $(document).ready(function(){
        
        var dateFieldValue = $.trim($("#license_date").val());
        if (dateFieldValue == '') {
            $("#license_date").val(displayDateFormat);
        }

        daymarker.bindElement("#license_date",
        {
            showOn: "both",
            dateFormat: datepickerDateFormat,
            buttonImage: "/resources/images/calendar.png",
            buttonText:"",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+100",
            firstDay: 1,
            onClose: function() {
                $("#license_date").trigger('blur');
            }            
        });
        
        //$("img.ui-datepicker-trigger").addClass("editable");
        
        $("#license_date").click(function(){
            daymarker.show("#license_date");
            if ($(this).val() == displayDateFormat) {
                $(this).val('');
            }
        });
    
    });

</script></li>
									<li><label for="license_renewal_date">Date
											d'expiration</label> <input id="license_renewal_date" type="text"
										name="license[renewal_date]" class="calendar hasDatepicker"><img
											class="ui-datepicker-trigger"
											src="<c:url value="/resources/images/calendar.png"/>"
											alt="" title=""/> <script type="text/javascript">

    var datepickerDateFormat = 'D, dd M yy';
    var displayDateFormat = datepickerDateFormat.replace('yy', 'yyyy');

    $(document).ready(function(){
        
        var dateFieldValue = $.trim($("#license_renewal_date").val());
        if (dateFieldValue == '') {
            $("#license_renewal_date").val(displayDateFormat);
        }

        daymarker.bindElement("#license_renewal_date",
        {
            showOn: "both",
            dateFormat: datepickerDateFormat,
            buttonImage: "/resources/images/calendar.png",
            buttonText:"",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+100",
            firstDay: 1,
            onClose: function() {
                $("#license_renewal_date").trigger('blur');
            }            
        });
        
        //$("img.ui-datepicker-trigger").addClass("editable");
        
        $("#license_renewal_date").click(function(){
            daymarker.show("#license_renewal_date");
            if ($(this).val() == displayDateFormat) {
                $(this).val('');
            }
        });
    
    });

</script> <input value="1" type="hidden" name="license[emp_number]"
												id="license_emp_number"> <input type="hidden"
													name="license[_csrf_token]"
													value="36dc1c20437cc178d91f7b8e79d98032"
													id="license__csrf_token"></li>

									<li class="required"><em>*</em> Required field</li>
								</ol>
								<p>
									<input type="button" class="" id="btnLicenseSave"
										value="Sauver"> <input type="button" class="reset"
										id="btnLicenseCancel" value="Annuler">
								</p>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- changeLicense -->

				<div class="miniList" id="tblLicense">
					<div class="head">
						<h1>Permis</h1>
					</div>

					<div class="inner">






						<form id="frmDelLicense"
							action="/symfony/web/index.php/pim/saveDeleteLicense/empNumber/1/option/delete"
							method="post">
							<input type="hidden" name="defaultList[_csrf_token]"
								value="54f2b73f8b611384e7659cb2f5be4eed"
								id="defaultList__csrf_token">
								<p id="actionLicense">
									<input type="button" value="Ajouter" class="" id="addLicense">&nbsp;
										<input type="button" value="Effacer" class="delete"
										id="delLicense" style="display: none;">
								</p>
								<table id="" cellpadding="0" cellspacing="0" width="100%"
									class="table tablesorter">
									<thead>
										<tr>
											<th class="check" width="2%" style="display: none;"><input
												type="checkbox" id="licenseCheckAll"></th>
											<th>Type de permis</th>
											<th>Date d'émission</th>
											<th>Date d'expiration</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="check" style="display: none;"></td>
											<td>Aucun résultat</td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
						</form>


					</div>
				</div>
				<!-- miniList-tblLicense -->

				<script type="text/javascript">
    //<![CDATA[

    var fileModified = 0;
    var lang_addLicense = "Ajouter un permis";
    var lang_editLicense = "Modifier permis";
    var lang_licenseRequired = 'Requis';
    var lang_invalidDate = 'Doit être une date valide au format D, dd M yyyy';
    var lang_startDateAfterEndDate = "Date d'expiration doit être postérieure à la date de délivrance";
    var lang_selectLicenseToDelete = "Sélectionnez les enregistrements à supprimer";
    var lang_licenseNoMaxLength = "Doit être inférieur à niveau %montant%";

    var datepickerDateFormat = 'D, dd M yy';
    //]]>
</script>

				<script type="text/javascript">
//<![CDATA[

$(document).ready(function() {
    //To hide unchanged element into hide and show the value in span while editing
    $('#license_code').after('<span id="static_license_code" style="display:none;"></span>');

var issuedDate = "";
    function addEditLinks() {
        // called here to avoid double adding links - When in edit mode and cancel is pressed.
        removeEditLinks();
        $('form#frmDelLicense table tbody td.desc').wrapInner('<a class="edit" href="#"/>');
    }

    function removeEditLinks() {
        $('form#frmDelLicense table tbody td.desc a').each(function(index) {
            $(this).parent().text($(this).text());
        });
    }
    
    //hide add section
    $("#changeLicense").hide();
    $("#licenseRequiredNote").hide();

    //hiding the data table if records are not available
    if($("div#tblLicense .chkbox").length == 0) {
        //$("#tblLicense").hide();
        $('div#tblLicense .check').hide();
        $("#editLicense").hide();
        $("#delLicense").hide();
    }

    //if check all button clicked
    $("#licenseCheckAll").click(function() {
        $("div#tblLicense .chkbox").removeAttr("checked");
        if($("#licenseCheckAll").attr("checked")) {
            $("div#tblLicense .chkbox").attr("checked", "checked");
        }
    });

    //remove tick from the all button if any checkbox unchecked
    $("div#tblLicense .chkbox").click(function() {
        $("#licenseCheckAll").removeAttr('checked');
        if($("div#tblLicense .chkbox").length == $("div#tblLicense .chkbox:checked").length) {
            $("#licenseCheckAll").attr('checked', 'checked');
        }
    });

    $("#addLicense").click(function() {

        removeEditLinks();
        clearMessageBar();
        $('div#changeLicense label.error').hide();        
        

        //changing the headings
        $("#headChangeLicense").text(lang_addLicense);
        $("div#tblLicense .chkbox").hide();
        $("#licenseCheckAll").hide();

        //hiding action button section
        $("#actionLicense").hide();

        $('#static_license_code').hide().val("");        
        $("#license_code").show().val("");
        $("#license_code option[class='added']").remove();
        $("#license_major").val("");
        $("#license_year").val("");
        $("#license_gpa").val("");
        $("#license_date").val(displayDateFormat);
        $("#license_renewal_date").val(displayDateFormat);

        //show add form
        $("#changeLicense").show();
        $("#licenseRequiredNote").show();
    });

    //clicking of delete button
    $("#delLicense").click(function(){

        clearMessageBar();

        if ($("div#tblLicense .chkbox:checked").length > 0) {
            $("#frmDelLicense").submit();
        } else {
            $("#licenseMessagebar").attr('class', 'messageBalloon_notice').text(lang_selectLicenseToDelete);
        }

    });

    $("#btnLicenseSave").click(function() {
        clearMessageBar();
        issuedDate = $('#license_date').val();
        $("#frmLicense").submit();
    });

    //form validation
    var licenseValidator =
        $("#frmLicense").validate({
        rules: {
            'license[code]': {required: true},
            'license[license_no]': {required: false, maxlength: 50},
            'license[date]': {valid_date: function(){return {format:datepickerDateFormat, required:false, displayFormat:displayDateFormat}}},
            'license[renewal_date]': {valid_date: function(){return {format:datepickerDateFormat, required:false, displayFormat:displayDateFormat}}, date_range: function() {return {format:datepickerDateFormat, displayFormat:displayDateFormat, fromDate:issuedDate}}}
        },
        messages: {
            'license[code]': {required: lang_licenseRequired},
            'license[license_no]': {maxlength: lang_licenseNoMaxLength},
            'license[date]': {valid_date: lang_invalidDate},
            'license[renewal_date]': {valid_date: lang_invalidDate, date_range:lang_startDateAfterEndDate}
        }
    });

    $("#btnLicenseCancel").click(function() {
        clearMessageBar();
                    addEditLinks();
        
        licenseValidator.resetForm();

        $('div#changeLicense label.error').hide();

        $("div#tblLicense .chkbox").removeAttr("checked").show();

        //hiding action button section
        $("#actionLicense").show();
        $("#changeLicense").hide();
        $("#licenseRequiredNote").hide();        
        $("#licenseCheckAll").show();

        // remove any options already in use
        $("#license_code option[class='added']").remove();
        $('#static_license_code').hide().val("");

        //remove if disabled while edit
        $('#license_code').removeAttr('disabled');
    });
    
    $('form#frmDelLicense a.edit').live('click', function(event) {
        event.preventDefault();
        clearMessageBar();

        //changing the headings
        $("#headChangeLicense").text(lang_editLicense);

        licenseValidator.resetForm();

        $('div#changeLicense label.error').hide();

        //hiding action button section
        $("#actionLicense").hide();

        //show add form
        $("#changeLicense").show();
        var code = $(this).closest("tr").find('input.chkbox:first').val();
        
        $('#static_license_code').text($("#code_desc_" + code).val()).show();

        // remove any options already in use
        $("#license_code option[class='added']").remove();

        $('#license_code').
              append($("<option class='added'></option>").
              attr("value", code).
              text($("#code_desc_" + code).val())); 

        $('#license_code').val(code).hide();

        $("#license_license_no").val($("#license_no_" + code).val());
        $("#license_date").val($("#start_date_" + code).val());
        $("#license_renewal_date").val($("#end_date_" + code).val());
        
        if ($("#license_date").val() == '') {
            $("#license_date").val(displayDateFormat);
        }
        if ($("#license_renewal_date").val() == '') {
            $("#license_renewal_date").val(displayDateFormat);
        }        

        $("#licenseRequiredNote").show();

        $("div#tblLicense .chkbox").hide();
        $("#licenseCheckAll").hide();        
    });
});

//]]>
</script>




				<a name="attachments"></a>

				<div id="addPaneAttachments" style="display: none;">
					<div class="head" id="saveHeading">
						<h1>Ajouter une pièce jointe</h1>
					</div>
					<!-- head -->
					<div class="inner">





						<form name="frmEmpAttachment" id="frmEmpAttachment" method="post"
							enctype="multipart/form-data"
							action="/symfony/web/index.php/pim/updateAttachment/empNumber/1"
							novalidate="novalidate">

							<input type="hidden" name="_csrf_token"
								value="0070c8281ef13ba5170abd7e1baf482c" id="csrf_token">
								<input type="hidden" name="EmpID" value="1"> <input
									type="hidden" name="seqNO" id="seqNO" value=""> <input
										type="hidden" name="screen" value="qualifications"> <input
											type="hidden" name="commentOnly" id="commentOnly" value="0">

												<fieldset>
													<ol>
														<li id="currentFileLi" style="display: none;"><label>Current
																File</label> <span id="currentFileSpan"></span></li>
														<li class="fieldHelpContainer"><label
															id="selectFileSpan" style="height: 100%">Sélectionnez
																Fichier <em>*</em>
														</label> <input type="hidden" name="MAX_FILE_SIZE" value="1048576">
																<input type="file" name="ufile" id="ufile"> <label
																	class="fieldHelpBottom">Accepte jusqu'à 1 Mo</label></li>
														<li class="largeTextBox"><label>Commenter</label> <textarea
																name="txtAttDesc" id="txtAttDesc" rows="3" cols="35"></textarea>
														</li>
														<li class="required"><em>*</em> Required field</li>
													</ol>
													<p>
														<input type="button" name="btnSaveAttachment"
															id="btnSaveAttachment" value="Charger"> <input
															type="button" id="btnCommentOnly"
															value="Enregistrer un commentaire seulement"
															style="display: none;"> <input type="button"
																class="cancel" id="cancelButton" value="Annuler">
													</p>
												</fieldset>
						</form>
						<!-- frmEmpAttachment -->

					</div>
					<!-- inner -->
				</div>
				<!-- addPaneAttachments -->


				<div id="attachmentList" class="miniList">
					<div class="head">
						<h1>Les pièces jointes</h1>
					</div>
					<div class="inner">





						<form name="frmEmpDelAttachments" id="frmEmpDelAttachments"
							method="post"
							action="/symfony/web/index.php/pim/deleteAttachments/empNumber/1">

							<input type="hidden" name="employeeAttachmentDelete[_csrf_token]"
								value="1561e9c764feaf1420d0d2ad894b203f"
								id="employeeAttachmentDelete__csrf_token"> <input
								type="hidden" name="EmpID" value="1">

									<p id="attachmentActions">
										<input type="button" class="addbutton" id="btnAddAttachment"
											value="Ajouter">
									</p>
						</form>

					</div>
				</div>
				<!-- attachmentList -->

				<script type="text/javascript">
    //<![CDATA[
    
    var hideAttachmentListOnAdd = true;
    var lang_EditAttachmentHeading = "Modifier la pièce jointe";
    var lang_AddAttachmentHeading = "Ajouter une pièce jointe";
    var lang_SelectFile = "Sélectionnez Fichier";
    var lang_ReplaceWith = "Replace With";
    var lang_PleaseSelectAFile = "Requis";
    var lang_CommentsMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_SelectAtLeastOneAttachment = "Sélectionnez les enregistrements à supprimer";
    var hasError = false;

    var clearAttachmentMessages = true;
    
    $(document).ready(function() {
        
        $('#btnDeleteAttachment').attr('disabled', 'disabled');

        if (!hasError) {
            $('#addPaneAttachments').hide();
        }
        
        $('#currentFileLi').hide();
        $('#btnCommentOnly').hide();
        
        $("#frmEmpAttachment").data('add_mode', true);

        jQuery.validator.addMethod("attachment",
        function() {

            var addMode = $("#frmEmpAttachment").data('add_mode');
            if (!addMode) {
                return true;
            } else {
                var file = $('#ufile').val();
                return file != "";
            }
        }, ""
    );
    var attachmentValidator =
        $("#frmEmpAttachment").validate({

            rules: {
                ufile : {attachment:true},
                txtAttDesc: {maxlength: 200}
            },
            messages: {
                ufile: lang_PleaseSelectAFile,
                txtAttDesc: {maxlength: lang_CommentsMaxLength}
            }
            
        });

        //if check all button clicked
        $("#attachmentsCheckAll").click(function() {
            $("table#tblAttachments tbody input.checkboxAtch").removeAttr("checked");
            if($("#attachmentsCheckAll").attr("checked")) {
                $("table#tblAttachments tbody input.checkboxAtch").attr("checked", "checked");
            }
            
            if($('table#tblAttachments tbody .checkboxAtch:checkbox:checked').length > 0) {
                $('#btnDeleteAttachment').removeAttr('disabled');
            } else {
                $('#btnDeleteAttachment').attr('disabled', 'disabled');
            }
        });

        //remove tick from the all button if any checkbox unchecked
        $("table#tblAttachments tbody input.checkboxAtch").click(function() {
            $("#attachmentsCheckAll").removeAttr('checked');
            if($("table#tblAttachments tbody input.checkboxAtch").length == $("table#tblAttachments tbody input.checkboxAtch:checked").length) {
                $("#attachmentsCheckAll").attr('checked', 'checked');
            }
            
            if($('table#tblAttachments tbody .checkboxAtch:checkbox:checked').length > 0) {
                $('#btnDeleteAttachment').removeAttr('disabled');
            } else {
                $('#btnDeleteAttachment').attr('disabled', 'disabled');
            }
        });
        // Edit an attachment in the list
        $('#attachmentList a.editLink').click(function(event) {
            event.preventDefault();
            
            if (clearAttachmentMessages) {
                $("#attachmentsMessagebar").text("").attr('class', "");
            }
            
            attachmentValidator.resetForm();
            
            var row = $(this).closest("tr");            
            var fileName = row.find('a.fileLink').text();
            var seqNo;
            var description;
            
            var checkBox = row.find('input.checkboxAtch:first');
            if (checkBox.length > 0) {
                seqNo = checkBox.val();
                description = row.find("td:nth-child(3)").text();
            } else {
                seqNo = row.find('input[type=hidden]:first').val();
                description = row.find("td:nth-child(2)").text();                
            }
            description = jQuery.trim(description); 

            $('#seqNO').val(seqNo);
            $('#ufile').removeAttr("disabled");
            
            $('#txtAttDesc').val(description);

            $("#frmEmpAttachment").data('add_mode', false);

            $('#btnCommentOnly').show();

            // hide validation error messages
            $("label.error1col[generated='true']").css('display', 'none');
            $('#attachmentActions').hide();
            
            $("table#tblAttachments input.checkboxAtch").hide();
            
            $('#addPaneAttachments').show();
            $('#saveHeading h1').text(lang_EditAttachmentHeading);
            
            $('#currentFileLi').show();
            $('#currentFileSpan').text(fileName);
            $('#selectFileSpan').text(lang_ReplaceWith);
            
        });

        // Add a emergency contact
        $('#btnAddAttachment').click(function() {
            
            $('#currentFileLi').hide();
            $('#selectFileSpan').text(lang_SelectFile);
            
            if (clearAttachmentMessages) {
                $("#attachmentsMessagebar").text("").attr('class', "");
            }
            $('#seqNO').val('');
            $('#attachmentEditNote').text('');
            $('#txtAttDesc').val('');

            $("#frmEmpAttachment").data('add_mode', true);
            $('#btnCommentOnly').hide();

            // hide validation error messages
            $("label.error1col[generated='true']").css('display', 'none');
            
            $('#ufile').removeAttr("disabled");
            $('#attachmentActions').hide();
            $('#saveHeading h1').text(lang_AddAttachmentHeading);
            $('#addPaneAttachments').show();
            
            $("table#tblAttachments input.checkboxAtch").hide();
            $("table#tblAttachments a.editLink").hide();
            
            if (hideAttachmentListOnAdd) {
                $('#attachmentList').hide();
            }
            
        });
        
        $('#cancelButton').click(function() {
            $("#attachmentsMessagebar").text("").attr('class', "");
            
            attachmentValidator.resetForm();
            $('#addPaneAttachments').hide();
            $('#attachmentActions').show();
            $('#ufile').val('');
            $('#txtAttDesc').val('');
            $('#attachmentList').show();
            $("table#tblAttachments input.checkboxAtch").show();
            $("table#tblAttachments a.editLink").show();            
        });
        
        $('#btnDeleteAttachment').click(function() {

            var checked = $('#attachmentList input:checked').length;

            if (checked > 0) {
                $('#frmEmpDelAttachments').submit();
            }
            
        });

        $('#btnSaveAttachment').click(function() {
            $("#frmEmpAttachment").data('add_mode', true);
            $('#frmEmpAttachment').submit();
        });
        
        $('#btnCommentOnly').click(function() {
            $("#frmEmpAttachment").data('add_mode', false);
            $('#commentOnly').val('1');
            $('#frmEmpAttachment').submit();
        });
        
      
 
    //
    // Scroll to bottom if neccessary. Works around issue in IE8 where
    // using the <a name="attachments" is not sufficient
    //
    });
    //]]>
</script>
			</div>
			<!-- Box -->

			<script type="text/javascript">
    //<![CDATA[
    var fileModified = 0;
    var lang_addWorkExperience = "Ajouter expérience professionnelle";
    var lang_editWorkExperience = "Modifier l'expérience professionnelle";
    var lang_companyRequired = "Requis";
    var lang_jobTitleRequired = "Requis";
    var lang_invalidDate = 'Doit être une date valide au format D, dd M yyyy';
    var lang_commentLength = "Doit être inférieur à niveau %montant%";
    var lang_fromDateLessToDate = "À ce jour, devrait avoir lieu après la date de";
    var lang_selectWrkExprToDelete = "Sélectionnez les enregistrements à supprimer";
    var lang_jobTitleMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_companyMaxLength = "Doit être inférieur à niveau %montant%";
    var datepickerDateFormat = 'D, dd M yy';
    var canEdit = '1';
    //]]>
</script>
		</div>
		<!-- content -->

	</div>
	<!-- wrapper -->

	<div id="footer">
		CLINKAST ESS <br> © 2014 <a href="http://www.clinkast.fr"
			target="_blank">CLINKAST</a>. All rights reserved. 
	</div>
	<!-- footer -->



	<script type="text/javascript">

            $(document).ready(function() {                            
                
                /* Enabling tooltips */
                $(".tiptip").tipTip();

                /* Toggling header menus */
                $("#welcome").click(function () {
                    $("#welcome-menu").slideToggle("fast");
                    $(this).toggleClass("activated-welcome");
                    return false;
                });
                
                $("#help").click(function () {
                    $("#help-menu").slideToggle("fast");
                    $(this).toggleClass("activated-help");
                    return false;
                });
                
                $('.panelTrigger').outside('click', function() {
                    $('.panelContainer').stop(true, true).slideUp('fast');
                });                

                /* 
                 * Button hovering effects 
                 * Note: we are not using pure css using :hover because :hover applies to even disabled elements.
                 * The pseudo class :enabled is not supported in IE < 9.
                 */                
                $(document).on({
                    mouseenter: function () {
                        $(this).addClass('hover');                        
                    },
                    mouseleave: function () {
                        $(this).removeClass('hover');                        
                    }

                }, 'input[type=button], input[type=submit], input[type=reset]'); 
  
                /* Fading out main messages */
                $(document).on({
                    click: function() {
                        $(this).parent('div.message').fadeOut("slow");
                    }
                }, '.message a.messageCloseButton');                

                /* Toggling search form: Begins */
                //$(".toggableForm .inner").hide(); // Disabling this makes search forms to be expanded by default.

                $(".toggableForm .toggle").click(function () {
                    $(".toggableForm .inner").slideToggle('slow', function() {
                        if($(this).is(':hidden')) {
                            $('.toggableForm .tiptip').tipTip({content:'Expand for Options'});
                        } else {
                            $('.toggableForm .tiptip').tipTip({content:'Hide Options'});
                        }
                    });
                    $(this).toggleClass("activated");
                });
                /* Toggling search form: Ends */

                /* Enabling/disabling form fields: Begin */
                
                $('form.clickToEditForm input, form.clickToEditForm select, form.clickToEditForm textarea').attr('disabled', 'disabled');
                $('form.clickToEditForm input.calendar').datepicker('disable');
                $('form.clickToEditForm input[type=button]').removeAttr('disabled');
                
                $('form input.editButton').click(function(){
                    $('form.clickToEditForm input, form.clickToEditForm select, form.clickToEditForm textarea').removeAttr('disabled');
                    $('form.clickToEditForm input.calendar').datepicker('enable');
                });
                
                /* Enabling/disabling form fields: End */
                
            });
            
        </script>





	<div id="ui-datepicker-div"
		class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>
	<div id="tiptip_holder"
		style="max-width: 200px; margin: 383px 0px 0px 79px; display: none;"
		class="tip_top">
		<div id="tiptip_arrow" style="margin-left: 36.5px; margin-top: 22px;">
			<div id="tiptip_arrow_inner"></div>
		</div>
		<div id="tiptip_content">Change Photo</div>
	</div>
</body>
</html>