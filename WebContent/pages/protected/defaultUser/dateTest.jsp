<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Welcome" />
<c:set var="active" value="welcome" />
<%@ include file="../../templates/header.jsp"%>
<input type="text" class="monthpicker" data-start-year="1990" data-final-year="2016" data-selected-year="2014">


<script>
$(document).ready(function() {
	$('.monthpicker').monthpicker();
});
</script>
<%@ include file="../../templates/footer.jsp"%>