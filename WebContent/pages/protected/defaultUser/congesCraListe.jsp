<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Interface Conges" />
<c:set var="active" value="Resume list" />
<%@ include file="../../templates/header.jsp"%>

<script type="text/javascript">
	function confirmDelete() {
		return confirm("Voulez vous vraiment supprimer ce congé ?");
	}
</script>


<c:choose>
	<c:when test="${empty congesListe}">
		<p class="succes">Aucun congé demandé</p>
		<br />
		<a href="<c:url value="/gestionCongesDemande"/>">Faire une demande
			de congé</a>
	</c:when>
	<c:otherwise>

		<%-- <form method="get" action="<c:url value="/gestionCongesListe"/>"
			id="formConges"> --%>

		<fieldset>
			<legend>Liste de Conges</legend>

			<table class="table">
				<thead>
					<tr>
						<th>Type de congé</th>
						<th>Periode</th>
						<th>Nombre de jours</th>
						<th>Status Congé</th>
						<th>Suppression</th>
					</tr>

				</thead>
				<tbody>
					<c:forEach items="${congesListe}" var="conge" varStatus="myIndex">
						<c:choose>
							<c:when test="${conge.actif}">
								<c:choose>
									<c:when test="${conge.status == 0}">
										<tr>
									</c:when>
									<c:when test="${conge.status == 1}">
										<tr class="congeValide">
									</c:when>
									<c:otherwise>
										<tr class="congeNonValide">
									</c:otherwise>
								</c:choose>

							</c:when>
						</c:choose>
						<td><c:out value="${conge.typeConge}" /></td>
						<td><c:out value="${conge.periode}" /></td>
						<td><c:out value="${conge.total_jour}" /></td>
						<c:choose>
							<c:when test="${conge.actif}">
								<c:choose>
									<c:when test="${conge.status == 0}">
										<td>En attente de validation</td>
									</c:when>
									<c:when test="${conge.status == 1}">
										<td>Validé</td>
									</c:when>
									<c:otherwise>
										<td>Non validé</td>
									</c:otherwise>
								</c:choose>

							</c:when>
						</c:choose>


						<td><form id="frmTitle"
								action="<c:url value="/deleteConge"> <c:param name="idConge" value="${conge.id}" /></c:url>"
								method="post">

								<a class="infobulle" title="Suppression"><input
									type="submit" class="" id="btnResumeDel" value=""
									onclick="return confirmDelete()"></a>
							</form></td>

						</tr>
					</c:forEach>
				</tbody>
			</table>

		</fieldset>
		<!-- 	</form> -->

	</c:otherwise>
</c:choose>

<%--For displaying Previous link except for the 1st page --%>
<c:if test="${currentPage != 1}">
	<td><a
		href="<c:url value="/gestionCongesListe"><c:param name="page" value="${currentPage - 1}" /></c:url>">Précédent</a></td>

</c:if>

<%--For displaying Page numbers. 
    The when condition does not display a link for the current page--%>
<c:if test="${noOfPages > 1}">
	<table cellpadding="5" cellspacing="5">
		<tr>
			<c:forEach begin="1" end="${noOfPages}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<td>${i}</td>
					</c:when>
					<c:otherwise>

						<td><a
							href="<c:url value="/gestionCongesListe"><c:param name="page" value="${i}" /></c:url>">${i}</a></td>

					</c:otherwise>
				</c:choose>
			</c:forEach>
		</tr>
	</table>
</c:if>
<%--For displaying Next link --%>

<c:if test="${currentPage lt noOfPages}">
	<td><a
		href="<c:url value="/gestionCongesListe"><c:param name="page" value="${currentPage + 1}" /></c:url>">Suivant</a></td>
</c:if>


<%@ include file="../../templates/footer.jsp"%>