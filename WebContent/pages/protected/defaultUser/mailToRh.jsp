<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="title" value="Formulaire de contact" />
<c:set var="active" value="Contact form" />
<%@ include file="../../templates/header.jsp"%>
<script>
var nb=1;
function checkfile(sender){
    var invalidExts = new Array(".ade", ".adp", ".bat", ".chm", ".cmd", ".com", ".cpl", ".exe", ".hta", ".ins", ".isp", ".jse",
    		".lib", ".lnk", ".mde", ".msc", ".msp", ".mst", ".pif", ".scr", ".sct", ".shb", ".sys", ".vb", ".vbe", ".vbs"
    		, ".vxd", ".wsc", ".wsf", ".wsh");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    //For Indexof() -IE < 9
    if (!Array.prototype.indexOf){
        Array.prototype.indexOf = function (elt /*, from*/)
        {
            var len = this.length >>> 0;
            var from = Number(arguments[1]) || 0;
            from = (from < 0) ? Math.ceil(from) : Math.floor(from);
            if (from < 0)
                from += len;
            for (; from < len; from++){
                if (from in this && this[from] === elt)
                    return from;
                }
            return -1;
        };
    }

    if (invalidExts.indexOf(fileExt) >= 0)
    {
    	alert("Invalid file selected,files of this type cannot be send.");
        sender.value = '';
        return false;
    }
    else
    {
        return true;

    }
}


function loadFile(){
	// $("#joint").click(function(){ 
		var tr = $("#attachment");
		var myClone = tr.clone();
		myClone.attr("id", "attachement_"+nb);
		$("#filestojoint").append(myClone);
		document.getElementById("attachement_"+nb).style.display="none";	
		nb++;
		return false;
	//});
}
</script>

<table>
	<tr class="odd">
		<td class="none">
			<form method="post" ENCTYPE='multipart/form-data'
				action="<c:url value="/contactMail"></c:url>">
				<table>
					<h2>Mail API</h2>

					<tr>
						<td class="none"><b>Subject:</b></td>
						<td class="none"><input type="text" name="subject" size="70"> <span
							class="erreur">${form.erreurs['subject']}</span></td>
					</tr>
					<tr>
						<td class="none"><b>Description:</b></td>
						<td class="none"><textarea name="body" type="text" cols="90" rows="15"
								size=100>
  </textarea> <span class="erreur">${form.erreurs['body']}</span></td>
					</tr>
					<tr>
						<td class="none"><b>Joindre des fichiers:</b></td>
						<td class="none" id="filestojoint">
							<!--  <input type="button" id ="joint" value="+" onclick="loadFile()" /> -->
							<input type="file" name="attachment" id="attachment" size="30"
							onchange="checkfile(this);" /><br>
						</td>
					</tr>
					<tr>
						<td class="none"><p>
								<input type="submit" value="Send Mail" name="sendMail"></td>
					</tr>
				</table>

			</form>
		</td>
	</tr>
</table>
<%@ include file="../../templates/footer.jsp"%>