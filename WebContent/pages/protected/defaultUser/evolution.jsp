<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul>
	<li class="largeTextBox">
	 	<label for="evolutions_souhaitsCourtMoyen">Quels sont mes souhaits d’évolution à court/moyen terme (2/3 ans) ? (Orientations métier, type de
mission, sujets abordés, spécialisation</label>
		<textarea rows="4" cols="28" name="evolutions[souhaitsCourtMoyen]" class="formInputText" id="evolutions_souhaitsCourtMoyen"></textarea>
	</li>
	<li class="largeTextBox">
	 	<label for="evolutions_elemetsImportant">Quelles satisfactions je retire de mon métier de consultant? Qu’est-ce qui me motive ? Quels sont
les éléments auxquels j’attache de l’importance ?</label>
		<textarea rows="4" cols="28" name="evolutions[elemetsImportant]" class="formInputText" id="evolutions_elemetsImportant"></textarea>
	</li>
</ul>
		<p>
		<input type="button" class="" id="btnWorkAutresExigenceSauve" value="Sauver">
		<input type="button" class="reset" id="btnWorkAutresExigenceCancel" value="Annuler">
	</p>	
