<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<c:set var="title" value="Formulaire d'auto-évaluation" />
<c:set var="active" value="Complete your resume" />
<%@ include file="../../templates/header.jsp"%>

<div class="box pimPane">
	<c:if test="${!empty form }">
		<div id="slickbox"><div class="${empty form.erreurs ? 'successs' :'errors'}"><p>${form.resultat}</p></div></div>
	</c:if>
	<!-- this is title section -->
		<div class="inner">
			<table class="table">
				<thead>
					<tr>
						<th>Libellé</th>
						<th>Date réception</th>
						<th>Date modification</th>
						<th>PDF</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="name">
							<c:out value="Auto-Evaluation ${ gestionEvaluations.dates.datesPeriodes}" /></td>
						<td><joda:format value="${gestionEvaluations.gestionEvaluationsDateReception}" pattern="dd/MM/yyyy"/></td>
						<td><joda:format value="${gestionEvaluations.gestionEvaluationsDateModification}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
						<td>
							<a class="infobulle" title = "Voir le pdf" href="<c:url value="/viewPDF"><c:param name="id" value="${gestionEvaluations.dates.datesId}&${gestionEvaluations.autoEvaluations.autoEvaluationsId}" /></c:url>" target="blank">
								<img src="<c:url value="/resources/images/logo-pdf.jpg"/>" alt="voir le pdf" />
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<h4><a id="act" href="#">Activité de l'année écoulée</a> || <a id="exi" href="#">Exigences de la fonction</a> || <a id = "evo" href="#">Evoultion</a> || <a id = "res" href="#">Résumé</a></h4>
		<!-- Section activités -->
		<div id="activites">
			<!-- Sous section synthese de l'activité -->
			<div class="module" >
				<h3>1.1 &nbsp;Synthèse de l’activité (Pour chaque mission quelles sont mes réalisations ?)</h3>
				<p>	<button id="addAct">Ajouter une activité</button>
					<button id="delAct">Supprimer</button>
				</p>
				<c:import url="form_activites.jsp" />
				<c:import url="list_activites.jsp" />				
			</div>
			
			<!-- Sous section exigences -->
			<div class="module" >
				<h3>1.2 &nbsp;Bilan sur l’apport de moyens (Formation, accompagnement, suivi de mission)</h3>
				<p>	<button id="addFms">Ajouter une formation suivie</button>
					<button id="delFms">Supprimer</button>
				</p>
				<c:import url="form_formation.jsp" />
				<c:import url="list_formationSuivie.jsp" />		
			</div>
			
			<!-- Sous section Objectifs -->
			<div class="module" >
				<h3>1.3 &nbsp;Atteinte de mes objectifs (Pour chaque objectif quel niveau de résultat j’estime avoir atteint ?)</h3>
				<p>	<button id="addObj">Ajouter un objectif</button>
					<button id="delObj">Supprimer</button>
				</p>
				<c:import url="form_objectif.jsp" />
				<c:import url="list_objectifs.jsp" />		
			</div>
		</div>
		<!-- End section activité -->
		
		<!-- Section Exigences -->
		<div id="exigences">
			<!-- Sous section Exigences de la fonction -->
			<div class="module" >
				<h3>Exigences de la fonction : Par rapport au Guide de Compétences sur quels critères ai-je évolué, progressé ?</h3>
				<c:import url="list_axesExigences.jsp" />
				<p>	<button id="modExi">Editer</button>
					<button id="lecExi">Lecture</button>
					<button id="engExi">Enregistre</button>
				</p>
				<c:import url="form_axes.jsp" />
			</div>
		</div>
		<!-- End section Exigences -->
		
		<!-- Section Evolution -->
		<div id="evolution">
			<div class="module">
				<h3>Actions de formation demandées</h3>
				<p>	<button id="addFdd">Ajouter une formation demandée</button>
					<button id="delFdd">Supprimer</button>
				</p>
				<c:import url="form_formationsDemandes.jsp" />
				<c:import url="list_formationsDemandes.jsp" />	
			</div>
		</div>
		<!-- End section Evolution -->
		
		<!-- Section Resume -->
		<div id="resume">
			<div class="module">
				<h3>Resumé</h3>
				<p>	
					<button id="modRes">Editer</button>
					<button id="lecRes">Lecture</button>
					<button id="engRes">Enregistre</button>
				</p>
				<c:import url="list_resume.jsp" />	
			</div>
		</div>
</div>		
<script>
$(document).ready(function(){
	setTimeout(function(){
	 	 $('#slickbox').hide('slow');
		}, 3000);
	$('#axe option[value=""]').prop('selected', true);
	$("form").resetForm();
	$('.monthpicker').monthpicker();
	$("#activites").hide();
	$("#exigences").hide();
	$("#evolution").hide();	
	$("#resume").hide();
	$("#changeTitle").hide();

	$("#act").click(function(){
		$("#exigences").hide();
		$("#evolution").hide();
		$("#resume").hide();
		$("#delAct").hide();
		$("#delFms").hide();
		$("#delObj").hide();
    	$("#activites").show();
  	});

  	$("#exi").click(function(){
  		$("#activites").hide();
  		$("#evolution").hide();
  		$("#resume").hide();
	    $("#exigences").show();
	    $("#modExi").hide();
	    $("#lecExi").hide();
	    $("#engExi").hide();
	  });

	$("#evo").click(function(){
		$("#activites").hide();
		$("#exigences").hide();
		$("#resume").hide();
	    $("#evolution").show();
	    $("#delFdd").hide();
	  });

  	$("#res").click(function(){
  		$("#activites").hide();
  		$("#exigences").hide();
  		$("#evolution").hide();
	    $("#resume").show();
	    $("#modRes").show();
		$("#engRes").hide();
		$("#lecRes").hide();
	  });
	/*Section Formation demandées*/
	$("#addFdd").click(function(){
		$('#allFormationDemandes').attr('checked', false);
		$('#headChangeFormationDemandes').text("Ajouter une formation demandée");
		$("#changeFormationDemandes").show();
  		$("#addFdd").hide();
  		$("#delFdd").hide();
  		$("input[name='formationDemande_id_formations']").each(function(){
			this.checked = "";
		});
  		$('#allFormationDemandes').attr('checked', false);
	});

	$("#btnWorkFormationDemandesCancel").click(function(){
		//validatorFormationSuivies.resetForm();
  		$("#addFdd").show();  		
  		$("#changeFormationDemandes").hide(); 	    
  	  });

	$("#btnWorkFormationDemandesSauve").click(function(){
 		$("#formWorkFormationDemandesSauve").submit(); 	    
 	  });

	var validatorFormationDemandes = $("#formWorkFormationDemandesSauve").validate({
		rules:{
			'formationDemandes[intitule]':{
				required: true,
				minlength: 3,
				maxlength: 150
			},
			'formationDemandes[objectif]':{
				required: true,
				minlength: 3,
				maxlength: 2000
			}
		},
		messages:{
			'formationDemandes[intitule]':{
				required: " Saisir l'intitulé de la formation",
				minlength: "Ce champs doit avoir plus de 3 lettres",
				maxlength: "Ce champs ne doit pas dépasser 150 lettres"
			},
			'formationDemandes[objectif]':{
				required: " Saisir l'objectif de la formation",
				minlength: "Ce champs doit avoir plus de 2 lettres",
				maxlength: "Ce champs ne doit pas dépasser 30 lettres"
			}
		}
	});

	//Click sur une colonne du tableau
  	$(".dataFdd td").click(function(){
  			
	  		var cell = $(this).prop('cellIndex');
	  		if(cell == 0)
	  	  	{
	  	  	  	//alert(cell);
	  	  	}
	  	  	else
	  	  	{
  	  			var formationDemandes_intitule = $("td:eq(1)", $(this).parent()).html();
  	  			var formationDemandes_objectif = $("td:eq(2)", $(this).parent()).html();
  	  			var formationDemande_id_formations = $("input[name='formationDemande_id_formations']", $(this).parent()).val();

  	  		$("#formationDemandes_intitule").val(formationDemandes_intitule);
	  	  	$("#formationSuivies_sauve").val("sauve");
  	  		$('#formationDemandes_objectif').val(formationDemandes_objectif);
  	  	 	$('#formationDemande_id_formations').val(formationDemande_id_formations);
  	  					
			$('#headChangeFormationDemandes').text("Modifier la formation demandée");
  	  		$("#addFdd").hide();
  	  		$("#delFdd").hide();  	  		
  	  		$("input[name='formationDemande_id_formations']").each(function(){
				this.checked = "";
			});
	  	  	$("#changeFormationDemandes").show();
	    	
  	  	}
	  	});

  		//Selectionner toutes les cases à cocher
		$("#allFormationDemandes").click(function(){
			var checked_status = this.checked;
			$("input[name='formationDemande_id_formations']").each(function(){
				this.checked = checked_status;
			});
			validatorFormationDemandes.resetForm();
  			$("#changeFormationDemandes").hide();
  			$("#delFdd").show();
  	    
	  	});

	  	/*traitement lorsqu'on clique sur un checkbox*/
	  	$("input:checkbox").click(function(){
	  		
	  		if($("input[name='formationDemande_id_formations']", $(this).parent()).prop("checked"))
	  		{
	  			$("#delFdd").show();
	  			$("#addFdd").show();
	  			$("#changeFormationDemandes").hide();
	  		}
	  		else
	  	  	{
	  	  	  	if($("input[name='formationDemande_id_formations']:checked").length == 0){
	  	  	  		$("#delFdd").hide();
	  	  	  		$("#allFormationDemandes").removeAttr("checked");
	  	  	  	}
	  	  	  		
	  	  	}
	  	});
	  	/*traitement du click sur le bouton supprimer*/
		$("#delFdd").click(function()
		{
			if (confirm("Voulez vous supprimer ses données?")) {
				$("#formWorkFormationsDemandesDelete").submit();
	   		}
		});
	
  	/*Section OBJECTIF*/
	$("#addObj").click(function(){
		$('#headChangeObjectifs').text("Ajouter un objectif");
		$("#changeFormationSuivies").hide();
  		$("#changeObjectifs").show();
  		$("#changeActivites").hide();
  		$("#addAct").show();
  		$("#addFms").show();
  		$("#addObj").hide();
  		$("#delObj").hide();
  		$("input[name='objectifs_id_objectif']").each(function(){
			this.checked = "";
		});
  		$('#allObjectifs').attr('checked', false);
  		
	});

	$("#btnWorkObjectifCancel").click(function(){
		//validatorFormationSuivies.resetForm();
  		$("#addFms").show();  		
  		$("#addAct").show();
  		$("#addObj").show();
  	    $("#changeObjectifs").hide(); 	    
  	  });

	$("#btnWorkObjectifSauve").click(function(){
 		$("#formWorkObjectifs").submit(); 	    
 	  });

	var validatorObjectifs = $("#formWorkObjectifs").validate({
		rules:{
			'objectif[name]':{
				required: true,
				minlength: 3,
				maxlength: 150
			},
			'objectif[estimation]':{
				required: true				
			}
		},
		messages:{
			'objectif[name]':{
				required: " Saisir l' objectif initial",
				minlength: "Ce champs doit avoir plus de 3 lettres",
				maxlength: "Ce champs ne doit pas dépasser 150 lettres"
			},
			'objectif[estimation]':{
				required: " Selectionner l' estimation"
			}
		}
	});

	//Click sur une colonne du tableau
  	$(".dataObj td").click(function(){
	  		var cell = $(this).prop('cellIndex');
	  		if(cell == 0)
	  	  	{
	  	  	  	//alert(cell);
	  	  	}
	  	  	else
	  	  	{
  	  			var objectif_name = $("td:eq(1)", $(this).parent()).html();
  	  			var objectif_estimation = $("td:eq(2)", $(this).parent()).html();
  	  			var objectifs_id_objectif = $("input[name='objectifs_id_objectif']", $(this).parent()).val();

  	  		$("#objectif_name").val(objectif_name);
	  	  	$("#objectifs_sauve").val("sauve");
  	  		$('#objectifs_id_objectif').val(objectifs_id_objectif);
  	  	 				
			$('#headChangeObjectifs').text("Modifier l'objectif");
  	  		$("#addObj").hide();
  	  	    $("#delObj").hide();
	  	  	$("input[name='objectifs_id_objectif']").each(function(){
				this.checked = "";
			});
  	  		$("#changeObjectifs").show();
  	    	$("#changeFormationSuivies").hide();
  	    	$("#changeActivites").hide();
  	    	$("#addFms").show();
  	    	$("#addAct").show();
  	    	
  	  	}
	  	});

  		//Selectionner toutes les cases à cocher
		$("#allObjectifs").click(function(){
			var checked_status = this.checked;
			$("input[name='objectifs_id_objectif']").each(function(){
				this.checked = checked_status;
			});
			validatorObjectifs.resetForm();
  			$("#changeObjectifs").hide();
  			$("#delObj").show();
  	    
	  	});

	  	/*traitement lorsqu'on clique sur un checkbox*/
	  	$("input:checkbox").click(function(){
	  		
	  		if($("input[name='objectifs_id_objectif']", $(this).parent()).prop("checked"))
	  		{
		  		$("#delObj").show();
		  		$("#addObj").show();
		  		$("#changeFormationSuivies").hide();
	  			$("#changeActivites").hide();
	  			$("#changeObjectifs").hide();
	  		}
	  		else
	  	  	{
	  	  	  	if($("input[name='objectifs_id_objectif']:checked").length == 0){
	  	  	  		$("#delObj").hide();
	  	  	  		$("#allObjectifs").removeAttr("checked");
	  	  	  	}
	  	  	  		
	  	  	}
	  	});
	  	/*traitement du click sur le bouton supprimer*/
		$("#delObj").click(function()
		{
			if (confirm("Voulez vous supprimer ses données?")) {
				$("#formWorkObjectifsDelete").submit();
	   		}
		});
  	
	  
	/*Section FORMATION SUIVIE*/
	$("#addFms").click(function(){
		$('#headChangeFormationSuivies').text("Ajouter une formation suivie");
		$("#changeFormationSuivies").show();
  		$("#changeObjectifs").hide();
  		$("#changeActivites").hide();
  		$("#addAct").show();
  		$("#addFms").hide();
  		$("#addObj").show();
  		$("#delFms").hide();
  		$("input[name='formation_id_formations']").each(function(){
			this.checked = "";
		});
  		$('#allFormationSuivies').attr('checked', false);
	});

	$("#btnWorkFormationSuiviesCancel").click(function(){
		//validatorFormationSuivies.resetForm();
  		$("#addFms").show();  		
  		$("#addAct").show();
  		$("#addObj").show();
  	    $("#changeFormationSuivies").hide(); 	    
  	  });

	$("#btnWorkFormationSuiviesSauve").click(function(){
 		$("#formWorkFormationSuivies").submit(); 	    
 	  });

	var validatorFormationSuivies = $("#formWorkFormationSuivies").validate({
		rules:{
			'formationSuivies[intitule]':{
				required: true,
				minlength: 3,
				maxlength: 150
			},
			'formationSuivies[commanetaire]':{
				required: true,
				minlength: 3,
				maxlength: 2000
			}
		},
		messages:{
			'formationSuivies[intitule]':{
				required: " Saisir l'intitulé de la formation",
				minlength: "Ce champs doit avoir plus de 3 lettres",
				maxlength: "Ce champs ne doit pas dépasser 150 lettres"
			},
			'formationSuivies[commanetaire]':{
				required: " Saisir l'intitulé de la formation",
				minlength: "Ce champs doit avoir plus de 2 lettres",
				maxlength: "Ce champs ne doit pas dépasser 30 lettres"
			}
		}
	});

	//Click sur une colonne du tableau
  	$(".dataFms td").click(function(){
  			
	  		var cell = $(this).prop('cellIndex');
	  		if(cell == 0)
	  	  	{
	  	  	  	//alert(cell);
	  	  	}
	  	  	else
	  	  	{
  	  			var formationSuivies_intitule = $("td:eq(1)", $(this).parent()).html();
  	  			var formationSuivies_commanetaire = $("td:eq(2)", $(this).parent()).html();
  	  			var formationSuivies_id_formationSuivie = $("input[name='formation_id_formations']", $(this).parent()).val();

  	  		$("#formationSuivies_intitule").val(formationSuivies_intitule);
	  	  	$("#formationSuivies_sauve").val("sauve");
  	  		$('#formationSuivies_commanetaire').val(formationSuivies_commanetaire);
  	  	 	$('#formationSuivies_id_formationSuivie').val(formationSuivies_id_formationSuivie);
  	  					
			$('#headChangeFormationSuivies').text("Modifier la formation suivie");
  	  		$("#addFms").hide();
  	  		$("#delFms").hide();  	  		
  	  		$("#changeObjectifs").hide();
	  	  	$("input[name='formation_id_formations']").each(function(){
				this.checked = "";
			});
	  	  $("#changeObjectifs").hide();
	    	$("#changeFormationSuivies").show();
	    	$("#changeActivites").hide();
	    	$("#addObj").show();
  	    	$("#addAct").show();
  	  	}
	  	});

  		//Selectionner toutes les cases à cocher
		$("#allFormationSuivies").click(function(){
			var checked_status = this.checked;
			$("input[name='formation_id_formations']").each(function(){
				this.checked = checked_status;
			});
			validatorFormationSuivies.resetForm();
  			$("#changeFormationSuivies").hide();
  			$("#delFms").show();
  	    
	  	});

	  	/*traitement lorsqu'on clique sur un checkbox*/
	  	$("input:checkbox").click(function(){
	  		
	  		if($("input[name='formation_id_formations']", $(this).parent()).prop("checked"))
	  		{
	  			$("#delFms").show();
	  			$("#addFms").show();
	  			$("#changeFormationSuivies").hide();
	  			$("#changeActivites").hide();
	  			$("#changeObjectifs").hide();
	  		}
	  		else
	  	  	{
	  	  	  	if($("input[name='formation_id_formations']:checked").length == 0){
	  	  	  		$("#delFms").hide();
	  	  	  		$("#allFormationSuivies").removeAttr("checked");
	  	  	  	}
	  	  	  		
	  	  	}
	  	});
	  	/*traitement du click sur le bouton supprimer*/
		$("#delFms").click(function()
		{
			if (confirm("Voulez vous supprimer ses données?")) {
				$("#formWorkFormationsSuiviesDelete").submit();
	   		}
		});

	
	  
	/*section ACTIVITE*/
  	//Valide the activities form when it's submitted
  	$("#addAct").click(function(){
  		$('#headChangeActivites').text("Ajouter une activité");
  		$("#changeFormationSuivies").hide();
  		$("#changeObjectifs").hide();
  		$("#changeActivites").show();
  		$("#delAct").hide();
  		$("#addAct").hide();
  		$("#addFms").show();
  		$("#addObj").show();
  		$("input[name='activites_id_activite']").each(function(){
			this.checked = "";
		});
  		$('#all').attr('checked', false); 	    
  	  });

  	$("#btnWorkActvititeCancel").click(function(){
  		validatorActivites.resetForm();
  		$("#addFms").show();  		
  		$("#addAct").show();
  		$("#addObj").show();
  	    $("#changeActivites").hide();  	    
  	  });

  	$("#btnWorkActvititeSauve").click(function(){
  		$("#formWorkActivites").submit();  	    
  	  });


	  	var validatorActivites = $("#formWorkActivites").validate({
	rules: {
		'activites[periode_de]':{
			required: true
			},
		'activites[periode_au]':{
			required: true
			},
		'activites[missions]':{
			required: true,
			minlength: 3,
			maxlength: 2000
			},
		'activites[responsable]':{
			required: true,
			minlength: 3,
			maxlength: 30
			},
		'activites[entretien]':{
			required: true
			}
		},
	messages:{
		'activites[periode_de]':{
			required: "Selectionner une date"
		},
		'activites[periode_au]':{
			required: "Selectionner une date"
		},
		'activites[missions]':{
			required: " Saisir les missions et les réalisations effectuées",
			minlength: "Ce champs doit avoir plus de 3 lettres",
			maxlength: "Ce champs ne doit pas dépasser 255 lettres"
		},
		'activites[responsable]':{
			required: "ce champs est obligatoire",
			minlength: "Ce champs doit avoir plus de 2 lettres",
			maxlength: "Ce champs ne doit pas dépasser 30 lettres"
		},
		'activites[entretien]':{
			required: "Veuiller cocher une case"
		}
	}
	  	});

	//Click sur une colonne du tableau
  	$(".data td").click(function(){
	  		var cell = $(this).prop('cellIndex');
	  		if(cell == 0)
	  	  	{
	  	  	  	//alert(cell);
	  	  	}
	  	  	else
	  	  	{
  	  		var periode = ($("td:eq(1)", $(this).parent()).html()).split(" au ");
  	  		var activites_periode_de = periode[0];
  	  		var activites_periode_au = periode[1];
  	  		var activites_missions = $("td:eq(2)", $(this).parent()).html();
  	  		var activites_responsable = $("td:eq(3)", $(this).parent()).html();
  	  		var activites_entretien = $("td:eq(4)", $(this).parent()).html();
  	  		var activite_id_activite = $("input[name='activites_id_activite']", $(this).parent()).val();

  	  		$("#activites_id_activite").val(activite_id_activite);
	  	  	$("#activites_sauve").val("sauve");
  	  		$('#activites_periode_de').val(activites_periode_de);
  	  	 	$('#activites_periode_au').val(activites_periode_au);
  	  		$('#activites_missions').val(activites_missions);
  	  		$('#activites_responsable').val(activites_responsable);

  	  		if(activites_entretien == "Oui")
			{
				$('#activites_entretien_oui').attr('checked','checked');
			}
			else{
				$('#activites_entretien_non').attr('checked','checked');
			}
			
			$('#headChangeActivites').text("Modifier l'activité");
  	  		$("#addAct").hide();
  	  		$("#delAct").hide();
	  	  	$("input[name='activites_id_activite']").each(function(){
				this.checked = "";
			});
			
  	  		$("#changeObjectifs").hide();
	    	$("#changeFormationSuivies").hide();
	    	$("#changeActivites").show();
	    	$("#addObj").show();
  	    	$("#addFms").show();
  	  	}
	  	});

    //Selectionner toutes les cases à cocher
	$("#all").click(function(){
		var checked_status = this.checked;
		$("input[name='activites_id_activite']").each(function(){
			this.checked = checked_status;
		});		
		$("#delAct").show();
  		$("#changeActivites").hide(); 		
  	    
	  	});
	  	/*traitement lorsqu'on clique sur un checkbox*/
	  	$("input:checkbox").click(function(){
	  		
	  		if($("input[name='activites_id_activite']", $(this).parent()).prop("checked"))
	  		{	
		  		$("#delAct").show();
		  		$("#addAct").show();
		  		$("#changeFormationSuivies").hide();
	  			$("#changeActivites").hide();
	  			$("#changeObjectifs").hide();
	  		}
	  		else
	  	  	{
	  	  	  	if($("input[name='activites_id_activite']:checked").length == 0){
	  	  	  		$("#delAct").hide();
	  	  	  		$("#all").removeAttr("checked");
	  	  	  	}
	  	  	  		
	  	  	}
	  	});
	  	
	/*traitement du click sur le bouton supprimer*/
	$("#delAct").click(function()
	{
		if (confirm("Voulez vous supprimer ses données?")) {
			$("#formWorkActivitesDelete").submit();
   		}
	});
	  	
	/*FIN SECTION ACTIVITE*/

	/*Section Exigence*/
	$("#axe").on('change', function (e) {
	    var valueSelected = this.value;
		if(valueSelected == "Clt")
		{
			$("#client").show();
			$("#consultant").hide();
			$("#engagement").hide();
			$("#developpement").hide();
			
		}
		else if(valueSelected == "Ctt")
		{
			$("#client").hide();
			$("#consultant").show();
			$("#engagement").hide();
			$("#developpement").hide();
		}
		else if(valueSelected == "Egt")
		{
			$("#client").hide();
			$("#consultant").hide();
			$("#engagement").show();
			$("#developpement").hide();
		}
		else if(valueSelected == "Dev")
		{
			$("#client").hide();
			$("#consultant").hide();
			$("#engagement").hide();
			$("#developpement").show();
		}
		else{
			$("#client").hide();
			$("#consultant").hide();
			$("#engagement").hide();
			$("#developpement").hide();
		}

		if(valueSelected == "Dev" || valueSelected == "Egt" || valueSelected == "Ctt" || valueSelected == "Clt"){
			$("#modExi").show();
			$(".view").show();
			$(".edit").hide();
			$("#lecExi").hide();
			$("#engExi").hide();
					
		}
		else
		{
			$("#modExi").hide();
			$("#lecExi").hide();
		}	
	});

	/*Clic sur le bouton editer*/
	$("#modExi").click(function(){
		$(".edit").show();
		$("#lecExi").show();
		$("#engExi").show();
		$("#modExi").hide();
		$(".view").hide();
	});

	$("#lecExi").click(function(){
		$(".edit").hide();
		$(".view").show();
		$("#lecExi").hide();
		$("#modExi").show();
		$("#engExi").hide();
	});

	$("#engExi").click(function(){
		if($("#consultant").is(":visible")){
			$("#formCtt").submit();
		}
		if($("#client").is(":visible")){
			$("#formClt").submit();
		}
		if($("#engagement").is(":visible")){
			$("#formEng").submit();
		}
		if($("#developpement").is(":visible")){
			$("#formDev").submit();
		}
	});

	/*Fin SECTION EXIGENCES*/
	
	/*Section RESUME*/
	
	/*Clic sur le bouton editer*/
	$("#modRes").click(function(){
		$(".edit").show();
		$("#lecRes").show();
		$("#engRes").show();
		$("#modRes").hide();
		$(".view").hide();
	});

	$("#lecRes").click(function(){
		$(".edit").hide();
		$(".view").show();
		$("#lecRes").hide();
		$("#modRes").show();
		$("#engRes").hide();
	});

	$("#engRes").click(function(){
		$(".jqte-test").each(function(){
			$(this).val($(this).val().replace(/<br>/gm, "<br/>"));			
		});
		
		var text = $("#resume_autoEvaluationsElementsSignifications").val();
		//alert(text);
		$("#formRes").submit();
	});
});

</script>
<%@ include file="../../templates/footer.jsp"%>