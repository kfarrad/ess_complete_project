<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Section activit�s -->
<form id = "formWorkActivitesDelete" action="<c:url value="/autoEvaluation"><c:param name="formType" value="ACT"/></c:url>" method="post" novalidate = "novalidate">
	<input type="hidden" name="activites[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
	<input type="hidden" name="champs[action]" id = "activites_delete" value="delete"/>
	<table class="table">
			<thead>
				<tr>
					<th><input type="checkbox" id = "all"></th>
					<th>P�riode</th>
					<th>Missions ou r�alisation</th>
					<th>Responsable</th>
					<th>Entretien de mission</th>
				</tr>
			</thead>
			<tbody>
			<c:choose>
				<%--Si la variable gestionEvaluation est null, affiche le message par defaut --%>
				<c:when test="${empty gestionEvaluations.autoEvaluations.systemesActiviteses}">
					<tr>
						<td colspan="5" class="name">Aucune donn�e</td>
					</tr>
				</c:when>
				<%-- Sinon, affichage du tableau --%>
				<c:otherwise>
					<%-- Parcours le tableau et affiche la liste des activit�s  --%>
					
					<c:forEach items="${gestionEvaluations.autoEvaluations.systemesActiviteses}" var="mapActivites" varStatus="boucle">
					<!-- <input type="hidden" name="activites_id_activite" value="${mapActivites.systemesActivitesId}"/> -->
					<tr class="data">
						<!-- <td><c:out value="${boucle.index + 1}" /></td> -->
						<td><input type="checkbox" name="activites_id_activite" value="${mapActivites.systemesActivitesId}"></td>
						<td><c:out value="${mapActivites.systemesActivitesPeriodeDe} au ${mapActivites.systemesActivitesPeriodeAu}" /></td>
						<td><c:out value="${mapActivites.systemesActivitesMissions }" /></td>
						<td><c:out value="${mapActivites.systemesActivitesResponsable }" /></td>
						<td><c:out value="${mapActivites.systemesActivitesEntretien == true ? 'Oui' : 'Non' }" /></td>
					</tr>
					</c:forEach>
					</c:otherwise>
			</c:choose>
			</tbody>
		</table>
	   </form>
				