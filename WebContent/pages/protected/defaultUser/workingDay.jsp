<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Valider le jour de travail" />
<c:set var="active" value="working day" />
<%@ include file="../../templates/header.jsp"%>
<c:if test="${!empty message }">
	<div id="slickbox">
		<div class="successs">
			<p>${message}</p>
		</div>
	</div>
</c:if>


<c:choose>
	<c:when test="${sessionScope.sessionUser.isUser()}">
		<c:import url="/pages/protected/defaultUser/calendar.jsp" />
	</c:when>
	<c:otherwise>
		<c:import url="/pages/protected/admin/adminCalendar.jsp" />
	</c:otherwise>
</c:choose>

<%@ include file="../../templates/footer.jsp"%>