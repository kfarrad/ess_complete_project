<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- Axes Consultant --%>
<div id ="consultant" style='display:none;'>
	<form id= "formCtt" action="<c:url value="/autoEvaluation"><c:param name="formType" value="EXI"/></c:url>" method="post" novalidate = "novalidate">							
		<input type="hidden" name="rapport[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
		<input type="hidden" name="rapport[cat]" value="Ctt"/>
		<table class="table">
			<thead>
				<tr>
					<th width="35%">Comp�tences attendues</th>
					<th width="30%">Niveau</th>
					<th width="35%">Commentaire ou exemple</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
					<c:if test="${rapport.competences.competencesCategorie == 'Metier du consultant'}">									
						<tr>									
							<td width="35%" style="text-align: left">${rapport.competences.competencesLibelle}</td>
							<td width="30%"><div class="view"  style="display: none">${rapport.rapportsNiveau}</div><div class="edit" style="display: none"><select class="" name="rapport[${rapport.competences.competencesId}][niveau]" style="text-align: left"><option value="">-----</option><option value="Tr�s inf�rieur" ${rapport.rapportsNiveau == 'Tr�s inf�rieur' ? 'selected="selected"' : ''}>Tr�s inf�rieur</option><option value="En deca" ${rapport.rapportsNiveau == 'En deca' ? 'selected="selected"' : ''}>En deca</option><option value="Rejoignant partiellement" ${rapport.rapportsNiveau == 'Rejoignant partiellement' ? 'selected="selected"' : ''}>Rejoignant partiellement</option><option value="Conforme" ${rapport.rapportsNiveau == 'Conforme' ? 'selected="selected"' : ''}>Conforme</option><option value="R�guli�rement sup�rieur" ${rapport.rapportsNiveau == 'R�guli�rement sup�rieur' ? 'selected="selected"' : ''}>R�guli�rement sup�rieur</option><option value="Significativement sup�rieur" ${rapport.rapportsNiveau == 'Significativement sup�rieur' ? 'selected="selected"' : ''}>Significativement sup�rieur</option></select></div></td>
							<td width="35%"><div class="view">${rapport.rapportsCommentaire}</div><div class="edit" style="display: none"><textarea rows="10" cols="28" name="rapport[${rapport.competences.competencesId}][commanetaire]" class="formInputText" id="rapport_commanetaire">${rapport.rapportsCommentaire}</textarea></div></td>
						</tr>							
					</c:if>								
				</c:forEach>
			</tbody>
		</table>
	</form>
</div>
<%-- Axes Client --%>
<div id ="client" style='display:none;'>
	<form id= "formClt" action="<c:url value="/autoEvaluation"><c:param name="formType" value="EXI"/></c:url>" method="post" novalidate = "novalidate">							
		<input type="hidden" name="rapport[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
		<input type="hidden" name="rapport[cat]" value="Clt"/>
		<table class="table">
			<thead>
				<tr>
					<th width="35%">Comp�tences attendues</th>
					<th width="30%">Niveau</th>
					<th width="35%">Commentaire ou exemple</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
					<c:if test="${rapport.competences.competencesCategorie == 'Client'}">									
						<tr>									
							<td width="35%" style="text-align: left">${rapport.competences.competencesLibelle}</td>
							<td width="30%"><div class="view"  style="display: none">${rapport.rapportsNiveau}</div><div class="edit" style="display: none"><select class="" name="rapport[${rapport.competences.competencesId}][niveau]" style="text-align: left"><option value="">-----</option><option value="Tr�s inf�rieur" ${rapport.rapportsNiveau == 'Tr�s inf�rieur' ? 'selected="selected"' : ''}>Tr�s inf�rieur</option><option value="En deca" ${rapport.rapportsNiveau == 'En deca' ? 'selected="selected"' : ''}>En deca</option><option value="Rejoignant partiellement" ${rapport.rapportsNiveau == 'Rejoignant partiellement' ? 'selected="selected"' : ''}>Rejoignant partiellement</option><option value="Conforme" ${rapport.rapportsNiveau == 'Conforme' ? 'selected="selected"' : ''}>Conforme</option><option value="R�guli�rement sup�rieur" ${rapport.rapportsNiveau == 'R�guli�rement sup�rieur' ? 'selected="selected"' : ''}>R�guli�rement sup�rieur</option><option value="Significativement sup�rieur" ${rapport.rapportsNiveau == 'Significativement sup�rieur' ? 'selected="selected"' : ''}>Significativement sup�rieur</option></select></div></td>
							<td width="35%"><div class="view">${rapport.rapportsCommentaire}</div><div class="edit" style="display: none"><textarea rows="10" cols="28" name="rapport[${rapport.competences.competencesId}][commanetaire]" class="formInputText" id="rapport_commanetaire">${rapport.rapportsCommentaire}</textarea></div></td>
						</tr>							
					</c:if>								
				</c:forEach>
			</tbody>
		</table>
	</form>
</div>
<%-- Axes Engagement --%>
<div id ="engagement" style='display:none;'>
	<form id= "formEng" action="<c:url value="/autoEvaluation"><c:param name="formType" value="EXI"/></c:url>" method="post" novalidate = "novalidate">							
		<input type="hidden" name="rapport[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
		<input type="hidden" name="rapport[cat]" value="Eng"/>
		<table class="table">
			<thead>
				<tr>
					<th width="35%">Comp�tences attendues</th>
					<th width="30%">Niveau</th>
					<th width="35%">Commentaire ou exemple</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
					<c:if test="${rapport.competences.competencesCategorie == 'Engagement et Qualit�s Individuelles'}">									
						<tr>									
							<td width="35%" style="text-align: left">${rapport.competences.competencesLibelle}</td>
							<td width="30%"><div class="view"  style="display: none">${rapport.rapportsNiveau}</div><div class="edit" style="display: none"><select class="" name="rapport[${rapport.competences.competencesId}][niveau]" style="text-align: left"><option value="">-----</option><option value="Tr�s inf�rieur" ${rapport.rapportsNiveau == 'Tr�s inf�rieur' ? 'selected="selected"' : ''}>Tr�s inf�rieur</option><option value="En deca" ${rapport.rapportsNiveau == 'En deca' ? 'selected="selected"' : ''}>En deca</option><option value="Rejoignant partiellement" ${rapport.rapportsNiveau == 'Rejoignant partiellement' ? 'selected="selected"' : ''}>Rejoignant partiellement</option><option value="Conforme" ${rapport.rapportsNiveau == 'Conforme' ? 'selected="selected"' : ''}>Conforme</option><option value="R�guli�rement sup�rieur" ${rapport.rapportsNiveau == 'R�guli�rement sup�rieur' ? 'selected="selected"' : ''}>R�guli�rement sup�rieur</option><option value="Significativement sup�rieur" ${rapport.rapportsNiveau == 'Significativement sup�rieur' ? 'selected="selected"' : ''}>Significativement sup�rieur</option></select></div></td>
							<td width="35%"><div class="view">${rapport.rapportsCommentaire}</div><div class="edit" style="display: none"><textarea rows="10" cols="28" name="rapport[${rapport.competences.competencesId}][commanetaire]" class="formInputText" id="rapport_commanetaire">${rapport.rapportsCommentaire}</textarea></div></td>
						</tr>							
					</c:if>								
				</c:forEach>
			</tbody>
		</table>
	</form>
</div>
<%-- Axes Developpement --%>
<div id ="developpement" style='display:none;'>
	<form id= "formDev" action="<c:url value="/autoEvaluation"><c:param name="formType" value="EXI"/></c:url>" method="post" novalidate = "novalidate">							
		<input type="hidden" name="rapport[id_auto]" value="${gestionEvaluations.autoEvaluations.autoEvaluationsId }"/>
		<input type="hidden" name="rapport[cat]" value="Dev"/>
		<table class="table">
			<thead>
				<tr>
					<th width="35%">Comp�tences attendues</th>
					<th width="30%">Niveau</th>
					<th width="35%">Commentaire ou exemple</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${gestionEvaluations.autoEvaluations.rapportses}" var="rapport" varStatus="boucle">						
					<c:if test="${rapport.competences.competencesCategorie == 'D�veloppement des Hommes et Management'}">									
						<tr>									
							<td width="35%" style="text-align: left">${rapport.competences.competencesLibelle}</td>
							<td width="30%"><div class="view"  style="display: none">${rapport.rapportsNiveau}</div><div class="edit" style="display: none"><select class="" name="rapport[${rapport.competences.competencesId}][niveau]" style="text-align: left"><option value="">-----</option><option value="Tr�s inf�rieur" ${rapport.rapportsNiveau == 'Tr�s inf�rieur' ? 'selected="selected"' : ''}>Tr�s inf�rieur</option><option value="En deca" ${rapport.rapportsNiveau == 'En deca' ? 'selected="selected"' : ''}>En deca</option><option value="Rejoignant partiellement" ${rapport.rapportsNiveau == 'Rejoignant partiellement' ? 'selected="selected"' : ''}>Rejoignant partiellement</option><option value="Conforme" ${rapport.rapportsNiveau == 'Conforme' ? 'selected="selected"' : ''}>Conforme</option><option value="R�guli�rement sup�rieur" ${rapport.rapportsNiveau == 'R�guli�rement sup�rieur' ? 'selected="selected"' : ''}>R�guli�rement sup�rieur</option><option value="Significativement sup�rieur" ${rapport.rapportsNiveau == 'Significativement sup�rieur' ? 'selected="selected"' : ''}>Significativement sup�rieur</option></select></div></td>
							<td width="35%"><div class="view">${rapport.rapportsCommentaire}</div><div class="edit" style="display: none"><textarea rows="10" cols="28" name="rapport[${rapport.competences.competencesId}][commanetaire]" class="formInputText" id="rapport_commanetaire">${rapport.rapportsCommentaire}</textarea></div></td>
						</tr>							
					</c:if>								
				</c:forEach>
			</tbody>
		</table>
	</form>
</div>			
