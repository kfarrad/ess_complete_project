<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Liste C.V." />
<c:set var="active" value="Resume List" />
<%@ include file="../../templates/header.jsp"%>
<div class="box">
	<table class="table">
		<tr>
			<th>C.V N°</th>
			<th>Titre</th>
			<th>Derniere modification</th>
			<th>Modifier C.V.</th>
			<th>Creer pdf</th>
			<th>Afficher pdf</th>
			<th>Supprimer C.V</th>
		</tr>
		<c:choose>
			<c:when test="${ empty listResume }">
				<tr>
					<td></td>
					<td>Ancun C.V. enregistré</td>
					<td>...</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach items="${ listResume }" var="resume" varStatus="boucle">
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						<td><c:out value="${boucle.index + 1}" /></td>
						<td><c:out value="${resume.titre.nom}" /></td>
						<td><c:out value="${resume.date}" /></td>
						<td><a
							href="<c:url value="/resume"><c:param name="idResume" value="${resume.id}" /></c:url>">
								<img src="<c:url value="/resources/images/modification.png"/>"
								alt="modification" />
						</a></td>
						<td><a
							href="<c:url value="/xmlResume"><c:param name="idResume" value="${resume.id}" /></c:url>">
								<img src="<c:url value="/resources/images/inscription.png"/>"
								alt="modification" />
						</a></td>
						<td>
						<c:if test="${ !empty resume.pdf }">
							<a	href="<c:url value="/download/${resume.pdf }"/>"><img
								src="<c:url value="/resources/images/activation.png"/>"
								alt="modification" /> </a>
						</c:if>
						</td>
						<td>
							<form id="frmTitle" action="<c:url value="/userResume">
								<c:param name="delete" value="${ resume.id }" /></c:url>" method="post">
								
								<input type="submit" class="" id="btnResumeDel" value="">
							</form>
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</table>
	<input type="button" value="Creer un nouveau CV"
		onclick="document.location.href='resume'" />
</div>
<%@ include file="../../templates/footer.jsp"%>