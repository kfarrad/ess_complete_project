<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Bilan de ce mois" />
<c:set var="active" value="bilan" />
<%@ include file="../../templates/header.jsp"%>
	
<c:if test="${!empty sessionScope.sessionUser}">
	<c:if test="${sessionScope.sessionUser.isUser()}">
		<c:import url="/pages/protected/defaultUser/resultCalendar.jsp" />
	</c:if>

	<c:if test="${sessionScope.sessionUser.isAdmin()}">
		<c:import url="/pages/protected/admin/adminBilanCalendar.jsp" />

	</c:if>
	<c:if test="${sessionScope.sessionUser.isRh()}">
		<c:import url="/pages/protected/RH/rhResultCalendar.jsp" />
	</c:if>
</c:if>
<%@ include file="../../templates/footer.jsp"%>