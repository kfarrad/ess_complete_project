<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<c:set var="title" value="Mes auto-évaluations" />
<c:set var="active" value="Resume List" />
<%@ include file="../../templates/header.jsp"%>

<c:if test="${!empty resultat }">
		<div id="slickbox"><div class="${!empty type ? 'warnings': 'successs' }"><p>${resultat}</p></div></div>
	</c:if>
<div class="box">
	<table class="table">
		<tr>
			<th>N°</th>
			<th>Titre</th>
			<th>Date de réception</th>
			<th>Dernière modification</th>
			<th>voir pdf</th>
			<th>Etat</th>
		</tr>
		<c:choose>
			<c:when test="${ empty gestionEvaluations }">
				<tr>
					<td colspan="7">Aucun document</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach items="${ gestionEvaluations }" var="gestionEvaluation" varStatus="boucle">
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						<td><c:out value="${boucle.index + 1}" /></td>
						<td><c:out value="Auto-Evaluation ${gestionEvaluation.dates.datesPeriodes}" /><c:if test="${ empty gestionEvaluation.gestionEvaluationsDateModification }">
							<img src="<c:url value="/resources/images/new.jpeg"/>" width="25px" height="25px" alt="Nouveau" /></c:if></td>
						<td><joda:format value="${gestionEvaluation.gestionEvaluationsDateReception}" pattern="dd/MM/yyyy"/></td>
						<td><joda:format value="${gestionEvaluation.gestionEvaluationsDateModification}" pattern="dd/MM/yyyy"/> **modifié ${gestionEvaluation.gestionEvaluationsNbreModification} fois</td>
						<td>
							<a class="infobulle" title = "Voir le pdf" href="<c:url value="/viewPDF"><c:param name="id" value="${gestionEvaluation.dates.datesId}&${gestionEvaluation.autoEvaluations.autoEvaluationsId}" /></c:url>">
								<img src="<c:url value="/resources/images/logo-pdf.jpg"/>" alt="voir le pdf" />
							</a>
						</td>
						<td>
						<c:choose>
							<c:when test="${empty gestionEvaluation.gestionEvaluationsDateTransmission }">
								<a class="infobulle" title = "Editer le document" href="<c:url value="/autoEvaluation"><c:param name="id" value="${gestionEvaluation.dates.datesId}&${gestionEvaluation.autoEvaluations.autoEvaluationsId}" /></c:url>">
									<img src="<c:url value="/resources/images/modification.png"/>"
								alt="modification" /></a>
								<a id="send" title = "Soumettre le document à la direction" href="<c:url value="/mesAutoEvaluations"><c:param name="send" value="${gestionEvaluation.dates.datesId}&${gestionEvaluation.autoEvaluations.autoEvaluationsId}" /></c:url>">
									<img src="<c:url value="/resources/images/activation.png"/>" alt="Soumettre le document" />
								</a>
							</c:when>
							<c:otherwise>
								<p>Soumis le <joda:format value="${gestionEvaluation.gestionEvaluationsDateTransmission}" pattern="dd/MM/yyyy"/></p>
							</c:otherwise>
						</c:choose>				
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</table>
</div>
<script>
$(document).ready(function(){
	$("#send").click(function(){
		return confirm("Une fois le formulaire envoyé, vous ne pouvez plus le modifier.\nEtes-vous sûr de vouloir soumettre le formulaire?");
			
	});
});
</script>
<%@ include file="../../templates/footer.jsp"%>