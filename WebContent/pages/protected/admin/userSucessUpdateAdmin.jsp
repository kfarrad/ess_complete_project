<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Changements enregistrés" />
<c:set var="active" value="Modification pris en compte" />
<%@ include file="../../templates/header.jsp"%>

<fieldset>
	<p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
</fieldset>

<%@ include file="../../templates/footer.jsp"%>
<script>
$(document).ready(function(){
    setTimeout(function() {
     window.location.href = "userProfilList";
    }, 1500);
  });
</script>