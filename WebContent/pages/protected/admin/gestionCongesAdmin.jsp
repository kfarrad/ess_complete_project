	<%@ page pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<c:set var="title" value="Interface Conges" />
	<c:set var="active" value="Resume list" />
	<%@ include file="../../templates/header.jsp"%>
	
	<style>
	#OkValidation {
		margin-right: 25px;
		color: white;
	}
	
	#OkValidation img:last-child {
		display: none
	}
	
	#OkValidation:hover img:first-child {
		display: none
	}
	
	#OkValidation:hover img:last-child {
		display: inline-block
	}
	
	#NoValidation img:last-child {
		display: none
	}
	
	#NoValidation:hover img:first-child {
		display: none
	}
	
	#NoValidation:hover img:last-child {
		display: inline-block
	}
	</style>
	
	<script type="text/javascript">
	function confirmDelete() {
		return confirm("Voulez vous vraiment supprimer ce congé ?");
	}
	</script>
	
	<c:if test="${statutValidation}">
	<script type="text/javascript">
		$(document).ready(function() {
			msg.open(document.getElementById('detail'));
		});
	</script>
	</c:if>
	
	<c:if test="${statutUpdate}">
	<script type="text/javascript">
		$(document).ready(
				function() {
					$(".titre").notify(
							"La modification a été éffectuée avec succès",
							"success");
				});
	</script>
	</c:if>
	
	<div id="detail" style="display: none;">
		<fieldset class="details">
			<legend>Confirmation de validation de congé</legend>
			<p>Votre validation a été éffectué avec succès, le salarié va être
				notifié par un mail</p>
		</fieldset>
	</div>
	
	<%-- <form method="get" action="<c:url value="/gestionCongesListeAdmin"/>"
		id="formConges"> --%>
	
	<fieldset>
		<legend>Liste de Conges</legend>
	
		<table class="table">
			<thead>
				<tr>
					<th>Nom d'utilisateur</th>
					<th>Type de congé</th>
					<th>Periode</th>
					<th>Nombre de jours</th>
					<th>Validation</th>
					<th>Suppression</th>
					<th>Modification</th>
				</tr>
	
			</thead>
			<tbody>
				<c:forEach items="${congesListe}" var="userconge">
				<td><c:out value="${userconge.utilisateur}" /></td>
				<td><c:out value="${userconge.typeConge}" /></td>
				<td><c:out value="${userconge.periode}" /></td>
				<td><c:out value="${userconge.total_jour}" /></td>
				<td>
					<%-- <a
					href="<c:url value="/gestionCongesValider"><c:param name="idConge" value="${ userconge.id }" /></c:url>">
						Valider le congé </a> --%> <a id="OkValidation"
					href="<c:url value="/gestionCongesValider"><c:param name="idConge" value="${ userconge.id }" />
																<c:param name="accepter" value="1"/>
					
						</c:url>"><img
						src="<c:url value="/resources/images/Ok.png"/>"
						alt="validation" class="infobulle" title="Valider" /><img
						src="<c:url value="/resources/images/Ok_.png"/>"
						alt="modification" class="infobulle" title="Valider" /></a> <a
					id="NoValidation"
					href="<c:url value="/gestionCongesValider"> <c:param name="idConge" value="${userconge.id}"/>
																<c:param name="accepter" value="0"/>
						</c:url> ">
						<img src="<c:url value="/resources/images/No.png"/>"
						alt="modification" class="infobulle" title="Refuser" /> <img
						src="<c:url value="/resources/images/No_.png"/>"
						alt="modification" class="infobulle" title="Refuser" />
				</a>
	
						</td>
				<td><form id="frmTitle"
						action="<c:url value="/deleteConge"> <c:param name="idConge" value="${userconge.id}" /></c:url>"
						method="post">
	
						<a class="infobulle" title="Suppression"><input type="submit"
							class="" id="btnResumeDel" value=""
							onclick="return confirmDelete()"></a>
					</form></td>
				<td><a
					href="<c:url value="/modificationConge"> <c:param name="idConge" value="${userconge.id}"/></c:url> ">
						<img src="<c:url value="/resources/images/modification.png"/>"
						alt="modification" class="infobulle" title="Modification" />
				</a></td>
	
				</tr>
			</c:forEach>
			<%-- </c:forEach> --%>
			</tbody>
		</table>
	
	</fieldset>
	
	<%--For displaying Previous link except for the 1st page --%>
	<c:if test="${currentPage != 1}">
	<td><a
		href="<c:url value="/gestionCongesListe"><c:param name="page" value="${currentPage - 1}" /></c:url>">Précédent</a></td>
	
	</c:if>
	
	<%--For displaying Page numbers. 
	    The when condition does not display a link for the current page--%>
	<c:if test="${noOfPages > 1}">
	<table cellpadding="5" cellspacing="5">
		<tr>
			<c:forEach begin="1" end="${noOfPages}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<td>${i}</td>
					</c:when>
					<c:otherwise>
	
						<td><a
							href="<c:url value="/gestionCongesListe"><c:param name="page" value="${i}" /></c:url>">${i}</a></td>
	
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</tr>
	</table>
	</c:if>
	<%--For displaying Next link --%>
	
	<c:if test="${currentPage lt noOfPages}">
	<td><a
		href="<c:url value="/gestionCongesListe"><c:param name="page" value="${currentPage + 1}" /></c:url>">Suivant</a></td>
	</c:if>
	
	<%@ include file="../../templates/footer.jsp"%>