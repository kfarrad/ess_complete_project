<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Interface Conges" />
<c:set var="active" value="Resume list" />
<%@ include file="../../templates/header.jsp"%>

<style>
.formConges fieldset input[type=submit] {
	float: right;
}

.formConges fieldset input[type=reset] {
	float: right;
}

.formConges fieldset input {
	height: 26px;
	margin-bottom: 5px;
	line-height: 26px;
	border-radius: 5px;
	border-right-width: 7px;
	width: 170px;
}

.formConges select, label {
	height: 26px;
	margin-bottom: 5px;
	line-height: 26px;
	border-radius: 5px;
	border-right-width: 7px;
	width: 170px;
}

#advancedSelectionDay {
	display: none;
}
</style>

<script>
	$(document).ready(function() {

		$("#formConges").validate({

			rules : {
				fromDate : {
					required : true,

				},
				toDate : {
					required : true
				},
				motif : {
					required : true
				},
				journee : {
					required : true
				},

			},
			messages : {

				fromDate : "Date de debut est obligatoire",
				toDate : "Date de fin est obligatoire",
				motif : "Motif est obligatoire",
				journee : "Journée ou demi-journée est obligatoire"

			}

		});

	});
</script>

<script>
	$(function() {
		$("#fromDate").datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			numberOfMonths : 1,
			ChangeYear : true,
			onClose : function(selectedDate) {
				$("#toDate").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#toDate").datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			numberOfMonths : 1,
			ChangeYear : true,
			maxDate : '+1Y',
			/* yearRange: yrRange, */
			onClose : function(selectedDate) {
				$("#fromDate").datepicker("option", "maxDate", selectedDate);
				/* var dateToday= new Date ();
				var yrRange=dateToday.getFullYear() + ":" + (dateToday.getFullYear() + 2); */
			}
		});
	});

	function display(val) {
		if (document.getElementById("fromDate").value == document
				.getElementById("toDate").value) {
			var div = document.createElement('DIV');
			div.id = 'advancedSelectionDay';
			if (val === -1) {
				div.innerHTML = '<label> Une journée ou demi-journée ? : </label><select name="journee" class="required" id="journee"><option value="-1" selected>Journée ou demi-journée</option><option value="0.5">0.5</option><option value="1">1</option></select>';
			} else if (val === 0.5) {
				div.innerHTML = '<label> Une journée ou demi-journée ? : </label><select name="journee" class="required" id="journee"><option value="-1">Journée ou demi-journée</option><option value="0.5" selected>0.5</option><option value="1">1</option></select>';
			} else if (val === 1) {
				div.innerHTML = '<label> Une journée ou demi-journée ? : </label><select name="journee" class="required" id="journee"><option value="-1">Journée ou demi-journée</option><option value="0.5">0.5</option><option value="1" selected>1</option></select>';
			}

			document.getElementById("advancedSelection").insertBefore(div,
					document.getElementById('toDate').nextSibling);
			if (document.getElementById('advancedSelectionDay') != null)
				document.getElementById('advancedSelectionDay').style.display = 'block';

		} else {
			if (document.getElementById('advancedSelectionDay') != null) {
				document.getElementById('advancedSelectionDay').style.display = 'none';
				document.getElementById("advancedSelectionDay").remove();
			}

		}
	}
</script>

<c:if test="${statutAjout}">
	<script type="text/javascript">
		$(document).ready(function() {
			msg.open(document.getElementById('detail'));
		});
	</script>
</c:if>

<c:choose>
	<c:when test="${ conge.total_jour == 0.5 }">
		<script type="text/javascript">
			$(document).ready(function() {
				display(0.5);
			});
		</script>
	</c:when>
	<c:when test="${ conge.total_jour == 1 }">
		<script type="text/javascript">
			$(document).ready(function() {
				display(1);
			});
		</script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript">
			$(document).ready(function() {
				display(-1);
			});
		</script>
	</c:otherwise>
</c:choose>


<div id="detail" style="display: none;">
	<fieldset class="details">
		<legend>Confirmation de demande de congé</legend>
		<p>Votre demande de congés a bien été prise en compte, vous allez
			être notifié, une fois l'administrateur valide la demande</p>
	</fieldset>
</div>
<form method="post" action="<c:url value="/modificationConge"/>"
	id="formConges">
	<fieldset id="advancedSelection">
		<legend>Veuillez entrer vos modifications</legend>

		<label for="fromDate">Date de debut :</label> <input type="text"
			id="fromDate" name="fromDate" onchange="javascript:display(-1);"
			value="${dateDebut}"> <br> <label for="toDate">Date
			de fin :</label> <input type="text" id="toDate" name="toDate"
			onchange="javascript:display(-1);" value="${dateFin}"> <br>

		<!-- <div id="advancedSelection"></div> -->

		<label for="motif"> Motif Conges : </label> <select name="motif"
			class="required" id="motif">

			<option value="">Type Conges</option>

			<c:forEach items="${projetConges}" var="projetCra"
				varStatus="myIndex">
				<c:choose>
					<c:when test="${projetCra.id == conge.id_motif}">
						<option value="${projetCra.id}" selected>${projetCra.nom}</option>
					</c:when>
					<c:otherwise>
						<option value="${projetCra.id}">${projetCra.nom}</option>
					</c:otherwise>
				</c:choose>
			</c:forEach>

		</select> <br> <input type="hidden" name="statut" id="statut" value="1" /><br>

		<input type="submit" value="Modifier" name="bouton_ok" id="bouton_ok" />
		&nbsp;&nbsp; <input type="reset" value="Effacer" name="bouton_reset"
			id="bouton_reset" />
	</fieldset>

	<!-- <input type="hidden" name="to" value="farid.chkirrou@clinkast.fr">
	<input type="hidden" name="subject" value="Demande de congé">
	<input type="hidden" name="body" value="Bonjour, Veuillez vous connecter à votre compte via le lien suivant :http://intranet.clinkast.fr:81/ess/ afin de valider cette demande de congé"> -->


</form>

<%@ include file="../../templates/footer.jsp"%>