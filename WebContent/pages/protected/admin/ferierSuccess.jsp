<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Succès" />
<c:set var="active" value="Succès" />
<%@ include file="../../templates/header.jsp"%>
<fieldset>
	<p class="info">${ form.resultat }</p>
	<p>Redirection vers le fixer les jours fériés..</p>
</fieldset>
<%@ include file="../../templates/footer.jsp"%>
<script>
$(document).ready(function(){
    setTimeout(function() {
     window.location.href = "holidays";
    }, 2000);
  });
</script>