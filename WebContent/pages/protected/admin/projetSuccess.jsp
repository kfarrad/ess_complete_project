<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Succès" />
<c:set var="active" value="Succès" />
<%@ include file="../../templates/header.jsp"%>
<fieldset>
	<legend>Succès</legend>
	<p>
		Nom :
		<c:out value="${ projet.nom }" />
	</p>
	<p class="info">${ form.resultat }</p>
	<p>Redirection vers la liste des projets...
	<p>
</fieldset>
<script>
$(document).ready(function(){
      setTimeout(function() {
       document.write("Redirection vers la liste des projets"); 
       window.location.href = "projetList";
      }, 3000);
    });	
</script>
<%@ include file="../../templates/footer.jsp"%>