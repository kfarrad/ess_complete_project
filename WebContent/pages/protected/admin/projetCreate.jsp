<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Creation d'un projet" />
<c:set var="active" value="Creation d'un projet" />
<%@ include file="../../templates/header.jsp"%>
<div id="corps">
	<fieldset>
		<legend>Informations Client</legend>
		<p>
			Nom :
			<c:out value="${ client.nom }" />
		</p>
		<p>
			Adresse :
			<c:out value="${ client.adresse }" />
		</p>
	</fieldset>
</div>

<div id="corps">
	<form method="post"
		action="<c:url value="/projetCreation"> <c:param name="idClient" value="${ client.id }" /></c:url>">
		<fieldset>

			<legend>Informations Projet</legend>
			<label for="nom">Nom<span class="requis">*</span></label> <input
				type="text" id="nom" name="nom"
				value="<c:out value="${projet.nom}"/>" size="30" maxlength="60" />
			<span class="erreur">${form.erreurs['nom']}</span> <br />

		</fieldset>
		<p class="info">${ form.resultat }</p>
		<input type="submit" value="Creer" /> <input type="reset"
			value="Remettre à zéro" /> <br />
	</form>
</div>
<%@ include file="../../templates/footer.jsp"%>