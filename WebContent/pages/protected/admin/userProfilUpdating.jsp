<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Modification du profil" />
<c:set var="active" value="modification du profil" />
<%@ include file="../../templates/header.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {

		$("#dobProfil").datepicker({
			dateFormat : 'dd/mm/yy',
			defaultDate : "+1w",
			changeMonth : true,
			changeYear : true,
			yearRange : "-65:+0",
			numberOfMonths : 1
		});

	});
</script>


<form method="post" action="<c:url value="/userProfilUpdate"/>"
	enctype="multipart/form-data">
	<fieldset>

		<label for="nomProfil">Nom <span class="requis">*</span></label> <input
			type="text" id="nomProfil" name="nomProfil" required
			title="Ce champ est requis" value="<c:out value="${profil.nom}"/>"
			size="30" maxlength="30" /> <span class="erreur">${form.erreurs['nomProfil']}</span>
		<br /> <label for="prenomProfil">Prénom </label> <input type="text"
			id="prenomProfil" name="prenomProfil"
			value="<c:out value="${profil.prenom}"/>" size="30" maxlength="30" />
		<span class="erreur">${form.erreurs['prenomProfil']}</span> <br /> <label
			for="adresseProfil">Adresse <span class="requis">*</span></label> <input
			type="text" id="adresseProfil" name="adresseProfil" required
			title="Ce champ est requis"
			value="<c:out value="${profil.adresse}"/>" size="30" maxlength="60" />
		<span class="erreur">${form.erreurs['adresseProfil']}</span> <br /> <label
			for="telephoneProfil">Numéro de téléphone <span
			class="requis">*</span></label> <input type="text" id="telephoneProfil"
			pattern="[+\d{2}]?\d{9,11}$" name="telephoneProfil" required
			title="Numéro de téléphone n'est pas valide entre 9 et 11 chiffres"
			value="<c:out value="${profil.telephone}"/>" size="30" maxlength="30" />
		<span class="erreur">${form.erreurs['telephoneProfil']}</span> <br />
		<label for="emailProfil">Adresse email<span class="requis">*</span></label>
		<input type="email" id="emailProfil" name="emailProfil" required
			title="Ce champ est requis"
			value="<c:out value="${profil.email}"/>" size="30" maxlength="60"
			disabled="disabled" /> <span class="erreur">${form.erreurs['emailProfil']}</span>
		<br /> <label for="emailProfil">Date de naissance</label> <input type="text" id="dobProfil"
			name="dobProfil" value="<c:out value="${dateParsed}"/>" size="30"
			maxlength="60" /> <span class="erreur">${form.erreurs['dobProfil']}</span>
		<br /> <label for="emailProfil">Numéro sécurité sociale</label> <input
			type="text" id="numero_snnProfil" pattern="\d{15}$"
			title="Le n° de sécurité social doit contenir 15 chiffres"
			name="numero_snnProfil" value="<c:out value="${profil.numero_ssn}"/>"
			size="30" maxlength="15" /> <span class="erreur">${form.erreurs['numero_snnProfil']}</span>
		<br /> <label for="file">Importer la carte vitale </label> <input
			type="file" name="carteVitaleFile" /><span class="erreur">${form.erreurs['carte_vitaleProfil']}</span>
			
			<c:if test="${!empty profil.carte_vitale}">
							<a
								href="<c:url value="/download/${profil.carte_vitale}"/>"
								target="_blank"
								title="Carte vitale de <c:out value="${profil.nom}"></c:out> <c:out value="${profil.prenom}"></c:out>"><img
									src="<c:url value="/resources/images/carte_vitale.png"/>" /></a>
						</c:if>
			
		<br /> <label for="file">Importer la pièce d'identité </label> <input
			type="file" name="CNIfile" /><span class="erreur">${form.erreurs['cniProfil']}</span>
			
			<c:if test="${!empty profil.cni}">
							<a href="<c:url value="/download/${profil.cni}"/>"
								target="_blank"
								title="Carte d'identité de <c:out value="${profil.nom}"></c:out> <c:out value="${profil.prenom}"></c:out>"><img
									src="<c:url value="/resources/images/cni.png"/>" /></a>
						</c:if>
			
		<br /> <label for="file">Importer la photo de profil </label> <input
			type="file" name="profilFile" /><span class="erreur">${form.erreurs['profilProfil']}</span>
			
			<c:if test="${!empty profil.photoProfil}">
							<a href="<c:url value="/download/${profil.photoProfil}"/>">
								<c:choose>
									<c:when test="${!empty profil.photoProfil}">
								<img
								id="userLogoUpdating" src="<c:url value="/download/${profil.photoProfil}"/>"
								title="${profil.getName()}">
								</c:when>
								<c:otherwise>
									<img
								id="userLogo" src="<c:url value="/resources/images/defaultUser.png"/>"
								title="${profil.getName()}">
								</c:otherwise>
								</c:choose>
							</a>
						</c:if>
			
		<br />
	</fieldset>
	<fieldset>
		<legend>Modification du role</legend>
		<%
			Utilisateur user = (Utilisateur) request.getAttribute("user");
			if (user != null) {
		%>

		<label for="typeCompte">Type de compte <span class="requis">*</span></label>
		<input type="radio" id="USER" name="typeCompte" value="USER"
			<%if (user.isUser()) {%> checked="checked" <%}%> /> USER <input
			type="radio" id="RH" name="typeCompte" value="RH"
			<%if (user.isRh()) {%> checked="checked" <%}%> /> RH <input
			type="radio" id="ADMIN" name="typeCompte" value="ADMIN"
			<%if (user.isAdmin()) {%> checked="checked" <%}%> /> ADMIN <br />
		<%
			}
		%>
	</fieldset>
	<p class="info">${ form.resultat }</p>
	<input type="submit" value="Valider" /> <input type="reset"
		value="Remettre à zéro" /> <br />
</form>

<%@ include file="../../templates/footer.jsp"%>