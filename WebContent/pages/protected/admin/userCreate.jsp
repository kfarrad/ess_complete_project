<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Creation d'un compte" />
<c:set var="active" value="user Creation" />
<%@ include file="../../templates/header.jsp"%>
<form method="post"
	action="<c:url value="/userCreation"> <c:param name="idProfil" value="${ user.profil.id }" /></c:url>">
	<fieldset>

		<legend>Informations Utilisateur</legend>
		<label for="login">Login<span class="requis">*</span></label> <input
			type="text" id="login" name="login"
			value="<c:out value="${user.login}"/>" size="30" maxlength="60" /> <span
			class="erreur">${form.erreurs['login']}</span> <br /> <label
			for="motdepasse">Mot de passe <span class="requis">*</span></label> <input
			type="password" id="motdepasse" name="motdepasse"
			value="<c:out value="${user.motDePasse}"/>" size="30" maxlength="20" />
		<span class="erreur">${form.erreurs['motdepasse']}</span> <br /> <label
			for="confirmation">Confirmation du mot de passe <span
			class="requis">*</span>
		</label> <input type="password" id="confirmation" name="confirmation" value=""
			size="30" maxlength="20" /> <span class="erreur">${form.erreurs['confirmation']}</span>
		<br /> <input type="hidden" name="idProfil"
			value="${ user.profil.id }" />

		<%-- Si et seulement si la Map des clients en session n'est pas vide, alors on propose un choix à l'utilisateur --%>
		
	</fieldset>
	<fieldset>
		<legend>Role</legend>
		<label for="typeCompte">Type de compte <span class="requis">*</span></label>
		<input type="radio" id="USER" name="typeCompte" value="USER"
			onchange="returnRole();" /> USER <input type="radio" id="RH"
			name="typeCompte" value="RH" onchange="returnRole();" /> RH <input
			type="radio" id="ADMIN" name="typeCompte" value="ADMIN"
			onchange="returnRole();" /> ADMIN  <span class="erreur">${form.erreurs['typeCompte']}</span><br />
		</fieldset>
	<p class="info">${ form.resultat }</p>
	<input type="submit" value="Creer" /> <input type="reset"
		value="Remettre à zéro" /> <br />
</form>
<%@ include file="../../templates/footer.jsp"%>