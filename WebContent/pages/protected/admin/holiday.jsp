<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="title" value="Fixer jours fériés" />
<c:set var="active" value="Holidays" />
<%@ include file="../../templates/header.jsp"%>

<%
Calendar ca = new GregorianCalendar();
int iTDay=ca.get(Calendar.DATE);
int iTYear=ca.get(Calendar.YEAR);
int iTMonth=ca.get(Calendar.MONTH);
int iMonth=-1;
int iYear=-1;

try {
	 iYear= ( Integer)request.getAttribute("annee");
	 iMonth= ( Integer)request.getAttribute("mois");
} catch (Exception e) {

	 e.printStackTrace();
}
if(iYear==-1)
{
	  iYear=iTYear;
	  
}
if(iMonth==-1)
{
	 iMonth=iTMonth;
} 

 GregorianCalendar cal = new GregorianCalendar (iYear, iMonth, 1); 

 int days=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
 
 int weekStartDay=cal.get(Calendar.DAY_OF_WEEK);
 
 cal = new GregorianCalendar (iYear, iMonth, days); 
 int iTotalweeks=cal.get(Calendar.WEEK_OF_MONTH);
 List<Integer> feriers = (List<Integer>)request.getAttribute(Constants.ATT_FERIERS);
%>

<script>
function goTo()
{
	document.location.href="holidays?annee="+$("#annee").val()+"&mois="+$("#mois").val();
}

$(document).ready(function() {
	$("#dynamic").scroll(function(){
		  $("#divSelect").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  			
		});	
});
</script>

<form name="frm" method="post">
	<div id="divSelect" class="TabDyn">
		<table class="table" id="selection">
			<tbody>
				<tr class="odd">
					<td>Year</td>
					<td><select id="annee" name="annee" onchange="goTo()">
							<%
		// start year and end year in combo box to change year in calendar
	    for(int iy=iTYear-20;iy<=iTYear+20;iy++)
		{
		  if(iy==iYear)
		  {
		    %>
							<option value="<%=iy%>" selected="selected"><%=iy%></option>
							<%
		  }
		  else
		  {
		    %>
							<option value="<%=iy%>"><%=iy%></option>
							<%
		  }
		}
	   %>
					</select></td>
					<td><h3><%=iTDay %>
							<%=new SimpleDateFormat("MMMM").format(new Date(2008,iMonth,01))%>
							<%=iYear%></h3></td>
					<td>Month</td>
					<td><select id="mois" name="mois" onchange="goTo()">
							<%
		// print month in combo box to change month in calendar
	    for(int im=0;im<=11;im++)
		{
		  if(im==iMonth)
		  {
	     %>
							<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(new Date(2008,im,01))%></option>
							<%
		  }
		  else
		  {
		    %>
							<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(new Date(2008,im,01))%></option>
							<%
		  }
		}
	   %>
					</select></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="TabDyn">

		<table class="table" id="affichage">

			<tbody>

				<tr>
					<th width="10">Sun</th>
					<th width="10">Mon</th>
					<th width="10">Tue</th>
					<th width="10">Wed</th>
					<th width="10">Thu</th>
					<th width="10">Fri</th>
					<th width="10">Sat</th>
				</tr>
				<%
        int cnt =0;
        int cnt2 =0;
        for(int i=1;i<=days;i++)
        {
		%>
			<%if(cnt==0)
			{
			%>				
				<tr <%if(cnt2++ % 2 == 0){%>class="odd"<%} %>>
			<%			
			}
			%>
			<%
			if(i==1)
			{
				for(int j = 1; j<weekStartDay; j++)
				{
				%>
					<td>&nbsp;</td>
				<%
				}
				%><td><%=i %><br/><input type="checkbox" id="holiday" name="holiday" value="<%=i %>" <%if(feriers.contains(i)){ %> checked="checked"<%} %>></td>
				<% cnt = weekStartDay;
			}
			else if(cnt < 7 && i == days)
			{
				%>
				<td><%=i %><br/><input type="checkbox" id="holiday" name="holiday" value="<%=i %>" <%if(feriers.contains(i)){ %> checked="checked"<%} %>></td>
				<%
				for(int j = 1; j<7-cnt; j++)
				{
				%>
					<td>&nbsp;</td>
				<%
				}
				cnt = 7;
			}
			else{
				cnt ++;
				%>
				<td><%=i %><br/><input type="checkbox" id="holiday" name="holiday" value="<%=i %>" <%if(feriers.contains(i)){ %> checked="checked"<%} %>></td>
			<%
			}
			if(cnt >= 7)
			{				
				cnt = 0;
				%>
				</tr>
				
				<%
			}
        }
			%>
			
			
			
				

			</tbody>
		</table>
	</div>

	<div id="corps">
		<input type="submit" value="Valider" />
	</div>
</form>

<%@ include file="../../templates/footer.jsp"%>
