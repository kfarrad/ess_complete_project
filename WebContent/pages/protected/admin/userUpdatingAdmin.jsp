<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Modification du mot de passe" />
<c:set var="active" value="Modification du mot de passe" />
<%@ include file="../../templates/header.jsp"%>

<form method="post"
	action="<c:url value="/userUpdate"><c:param name="idProfil" value=" ${sessionScope.sessionUser.profil.id}" /></c:url>">

	<fieldset>
		<label for="login">Login<span class="requis">*</span></label> <input
			type="text" id="login" name="login"
			value="<c:out value="${user.login}"/>" disabled="disabled" size="30"
			maxlength="60" /> <br /> <label for="motdepasse">Nouveau
			Mot de passe <span class="requis">*</span>
		</label> <input type="password" id="motdepasse" name="motdepasse"
			value="<c:out value=""/>" size="30" maxlength="20" /> <span
			class="erreur">${form.erreurs['motdepasse']}</span> <br /> <label
			for="confirmation">Confirmation du mot de passe <span
			class="requis">*</span></label> <input type="password" id="confirmation"
			name="confirmation" value="" size="30" maxlength="20" /> <span
			class="erreur">${form.erreurs['confirmation']}</span> <br /> <input
			type="submit" value="Valider" class="sansLabel" /> <br />



	</fieldset>
</form>

<%@ include file="../../templates/footer.jsp"%>