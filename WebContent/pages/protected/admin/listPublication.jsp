<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Liste des publications" />
<c:set var="active" value="liste publication" />
<%@ include file="../../templates/header.jsp"%>
<script type="text/javascript">
function confirmDelete() {
	return confirm("Voulez vous vraiment supprimer cette publication ?");
}
</script>

<c:if test="${statutUpdate}">
	<script type="text/javascript">
$(document).ready(function(){
	$(".titre").notify("La modification a été éffectuée avec succès", "success");
});
</script>
</c:if>
<c:choose>
	<c:when test="${!empty contenus}">
		<table class="table">
		<tr>
			<th>Titre</th>
			<th>Contenu</th>
			<th>Editer</th>
			<th>Supprimer</th>
		</tr>

		<c:forEach items="${contenus}" var="contenu"
			varStatus="myIndex">
			<tr>
				<td>${contenu.titre}</td>
				<td>${contenu.contenu}</td>
				<td><a
					href="<c:url value="/modificationPublication"> <c:param name="idPublication" value="${contenu.id}"/></c:url> ">
						<img src="<c:url value="/resources/images/modification.png"/>"
						alt="modification" class="infobulle" title="Modification" />
				</a></td>
				<td><form id="frmTitle"
						action="<c:url value="/supprimerPublication">
									<c:param name="idPublication" value="${contenu.id}" /></c:url>"
						method="post">

						<a class="infobulle" title="Suppression"><input type="submit"
							class="" id="btnResumeDel" value=""
							onclick="return confirmDelete()"></a>
					</form></td>
			</tr>
		</c:forEach>
	</table>
		
	</c:when>
	<c:otherwise>
		<p class="succes">Aucune publication enregistrée.</p><br/>
		<a href="<c:url value="/ajouterContenu"/>">Ajouter une publication</a>
	</c:otherwise>
</c:choose>
<%-- <c:if test="${!empty contenus}">
	
</c:if> --%>


<%@ include file="../../templates/footer.jsp"%>