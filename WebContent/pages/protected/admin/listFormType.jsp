<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<c:set var="title" value="Gestion des periodes d'auto-évaluation" />
<c:set var="active" value="Complete your resume" />
<%@ include file="../../templates/header.jsp"%>

<style>
.ui-datepicker-calendar {
    /*display: none; */
    }
</style>

<div class="box pimPane">
	<c:if test="${!empty form }">
		<div id="slickbox"><div class="${empty form.erreurs ? 'successs' :'errors'}"><p>${form.resultat}</p></div></div>
	</c:if>
	<!-- this is title section -->
	
	<!-- this is work experience section -->
	<div class="module" >
	<div id="changeWorkFormType" style="display: none;">
		<div class="head">
			<h1 id="headChangeWorkFormType">Ajouter une période d'auto-Evaluation</h1>
		</div>

		<div class="inner">
			<form id="frmWorkFormType" action="<c:url value="/listFormType"><c:param name="action" value="creer"/></c:url>" method="post"
				novalidate="novalidate">
				<input type="hidden" name="dates_id" value="0"/>
				<fieldset>
					<ul>
						<li>
	                        <label for="periode_de">Période du</label>
	                        <input id="periode_de" type="text" name="periode_de" class="monthpicker">
	                    </li>
	                    <li>
	                        <label for="periode_au">au</label>
	                        <input id="periode_au" type="text" name="periode_au" 
	                        class="monthpicker">
	                    </li>
						
					</ul>
					<p>
						<input type="button" class="" id="btnWorkExpSave" value="Enregistrer">
						<input type="button" class="reset" id="btnWorkExpCancel" value="Annuler">
					</p>
				</fieldset>
			</form>			
		</div>
	</div>
	<!-- changeWorkFormType  -->

	<div class="miniList" id="sectionWorkFormType">

		<div class="head">
			<h1>Liste des formulaires types</h1>
		</div>

		<div class="inner">
			<p>
				<button id="addWorkFormType">Créer une période</button>
			</p>
				<table id="data" class="table hover">
					<thead>
						<tr>
							<th>Désignation</th>
							<th>Date de création</th>
							<th>Date d'envoi</th>
							<th>Statistique</th>
						</tr>
					</thead>
					<tbody>
					<c:choose>
					<c:when test="${ empty listeDates  }">
						<tr >
							<td colspan="6">Aucun formulaire type</td>
						</tr>
					</c:when>
					<c:otherwise>
					<c:forEach items="${ listeDates  }" var="workExp" varStatus="boucle">
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						 <td>
						 	<c:choose>
								<c:when test="${empty workExp.datesEnvoi}">
									<a href="#">Auto-Evaluation <c:out value="${ workExp.datesPeriodes}" /></a>
								</c:when>
								<c:otherwise>Auto-Evaluation <c:out value="${ workExp.datesPeriodes}" /></c:otherwise>
							</c:choose>	 
						</td>
	                     <td><joda:format value="${workExp.datesCreation }" pattern="dd/MM/yyyy HH:mm:ss"/></td>
	                     <td class="comments">
		                     <c:choose>
								<c:when test="${empty workExp.datesEnvoi}">
									<form id="form${workExp.datesId }" action="<c:url value="/listFormType"><c:param name="action" value="send"/></c:url>" method="post">
			                     		<input type="hidden" name="dates_id" value="${workExp.datesId }"/>
			                     		<input type="hidden" name="dates_datesPeriodesDe" value="${workExp.datesPeriodesDe }"/>
			                     		<input type="hidden" name="dates_datesPeriodesAu" value="${workExp.datesPeriodesAu }"/>
			                     		<input type="submit" class="fwait" value="A envoyer" />
			                     	</form>
								</c:when>
								<c:otherwise><p>Envoyé le <joda:format value="${workExp.datesEnvoi}" pattern="dd/MM/yyyy HH:mm:ss"/></p></c:otherwise>
							</c:choose>	                     	
	                     </td>
	                     <td>
	                     	<p>${workExp.datesNbreEnvoi} envois, ${workExp.datesNbreRecu } Recus, ${workExp.datesNbreRejet} Rejetés</p>
	                     </td>
						</tr>
					</c:forEach>
					</c:otherwise>
					</c:choose>
					</tbody>
				</table>
			


		</div>
	<span id="workExpMessagebar" ></span>
	</div>
	</div>
</div>
"<div id='loader' style='display:none;'>
            <div class='popup_block'>
                <p align='center'>Traitement en cours .....<br/>
					<img src="<c:url  value="/resources/images/loader9.gif" />" alt='' style='margin: 5px;'/>
                </p>
                <p  align='center'>L'opération peut durer plusieurs minutes, ne pas fermer la page.<br/>La machine à café se trouve dans le couloir derrière vous !
                </p>
            <div class='clear'></div>
            </div>
        </div>		

<script>
$(document).ready(function(){
	setTimeout(function(){
	 	 $('#slickbox').hide('slow');
		}, 3000);
	
	$('.monthpicker').monthpicker();

	$(".fwait").click(function(){
		$("#loader").show();
	});

	$('table a').on('click', function(event) {
        event.preventDefault();
        //var dates_id = $("input[name='dates_id']", $(this).parent()).val();
        var dates_id = $(this).closest("tr").find("input[name='dates_id']").val();
        var dates_datesPeriodesDe = $(this).closest("tr").find("input[name='dates_datesPeriodesDe']").val();
        var dates_datesPeriodesAu = $(this).closest("tr").find("input[name='dates_datesPeriodesAu']").val();

        $("#dates_id").val(dates_id);
  	  	$("#periode_de").val(dates_datesPeriodesDe);
	  	$('#periode_au').val(dates_datesPeriodesAu);
		$("#headChangeWorkFormType").text("Modifier le calendrier du formulaire d'auto-évaluation");
	  	$("#changeWorkFormType").show();
	  	$("#addWorkFormType").hide();
	  	$("#btnWorkExpSave").hide();
        
	});
	$("#addWorkFormType").click(function(){
		$("#changeWorkFormType").show();
		$("#addWorkFormType").hide();
		$("#btnWorkExpSave").show();
	});

	$("#btnWorkExpCancel").click(function(){
		validatorFormType.resetForm();
		$("#changeWorkFormType").hide();
		$("#addWorkFormType").show();
	});

	$("#btnWorkExpSave").click(function(){
		$("#frmWorkFormType").submit();
	});

	var validatorFormType = $("#frmWorkFormType").validate({
		rules: {
			'periode_de':{
				required: true
				},
			'periode_au':{
				required: true
				}
			},
		messages:{
			'periode_de':{
				required: "Selectionner une date"
			},
			'periode_au':{
				required: "Selectionner une date"
			}
		}
	});
		
});
</script>	
<%@ include file="../../templates/footer.jsp"%>