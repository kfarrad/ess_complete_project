<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Liste des clients existants" />
<c:set var="active" value="Liste des clients" />
<%@ include file="../../templates/header.jsp"%>
<div id="corps">
	<c:choose>
		<%-- Si aucun client n'existe , affichage d'un message par défaut. --%>
		<c:when test="${ empty clients }">
			<p class="erreur">Aucun client enregistré.</p>
		</c:when>
		<%-- Sinon, affichage du tableau. --%>
		<c:otherwise>
			<table class="table">
				<tr>
					<th>Nom</th>
					<th>Adresse</th>
					<th class="modifier">Modifier</th>
					<th class="modifier">Creer un projet</th>
				</tr>
				<%-- Parcours de la Map des clients , et utilisation de l'objet varStatus. --%>
				<c:forEach items="${ clients }" var="mapClients" varStatus="boucle">
					<%-- Simple test de parité sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						<%-- Affichage des propriétés du bean Client, qui est stocké en tant que valeur de l'entrée courante de la map --%>
						<td><c:out value="${ mapClients.value.nom }" /></td>
						<td><c:out value="${ mapClients.value.adresse }" /></td>
						<%-- Lien vers la servlet de suppression, avec passage du nom du client - c'est-à-dire la clé de la Map - en paramètre grâce à la balise <c:param/>. --%>
						<td class="modifier"><a
							href="<c:url value="/clientUpdate"><c:param name="idClient" value="${ mapClients.key }" /></c:url>">
								<img src="<c:url value="/resources/images/modification.png"/>"
								alt="modification" />
						</a></td>
						<td class="modifier"><a
							href="<c:url value="/projetCreation"><c:param name="idClient" value="${ mapClients.key }" /></c:url>">
								<img src="<c:url value="/resources/images/attribution.png"/>"
								alt="attribution" />
						</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
</div>
<%@ include file="../../templates/footer.jsp"%>