<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Resultat" />
<c:set var="active" value="result" />
<%@ include file="../../templates/header.jsp"%>
<div id="corps">
	<fieldset>

		<legend>Informations Utilisateur</legend>
		<p class="info">${ form.resultat }</p>
		<p>
			Nom :
			<c:out value="${ profil.nom }" />
		</p>
		<p>
			Prénom :
			<c:out value="${ profil.prenom }" />
		</p>
		<p>
			Adresse :
			<c:out value="${ profil.adresse }" />
		</p>
		<p>
			Numéro de téléphone :
			<c:out value="${ profil.telephone }" />
		</p>
		<p>
			Email :
			<c:out value="${ profil.email }" />
		</p>
		<p>
			Role :
			<c:out value="${ user.role }" />
		</p>


		<p>
			<a href="<c:url value="/userProfilList"></c:url>"> Liste des
				utilisateurs </a>
		</p>

	</fieldset>
</div>

<%@ include file="../../templates/footer.jsp"%>