<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Liste des projets" />
<c:set var="active" value="Liste des projets" />
<%@ include file="../../templates/header.jsp"%>
<div id="corps">
	<c:choose>
		<%-- Si aucun client n'existe en session, affichage d'un message par défaut. --%>
		<c:when test="${ empty projetUsers }">
			<p class="erreur">Aucun projet enregistré.</p>
		</c:when>
		<%-- Sinon, affichage du tableau. --%>
		<c:otherwise>
			<table class="table">
				<tr>
					<th>Nom</th>
					<th>Utilisateurs</th>
					<th class="modifier">Attribuer</th>
					<th class="modifier">Modifier</th>
				</tr>
				<%-- Parcours de la Map des clients en session, et utilisation de l'objet varStatus. --%>
				<c:forEach items="${ projetUsers }" var="mapProjets"
					varStatus="boucle">
					<%-- Simple test de parité sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						<%-- Affichage des propriétés du bean Client, qui est stocké en tant que valeur de l'entrée courante de la map --%>
						<td><c:out value="${ mapProjets.key.getNom()}" /></td>
						<td><c:out value="${ mapProjets.value.toString() }" /></td>
						<%-- Lien vers la servlet de suppression, avec passage du nom du client - c'est-à-dire la clé de la Map - en paramètre grâce à la balise <c:param/>. --%>
						<td class="attribuer"><a
							href="<c:url value="/projetAttribution"><c:param name="id" value="${ mapProjets.key.getId() }" /></c:url>">
								<img src="<c:url value="/resources/images/attribution.png"/>"
								alt="attribution" />
						</a></td>
						<td class="modifier"><a
							href="<c:url value="/projetUpdate"><c:param name="id" value="${ mapProjets.key.getId() }" /></c:url>">
								<img src="<c:url value="/resources/images/modification.png"/>"
								alt="modification" />
						</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
</div>
<%@ include file="../../templates/footer.jsp"%>