<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Ajout d'une publication" />
<c:set var="active" value="Gestion de contenu" />
<%@ include file="../../templates/header.jsp"%>

<style>
#userBD {
	height: 200px;
	width: 300px;
}
</style>
	<form action="<c:url value="/ajouterContenu"/>" id="contenuForm"
		method="post">
		<fieldset>
			<legend>Ajout d'une publication</legend>
			<p>Vous pouvez ajouter une publication via le formulaire
				ci-dessous</p>
			<br /> <label for="contenu">Titre: </label> <input type="text"
				id="title" name="titreContenu" size="30" maxlength="60" placeholder="Entrez un titre"/> <br />
			<label for="contenu">Contenu: </label>
			<textarea rows="3" cols="50" name="contenu" form="contenuForm"
				id="specialiteCandidat" placeholder="Entrez un contenu"></textarea>
			<br /> <input type="submit" value="Ajouter une publication" />
		</fieldset>

		<p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
	</form>


<%@ include file="../../templates/footer.jsp"%>