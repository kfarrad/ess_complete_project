<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	ProjetDaoImpl projetDao = (ProjetDaoImpl) (request
	.getAttribute("projetDao"));
	CalendarDaoImpl calendarDao = (CalendarDaoImpl) (request
	.getAttribute("calendarDao"));
	UserDaoImpl utilisateurDao = (UserDaoImpl) (request
	.getAttribute("utilisateurDao"));
	UserProfilDaoImpl profilDao = (UserProfilDaoImpl) (request
	.getAttribute("profilDao"));
	String resultat = (String)request.getAttribute("resultat");
	List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
	List<Utilisateur> utilisateurs = utilisateurDao.lister();
	int nbUser = 0;
	for (Utilisateur elt : utilisateurs) {
		if (elt.isUser() && elt.isActif())
			listeUtilisateurs.add(nbUser++, elt);
	} %>

<script>
var nb_user = <%=nbUser%>;
function goTo()
{	
	document.location.href="calendarBilan?annee="+$("#annee").val()+"&mois="+$("#mois").val();	
}
function gotoInfo(id){
	
	msg.open(document.getElementById('info_'+id));
	return false;
}

function affiche(){
	var result="${resultat}";
	if(result!="") msg.open(result);
	return false;
}

	function goToAll() {
		document.formRappelG.all.value = "true";
		document.formRappelG.submit();
	}
	
jQuery(document).ready(function() {
		affiche();
		
		$("#dynamic").scroll(function(){
			  $("#divSelect").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
			  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
			  				
			});
});	
		 
</script>


<%
	int iMonth = -1;
	int iYear = -1;
	Calendar ca = new GregorianCalendar();
	int mois = ca.get(Calendar.MONTH);
	int annee = ca.get(Calendar.YEAR);
	int iTday = ca.get(Calendar.DATE);

	try {
		iYear = (Integer) request.getAttribute("annee");
		iMonth = (Integer) request.getAttribute("mois");
	} catch (Exception e) {
		e.printStackTrace();
	}

	if (iYear == -1) {
		iYear = annee;

	}
	if (iMonth == -1) {
		iMonth = mois;
	}
%>
<div id="divSelect" class="TabDyn">
	<table class="table" id="selection">
		<tbody>
			<tr class="odd">
				<td>Year</td>
				<td><select id="annee" name="annee" onchange="goTo()">
						<%
							// start year and end year in combo box to change year in calendar
							for (int iy = annee - 20; iy <= annee + 20; iy++) {
								if (iy == iYear) {
						%>
						<option value="<%=iy%>" selected="selected"><%=iy%></option>
						<%
							} else {
						%>
						<option value="<%=iy%>"><%=iy%></option>
						<%
							}
							}
						%>
				</select></td>
				<td align="center"><h3>
						<%
							Calendar calend = Calendar.getInstance();
							calend.set(Calendar.YEAR, 2008);
							calend.set(Calendar.MONTH, iMonth);
							calend.set(Calendar.DAY_OF_MONTH, 01);
						%>
						<%=new SimpleDateFormat("MMMM").format(calend.getTime())%>
						<%=iYear%></h3></td>
				<td>Month</td>
				<td><select id="mois" name="mois" onchange="goTo()">
						<%
							// print month in combo box to change month in calendar
							for (int im = 0; im <= 11; im++) {
								if (im == iMonth) {
									calend.set(Calendar.MONTH, im);
						%>
						<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
						<%
							} else {
									calend.set(Calendar.MONTH, im);
						%>
						<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
						<%
							}
							}
						%>
				</select></td>
				
			</tr>
		</tbody>
	</table>
</div>

<div class="box">
	<div id="scrolableContent">
	<table class="table" id="selection">
		<tbody>
			<tr class="odd">
				<%
					int m = 0;
					for (Utilisateur user : listeUtilisateurs) {
						UserProfil profil = user.getProfil();
						List<Calendrier> listeCalendrier = new ArrayList<Calendrier>();
						
						List<Projet> listeProjets = null;
						if(iYear>=annee && iMonth>=mois) listeProjets=projetDao.listerUserProjetsActif(user);
						else listeProjets = projetDao.listerUserProjets(user);
						
						List<Calendrier> calendriers = calendarDao
								.listerCalendrier(user, iMonth, iYear);
						Map<String, Double> bilan = new HashMap<String, Double>();
						Double total_bilan = 0.0;
						Double total_presence = 0.0;
						if( calendriers.size() != 0) {
							 for(Projet projet: listeProjets){
								 for(Calendrier calendrier: calendriers){				 	
							 		if(calendrier.getId_projet()==projet.getId()) listeCalendrier.add(calendrier);
							 	}
							 
							 }
						 }
						
						if (!listeCalendrier.isEmpty() && !listeProjets.isEmpty()) {
							for (int j = 0; j < listeCalendrier.size(); j++) {
								Long id_projet = listeCalendrier.get(j).getId_projet();
								for (Projet projet : listeProjets) {
									String nom_projet = projet.getNomProjet(id_projet);
									if (nom_projet != null) {
										if (!nom_projet.contains("CP")
												&& !nom_projet.contains("RTT")
												&& !nom_projet.contains("CSS"))
											total_presence += listeCalendrier.get(j)
													.getTotal_jour();
										else
											bilan.put(nom_projet, listeCalendrier
													.get(j).getTotal_jour());
										total_bilan += listeCalendrier.get(j)
												.getTotal_jour();
									}
								}

							}
							bilan.put("PRESENCE", total_presence);
							bilan.put("TOTAL", total_bilan);
						}
				%>

				<td class="bilan">
				
					<div id="info_<%=profil.getId() %>" style="display: none;">
						<fieldset>

							<legend>Informations Utilisateur</legend>
							<p>
								Nom :
								<%=profil.getNom() %></p>
							<p>
								Prénom :<%if(profil.getPrenom()!=null ){%>
								<%=profil.getPrenom() %>
								<%} %>
							</p>
							<p>
								Adresse :<%=profil.getAdresse() %>
							</p>
							<p>
								Numéro de téléphone :<%=profil.getTelephone() %>
							</p>
							<p>
								Email :<a
									href="<c:url value="/contactMail">
            					<c:param name="idProfil" value="<%=profil.getId().toString() %>" /></c:url>">
									<%=profil.getEmail() %></a>
							</p>
						</fieldset>
					</div>
					<table>
						<tr>
							<td class="none bName">
								<fieldset class="name">
									<a href="userCalendar?id=<%=user.getId_profil()%>&annee=<%=iYear%>&mois=<%=iMonth%>"><h2><%=profil.getName()%></h2></a>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td class="none bInfo">
								<table id="affichage_<%=m%>" class="table">
									<tr>

										<td>
											<h3>
												Status:
												<%
													if (listeCalendrier.isEmpty()) {
												%>
												<span id="status" class="nonSoumis"> Non soumis </span>
												<%
														} else {
																if (listeCalendrier.get(0).getStatus() == 0) {
													%>
												<span id="status" class="soumis"> Sauvé </span>
												<%
														} else {
																	if (listeCalendrier.get(0).getStatus() == 1) {
													%>
												<span id="status" class="traitement"> Soumis </span>
												<%
														} else {
													%>
												<span id="status" class="approuve"> Approuvé </span>
												<%
														}
																}
															}
													%>
											</h3>
										</td>
										<td>
											<%
												if (iTday >= 15) { 
														if ((listeCalendrier.isEmpty())
																|| ((!listeCalendrier.isEmpty()) && (listeCalendrier
																		.get(0).getStatus() == 0))) {
											%>
											<form name="formRappel" method="post"
												action="<c:url value="/calendarBilan"></c:url>">
												<input type="submit" name="rappel" id="rappel"
													value="Rappel" /> <input type="hidden" name="id"
													value="<%=user.getId_profil()%>" /> <input type="hidden"
													name="annee" value="<%=iYear%>" /> <input type="hidden"
													name="mois" value="<%=iMonth%>" />

											</form> <%
 	}
 		}
 %>
											<button name="informations"
												onclick="gotoInfo(<%=profil.getId()%>)">Informations</button>
										</td>
									</tr>
									<tr>
										<th>cra</th>
										<th align="center" height="15"><span>Jours</span></th>
									</tr>

									<%-- <%
										int k = 0;
											for (Map.Entry<String, Double> e : bilan.entrySet()) {
												String key = e.getKey();
												Double value = e.getValue();
									%>
									<tr <%if (k % 2 == 0) {%> class="odd" <%}%>>
										<td><span><%=key%></span></td>
										<td><span><%=value%></span></td>
									</tr>
									<%
										k++;
											}
									%> --%>
									<%
										if (!listeProjets.isEmpty()) {
												for (int i = 0; i < listeProjets.size(); i++) {
									%>
									<tr <%if (i % 2 == 0) {%> class="odd" <%}%>>
										<td><span><%=listeProjets.get(i).getNom()%></span></td>
										<td><span> <%
 	if ((!listeCalendrier.isEmpty())
 						&& (i < listeCalendrier.size())) {
 %> <%=listeCalendrier.get(i).getTotal_jour()%> <%
 	} else {
 %> 0.0 <%
 	}
 %>
										</span></td>
									</tr>
									<%
										}
											}
									%>
									<tr>
										<td><span> Total </span></td>
										<td><%=total_bilan %></td>
									</tr>

								</table>
							</td>
						</tr>

					</table>
				</td>
				<%
					m++;
					}
				%>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="rappelForm" class="TabDyn">
		<form name="formRappelG" method="post"
			action="<c:url value="/calendarBilan"></c:url>">
			<button name="rappelG" onclick="goToAll()">Rappel General</button>
			<input type="hidden" name="annee" value="<%=iYear%>" /> <input
				type="hidden" name="mois" value="<%=iMonth%>" /> <input
				type="hidden" name="all" value="0" />

		</form>
		<a id="pdf"  href="
			<c:url value="/viewBilans"></c:url>?annee=<%=iYear%>&mois=<%=iMonth%>">Générer le bilan</a>
	</div>

</div>

