<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Bilan Financier" />
<c:set var="active" value="Bilan Financier" />
<%@ include file="../../templates/header.jsp"%>


<%
ProjetDaoImpl projetDao = (ProjetDaoImpl) (request
.getAttribute("projetDao"));
CalendarDaoImpl calendarDao = (CalendarDaoImpl) (request
.getAttribute("calendarDao"));
UserDaoImpl utilisateurDao = (UserDaoImpl) (request
.getAttribute("utilisateurDao"));
List<Utilisateur> liste_Utilisateurs = utilisateurDao.lister();
 %>
<script>
function goTo()
{	
	document.location.href="bilanFinancier?annee="+$("#annee").val()+"&mois="+$("#mois").val()+"&annee2="+$("#annee2").val()+"&mois2="+$("#mois2").val();	
}
$(document).ready(function() {
	$("#dynamic").scroll(function(){
		  $("#divSelect").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  			
		});	
});
</script>


<%
	int iMonth = -1;
	int iYear = -1;
	int iMonth2 = -1;
	int iYear2 = -1;
	Calendar ca = new GregorianCalendar();
	int mois = ca.get(Calendar.MONTH);
	int annee = ca.get(Calendar.YEAR);
	
	try {
		iYear = Constants.signedIntconv(request.getParameter("annee"));
		iMonth = Constants.signedIntconv(request.getParameter("mois"));
		iYear2 = Constants.signedIntconv(request.getParameter("annee2"));
		iMonth2 = Constants.signedIntconv(request.getParameter("mois2"));
	} catch (Exception e) {
		e.printStackTrace();
	}

	if (iYear == -1) {
		iYear = annee;

	}
	if (iMonth == -1) {
		iMonth = mois;
	}
	if (iYear2 == -1) {
		iYear2 = annee;

	}
	if (iMonth2 == -1) {
		iMonth2 = mois;
	}
	if(iMonth>iMonth2)
		if(iYear>=iYear2)
			iMonth=iMonth2;
	if(iYear>iYear2)
		iYear=iYear2;
	
%>


<div id="corps">
	<form method="post" enctype="multipart/form-data" action="<c:url value="/bilanFinancier"/>">
	<div id="divSelect" class="TabDyn">
	<table class="table" id="selection">
		<tbody>
			<tr class="odd">
				<td><select id="annee" name="annee">
						<%
							// start year and end year in combo box to change year in calendar
							for (int iy = annee - 20; iy <= annee; iy++) {
								if (iy == iYear) {
						%>
						<option value="<%=iy%>" selected="selected"><%=iy%></option>
						<%
							} else {
						%>
						<option value="<%=iy%>"><%=iy%></option>
						<%
							}
							}
						%>
				</select>
						<%
							Calendar calend = Calendar.getInstance();
							calend.set(Calendar.YEAR, 2008);
							calend.set(Calendar.MONTH, mois);
							calend.set(Calendar.DAY_OF_MONTH, 01);
						%>
				<select id="mois" name="mois">
						<%
							// print month in combo box to change month in calendar
							for (int im = 0; im <= 11; im++) {
								if (im == iMonth) {
								calend.set(Calendar.MONTH, im);
						%>
						
						<option value="<%=im%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
						<%
							} else {
									calend.set(Calendar.MONTH, im);
						%>
						<option value="<%=im%>"><%=new SimpleDateFormat("MMMM").format(calend
							.getTime())%></option>
						<%
							}
							}
						%>
				</select>
				</td>
				<td> To </td>
					<td><select id="annee2" name="annee2">
						<%
							// start year and end year in combo box to change year in calendar
							for (int iy2 = annee - 20; iy2 <= annee; iy2++) {
								if (iy2 == iYear2) {
						%>
						<option value="<%=iy2%>" selected="selected"><%=iy2%></option>
						<%
							} else {
						%>
						<option value="<%=iy2%>"><%=iy2%></option>
						<%
							}
							}
						%>
				</select>
						<%
							Calendar calend2 = Calendar.getInstance();
							calend2.set(Calendar.YEAR, 2008);
							calend2.set(Calendar.MONTH, mois);
							calend2.set(Calendar.DAY_OF_MONTH, 01);
						%>
				<select id="mois2" name="mois2">
						<%
							// print month in combo box to change month in calendar
							for (int im2 = 0; im2 <= 11; im2++) {
								if (im2 == iMonth2) {
									calend2.set(Calendar.MONTH, im2);
						%>
						<option value="<%=im2%>" selected="selected"><%=new SimpleDateFormat("MMMM").format(calend2
							.getTime())%></option>
						<%
							} else {
									calend2.set(Calendar.MONTH, im2);
						%>
						<option value="<%=im2%>"><%=new SimpleDateFormat("MMMM").format(calend2
							.getTime())%></option>
						<%
							}
							}
						%>
				</select>
				</td>
					
				</tr>
			</tbody>
		</table>
		
			
<div class="box" id="scrolableBox">
	<table class="table" id="selection">
		<tbody>
			<tr class="odd">
				<%
				int cpt=0;
				for (Utilisateur user : liste_Utilisateurs ){
					ArrayList<List<Calendrier>> liste_Calendriers = new ArrayList<List<Calendrier>>();
					// Liste des calendriers
					for(int iy = iYear,cpt_debut=0;iy<=iYear2;++iy){
						if(cpt_debut==0){ // On ne passe que une fois ici, au début !
							if(iy==iYear2){ // Si la même année, on s'arrête au mois de l'annee2
								for(int im=iMonth;im<=iMonth2;++im)
									liste_Calendriers.add(calendarDao.listerCalendrier(user, im, iy));
							}
							else {//Si au depart, ce n'est pas la même année, on commence au mois de départ
								for(int im=iMonth;im<=11;++im)
									liste_Calendriers.add(calendarDao.listerCalendrier(user, im, iy));
							}
							++cpt_debut;
						}
						else {
							if(iy==iYear2){ // Si la même année, on s'arrête au mois de l'annee2
								for(int im=0;im<=iMonth2;++im)
									liste_Calendriers.add(calendarDao.listerCalendrier(user, im, iy));
							}
							else {//aussi non on est entre l'année de départ et de fin, donc on prends tout les mois
								for(int im=0;im<=11;++im)
									liste_Calendriers.add(calendarDao.listerCalendrier(user, im, iy));
							}
						}
					}
					boolean vide=true;
					for(List<Calendrier> liste_cal : liste_Calendriers){
						if(liste_cal.size()>0){
							vide=false;
							break;
						}
					}
					if(!vide){
						// Peut être améliorer
						ArrayList<Projet> liste_Projet = new ArrayList<Projet>();
						for(List<Calendrier> calendriers : liste_Calendriers){// Parcourt la liste des calendriers
							for(Calendrier cal : calendriers){// Pour chaque cal, on regarde si le projet est déjà répertorier
								if(cal.getTotal_jour()>0){
									Projet projet = projetDao.trouver(cal.getId_projet());
									if(!liste_Projet.contains(projet)){
										liste_Projet.add(projet);
										
									}
								}
							}
						}
						if(liste_Projet.size()>0){
						
				%>
				
				<td class="bilan">
				<input type="hidden" name="<%=user.getId()%>_Number_Of_Projet" value="<%=liste_Projet.size()%>">
					<table>
						<tr>
							<td class="none bName">
								<fieldset class="name">
									<h2><%=user.getName()%>
									<input type="hidden" name="<%=user.getId()%>" value="<%=user.getId()%>">
									<input type="hidden" name="<%=user.getId()%>_Name" value="<%=user.getName()%>">
									</h2>
								</fieldset>
							</td>
						</tr>

						<tr>
							<td class="none bInfo">
								<table id="affichage_<%=cpt%>" class="table">
									<tr>
										<th align="center" height="15"><span>Id</span></th>
										<th align="center" height="15"><span>Projet</span></th>
										<th align="center" height="15"><span>Jours</span></th>
										<th align="center" height="15"><span>CJM</span></th>
										<th align="center" height="15"><span>TJM</span></th>
									</tr>
								
									<%
										double [] projet_Jour = new double [liste_Projet.size()];
										for(double d : projet_Jour)
											d=0.0;
										for(int i = 0; i < liste_Projet.size();++i){
											for(List<Calendrier> calendriers : liste_Calendriers){
												for(Calendrier cal : calendriers){
													if(cal.getId_projet()==liste_Projet.get(i).getId())
														projet_Jour[i]+=cal.getTotal_jour();
												}
											}
									%>
									<tr <%if (i % 2== 0) {%> class="odd" <%}%>>
										
										
										<td><span><%=liste_Projet.get(i).getId()%></span>
										<input type="hidden" name="<%=user.getId()%>_Id_Projet_<%=i%>" value="<%=liste_Projet.get(i).getId()%>"></td>
										<td><span><%=liste_Projet.get(i).getNom()%></span>
										<input type="hidden" name="<%=user.getId()%>_Nom_Projet_<%=i%>" value="<%=liste_Projet.get(i).getNom()%>"></td>
										<td><span>				
									 <%=projet_Jour[i]%>
										</span>
										<input type="hidden" name="<%=user.getId()%>_Nb_Jours_Projet_<%=i%>" value="<%=projet_Jour[i]%>">
										</td>
										<%
										Double CJM=0.0,TJM=0.0;
										if(request.getAttribute("CJM_"+user.getId()+"_Id_Projet_"+liste_Projet.get(i).getId())!=null)
											CJM=(Double)request.getAttribute("CJM_"+user.getId()+"_Id_Projet_"+liste_Projet.get(i).getId());
										if(request.getAttribute("TJM_"+user.getId()+"_Id_Projet_"+liste_Projet.get(i).getId())!=null)
											TJM=(Double)request.getAttribute("TJM_"+user.getId()+"_Id_Projet_"+liste_Projet.get(i).getId());
										%>
										<td><input type="text" name="CJM_<%=user.getId()%>_Projet_<%=i%>" value=<%=CJM %> ></td>
										<td><input type="text" name="TJM_<%=user.getId()%>_Projet_<%=i%>" value=<%=TJM %>></td>
									</tr>
									
									<%
											
											}
										
									
									%>


								</table>
							</td>
						</tr>

					</table>
				</td>
				<%
					cpt++;
					}
				}
			}
				
				%>

			</tr>
		</tbody>
	</table>

</div>
		
		<a href="#" onclick="goTo()">Générer le bilan financier</a>
		<br><br>
	</div>
	<% String s="";if(request.getAttribute("msg_Export")!=null) s=(String)request.getAttribute("msg_Export");%><%=s %>
	<input type="submit" name="submit" value="export" />  <br/>
	Select file : <input type="file" name="file">
	<input type="submit" name="submit" value="import" /> <% s="";if(request.getAttribute("msg_Import")!=null) s=(String)request.getAttribute("msg_Import");%><%=s %> <br/>
</form>
</div>	

	<%@ include file="../../templates/footer.jsp"%>