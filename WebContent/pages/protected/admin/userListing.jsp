<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="title" value="Liste utilisateurs" />
<c:set var="active" value="userList" />
<%@ include file="../../templates/header.jsp"%>
<div class="box">
	<c:choose>
		<%-- Si aucun utilisateur n'existe en session, affichage d'un message par défaut. --%>
		<c:when test="${ empty salariers }">
			<p class="erreur">Aucun utilisateur enregistré.</p>
		</c:when>
		<%-- Sinon, affichage du tableau. --%>
		<c:otherwise>
			<table class="table">
				<tr>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Adresse</th>
					<th>Téléphone</th>
					<th>Email</th>
					<th>Role</th>
					<th>Actif</th>
					<th>Editer</th>
					<th>MDP</th>
					<th>On/Off</th>
					<!-- <th>Del</th> -->
					<th>C.V.</th>
					<th>N° securité sociale</th>
					<th>Carte vitale</th>
					<th>CNI</th>


				</tr>


				<c:forEach items="${ salariers }" var="salarier" varStatus="boucle">
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						<td><c:out value="${salarier.key.nom}" /></td>
						<td><c:out value="${salarier.key.prenom}" /></td>
						<td><c:out value="${salarier.key.adresse}" /></td>
						<td><c:out value="${salarier.key.telephone}" /></td>
						<td><c:out value="${salarier.key.email}" /></td>

						<c:choose>
							<c:when test="${!empty salarier.value}">
								<td><c:out value="${salarier.value.role}" /></td>
								<td><c:out value="${salarier.value.isActif()}" /></td>
								<%-- Lien vers la servlet de suppression, avec passage du nom du client - c'est-à-dire la clé de la Map - en paramètre grâce à la balise <c:param/>. --%>
								<td><a
									href="<c:url value="/userProfilUpdate"><c:param name="idProfil" value="${salarier.key.id}" /></c:url>">
										<img src="<c:url value="/resources/images/modification.png"/>"
										alt="modification" />
								</a>
								<td><a
									href="<c:url value="/userUpdate"><c:param name="idProfil" value="${salarier.key.id}" /></c:url>">
										<img src="<c:url value="/resources/images/password.png"/>"
										alt="modification" />
								</a> <c:choose>
										<c:when test="${!salarier.value.isAdmin() }">
											<td><a
												href="<c:url value="/userSuppression"><c:param name="idProfil" value="${salarier.key.id}" /></c:url>">
													<c:choose>
														<c:when test="${salarier.value.isActif()}">
															<img
																src="<c:url  value="/resources/images/activation_false.png"/>"
																alt="activer" />
														</c:when>
														<c:otherwise>
															<img
																src="<c:url  value="/resources/images/activation.png"/>"
																alt="activer" />
														</c:otherwise>
													</c:choose>
											</a></td>

										</c:when>
										<c:otherwise>
											<td><span>&nbsp; </span></td>
										</c:otherwise>
									</c:choose> <!-- <td><span>&nbsp; vghg</span></td> -->
								<td><c:forEach items="${ mapUserCV[salarier.value.id]  }"
										var="userCV" varStatus="boucle">
										<a href="<c:url value="/download/${userCV}" />"
											title="CV<c:out value="${boucle.index +1}"></c:out>"> <img
											src="<c:url value="/resources/images/cv_icon.png"/>"
											alt="CV<c:out value="${boucle.index +1}"></c:out>" /></a>
									</c:forEach></td>
							</c:when>

							<c:otherwise>
								<td><span> None </span></td>
								<td><span> None </span></td>
								<td><a
									href="<c:url value="/userCreation"><c:param name="idProfil" value="${salarier.key.id}" /></c:url>">
										<img src="<c:url value="/resources/images/inscription.png"/>"
										alt="modification" />
								</a></td>
								<td><span>&nbsp; </span></td>
								<td><span>&nbsp;</span></td>
								<%-- <td><a
									href="<c:url value="/userProfilSuppression"><c:param name="idProfil" value="${salarier.key.id}" /></c:url>">
										<img src="<c:url  value="/resources/images/supprimer.png"/>"
										alt=suppresion />
								</a></td> --%>
								<td><span>&nbsp;</span></td>
							</c:otherwise>
						</c:choose>

						<td><c:out value="${salarier.key.numero_ssn}" /></td>
						<td>
						<c:if test="${!empty salarier.key.carte_vitale}">
							<a
								href="<c:url value="/download/${salarier.key.carte_vitale}"/>"
								target="_blank"
								title="Carte vitale de <c:out value="${salarier.key.nom}"></c:out> <c:out value="${salarier.key.prenom}"></c:out>"><img
									src="<c:url value="/resources/images/carte_vitale.png"/>" /></a>
						</c:if>
						</td>
						<td>
						<c:if test="${!empty salarier.key.cni}">
							<a href="<c:url value="/download/${salarier.key.cni}"/>"
								target="_blank"
								title="Carte d'identité de <c:out value="${salarier.key.nom}"></c:out> <c:out value="${salarier.key.prenom}"></c:out>"><img
									src="<c:url value="/resources/images/cni.png"/>" /></a>
						</c:if>
						</td>

					</tr>
				</c:forEach>
			</table>
			
			<%--For displaying Previous link except for the 1st page --%>
    <c:if test="${currentPage != 1}">
        <td><a href="<c:url value="/userProfilList"><c:param name="page" value="${currentPage - 1}"/></c:url>">Previous</a></td>
    </c:if>
 
    <%--For displaying Page numbers. 
    The when condition does not display a link for the current page--%>
    <table border="1" cellpadding="5" cellspacing="5">
        <tr>
            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <td>${i}</td>
                    </c:when>
                    <c:otherwise>
                        <td><a href="userProfilList?page=${i}">${i}</a></td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tr>
    </table>
     
    <%--For displaying Next link --%>
    <c:if test="${currentPage lt noOfPages}">
        <td><a href="<c:url value="/userProfilList"><c:param name="page" value="${currentPage + 1}"/></c:url>">Next</a></td>
    </c:if>
		</c:otherwise>
	</c:choose>
</div>

<%@ include file="../../templates/footer.jsp"%>