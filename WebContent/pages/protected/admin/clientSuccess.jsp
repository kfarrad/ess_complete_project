<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Bienvenue" />
<c:set var="active" value="bienvenue" />
<%@ include file="../../templates/header.jsp"%>
<fieldset>
	<legend>Succès</legend>
	<p>
		Nom :
		<c:out value="${ client.nom }" />
	</p>
	<p>
		Adresse :
		<c:out value="${ client.adresse }" />
	</p>
	<p class="info">${ form.resultat }</p>

	<p>Redirection vers la liste des clients...
	<p>
</fieldset>

<script>
$(document).ready(function(){
      setTimeout(function() {
       document.write("Redirection vers la liste des projets"); 
       window.location.href = "clientList";
      }, 3000);
    });	
</script>
<%@ include file="../../templates/footer.jsp"%>