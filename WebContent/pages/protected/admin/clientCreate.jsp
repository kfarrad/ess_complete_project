<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Creation d'un client" />
<c:set var="active" value="Creer client" />
<%@ include file="../../templates/header.jsp"%>
<form method="post" action="<c:url value="/clientCreation"/>">
	<fieldset>

		<legend>Informations du Client</legend>
		<label for="nom">Nom<span class="requis">*</span></label> <input
			type="text" id="nom" name="nom"
			value="<c:out value="${client.nom}"/>" size="30" maxlength="60" /> <span
			class="erreur">${form.erreurs['nom']}</span> <br /> <label
			for="adresse">Adresse <span class="requis">*</span></label> <input
			type="text" id="adresse" name="adresse"
			value="<c:out value="${client.adresse}"/>" size="30" maxlength="60" />
		<span class="erreur">${form.erreurs['adresse']}</span> <br />

	</fieldset>
	<p class="info">${ form.resultat }</p>
	<input type="submit" value="Creer" /> <input type="reset"
		value="Remettre à zéro" /> <br />
</form>
<%@ include file="../../templates/footer.jsp"%>