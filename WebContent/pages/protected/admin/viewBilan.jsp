<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<c:set var="title"
	value="Bilan de ${mois}  ${annee} à transmettre par E-mail" />
<c:set var="active" value="Complete your resume" />
<%@ include file="../../templates/header.jsp"%>

<div class="box pimPane">
	<c:if test="${!empty pers }">
		<div id="slickbox">
			<div class="errors">
				<p>Les consultants doivent valider leurs jours:</p>
				<ul>
					<c:forEach items="${pers}" var="user">
						<li>${user}</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</c:if>
	<c:if test="${!empty message }">
		<div id="slickbox">
			<div class="successs">
				<p>${message}</p>
			</div>
		</div>
	</c:if>
	<c:if test="${!empty messages }">
		<div id="slickbox">
			<div class="errors">
				<p>${messages}</p>
			</div>
		</div>
	</c:if>
	<c:if test="${empty pers && empty message && empty messages }">
		<c:choose>
			<c:when test="${empty detailsBilan}">
					NO DATA
				</c:when>
			<c:otherwise>
				<div id="bilan">
					<c:forEach var="user" items="${detailsBilan}">
						<p>
							<c:out value="${user.key}" />
							=
							<c:choose>
								<c:when test="${!empty user.value}">
									<c:forEach var="projet" items="${user.value}" varStatus="statusProjet">
										<B><c:out value="${projet.key.duree}" />&nbsp;jour(s) de <c:out
											value="${projet.key.nom}" />:</B>
										<c:if test="${!empty projet.value }">
											<c:forEach items="${projet.value}" var="conges" varStatus="statusConges">
												<c:choose>
													<c:when test="${conges.value<=1}">
											le&nbsp;<c:out value="${conges.key }" />&nbsp;<c:out
															value="${mois.substring(0, 3)}" />
														<fmt:parseNumber var="congesValueInt" integerOnly="true"
															type="number" value="${conges.value}" />
															<c:choose>
															<c:when test="${conges.value - congesValueInt!=0.0}">
																(<c:out value="${conges.value}" />
															</c:when>
															<c:otherwise>
																(<c:out value="${congesValueInt}" />
															</c:otherwise>
														</c:choose>j)
													</c:when>
													<c:otherwise>
														<fmt:parseNumber var="endDate" integerOnly="true"
															type="number" value="${(conges.key+conges.value-1)}" />
											 du&nbsp;<c:out value="${conges.key}" />&nbsp;<c:out
															value="${mois.substring(0, 3)}" />&nbsp;au&nbsp;<c:out
															value="${endDate}" />&nbsp;<c:out
															value="${mois.substring(0, 3)}" />
													</c:otherwise>
												</c:choose>
												<c:if test="${not statusConges.last}">,</c:if>
											</c:forEach>
										</c:if>
										<c:if test="${not statusProjet.last}">***</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>100%,</c:otherwise>
							</c:choose>
							;
						</p>
					</c:forEach>
				</div>
			</c:otherwise>
		</c:choose>

		<div class="inner">
			<div id="formMail">
				<form id="frmSendMail" action="<c:url value="/viewBilans"></c:url>"
					method="post" novalidate="novalidate">
					<fieldset>
						<ul>
							<li class="largeTextBox"><label for="receiver">Destinateur
									<em>*</em>
							</label> <input id="receiver" type="text" name="receiver"
								value="${email}" size="40px"></li>
							<li class="largeTextBox"><label for="objects">Objet
									<em>*</em>
							</label> <input id="objects" type="text" name="titre"
								value="Bilan de ${mois}  ${annee}" size="40px"></li>
							<li class="largeTextBox"><textarea
									style="height: 100px; width: 600px;" name="body" id="body"></textarea>
							</li>
						</ul>
						<p>
							<input type="button" class="" id="btnSendMail"
								value="Envoyer le Mail"> <input type="button"
								class="reset" id="btnCancel" value="Annuler">
						</p>
					</fieldset>
				</form>
			</div>
		</div>
		<p>
			<button id="btnDoMail">Faire le Mail</button>
		</p>
	</c:if>
</div>


<script>
	$(document).ready(function() {

		$("#btnSendMail").hide();
		$("#btnCancel").hide();
		$("#formMail").hide();
		$("#btnDoMail").click(function() {
			var copie = $('#bilan').text();
			//var purge = copie.trim();
			//var purge = copie.replace("\t", "");
			//var myRegEx=new RegExp(",;","gm");
			var purge = copie.replace(/,;/gm, "\n\n");
			$("#body").val(purge);
			$("#btnSendMail").show();
			$("#btnCancel").show();
			$("#btnDoMail").hide();
			$("#formMail").show();
			$("#bilan").hide();
		});

		$("#btnCancel").click(function() {
			//validatorFormationSuivies.resetForm();
			$("#btnSendMail").hide();
			$("#btnCancel").hide();
			$("#btnDoMail").show();
			$("#formMail").hide();
			$("#bilan").show();
		});

		$("#btnSendMail").click(function() {
			$("#frmSendMail").submit();
		});

		$('.jqte-test').jqte();

		// settings of status
		var jqteStatus = true;
		$(".status").click(function() {
			jqteStatus = jqteStatus ? false : true;
			$('.jqte-test').jqte({
				"status" : jqteStatus
			})
		});

	});
</script>
<%@ include file="../../templates/footer.jsp"%>