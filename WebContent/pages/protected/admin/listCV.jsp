<%@page import="com.clinkast.cra.dao.ResumeDao"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Liste des C.V." />
<c:set var="active" value="liste des CV" />
<%@ include file="../../templates/header.jsp"%>
<%
ResumeDao resumeDao = (ResumeDao) (request
.getAttribute("resumeDao"));
ArrayList<Long> listeUser = (ArrayList<Long>)(request.getAttribute("listeIdUser"));
%>

<div class="box">
	<table class="table">
		<tr>
			<th>C.V N°</th>
			<th>Titre</th>
			<th>Derniere modification</th>
			<th>Modifier C.V.</th>
			<th>Creer pdf</th>
			<th>Afficher pdf</th>
			<th>Supprimer C.V</th>
		</tr>
		<%
		if(listeUser.isEmpty()) {
		%>
				<tr>
					<td colspan="7">Aucun Utilisateurs</td>
				</tr>
		<% }
		else {
			int boucle=0;
			int boucleUser=0;
			%><tr>
			<td colspan="7">&nbsp;</td></tr>
			<%
			for (Long id : listeUser) {
				boucleUser++;
				
				String id_User=id.toString();
				String stringResume = "listResume_"+id_User;
				String stringName = (String) (request.getAttribute("userName_"+id_User));
				List<Resume> listResume = (List<Resume>) (request.getAttribute(stringResume));
				if(listResume.size()!=0) {
					%><tr>
					<th colspan="7">Les CV de <%=stringName%></th></tr>
					<%
					for(Resume resume : listResume){
						boucle++;
						if (boucle % 2 == 0) {
							%><tr class="odd"><%}
						else {%><tr><%}%>
								<td><%=resume.getId() %></td>
								<% if (null != resume.getTitre() ) {%>
								<td><%=resume.getTitre().getNom() %></td>
								<% } else {%>
								<td>&nbsp;</td>
								<% } %>
								<td><%=resume.getDate() %></td>
								<td><a
									href="<c:url value="/resume"><c:param name="idResume" value="<%=resume.getId().toString()%>" /></c:url>">
										<img src="<c:url value="/resources/images/modification.png"/>"
										alt="modification" />
								</a></td>
								<td><a
									href="<c:url value="/xmlResume"><c:param name="idResume" value="<%=resume.getId().toString()%>" /></c:url>">
										<img src="<c:url value="/resources/images/inscription.png"/>"
										alt="modification" />
								</a></td>
								<td>
								<% if (resume.getPdf()!=null) { 
									pageContext.setAttribute("resumePDF", resume.getPdf());%>
									<a	href="<c:url value="/download/${resumePDF }"/>"><img
									src="<c:url value="/resources/images/activation.png"/>"
									alt="modification" /> </a>
								<% } %>
								</td>
							<td>
								<form id="frmTitle" action="<c:url value="/listCV">
									<c:param name="delete" value="<%=resume.getId().toString()%>" /></c:url>" method="post">
									
									<input type="submit" class="" id="btnResumeDel" value="">
								</form>
							</td>
					</tr>
						<%}
					}
				}
			} %>
	</table>

</div>
<%@ include file="../../templates/footer.jsp"%>