<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="title" value="Attribution d'un projet" />
<c:set var="active" value="Attribution d'un projet" />
<%@ include file="../../templates/header.jsp"%>
<div id="corps">
	<fieldset>
		<legend>Informations Projet</legend>
		<p>
			Nom :
			<c:out value="${ projet.nom }" />
		</p>

	</fieldset>
</div>
<div id="corps">
	<form method="post" action="<c:url value="/projetAttribution"/>">
		<fieldset>

			<legend>Selection des utilisateurs</legend>
			<table class="table">

				<c:forEach items="${ utilisateurs }" var="elt" varStatus="boucle">
					<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						<td><c:out value="${elt.getName()}" /></td>
						<td><input type="checkbox" id="userslist" name="userslist"
							value="${ elt.id }"
							<c:if test="${fn:contains(projetUsers, elt.email)}">
					checked="checked"
						</c:if>>
						</td>
					</tr>
				</c:forEach>
			</table>


		</fieldset>
		<input type="submit" value="Creer" /> <br />
	</form>
</div>
<%@ include file="../../templates/footer.jsp"%>