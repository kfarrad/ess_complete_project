<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    ProjetDaoImpl projetDao = (ProjetDaoImpl)(request.getAttribute("projetDao"));
	CalendarDaoImpl calendarDao = (CalendarDaoImpl)(request.getAttribute("calendarDao"));
	UserDaoImpl utilisateurDao =(UserDaoImpl)(request.getAttribute("utilisateurDao"));
	UserProfilDaoImpl profilDao =(UserProfilDaoImpl)(request.getAttribute("profilDao"));
	List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
	List<Utilisateur> utilisateurs =utilisateurDao.lister();
	int i=0;
	
	// listons uniquement les utilisateur de role user
	for(Utilisateur elt : utilisateurs){
		 if(elt.isUser() && elt.isActif()) listeUtilisateurs.add(i++,elt);
	}
	Long id_user = (Long)request.getAttribute("id");
	Long idUser=0L;
	if(id_user!=null){
		idUser= id_user;
	}

	List<Calendrier> listeCalendrier =null;
	List<Projet> listeProjets = null;
	
	Calendar ca = new GregorianCalendar();
	int mois = ca.get(Calendar.MONTH);
	int annee = ca.get(Calendar.YEAR);

    int iMonth = -1;
    int iYear = -1;
    try {
		iYear = (Integer) request.getAttribute("annee");
		iMonth = (Integer) request.getAttribute("mois");
    }
    catch (Exception e) {
		e.printStackTrace();
    }
    
    if (iYear == -1) {
		iYear = annee;
		
    }
    if (iMonth == -1) {
		iMonth = mois;
    }
    if (!listeUtilisateurs.isEmpty()) {
		/* if (idUser == 0)
		    idUser = listeUtilisateurs.get(0).getId_profil(); */
		Utilisateur user = utilisateurDao.trouver(idUser);
		
		if (user != null) {
		    if (iYear >= annee && iMonth >= mois) listeProjets = projetDao.listerUserProjetsActif(user);
		    else listeProjets = projetDao.listerUserProjets(user);
		    
		    
		    
		    listeCalendrier = calendarDao.listerCalendrier(user,
			    iMonth, iYear);
		    
		}
		
		request.setAttribute(Constants.ATT_CALENDRIER, listeCalendrier);
		request.setAttribute(Constants.ATT_PROJETS, listeProjets);
		
		
    }
 // jours férié
    Calendrier jourFeriers = calendarDao.trouver(-1L, -1L,
	    (long) iYear, (long) iMonth);
    List<Integer> feriers = new ArrayList<Integer>();
    
    if (jourFeriers != null) {
	int jour = 1;
	
	for (Float ferier : jourFeriers.getJours()) {
	    if (ferier == -1) feriers.add(jour);
	    jour++;
	}
    }
    request.setAttribute(Constants.ATT_FERIERS, feriers);
    if (listeCalendrier == null)
		listeCalendrier = new ArrayList<Calendrier>();
    if (listeProjets == null) listeProjets = new ArrayList<Projet>();
%>
<script>

	function gotoCalendar() {
		document.location.href = "userCalendar?id=" + $("#id").val()+"&annee="+<%=iYear%>+"&mois="+<%=iMonth%>;
	}

	function setStatusValider() {
		document.formStatus.valider.value = "true";
		//document.formStatus.submit();

	}

	function setStatusRefuser() {
		document.formStatus.refuser.value = "true";
		//document.formStatus.submit();

	}
	
$(document).ready(function() {
	$("#dynamic").scroll(function(){
		  $("#corps").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		  $("#divSelectSalarier").stop().animate({"marginTop": ($("#dynamic").scrollTop()) + "px", "marginLeft":($("#dynamic").scrollLeft()) + "px"}, "slow" );
		});
});
</script>

<div id="divSelectSalarier" class="TabDyn box">
	<span> Selection salarié: <select id="id" name="id"
		onchange="gotoCalendar();">
		<option value="-1">Veuillez selectionner un utilisateur</option>
			<%
				if (listeUtilisateurs != null) {
					if (listeUtilisateurs.size() != 0) {
						for (Utilisateur userElt : listeUtilisateurs) {
							UserProfil profil = userElt.getProfil();
			%>
			<option value="<%=userElt.getId_profil()%>"
				<%if (userElt.getId_profil() == idUser) {%> selected="selected"
				<%}%>>
				<%=profil.getNom()%>
				<%
					if (profil.getPrenom() != null)
				%><%=profil.getPrenom()%></option>

			<%
				}
					}
				}
			%>
	</select>
	</span>
</div>
<%if(idUser!=0L){ %>
<c:import url="/pages/protected/defaultUser/calendar.jsp" />
<%} %>

<c:if test="${sessionScope.sessionUser.isAdmin()}">
	<form name="formStatus" method="post"
		action="<c:url value="/userCalendar"></c:url>">
		<%
		
		if (idUser != 0L ) {
	%>
		<div class="submit box" id="subAdmin">
			<input type="hidden" name="anneeStatus" value="<%=iYear%>" /> <input
				type="hidden" name="moisStatus" value="<%=iMonth%>" /> <input
				type="hidden" name="userId" value="<%=idUser%>" /> <input
				type="hidden" name="valider" id="valider" value="" /> <input
				type="hidden" name="refuser" id="refuser" value="" />
			<%
			if (listeCalendrier.size() != 0) {

						if (listeCalendrier.get(0).getStatus() !=0) {
		
			if (listeCalendrier.get(0).getStatus() == 1) {%>	<input type="submit" name="validation" id="validation"
				value="Valider" onclick="setStatusValider();" /> 
			<%} %>
			<input type="submit" name="refus" id="refus" value="Refuser"
				onclick="setStatusRefuser();" />
		</div>
		<%
		}
				}
			}

	%>
	</form>
	
</c:if>
