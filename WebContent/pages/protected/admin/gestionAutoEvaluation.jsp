<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<c:set var="title" value="Gestion des formulaires d'auto-évaluation" />
<c:set var="active" value="Complete your resume" />
<%@ include file="../../templates/header.jsp"%>

<div class="box pimPane">
	<div class="module" >
		<div class="miniList" id="sectionWorkExperience">
			<div class="head">
				<h1>Liste des AutoEvaluations récues</h1>
			</div>

			<div class="inner">
				<table id="" class="table hover">
					<thead>
						<tr>
							<th>Désignation</th>
							<th>Consultant</th>
							<th>Date Reception</th>	
							<th>PDF</th>						
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ empty gestionEvaluations  }">
								<tr >
									<td colspan="6">Aucun formulaire type</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${ gestionEvaluations  }" var="gestionAutoEvaluation" varStatus="boucle">
									<tr class="${boucle.index % 2 == 0 ? 'odd' : ''}">
						 				<td>Auto-Evaluation ${ gestionAutoEvaluation.dates.datesPeriodes}</td>
	                     				<td>${gestionAutoEvaluation.user.profil.prenom} ${gestionAutoEvaluation.user.profil.nom}</td>
	                     				<td><joda:format value="${gestionAutoEvaluation.gestionEvaluationsDateTransmission}" pattern="dd/MM/yyyy"/></td>
	                     				<td>
											<a class="infobulle" title = "Voir le pdf" href="<c:url value="/viewPDF"><c:param name="id" value="${gestionAutoEvaluation.dates.datesId}&${gestionAutoEvaluation.autoEvaluations.autoEvaluationsId}&${gestionAutoEvaluation.user.id}" /></c:url>">
												<img src="<c:url value="/resources/images/logo-pdf.jpg"/>" alt="voir le pdf" />
											</a>
										</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<%@ include file="../../templates/footer.jsp"%>