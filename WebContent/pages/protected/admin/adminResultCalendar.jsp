<%@ page pageEncoding="UTF-8"%>
<%@ page language="java"
	import="java.util.*,java.text.*,com.clinkast.cra.beans.*,com.clinkast.cra.dao.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	ProjetDaoImpl projetDao = (ProjetDaoImpl)(request.getAttribute("projetDao"));
	CalendarDaoImpl calendarDao = (CalendarDaoImpl)(request.getAttribute("calendarDao"));
	UserDaoImpl utilisateurDao =(UserDaoImpl)(request.getAttribute("utilisateurDao"));
	UserProfilDaoImpl profilDao =(UserProfilDaoImpl)(request.getAttribute("profilDao"));
	List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
	List<Utilisateur> Utilisateurs =utilisateurDao.lister();
	int i=0;
	for(Utilisateur elt : Utilisateurs){
		if(elt.isUser())listeUtilisateurs.add(i++,elt);
	}
	Long id_user = (Long)request.getAttribute("id");
	Long idUser=0L;
	if(id_user!=null){
		idUser= id_user;
	}
	if(idUser==0) idUser = listeUtilisateurs.get(0).getId_profil(); 
	Utilisateur utilisateur = utilisateurDao.trouver(idUser);
	
	int iMonth=-1;
	int iYear=-1;
	if(utilisateur!=null){

		List<Projet> listeProjets =projetDao.listerUserProjets(utilisateur);
		Calendar ca = new GregorianCalendar();
		int mois = ca.get(Calendar.MONTH);
		int annee = ca.get(Calendar.YEAR);
		
		 try {
			 iYear= ( Integer)request.getAttribute("annee");
			 iMonth= ( Integer)request.getAttribute("mois");
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 
		 if(iYear==-1)
		 {
			  iYear=annee;
			  
		 }
		 if(iMonth==-1)
		 {
			 iMonth=mois;
		 }
		 
		
		List<Calendrier> listeCalendrier= calendarDao.listerCalendrier(utilisateur, iMonth, iYear);
		
		request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier );
		request.setAttribute( Constants.ATT_PROJETS, listeProjets );
	}
%>
<script>
	function gotoCalendar() {		
		document.location.href="calendarBilan?id="+$("#id").val();
	}

</script>

<div class="TabDyn">
	<span> Selection salarié: <select id="id" name="id"
		onchange="gotoCalendar();">
			<option value="">--</option>
			<%
				if (listeUtilisateurs!=null){
					if( listeUtilisateurs.size() != 0) {
					for (Utilisateur userElt : listeUtilisateurs) {
						UserProfil profil = userElt.getProfil();
			%>
			<option value="<%=userElt.getId_profil()%>"
				<% 	if (userElt.getId_profil()==idUser) {
			%> selected="selected"
				<%}%>>
				<%=profil.getNom()%>
				<% if(profil.getPrenom()!=null) %><%=profil.getPrenom()%></option>

			<%
				}
				}}
			%>
	</select>
	</span>
</div>

<c:import url="/pages/protected/defaultUser/resultCalendar.jsp" />