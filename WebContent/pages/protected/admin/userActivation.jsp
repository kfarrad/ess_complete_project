<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="title" value="Activation d'un profil" />
<c:set var="active" value="Activer compte" />
<%@ include file="../../templates/header.jsp"%>
<p class="info">${ erreur }</p>
<p>
	Nom :
	<c:out value="${ profil.nom }" />
</p>
<p>
	Prénom :
	<c:out value="${ profil.prenom }" />
</p>
<p>
	Adresse :
	<c:out value="${ profil.adresse }" />
</p>
<p>
	Numéro de téléphone :
	<c:out value="${ profil.telephone }" />
</p>
<p>
	Email :
	<c:out value="${ profil.email }" />
</p>
<p>
	actif :
	<c:out value="${ profil.actif }" />
</p>
<p>
	<a href="<c:url value="/userList"/>">liste des profils</a>
</p>
<%@ include file="../../templates/footer.jsp"%>