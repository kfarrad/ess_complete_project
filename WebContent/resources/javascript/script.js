$(function(){
    $('.memberlist li a.member').each(ajaxifyMemberList);
    $('.memberlist li a.delete').each(deleteMember);
    $('.messagelist').tablesorter();
});

function ajaxifyMemberList() {
    $(this).click(addLoadMember);
}

function addLoadMember() {
    var a = $(this);
    a.after(document.createElement('div'));
    a.next('div').load(a.attr('href') + ' #content table', loadMember);
    return false;
}

function loadMember() {
	var div = $(this);
    div.hide().css('border-bottom', '1px solid black');
    var a = div.prev('a');
    a.replaceWith('<h3><a>' + a.text() + '</a></h3>');
    div.prev('h3').addClass('clickable').find('a').click(addToggle);
    div.slideDown('normal');
}

function addToggle() {
    $(this).parent().next('div').slideToggle('normal');
}

function deleteMember() {
    var a = $(this);
    a.click(function() {
        a.parent().load(a.attr('href') + ' .message-body p', function(responseText, textStatus){
            if (textStatus == "success") {
                $(this).fadeOut("slow");
            }
        });
        return false;
    });
}