
    var lang_addSkill = "Ajouter compétences";
    var lang_editSkill = "Modifier des compétences";
    var lang_skillRequired = 'Requis';
    var lang_selectSkillToDelete = "Sélectionnez les enregistrements à supprimer";
    var lang_commentsMaxLength = "Doit être inférieur à niveau %montant%";
    var lang_yearsOfExpShouldBeNumber = "Doit être un nombre";
    var lang_yearsOfExpMax = "Doit être inférieur à %montant%";
    var canUpdate = '1';

    $(document).ready(function() {
        //To hide unchanged element into hide and show the value in span while editing
       // $('#skill_name').after('<span id="static_skill_name" style="display:none;"></span>');

        function addEditLinks() {
            // called here to avoid double adding links - When in edit mode and cancel is pressed.
            removeEditLinks();
            $('form#frmDelSkill table tbody td.name').wrapInner('<a class="edit" href="#"/>');
        }
        
        function removeEditLinks() {
            $('form#frmDelSkill table tbody td.name a').each(function(index) {
                $(this).parent().text($(this).text());
            });
        }
        
        //hide add section
        $("#changeSkill").hide();
        $("#skillRequiredNote").hide();
        
        //hiding the data table if records are not available
        if($("div#tblSkill .chkbox").length == 0) {
            //$("#tblSkill").hide();
            $('div#tblSkill .check').hide();
            $("#editSkill").hide();
            $("#delSkill").hide();
        }
        
        //if check all button clicked
        $("#skillCheckAll").click(function() {
            $("div#tblSkill .chkbox").removeAttr("checked");
            if($("#skillCheckAll").attr("checked")) {
                $("div#tblSkill .chkbox").attr("checked", "checked");
            }
        });
        
        //remove tick from the all button if any checkbox unchecked
        $("div#tblSkill .chkbox").click(function() {
            $("#skillCheckAll").removeAttr('checked');
            if($("div#tblSkill .chkbox").length == $("div#tblSkill .chkbox:checked").length) {
                $("#skillCheckAll").attr('checked', 'checked');
            }
        });
        
        $("#addSkill").click(function() {
            
            removeEditLinks();
            clearMessageBar();
            $('div#changeSkill label.error').hide();        
            
            //changing the headings
            $("#headChangeSkill").text(lang_addSkill);
            $("div#tblSkill .chkbox").hide();
            $("#skillCheckAll").hide();
            
            //hiding action button section
            $("#actionSkill").hide();
            
            $('#static_skill_name').hide().val("");
            $("#skill_name").show().val("");
            $("#skill_name option[class='added']").remove();
            $("#skill_list").val("");
            $("#skill_years_of_exp").val("");
            
            //show add form
            $("#changeSkill").show();
            $("#skillRequiredNote").show();
        });
        
        //clicking of delete button
        $("#delSkill").click(function(){
            
            clearMessageBar();
            
            if ($("div#tblSkill .chkbox:checked").length > 0) {
            	$("#deleteSkill").val("true");
                $("#frmDelSkill").submit();
            } else {
            	msg.open(lang_selectSkillToDelete);
            	//$("#skillMessagebar").attr('class', 'messageBalloon_notice').text();
            }
            
        });
        
        $("#btnSkillSave").click(function() {
            clearMessageBar();

    		$("#saveSkill").val("true"); 
            $("#frmSkill").submit();
        });
        
        //form validation
        var skillValidator =
            $("#frmSkill").validate({
            rules: {
                'skill[name]': {required: true},
                'skill[years_of_exp]': {required: false, digits: true, max: 99},
                'skill[list]': {required: false}
            },
            messages: {
                'skill[name]': {required: lang_skillRequired},
                'skill[years_of_exp]': {digits: lang_yearsOfExpShouldBeNumber, max: lang_yearsOfExpMax},
                'skill[list]': {}
            }
        });
        
        $("#btnSkillCancel").click(function() {
            clearMessageBar();
            if(canUpdate){
                addEditLinks();
            }
            
            skillValidator.resetForm();
            
            $('div#changeSkill label.error').hide();
            
            $("div#tblSkill .chkbox").removeAttr("checked").show();
            $("#skill_id").removeAttr("value");
            //hiding action button section
            $("#actionSkill").show();
            $("#changeSkill").hide();
            $("#skillRequiredNote").hide();        
            $("#skillCheckAll").show();
            
        });
        
        $('form#frmDelSkill').on('click','a.edit', function(event) {
            event.preventDefault();
            clearMessageBar();
            
            //changing the headings
            $("#headChangeSkill").text(lang_editSkill);
            
            skillValidator.resetForm();
            
            $('div#changeSkill label.error').hide();
            
            //hiding action button section
            $("#actionSkill").hide();
            
            //show add form
            $("#changeSkill").show();
            var code = $(this).closest("tr").find('input.chkbox:first').val();
            
            //$('#static_skill_name').text($("#skill_name_" + code).val()).show();
            
            
            
            // remove any options already in use
            //$("#skill_name option[class='added']").remove();
            
           /* $('#skill_name').
                append($("<option class='added'></option>").
                attr("value", code).
                text($("#skill_name_" + code).val())); 
            $('#skill_name').val(code).hide();*/
            $("#skill_id").val(code);
            $('#skill_name').val($("#skill_name_" + code).val());
            $("#skill_years_of_exp").val($("#years_of_exp_" + code).val());
            $("#skill_list").val($("#skill_list_" + code).val());
            
            $("#skillRequiredNote").show();
            
            $("div#tblSkill .chkbox").hide();
            $("#skillCheckAll").hide();        
        });
    });

