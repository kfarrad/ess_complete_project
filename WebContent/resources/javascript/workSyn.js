/*
 * Work Experience Section
 */

function addEditLinks() {
    // called here to avoid double adding links - When in edit mode and cancel is pressed.
    removeEditLinks();
    $('form#frmDelWorkExperience table tbody td.name').wrapInner('<a class="edit" href="#"/>');
}

function removeEditLinks() {
    $('form#frmDelWorkExperience table tbody td.name a').each(function(index) {
        $(this).parent().text($(this).text());
    });
}

function clearMessageBar() {
    $("#mainMessagebar").text("").attr('class', "");
    $("#workExpMessagebar").text("").attr('class', "");
    $("#educationMessagebar").text("").attr('class', "");
    $("#skillMessagebar").text("").attr('class', "");
    $("#languageMessagebar").text("").attr('class', "");
    $("#licenseMessagebar").text("").attr('class', "");
}

$(document).ready(function(){    
		
	 //hide add work experience section
    $("#changeWork").hide();
    $("#workExpRequiredNote").hide();

    //hiding the data table if records are not available
    if($(".chkbox1").length == 0) {
        $('div#sectionWorkExperience .check').hide();
        $("#editWorkExperience").hide();
        $("#delWorkExperience").hide();
    }

    //if check all button clicked
    $("#workCheckAll").click(function() {
        $(".chkbox1").removeAttr("checked");
        if($("#workCheckAll").attr("checked")) {
            $(".chkbox1").attr("checked", "checked");
        }
    });

    //remove tick from the all button if any checkbox unchecked
    $(".chkbox1").click(function() {
        $("#workCheckAll").removeAttr('checked');
        if($(".chkbox1").length == $(".chkbox1:checked").length) {
            $("#workCheckAll").attr('checked', 'checked');
        }
    });
    
	 
	 
//--this section is for work experience

	 
	//form validation
	 var workExperienceValidator =
		 $("#frmWorkSyn").validate({
	     rules: {
	         'experience[employer]': {
	             required: true,
	             maxlength: 100
	         },
	         'experience[jobtitle]': {
	             required: true,
	             maxlength: 100
	         },
	         'experience[comments]': {
	        	 required: true,
	             maxlength: 200
	         }
	     },
	     messages: {
	         'experience[employer]': {
	             required: lang_companyRequired,
	             maxlength: lang_companyMaxLength
	         },
	         'experience[jobtitle]': {
	             required: lang_jobTitleRequired,
	             maxlength: lang_jobTitleMaxLength
	         },    
	         'experience[comments]': {
	        	 required: lang_companyRequired,
	             maxlength: lang_commentLength
	         }
	     }
	 });
	 var fromDate = "";	
	 $("#addWorkExperience").click(function() {

	        removeEditLinks();

	        //changing the headings
	        $("#headChangeWork").text(lang_addWork);
	        $(".chkbox1").hide();
	        $("#workCheckAll").hide();        

	        //hiding action button section
	        $("#actionWorkExperience").hide();

	        $('div#changeWorkExperience label.error').hide();

	        $("#experience_id").val("");
	        $("#experience_employer").val("");
	        $("#experience_jobtitle").val("");
	       // $("#experience_from_date").val(displayDateFormat);
	        //$("#experience_to_date").val(displayDateFormat);
	        $("#experience_comments").val("");

	        //show add work experience form
	        $("#changeWorkExperience").show();
	        $("#workExpRequiredNote").show();
	    });
	 
	 $("#btnWorkExpSave").click(function(){
	     fromDate = $('#experience_from_date').val();
		 $("#saveWork").val("true"); 
		 /*document.formStatus.valider.value = "true"; */
		 $("#frmWorkSyn").submit();
		 
	 });
	 
	//clicking of delete button
	    $("#delWorkExperience").click(function(){

	        clearMessageBar();
	        
	        if ($(".chkbox1:checked").length > 0) {
	        	 $("#deleteWorkExperience").val("true"); 
	            $("#frmDelWorkExperience").submit();
	        } else {
	            //$("#workExpMessagebar").attr('class', 'erreur').text(lang_selectWrkExprToDelete);
	            msg.open(lang_selectWrkExprToDelete);
	        }

	    });

	    $("#btnWorkExpCancel").click(function() {
	        clearMessageBar();
	        if(canEdit){
	            addEditLinks();
	        }
	        
	        workExperienceValidator.resetForm();
	        
	        $('div#changeWork label.error').hide();

	        $(".chkbox1").removeAttr("checked");

	        //hiding action button section
	        $("#actionWorkExperience").show();

	        $("#changeWork").hide();
	        $("#workExpRequiredNote").hide();

	        $(".chkbox1").show();
	        $("#workCheckAll").show();
	    });
	    
	    $('form#frmDelWorkExperience').on('click', 'table a.edit', function(event) {
	        event.preventDefault();

	        var id = $(this).closest("tr").find('input.chkbox1:first').val();
	        clearMessageBar();

	        //changing the headings
	        $("#headChangeWork").text(lang_editWork);
	        workExperienceValidator.resetForm();
	        $('div#changeWork label.error').hide();

	        //hiding action button section
	        $("#actionWorkExperience").hide();

	        //show add work experience form
	        $("#changeWork").show();

	        $("#experience_id").val(id);
	        $("#experience_employer").val($("#employer_" + id).val());
	        $("#experience_jobtitle").val($("#jobtitle_" + id).val());
	        $("#experience_from_date").val($("#fromDate_" + id).val());
	        $("#experience_to_date").val($("#toDate_" + id).val());
	        $("#experience_comments").val($("#comment_" + id).val());

	        if ($("#experience_from_date").val() == '') {
	            $("#experience_from_date").val(displayDateFormat);
	        }
	        if ($("#experience_to_date").val() == '') {
	            $("#experience_to_date").val(displayDateFormat);
	        }

	        $("#workExpRequiredNote").show();

	        $(".chkbox1").hide();
	        $("#workCheckAll").hide();
	    });
	 

});

/*
 * END Work Experience Section
 */

