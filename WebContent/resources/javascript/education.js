var startDate = "";
$(document).ready(function() {

	//$('#education_code').after('<span id="static_education_code" style="display:none;"></span>');
	/*if ($("#education_start_date").val() != null) {
		alert($("#education_start_date").val());//.substr(3,4));
		$("#education_year").val($("#education_start_date").val().substr(3,4));
		
	}*/
//	hide add section
	$("#changeEducation").hide();
	$("#educationRequiredNote").hide();

//	hiding the data table if records are not available
	if($("div#tblEducation .chkbox").length == 0) {
		$('div#tblEducation .check').hide();
		$("#editEducation").hide();
		$("#delEducation").hide();
	}

//	if check all button clicked
	$("#educationCheckAll").click(function() {
		$("div#tblEducation .chkbox").removeAttr("checked");
		if($("#educationCheckAll").attr("checked")) {
			$("div#tblEducation .chkbox").attr("checked", "checked");
		}
	});

//	remove tick from the all button if any checkbox unchecked
	$("div#tblEducation .chkbox").click(function() {
		$("#educationCheckAll").removeAttr('checked');
		if($("div#tblEducation .chkbox").length == $("div#tblEducation .chkbox:checked").length) {
			$("#educationCheckAll").attr('checked', 'checked');
		}
	});

	$("#addEducation").click(function() {
		removeEditLinks();
		clearMessageBar();
		$('div#changeEducation label.error').hide();

//		changing the headings
		$("#headChangeEducation").text(lang_addEducation);
		$("div#tblEducation .chkbox").hide();
		$("#educationCheckAll").hide();

//		hiding action button section
		$("#actionEducation").hide();
		$("#education_id").val("");
		$("#education_code").val("");
		$("#education_institute").val("");
		$("#education_major").val("");
		$("#education_year").val("");
		$("#education_gpa").val("");
		$("#education_start_date").val(displayDateFormat);
		$("#education_end_date").val(displayDateFormat);

//		show add form
		$("#changeEducation").show();
		$("#educationRequiredNote").show();
	});

//	clicking of delete button
	$("#delEducation").click(function(){
		clearMessageBar();
		if ($("div#tblEducation .chkbox:checked").length > 0) {
			$("#deleteEducation").val("true"); 
			$("#frmDelEducation").submit();
		} else {
			//$("#educationMessagebar").attr('class', 'messageBalloon_notice').text(lang_selectEducationToDelete);
			msg.open(lang_selectEducationToDelete);
		}
	});

	$("#btnEducationSave").click(function() {
		clearMessageBar();
		startDate = $('#education_start_date').val();
		$("#saveEducation").val("true"); 
		if ($("#education_start_date").val() == displayDateFormat) {
			$("#education_start_date").val(' ');
		}
		if ($("#education_end_date").val() == displayDateFormat) {
			$("#education_end_date").val(' ');
		}
		$("#frmEducation").submit();
	});

//	form validation
	var educationValidator =
		$("#frmEducation").validate({
			rules: {
				'education[code]': {required: true},
				'education[institute]': {required: true, maxlength: 100},
				'education[major]': {required: false, maxlength: 100},
				'education[year]': {required: false, digits: true},
				'education[gpa]': {required: false, maxlength: 25},
				//'education[start_date]': {valid_date: function(){return {format: datepickerDateFormat, required:false, displayFormat:displayDateFormat}}},
				//'education[end_date]': {valid_date: function(){return {format:datepickerDateFormat, required:false, displayFormat:displayDateFormat}}, date_range: function() {return {format:datepickerDateFormat, displayFormat:displayDateFormat, fromDate:startDate}}}
			},
			messages: {
				'education[code]': {required: lang_educationRequired},
				'education[institute]': {required: lang_educationRequired, maxlength: lang_instituteMaxLength},
				'education[major]': {maxlength: lang_majorMaxLength},
				'education[year]': {digits: lang_yearShouldBeNumber},
				'education[gpa]': {maxlength: lang_gpaMaxLength},
				//'education[start_date]': {valid_date: lang_invalidDate},
				//'education[end_date]': {valid_date: lang_invalidDate, date_range:lang_EndDateBeforeSatrtDate }
			}
		});

	function addEditLinks() {
		// called here to avoid double adding links - When in edit mode and cancel is pressed.
		removeEditLinks();
		$('div#tblEducation table tbody td.program').wrapInner('<a class="edit" href="#"/>');
	}

	function removeEditLinks() {
		$('div#tblEducation table tbody td.program a').each(function(index) {
			$(this).parent().text($(this).text());
		});
	}

	$("#btnEducationCancel").click(function() {
		clearMessageBar();
		addEditLinks();
		educationValidator.resetForm();
		$('div#changeEducation label.error').hide();
		$("div#tblEducation .chkbox").removeAttr("checked").show();

//		hiding action button section
		$("#actionEducation").show();
		$("#changeEducation").hide();
		$("#educationRequiredNote").hide();
		$("#educationCheckAll").show();


	});

	$('form#frmDelEducation').on('click', 'a.edit', function(event) {
		event.preventDefault();
		clearMessageBar();

//		changing the headings
		$("#headChangeEducation").text(lang_editEducation);
		educationValidator.resetForm();
		$('div#changeEducation label.error').hide();

//		hiding action button section
		$("#actionEducation").hide();

//		show add form
		$("#changeEducation").show();
		var code = $(this).closest("tr").find('input.chkbox:first').val();
		
		$("#education_id").val(code);
		$('#education_code').val($("#code_" + code).val());
		$("#education_institute").val($("#institute_" + code).val());
		$("#education_major").val($("#major_" + code).val());
		$("#education_year").val($("#year_" + code).val());
		$("#education_gpa").val($("#gpa_" + code).val());
		$("#education_start_date").val($("#start_date_" + code).val());
		$("#education_end_date").val($("#end_date_" + code).val());
		if ($("#education_start_date").val() == '') {
			$("#education_start_date").val(displayDateFormat);
		}
		if ($("#education_end_date").val() == '') {
			$("#education_end_date").val(displayDateFormat);
		}
		$("#educationRequiredNote").show();
		$("div#tblEducation .chkbox").hide();
		$("#educationCheckAll").hide();
	});
});

