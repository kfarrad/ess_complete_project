/*Au passage de la souris sur certains termes, affiche une infobulle
 * version : 1.0
 * Evolution: améliorer le stype de l'affichage
 */
this.popup = function(){
	//deport horizontal et vertical
	xOffset = 10;
	yOffset = 20;
	$("a.infobulle").hover(function(e){
		this.texte = this.title;
		this.title = "";

		$("body").append("<p id='infobulle'>" + this.texte + "</p>");
		$("#infobulle")
		.css("top", (e.pageY - xOffset) + "px")
		.css("left", (e.pageX + yOffset) + "px")
		.fadeIn("fast");
	},
	function(){
		this.title =this.texte;
		$("#infobulle").remove();
	});
	$("a.infobulle").mousemove(function(e){
		$("#infobulle")
		.css("top", (e.pageY - xOffset) + "px")
		.css("left", (e.pageX + yOffset) + "px");
	});
};
/*$(document).ready(function(){
	popup();
});
*/