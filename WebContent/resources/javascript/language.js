
$(document).ready(function() {
     
    function addEditLinks() {
        // called here to avoid double adding links - When in edit mode and cancel is pressed.
        removeEditLinks();
        $('form#frmDelLanguage table tbody td.name').wrapInner('<a class="edit" href="#"/>');
    }

    function removeEditLinks() {
        $('form#frmDelLanguage table tbody td.name a').each(function(index) {
            $(this).parent().text($(this).text());
        });
    }
    
    //hide add section
    $("#changeLanguage").hide();
    $("#languageRequiredNote").hide();

    //hiding the data table if records are not available
    if($("div#tblLanguage .chkbox").length == 0) {
        //$("#tblLanguage").hide();
        $('div#tblLanguage .check').hide();
        $("#editLanguage").hide();
        $("#delLanguage").hide();
    }

    //if check all button clicked
    $("#languageCheckAll").click(function() {
        $("div#tblLanguage .chkbox").removeAttr("checked");
        if($("#languageCheckAll").attr("checked")) {
            $("div#tblLanguage .chkbox").attr("checked", "checked");
        }
    });

    //remove tick from the all button if any checkbox unchecked
    $("div#tblLanguage .chkbox").click(function() {
        $("#languageCheckAll").removeAttr('checked');
        if($("div#tblLanguage .chkbox").length == $("div#tblLanguage .chkbox:checked").length) {
            $("#languageCheckAll").attr('checked', 'checked');
        }
    });
    
    //hide already added languages and fluencys
    /*$("#language_code").change(function() {
        //show all the options to reseting hide options
        $("#language_lang_type option").each(function() {
            $(this).show();
        });
        $("#language_lang_type").val("");
        var $table_tr = $("#lang_data_table tr");
        var i=0;
        //hide already added optons for selected language
        $table_tr.each(function() {
            i++;
            if (i != 1) {           // skip heading tr
                if ($('#language_code').val() == $(this).find('td:eq(0)').find('input[class="code"]').val()){
                    $td = $(this).find('td:eq(0)').find('input[class="lang_type"]').val();
                    $("#language_lang_type option[value=" + $td + "]").hide();
                }
            }
        });        
    });*/
    
    $("#addLanguage").click(function() {
        removeEditLinks();
        clearMessageBar();
        $('div#changeLanguage label.error').hide();        
        

        //changing the headings
        $("#headChangeLanguage").text(lang_addLanguage);
        $("div#tblLanguage .chkbox").hide();
        $("#languageCheckAll").hide();

        //hiding action button section
        $("#actionLanguage").hide();

        $("#language_name").show().val("");
        $("#language_fluency").show().val("");

        //show add form
        $("#changeLanguage").show();
        $("#languageRequiredNote").show();
        
        //show all the options to reseting hide options
       /* $("#language_lang_type option").each(function() {
            $(this).show();
        });*/
    });

    //clicking of delete button
    $("#delLanguage").click(function(){

        clearMessageBar();

        if ($("div#tblLanguage .chkbox:checked").length > 0) {
        	$("#deleteLanguage").val("true");
            $("#frmDelLanguage").submit();
        } else {
        	msg.open(lang_selectLanguageToDelete);
        	//$("#languageMessagebar").attr('class', 'messageBalloon_notice').text(lang_selectLanguageToDelete);
        }

    });

    $("#btnLanguageSave").click(function() {
        clearMessageBar();
        $("#saveLanguage").val("true"); 
        $("#frmLanguage").submit();
    });

    //form validation
    var languageValidator =
        $("#frmLanguage").validate({
        rules: {
            'language[code]': {required: true},
            'language[fluency]': {required: true},
            /*'language[competency]': {required: true},
            'language[comments]' : {required: false, maxlength:100}*/
        },
        messages: {
            'language[code]': {required: lang_languageRequired},
            'language[fluency]': {required: lang_languageTypeRequired},
          /*  'language[competency]': {required: lang_competencyRequired},
            'language[comments]' : {maxlength: lang_commentsMaxLength}*/
        }
    });

    $("#btnLanguageCancel").click(function() {
        clearMessageBar();
                    addEditLinks();
                languageValidator.resetForm();
        
        $('div#changeLanguage label.error').hide();

        $("div#tblLanguage .chkbox").removeAttr("checked").show();
        $("#language_id").removeAttr("value");
        //hiding action button section
        $("#actionLanguage").show();
        $("#changeLanguage").hide();
        $("#languageRequiredNote").hide();        
        $("#languageCheckAll").show();
    });
    
    $('form#frmDelLanguage').on('click','a.edit', function(event) {
        event.preventDefault();
        clearMessageBar();

        //changing the headings
        $("#headChangeLanguage").text(lang_editLanguage);

        languageValidator.resetForm();

        $('div#changeLanguage label.error').hide();

        //hiding action button section
        $("#actionLanguage").hide();

        //show add form
        $("#changeLanguage").show();
                                
        var code = $(this).closest("tr").find('input.chkbox:first').val();
        
        $("#language_id").val(code);
        $('#language_name').val($("#language_name_" + code).val());
        $("#language_fluency").val($("#language_fluency_" + code).val());
        

        $("#languageRequiredNote").show();

        $("div#tblLanguage .chkbox").hide();
        $("#languageCheckAll").hide();        
    });
});