package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.FormationsDemandes;

public class FormationsDemandesDAOImp implements FormationsDemandesDAO {

	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO formations_demandes ( " +
			"formations_demandes_libelle, " +
			"formations_demandes_objectifs, " +
			"auto_evaluations_auto_evaluations_id) VALUES (?, ?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE formations_demandes SET  formations_demandes_libelle = ?, formations_suivies_objectifs = ? WHERE formations_demandes_id = ?";
	/*
	 * Constante SQL_DELETE
	 */
	private static final String SQL_DELETE = "DELETE FROM formations_demandes WHERE formations_demandes_id = ?";
	/*
	 * Constante SQL_SELECT_ALL
	 */
	private static final String SQL_SELECT_ALL = "SELECT * FROM formations_demandes WHERE auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM formations_demandes WHERE formations_demandes_id = ?";
    
	private DAOFactory daoFactory;
	
	public FormationsDemandesDAOImp(DAOFactory daoFactory) {
		// TODO Auto-generated constructor stub
			this.daoFactory = daoFactory;
	}
	@Override
	public void creer(FormationsDemandes formationsDemandes)
			throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, formationsDemandes.getFormationsDemandesLibelle(), formationsDemandes.getFormationsDemandesObjectifs(), formationsDemandes.getAutoEvaluations().getAutoEvaluationsId());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                formationsDemandes.setFormationsDemandesId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }

	}

	@Override
	public void modifier(FormationsDemandes formationsDemandes)
			throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		formationsDemandes.getFormationsDemandesLibelle(), formationsDemandes.getFormationsDemandesObjectifs(), formationsDemandes.getFormationsDemandesId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }

	}

	@Override
	public List<FormationsDemandes> lister(Long idAutoEvaluation)
			throws DAOException {
		// TODO Auto-generated method stub
		List<FormationsDemandes> formationsDemandes = new ArrayList<FormationsDemandes>();
		try {
            formationsDemandes = retrouve(SQL_SELECT_ALL, idAutoEvaluation);
           
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return formationsDemandes;
	}

	@Override
	public FormationsDemandes trouver(Long idFormationsDemandes)
			throws DAOException {
		// TODO Auto-generated method stub
		FormationsDemandes formationsDemandes = null;  
        try {
        	formationsDemandes = trouve(SQL_SELECT_BY_ID, idFormationsDemandes);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return formationsDemandes;
	}

	@Override
	public void supprimer(FormationsDemandes formationsDemandes)
			throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, formationsDemandes.getFormationsDemandesId());
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la suppression de la formation , aucune ligne supprimée de la table." );
            } else {
            	formationsDemandes = null;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }

	}
	
	/**
	 * Map fabrique un objet de type SystemesActivites
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private FormationsDemandes map( ResultSet resultSet ) throws SQLException {
		
		//AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		FormationsDemandes formationsDemandes = new FormationsDemandes();
		formationsDemandes.setFormationsDemandesId(resultSet.getLong( "formations_demandes_id" ) );	    	       
		formationsDemandes.setFormationsDemandesLibelle(resultSet.getString("formations_demandes_libelle"));
		formationsDemandes.setFormationsDemandesObjectifs(resultSet.getString("formations_demandes_objectifs"));
		//formationsDemandes.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));
       
        return formationsDemandes;
    }
	
	private List<FormationsDemandes> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<FormationsDemandes> formationsDemandes = new ArrayList<FormationsDemandes>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	formationsDemandes.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return formationsDemandes;
	}
	private FormationsDemandes trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        FormationsDemandes formationsDemandes = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	formationsDemandes = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return formationsDemandes;
	}


}
