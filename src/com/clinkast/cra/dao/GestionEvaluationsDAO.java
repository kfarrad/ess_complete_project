package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;

public interface GestionEvaluationsDAO {
	/**
	 * 
	 * @param gestionEvaluations
	 * @throws DAOException
	 */
	public void creer(GestionEvaluations gestionEvaluations) throws DAOException;
	/**
	 * 
	 * @param gestionEvaluations
	 * @param idUser
	 * @param idAutoEvaluation
	 * @param idDate
	 * @return
	 * @throws DAOException
	 */
	public void modifier(GestionEvaluations gestionEvaluations) throws DAOException;
	/**
	 * 
	 * @param idUser
	 * @param idAutoEvaluation
	 * @param idDate
	 * @return
	 * @throws DAOException
	 */
	public GestionEvaluations trouver_By_KEY(Long idAutoEvaluation,	Long idDate, Long idUser) throws DAOException;
	/**
	 * 
	 * @param idGestionAutoEvaluation
	 * @return
	 * @throws DAOException
	 */
	public GestionEvaluations trouver_By_ID(Long idGestionAutoEvaluation) throws DAOException; 
	/**
	 * 
	 * @param idDates
	 * @return
	 * @throws DAOException
	 */
	public List<GestionEvaluations> lister_By_Dates(Long idDates) throws DAOException;
	/**
	 * 
	 * @param idUser
	 * @return
	 * @throws DAOException
	 */
	public List<GestionEvaluations> lister_By_User(Utilisateur utilisateur) throws DAOException;
	public List<GestionEvaluations> lister_By_ALL() throws DAOException;
	public int nombreRenvoi(Long idDate) throws DAOException;
	public int nombreRecu(Long idDate) throws DAOException;
	public int nombreRejet(Long idDate) throws DAOException;
}
