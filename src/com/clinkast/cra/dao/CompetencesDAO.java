package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Competences;

public interface CompetencesDAO {
	/**
	 * 
	 * @param competences
	 * @throws DAOException
	 */
	public void creer(Competences competences) throws DAOException;
	/**
	 * 
	 * @param competences
	 * @param idCompetences
	 * @return
	 * @throws DAOException
	 */
	public void modifier(Competences competences) throws DAOException;
	/**
	 * 
	 * @return
	 * @throws DAOException
	 */
	public List<Competences> lister()throws DAOException;
	/**
	 * 
	 * @param categorie
	 * @return
	 * @throws DAOException
	 */
	public List<Competences> lister(String categorie)throws DAOException;
	
	/**
	 * 
	 * @param idCompetences
	 * @return
	 * @throws DAOException
	 */
	public Competences trouver(Long idCompetences) throws DAOException;
	/**
	 * 
	 * @param competences
	 * @throws DAOException
	 */
	public void supprimer(Competences competences) throws DAOException;
	

}
