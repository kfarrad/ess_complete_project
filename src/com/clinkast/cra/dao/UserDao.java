package com.clinkast.cra.dao;



import java.util.List;

import com.clinkast.cra.beans.Utilisateur;



// TODO: Auto-generated Javadoc
/**
 * The Interface UserDao.
 */
public interface UserDao {
	 
 	/**
 	 * Creer.
 	 *
 	 * @param client the client
 	 * @param id_profil the id_profil
 	 * @throws DAOException the DAO exception
 	 */
 	void creer( Utilisateur client, Long id_profil ) throws DAOException;
 	
 	/**
 	 * Trouver_ID: rechercher un utilisateur à partir de son id
 	 * @param user_id
 	 * @return
 	 * @throws DAOException
 	 */
 	public Utilisateur trouver_ID(Long user_id ) throws DAOException;
	 /**
 	 * Trouver.
 	 *
 	 * @param id_profil the id_profil
 	 * @return the utilisateur
 	 * @throws DAOException the DAO exception
 	 */
 	Utilisateur trouver( Long id_profil ) throws DAOException; 
	 
	 /**
 	 * Trouver.
 	 *
 	 * @param login the login
 	 * @return the utilisateur
 	 * @throws DAOException the DAO exception
 	 */
 	Utilisateur trouver(String login ) throws DAOException; 
	 
	 /**
 	 * Trouver password.
 	 *
 	 * @param mdp the mdp
 	 * @param id the id
 	 * @return true, if successful
 	 * @throws DAOException the DAO exception
 	 */
 	boolean trouverPassword(String mdp, Long id ) throws DAOException;
	    
	 /**
 	 * Est utilisateur.
 	 *
 	 * @param email the email
 	 * @param mdp the mdp
 	 * @return the utilisateur
 	 */
 	Utilisateur estUtilisateur(String email, String mdp);
	 
	 /**
 	 * Lister.
 	 *
 	 * @return the list
 	 * @throws DAOException the DAO exception
 	 */
 	List<Utilisateur> lister() throws DAOException;
	    

	 /**
 	 * Supprimer.
 	 *
 	 * @param client the client
 	 * @throws DAOException the DAO exception
 	 */
 	void supprimer( Utilisateur client ) throws DAOException;
}
