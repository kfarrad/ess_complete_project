package com.clinkast.cra.dao;

import java.util.Set;

import com.clinkast.cra.beans.AutoEvaluations;

public interface AutoEvaluationDAO {
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	public AutoEvaluations trouver(Long id) throws DAOException;
	/**
	 * 
	 * @param autoEvaluations
	 * @throws DAOException
	 */
	public AutoEvaluations creer(AutoEvaluations autoEvaluations) throws DAOException;
	/**
	 * 
	 * @param autoEvaluations
	 * @return
	 * @throws DAOException
	 */
	public AutoEvaluations modifier(AutoEvaluations autoEvaluations) throws DAOException;
	/**
	 * 
	 * @param idAutoEvaluation
	 * @return
	 * @throws DAOException
	 */
	public Set<AutoEvaluations> lister(int idAutoEvaluation) throws DAOException;
}
