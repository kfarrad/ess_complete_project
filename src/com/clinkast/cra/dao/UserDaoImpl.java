package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Role;
import com.clinkast.cra.beans.Utilisateur;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDaoImpl.
 */
public class UserDaoImpl implements UserDao{

	/** The Constant SQL_SELECT. */
	private static final String SQL_SELECT				= "SELECT *  FROM user order by login ASC";
	
	private static final String SQL_SELECT_ACTIF			= "SELECT *  FROM user where actif = 1";
	
	/** The Constant SQL_SELECT_USER */
	private static final String SQL_SELECT_ALL_USER        	= "SELECT * FROM user WHERE role = ?";
	
	/** Constante pour selectionner un utilisateur par son id. */
	private static final String SQL_SELECT_BY_ID_USER		= "SELECT *  FROM user WHERE id = ?";
	
	/** The Constant SQL_SELECT_PAR_ID. */
	private static final String SQL_SELECT_PAR_ID		= "SELECT *  FROM user WHERE id_profil = ?";
	
	/** The Constant SQL_SELECT_PAR_LOGIN. */
	private static final String SQL_SELECT_PAR_LOGIN	= "SELECT *  FROM user WHERE login = ?";
	
	/** The Constant SQL_INSERT. */
	private static final String SQL_INSERT        		= "INSERT INTO user (id_profil, login, password, role) VALUES (?, ?, ?, ?)";
	
	/** The Constant SQL_OFF_PAR_ID. */
	private static final String SQL_OFF_PAR_ID 			= "UPDATE user SET  actif=? WHERE id_profil = ?";
	
	/** The Constant SQL_UPDATE_PAR_ID. */
	private static final String SQL_UPDATE_PAR_ID 		= "UPDATE user SET  password=? WHERE id_profil = ?";
	
	/** The Constant SQL_UPDATE_PAR_ID_2. */
	private static final String SQL_UPDATE_PAR_ID_2 	= "UPDATE user SET  role=? WHERE id_profil = ?";
	
	/** The Constant SQL_SELECT_USER. */
	private static final String SQL_SELECT_USER			= "SELECT * FROM user_projets up, user u WHERE up.id_projet = ? and up.id_user=u.id and up.actif=1";

	/** The dao factory. */
	private  DAOFactory          daoFactory;

	/**
	 * Instantiates a new user dao impl.
	 *
	 * @param daoFactory the dao factory
	 */
	UserDaoImpl( DAOFactory daoFactory ) {
		this.daoFactory = daoFactory;
	}

	/* Implémentation de la méthode définie dans l'interface ClientDao */

	public Utilisateur trouver_ID(Long user_id )
	{
		return trouver( SQL_SELECT_BY_ID_USER,  user_id  );
	}

	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#trouver(java.lang.Long)
	 */
	@Override
	public Utilisateur trouver(Long id_profil )
	{
		return trouver( SQL_SELECT_PAR_ID,  id_profil  );
	}

	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#trouver(java.lang.String)
	 */
	@Override
	public Utilisateur trouver(String login )
	{
		return trouver(SQL_SELECT_PAR_LOGIN, login);
	}

	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#trouverPassword(java.lang.String, java.lang.Long)
	 */
	@Override
	public boolean trouverPassword(String mdp, Long id ) throws DAOException
	{
		boolean trouvePassword = false;
		Utilisateur utilisateur = trouver(id);
		if(utilisateur!=null){
			String motDePasse = md5Java(utilisateur.getLogin() + "zo5proù^à!!;,*cz4a8ùtvb#ui4oeuio" + mdp);
			trouvePassword = motDePasse.equals(utilisateur.getMotDePasse());

		}



		return trouvePassword;
	}
	/* Implémentation de la méthode définie dans l'interface ClientDao */



	/* Implémentation de la méthode définie dans l'interface ClientDao */
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#estUtilisateur(java.lang.String, java.lang.String)
	 */
	@Override
	public Utilisateur estUtilisateur(String login, String mdp) throws DAOException
	{
		boolean connected= false;


		Utilisateur utilisateur = trouver( login );

		if(utilisateur!=null)
		{
			String motDePasse = md5Java(login + "zo5proù^à!!;,*cz4a8ùtvb#ui4oeuio" + mdp);
			connected = motDePasse.equals(utilisateur.getMotDePasse());
			if(!connected) throw new DAOException( "nom d'utilisateur et/ou mot de passe invalide" );
			else return utilisateur;

		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#creer(com.clinkast.cra.beans.Utilisateur, java.lang.Long)
	 */
	@Override
	public void creer( Utilisateur utilisateur, Long id_profil ) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		String motDePasse = md5Java(utilisateur.getLogin() + "zo5proù^à!!;,*cz4a8ùtvb#ui4oeuio" + utilisateur.getMotDePasse());
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
					id_profil ,utilisateur.getLogin(),motDePasse, utilisateur.getRole().toString()  );
			int statut = preparedStatement.executeUpdate();
			
			if ( statut == 0 ) {
				throw new DAOException( "Échec de la création de l'utilisateur, aucune ligne ajoutée dans la table." );
			}
			valeursAutoGenerees = preparedStatement.getGeneratedKeys();

			if ( valeursAutoGenerees.next() ) {            	
				utilisateur.setId( valeursAutoGenerees.getLong( 1 ) );

			} else {
				throw new DAOException( "Échec de la création de l'utilisateur en base, aucun ID auto-généré retourné." );
			}
			/*}

			 * else

	            {
	            	throw new DAOException( "Échec de la création du client en base, login déjà existant." );
	            }
			 */
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
		}

	}

	/**
	 * Modifier role.
	 *
	 * @param utilisateur the utilisateur
	 * @param id the id
	 * @return the utilisateur
	 * @throws DAOException the DAO exception
	 */
	public Utilisateur modifierRole(Utilisateur utilisateur, Long id ) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PAR_ID_2, true,
					utilisateur.getRole().toString(), id );

			int statut = preparedStatement.executeUpdate();

			if ( statut == 0 ) {
				throw new DAOException( "Échec de la modification  du role , aucune ligne modifiée dans la table." );
			}

		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}

		return utilisateur;
	}



	/**
	 * Modifier mot de passe.
	 *
	 * @param utilisateur the utilisateur
	 * @param id the id
	 * @return the utilisateur
	 * @throws DAOException the DAO exception
	 */
	public Utilisateur modifierMotDePasse(Utilisateur utilisateur, Long id ) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			String motDePasse = md5Java(utilisateur.getLogin() + "zo5proù^à!!;,*cz4a8ùtvb#ui4oeuio" + utilisateur.getMotDePasse());

			preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PAR_ID, true,
					motDePasse, id );
			int statut = preparedStatement.executeUpdate();

			if ( statut == 0 ) {
				throw new DAOException( "Échec de la modification  du mot de passe , aucune ligne modifiée dans la table." );
			}

		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}

		return utilisateur;
	}

	/* Implémentation de la méthode définie dans l'interface ClientDao */
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#lister()
	 */
	@Override
	public List<Utilisateur> lister() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement( SQL_SELECT );
			resultSet = preparedStatement.executeQuery();
			int k=0;
			while ( resultSet.next() ) {
				utilisateurs.add(k++, map( resultSet ) );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connection );
		}

		return utilisateurs;
	}
	
	public List<Utilisateur> listerActif() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement( SQL_SELECT_ACTIF );
			resultSet = preparedStatement.executeQuery();
			int k=0;
			while ( resultSet.next() ) {
				utilisateurs.add(k++, map( resultSet ) );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connection );
		}

		return utilisateurs;
	}
	public List<Utilisateur> lister_USER() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

		try {
			connection = daoFactory.getConnection();
			//preparedStatement = connection.prepareStatement( SQL_SELECT );
			preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT_ALL_USER, false, "USER" );
			resultSet = preparedStatement.executeQuery();
			int k=0;
			while ( resultSet.next() ) {
				utilisateurs.add(k++, map( resultSet ) );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connection );
		}

		return utilisateurs;
	}



	/* Implémentation de la méthode définie dans l'interface ClientDao */
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.UserDao#supprimer(com.clinkast.cra.beans.Utilisateur)
	 */
	@Override
	public void supprimer( Utilisateur utilisateur ) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;

		try {
			connexion = daoFactory.getConnection();
			Utilisateur monUtilisateur = this.trouver(utilisateur.getProfil().getId());
			if(monUtilisateur!=null)
				preparedStatement = initialisationRequetePreparee( connexion, SQL_OFF_PAR_ID, true, !utilisateur.isActif(),utilisateur.getProfil().getId());
			int statut = preparedStatement.executeUpdate();
			if ( statut == 0 ) {
				throw new DAOException( "Échec de la suppression du client, aucune ligne supprimée de la table." );
			} else {
				utilisateur.setActif(!utilisateur.isActif());
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}
	}

	/*
	 * Méthode générique utilisée pour retourner un client depuis la base de
	 * données, correspondant à la requête SQL donnée prenant en paramètres les
	 * objets passés en argument.
	 */
	/**
	 * Trouver.
	 *
	 * @param sql the sql
	 * @param objets the objets
	 * @return the utilisateur
	 * @throws DAOException the DAO exception
	 */
	private Utilisateur trouver( String sql, Object... objets ) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Utilisateur client = null;

		try {
			/* Récupération d'une connexion depuis la Factory */
			connexion = daoFactory.getConnection();
			/*
			 * Préparation de la requête avec les objets passés en arguments
			 * (ici, uniquement un id) et exécution.
			 */
			preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
			resultSet = preparedStatement.executeQuery();
			/* Parcours de la ligne de données retournée dans le ResultSet */
			if ( resultSet.next() ) {
				client = map( resultSet );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connexion );
		}

		return client;
	}


	/**
	 * Lister projets users.
	 *
	 * @param projet the projet
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	public List<String> listerProjetsUsers(Projet projet) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<String> utilisateurs = new ArrayList<String>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_USER, false, projet.getId());
			resultSet = preparedStatement.executeQuery();
			int i=0;
			while ( resultSet.next() ) {		            	
				utilisateurs.add(i, resultSet.getString( "login" ) );
				i++;
			}

		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connexion );
		}

		return utilisateurs;
	}
	/*
	 * Simple méthode utilitaire permettant de faire la correspondance (le
	 * mapping) entre une ligne issue de la table des clients (un ResultSet) et
	 * un bean Client.
	 */
	/**
	 * Map.
	 *
	 * @param resultSet the result set
	 * @return the utilisateur
	 * @throws SQLException the SQL exception
	 */
	private Utilisateur map( ResultSet resultSet ) throws SQLException {
	        
	   UserProfilDaoImpl profilDao = daoFactory.getUserProfilDao();
	   
	    
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId( resultSet.getLong( "id" ) );	    
		utilisateur.setProfil(profilDao.trouver(resultSet.getLong("id_profil")));
		utilisateur.setLogin( resultSet.getString( "login" ) );
		utilisateur.setRole(Role.valueOf(resultSet.getString( "role" )));
		utilisateur.setMotDePasse(resultSet.getString("password"));
		utilisateur.setActif(resultSet.getBoolean("actif"));
		return utilisateur;
	}

	/**
	 * Md5 java.
	 *
	 * @param message the message
	 * @return the string
	 */
	public static String md5Java(String message)
	{ 
		String digest = null;
		try 
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(message.getBytes("UTF-8"));
			//converting byte array to Hexadecimal String 

			StringBuilder sb = new StringBuilder(2*hash.length); 
			for(byte b : hash)
			{
				sb.append(String.format("%02x", b&0xff)); 
			}
			digest = sb.toString();
		}
		catch (UnsupportedEncodingException ex)
		{ 
			ex.printStackTrace();
		} 
		catch (NoSuchAlgorithmException ex)
		{
			ex.printStackTrace();
		}
		return digest;  
	}



}
