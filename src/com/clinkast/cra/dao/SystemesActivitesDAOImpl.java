package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.SystemesActivites;

public class SystemesActivitesDAOImpl implements SystemesActivitesDAO {
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO systemes_activites ( " +
			"systemes_activites_periode_de, " +
			"systemes_activites_periode_au, " +
			"systemes_activites_missions, " +
			"systemes_activites_responsable, " +
			"systemes_activites_entretien, " +
			"auto_evaluations_auto_evaluations_id) VALUES (?, ?, ?, ?, ?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE systemes_activites SET  systemes_activites_periode_de=?, systemes_activites_periode_au=?, systemes_activites_missions=?, systemes_activites_responsable=?, systemes_activites_entretien=? WHERE systemes_activites_id = ?";
	/*
	 * Constante SQL_DELETE
	 */
	private static final String SQL_DELETE = "DELETE FROM systemes_activites WHERE systemes_activites_id = ?";
	/*
	 * Constante SQL_SELECT_ALL
	 */
	private static final String SQL_SELECT_ALL = "SELECT * FROM systemes_activites WHERE auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM systemes_activites WHERE systemes_activites_id = ?";
    
	private DAOFactory daoFactory;
	
	public SystemesActivitesDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}

	@Override
	public void creer(SystemesActivites systemesActivites) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, systemesActivites.getSystemesActivitesPeriodeDe(), systemesActivites.getSystemesActivitesPeriodeAu(), systemesActivites.getSystemesActivitesMissions(), systemesActivites.getSystemesActivitesResponsable(), systemesActivites.getSystemesActivitesEntretien(), systemesActivites.getAutoEvaluations().getAutoEvaluationsId());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                systemesActivites.setSystemesActivitesId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }

	}

	@Override
	public SystemesActivites modifier(SystemesActivites systemesActivites) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
                     systemesActivites.getSystemesActivitesPeriodeDe(), systemesActivites.getSystemesActivitesPeriodeAu(), systemesActivites.getSystemesActivitesMissions(), systemesActivites.getSystemesActivitesResponsable(), systemesActivites.getSystemesActivitesEntretien(), systemesActivites.getSystemesActivitesId() );
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  du nom du Client, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
		return systemesActivites;
	}

	@Override
	public List<SystemesActivites> lister(Long idEvaluation) throws DAOException {
		// TODO Auto-generated method stub
		List<SystemesActivites> systemesActivites = new ArrayList<SystemesActivites>();
		try {
        	systemesActivites = retrouve(SQL_SELECT_ALL, idEvaluation);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return systemesActivites;
	}
	
	@Override
	public void supprimer(SystemesActivites systemesActivites) throws DAOException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, systemesActivites.getSystemesActivitesId());
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la suppression de la synthese d'activité <<" + systemesActivites.getSystemesActivitesMissions()+ ">> , aucune ligne supprimée de la table." );
            } else {
                systemesActivites = null;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}
	/**
	 * 
	 * @param idSystemesActivites
	 * @return
	 * @throws SQLException
	 */
	public SystemesActivites trouver(Long idSystemesActivites)throws DAOException{
		SystemesActivites systemesActivites = null;  
        try {
        	systemesActivites = trouve(SQL_SELECT_BY_ID, idSystemesActivites);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        return systemesActivites;
	}
	/**
	 * 
	 * @param sql
	 * @param objects
	 * @return un tableau de resultSet
	 * @throws SQLException
	 */
	
	/**
	 * Map fabrique un objet de type SystemesActivites
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private SystemesActivites map( ResultSet resultSet ) throws SQLException {
		
		//AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		SystemesActivites systemesActivites = new SystemesActivites();
		systemesActivites.setSystemesActivitesId(resultSet.getLong( "systemes_activites_id" ) );	    	       
		systemesActivites.setSystemesActivitesMissions(resultSet.getString("systemes_activites_missions"));
		systemesActivites.setSystemesActivitesPeriodeDe(resultSet.getString("systemes_activites_periode_de"));
		systemesActivites.setSystemesActivitesPeriodeAu(resultSet.getString("systemes_activites_periode_au"));
		systemesActivites.setSystemesActivitesResponsable(resultSet.getString("systemes_activites_responsable"));
		systemesActivites.setSystemesActivitesEntretien(resultSet.getBoolean("systemes_activites_entretien"));
		//systemesActivites.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));
       
        return systemesActivites;
    }
	
	private List<SystemesActivites> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<SystemesActivites> systemesActivites = new ArrayList<SystemesActivites>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	systemesActivites.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return systemesActivites;
	}
	private SystemesActivites trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        SystemesActivites systemesActivites = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	systemesActivites = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return systemesActivites;
	}

    

}
