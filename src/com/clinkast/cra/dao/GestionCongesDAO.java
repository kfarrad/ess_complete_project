package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.GestionConges;

public interface GestionCongesDAO {
	/*creer un nouveau objet*/
	public void creer(GestionConges gestionConges) throws DAOException;
	/*
	 * Modifier une ou plusieurs valeurs
	 */
	public void modifier(GestionConges gestionConges) throws DAOException;
	/*
	 * Lire un objet donc on connait son id
	 */
	public GestionConges trouver_by_user(Long id) throws DAOException;
	/*
	 * Liste tous les objets
	 */
	public List<GestionConges>lister() throws DAOException;

}
