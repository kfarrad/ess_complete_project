package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Dates;

public interface DatesDAO {
	/**
	 * 
	 * @param dates
	 * @throws DAOException
	 */
	public void creer(Dates dates) throws DAOException;
	/**
	 * 
	 * @param dates
	 * @param idDate
	 * @return
	 * @throws DAOException
	 */
	public void modifier(Dates dates)throws DAOException;
	/**
	 * 
	 * @return
	 * @throws DAOException
	 */
	public List<Dates> lister()throws DAOException;
	/**
	 * 
	 * @param idDate
	 * @return
	 * @throws DAOException
	 */
	public Dates trouver(Long idDate) throws DAOException;	
	

}
