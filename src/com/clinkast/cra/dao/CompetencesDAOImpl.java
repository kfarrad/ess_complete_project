package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Competences;

public class CompetencesDAOImpl implements CompetencesDAO {
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO competences ( competences_libelle, competences_categorie) VALUES (?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE competences SET  competences_libelle=?, competences_categorie=? WHERE competences_id = ?";
	/*
	 * Constante SQL_DELETE
	 */
	private static final String SQL_DELETE = "DELETE FROM competences WHERE competences_id = ?";
	/*
	 * Constante SQL_SELECT_ALL
	 */
	private static final String SQL_SELECT_ALL = "SELECT * FROM competences";
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM competences WHERE competences_id = ?";
	/*
	 * Constante SQL_SELECT_BY_CATEGORY
	 */
	private static final String SQL_SELECT_BY_CATEGORY = "SELECT * FROM competences WHERE competences_categorie = ?";
    
	private DAOFactory daoFactory;
	
	public CompetencesDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}

	@Override
	public void creer(Competences competences) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, competences.getCompetencesLibelle(), competences.getCompetencesCategorie());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une competences, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                competences.setCompetencesId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }


	}

	@Override
	public void modifier(Competences competences)
			throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		competences.getCompetencesLibelle(), competences.getCompetencesCategorie(), competences.getCompetencesId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}
	@Override
	public Competences trouver(Long idCompetences) throws DAOException{
		Competences competences = null;  
        try {
        	competences = trouve(SQL_SELECT_BY_ID, idCompetences);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return competences;
	}

	@Override
	public List<Competences> lister() throws DAOException {
		// TODO Auto-generated method stub
		List<Competences> competences = new ArrayList<Competences>();
		try {
        	//Si value est null, alors on selectionne tout
        	competences = retrouve(SQL_SELECT_ALL);
        	
         } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return competences;
	}
	
	
	@Override
	public void supprimer(Competences competences) throws DAOException{
		// TODO Auto-generated method stub
		/***********************************************************************
		 *   FONCTION A ADAPTER POUR LA SUPPRIMER EN CASCADE                   *
		 * ********************************************************************/
		
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
        try {
	            connexion = daoFactory.getConnection();
	            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, competences.getCompetencesId());
	            int statut = preparedStatement.executeUpdate();
	            if ( statut == 0 ) {
	                throw new DAOException( "Échec de la suppression de la formation , aucune ligne supprimée de la table." );
	            } else {
	            	competences = null;
	            }
	     } catch ( SQLException e ) {
	         throw new DAOException( e );
	     } finally {
	    	 fermeturesSilencieuses( preparedStatement, connexion );
	     }
	}

	/**
	 * Map fabrique un objet de type SystemesActivites
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private Competences map( ResultSet resultSet ) throws SQLException {
		
		Competences competences = new Competences();
		competences.setCompetencesId(resultSet.getLong( "competences_id" ) );	    	       
		competences.setCompetencesLibelle(resultSet.getString("competences_libelle"));
		competences.setCompetencesCategorie(resultSet.getString("competences_categorie"));
        
		return competences;
    }

	@Override
	public List<Competences> lister(String categorie) throws DAOException {
		// TODO Auto-generated method stub
		List<Competences> competences = new ArrayList<Competences>();
		try {
        	//Si value est null, alors on selectionne tout
        	competences = retrouve(SQL_SELECT_BY_CATEGORY, categorie);
        	
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return competences;
	}
	
	private List<Competences> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<Competences> competences = new ArrayList<Competences>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	competences.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return competences;
	}
	private Competences trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        Competences competences = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	competences = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return competences;
	}


}
