package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Experience;

// TODO: Auto-generated Javadoc
/**
 * The Interface ExperienceDao.
 */
public interface ExperienceDao {

        /**
         * Creer.
         *
         * @param workExp the work exp
         * @param id_resume the id_resume
         * @throws DAOException the DAO exception
         */
        void creer( Experience workExp ) throws DAOException;   
        
        /**
         * Modifier.
         *
         * @param workExp the work exp
         * @param id the id
         * @return the experience
         * @throws DAOException the DAO exception
         */
        Experience modifier( Experience workExp, Long id) throws DAOException;
        
        /**
         * Supprimer.
         *
         * @param id the id
         * @throws DAOException the DAO exception
         */
        void supprimer (Long id) throws DAOException;
        
        /**
         * Lister.
         *
         * @return the list
         * @throws DAOException the DAO exception
         */
        List<Experience> lister(Long id_resume) throws DAOException;
        
}
