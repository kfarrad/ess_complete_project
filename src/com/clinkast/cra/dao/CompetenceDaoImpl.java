package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Competence;

public class CompetenceDaoImpl implements CompetenceDao {
    private static final String SQL_SELECT          = "SELECT *  FROM competence WHERE id_resume= ?";
    
    private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM competence WHERE id = ?";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT          = "INSERT INTO competence (id_resume, nom, liste, annees_experience)"
	    + " VALUES (?, ?, ?, ?)";
    
    /** The Constant SQL_DELETE. */
    private static final String SQL_DELETE          = "DELETE FROM competence WHERE id = ?";
    
    /** The Constant SQL_UPDATE. */
    private static final String SQL_UPDATE          = "UPDATE competence SET  nom = ?, liste = ?,"
	    + " annees_experience = ? WHERE id = ?";
    
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    @Override
    public void creer(Competence competence) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
		    competence.getId_resume(), competence.getNom(), competence.getListe(), 
		    competence.getAnnees_experience());
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la création de la competence, aucune ligne ajoutée dans la table." );
	    }
	    valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	    
	    if ( valeursAutoGenerees.next() ) {             
		competence.setId( valeursAutoGenerees.getLong( 1 ) );
		
	    } else {
		throw new DAOException( "Échec de la création de la Competence en base, aucun ID auto-généré retourné." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	}
	
    }
    
    @Override
    public void modifier(Competence competence) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
		    competence.getNom(), competence.getListe(), competence.getAnnees_experience(),
		    competence.getId());
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification  de la Formation, aucune ligne modifiée dans la table." );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
    }
    
    @Override
    public void supprimer(Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, id);
	    int statut = preparedStatement.executeUpdate();
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la suppression de la competence, aucune ligne supprimée de la table." );
	    } 
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    @Override
    public List<Competence> lister(Long id_resume) throws DAOException {
	// TODO Auto-generated method stub
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Competence> competences = new ArrayList<Competence>();
	
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT, false, id_resume );
	    resultSet = preparedStatement.executeQuery();
	    while ( resultSet.next() ) {
		competences.add( map( resultSet ) );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connection );
	}
	
	return competences;
    }
    
    /**
     * Instantiates a new domaine dao impl.
     *
     * @param daoFactory the dao factory
     */
    CompetenceDaoImpl( DAOFactory daoFactory ) {
	this.daoFactory = daoFactory;
    }
    
    
    /**
     * Trouver.
     *
     * @param id the id
     * @return the domaine
     */
    public Competence trouver(Long id )
    {
	return trouver( SQL_SELECT_BY_ID,  id );
    }
    
    
    private Competence trouver( String sql, Object... objets ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Competence competence = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    /*
	     * Préparation de la requête avec les objets passés en arguments
	     * (ici, uniquement un id) et exécution.
	     */
	    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	    resultSet = preparedStatement.executeQuery();
	    /* Parcours de la ligne de données retournée dans le ResultSet */
	    if ( resultSet.next() ) {
		competence = map( resultSet );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return competence;
    }
    
    private static Competence map( ResultSet resultSet ) throws SQLException {
	Competence competence = new Competence();
	competence.setId(resultSet.getLong("id"));
	competence.setId_resume(resultSet.getLong("id_resume"));
	competence.setNom(resultSet.getString("nom"));
	competence.setListe(resultSet.getString("liste"));
	competence.setAnnees_experience((Double) resultSet.getObject("annees_experience"));
	
	return competence;
    }
}
