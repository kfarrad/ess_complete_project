package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Competence;

public interface CompetenceDao {
        
        void creer( Competence competence ) throws DAOException;
        
        void modifier( Competence competence) throws DAOException;
        
        void supprimer( Long id) throws DAOException;
        
        List<Competence> lister(Long id_resume) throws DAOException;
}
