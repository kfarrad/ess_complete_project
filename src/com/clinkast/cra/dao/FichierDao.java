package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Fichier;

// TODO: Auto-generated Javadoc
/**
 * The Interface FichierDao.
 */
public interface FichierDao {

        /**
         * Creer.
         *
         * @param fichier the fichier
         * @throws DAOException the DAO exception
         */
        void  creer(Fichier fichier) throws DAOException;
        
        /**
         * Modifier.
         *
         * @param fichier the fichier
         * @param id the id
         * @throws DAOException the DAO exception
         */
        void modifier(Fichier fichier, Long id) throws DAOException;;
        
        /**
         * Supprimer.
         *
         * @param fichier the fichier
         * @throws DAOException the DAO exception
         */
        void supprimer(Fichier fichier) throws DAOException;
        
        /**
         * Lister.
         *
         * @return the list
         * @throws DAOException the DAO exception
         */
        List<Fichier> lister() throws DAOException;
}
