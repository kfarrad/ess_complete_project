package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.FormationsDemandes;

public interface FormationsDemandesDAO {
	/**
	 * 
	 * @param formationsDemandes
	 * @throws DAOException
	 */
	public void creer(FormationsDemandes formationsDemandes) throws DAOException;
	
	/**
	 * 
	 * @param formationsDemandes
	 * @throws DAOException
	 */
	public void modifier(FormationsDemandes formationsDemandes) throws DAOException;
	/**
	 * 
	 * @param idAutoEvaluation
	 * @return
	 * @throws DAOException
	 */
	public List<FormationsDemandes> lister(Long idAutoEvaluation) throws DAOException;
	/**
	 * 
	 * @param idFormationsSuivies
	 * @return
	 * @throws DAOException
	 */
	public FormationsDemandes trouver(Long idFormationsDemandes) throws DAOException;
	/**
	 * 
	 * @param formationsSuivies
	 * @throws DAOException
	 */
	public void supprimer(FormationsDemandes formationsDemandes) throws DAOException;
}
