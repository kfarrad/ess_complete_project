package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Conges;
import com.clinkast.cra.beans.Utilisateur;

public class CongesDaoImpl implements CongesDAO {

	/** The Constant SQL_SELECT. */
	private static final String SQL_SELECT_BY_ID = "SELECT *  FROM conges WHERE id = ?";
	
	private static final String SQL_SELECT_ALL = "SELECT *  FROM conges";

	private static final String SQL_SELECT_CONGES = "SELECT * FROM conges  "
			+ "where " + "status = ? and id_user=? and id_motif=? and annee=? and mois=?";
	
	private static final String SQL_SELECT_CONGES_ = "SELECT * FROM conges  "
			+ "where " + "status = ? and id_user=? and annee=? and mois=?";

	private static final String SQL_SELECT_CONGES_VALIDER = "SELECT status ,id_user, id_motif, annee, mois, total_jour,j1, "
			+ "j2, j3,j4, j5, j6,j7, j8, j9, j10, j11,j12, j13, j14, j15, j16, j17, j18, j19, j20, j21, j22, j23, j24,"
			+ " j25 , j26, j27 , j28, j29, j30, "
			+ "j31 FROM conges  "
			+ "where " + "periode = ?";

	private static final String SQL_SELECT_ALL_CONGES_VALIDE = "SELECT * FROM conges  "
			+ "where " + "status = 1 and id_user=? and annee=? and mois=?";
	
	/** The Constant SQL_SELECT_CAL. */
	private static final String SQL_SELECT_CAL = "SELECT *  FROM conges WHERE id_user = ? and id_motif = ? and annee = ? and mois = ? ";

	/** The Constant SQL_INSERT. */
	private static final String SQL_INSERT = "INSERT INTO conges ( id_user, id_motif, annee, mois, periode, utilisateur, total_jour,"
			+ " j1,  j2 ,  j3, j4, j5, j6, j7, j8, j9, j10, j11, j12, j13, j14,"
			+ " j15, j16, j17, j18, j19, j20, j21,	j22, j23, j24, j25, j26, j27, j28, j29, j30, j31)"
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE = "UPDATE conges SET status=?, id_user=?, id_motif=?, annee=?, mois=?, total_jour=?, j1=?, j2=?, j3=?, j4=?, j5=?, j6=?, j7=?, j8=?, j9=?, j10=?, j11=?, j12=?, j13=?, j14=?, j15=?,"
			+ "j16=?, j17=?, j18=?, j19=?, j20=?, j21=?, j22=?, j23=?, j24=?, j25=?, j26=?, j27=?, j28=?, j29=?, j30=?, j31=?, periode=?, utilisateur=?"
			+ "WHERE id = ?";

	/** The Constant SQL_UPDATE_FERIER. */
	private static final String SQL_UPDATE_FERIER = "UPDATE conges SET  j1=?, j2=?, j3=?, j4=?, j5=?, j6=?, j7=?, j8=?, j9=?, j10=?, j11=?, j12=?, j13=?, j14=?, j15=?,"
			+ "j16=?, j17=?, j18=?, j19=?, j20=?, j21=?, j22=?, j23=?, j24=?, j25=?, j26=?, j27=?, j28=?, j29=?, j30=?, j31=?"
			+ "WHERE id=?";

	private static final String SQL_DELETE_CONGE = "UPDATE conges SET  actif= 0 WHERE id = ?";

	/** The Constant SQL_UPDATE_STATUS. */
	private static final String SQL_UPDATE_STATUS = "UPDATE conges SET status=? WHERE id = ?";

	private static final String SQL_COUNT_JOUR = "SELECT sum(total_jour)  FROM conges WHERE id_user= ? and id_motif = ?  and annee >= ? and mois >= ?";

	/** The Constant SQL_STATUS_USER. */
	private static final String SQL_STATUS_USER = "SELECT status  FROM conges WHERE id_user = ? and mois = ? and annee = ? ";

	private static final String SQL_SELECT = "SELECT *  FROM conges WHERE id_user = ? and mois = ? and annee = ? ";

	private static final String SQL_SELECT_BY_STATUT_ACTIF = "SELECT *  FROM conges WHERE status = ? and actif = 1" ;

	/** The dao factory. */
	private DAOFactory daoFactory;


	private int noOfRecords;
	
	/**
	 * Instantiates a new conges dao impl.
	 *
	 * @param daoFactory
	 *            the dao factory
	 */
	CongesDaoImpl(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	/**
	 * Trouver.
	 *
	 * @param id
	 *            the id
	 * @return the conges
	 */
	public Conges trouver(Long id) {
		return trouver(SQL_SELECT_BY_ID, id);
	}

	/**
	 * Trouver.
	 *
	 * @param id_user
	 *            the id_user
	 * @param id_motif
	 *            the id_motif
	 * @param annee
	 *            the annee
	 * @param mois
	 *            the mois
	 * @return the conges
	 */
	public Conges trouver(Long id_user, Long id_motif, Long annee, Long mois) {
		return trouver(SQL_SELECT_CAL, id_user, id_motif, annee, mois);
	}

	/**
	 * Sets the status.
	 *
	 * @param id_user
	 *            the id_user
	 * @param id_motif
	 *            the id_motif
	 * @param annee
	 *            the annee
	 * @param mois
	 *            the mois
	 * @param status
	 *            the status
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void setStatus(Long idConge,	Long status) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_UPDATE_STATUS, false, status, idConge);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DAOException(
						"Ãƒâ€°chec de la modification du status, aucune ligne modifiÃƒÂ©e dans la table.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(preparedStatement, connexion);
		}

	}

	/**
	 * Sets the holiday.
	 *
	 * @param id_user
	 *            the id_user
	 * @param id_motif
	 *            the id_motif
	 * @param annee
	 *            the year
	 * @param mois
	 *            the month
	 * @param jours
	 *            the List of days
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void setFerier(Long id_user, Long id_motif, Long annee, Long mois,
			List<Float> jours) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			int i = 0;
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_INSERT, true, id_user, id_motif, annee, mois, -1,
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++));
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DAOException(
						"Ãƒâ€°chec de la modification du status, aucune ligne modifiÃƒÂ©e dans la table.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(preparedStatement, connexion);
		}

	}

	/**
	 * Update ferier.
	 *
	 * @param id
	 *            the id
	 * @param jours
	 *            the jours
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void updateFerier(Long id, List<Float> jours) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			int i = 0;
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_UPDATE_FERIER, false, jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), jours.get(i++),
					jours.get(i++), jours.get(i++), id);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DAOException(
						"Ãƒâ€°chec de la modification des jours feriers, aucune ligne modifiÃƒÂ©e dans la table.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(preparedStatement, connexion);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.clinkast.cra.dao.CalendarDao#creer(com.clinkast.cra.beans.Conges)
	 */
	@Override
	public void creer(Conges conges) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		try {
			connexion = daoFactory.getConnection();
			int i = 0;
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_INSERT, true, conges.getId_user(),
					conges.getId_motif(), conges.getAnnee(), conges.getMois(),
					conges.getPeriode(), conges.getUtilisateur(),
					conges.getTotal_jour(), conges.getJours().get(i++), conges
							.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++));
			int statut = preparedStatement.executeUpdate();

			if (statut == 0) {
				throw new DAOException(
						"Ãƒâ€°chec de la crÃƒÂ©ation du conges, aucune ligne ajoutÃƒÂ©e dans la table.");
			}
			valeursAutoGenerees = preparedStatement.getGeneratedKeys();

			if (valeursAutoGenerees.next()) {
				conges.setId(valeursAutoGenerees.getLong(1));

			} else {
				throw new DAOException(
						"Ãƒâ€°chec de la crÃƒÂ©ation du conges en base, aucun ID auto-gÃƒÂ©nÃƒÂ©rÃƒÂ© retournÃƒÂ©.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
					connexion);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.clinkast.cra.dao.CalendarDao#modifier(com.clinkast.cra.beans.Conges,
	 * java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public Conges modifier(Long id, Long status, Conges conges) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		int i = 0;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_UPDATE, false, status, conges.getId_user(), conges.getId_motif(), conges.getAnnee(), conges.getMois(), conges.getTotal_jour(), conges
							.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getJours().get(i++),
					conges.getJours().get(i++), conges.getPeriode(), conges.getUtilisateur(), id);

			int statut = preparedStatement.executeUpdate();

			if (statut == 0) {
				throw new DAOException(
						"Echec de la modification   conges, aucune ligne modifiée dans la table.");
			}
//		} catch (DAOException e) {
//			e.printStackTrace();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(preparedStatement, connexion);
		}

		return conges;
	}

	/* ImplÃƒÂ©mentation de la mÃƒÂ©thode dÃƒÂ©finie dans l'interface ClientDao */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.clinkast.cra.dao.CalendarDao#lister()
	 */
	@Override
	public List<Conges> lister() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> clients = new ArrayList<Conges>();

		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ALL);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				clients.add(map(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connection);
		}

		return clients;
	}

	@Override
	public List<Conges> listerConges(Long statut, Long id_user, Long id_motif, Long annee , Long mois) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> listeConges = new ArrayList<Conges>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT_CONGES, false, statut, id_user, id_motif, annee , mois);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				listeConges.add(map(resultSet));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return listeConges;
	}
	
	@Override
	public List<Conges> listerConges(Long statut, Long id_user, Long annee , Long mois) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> listeConges = new ArrayList<Conges>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT_CONGES_, false, statut, id_user, annee , mois);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				listeConges.add(map(resultSet));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return listeConges;
	}

	@Override
	public List<Conges> listerCongesValide(String periode) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> listeConges = new ArrayList<Conges>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT_CONGES_VALIDER, false, periode);
			resultSet = preparedStatement.executeQuery();
			int i = 0;
			while (resultSet.next()) {
				listeConges.add(i, map3(resultSet));
				i++;
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return listeConges;
	}

	/* ImplÃƒÂ©mentation de la mÃƒÂ©thode dÃƒÂ©finie dans l'interface ClientDao */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.clinkast.cra.dao.CalendarDao#supprimer(com.clinkast.cra.beans.Conges)
	 */
	@Override
	public void supprimer(Long idConge, boolean isUser) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;

		try {
			connexion = daoFactory.getConnection();
			if (isUser)
				preparedStatement = initialisationRequetePreparee(connexion,
						SQL_DELETE_CONGE, true, idConge);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DAOException(
						"Ãƒâ€°chec de la suppression du profil, aucune ligne supprimÃƒÂ©e de la table.");
			} 
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(preparedStatement, connexion);
		}
	}

	/*
	 * MÃƒÂ©thode gÃƒÂ©nÃƒÂ©rique utilisÃƒÂ©e pour retourner un client depuis la
	 * base de donnÃƒÂ©es, correspondant ÃƒÂ  la requÃƒÂªte SQL donnÃƒÂ©e
	 * prenant en paramÃƒÂ¨tres les objets passÃƒÂ©s en argument.
	 */
	/**
	 * Trouver.
	 *
	 * @param sql
	 *            the sql
	 * @param objets
	 *            the objets
	 * @return the conges
	 * @throws DAOException
	 *             the DAO exception
	 */
	private Conges trouver(String sql, Object... objets) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Conges client = null;

		try {
			/* RÃƒÂ©cupÃƒÂ©ration d'une connexion depuis la Factory */
			connexion = daoFactory.getConnection();
			/*
			 * PrÃƒÂ©paration de la requÃƒÂªte avec les objets passÃƒÂ©s en
			 * arguments (ici, uniquement un id) et exÃƒÂ©cution.
			 */
			preparedStatement = initialisationRequetePreparee(connexion, sql,
					false, objets);
			resultSet = preparedStatement.executeQuery();
			/* Parcours de la ligne de donnÃƒÂ©es retournÃƒÂ©e dans le ResultSet */
			if (resultSet.next()) {
				client = map(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return client;
	}

	/*
	 * Simple mÃƒÂ©thode utilitaire permettant de faire la correspondance (le .
	 */
	/**
	 * Mapping entre une ligne issue de la table (un ResultSet) et un bean
	 * Conges.
	 *
	 * @param resultSet
	 *            the result set
	 * @return the conges
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Conges map(ResultSet resultSet) throws SQLException {
		
		Conges conges = new Conges();
		conges.setId(resultSet.getLong("id"));
		conges.setStatus(resultSet.getLong("status"));
		conges.setId_user(resultSet.getLong("id_user"));
		conges.setUtilisateur(resultSet.getString("utilisateur"));
		// conges.setUtilisateur(userDao.trouver_ID(resultSet.getLong("id_user")));
		conges.setId_motif(resultSet.getLong("id_motif"));
		conges.setAnnee(resultSet.getLong("annee"));
		conges.setMois(resultSet.getLong("mois"));
		conges.setTotal_jour(resultSet.getDouble("total_jour"));
		// conges.setUtilisateur(userDao.trouver_ID(resultSet.getLong("id_user")));
		conges.setPeriode(resultSet.getString("periode"));

		List<Float> jours = new ArrayList<Float>();

		for (int i = 1; i < 32; i++) {
			String index = "j" + i;
			jours.add(resultSet.getFloat(index));
		}
		conges.setJours(jours);
		return conges;
	}

	private Conges map3(ResultSet resultSet) throws SQLException {

		long status = 2;
		Conges conges = new Conges();
		conges.setStatus(status);
		conges.setId_user(resultSet.getLong("id_user"));
		conges.setId_motif(resultSet.getLong("id_motif"));
		conges.setAnnee(resultSet.getLong("annee"));
		conges.setMois(resultSet.getLong("mois"));
		conges.setTotal_jour(resultSet.getDouble("total_jour"));

		List<Float> jours = new ArrayList<Float>();

		for (int i = 1; i < 32; i++) {
			String index = "j" + i;
			jours.add(resultSet.getFloat(index));
		}
		conges.setJours(jours);
		return conges;
	}

	private Conges map2(ResultSet resultSet) throws SQLException {

		// String user2= utilisateur.getName();
		// UserDaoImpl userDao = daoFactory.getUserDao();
		Conges conges = new Conges();
		conges.setId(resultSet.getLong("id"));
		conges.setStatus(resultSet.getLong("status"));
		conges.setId_user(resultSet.getLong("id_user"));
		conges.setUtilisateur(resultSet.getString("utilisateur"));
		conges.setId_motif(resultSet.getLong("id_motif"));
		// congesetUtilisate.sur(userDao.trouver_ID(resultSet.getLong("id_user")));
		conges.setPeriode(resultSet.getString("periode"));
		// conges.setUtilisateur(user2);

		return conges;
	}

	/**
	 * Lister conges.
	 *
	 * @param utilisateur
	 *            the utilisateur
	 * @param mois
	 *            the mois
	 * @param annee
	 *            the annee
	 * @return the list
	 */
	public List<Conges> listerConges(Utilisateur utilisateur, int mois,
			int annee) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> listeCalendar = new ArrayList<Conges>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT, false, utilisateur.getId(), mois, annee);
			resultSet = preparedStatement.executeQuery();
			int i = 0;
			while (resultSet.next()) {
				listeCalendar.add(i, map(resultSet));
				i++;
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return listeCalendar;
	}

	public int getStatus(final Long id_user, final int mois, final int annee) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int status = 0;
		try {
			connection = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connection,
					SQL_STATUS_USER, false, id_user, mois, annee);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				status = resultSet.getInt("status");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connection);
		}
		return status;
	}

	/**
	 * Lister jours feriers.
	 *
	 * @param utilisateur
	 *            the utilisateur
	 * @param mois
	 *            the mois
	 * @param annee
	 *            the annee
	 * @return the list
	 */
	public List<Float> listerJoursFeriers(Utilisateur utilisateur, int mois,
			int annee) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Float> JoursFeriers = new ArrayList<Float>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT, false, utilisateur.getId(), mois, annee);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				for (int i = 1; i < 32; i++) {
					String index = "j" + i;
					if (resultSet.getFloat(index) == -1)
						JoursFeriers.add(i, resultSet.getFloat(index));
				}

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return JoursFeriers;
	}

	@Override
	public float count_jour(Long id_user, Long id_motif, Long annee, Long mois)
			throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		float nombre = 0;
		ResultSet resultSet = null;

		try {
			/* RÃƒÂ©cupÃƒÂ©ration d'une connexion depuis la Factory */
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_COUNT_JOUR, false, id_user, id_motif, annee, mois);
			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next())
					nombre = resultSet.getFloat(1);
				// nombre = ((Number) resultSet.getObject(1)).intValue();
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} catch (NullPointerException e) {
			// TODO: handle exception
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}
		return nombre;
	}

	@SuppressWarnings("resource")
	public List<Conges> listerCongesByUser(Long id_user,int debut, int nombreConges) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Conges> congesList = new ArrayList<Conges>();
		String request = "SELECT SQL_CALC_FOUND_ROWS c.id, c.utilisateur, p.nom, c.periode,c.total_jour, c.status, c.actif,  ( CASE  WHEN c.status = 0 THEN 0 WHEN c.status = -1 THEN 2 WHEN c.status = -2 THEN 3 WHEN c.status = 1 THEN 1 END ) as ordre  FROM conges c,projet p WHERE c.id_motif=p.id and c.id_user = "+id_user+" and c.actif <> 0 and c.status <> 0 ORDER BY ordre ASC LIMIT "
				+ debut + ", " + nombreConges;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				Conges conges = new Conges();
				conges.setId(results.getLong("id"));
				conges.setUtilisateur(results.getString("utilisateur"));
				conges.setTypeConge(results.getString("nom"));
				conges.setPeriode(results.getString("periode"));
				conges.setTotal_jour(results.getDouble("total_jour"));
				conges.setStatus(results.getLong("status"));
				conges.setActif(results.getBoolean("actif"));
				congesList.add(conges);
			}
			
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}
		return congesList;
	}
	@SuppressWarnings("resource")
	public List<Conges> listerCongesByUserAndByStatus(Long id_user,int debut, int nombreConges, int status) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Conges> congesList = new ArrayList<Conges>();
		String request = "SELECT SQL_CALC_FOUND_ROWS c.id, c.utilisateur, p.nom, c.periode,c.total_jour, c.status, c.actif,  as ordre  FROM conges c,projet p WHERE c.id_motif=p.id and c.id_user = "+id_user+" and c.actif <> 0 and c.status=0 ORDER BY ordre ASC LIMIT "
				+ debut + ", " + nombreConges;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				Conges conges = new Conges();
				conges.setId(results.getLong("id"));
				conges.setUtilisateur(results.getString("utilisateur"));
				conges.setTypeConge(results.getString("nom"));
				conges.setPeriode(results.getString("periode"));
				conges.setTotal_jour(results.getDouble("total_jour"));
				conges.setStatus(results.getLong("status"));
				conges.setActif(results.getBoolean("actif"));
				congesList.add(conges);
			}
			
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}
		return congesList;
	}
	
	@SuppressWarnings("resource")
	public List<Conges> listerCongesByUserByStatusNotZero(int debut, int nombreConges) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Conges> congesList = new ArrayList<Conges>();
		String request = "SELECT SQL_CALC_FOUND_ROWS  c.id,  c.utilisateur, p.nom, c.periode,c.total_jour, c.status,c.id_user, c.actif,c.status as ordre FROM conges c,projet p WHERE c.id_motif=p.id and c.status <> 0 and c.actif <> 0  ORDER BY c.status ASC LIMIT "
				+ debut + ", " + nombreConges;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				Conges conges = new Conges();
				conges.setId(results.getLong("id"));
				conges.setUtilisateur(results.getString("utilisateur"));
				conges.setTypeConge(results.getString("nom"));
				conges.setPeriode(results.getString("periode"));
				conges.setTotal_jour(results.getDouble("total_jour"));
				conges.setStatus(results.getLong("status"));
				conges.setActif(results.getBoolean("actif"));
				congesList.add(conges);
			}
			
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}
		return congesList;
	}
	
	public List<Conges> listerCongesByUserAndStatut(Long statut) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> listeConges = new ArrayList<Conges>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT_BY_STATUT_ACTIF, false, statut);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				listeConges.add(map(resultSet));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return listeConges;
	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

	public void setNoOfRecords(int noOfRecords) {
		this.noOfRecords = noOfRecords;
	}

	@Override
	public List<Conges> listerConges(Long id, int mois, int annee) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Conges> listeConges = new ArrayList<Conges>();
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_SELECT_ALL_CONGES_VALIDE, false, id, annee , mois);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				listeConges.add(map(resultSet));
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connexion);
		}

		return listeConges;
	}
	
	@SuppressWarnings("resource")
	public List<Conges> listerCongesByUser(int debut, int nombreConges) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Conges> congesList = new ArrayList<Conges>();
		String request = "SELECT SQL_CALC_FOUND_ROWS c.id, c.utilisateur, p.nom, c.periode,c.total_jour, c.status, c.actif,c.status as ordre FROM conges c,projet p WHERE c.id_motif=p.id and c.status = 0 and c.actif <> 0  ORDER BY ordre ASC, c.utilisateur ASC LIMIT "
				+ debut + ", " + nombreConges;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				Conges conges = new Conges();
				conges.setId(results.getLong("id"));
				conges.setUtilisateur(results.getString("utilisateur"));
				conges.setTypeConge(results.getString("nom"));
				conges.setPeriode(results.getString("periode"));
				conges.setTotal_jour(results.getDouble("total_jour"));
				conges.setStatus(results.getLong("status"));
				conges.setActif(results.getBoolean("actif"));
				congesList.add(conges);
			}
			
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}
		return congesList;
	}
}
