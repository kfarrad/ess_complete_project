package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjetDaoImpl.
 */
public class ProjetDaoImpl implements ProjetDao{

	/** The Constant SQL_SELECT. */
	private static final String SQL_SELECT			= "SELECT *  FROM projet ";
	
	/** The Constant SQL_SELECT_BY_ID. */
	private static final String SQL_SELECT_BY_ID	= "SELECT *  FROM projet WHERE id = ?";
	
	/** The Constant SQL_SELECT_BY_NOM. */
	private static final String SQL_SELECT_BY_NOM	= "SELECT *  FROM projet WHERE nom = ?";
	
	/** The Constant SQL_INSERT. */
	private static final String SQL_INSERT      	= "INSERT INTO projet (id_client, nom) VALUES (?, ?)";
	
	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE			= "UPDATE projet SET  nom=? WHERE id = ?";
	
	/** The Constant SQL_DELETE. */
	private static final String SQL_DELETE			= "DELETE FROM projet WHERE id = ?";
	
	/** The Constant SQL_INSERT_USER. */
	private static final String SQL_INSERT_USER		= "INSERT INTO user_projets (id_user, id_projet) VALUES (?, ?)";
	
	/** The Constant SQL_SELECT_PROJET. */
	private static final String SQL_SELECT_PROJET	= "SELECT * FROM user_projets up, projet p WHERE up.id_user = ? and up.id_projet=p.id";
	
	/** The Constant SQL_SELECT_PROJET_ACTIF. */
	private static final String SQL_SELECT_PROJET_ACTIF	= "SELECT * FROM user_projets up, projet p WHERE up.id_user = ? and up.id_projet=p.id and up.actif=1";
	
	/** The Constant SQL_TROUVE_PROJET. */
	private static final String SQL_TROUVE_PROJET_ACTIF	= "SELECT * FROM user_projets up WHERE up.id_user = ? and up.id_projet= ? and up.actif=1";
	
	/** The Constant SQL_TROUVE_PROJET. */
	private static final String SQL_TROUVE_PROJET	= "SELECT * FROM user_projets up WHERE up.id_user = ? and up.id_projet= ?";

	/** The Constant SQL_DELETE_ATTRIBUTION. */
	private static final String SQL_DELETE_ATTRIBUTION = "UPDATE user_projets SET actif=? WHERE id_user = ? and id_projet=?";
	
	/** The Constant SQL_TROUVE_PROJET. */
	private static final String SQL_TROUVE_CONGES	= "SELECT * FROM  `projet`	WHERE nom LIKE'%RTT%'	OR nom LIKE'%CP%'	OR nom LIKE'%CSS%'	OR nom LIKE'%AR%'";
    
	/** The dao factory. */
	private DAOFactory          daoFactory;

	/**
	 * Instantiates a new projet dao impl.
	 *
	 * @param daoFactory the dao factory
	 */
	ProjetDaoImpl( DAOFactory daoFactory ) {
		this.daoFactory = daoFactory;
	}

	/* Implémentation de la méthode définie dans l'interface ClientDao */




	/**
	 * Trouver.
	 *
	 * @param id the id
	 * @return the projet
	 */
	public Projet trouver(Long id )
	{
		return trouver( SQL_SELECT_BY_ID,  id  );
	}

	/**
	 * Trouver.
	 *
	 * @param nom the nom
	 * @return the projet
	 */
	public Projet trouver(String nom )
	{
		return trouver( SQL_SELECT_BY_NOM,  nom  );
	}
	
	/**
	 * Trouver attribution.
	 *
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 * @return true, if successful
	 */
	public boolean trouverAttribution(Long id_user , Long id_projet )
	{
		boolean trouve = false;
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			/* Récupération d'une connexion depuis la Factory */
			connexion = daoFactory.getConnection();
			/*
			 * Préparation de la requête avec les objets passés en arguments
			 * (ici, uniquement un id) et exécution.
			 */
			preparedStatement = initialisationRequetePreparee( connexion, SQL_TROUVE_PROJET_ACTIF, false, id_user, id_projet );
			resultSet = preparedStatement.executeQuery();
			/* Parcours de la ligne de données retournée dans le ResultSet */
			if ( resultSet.next() ) {
				trouve=true;
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connexion );
		}
		return trouve;
	}
	
	
	
	/**
	 * Trouver attribution inactive.
	 *
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 * @return true, if successful
	 */
	public boolean trouverAttributionInactive(Long id_user , Long id_projet )
	{
		boolean trouve = false;
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			/* Récupération d'une connexion depuis la Factory */
			connexion = daoFactory.getConnection();
			/*
			 * Préparation de la requête avec les objets passés en arguments
			 * (ici, uniquement un id) et exécution.
			 */
			preparedStatement = initialisationRequetePreparee( connexion, SQL_TROUVE_PROJET, false, id_user, id_projet );
			resultSet = preparedStatement.executeQuery();
			/* Parcours de la ligne de données retournée dans le ResultSet */
			if ( resultSet.next() ) {
				trouve=true;
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connexion );
		}
		return trouve;
	}
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.ProjetDao#creer(com.clinkast.cra.beans.Projet, java.lang.Long)
	 */
	@Override
	public void creer( Projet projet, Long id_client ) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
					id_client, projet.getNom() );
			int statut = preparedStatement.executeUpdate();

			if ( statut == 0 ) {
				throw new DAOException( "Échec de la création du projet, aucune ligne ajoutée dans la table." );
			}
			valeursAutoGenerees = preparedStatement.getGeneratedKeys();
			projet.setId_client(id_client);
			if ( valeursAutoGenerees.next() ) {            	
				projet.setId( valeursAutoGenerees.getLong( 1 ) );

			} else {
				throw new DAOException( "Échec de la création du projet en base, aucun ID auto-généré retourné." );
			}

		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
		}

	}
	
	
	
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.ProjetDao#modifier(com.clinkast.cra.beans.Projet, java.lang.Long)
	 */
	@Override
	public Projet modifier(Projet projet, Long id ) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
					projet.getNom(), id );

			int statut = preparedStatement.executeUpdate();

			if ( statut == 0 ) {
				throw new DAOException( "Échec de la modification  du nom du projet, aucune ligne modifiée dans la table." );
			}

		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}

		return projet;
	}

	/* Implémentation de la méthode définie dans l'interface ClientDao */
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.ProjetDao#lister()
	 */
	@Override
	public List<Projet> lister() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Projet> clients = new ArrayList<Projet>();

		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement( SQL_SELECT );
			resultSet = preparedStatement.executeQuery();
			while ( resultSet.next() ) {
				clients.add( map( resultSet ) );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connection );
		}

		return clients;
	}
	
	
	 /**
 	 * Lister user projets.
 	 *
 	 * @param utilisateur the utilisateur
 	 * @return the list
 	 * @throws DAOException the DAO exception
 	 */
 	public List<Projet> listerUserProjets(Utilisateur utilisateur) throws DAOException {
	        Connection connexion = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        List<Projet> projets = new ArrayList<Projet>();
	        try {
		            connexion = daoFactory.getConnection();
		            //System.out.println(SQL_SELECT_PROJET);
		            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PROJET, false, utilisateur.getId());
		            resultSet = preparedStatement.executeQuery();
		            int i=0;
		            while ( resultSet.next() ) {		            	
		            	projets.add(i, map( resultSet ) );
		            	i++;
		            }
	        	
	        } catch ( SQLException e ) {
	            throw new DAOException( e );
	        } finally {
	            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	        }

	        return projets;
	    }
 	
 	
 	/**
	  * Lister user projets actif.
	  *
	  * @param utilisateur the utilisateur
	  * @return the list
	  * @throws DAOException the DAO exception
	  */
	 public List<Projet> listerUserProjetsActif(Utilisateur utilisateur) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Projet> projets = new ArrayList<Projet>();
        try {
	            connexion = daoFactory.getConnection();
	            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PROJET_ACTIF, false, utilisateur.getId());
	            resultSet = preparedStatement.executeQuery();
	            int i=0;
	            while ( resultSet.next() ) {		            	
	            	projets.add(i, map( resultSet ) );
	            	i++;
	            }
        	
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return projets;
    }
 	
 	
 	
 	
 	
 	
	/* Implémentation de la méthode définie dans l'interface ClientDao */
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.ProjetDao#supprimer(com.clinkast.cra.beans.Projet)
	 */
	@Override
	public void supprimer( Projet projet ) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;

		try {
			connexion = daoFactory.getConnection();
			Projet monProjet = this.trouver(projet.getId());
			if(monProjet!=null)
				preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, false, projet.getId());
			int statut = preparedStatement.executeUpdate();
			if ( statut == 0 ) {
				throw new DAOException( "Échec de la suppression du projet, aucune ligne supprimée de la table." );
			} else {
				projet.setId( null );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}
	}

	/*
	 * Méthode générique utilisée pour retourner un client depuis la base de
	 * données, correspondant à la requête SQL donnée prenant en paramètres les
	 * objets passés en argument.
	 */
	/**
	 * Trouver.
	 *
	 * @param sql the sql
	 * @param objets the objets
	 * @return the projet
	 * @throws DAOException the DAO exception
	 */
	private Projet trouver( String sql, Object... objets ) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Projet client = null;

		try {
			/* Récupération d'une connexion depuis la Factory */
			connexion = daoFactory.getConnection();
			/*
			 * Préparation de la requête avec les objets passés en arguments
			 * (ici, uniquement un id) et exécution.
			 */
			preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
			resultSet = preparedStatement.executeQuery();
			/* Parcours de la ligne de données retournée dans le ResultSet */
			if ( resultSet.next() ) {
				client = map( resultSet );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connexion );
		}

		return client;
	}

	/*
	 * Simple méthode utilitaire permettant de faire la correspondance (le
	 * mapping) entre une ligne issue de la table des clients (un ResultSet) et
	 * un bean Client.
	 */
	/**
	 * Map.
	 *
	 * @param resultSet the result set
	 * @return the projet
	 * @throws SQLException the SQL exception
	 */
	private static Projet map( ResultSet resultSet ) throws SQLException {
		Projet client = new Projet();
		client.setId( resultSet.getLong( "id" ) );	    
		client.setId_client(resultSet.getLong("id_client"));
		client.setNom( resultSet.getString( "nom" ) );

		return client;
	}

	/**
	 * Attribution.
	 *
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 */
	public void attribution(Long id_user, Long id_projet) {
		// TODO Auto-generated method stub
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			
			if(!trouverAttributionInactive(id_user, id_projet))
			preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT_USER, true,
					id_user, id_projet );
			
			else preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE_ATTRIBUTION, false, 1,
					id_user, id_projet );
			int statut = preparedStatement.executeUpdate();

			if ( statut == 0 ) {
				throw new DAOException( "Échec de la création du projet, aucune ligne ajoutée dans la table." );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}


	}

	/**
	 * Supprimer attribution.
	 *
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 */
	public void supprimerAttribution(Long id_user, Long id_projet) {
		// TODO Auto-generated method stub
		
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE_ATTRIBUTION, false, 0,
					id_user, id_projet );
			int statut = preparedStatement.executeUpdate();
			if ( statut == 0 ) {
				throw new DAOException( "Échec de la modification de user_projet, aucune ligne ajoutée dans la table." );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}


	}

	@Override
	public List<Projet> listerConges() {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Projet> conges = new ArrayList<Projet>();

		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement(SQL_TROUVE_CONGES);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				conges.add(map(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(resultSet, preparedStatement, connection);
		}

		return conges;
	
	}

	@Override
	public List<Projet> listerProjetNonFactures() throws DAOException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Projet> projets = new ArrayList<Projet>();
		String request = "";
		try {
			request = "SELECT *  FROM projet where id IN ("+Constants.getIdProjetsNonFactures().toString().trim().substring(1, Constants.getIdProjetsNonFactures().toString().length()-1)+")";
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement( request );
			resultSet = preparedStatement.executeQuery();
			while ( resultSet.next() ) {
				projets.add( map( resultSet ) );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connection );
		}

		return projets;
	
	}
	

}
