package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.FormationsSuivies;

public class FormationsSuiviesDAOImpl implements FormationsSuiviesDAO {
	
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO formations_suivies ( " +
			"formations_suivies_intitule, " +
			"formations_suivies_commentaires, " +
			"auto_evaluations_auto_evaluations_id) VALUES (?, ?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE formations_suivies SET  formations_suivies_intitule=?, formations_suivies_commentaires=? WHERE formations_suivies_id = ?";
	/*
	 * Constante SQL_DELETE
	 */
	private static final String SQL_DELETE = "DELETE FROM formations_suivies WHERE formations_suivies_id = ?";
	/*
	 * Constante SQL_SELECT_ALL
	 */
	private static final String SQL_SELECT_ALL = "SELECT * FROM formations_suivies WHERE auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM formations_suivies WHERE formations_suivies_id = ?";
    
	private DAOFactory daoFactory;
	
	public FormationsSuiviesDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}
	

	@Override
	public void creer(FormationsSuivies formationsSuivies) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, formationsSuivies.getFormationsSuiviesIntitule(), formationsSuivies.getFormationsSuiviesCommentaires(), formationsSuivies.getAutoEvaluations().getAutoEvaluationsId());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                formationsSuivies.setFormationsSuiviesId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
	}

	@Override
	public void modifier(FormationsSuivies formationsSuivies) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		formationsSuivies.getFormationsSuiviesIntitule(), formationsSuivies.getFormationsSuiviesCommentaires(), formationsSuivies.getFormationsSuiviesId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}

	@Override
	public List<FormationsSuivies> lister(Long idAutoEvaluation)
			throws DAOException {
		// TODO Auto-generated method stub
		List<FormationsSuivies> formationsSuivies = new ArrayList<FormationsSuivies>();
		
		try {
            formationsSuivies = retrouve(SQL_SELECT_ALL, idAutoEvaluation);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return formationsSuivies;
		
	}
	@Override
	public FormationsSuivies trouver(Long idFormationsSuivies) 
			throws DAOException{
		FormationsSuivies formationsSuivies = null;  
        try {
            formationsSuivies = trouve(SQL_SELECT_BY_ID, idFormationsSuivies);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return formationsSuivies;
	}
	@Override
	public void supprimer(FormationsSuivies formationsSuivies) throws DAOException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, formationsSuivies.getFormationsSuiviesId());
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la suppression de la formation , aucune ligne supprimée de la table." );
            } else {
            	formationsSuivies = null;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}
	
	private List<FormationsSuivies> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<FormationsSuivies> formationsSuivies = new ArrayList<FormationsSuivies>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	formationsSuivies.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return formationsSuivies;
	}
	private FormationsSuivies trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        FormationsSuivies formationsSuivies = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	formationsSuivies = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return formationsSuivies;
	}

	/**
	 * Map fabrique un objet de type SystemesActivites
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private FormationsSuivies map( ResultSet resultSet ) throws SQLException {
		
		//AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		FormationsSuivies formationsSuivies = new FormationsSuivies();
		formationsSuivies.setFormationsSuiviesId(resultSet.getLong( "formations_suivies_id" ) );	    	       
		formationsSuivies.setFormationsSuiviesIntitule(resultSet.getString("formations_suivies_intitule"));
		formationsSuivies.setFormationsSuiviesCommentaires(resultSet.getString("formations_suivies_commentaires"));
		//formationsSuivies.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));
       
        return formationsSuivies;
    }
    
	

}
