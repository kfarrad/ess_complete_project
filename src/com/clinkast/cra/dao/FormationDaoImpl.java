package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Formation;

public class FormationDaoImpl implements FormationDao{
    /** The Constant SQL_SELECT. */
    private static final String SQL_SELECT          = "SELECT *  FROM formation WHERE id_resume = ? ";
    
    private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM formation WHERE id = ?";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT          = "INSERT INTO formation (id_resume, domaine, institut, specialisation, annee, moyenne,"
	    + " date_debut, date_fin) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    /** The Constant SQL_DELETE. */
    private static final String SQL_DELETE          = "DELETE FROM formation WHERE id = ?";
    
    /** The Constant SQL_UPDATE. */
    private static final String SQL_UPDATE          = "UPDATE formation SET  domaine = ?, institut = ?,"
	    + " specialisation = ?, annee = ?, moyenne = ?,"
	    + " date_debut = ?, date_fin = ? WHERE id = ?";
    
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    /**
     * Instantiates a new domaine dao impl.
     *
     * @param daoFactory the dao factory
     */
    FormationDaoImpl( DAOFactory daoFactory ) {
	this.daoFactory = daoFactory;
    }
    
    
    /**
     * Trouver.
     *
     * @param id the id
     * @return the domaine
     */
    public Formation trouver(Long id )
    {
	return trouver( SQL_SELECT_BY_ID,  id );
    }
    
    
    @Override
    public void creer(Formation formation) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
		    formation.getId_resume(), formation.getDomaine(),
		    formation.getInstitut(), formation.getSpecialisation(),
		    formation.getAnnee(), formation.getMoyenne(), 
		    formation.getDate_debut(), formation.getDate_fin());
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la création de la Formation, aucune ligne ajoutée dans la table." );
	    }
	    valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	    
	    if ( valeursAutoGenerees.next() ) {             
		formation.setId( valeursAutoGenerees.getLong( 1 ) );
		
	    } else {
		throw new DAOException( "Échec de la création de la Formation en base, aucun ID auto-généré retourné." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	}
	
    }
    
    
    @Override
    public Formation modifier(Formation formation, Long id)
	    throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
		    formation.getDomaine(), formation.getInstitut(), formation.getSpecialisation(),
		    formation.getAnnee(), formation.getMoyenne(),  formation.getDate_debut(), formation.getDate_fin(), id);
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification  de la Formation, aucune ligne modifiée dans la table." );
	    }
	    formation.setId(id);
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
	return formation;
    }
    
    
    
    
    
    @Override
    public List<Formation> lister(Long id_resume) throws DAOException {
	// TODO Auto-generated method stub
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Formation> formations = new ArrayList<Formation>();
	
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT, false, id_resume );
	    resultSet = preparedStatement.executeQuery();
	    while ( resultSet.next() ) {
		formations.add( map( resultSet ) );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connection );
	}
	
	return formations;
    }
    
    
    @Override
    public void supprimer(Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	
	try {
	    connexion = daoFactory.getConnection();
	    Formation maFormation = this.trouver(id);
	    if(maFormation!=null){
		preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, id);
		int statut = preparedStatement.executeUpdate();
		if ( statut == 0 ) {
		    throw new DAOException( "Échec de la suppression de la formation, aucune ligne supprimée de la table." );
		} 
	    }
	    else throw new DAOException( "Échec de la suppression de la formation, aucune ligne supprimée de la table." );
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    private Formation trouver( String sql, Object... objets ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Formation formation = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    /*
	     * Préparation de la requête avec les objets passés en arguments
	     * (ici, uniquement un id) et exécution.
	     */
	    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	    resultSet = preparedStatement.executeQuery();
	    /* Parcours de la ligne de données retournée dans le ResultSet */
	    if ( resultSet.next() ) {
		formation = map( resultSet );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return formation;
    }
    
    /**
     * Map.
     *
     * @param resultSet the result set
     * @return the fichier
     * @throws SQLException the SQL exception
     */
    private static Formation map( ResultSet resultSet ) throws SQLException {
	Formation formation= new Formation();
	formation.setId(resultSet.getLong( "id" ));
	formation.setId_resume(resultSet.getLong( "id_resume" ));
	formation.setDomaine(resultSet.getString("domaine"));
	formation.setInstitut(resultSet.getString("institut"));
	formation.setSpecialisation(resultSet.getString("specialisation"));
	formation.setAnnee(resultSet.getObject("annee") != null ? resultSet.getLong("annee") : null);
	formation.setMoyenne(resultSet.getString("moyenne"));
	formation.setDate_debut(resultSet.getString("date_debut"));
	formation.setDate_fin(resultSet.getString("date_fin"));         
	return formation;
    }
    
}
