package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.SystemesActivites;

/**
 * 
 * @author donnang
 *
 */
public interface SystemesActivitesDAO {
	
	/**
	 * creer
	 * @param systemesActivites
	 * @throws DAOException
	 */
	public void creer(SystemesActivites systemesActivites) throws DAOException;
	/**
	 * 
	 * @param systemesActivites
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	public SystemesActivites modifier(SystemesActivites systemesActivites) throws DAOException;
	/**
	 * 
	 * @return
	 * @throws DAOException
	 */
	public List<SystemesActivites> lister(Long idEvaluation) throws DAOException;
	/**
	 * 
	 * @param systemesActivites
	 * @throws DAOException
	 */
	public void supprimer(SystemesActivites systemesActivites) throws DAOException;
}


