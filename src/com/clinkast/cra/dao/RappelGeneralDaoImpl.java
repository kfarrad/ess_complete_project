package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.RappelGeneral;

public class RappelGeneralDaoImpl implements RappelGeneralDao {

	/** The Constant SQL_UPDATE. */
	private static final String SQL_UPDATE			= "UPDATE rappel_general SET  status=? WHERE id = ?";
	/** The Constant SQL_SELECT. */
	private static final String SQL_SELECT			= "SELECT *  FROM rappel_general";
	/** The Constant SQL_SELECT_BY_ID. */
	private static final String SQL_SELECT_BY_ID	= "SELECT *  FROM rappel_general WHERE id = ?";
	/** The dao factory. */
	private DAOFactory          daoFactory;
	/**
	 * Instantiates a new RappelGeneral dao impl.
	 *
	 * @param daoFactory the dao factory
	 * @return 
	 */
	RappelGeneralDaoImpl( DAOFactory daoFactory ) {
		this.daoFactory = daoFactory;
	}
	/*
	 * Simple méthode utilitaire permettant de faire la correspondance (le
	 * mapping) entre une ligne issue de la table des rappel_general (un ResultSet) et
	 * un bean RappelGeneral.
	 */
	/**
	 * Map.
	 *
	 * @param resultSet the result set
	 * @return the RappelGeneral
	 * @throws SQLException the SQL exception
	 */
	private static RappelGeneral map( ResultSet resultSet ){
		RappelGeneral result = new RappelGeneral();
		try {
			result.setId( resultSet.getInt( "id" ) );
			result.setStatus( resultSet.getInt( "status" ) );
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		

		return result;
	}

	@Override
	public void modifier(RappelGeneral rg, int id){
		// TODO Auto-generated method stub
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			connexion.setAutoCommit(false);                   
			preparedStatement = connexion.prepareStatement(SQL_UPDATE);
			preparedStatement.setInt(1,1);
			preparedStatement.setInt(2,id);
			preparedStatement.addBatch();
			for (int i=0; i < 12; i++) 
				if(i!=id)
				{
					preparedStatement.setInt(1,0);
					preparedStatement.setInt(2,i);
					preparedStatement.addBatch();                                   
				}
			int [] numUpdates=preparedStatement.executeBatch();             
			for (int i=0; i < numUpdates.length; i++) {            
				if (numUpdates[i] == -2)
					System.out.println("Execution " + i + 
							": unknown number of rows updated");
				else
					System.out.println("Execution " + i + "successful: " + numUpdates[i] + " rows updated");
			}
			connexion.commit(); 
		} catch ( BatchUpdateException  e ) {
			System.out.println("erreur BatchUpdateException "+ e);
		} 
		catch ( SQLException  e ) {
			System.out.println("erreur SQLException "+ e);
		}finally {
			fermeturesSilencieuses( preparedStatement, connexion );
		}
	}
	/* Implémentation de la méthode définie dans l'interface RappelGeneralDao */
	/* (non-Javadoc)
	 * @see com.clinkast.cra.dao.RappelGeneralDao#lister()
	 */
	@Override
	public List<RappelGeneral> lister() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<RappelGeneral> result = new ArrayList<RappelGeneral>();

		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement( SQL_SELECT );
			resultSet = preparedStatement.executeQuery();
			while ( resultSet.next() ) {
				result.add( map( resultSet ) );
			}
		} catch ( SQLException e ) {
			throw new DAOException( e );
		} finally {
			fermeturesSilencieuses( resultSet, preparedStatement, connection );
		}

		return result;
	}
	 /*
     * Méthode générique utilisée pour retourner un client depuis la base de
     * données, correspondant à la requête SQL donnée prenant en paramètres les
     * objets passés en argument.
     */
    /**
	 * Trouver.
	 *
	 * @param sql the sql
	 * @param objets the objets
	 * @return the RappelGeneral
	 * @throws DAOException the DAO exception
	 */
	private RappelGeneral trouver( String sql, Object... objets ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        RappelGeneral client = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de données retournée dans le ResultSet */
            if ( resultSet.next() ) {
                client = map( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return client;
    }
	public RappelGeneral trouver(long id )
    {
    	return trouver( SQL_SELECT_BY_ID,  id  );
    }

}
