package com.clinkast.cra.dao;

// TODO: Auto-generated Javadoc
/**
 * The Class DAOConfigurationException.
 */
public class DAOConfigurationException extends RuntimeException {
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9047981508272212551L;

	/*
     * Constructeurs
     */
    /**
	 * Instantiates a new DAO configuration exception.
	 *
	 * @param message the message
	 */
	public DAOConfigurationException( String message ) {
        super( message );
    }

    /**
     * Instantiates a new DAO configuration exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public DAOConfigurationException( String message, Throwable cause ) {
        super( message, cause );
    }

    /**
     * Instantiates a new DAO configuration exception.
     *
     * @param cause the cause
     */
    public DAOConfigurationException( Throwable cause ) {
        super( cause );
    }
}