package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Rapports;

public class RapportsDAOImpl implements RapportsDAO {
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO rapports ( " +
			"competences_competences_id, " +
			"auto_evaluations_auto_evaluations_id, " +
			"rapports_niveau, " +
			"rapports_commentaire) VALUES (?, ?, ?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE rapports SET  " +
			"rapports_niveau=?, " +
			"rapports_commentaire=? " +
			"WHERE competences_competences_id = ? AND " +
			"auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_DELETE
	 */
	private static final String SQL_DELETE = "DELETE FROM rapports  WHERE competences_competences_id = ? AND auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_SELECT_ALL
	 */
	private static final String SQL_SELECT_ALL = "SELECT * FROM rapports WHERE auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM rapports WHERE competences_competences_id = ? AND auto_evaluations_auto_evaluations_id = ?";
    
	private DAOFactory daoFactory;
	public RapportsDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}

	@Override
	public void creer(Rapports rapports) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, rapports.getCompetences().getCompetencesId(), rapports.getAutoEvaluations().getAutoEvaluationsId(), rapports.getRapportsNiveau(), rapports.getRapportsCommentaire());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une synthese de l'activité, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            		       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
	}

	@Override
	public Rapports modifier(Rapports rapports) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		rapports.getRapportsNiveau(), 
            		rapports.getRapportsCommentaire(),
            		rapports.getCompetences().getCompetencesId(), 
            		rapports.getAutoEvaluations().getAutoEvaluationsId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
		return rapports;
	}
	
	@Override
	public Rapports trouver_By_ID(Long idCompetences, Long idEvaluations) throws DAOException{
		Rapports rapports = null;  
        try {
            rapports = trouve(SQL_SELECT_BY_ID, idCompetences, idEvaluations);
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return rapports;
	}
	@Override
	public Rapports trouver_By_ALL(Long idEvaluations) throws DAOException{
		Rapports rapports = null;  
        try {
        	rapports = trouve(SQL_SELECT_ALL, idEvaluations);
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
       
		return rapports;
	}
	public void supprimer(Rapports rapports)throws DAOException{
		/***********************************************************************
		 *   FONCTION A MODIFIER
		 * ********************************************************************/
		
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
        try {
	            connexion = daoFactory.getConnection();
	            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, rapports.getAutoEvaluations().getAutoEvaluationsId(), rapports.getCompetences().getCompetencesId());
	            int statut = preparedStatement.executeUpdate();
	            if ( statut == 0 ) {
	                throw new DAOException( "Échec de la suppression du rapport , aucune ligne supprimée de la table." );
	            } else {
	            	rapports = null;
	            }
	     } catch ( SQLException e ) {
	         throw new DAOException( e );
	     } finally {
	    	 fermeturesSilencieuses( preparedStatement, connexion );
	     }
	}
	@Override
	public List<Rapports> lister(Long idEvaluation) throws DAOException {
		// TODO Auto-generated method stub
		List<Rapports> rapports = new ArrayList<Rapports>();
		
		try {
        	rapports = retrouve(SQL_SELECT_ALL, idEvaluation);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return rapports;
	}
	private Rapports map(ResultSet resultSet) throws SQLException{
		// TODO Auto-generated method stub
		//AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		CompetencesDAOImpl competencesDAOImpl = daoFactory.getCompetencesDAOImpl();
		Rapports rapports = new Rapports();
		//rapports.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));	    	       
		rapports.setCompetences(competencesDAOImpl.trouver(resultSet.getLong("competences_competences_id")));
		rapports.setRapportsNiveau(resultSet.getString("rapports_niveau"));
		rapports.setRapportsCommentaire(resultSet.getString("rapports_commentaire"));
		
        return rapports;
	}
	
	private Rapports map2(ResultSet resultSet) throws SQLException{
		// TODO Auto-generated method stub
		//AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		//CompetencesDAOImpl competencesDAOImpl = daoFactory.getCompetencesDAOImpl();
		Rapports rapports = new Rapports();
		//rapports.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));	    	       
		//rapports.setCompetences(competencesDAOImpl.trouver(resultSet.getLong("competences_competences_id")));
		rapports.setRapportsNiveau(resultSet.getString("rapports_niveau"));
		rapports.setRapportsCommentaire(resultSet.getString("rapports_commentaire"));
		
		
        return rapports;
	}

	private List<Rapports> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<Rapports> rapports = new ArrayList<Rapports>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	rapports.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return rapports;
	}
	private Rapports trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        Rapports rapports = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	rapports = map2( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return rapports;
	}

}
