package com.clinkast.cra.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating DAO objects.
 */
public class DAOFactory {
        
        /** The Constant FICHIER_PROPERTIES. */
        //private static final String FICHIER_PROPERTIES       = "/com/clinkast/cra/dao/dao.properties";
        private static String FICHIER_PROPERTIES       = null;
        /** The Constant PROPERTY_URL. */
        private static final String PROPERTY_URL             = "url";
        
        /** The Constant PROPERTY_DRIVER. */
        private static final String PROPERTY_DRIVER          = "driver";
        
        /** The Constant PROPERTY_NOM_UTILISATEUR. */
        private static final String PROPERTY_NOM_UTILISATEUR = "nomutilisateur";
        
        /** The Constant PROPERTY_MOT_DE_PASSE. */
        private static final String PROPERTY_MOT_DE_PASSE    = "motdepasse";
        
        /* package */
        /** The connection pool. */
        private static BoneCP                      connectionPool           = null;
        
        /* package *//**
         * Instantiates a new DAO factory.
         *
         * @param connectionPool the connection pool
         */
        DAOFactory( BoneCP connectionPool ) {
                DAOFactory.connectionPool = connectionPool;
        }
        
        
        /**
         * Gets the connection pool.
         *
         * @return the connection pool
         */
        public static BoneCP getConnectionPool() {
                return connectionPool;
        }
        /*
         * Méthode chargée de récupérer les informations de connexion à la base de
         * données, charger le driver JDBC et retourner une instance de la Factory
         */
        /**
         * Gets the single instance of DAOFactory.
         *
         * @return single instance of DAOFactory
         * @throws DAOConfigurationException the DAO configuration exception
         */
        public static DAOFactory getInstance() throws DAOConfigurationException {
                Properties properties = new Properties();
                String url;
                String driver;
                String nomUtilisateur;
                String motDePasse;
                BoneCP connectionPool = null;
                
                String chemin = System.getProperty("cra.confpath");
                
                FICHIER_PROPERTIES = chemin  + File.separator + "cra.properties";

                /* ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream fichierProperties = classLoader.getResourceAsStream( FICHIER_PROPERTIES );
        if ( fichierProperties == null ) {
            throw new DAOConfigurationException( "Le fichier properties " + FICHIER_PROPERTIES + " est introuvable." );
        }*/
                
                try {
                        
                        InputStream fichierProperties = new FileInputStream(new File(FICHIER_PROPERTIES));
                        properties.load( fichierProperties );        
                        url = properties.getProperty( PROPERTY_URL );
                        driver = properties.getProperty( PROPERTY_DRIVER );
                        nomUtilisateur = properties.getProperty( PROPERTY_NOM_UTILISATEUR );
                        motDePasse = properties.getProperty( PROPERTY_MOT_DE_PASSE );
                } catch ( FileNotFoundException e ) {
                        throw new DAOConfigurationException( "Le fichier properties " + FICHIER_PROPERTIES + " est introuvable.", e );
                } catch ( IOException e ) {
                        throw new DAOConfigurationException( "Impossible de charger le fichier properties " + FICHIER_PROPERTIES, e );
                }
                
                try {
                        Class.forName( driver );
                } catch ( ClassNotFoundException e ) {
                        throw new DAOConfigurationException( "Le driver est introuvable dans le classpath.", e );
                }
                
                try {
                        /*
                         * Création d'une configuration de pool de connexions via l'objet
                         * BoneCPConfig et les différents setters associés.
                         */
                        BoneCPConfig config = new BoneCPConfig();
                        /* Mise en place de l'URL, du nom et du mot de passe */
                        config.setJdbcUrl( url );
                        config.setUsername( nomUtilisateur );
                        config.setPassword( motDePasse );
                        /* Paramétrage de la taille du pool */
                        config.setMinConnectionsPerPartition( 5 );
                        config.setMaxConnectionsPerPartition( 10 );
                        config.setPartitionCount( 2 );
                        /* Création du pool à partir de la configuration, via l'objet BoneCP */
                        connectionPool = new BoneCP( config );
                } catch ( SQLException e ) {
                        e.printStackTrace();
                        throw new DAOConfigurationException( "Erreur de configuration du pool de connexions.", e );
                }
                /*
                 * Enregistrement du pool créé dans une variable d'instance via un appel
                 * au constructeur de DAOFactory
                 */
                DAOFactory instance = new DAOFactory( connectionPool );
                return instance;
        }
        
        /* Méthode chargée de fournir une connexion à la base de données */
        /* package *//**
         * Gets the connection.
         *
         * @return the connection
         * @throws SQLException the SQL exception
         */
        public Connection getConnection() throws SQLException {
                return connectionPool.getConnection();
        }
        
        
        /**
         * Shutdown conn pool.
         */
        public static void shutdownConnPool() {
                
                try {
                        BoneCP connectionPool = DAOFactory.getConnectionPool();
                        System.out.println("contextDestroyed....");
                        if (connectionPool != null) {
                                connectionPool.shutdown(); //this method must be called only once when the application stops.
                                //you don't need to call it every time when you get a connection from the Connection Pool
                                System.out.println("contextDestroyed.....Connection Pooling shut downed!");
                        }
                        
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }
        
        /*
         * Méthodes de récupération de l'implémentation des différents DAO (un seul
         * pour le moment)
         */
        /**
         * Gets the user profil dao.
         *
         * @return the user profil dao
         */
        public UserProfilDaoImpl getUserProfilDao() {
                return new UserProfilDaoImpl( this );
        }
        
        /**
         * Gets the user dao.
         *
         * @return the user dao
         */
        public UserDaoImpl getUserDao() {
                return new UserDaoImpl( this );
        }
        
        /**
         * Gets the calendar dao.
         *
         * @return the calendar dao
         */
        public CalendarDaoImpl getCalendarDao() {
                // TODO Auto-generated method stub
                return new CalendarDaoImpl( this );
        }
        
        /**
         * Gets the projet dao.
         *
         * @return the projet dao
         */
        public ProjetDaoImpl getProjetDao() {
                // TODO Auto-generated method stub
                return new ProjetDaoImpl( this );
        }
        
        /**
         * Gets the client dao.
         *
         * @return the client dao
         */
        public ClientDaoImpl getClientDao() {
                // TODO Auto-generated method stub
                return new ClientDaoImpl( this );
        }
        
        
        /**
         * Gets the fichier dao.
         *
         * @return the fichier dao
         */
        public FichierDaoImpl getFichierDao() {
                // TODO Auto-generated method stub
                return new FichierDaoImpl( this );
        }


        /**
         * Gets the experience dao.
         *
         * @return the experience dao
         */
        public ExperienceDaoImpl getExperienceDao() {
                // TODO Auto-generated method stub
                return new ExperienceDaoImpl(this);
        }


        public FormationDaoImpl getFormationDao() {
                // TODO Auto-generated method stub
                return new FormationDaoImpl(this);
        }


        public DomaineDaoImpl getDomaineDao() {
                // TODO Auto-generated method stub
                return new DomaineDaoImpl(this);
        }


        public CompetenceDaoImpl getCompetenceDao() {
                // TODO Auto-generated method stub
                return new CompetenceDaoImpl(this);
        }


        public LangueDaoImpl getLangueDao() {
                // TODO Auto-generated method stub
                return new LangueDaoImpl(this);
        }


        public TitreCVDaoImpl getTitreCVDao() {
                // TODO Auto-generated method stub
                return new TitreCVDaoImpl(this);
        }


        public ResumeDaoImpl getResumeDao() {
                // TODO Auto-generated method stub
                return new ResumeDaoImpl(this);
        }
        
        public AutoEvaluationDAOImpl getAutoEvaluationDAOImpl(){
        	return new AutoEvaluationDAOImpl(this);
        }
        
        public SystemesActivitesDAOImpl getSystemesActivitesDAOImpl(){
        	return new SystemesActivitesDAOImpl(this);
        }
        
        public CompetencesDAOImpl getCompetencesDAOImpl(){
        	return new CompetencesDAOImpl(this);
        }
        
        
        public DatesDAOImpl getDatesDAOImpl(){
        	return new DatesDAOImpl(this);
        }
        
        public ObjectifsDAOImpl getObjectifsDAOImpl(){
        	return new ObjectifsDAOImpl(this);
        }
        
        public FormationsSuiviesDAOImpl getFormationsSuiviesDAOImpl(){
        	return new FormationsSuiviesDAOImpl(this);
        }
        
        public RapportsDAOImpl getRapportsDAOImpl(){
        	return new RapportsDAOImpl(this);
        }
        
        public GestionEvaluationsDAOImpl getGestionEvaluationsDAOImpl(){
        	return new GestionEvaluationsDAOImpl(this);
        }
        
        public FormationsDemandesDAOImp getFormationsDemandesDAOImp(){
        	return new FormationsDemandesDAOImp(this);
        }
        
        public GestionCongesDAOImpl getGestionConges(){
        	return new GestionCongesDAOImpl(this);
        }
        
        public CandidatureDaoImpl getCandidature() {
        	return new CandidatureDaoImpl(this);
        }
        
        public ContenuDaoImp getContenu() {
        	return new ContenuDaoImp(this);
        }


		public CongesDaoImpl getCongesDao() {
			return new CongesDaoImpl(this);		
		}
		
		public RappelGeneralDaoImpl getRappelGeneral() {
			return new RappelGeneralDaoImpl(this);		
		}
}