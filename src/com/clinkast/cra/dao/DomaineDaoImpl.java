package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Domaine;

public class DomaineDaoImpl implements DomaineDao{
    /** The Constant SQL_SELECT. */
    private static final String SQL_SELECT          = "SELECT *  FROM domaine";
    
    private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM domaine WHERE id = ?";
    /** The Constant SQL_SELECT_BY_NAME. */
    private static final String SQL_SELECT_BY_NAME  = "SELECT *  FROM domaine WHERE nom = ?";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT          = "INSERT INTO domaine (nom) VALUES (?)";
    
    /** The Constant SQL_DELETE. */
    private static final String SQL_DELETE          = "DELETE FROM domaine WHERE id = ?";
    
    /** The Constant SQL_UPDATE. */
    private static final String SQL_UPDATE          = "UPDATE domaine SET nom = ? WHERE id ?";
    
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    /**
     * Instantiates a new domaine dao impl.
     *
     * @param daoFactory the dao factory
     */
    DomaineDaoImpl( DAOFactory daoFactory ) {
	this.daoFactory = daoFactory;
    }
    
    
    /**
     * Trouver.
     *
     * @param id the id
     * @return the domaine
     */
    public Domaine trouver(Long id )
    {
	return trouver( SQL_SELECT_BY_ID,  id );
    }
    
    /**
     * Trouver.
     *
     * @param nom the nom
     * @return the domaine
     */
    public Domaine trouver(String nom )
    {
	return trouver( SQL_SELECT_BY_NAME,  nom );
    }
    
    
    @Override
    public void creer(Domaine domaine) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
	    connexion = daoFactory.getConnection();
	    
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
		    domaine.getNom());
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la création du Domaine, aucune ligne ajoutée dans la table." );
	    }
	    valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	    
	    if ( valeursAutoGenerees.next() ) {             
		domaine.setId( valeursAutoGenerees.getLong( 1 ) );
		
	    } else {
		throw new DAOException( "Échec de la création du Domaine en base, aucun ID auto-généré retourné." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	}
	
    }
    
    
    @Override
    public Domaine modifier(Domaine domaine, Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
		    domaine.getNom(), id );
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification  du nom du domaine, aucune ligne modifiée dans la table." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
	return domaine;
    }
    
    
    @Override
    public void supprimer(Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	
	try {
	    connexion = daoFactory.getConnection();
	    Domaine monDomaine = this.trouver(id);
	    if(monDomaine!=null){
		preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, id);
		int statut = preparedStatement.executeUpdate();
		if ( statut == 0 ) {
		    throw new DAOException( "Échec de la suppression du profil, aucune ligne supprimée de la table." );
		}
	    }
	    else throw new DAOException( "Échec de la suppression du profil, aucune ligne supprimée de la table." );
		
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    
    @Override
    public List<Domaine> lister() throws DAOException {
	// TODO Auto-generated method stub
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Domaine> domaines = new ArrayList<Domaine>();
	
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = connection.prepareStatement( SQL_SELECT );
	    resultSet = preparedStatement.executeQuery();
	    while ( resultSet.next() ) {
		domaines.add( map( resultSet ) );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connection );
	}
	
	return domaines;
    }
    
    private Domaine trouver( String sql, Object... objets ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Domaine domaine = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    /*
	     * Préparation de la requête avec les objets passés en arguments
	     * (ici, uniquement un id) et exécution.
	     */
	    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	    resultSet = preparedStatement.executeQuery();
	    /* Parcours de la ligne de données retournée dans le ResultSet */
	    if ( resultSet.next() ) {
		domaine = map( resultSet );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return domaine;
    }
    
    /**
     * Map.
     *
     * @param resultSet the result set
     * @return the domaine
     * @throws SQLException the SQL exception
     */
    private static Domaine map( ResultSet resultSet ) throws SQLException {
	Domaine domaine= new Domaine();
	domaine.setId(resultSet.getLong( "id" ));
	domaine.setNom(resultSet.getString("nom"));
	
	return domaine;
    }
    
}
