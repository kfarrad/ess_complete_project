package com.clinkast.cra.dao;



import java.util.List;

import com.clinkast.cra.beans.Projet;




// TODO: Auto-generated Javadoc
/**
 * The Interface ProjetDao.
 */
public interface ProjetDao {
	 
 	/**
 	 * Creer.
 	 *
 	 * @param projet the projet
 	 * @param id_client the id_client
 	 * @throws DAOException the DAO exception
 	 */
 	void creer( Projet projet, Long id_client ) throws DAOException;  
	 /**
 	 * Modifier.
 	 *
 	 * @param projet the projet
 	 * @param id the id
 	 * @return the projet
 	 * @throws DAOException the DAO exception
 	 */
 	Projet modifier( Projet projet, Long id) throws DAOException;

	 /**
 	 * Supprimer.
 	 *
 	 * @param projet the projet
 	 * @throws DAOException the DAO exception
 	 */
 	void supprimer( Projet projet ) throws DAOException;
	 
	 /**
 	 * Lister.
 	 *
 	 * @return the list
 	 * @throws DAOException the DAO exception
 	 */
 	List<Projet> lister() throws DAOException;
 	
 	/**
 	 * Lister.
 	 *
 	 * @return the list
 	 * @throws DAOException the DAO exception
 	 */
 	List<Projet> listerProjetNonFactures() throws DAOException;
 	
 	/**
 	 * Lister les congés.
 	 *
 	 * @return the list
 	 * @throws DAOException the DAO exception
 	 */
	List<Projet> listerConges() throws DAOException;

}
