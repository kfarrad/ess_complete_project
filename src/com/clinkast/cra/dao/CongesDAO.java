package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Conges;

public interface CongesDAO {
	
	/**
	 * Creer un conges.
	 *
	 * @param conges the conges
	 * @throws DAOException the DAO exception
	 */
	void  creer( Conges conges ) throws DAOException;

	/**
	 * Modifier un conges.
	 *
	 * @param conges the conges
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 * @param annee the year
	 * @param mois the month
	 * @return the conges
	 * @throws DAOException the DAO exception
	 */
	Conges modifier( Long id, Long status, Conges conges) throws DAOException;

	/**
	 * Supprimer un conges.
	 *
	 * @param idConge the conges id
	 * @param isUser 
	 * @throws DAOException the DAO exception
	 */
	void supprimer( Long idConge , boolean isUser) throws DAOException;

	/**
	 * Lister tous les conges.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<Conges> lister() throws DAOException;

	
	float count_jour(Long id_user, Long id_projet, Long annee, Long mois) throws DAOException;

	//List<Conges> listerConges(long id_user, long statut) throws DAOException;

	List<Conges> listerConges(Long statut, Long id_user, Long id_motif, Long annee , Long mois) throws DAOException;
	
	List<Conges> listerConges(Long statut, Long id_user, Long annee , Long mois) throws DAOException;

	List<Conges> listerCongesValide(String periode) throws DAOException;
	
	List<Conges> listerConges(Long id, int mois, int annee);
	

}
