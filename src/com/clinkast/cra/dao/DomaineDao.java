package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Domaine;

// TODO: Auto-generated Javadoc
/**
 * The Interface DomaineDao.
 */
public interface DomaineDao {
        
        /**
         * Creer.
         *
         * @param domaine the domaine
         * @throws DAOException the DAO exception
         */
        void creer( Domaine domaine ) throws DAOException;
        
        /**
         * Modifier.
         *
         * @param domaine the domaine
         * @param id the id
         * @return the domaine
         * @throws DAOException the DAO exception
         */
        Domaine modifier( Domaine domaine, Long id) throws DAOException;
        
        /**
         * Supprimer.
         *
         * @param id the id
         * @throws DAOException the DAO exception
         */
        void supprimer( Long id) throws DAOException;
        
        /**
         * Lister.
         *
         * @return the list
         * @throws DAOException the DAO exception
         */
        List<Domaine> lister() throws DAOException;
        
}
