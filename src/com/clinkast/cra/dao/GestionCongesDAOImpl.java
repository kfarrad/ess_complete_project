package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import com.clinkast.cra.beans.GestionConges;

public class GestionCongesDAOImpl implements GestionCongesDAO {
	
	private static final String SQL_INSERT = "INSERT INTO gestion_conges ( user_id, gestion_conges_rTTInitial, gestion_conges_rTTAttente, gestion_conges_cPInitial, gestion_conges_cPAttente, gestion_conges_valide, gestion_conges_dateValidation, gestion_conges_historique) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	
	//private static final String SQL_SELECT_BY_ID = "SELECT * FROM gestion_conges WHERE gestion_conges_id = ?";
	
	private static final String SQL_SELECT_BY_USER = "SELECT * FROM gestion_conges WHERE user_id = ?";
	
	private static final String SQL_SELECT_ALL = "SELECT * FROM gestion_conges ORDER BY DESC";
    
	private static final String SQL_UPDATE = "UPDATE gestion_conges SET  gestion_conges_rTTInitial = ?,"
			+ " gestion_conges_rTTAttente = ?,"
			+ " gestion_conges_cPInitial = ?,"
			+ " gestion_conges_cPAttente = ?,"
			+ " gestion_conges_valide = ?,"
			+ " gestion_conges_dateValidation = ?,"
			+ " gestion_conges_historique = ?"
			+ " WHERE gestion_conges_id = ?";
	
	private DAOFactory daoFactory;
	
	public GestionCongesDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}
	@Override
	public void creer(GestionConges gestionConges) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
            		gestionConges.getUser().getId_profil(),
            		gestionConges.getrTTInitial(),
            		gestionConges.getrTTAttente(),
            		gestionConges.getcPInitial(),
            		gestionConges.getcPAttente(),
            		gestionConges.getValide(),
            		new Timestamp(gestionConges.getDateValidation().getMillis()),
            		gestionConges.getHistorique());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'un gestionnaire e congés, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                gestionConges.setId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'un gestionnaire de congés, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }


	}

	@Override
	public void modifier(GestionConges gestionConges) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		gestionConges.getrTTInitial(), gestionConges.getrTTAttente(),
            		gestionConges.getcPInitial(), gestionConges.getcPAttente(), gestionConges.getValide(),
            		new Timestamp(gestionConges.getDateValidation().getMillis()),
            		gestionConges.getHistorique(),
            		gestionConges.getId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
           
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }

	}

	@Override
	public GestionConges trouver_by_user(Long id) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        GestionConges gestionConges = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_BY_USER, false, id );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	gestionConges = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return gestionConges;
	}

	private GestionConges map(ResultSet resultSet) throws SQLException {
		// TODO Auto-generated method stub
		
		GestionConges gestionConges = new GestionConges();
		
		gestionConges.setId(resultSet.getLong("gestion_conges_id"));
		gestionConges.setUser(daoFactory.getUserDao().trouver_ID(resultSet.getLong("user_id")));
		gestionConges.setrTTInitial(resultSet.getFloat("gestion_conges_rTTInitial"));
		gestionConges.setrTTAttente(resultSet.getFloat("gestion_conges_rTTAttente"));
		gestionConges.setcPInitial(resultSet.getFloat("gestion_conges_cPInitial"));
		gestionConges.setcPAttente(resultSet.getFloat("gestion_conges_cPAttente"));
		gestionConges.setDateValidation(new DateTime(resultSet.getTimestamp("gestion_conges_dateValidation")));
		gestionConges.setHistorique(resultSet.getString("gestion_conges_historique"));
		gestionConges.setValide(resultSet.getInt("gestion_conges_valide"));
		
		return gestionConges;
	}
	@Override
	public List<GestionConges> lister() throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<GestionConges> listGestionConges = new ArrayList<GestionConges>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_ALL, false );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	listGestionConges.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return listGestionConges;
	}

}
