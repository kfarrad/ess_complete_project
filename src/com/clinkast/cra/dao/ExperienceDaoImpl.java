package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Experience;

// TODO: Auto-generated Javadoc
/**
 * The Class ExperienceDaoImpl.
 */
public class ExperienceDaoImpl implements ExperienceDao{
    
    /** The Constant SQL_SELECT. */
    private static final String SQL_SELECT          = "SELECT *  FROM experience WHERE id_resume = ? ";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT          = "INSERT INTO experience (id_resume, entreprise, titre_poste, date_debut, date_fin, description) VALUES (?, ?, ?, ?, ?, ?)";
    
    /** The Constant SQL_UPDATE. */
    private static final String SQL_UPDATE          = "UPDATE experience SET  entreprise=?, titre_poste=?, date_debut=?, date_fin=?, description=?   WHERE id = ?";
    
    /** The Constant SQL_DELETE. */
    private static final String SQL_DELETE          = "DELETE FROM experience WHERE id = ?";
    
    /** The Constant SQL_SELECT_BY_ID. */
    private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM experience WHERE id=? ";
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    /**
     * Instantiates a new projet dao impl.
     *
     * @param daoFactory the dao factory
     */
    public ExperienceDaoImpl( DAOFactory daoFactory ) {
	// TODO Auto-generated constructor stub
	this.daoFactory = daoFactory;
    } 
    
    /**
     * Trouver.
     *
     * @param id the id
     * @return the projet
     */
    public Experience trouver(Long id )
    {
	return trouver( SQL_SELECT_BY_ID,  id  );
    }
    
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.ExperienceDao#creer(com.clinkast.cra.beans.Experience, java.lang.Long)
     */
    @Override
    public void creer(Experience workExp) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
	    connexion = daoFactory.getConnection();
	    
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
		    workExp.getId_resume(), workExp.getEntreprise(), workExp.getTitre_poste(),
		    workExp.getDate_debut(), workExp.getDate_fin(), workExp.getDescription());
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la création de l'experience, aucune ligne ajoutée dans la table." );
	    }
	    valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	    
	    if ( valeursAutoGenerees.next() ) {             
		workExp.setId( valeursAutoGenerees.getLong( 1 ) );
		
	    } else {
		throw new DAOException( "Échec de la création de l'experience en base, aucun ID auto-généré retourné." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	}
    }
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.ExperienceDao#modifier(com.clinkast.cra.beans.Experience, java.lang.Long)
     */
    @Override
    public Experience modifier(Experience workExp, Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
		    workExp.getEntreprise(), workExp.getTitre_poste(),
		    workExp.getDate_debut(), workExp.getDate_fin(), workExp.getDescription(), id );
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification  de l'experience, aucune ligne modifiée dans la table." );
	    }
	    workExp.setId( id);
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
	return workExp;
    }
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.ExperienceDao#Supprimer(java.lang.Long)
     */
    @Override
    public void supprimer(Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	
	try {
	    connexion = daoFactory.getConnection();
	    Experience monProjet = this.trouver(id);
	    if(monProjet!=null){
		preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, false, id);
		int statut = preparedStatement.executeUpdate();
		if ( statut == 0 ) {
		    throw new DAOException( "Échec de la suppression du projet, aucune ligne supprimée de la table." );
		} 
	    } else throw new DAOException( "Échec de la suppression du projet, aucune ligne supprimée de la table." );
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.ExperienceDao#lister()
     */
    @Override
    public List<Experience> lister(Long id_resume) throws DAOException {
	// TODO Auto-generated method stub
	List<Experience> workExps = new ArrayList<Experience>();
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT, false, id_resume );
	    resultSet = preparedStatement.executeQuery();
	    while ( resultSet.next() ) {
		workExps.add( map( resultSet ) );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connection );
	}
	
	
	return workExps;
    }
    
    /**
     * Trouver.
     *
     * @param sql the sql
     * @param objets the objets
     * @return the projet
     * @throws DAOException the DAO exception
     */
    private Experience trouver( String sql, Object... objets ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Experience workExp = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    /*
	     * Préparation de la requête avec les objets passés en arguments
	     * (ici, uniquement un id) et exécution.
	     */
	    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	    resultSet = preparedStatement.executeQuery();
	    /* Parcours de la ligne de données retournée dans le ResultSet */
	    if ( resultSet.next() ) {
		workExp = map( resultSet );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return workExp;
    }
    
    /*
     * Simple méthode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des clients (un ResultSet) et
     * un bean Client.
     */
    /**
     * Map.
     *
     * @param resultSet the result set
     * @return the projet
     * @throws SQLException the SQL exception
     */
    private static Experience map( ResultSet resultSet ) throws SQLException {
	Experience workExp = new Experience();
	workExp.setId( resultSet.getLong( "id" ) );      
	workExp.setId_resume(resultSet.getLong("id_resume"));
	workExp.setEntreprise( resultSet.getString( "entreprise" ) );
	workExp.setTitre_poste( resultSet.getString( "titre_poste" ) );
	workExp.setDate_debut( resultSet.getString( "date_debut" ) );
	workExp.setDate_fin( resultSet.getString( "date_fin" ) );
	workExp.setDescription( resultSet.getString( "description" ) );
	return workExp;
    }
}
