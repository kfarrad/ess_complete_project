package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Resume;

public interface ResumeDao {
        public void creer(Resume resume) throws DAOException;
        
        public void modifier(Resume resume) throws DAOException;
        
        public Resume supprimer(Long id) throws DAOException;
        
        public List<Resume> lister();
}
