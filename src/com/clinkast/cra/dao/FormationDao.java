package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Formation;

// TODO: Auto-generated Javadoc
/**
 * The Interface FormationDao.
 */
public interface FormationDao {
        
        /**
         * Creer.
         *
         * @param formation the formation
         * @throws DAOException the DAO exception
         */
        void creer( Formation formation ) throws DAOException;
        
        /**
         * Modifier.
         *
         * @param formation the formation
         * @param id the id
         * @return the formation
         * @throws DAOException the DAO exception
         */
        Formation modifier( Formation formation, Long id) throws DAOException;
        
        /**
         * Supprimer.
         *
         * @param formation the formation
         * @throws DAOException the DAO exception
         */
        void supprimer( Long id) throws DAOException;
        
        /**
         * Lister.
         *
         * @return the list
         * @throws DAOException the DAO exception
         */
        List<Formation> lister(Long id_resume) throws DAOException;
}
