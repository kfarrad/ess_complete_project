package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Fichier;

// TODO: Auto-generated Javadoc
/**
 * The Class FichierDaoImpl.
 */
public class FichierDaoImpl implements FichierDao{
        
        /** The Constant SQL_SELECT. */
        private static final String SQL_SELECT          = "SELECT *  FROM fichier WHERE id = ?";
        
        /** The Constant SQL_SELECT_BY_NAME. */
        private static final String SQL_SELECT_BY_NAME  = "SELECT *  FROM fichier WHERE nom = ?";
        
        /** The Constant SQL_INSERT. */
        private static final String SQL_INSERT          = "INSERT INTO fichier (nom, description) VALUES (?, ?)";
        
        /** The Constant SQL_DELETE. */
        private static final String SQL_DELETE          = "DELETE FROM fichier WHERE id = ?";
        
        /** The Constant SQL_UPDATE. */
        private static final String SQL_UPDATE          = "UPDATE fichier SET nom = ?, description= ? WHERE id ?";
        
        /** The dao factory. */
        private DAOFactory          daoFactory;
        
        /**
         * Instantiates a new fichier dao impl.
         *
         * @param daoFactory the dao factory
         */
        FichierDaoImpl( DAOFactory daoFactory ) {
                this.daoFactory = daoFactory;
        }
        
        
        /**
         * Trouver.
         *
         * @param id the id
         * @return the fichier
         */
        public Fichier trouver(Long id )
        {
            return trouver( SQL_SELECT,  id );
        }
        
        /**
         * Trouver.
         *
         * @param nom the nom
         * @return the fichier
         */
        public Fichier trouver(String nom )
        {
            return trouver( SQL_SELECT_BY_NAME,  nom );
        }
        
        
        /* (non-Javadoc)
         * @see com.clinkast.cra.dao.FichierDao#creer(com.clinkast.cra.beans.Fichier)
         */
        @Override
        public void creer( Fichier fichier) throws DAOException {
            Connection connexion = null;
            PreparedStatement preparedStatement = null;
            ResultSet valeursAutoGenerees = null;
            try {
                connexion = daoFactory.getConnection();

                preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
                         fichier.getNom(), fichier.getDescription() );
                int statut = preparedStatement.executeUpdate();

                if ( statut == 0 ) {
                    throw new DAOException( "Échec de la création du fichier, aucune ligne ajoutée dans la table." );
                }
                valeursAutoGenerees = preparedStatement.getGeneratedKeys();
                
                if ( valeursAutoGenerees.next() ) {             
                    fichier.setId( valeursAutoGenerees.getLong( 1 ) );

                } else {
                    throw new DAOException( "Échec de la création du fichier en base, aucun ID auto-généré retourné." );
                }

            } catch ( SQLException e ) {
                throw new DAOException( e );
            } finally {
                fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
            }

        }
        
        

        /* (non-Javadoc)
         * @see com.clinkast.cra.dao.FichierDao#modifier(com.clinkast.cra.beans.Fichier, java.lang.Long)
         */
        @Override
        public void modifier(Fichier fichier, Long id) throws DAOException {
                // TODO Auto-generated method stub
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                try {
                    connexion = daoFactory.getConnection();
                    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
                                    fichier.getNom(), fichier.getDescription(), id );

                    int statut = preparedStatement.executeUpdate();

                    if ( statut == 0 ) {
                        throw new DAOException( "Échec de la modification  du nom du projet, aucune ligne modifiée dans la table." );
                    }

                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( preparedStatement, connexion );
                }

                
        }


        /* (non-Javadoc)
         * @see com.clinkast.cra.dao.FichierDao#supprimer(com.clinkast.cra.beans.Fichier)
         */
        @Override
        public void supprimer(Fichier fichier) throws DAOException {
                // TODO Auto-generated method stub
                Connection connexion = null;
                PreparedStatement preparedStatement = null;

                try {
                    connexion = daoFactory.getConnection();
                    Fichier monFichier = this.trouver(fichier.getId());
                    if(monFichier!=null){
                        preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, false, fichier.getId());
                    int statut = preparedStatement.executeUpdate();
                    if ( statut == 0 ) {
                        throw new DAOException( "Échec de la suppression du projet, aucune ligne supprimée de la table." );
                    } else {
                            fichier.setId( null );
                    }
                }} catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( preparedStatement, connexion );
                }
        }


        /* (non-Javadoc)
         * @see com.clinkast.cra.dao.FichierDao#lister()
         */
        @Override
        public List<Fichier> lister() throws DAOException {
                // TODO Auto-generated method stub
                Connection connection = null;
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                List<Fichier> fichiers = new ArrayList<Fichier>();

                try {
                    connection = daoFactory.getConnection();
                    preparedStatement = connection.prepareStatement( SQL_SELECT );
                    resultSet = preparedStatement.executeQuery();
                    while ( resultSet.next() ) {
                            fichiers.add( map( resultSet ) );
                    }
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( resultSet, preparedStatement, connection );
                }

                return fichiers;
        }
        

        /**
         * Trouver.
         *
         * @param sql the sql
         * @param objets the objets
         * @return the fichier
         * @throws DAOException the DAO exception
         */
        private Fichier trouver( String sql, Object... objets ) throws DAOException {
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                Fichier fichier = null;

                try {
                    /* Récupération d'une connexion depuis la Factory */
                    connexion = daoFactory.getConnection();
                    /*
                     * Préparation de la requête avec les objets passés en arguments
                     * (ici, uniquement un id) et exécution.
                     */
                    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
                    resultSet = preparedStatement.executeQuery();
                    /* Parcours de la ligne de données retournée dans le ResultSet */
                    if ( resultSet.next() ) {
                        fichier = map( resultSet );
                    }
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
                }

                return fichier;
            }
        
        /**
         * Map.
         *
         * @param resultSet the result set
         * @return the fichier
         * @throws SQLException the SQL exception
         */
        private static Fichier map( ResultSet resultSet ) throws SQLException {
                Fichier fichier= new Fichier();
                fichier.setId(resultSet.getLong( "id" ));
                fichier.setDescription(resultSet.getString("description"));
                fichier.setNom(resultSet.getString("nom"));
                
                return fichier;
        }

}
