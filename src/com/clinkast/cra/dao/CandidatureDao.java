package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Candidat;

public interface CandidatureDao {

		 
	 	/**
	 	 * Creer la candidature.
	 	 *
	 	 * @param candidature la candidature � cr�er 
	 	 * @throws DAOException the DAO exception
	 	 */
	 	void creer( Candidat candidature ) throws DAOException;
		    
		 /**
	 	 * Modifier la candidature.
	 	 *
	 	 * @param candidature la candidature � modifier
	 	 * @param id l'id de la candidature � modifier
	 	 * @return la candidatute modifi�e
	 	 * @throws DAOException the DAO exception
	 	 */
	 	Candidat modifier( Candidat candidature, Long id) throws DAOException;

		 /**
	 	 * Supprimer la candidature.
	 	 *
	 	 * @param id 
	 	 * 			id de candidature la candidature � supprimer
	 	 * @throws DAOException 
	 	 * 			the DAO exception
	 	 */
	 	Candidat supprimer( long id ) throws DAOException;
		 
		 /**
	 	 * Lister les candidatures.
	 	 *
	 	 * @return la liste des candidatuure
	 	 * @param debut entier repr�sentant le b�but de la selection
	 	 * @param nombreCadidat entier repr�sentatnt le nombre de candidat � selectionner
	 	 * @throws DAOException the DAO exception
	 	 */
	 	List<Candidat> lister(int debut, int nombreCandidat) throws DAOException;

	 	/**
	 	 * Cherhcer les candidatures dans la base de donn�es et dans les documents (.pdf, .docx).
	 	 *
	 	 * @return la liste des candidatuure correspandantes aux crit�res de la recherche 
	 	 * @param debut entier repr�sentant le b�but de la selection
	 	 * @param nombreCadidat entier repr�sentatnt le nombre de candidat � selectionner
	 	 * @param text Le texte � chercher
	 	 * @param dateFrom date de d�but pour les recherche
	 	 * @param dateTo date de fin pour les recherche
	 	 * @param searchInDocuments param�tre determinant si la recherche sera �ff�ctu�e dans les documents (.pdf, .docx)
	 	 * @throws DAOException the DAO exception
	 	 */
		List<Candidat> chercher(int debut, int nombreCandidat, String text, String dateFrom, String dateTo, String searchInDocuments) throws DAOException;
}
