package com.clinkast.cra.dao;



import java.util.List;

import com.clinkast.cra.beans.Calendrier;




// TODO: Auto-generated Javadoc
/**
 * The Interface CalendarDao.
 */
public interface CalendarDao {
	
	/**
	 * Creer un calendrier.
	 *
	 * @param calendrier the calendrier
	 * @throws DAOException the DAO exception
	 */
	void  creer( Calendrier calendrier ) throws DAOException;

	/**
	 * Modifier un calendrier.
	 *
	 * @param calendrier the calendrier
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 * @param annee the year
	 * @param mois the month
	 * @return the calendrier
	 * @throws DAOException the DAO exception
	 */
	Calendrier modifier( Calendrier calendrier, Long id_user, Long id_projet, Long annee, Long mois) throws DAOException;

	/**
	 * Supprimer un calendrier.
	 *
	 * @param calendrier the calendrier
	 * @throws DAOException the DAO exception
	 */
	void supprimer( Calendrier calendrier ) throws DAOException;

	/**
	 * Lister tous les calendriers.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<Calendrier> lister(Long id_user, Long mois, Long annee ) throws DAOException;
	
	float count_jour(Long id_user, Long id_projet, Long annee, Long mois) throws DAOException;
}
