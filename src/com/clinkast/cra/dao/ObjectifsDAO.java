package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Objectifs;

public interface ObjectifsDAO {
	/**
	 * 
	 * @param objectifs
	 * @throws DAOException
	 */
	public void creer(Objectifs objectifs) throws DAOException;
	/**
	 * 
	 * @param objectifs
	 * @param idObjectifs
	 * @return
	 * @throws DAOException
	 */
	public Objectifs modifier(Objectifs objectifs) throws DAOException;
	/**
	 * 
	 * @param idAutEvaluation
	 * @return
	 * @throws DAOException
	 */
	public List<Objectifs> lister(Long idAutEvaluation) throws DAOException;
	/**
	 * 
	 * @param idObjectifs
	 * @return
	 * @throws DAOException
	 */
	public Objectifs trouver(Long idObjectifs) throws DAOException;
	/**
	 * 
	 * @param objectifs
	 * @throws DAOException
	 */
	public void supprimer(Objectifs objectifs) throws DAOException;
	

}
