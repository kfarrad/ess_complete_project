package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;

import com.clinkast.cra.beans.UserProfil;

// TODO: Auto-generated Javadoc
/**
 * The Class UserProfilDaoImpl.
 */
public class UserProfilDaoImpl implements UserProfilDao {

    /** The Constant SQL_SELECT. */
    private static final String SQL_SELECT        		= "SELECT *  FROM user_profil ORDER BY id";
    
    /** The Constant SQL_SELECT_PAR_ID. */
    private static final String SQL_SELECT_PAR_ID 		= "SELECT *  FROM user_profil WHERE id = ?";
    
    /** The Constant SQL_SELECT_PAR_EMAIL. */
    private static final String SQL_SELECT_PAR_EMAIL	= "SELECT *  FROM user_profil WHERE email = ?";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT        		= "INSERT INTO user_profil (nom, prenom, adresse, telephone, email, dob, numero_ssn, carte_vitale, cni, photo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    /** The Constant SQL_DELETE_PAR_ID. */
    private static final String SQL_DELETE_PAR_ID 		= "DELETE FROM user_profil WHERE id = ?";
    
    /** The Constant SQL_UPDATE_PAR_ID. */
    private static final String SQL_UPDATE_PAR_ID 		= "UPDATE user_profil SET  nom = ?, prenom = ?, adresse = ?, telephone = ?, dob = ?, numero_ssn = ?, carte_vitale = ?, cni = ?, photo = ? WHERE id = ?";
    
    /** The Constant SQL_ACTIVATION_PAR_ID. */
    private static final String SQL_ACTIVATION_PAR_ID 	= "UPDATE user_profil SET  actif = ? WHERE id = ?";
    
    /** The Constant SQL_SELECT. */
    private static final String SQL_SELECT_BY_ORDER_NOM = "SELECT SQL_CALC_FOUND_ROWS user_profil.id, user_profil.nom, user_profil.prenom, user_profil.adresse, user_profil.email , user_profil.telephone, user_profil.dob, user.role, user.actif FROM user_profil,user WHERE user_profil.id = user.id_profil ORDER BY user_profil.nom" ;
    
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    /**Nombre d'enregistrement dans a BDD */
    private int noOfRecords;
     

    /**
     * Instantiates a new user profil dao impl.
     *
     * @param daoFactory the dao factory
     */
    UserProfilDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }

    /* Implémentation de la méthode définie dans l'interface profilDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#trouver(long)
     */
    @Override
    public UserProfil trouver( long id ) throws DAOException {
        return trouver( SQL_SELECT_PAR_ID, id );
    }
    
    
    /**
     * Trouver.
     *
     * @param email the email
     * @return the user profil
     * @throws DAOException the DAO exception
     */
    public UserProfil trouver( String email ) throws DAOException {
        return trouver( SQL_SELECT_PAR_EMAIL, email );
    }
    
 
    
    /* Implémentation de la méthode définie dans l'interface profilDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#activer(long)
     */
    @Override
    public UserProfil activer( long id ) throws DAOException {
    	 Connection connexion = null;
         PreparedStatement preparedStatement = null;
         UserProfil profil = trouver( SQL_SELECT_PAR_ID, id );
         try {
             connexion = daoFactory.getConnection();
             preparedStatement = initialisationRequetePreparee( connexion, SQL_ACTIVATION_PAR_ID, true, 1, id);
             int statut = preparedStatement.executeUpdate();
             if ( statut == 0 ) {
             throw new DAOException( "Échec de l'activation du profil, aucune ligne ajoutée dans la table." );
             }

         }
         catch ( SQLException e ) {
        	 throw new DAOException( e );
        	 }
         finally {
        	 fermeturesSilencieuses(preparedStatement, connexion );
        	 }
         
        return profil;
    }
    
    
   
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#creer(com.clinkast.cra.beans.UserProfil)
     */
    @Override
    public void creer( UserProfil profil ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
              try {
            connexion = daoFactory.getConnection();
            /* 
             * UserProfil utilisateur = trouver(profil.getEmail());
             
             if(utilisateur==null)
            {
            */
            	preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,            
                    profil.getNom(), profil.getPrenom(),
                    profil.getAdresse(), profil.getTelephone(),
                    profil.getEmail(), profil.getDob(), profil.getNumero_ssn(), profil.getCarte_vitale(), profil.getCni(), profil.getPhotoProfil() );
            	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création du profil, aucune ligne ajoutée dans la table." );
                }
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                profil.setId( valeursAutoGenerees.getLong( 1 ) );
	            } else {
	                throw new DAOException( "Échec de la création du profil en base, aucun ID auto-généré retourné." );
	            }
	       /*}
           
             * else
             
            {
            	throw new DAOException( "Échec de la création du profil en base, email déjà existant." );
            }
           */
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
        
    }
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#modifier(com.clinkast.cra.beans.UserProfil, java.lang.Long)
     */
    @Override
	public UserProfil modifier(UserProfil profil, Long id) throws DAOException {
		// TODO Auto-generated method stub
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
                try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PAR_ID, true,
            		profil.getNom(), profil.getPrenom(),
                    profil.getAdresse(), profil.getTelephone(), profil.getDob(), profil.getNumero_ssn(), profil.getCarte_vitale(), profil.getCni(), profil.getPhotoProfil(),
                    id );
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  du profil , aucune ligne modifiée dans la table." );
            }
                     	
                profil.setId(id );
           
           
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
    
		return profil;
	}

    /* Implémentation de la méthode définie dans l'interface profilDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#lister()
     */
    @Override
    public List<UserProfil> lister() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<UserProfil> profils = new ArrayList<UserProfil>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement( SQL_SELECT );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
                profils.add( map( resultSet ) );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }

        return profils;
    }
    
    /* Implémentation de la méthode définie dans l'interface UserprofilDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#lister()
     */
    @Override
    public List<UserProfil> listerParOrdreAlphabetique (int offset, int noOfrecords_) throws DAOException {
    	Connection connexion = null;
    	PreparedStatement statement = null;
    	ResultSet results = null;
    	List<UserProfil> listeProfils = new ArrayList<UserProfil>();
    	UserProfil profil = null;
    	String query = "select SQL_CALC_FOUND_ROWS * FROM user_profil limit " + offset + " , " + noOfrecords_;
    	try {
    		connexion = daoFactory.getConnection();
    		statement = connexion.prepareStatement(query);
    		results = statement.executeQuery();
    		while(results.next()) {
    			profil = new UserProfil();
    			listeProfils.add(this.map(results));
    		}
    		
    		results.close();
    		results = statement.executeQuery("SELECT FOUND_ROWS()");
    		if(results.next())
    				this.noOfRecords = results.getInt(1);
    	}catch (SQLException e) {
    		throw new DAOException( e );
    	} finally {
    		try {
    			if(statement != null)
    				statement.close();
    			 if(connexion != null)
                     connexion.close();
                 } catch (SQLException e) {
                 e.printStackTrace();
             }
    		}
  	
    	return listeProfils;
     }

    /* Implémentation de la méthode définie dans l'interface profilDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.UserProfilDao#supprimer(com.clinkast.cra.beans.UserProfil)
     */
    @Override
    public void supprimer( UserProfil profil ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE_PAR_ID, true, profil.getId() );
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la suppression du profil, aucune ligne supprimée de la table." );
            } else {
                profil.setId( null );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
    }

    /*
     * Méthode générique utilisée pour retourner un profil depuis la base de
     * données, correspondant à la requête SQL donnée prenant en paramètres les
     * objets passés en argument.
     */
    /**
     * Trouver.
     *
     * @param sql the sql
     * @param objets the objets
     * @return the user profil
     * @throws DAOException the DAO exception
     */
    private UserProfil trouver( String sql, Object... objets ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        UserProfil profil = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de données retournée dans le ResultSet */
            if ( resultSet.next() ) {
                profil = map( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return profil;
    }

    /*
     * Simple méthode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des profils (un ResultSet) et
     * un bean profil.
     */
    /**
     * Map.
     *
     * @param resultSet the result set
     * @return the user profil
     * @throws SQLException the SQL exception
     */
    private static UserProfil map( ResultSet resultSet ) throws SQLException {
        UserProfil profil = new UserProfil();
        profil.setId( resultSet.getLong( "id" ) );
        profil.setNom( resultSet.getString( "nom" ) );
        profil.setPrenom( resultSet.getString( "prenom" ) );
        profil.setAdresse( resultSet.getString( "adresse" ) );
        profil.setTelephone( resultSet.getString( "telephone" ) );
        profil.setEmail( resultSet.getString( "email" ) );
        profil.setDob(resultSet.getDate( "dob" ) );
        profil.setNumero_ssn(resultSet.getString( "numero_ssn" ) );
        profil.setCni(resultSet.getString( "cni" ) );
        profil.setCarte_vitale(resultSet.getString( "carte_vitale" ) );
        profil.setPhotoProfil(resultSet.getString( "photo" ) );

        return profil;

    }
    
    public int getNoOfRecords (){
    	return noOfRecords;
    }
}
    
  