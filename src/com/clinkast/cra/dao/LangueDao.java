package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Langue;

public interface LangueDao {
        
        public void creer(Langue langue) throws DAOException;
        
        public void modifier(Langue langue) throws DAOException;
        
        public void supprimer(Long id) throws DAOException;
        
        public List<Langue> lister(Long id_resume);
}
