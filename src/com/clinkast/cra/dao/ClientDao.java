package com.clinkast.cra.dao;



import java.util.List;

import com.clinkast.cra.beans.Client;




// TODO: Auto-generated Javadoc
/**
 * The Interface ClientDao.
 */
public interface ClientDao {
	 
 	/**
 	 * Creer.
 	 *
 	 * @param client the client
 	 * @throws DAOException the DAO exception
 	 */
 	void creer( Client client ) throws DAOException;
	    
	 /**
 	 * Modifier.
 	 *
 	 * @param client the client
 	 * @param id the id
 	 * @return the client
 	 * @throws DAOException the DAO exception
 	 */
 	Client modifier( Client client, Long id) throws DAOException;

	 /**
 	 * Supprimer.
 	 *
 	 * @param client the client
 	 * @throws DAOException the DAO exception
 	 */
 	void supprimer( Client client ) throws DAOException;
	 
	 /**
 	 * Lister.
 	 *
 	 * @return the list
 	 * @throws DAOException the DAO exception
 	 */
 	List<Client> lister() throws DAOException;
}
