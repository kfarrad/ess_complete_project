package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clinkast.cra.beans.Competence;
import com.clinkast.cra.beans.Experience;
import com.clinkast.cra.beans.Formation;
import com.clinkast.cra.beans.Langue;
import com.clinkast.cra.beans.Resume;

public class ResumeDaoImpl implements ResumeDao {
        
        /** The Constant SQL_SELECT. */
        private static final String SQL_SELECT          = "SELECT *  FROM resume ";
        
        private static final String SQL_SELECT_PDF      = "SELECT *  FROM resume WHERE pdf IS NOT NULL";
        
        private static final String SQL_SELECT_BY_USER  = "SELECT *  FROM resume WHERE id_user = ? ";
        
        private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM resume WHERE id = ?";
        
        private static final String SQL_VERIFICATION    = "SELECT *  FROM resume WHERE id = ? AND id_user = ?";
        /** The Constant SQL_INSERT. */
        private static final String SQL_INSERT          = "INSERT INTO resume (id_user, date) VALUES (?, ?)";
        
        private static final String SQL_UPDATE          = "UPDATE resume SET date = ? WHERE id = ?";
        
        private static final String SQL_UPDATE_PDF      = "UPDATE resume SET pdf = ? WHERE id = ?";
        
        /** The Constant SQL_DELETE. */
        private static final String SQL_DELETE          = "DELETE FROM resume WHERE id = ?";
        
        
        /** The dao factory. */
        private DAOFactory          daoFactory;
        
        /**
         * Instantiates a new domaine dao impl.
         *
         * @param daoFactory the dao factory
         */
        ResumeDaoImpl( DAOFactory daoFactory ) {
                this.daoFactory = daoFactory;
        }
        
        
        /**
         * Trouver.
         *
         * @param id the id
         * @return the resume
         */
        public Resume trouver(Long id )
        {
                return trouver( SQL_SELECT_BY_ID,  id );
        }
        
        @Override
        public void creer(Resume resume) throws DAOException {
                // TODO Auto-generated method stub
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                ResultSet valeursAutoGenerees = null;
                try {
                        connexion = daoFactory.getConnection();
                        preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
                                        resume.getId_user(), resume.getDate());
                        int statut = preparedStatement.executeUpdate();
                        
                        if ( statut == 0 ) {
                                throw new DAOException( "Échec de la création du resume, aucune ligne ajoutée dans la table." );
                        }
                        valeursAutoGenerees = preparedStatement.getGeneratedKeys();
                        
                        if ( valeursAutoGenerees.next() ) {             
                                resume.setId( valeursAutoGenerees.getLong( 1 ) );
                                
                        } else {
                                throw new DAOException( "Échec de la création du resume en base, aucun ID auto-généré retourné." );
                        }
                        
                } catch ( SQLException e ) {
                        throw new DAOException( e );
                } finally {
                        fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
                }
        }
        public void creerPdf(Long id_resume, String nom){
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                try {
                        connexion = daoFactory.getConnection();
                        preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PDF, false,
                                        nom, id_resume);
                        int statut = preparedStatement.executeUpdate();
                        
                        if ( statut == 0 ) {
                                throw new DAOException( "Échec de la modification  du resume, aucune ligne modifiée dans la table." );
                        }
                } catch ( SQLException e ) {
                        throw new DAOException( e );
                } finally {
                        fermeturesSilencieuses( preparedStatement, connexion );
                }
        }

@Override
public void modifier(Resume resume) throws DAOException {
        // TODO Auto-generated method stub
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
                connexion = daoFactory.getConnection();
                preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
                                resume.getDate(),resume.getId());
                int statut = preparedStatement.executeUpdate();
                
                if ( statut == 0 ) {
                        throw new DAOException( "Échec de la modification  du resume, aucune ligne modifiée dans la table." );
                }
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( preparedStatement, connexion );
        }
}

@Override
public Resume supprimer(Long id) throws DAOException {
        // TODO Auto-generated method stub
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        Resume monResume = null;
        try {
                connexion = daoFactory.getConnection();
                monResume = this.trouver(id);
                if(monResume!=null)
                        preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, id);
                int statut = preparedStatement.executeUpdate();
                if ( statut == 0 ) {
                        throw new DAOException( "Échec de la suppression du resume, aucune ligne supprimée de la table." );
                } 
                
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( preparedStatement, connexion );
        }
        return monResume;
}

@Override
public List<Resume> lister() {
        // TODO Auto-generated method stub
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Resume> resumes = new ArrayList<Resume>();
        
        try {
                connection = daoFactory.getConnection();
                preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT, false );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                        resumes.add( map( resultSet ) );
                }
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }
        
        return resumes;
}

public List<Resume> lister(Long id_user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Resume> resumes = new ArrayList<Resume>();
        
        try {
                connection = daoFactory.getConnection();
                preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT_BY_USER, false, id_user );
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                        resumes.add( map( resultSet ) );
                }
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }
        
        return resumes;
}

public Map<Long,List<String>> listerUserCV() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        Map<Long,List<String>> userCVs = new HashMap<Long, List<String>>();
        
        try {
                connection = daoFactory.getConnection();
                preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT_PDF, false);
                resultSet = preparedStatement.executeQuery();
                while ( resultSet.next() ) {
                        Long id_user =resultSet.getLong("id_user");
                        if(userCVs.containsKey(id_user)){
                                userCVs.get(id_user).add(resultSet.getString("pdf"));
                        }
                        else{
                                List<String> listeCV = new ArrayList<String>();
                                listeCV.add(resultSet.getString("pdf"));
                                userCVs.put(id_user, listeCV);
                        }
                }                    
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }
        
        return userCVs;
}
private Resume trouver( String sql, Object... objets ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Resume resume = null;
        
        try {
                /* Récupération d'une connexion depuis la Factory */
                connexion = daoFactory.getConnection();
                /*
                 * Préparation de la requête avec les objets passés en arguments
                 * (ici, uniquement un id) et exécution.
                 */
                preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
                resultSet = preparedStatement.executeQuery();
                /* Parcours de la ligne de données retournée dans le ResultSet */
                if ( resultSet.next() ) {
                        resume = map( resultSet );
                }
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
        
        return resume;
}

/**
 * Map.
 *
 * @param resultSet the result set
 * @return the fichier
 * @throws SQLException the SQL exception
 */
private  Resume map( ResultSet resultSet ) throws SQLException {
        
        ExperienceDaoImpl workExpDao = daoFactory.getExperienceDao();               
        FormationDaoImpl formationDao = daoFactory.getFormationDao();                
        CompetenceDaoImpl competenceDao = daoFactory.getCompetenceDao();                
        LangueDaoImpl langueDao = daoFactory.getLangueDao();                
        TitreCVDaoImpl titreCVDao = daoFactory.getTitreCVDao();   
        
        Resume resume= new Resume();
        Long id_resume = resultSet.getLong( "id" );
        
        Map<Long,Experience> experiences = new HashMap<Long,Experience>();                
        for(Experience eltExp:workExpDao.lister(id_resume)){                        
                experiences.put(eltExp.getId(), eltExp);                        
        }
        
        Map<Long,Competence> competences = new  HashMap<Long,Competence>(); 
        for(Competence eltComp: competenceDao.lister(id_resume)){
                competences.put(eltComp.getId(), eltComp);
        }
        
        Map<Long,Formation> formations = new HashMap<Long, Formation>();
        for(Formation eltForm: formationDao.lister(id_resume)){
                formations.put(eltForm.getId(), eltForm);
        }
        
        Map<Long,Langue> langues = new HashMap<Long, Langue>();
        for(Langue eltLang: langueDao.lister(id_resume)){
                langues.put(eltLang.getId(), eltLang);
        }
        resume.setId(id_resume);
        resume.setId_user(resultSet.getLong( "id_user" ));
        resume.setDate(resultSet.getDate("date"));
        resume.setCompetences(competences);
        resume.setExperiences(experiences);
        resume.setFormations(formations);
        resume.setLangues(langues);                              
        resume.setTitre(titreCVDao.trouverTitreUser(id_resume));
        resume.setPdf(resultSet.getString( "pdf" ));
        
        return resume;
}


public boolean verifier(Long id_resume, Long id_user) {
        // TODO Auto-generated method stub
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Boolean trouve = false;
        try {
                connexion = daoFactory.getConnection();
                preparedStatement = initialisationRequetePreparee( connexion, SQL_VERIFICATION, false, id_resume, id_user );
                resultSet = preparedStatement.executeQuery();
                if ( resultSet.next() ) trouve = true;
                
        } catch ( SQLException e ) {
                throw new DAOException( e );
        } finally {
                fermeturesSilencieuses( preparedStatement, connexion );
        }
        return trouve;
}



}
