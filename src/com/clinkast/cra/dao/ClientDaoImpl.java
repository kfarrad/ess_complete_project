package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Client;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientDaoImpl.
 */
public class ClientDaoImpl implements ClientDao{
		
		/** The Constant SQL_SELECT. */
		private static final String SQL_SELECT				= "SELECT *  FROM client ";
		
		/** The Constant SQL_SELECT_BY_ID. */
		private static final String SQL_SELECT_BY_ID		= "SELECT *  FROM client WHERE id = ?";
		
		/** The Constant SQL_SELECT_BY_NOM. */
		private static final String SQL_SELECT_BY_NOM		= "SELECT *  FROM client WHERE nom = ?";	    
		
		/** The Constant SQL_INSERT. */
		private static final String SQL_INSERT      		= "INSERT INTO client ( nom, adresse) VALUES (?, ?)";
	    
    	/** The Constant SQL_UPDATE. */
    	private static final String SQL_UPDATE				= "UPDATE client SET  nom=?, adresse=? WHERE id = ?";
	    
    	/** The Constant SQL_DELETE. */
    	private static final String SQL_DELETE				= "DELETE FROM client WHERE id = ?";
	    
    	/** The dao factory. */
    	private DAOFactory          daoFactory;

	    /**
    	 * Instantiates a new client dao impl.
    	 *
    	 * @param daoFactory the dao factory
    	 */
    	ClientDaoImpl( DAOFactory daoFactory ) {
	        this.daoFactory = daoFactory;
	    }

	    /* Implémentation de la méthode définie dans l'interface ClientDao */
	    
	   
	    
	    
	    /**
    	 * Trouver.
    	 *
    	 * @param id the id
    	 * @return the client
    	 */
    	public Client trouver(Long id )
	    {
	    	return trouver( SQL_SELECT_BY_ID,  id  );
	    }
	    
	    /**
    	 * Trouver.
    	 *
    	 * @param nom the nom
    	 * @return the client
    	 */
    	public Client trouver(String nom )
	    {
	    	return trouver( SQL_SELECT_BY_NOM,  nom  );
	    }
	    
	    
	   /* (non-Javadoc)
   	 * @see com.clinkast.cra.dao.ClientDao#creer(com.clinkast.cra.beans.Client)
   	 */
   	@Override
	    public void creer( Client client ) throws DAOException {
	        Connection connexion = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet valeursAutoGenerees = null;
	        try {
	            connexion = daoFactory.getConnection();
	            
	            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
	            			 client.getNom(),client.getAdresse() );
	              	int statut = preparedStatement.executeUpdate();
	            	
	            	if ( statut == 0 ) {
	                throw new DAOException( "Échec de la création du Client, aucune ligne ajoutée dans la table." );
	                }
	            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	            
		            if ( valeursAutoGenerees.next() ) {            	
		                client.setId( valeursAutoGenerees.getLong( 1 ) );

		            } else {
		                throw new DAOException( "Échec de la création du Client en base, aucun ID auto-généré retourné." );
		            }
		       
	        } catch ( SQLException e ) {
	            throw new DAOException( e );
	        } finally {
	            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	        }
	        
	    }
	    
	    /* (non-Javadoc)
    	 * @see com.clinkast.cra.dao.ClientDao#modifier(com.clinkast.cra.beans.Client, java.lang.Long)
    	 */
    	@Override
		public Client modifier(Client client, Long id ) throws DAOException {
			// TODO Auto-generated method stub
	    	Connection connexion = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            connexion = daoFactory.getConnection();
	            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
	                     client.getNom(), client.getAdresse(),  id );
	            
	            int statut = preparedStatement.executeUpdate();
	          
	            if ( statut == 0 ) {
	                throw new DAOException( "Échec de la modification  du nom du Client, aucune ligne modifiée dans la table." );
	            }
	            
	        } catch ( SQLException e ) {
	            throw new DAOException( e );
	        } finally {
	            fermeturesSilencieuses( preparedStatement, connexion );
	        }
	    
			return client;
		}

	    /* Implémentation de la méthode définie dans l'interface ClientDao */
	    /* (non-Javadoc)
    	 * @see com.clinkast.cra.dao.ClientDao#lister()
    	 */
    	@Override
	    public List<Client> lister() throws DAOException {
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        List<Client> clients = new ArrayList<Client>();

	        try {
	            connection = daoFactory.getConnection();
	            preparedStatement = connection.prepareStatement( SQL_SELECT );
	            resultSet = preparedStatement.executeQuery();
	            while ( resultSet.next() ) {
	                clients.add( map( resultSet ) );
	            }
	        } catch ( SQLException e ) {
	            throw new DAOException( e );
	        } finally {
	            fermeturesSilencieuses( resultSet, preparedStatement, connection );
	        }

	        return clients;
	    }

	    /* Implémentation de la méthode définie dans l'interface ClientDao */
	    /* (non-Javadoc)
    	 * @see com.clinkast.cra.dao.ClientDao#supprimer(com.clinkast.cra.beans.Client)
    	 */
    	@Override
	    public void supprimer( Client client ) throws DAOException {
	        Connection connexion = null;
	        PreparedStatement preparedStatement = null;

	        try {
	            connexion = daoFactory.getConnection();
	            Client monClient = this.trouver(client.getId());
	            if(monClient!=null)
	            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, client.getId());
	            int statut = preparedStatement.executeUpdate();
	            if ( statut == 0 ) {
	                throw new DAOException( "Échec de la suppression du profil, aucune ligne supprimée de la table." );
	            } else {
	                client.setId( null );
	            }
	        } catch ( SQLException e ) {
	            throw new DAOException( e );
	        } finally {
	            fermeturesSilencieuses( preparedStatement, connexion );
	        }
	    }

	    /*
	     * Méthode générique utilisée pour retourner un client depuis la base de
	     * données, correspondant à la requête SQL donnée prenant en paramètres les
	     * objets passés en argument.
	     */
	    /**
    	 * Trouver.
    	 *
    	 * @param sql the sql
    	 * @param objets the objets
    	 * @return the client
    	 * @throws DAOException the DAO exception
    	 */
    	private Client trouver( String sql, Object... objets ) throws DAOException {
	        Connection connexion = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        Client client = null;

	        try {
	            /* Récupération d'une connexion depuis la Factory */
	            connexion = daoFactory.getConnection();
	            /*
	             * Préparation de la requête avec les objets passés en arguments
	             * (ici, uniquement un id) et exécution.
	             */
	            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	            resultSet = preparedStatement.executeQuery();
	            /* Parcours de la ligne de données retournée dans le ResultSet */
	            if ( resultSet.next() ) {
	                client = map( resultSet );
	            }
	        } catch ( SQLException e ) {
	            throw new DAOException( e );
	        } finally {
	            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	        }

	        return client;
	    }

	    /*
	     * Simple méthode utilitaire permettant de faire la correspondance (le
	     * mapping) entre une ligne issue de la table des clients (un ResultSet) et
	     * un bean Client.
	     */
	    /**
    	 * Map.
    	 *
    	 * @param resultSet the result set
    	 * @return the client
    	 * @throws SQLException the SQL exception
    	 */
    	private static Client map( ResultSet resultSet ) throws SQLException {
	        Client client = new Client();
	        client.setId( resultSet.getLong( "id" ) );	    	       
	        client.setNom( resultSet.getString( "nom" ) );
	        client.setAdresse(resultSet.getString("adresse"));
	       
	        return client;
	    }
	    
	}
