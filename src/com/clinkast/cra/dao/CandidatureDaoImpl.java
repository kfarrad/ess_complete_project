package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;

public class CandidatureDaoImpl implements CandidatureDao {

	/** The Constant SQL_INSERT INTO Candidature. */
	private static final String SQL_INSERT = "INSERT INTO 	candidature ( nom, prenom, telephone, mail, diplome, specialite, nom_cv,commentaire) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	/** The Constant SQL_UPDATE Candidature. */
	private static final String SQL_UPDATE = "UPDATE candidature SET nom = ?, prenom = ?, telephone = ?, mail = ?, diplome = ?, specialite = ?, nom_cv = ?, commentaire = ? WHERE id = ?";
	/** The Constant SQL_SELECT Candidature. */
	// private static final String SQL_SELECT = "SELECT * FROM candidature LIMIT
	// ?, ?";
	/** The Constant SQL_SEARCH Candidature. */
	// private static final String SQL_SEARCH = "SELECT * FROM `candidature`
	// WHERE (`specialite` LIKE ?) OR (`diplome` LIKE ?) OR (`mail` LIKE ?) OR
	// (`telephone` LIKE ?) OR (`prenom` LIKE ?) OR (`nom` LIKE ?) OR (`nom_cv`
	// LIKE ?)"
	// + " ORDER BY (CASE WHEN `specialite` LIKE ? THEN 3 ELSE 0 END) + (CASE
	// WHEN `diplome` LIKE ? THEN 2 ELSE 0 END) + (CASE WHEN `nom` LIKE ? THEN 1
	// ELSE 0 END) DESC";

	/** The dao factory. */
	private DAOFactory daoFactory;
	private int noOfRecords;

	/**
	 * Instantiates a new candidature dao impl.
	 *
	 * @param daoFactory
	 *            the dao factory
	 */
	CandidatureDaoImpl(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public void creer(Candidat candidature) throws DAOException {

		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_INSERT, true, candidature.getNom(),
					candidature.getPrenom(), candidature.getTelephone(),
					candidature.getMail(), candidature.getDiplomes(),
					candidature.getSpecilites(), candidature.getPathCV(),
					candidature.getCommentaires());
			int statut = preparedStatement.executeUpdate();

			if (statut == 0) {
				throw new DAOException(
						"Echec de la création de la candidature, aucune ligne ajoutée dans la table.");
			}
			valeursAutoGenerees = preparedStatement.getGeneratedKeys();

			if (valeursAutoGenerees.next()) {
				candidature.setId(valeursAutoGenerees.getLong(1));

			} else {
				throw new DAOException(
						"Echec de la création de la candidature en base, aucun ID auto-généré retourné.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
					connexion);
		}

	}

	@Override
	public Candidat modifier(Candidat candidature, Long id) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_UPDATE, true, candidature.getNom(),
					candidature.getPrenom(), candidature.getTelephone(),
					candidature.getMail(), candidature.getDiplomes(),
					candidature.getSpecilites(), candidature.getPathCV(),
					candidature.getCommentaires(), id);
			int statut = preparedStatement.executeUpdate();

			if (statut == 0) {
				throw new DAOException(
						"Echec de la modification de la candidature, aucune ligne modifiée dans la table.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
					connexion);
		}
		return null;
	}

	@Override
	public Candidat supprimer(long id) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		Candidat candidat = null;
		String request = "DELETE FROM `clinkastcra`.`candidature` WHERE `candidature`.`id` = "
				+ id;
		try {
			candidat = this.getCandidatById(id);
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DAOException(
						"Echec de la suppression de la candidature, aucune ligne supprimée dans la base de données.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
					connexion);
		}
		return candidat;
	}

	@Override
	public List<Candidat> lister(int debut, int nombreCandidat)
			throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Candidat> candidats = new ArrayList<Candidat>();
		String request = "SELECT SQL_CALC_FOUND_ROWS * FROM candidature LIMIT "
				+ debut + ", " + nombreCandidat;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				candidats.add(doMapping(results, false));
			}
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}
		return candidats;
	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

	public Candidat getCandidatById(long id) {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Candidat candidat = new Candidat();
		String request = "SELECT * FROM candidature WHERE id = " + id;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			result = preparedStatement.executeQuery();
			if (result.next()) {
				candidat = doMapping(result, false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(result, preparedStatement, connexion);
		}
		return candidat;
	}

	/**
	 * Return the candidat after the mapping bitween the resultSet and the
	 * object Candidat is done
	 */
	private Candidat doMapping(ResultSet results, boolean search)
			throws SQLException {

		Candidat candidat = new Candidat();
		candidat.setId(results.getLong("id"));
		candidat.setNom(results.getString("nom"));
		candidat.setPrenom(results.getString("prenom"));
		candidat.setTelephone(results.getString("telephone"));
		candidat.setMail(results.getString("mail"));
		candidat.setDiplomes(results.getString("diplome"));
		candidat.setSpecilites(results.getString("specialite"));
		candidat.setPathCV(results.getString("nom_cv"));
		if (search)
			candidat.setPertinence(results.getString("pertinence"));
		candidat.setCommentaires(results.getString("commentaire"));
		return candidat;
	}

	@Override
	public List<Candidat> chercher(int debut, int nombreCandidat, String text,
			String dateFrom, String dateTo, String searchInDocuments)
			throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Candidat> candidats = new ArrayList<Candidat>();
		String formatSQL = "%".concat(text).concat("%");
		String requestSQL = "";
		String limitCondition = "";
		String listDocuments = "''";
		if (dateFrom != null && dateTo != null) {
			if (!dateFrom.isEmpty() && !dateTo.isEmpty()) {
				String from = getSqlFormatDateFrom(dateFrom);
				String to = getSqlFormatDateTo(dateTo);
				limitCondition = "AND (date_creation <= '" + to
						+ "' AND date_creation >= '" + from + "' ) ";
			}
			if (!dateFrom.isEmpty()) {
				String from = getSqlFormatDateFrom(dateFrom);
				limitCondition = "AND (date_creation >= '" + from + "' ) ";
			}
			if (!dateTo.isEmpty()) {
				String to = getSqlFormatDateFrom(dateFrom);
				limitCondition = "AND (date_creation <= '" + to + "' ) ";
			}

		} else {
			if (dateFrom != null) {
				if (!dateFrom.isEmpty()) {
					String from = getSqlFormatDateFrom(dateFrom);
					limitCondition = "AND (date_creation >= '" + from + "' ) ";
				}
			}
			if (dateTo != null) {
				if (!dateTo.isEmpty()) {
					String to = getSqlFormatDateFrom(dateFrom);
					limitCondition = "AND (date_creation <= '" + to + "' ) ";
				}
			}
		}
		if (searchInDocuments != null && searchInDocuments.equals("on")) {
			listDocuments = searchInDocuments(text);
		}

		requestSQL = "SELECT DISTINCT SQL_CALC_FOUND_ROWS `id`, `nom`, `prenom`, `telephone`, `mail`, `diplome`, `specialite`, `nom_cv`, `commentaire`, "
				+ " ( CASE " + "		  WHEN `specialite` LIKE '"
				+ formatSQL
				+ "' THEN 6 "
				+ "		  WHEN `diplome` LIKE '"
				+ formatSQL
				+ "' THEN 5 "
				+ "		  WHEN `nom` LIKE '"
				+ formatSQL
				+ "' THEN 4 "
				+ "		  WHEN `prenom` LIKE '"
				+ formatSQL
				+ "' THEN 3 "
				+ "		  WHEN `telephone` LIKE '"
				+ formatSQL
				+ "' THEN 2 "
				+ "		  WHEN `mail` LIKE '"
				+ formatSQL
				+ "' THEN 1 "
				+ " ELSE 0		END ) as pertinence "
				+ "FROM `candidature` "
				+ "WHERE ((`specialite` LIKE '"
				+ formatSQL
				+ "') OR (`diplome` LIKE '"
				+ formatSQL
				+ "') OR (`mail` LIKE '"
				+ formatSQL
				+ "') OR (`telephone` LIKE '"
				+ formatSQL
				+ "') OR (`prenom` LIKE '"
				+ formatSQL
				+ "') OR (`nom` LIKE '"
				+ formatSQL
				+ "') OR (`nom_cv` LIKE '"
				+ formatSQL
				+ "') OR nom_cv IN ("
				+ listDocuments
				+ ")) "
				+ limitCondition
				+ "ORDER BY pertinence DESC "
				+ "LIMIT "
				+ debut
				+ ", "
				+ nombreCandidat;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(requestSQL);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				candidats.add(doMapping(results, true));
			}
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}

		return candidats;
	}

	/**
	 * Méthode d'aide permettant de la liste des cv correspondants aux critères
	 * de la recherche
	 * 
	 * @param wordToFind
	 *            Le mot cl� � trouver
	 * @return String repr�sentant la liste des cv qui contiennent le mot cl�
	 *         pass� en param�tres
	 **/
	private String searchInDocuments(String wordToFind) {
		String result = "";
		String docxPath = "";
		String docPath = "";
		String pdfPath = "";

		List<String> pdfCvNames = new ArrayList<String>();
		List<String> docxCvNames = new ArrayList<String>();
		List<String> docCvNames = new ArrayList<String>();

		File pdfFolder = null;
		File docxFolder = null;
		File docFolder = null;

		try {
			pdfFolder = new File(Constants.getCheminUpload()
					+ Constants.CANDIDATURE_PDF_FILES_PATH);
			docxFolder = new File(Constants.getCheminUpload()
					+ Constants.CANDIDATURE_DOCX_FILES_PATH);
			docFolder = new File(Constants.getCheminUpload()
					+ Constants.CANDIDATURE_DOC_FILES_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		docxCvNames = listFilesInFolder(docxFolder);
		docCvNames = listFilesInFolder(docFolder);
		pdfCvNames = listFilesInFolder(pdfFolder);

		// Parcours la liste des cv récupérée depuis le répértoire des CV
		// d'extension PDF
		for (int i = 0; i < pdfCvNames.size(); i++) {
			try {
				pdfPath = (Constants.getCheminUpload() + Constants.CANDIDATURE_PDF_FILES_PATH)
						.concat(pdfCvNames.get(i));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (pdfContainsKeyword(pdfPath, wordToFind)) {
				result = result.concat("'"
						+ Constants.CANDIDATURE_PDF_FILES_PATH_Windows
						+ pdfCvNames.get(i) + "',");
			}
		}

		// Parcours la liste des cv récupérée depuis le répertoire des CV
		// d'extension DOCX
		for (int i = 0; i < docxCvNames.size(); i++) {
			try {
				docxPath = (Constants.getCheminUpload() + Constants.CANDIDATURE_DOCX_FILES_PATH)
						.concat(docxCvNames.get(i));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (docxContainsKeyword(docxPath, wordToFind)) {
				result = result.concat("'"
						+ Constants.CANDIDATURE_DOCX_FILES_PATH_Windows
						+ docxCvNames.get(i) + "',");
			}
		}

		// Parcours la liste des cv récupérée depuis le répertoire des CV
		// d'extension DOC
		for (int i = 0; i < docCvNames.size(); i++) {
			try {
				docPath = (Constants.getCheminUpload() + Constants.CANDIDATURE_DOC_FILES_PATH)
						.concat(docCvNames.get(i));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (docContainsKeyword(docPath, wordToFind)) {
				result = result.concat("'"
						+ Constants.CANDIDATURE_DOC_FILES_PATH_Windows
						+ docCvNames.get(i) + "',");
			}
		}

		result = result.concat("'CV_Default'");
		return result;

	}

	/**
	 * Methode d'aide permettant de récuperer l'ensemble des fichiers présentant
	 * dans le répertoire donné en paramètre
	 */
	private List<String> listFilesInFolder(final File folder) {
		List<String> files = new ArrayList<String>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesInFolder(fileEntry);
			} else {
				files.add(fileEntry.getName());
			}
		}
		return files;
	}

	private boolean pdfContainsKeyword(String path, String keyword) {
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = "";
		File file;
		try {
			file = new File(path);
			PDFParser parser = new PDFParser(new FileInputStream(file));
			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdDoc = new PDDocument(cosDoc);
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(5);
			parsedText = pdfStripper.getText(pdDoc);
			cosDoc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (parsedText.toLowerCase().contains(keyword.toLowerCase())) {
			return true;
		} else {
			return false;
		}

	}

	private boolean docxContainsKeyword(String path, String keyword) {
		XWPFDocument doc = null;
		InputStream is = null;
		XWPFWordExtractor extractor = null;
		try {
			is = new FileInputStream(path);
			doc = new XWPFDocument(is);
			extractor = new XWPFWordExtractor(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}

		String text = extractor.getText();
		if (text.toLowerCase().contains(keyword.toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}

	private boolean docContainsKeyword(String path, String keyword) {
		HWPFDocument doc = null;
		InputStream is = null;
		WordExtractor extractor = null;
		POIFSFileSystem fs = null;
		try {
			is = new FileInputStream(path);
			fs = new POIFSFileSystem(is);
			doc = new HWPFDocument(fs);
			extractor = new WordExtractor(doc);

		} catch (IOException e) {
			e.printStackTrace();
		}

		String text = extractor.getText();
		if (text.toLowerCase().contains(keyword.toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}

	private String getSqlFormatDateFrom(String date) {
		String[] tmp = date.split("-");
		if (tmp.length == 3)
			return tmp[2] + "-" + tmp[1] + "-" + tmp[0] + " 00:00:00";
		return "";
	}

	private String getSqlFormatDateTo(String date) {
		String[] tmp = date.split("-");
		if (tmp.length == 3)
			return tmp[2] + "-" + tmp[1] + "-" + tmp[0] + " 23:59:59";
		return "";
	}

}
