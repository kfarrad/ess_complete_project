package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.TitreCV;

public interface TitreCVDao {
 public void creer(TitreCV titre) throws DAOException;
        
        public void modifier(TitreCV titre) throws DAOException;
        
        public void supprimer(Long id) throws DAOException;
        
        public List<TitreCV> lister();
        
}
