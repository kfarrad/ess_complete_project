package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Langue;

public class LangueDaoImpl implements LangueDao {
    private static final String SQL_SELECT          = "SELECT *  FROM langue WHERE id_resume = ? ";
    
    private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM langue WHERE id = ?";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT          = "INSERT INTO langue (id_resume, nom, aisance)"
	    + " VALUES (?, ?, ?)";
    
    /** The Constant SQL_DELETE. */
    private static final String SQL_DELETE          = "DELETE FROM langue WHERE id = ?";
    
    /** The Constant SQL_UPDATE. */
    private static final String SQL_UPDATE          = "UPDATE langue SET  nom = ?, aisance = ? WHERE id = ?";
    
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    @Override
    public void creer(Langue langue) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
		    langue.getId_resume(), langue.getNom(), langue.getAisance());
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la création de la langue, aucune ligne ajoutée dans la table." );
	    }
	    valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	    
	    if ( valeursAutoGenerees.next() ) {             
		langue.setId( valeursAutoGenerees.getLong( 1 ) );
		
	    } else {
		throw new DAOException( "Échec de la création de la langue en base, aucun ID auto-généré retourné." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	}
    }
    
    @Override
    public void modifier(Langue langue) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
		    langue.getNom(), langue.getAisance(), langue.getId());
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification  de la Formation, aucune ligne modifiée dans la table." );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    @Override
    public void supprimer(Long id) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	
	try {
	    connexion = daoFactory.getConnection();
	    Langue maLangue = this.trouver(id);
	    if(maLangue!=null){
		preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, id);
		int statut = preparedStatement.executeUpdate();
		if ( statut == 0 ) {
		    throw new DAOException( "Échec de la suppression de la langue, aucune ligne supprimée de la table." );
		} 
	    }else  throw new DAOException( "Échec de la suppression de la langue, aucune ligne supprimée de la table." );
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    @Override
    public List<Langue> lister(Long id_resume) {
	// TODO Auto-generated method stub
	// TODO Auto-generated method stub
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Langue> langues = new ArrayList<Langue>();
	
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT, false, id_resume );
	    resultSet = preparedStatement.executeQuery();
	    while ( resultSet.next() ) {
		langues.add( map( resultSet ) );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connection );
	}
	
	return langues;
    }
    
    /**
     * Instantiates a new domaine dao impl.
     *
     * @param daoFactory the dao factory
     */
    LangueDaoImpl( DAOFactory daoFactory ) {
	this.daoFactory = daoFactory;
    }
    
    
    /**
     * Trouver.
     *
     * @param id the id
     * @return the domaine
     */
    public Langue trouver(Long id )
    {
	return trouver( SQL_SELECT_BY_ID,  id );
    }
    
    
    private Langue trouver( String sql, Object... objets ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Langue langue = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    /*
	     * Préparation de la requête avec les objets passés en arguments
	     * (ici, uniquement un id) et exécution.
	     */
	    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	    resultSet = preparedStatement.executeQuery();
	    /* Parcours de la ligne de données retournée dans le ResultSet */
	    if ( resultSet.next() ) {
		langue = map( resultSet );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return langue;
    }
    
    private static Langue map( ResultSet resultSet ) throws SQLException {
	Langue langue = new Langue();
	langue.setId(resultSet.getLong("id"));
	langue.setId_resume(resultSet.getLong("id_resume"));
	langue.setNom(resultSet.getString("nom"));
	langue.setAisance(resultSet.getString("aisance"));
	
	return langue;
    }
}
