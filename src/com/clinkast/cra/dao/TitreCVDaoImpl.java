package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.TitreCV;

public class TitreCVDaoImpl implements TitreCVDao {
        
        private static final String SQL_SELECT          = "SELECT *  FROM titrecv";
        
        private static final String SQL_SELECT_BY_ID    = "SELECT *  FROM titrecv WHERE id = ?";
          
        /** The Constant SQL_INSERT. */
        private static final String SQL_INSERT          = "INSERT INTO titrecv (id_resume, initiales, nom, sous_titre)"
                                                        + " VALUES (?, ?, ?, ?)";
        
        /** The Constant SQL_DELETE. */
        private static final String SQL_DELETE          = "DELETE FROM titrecv WHERE id = ?";
        
        /** The Constant SQL_UPDATE. */
        private static final String SQL_UPDATE          = "UPDATE titreCV SET  nom = ?, sous_titre = ? WHERE id = ?";

        private static final String SQL_SELECT_BY_ID_RESUME = "SELECT *  FROM titrecv WHERE id_resume = ?";
        
        /** The dao factory. */
        private DAOFactory          daoFactory;

        @Override
        public void creer(TitreCV titreCV) throws DAOException {
                // TODO Auto-generated method stub
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                ResultSet valeursAutoGenerees = null;
                try {
                    connexion = daoFactory.getConnection();
                    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
                                    titreCV.getId_resume(), titreCV.getInitiales(),titreCV.getNom(), titreCV.getSous_titre());
                    
                    int statut = preparedStatement.executeUpdate();
                    
                    if ( statut == 0 ) {
                            throw new DAOException( "Échec de la création du titreCV, aucune ligne ajoutée dans la table." );
                            }
                            valeursAutoGenerees = preparedStatement.getGeneratedKeys();
                        
                            if ( valeursAutoGenerees.next() ) {             
                                    titreCV.setId( valeursAutoGenerees.getLong( 1 ) );

                            } else {
                                throw new DAOException( "Échec de la création du titreCV en base, aucun ID auto-généré retourné." );
                            }
                       
                    } catch ( SQLException e ) {
                        throw new DAOException( e );
                    } finally {
                        fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
                    }
        }

        @Override
        public void modifier(TitreCV titreCV) throws DAOException {
                // TODO Auto-generated method stub
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                try {
                    connexion = daoFactory.getConnection();
                    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
                                    titreCV.getNom(), titreCV.getSous_titre(), titreCV.getId());
                    
                    int statut = preparedStatement.executeUpdate();
                  
                    if ( statut == 0 ) {
                        throw new DAOException( "Échec de la modification  du titreCV, aucune ligne modifiée dans la table." );
                    }
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( preparedStatement, connexion );
                }
        }

        @Override
        public void supprimer(Long id) throws DAOException {
                // TODO Auto-generated method stub
                Connection connexion = null;
                PreparedStatement preparedStatement = null;

                try {
                    connexion = daoFactory.getConnection();
                    TitreCV maTitreCV = this.trouver(id);
                    if(maTitreCV!=null)
                    preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, id);
                    int statut = preparedStatement.executeUpdate();
                    if ( statut == 0 ) {
                        throw new DAOException( "Échec de la suppression du titreCV, aucune ligne supprimée de la table." );
                    } 
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( preparedStatement, connexion );
                }
        }

        @Override
        public List<TitreCV> lister() {
                // TODO Auto-generated method stub
             // TODO Auto-generated method stub
                Connection connection = null;
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                List<TitreCV> titreCVs = new ArrayList<TitreCV>();

                try {
                    connection = daoFactory.getConnection();
                    preparedStatement = connection.prepareStatement( SQL_SELECT );
                    resultSet = preparedStatement.executeQuery();
                    while ( resultSet.next() ) {
                            titreCVs.add( map( resultSet ) );
                    }                    
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( resultSet, preparedStatement, connection );
                }

                return titreCVs;
        }
        
        /*public List<Long> listerUserCV() {
                // TODO Auto-generated method stub
             // TODO Auto-generated method stub
                Connection connection = null;
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                List<Long> userCVs = new ArrayList<Long>();

                try {
                    connection = daoFactory.getConnection();
                    preparedStatement = connection.prepareStatement( SQL_SELECT );
                    resultSet = preparedStatement.executeQuery();
                    while ( resultSet.next() ) {
                            userCVs.add( resultSet.getLong("id_user") );
                    }                    
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( resultSet, preparedStatement, connection );
                }

                return userCVs;
        }*/
        /**
         * Instantiates a new domaine dao impl.
         *
         * @param daoFactory the dao factory
         */
        TitreCVDaoImpl( DAOFactory daoFactory ) {
                this.daoFactory = daoFactory;
        }
        
        
        /**
         * Trouver.
         *
         * @param id the id
         * @return the domaine
         */
        public TitreCV trouver(Long id )
        {
            return trouver( SQL_SELECT_BY_ID,  id );
        }
        
        public TitreCV trouverTitreUser(Long id_resume )
        {
            return trouver( SQL_SELECT_BY_ID_RESUME,  id_resume );
        }
        private TitreCV trouver( String sql, Object... objets ) throws DAOException {
                Connection connexion = null;
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                TitreCV titreCV = null;

                try {
                    /* Récupération d'une connexion depuis la Factory */
                    connexion = daoFactory.getConnection();
                    /*
                     * Préparation de la requête avec les objets passés en arguments
                     * (ici, uniquement un id) et exécution.
                     */
                    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
                    resultSet = preparedStatement.executeQuery();
                    /* Parcours de la ligne de données retournée dans le ResultSet */
                    if ( resultSet.next() ) {
                            titreCV = map( resultSet );
                    }
                } catch ( SQLException e ) {
                    throw new DAOException( e );
                } finally {
                    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
                }

                return titreCV;
            }
        
        private static TitreCV map( ResultSet resultSet ) throws SQLException {
                TitreCV titreCV = new TitreCV();
                titreCV.setId(resultSet.getLong("id"));
                titreCV.setId_resume(resultSet.getLong("id_resume"));
                titreCV.setInitiales(resultSet.getString("initiales"));
                titreCV.setNom(resultSet.getString("nom"));
                titreCV.setSous_titre(resultSet.getString("sous_titre"));
                
                return titreCV;
        }
}
