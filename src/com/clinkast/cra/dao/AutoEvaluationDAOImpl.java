package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import com.clinkast.cra.beans.AutoEvaluations;


public class AutoEvaluationDAOImpl implements AutoEvaluationDAO {
	
	private static final String SELECT_BY_ID = "SELECT * FROM auto_evaluations WHERE auto_evaluations_id = ?";
	private static final String SQL_INSERT = "INSERT INTO auto_evaluations (" +
			"auto_evaluations_resume, " +
			"auto_evaluations_autresMoyens, " +
			"auto_evaluations_elementsSignifications, " +
			"auto_evaluations_bilanClinkastAttentes, " +
			"auto_evaluations_bilanMesAttentes, " +
			"auto_evaluations_atoutPourProgresser, " +
			"auto_evaluations_axesAProgresser, " +
			"auto_evaluations_souhaitsCourtMoyen, " +
			"auto_evaluations_elemetsImportant) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String  SQL_UPDATE = "UPDATE auto_evaluations SET " +
			"auto_evaluations_resume = ?, " +
			"auto_evaluations_autresMoyens = ?, " +
			"auto_evaluations_elementsSignifications = ?, " +
			"auto_evaluations_bilanClinkastAttentes = ?, " +
			"auto_evaluations_bilanMesAttentes = ?, " +
			"auto_evaluations_atoutPourProgresser = ?, " +
			"auto_evaluations_axesAProgresser = ?, " +
			"auto_evaluations_souhaitsCourtMoyen = ?, " +
			"auto_evaluations_elemetsImportant = ? " +
			"WHERE auto_evaluations_id = ?";
	private DAOFactory daoFactory;
	public AutoEvaluationDAOImpl(DAOFactory daoFactory)
	{
		this.daoFactory = daoFactory;
	}
	
	@Override
	public AutoEvaluations trouver(Long id) throws DAOException{
		
		AutoEvaluations autoEvaluations = null;  
        try {
        	autoEvaluations = trouve(SELECT_BY_ID, id);
            
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return autoEvaluations;
	}
	
	private AutoEvaluations map(ResultSet resultSet) throws SQLException {
		// TODO Auto-generated method stub
		
		/*Fabrique l'activité*/
		
		AutoEvaluations autoEvaluations = new AutoEvaluations();
		autoEvaluations.setAutoEvaluationsId(resultSet.getLong("auto_evaluations_id"));
		autoEvaluations.setAutoEvaluationsResume(resultSet.getString("auto_evaluations_resume"));
		autoEvaluations.setAutoEvaluationsAutresMoyens(resultSet.getString("auto_evaluations_autresMoyens"));
		autoEvaluations.setAutoEvaluationsElementsSignifications(resultSet.getString("auto_evaluations_elementsSignifications"));
		autoEvaluations.setAutoEvaluationsBilanClinkastAttentes(resultSet.getString("auto_evaluations_bilanClinkastAttentes"));
		autoEvaluations.setAutoEvaluationsBilanMesAttentes(resultSet.getString("auto_evaluations_bilanMesAttentes"));
		autoEvaluations.setAutoEvaluationsAtoutPourProgresser(resultSet.getString("auto_evaluations_atoutPourProgresser"));
		autoEvaluations.setAutoEvaluationsAxesAprogresser(resultSet.getString("auto_evaluations_axesAProgresser"));
		autoEvaluations.setAutoEvaluationsSouhaitsCourtMoyen(resultSet.getString("auto_evaluations_souhaitsCourtMoyen"));
		autoEvaluations.setAutoEvaluationsElemetsImportant(resultSet.getString("auto_evaluations_elemetsImportant"));
		autoEvaluations.setSystemesActiviteses(daoFactory.getSystemesActivitesDAOImpl().lister(resultSet.getLong("auto_evaluations_id")));
		autoEvaluations.setFormationsSuivieses(daoFactory.getFormationsSuiviesDAOImpl().lister(resultSet.getLong("auto_evaluations_id")));
		autoEvaluations.setObjectifses(daoFactory.getObjectifsDAOImpl().lister(resultSet.getLong("auto_evaluations_id")));
		autoEvaluations.setRapportses(daoFactory.getRapportsDAOImpl().lister(resultSet.getLong("auto_evaluations_id")));
		autoEvaluations.setFormationsDemandes(daoFactory.getFormationsDemandesDAOImp().lister(resultSet.getLong("auto_evaluations_id")));
		return autoEvaluations;
	}

	@Override
	public AutoEvaluations creer(AutoEvaluations autoEvaluations) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
            			 autoEvaluations.getAutoEvaluationsResume(), 
            			 autoEvaluations.getAutoEvaluationsAutresMoyens(),
            			 autoEvaluations.getAutoEvaluationsElementsSignifications(), 
            			 autoEvaluations.getAutoEvaluationsBilanClinkastAttentes(),
            			 autoEvaluations.getAutoEvaluationsBilanMesAttentes(), 
            			 autoEvaluations.getAutoEvaluationsAtoutPourProgresser(), 
            			 autoEvaluations.getAutoEvaluationsAxesAprogresser(), 
            			 autoEvaluations.getAutoEvaluationsSouhaitsCourtMoyen(), 
            			 autoEvaluations.getAutoEvaluationsElemetsImportant());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création du Client, aucune ligne ajoutée dans la table." );
                }
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                autoEvaluations.setAutoEvaluationsId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création du Client en base, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
        
		return autoEvaluations;

	}

	@Override
	public AutoEvaluations modifier(AutoEvaluations autoEvaluations) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		autoEvaluations.getAutoEvaluationsResume(), 
            		autoEvaluations.getAutoEvaluationsAutresMoyens(), 
            		autoEvaluations.getAutoEvaluationsElementsSignifications(),
            		autoEvaluations.getAutoEvaluationsBilanClinkastAttentes(), 
            		autoEvaluations.getAutoEvaluationsBilanMesAttentes(), 
            		autoEvaluations.getAutoEvaluationsAtoutPourProgresser(),
            		autoEvaluations.getAutoEvaluationsAxesAprogresser(), 
            		autoEvaluations.getAutoEvaluationsSouhaitsCourtMoyen(), 
            		autoEvaluations.getAutoEvaluationsElemetsImportant(), 
            		autoEvaluations.getAutoEvaluationsId());
                     
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  du nom du Client, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
		return autoEvaluations;
	}

	@Override
	public Set<AutoEvaluations> lister(int idAutoEvaluation)
			throws DAOException {
		// TODO Auto-generated method stub
		return null;
	}
	/*private List<AutoEvaluations> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<AutoEvaluations> autoEvaluations = new ArrayList<AutoEvaluations>();
        ResultSet resultSet = null;
       
        try {
             Récupération d'une connexion depuis la Factory 
            connexion = daoFactory.getConnection();
            
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	autoEvaluations.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return autoEvaluations;
	}
	*/
	private AutoEvaluations trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        AutoEvaluations autoEvaluations = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	autoEvaluations = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return autoEvaluations;
	}

}
