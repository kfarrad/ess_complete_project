package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Utilisateur;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarDaoImpl.
 */
public class CalendarDaoImpl implements CalendarDao{
    
    /** The Constant SQL_SELECT. */
    private static final String SQL_SELECT			= "SELECT *  FROM calendar WHERE id_user = ? and mois = ? and annee = ? ";
    
    /** The Constant SQL_SELECT_CAL. */
    private static final String SQL_SELECT_CAL		= "SELECT *  FROM calendar WHERE id_user = ? and id_projet = ? and annee = ? and mois = ? ";
    
    /** The Constant SQL_INSERT. */
    private static final String SQL_INSERT      	= "INSERT INTO calendar ( id_user, id_projet, annee, mois, total_jour,"
	    + " j1,  j2 ,  j3, j4, j5, j6, j7, j8, j9, j10, j11, j12, j13, j14,"
	    + " j15, j16, j17, j18, j19, j20, j21,	j22, j23, j24, j25, j26, j27, j28, j29, j30, j31)"
	    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    /** The Constant SQL_UPDATE. */
    private static final String SQL_UPDATE			= "UPDATE calendar SET total_jour=?, j1=?, j2=?, j3=?, j4=?, j5=?, j6=?, j7=?, j8=?, j9=?, j10=?, j11=?, j12=?, j13=?, j14=?, j15=?,"
	    + "j16=?, j17=?, j18=?, j19=?, j20=?, j21=?, j22=?, j23=?, j24=?, j25=?, j26=?, j27=?, j28=?, j29=?, j30=?, j31=?"
	    + "WHERE id_user = ? and id_projet=? and annee=? and mois=?";
    
    /** The Constant SQL_UPDATE_FERIER. */
    private static final String SQL_UPDATE_FERIER			= "UPDATE calendar SET  j1=?, j2=?, j3=?, j4=?, j5=?, j6=?, j7=?, j8=?, j9=?, j10=?, j11=?, j12=?, j13=?, j14=?, j15=?,"
	    + "j16=?, j17=?, j18=?, j19=?, j20=?, j21=?, j22=?, j23=?, j24=?, j25=?, j26=?, j27=?, j28=?, j29=?, j30=?, j31=?"
	    + "WHERE id=?";
    
    
    /** The Constant SQL_DELETE. */
    private static final String SQL_DELETE			= "DELETE FROM calendar WHERE id_user = ? and id_projet=? and annee=? and mois=?";
    
    /** The Constant SQL_UPDATE_STATUS. */
    private static final String SQL_UPDATE_STATUS	= "UPDATE calendar SET status=? WHERE id_user = ? and id_projet=? and annee=? and mois=?";
    
    private static final String SQL_COUNT_JOUR = "SELECT sum(total_jour)  FROM calendar WHERE id_user= ? and id_projet = ?  and annee >= ? and mois >= ?";
    
    /** The Constant SQL_STATUS_USER. */
    private static final String SQL_STATUS_USER          = "SELECT status  FROM calendar WHERE id_user = ? and mois = ? and annee = ? ";
    
    
    /** The dao factory. */
    private DAOFactory          daoFactory;
    
    /**
     * Instantiates a new calendar dao impl.
     *
     * @param daoFactory the dao factory
     */
    CalendarDaoImpl( DAOFactory daoFactory ) {
	this.daoFactory = daoFactory;
    }
    
    
    /**
     * Trouver.
     *
     * @param id the id
     * @return the calendrier
     */
    public Calendrier trouver(Long id )
    {
	return trouver( SQL_SELECT,  id );
    }
    
    
    /**
     * Trouver.
     *
     * @param id_user the id_user
     * @param id_projet the id_projet
     * @param annee the annee
     * @param mois the mois
     * @return the calendrier
     */
    public Calendrier trouver(Long id_user, Long id_projet, Long annee, Long mois )
    {
	return trouver( SQL_SELECT_CAL,  id_user, id_projet, annee, mois  );
    }
    
    /**
     * Sets the status.
     *
     * @param id_user the id_user
     * @param id_projet the id_projet
     * @param annee the annee
     * @param mois the mois
     * @param status the status
     * @throws DAOException the DAO exception
     */
    public void setStatus(Long id_user, Long id_projet, Long annee, Long mois, Long status)throws DAOException	    
    {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_STATUS, false, status, id_user, id_projet, annee, mois);
	    int statut = preparedStatement.executeUpdate();		          
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification du status, aucune ligne modifiée dans la table." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
    }
    
    
    /**
     * Sets the holiday.
     *
     * @param id_user the id_user
     * @param id_projet the id_projet
     * @param annee the year
     * @param mois the month
     * @param jours the List of days
     * @throws DAOException the DAO exception
     */
    public void setFerier(Long id_user, Long id_projet, Long annee, Long mois, List<Float> jours)throws DAOException	    
    {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    int i=0;
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,id_user, id_projet,annee, mois, -1
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++));
	    int statut = preparedStatement.executeUpdate();		          
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification du status, aucune ligne modifiée dans la table." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
    }
    
    
    
    /**
     * Update ferier.
     *
     * @param id the id
     * @param jours the jours
     * @throws DAOException the DAO exception
     */
    public void updateFerier(Long id, List<Float> jours)throws DAOException	    
    {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    int i=0;
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_FERIER, false
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++), jours.get(i++)
		    , jours.get(i++), jours.get(i++), jours.get(i++),id);
	    int statut = preparedStatement.executeUpdate();		          
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification des jours feriers, aucune ligne modifiée dans la table." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
    }
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.CalendarDao#creer(com.clinkast.cra.beans.Calendrier)
     */
    @Override
    public void creer( Calendrier calendrier ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
	    connexion = daoFactory.getConnection();
	    int i=0;
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true,
		    calendrier.getId_user(), calendrier.getId_projet(), calendrier.getAnnee(), calendrier.getMois(), calendrier.getTotal_jour(),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++));
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la création du calendar, aucune ligne ajoutée dans la table." );
	    }
	    valeursAutoGenerees = preparedStatement.getGeneratedKeys();
	    
	    if ( valeursAutoGenerees.next() ) {            	
		calendrier.setId( valeursAutoGenerees.getLong( 1 ) );
		
	    } else {
		throw new DAOException( "Échec de la création du calendar en base, aucun ID auto-généré retourné." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
	}
	
    }
    
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.CalendarDao#modifier(com.clinkast.cra.beans.Calendrier, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long)
     */
    @Override
    public Calendrier modifier(Calendrier calendrier, Long id_user, Long id_projet, Long annee, Long mois ) throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	int i=0;
	PreparedStatement preparedStatement = null;
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
		    calendrier.getTotal_jour(),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++),
		    calendrier.getJours().get(i++), calendrier.getJours().get(i++), calendrier.getJours().get(i++), id_user, id_projet, annee, mois);
	    
	    int statut = preparedStatement.executeUpdate();
	    
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la modification   calendar, aucune ligne modifiée dans la table." );
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
	
	return calendrier;
    }
    
    /* Implémentation de la méthode définie dans l'interface ClientDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.CalendarDao#lister()
     */
    @Override
    public List<Calendrier> lister(Long id_user, Long mois, Long annee ) throws DAOException {
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Calendrier> clients = new ArrayList<Calendrier>();
	
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT, true, id_user,mois, annee );
	    resultSet = preparedStatement.executeQuery();
	    while ( resultSet.next() ) {
		clients.add( map( resultSet ) );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connection );
	}
	
	return clients;
    }
    
    /* Implémentation de la méthode définie dans l'interface ClientDao */
    /* (non-Javadoc)
     * @see com.clinkast.cra.dao.CalendarDao#supprimer(com.clinkast.cra.beans.Calendrier)
     */
    @Override
    public void supprimer( Calendrier calendrier ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	
	try {
	    connexion = daoFactory.getConnection();
	    Calendrier monCalendar = this.trouver(calendrier.getId());
	    if(monCalendar!=null)
		preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, calendrier.getId());
	    int statut = preparedStatement.executeUpdate();
	    if ( statut == 0 ) {
		throw new DAOException( "Échec de la suppression du profil, aucune ligne supprimée de la table." );
	    } else {
		calendrier.setId( null );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( preparedStatement, connexion );
	}
    }
    
    /*
     * Méthode générique utilisée pour retourner un client depuis la base de
     * données, correspondant à la requête SQL donnée prenant en paramètres les
     * objets passés en argument.
     */
    /**
     * Trouver.
     *
     * @param sql the sql
     * @param objets the objets
     * @return the calendrier
     * @throws DAOException the DAO exception
     */
    private Calendrier trouver( String sql, Object... objets ) throws DAOException {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Calendrier client = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    /*
	     * Préparation de la requête avec les objets passés en arguments
	     * (ici, uniquement un id) et exécution.
	     */
	    preparedStatement = initialisationRequetePreparee( connexion, sql, false, objets );
	    resultSet = preparedStatement.executeQuery();
	    /* Parcours de la ligne de données retournée dans le ResultSet */
	    if ( resultSet.next() ) {
		client = map( resultSet );
	    }
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return client;
    }
    
    /*
     * Simple méthode utilitaire permettant de faire la correspondance (le
     * .
     */
    /**
     * Mapping entre une ligne issue de la table  (un ResultSet) et
     * un bean Calendrier.
     *
     * @param resultSet the result set
     * @return the calendrier
     * @throws SQLException the SQL exception
     */
    private static Calendrier map( ResultSet resultSet ) throws SQLException {
	Calendrier calendrier = new Calendrier();
	calendrier.setId( resultSet.getLong( "id" ) );
	calendrier.setStatus(resultSet.getLong("status"));
	calendrier.setId_user(resultSet.getLong("id_user"));
	calendrier.setId_projet(resultSet.getLong("id_projet"));
	calendrier.setAnnee(resultSet.getLong("annee"));
	calendrier.setMois(resultSet.getLong("mois"));
	calendrier.setTotal_jour(resultSet.getDouble("total_jour"));
	List<Float> jours= new ArrayList<Float>();
	
	for (int i = 1; i < 32; i++) {
	    String index="j"+i;
	    jours.add(resultSet.getFloat(index));
	}
	calendrier.setJours(jours);
	return calendrier;
    }
    
    /**
     * Lister calendrier.
     *
     * @param utilisateur the utilisateur
     * @param mois the mois
     * @param annee the annee
     * @return the list
     */
    public List<Calendrier> listerCalendrier(Utilisateur utilisateur, int mois, int annee) {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Calendrier> listeCalendar = new ArrayList<Calendrier>();
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT, false, utilisateur.getId(), mois, annee);
	    resultSet = preparedStatement.executeQuery();
	    int i=0;
	    while ( resultSet.next() ) {		            	
		listeCalendar.add(i, map( resultSet ) );
		i++;
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return listeCalendar;
    }
    
    public int getStatus(final Long id_user, final int mois,
	    final int annee) {
	Connection connection = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	int status = 0;
	try {
	    connection = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee(connection,
		    SQL_STATUS_USER, false, id_user, mois, annee);
	    resultSet = preparedStatement.executeQuery();
	    if (resultSet.next()) {
		status = resultSet.getInt("status");
	    }
	}
	catch (SQLException e) {
	    e.printStackTrace();
	}
	finally {
	    fermeturesSilencieuses(resultSet, preparedStatement, connection);
	}
	return status;
    }
    
    /**
     * Lister jours feriers.
     *
     * @param utilisateur the utilisateur
     * @param mois the mois
     * @param annee the annee
     * @return the list
     */
    public List<Float> listerJoursFeriers(Utilisateur utilisateur, int mois, int annee) {
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	List<Float> JoursFeriers = new ArrayList<Float>();
	try {
	    connexion = daoFactory.getConnection();
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT, false, utilisateur.getId(), mois, annee);
	    resultSet = preparedStatement.executeQuery();
	    
	    while ( resultSet.next() ) {	
		for (int i = 1; i < 32; i++) {
		    String index="j"+i;
		    if(resultSet.getFloat(index)==-1)
			JoursFeriers.add(i,resultSet.getFloat(index));
		}
		
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	} finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	
	return JoursFeriers;
    }
    
    
    @Override
    public float count_jour(Long id_user, Long id_projet, Long annee, Long mois)
	    throws DAOException {
	// TODO Auto-generated method stub
	Connection connexion = null;
	PreparedStatement preparedStatement = null;
	float nombre = 0;
	ResultSet resultSet = null;
	
	try {
	    /* Récupération d'une connexion depuis la Factory */
	    connexion = daoFactory.getConnection();
	    
	    preparedStatement = initialisationRequetePreparee( connexion, SQL_COUNT_JOUR, false, id_user, id_projet, annee, mois );
	    resultSet = preparedStatement.executeQuery();
	    if(resultSet!=null)
	    {
		while(resultSet.next())
		    nombre = resultSet.getFloat(1);
		//nombre = ((Number) resultSet.getObject(1)).intValue();
	    }
	    
	} catch ( SQLException e ) {
	    throw new DAOException( e );
	}
	catch (NullPointerException e) {
	    // TODO: handle exception
	    throw new DAOException( e );
	}
	finally{
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	}
	return nombre;
    }
    
    
}
