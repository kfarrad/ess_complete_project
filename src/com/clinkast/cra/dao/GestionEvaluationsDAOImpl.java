package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;

public class GestionEvaluationsDAOImpl implements GestionEvaluationsDAO {
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO gestion_evaluations (" +
			"auto_evaluations_auto_evaluations_id, " +
			"dates_dates_id, " +
			"user_id, " +
			"gestion_evaluations_dateReception, " +
			"gestion_evaluations_dateModification, " +
			"gestion_evaluations_dateTransmission," +
			"gestion_evaluations_dateValidation, " +
			"gestion_evaluations_rejet, " +
			"gestion_evaluations_nbreModification" +
			") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE gestion_evaluations SET  gestion_evaluations_dateModification=?, gestion_evaluations_dateTransmission=?, gestion_evaluations_dateValidation=?, gestion_evaluations_rejet=?, gestion_evaluations_nbreModification=? WHERE auto_evaluations_auto_evaluations_id = ? AND dates_dates_id = ? AND user_id = ?";
	/*
	 * Constante SQL_SELECT_BY_DATE
	 */
	private static final String SQL_SELECT_BY_DATE = "SELECT * FROM gestion_evaluations WHERE dates_dates_id = ?";
	/*
	 * Constante SQL_SELECT_BY_USER
	 */
	private static final String SQL_SELECT_BY_USER = "SELECT * FROM gestion_evaluations WHERE user_id = ?";
	
	private static final String SQL_SELECT_BY_AUTO_USER = "SELECT * FROM gestion_evaluations WHERE auto_evaluations_auto_evaluations_id = ? AND user_id = ?";
	
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM gestion_evaluations WHERE gestion_evaluations_id = ?";
	
	private static final String SQL_SELECT_BY_ALL = "SELECT * FROM gestion_evaluations WHERE gestion_evaluations_dateTransmission is not null";
	/*
	 * Constante SQL_SELECT_BY_KEY : Recherche la ligne avec les 3 keys
	 */
	private static final String SQL_SELECT_BY_KEY = "SELECT * FROM gestion_evaluations WHERE auto_evaluations_auto_evaluations_id = ? AND dates_dates_id = ? AND user_id = ?";
    
	private static final String SQL_NBRE_ENVOI = "SELECT count(*) FROM gestion_evaluations where dates_dates_id = ?";
	
	private static final String SQL_NBRE_RECU = "SELECT count(*) FROM gestion_evaluations where dates_dates_id = ? and gestion_evaluations_dateTransmission is not null";
	
	private static final String SQL_NBRE_REJET = "SELECT count(*) FROM gestion_evaluations where dates_dates_id = ? and gestion_evaluations_rejet is not null";
	
	private DAOFactory daoFactory;
	
	public GestionEvaluationsDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}
	@Override
	public void creer(GestionEvaluations gestionEvaluations)
			throws DAOException {		
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, false, 
            		gestionEvaluations.getAutoEvaluations().getAutoEvaluationsId(),
            		gestionEvaluations.getDates().getDatesId(), 
            		gestionEvaluations.getUser().getId(), 
            		new Timestamp(gestionEvaluations.getGestionEvaluationsDateReception().getMillis()));
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une gestion d'autoEvaluation, aucune ligne ajoutée dans la table." );
                }
            	
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }

	}

	@Override
	public void modifier(GestionEvaluations gestionEvaluations) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        Timestamp dateModification = null;
        Timestamp dateTransmission = null;
        Timestamp dateValidation = null;
        Timestamp dateRejet = null;
        if(gestionEvaluations.getGestionEvaluationsDateModification() != null)
        	dateModification = new Timestamp(gestionEvaluations.getGestionEvaluationsDateModification().getMillis());
        
        if(gestionEvaluations.getGestionEvaluationsDateTransmission() != null)
        	dateTransmission = new Timestamp(gestionEvaluations.getGestionEvaluationsDateTransmission().getMillis());
        
        if(gestionEvaluations.getGestionEvaluationsDateValidation() != null)
        	dateValidation = new Timestamp(gestionEvaluations.getGestionEvaluationsDateValidation().getMillis());
        
        if(gestionEvaluations.getGestionEvaluationsRejet() != null)
        	dateRejet = new Timestamp(gestionEvaluations.getGestionEvaluationsRejet().getMillis());
        
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		dateModification, dateTransmission,dateValidation,dateRejet,            		
            		gestionEvaluations.getGestionEvaluationsNbreModification(),
            		gestionEvaluations.getAutoEvaluations().getAutoEvaluationsId(),
            		gestionEvaluations.getDates().getDatesId(), 
            		gestionEvaluations.getUser().getId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
		//return gestionEvaluations;
	}
	@Override
	public List<GestionEvaluations> lister_By_ALL() throws DAOException{
		List<GestionEvaluations> gestionEvaluations = new ArrayList<GestionEvaluations>();
		try {
            gestionEvaluations = retrouve(SQL_SELECT_BY_ALL);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return gestionEvaluations;
	}
	@Override
	public List<GestionEvaluations> lister_By_User(Utilisateur utilisateur) throws DAOException {
		// TODO Auto-generated method stub
		List<GestionEvaluations> gestionEvaluations = new ArrayList<GestionEvaluations>();
		try {
        	gestionEvaluations = retrouve(SQL_SELECT_BY_USER, utilisateur.getId());
           
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
       
		return gestionEvaluations;
	}
	@Override
	public List<GestionEvaluations> lister_By_Dates(Long idDates) throws DAOException{
		List<GestionEvaluations> gestionEvaluations = new ArrayList<GestionEvaluations>();
		try {
            gestionEvaluations = retrouve(SQL_SELECT_BY_DATE, idDates);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return gestionEvaluations;
	}
	
	@Override
	public GestionEvaluations trouver_By_KEY(Long idAutoEvaluation,	Long idDate, Long idUser) throws DAOException {
		// TODO Auto-generated method stub
		GestionEvaluations gestionEvaluations = null;  
        try {
        	gestionEvaluations = trouve(SQL_SELECT_BY_KEY, idAutoEvaluation, idDate, idUser);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
        return gestionEvaluations;
	}
	
	public GestionEvaluations trouver_By_KEY(Long idAutoEvaluation, Long idUser) throws DAOException {
		// TODO Auto-generated method stub
		GestionEvaluations gestionEvaluations = null;  
        try {
        	gestionEvaluations = trouve(SQL_SELECT_BY_AUTO_USER, idAutoEvaluation, idUser);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
        return gestionEvaluations;
	}
	public GestionEvaluations trouver_By_ID(Long idGestionAutoEvaluation) throws DAOException {
		// TODO Auto-generated method stub
		//ResultSet resultSet = null;
		GestionEvaluations gestionEvaluations = null;  
        try {
        	gestionEvaluations = trouve(SQL_SELECT_BY_ID, idGestionAutoEvaluation);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        return gestionEvaluations;
	}
	
	/**
	 * 
	 * @param sql
	 * @param objects
	 * @return un tableau de resultSet
	 * @throws SQLException
	 */
	private List<GestionEvaluations> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<GestionEvaluations> gestionEvaluations = new ArrayList<GestionEvaluations>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	gestionEvaluations.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return gestionEvaluations;
	}
	private GestionEvaluations trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        GestionEvaluations gestionEvaluations = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	gestionEvaluations = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return gestionEvaluations;
	}
	
	public int nombreRenvoi(Long idDate) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        int nombre = 0;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_NBRE_ENVOI, false, idDate );
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next())
            	nombre = ((Number) resultSet.getObject(1)).intValue();
            
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return nombre;
	}
	@Override
	public int nombreRecu(Long idDate) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        int nombre = 0;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_NBRE_RECU, false, idDate );
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next())
            	nombre = ((Number) resultSet.getObject(1)).intValue();
            
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return nombre;
	}
	@Override
	public int nombreRejet(Long idDate) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        int nombre = 0;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_NBRE_REJET, false, idDate );
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next())
            	nombre = ((Number) resultSet.getObject(1)).intValue();
            
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return nombre;
	}
	
	/*private ResultSet retrouver(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
       
        try {
             Récupération d'une connexion depuis la Factory 
            connexion = daoFactory.getConnection();
            
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } 
		return resultSet;
	}*/
	/**
	 * Map fabrique un objet de type SystemesActivites
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private GestionEvaluations map( ResultSet resultSet ) throws SQLException {
		UserDaoImpl userDAOImpl = daoFactory.getUserDao();
		AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		DatesDAOImpl datesDAOImpl = daoFactory.getDatesDAOImpl();
		GestionEvaluations gestionEvaluations = new GestionEvaluations();
		gestionEvaluations.setUser(userDAOImpl.trouver_ID(resultSet.getLong("user_id")));
		gestionEvaluations.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));
		gestionEvaluations.setDates(datesDAOImpl.trouver(resultSet.getLong("dates_dates_id")));
		gestionEvaluations.setGestionEvaluationsNbreModification(resultSet.getInt("gestion_evaluations_nbreModification"));
		
		/*Formatage des dates*/
		if(resultSet.getTimestamp("gestion_evaluations_dateReception") != null)
			gestionEvaluations.setGestionEvaluationsDateReception(new DateTime(resultSet.getDate("gestion_evaluations_dateReception")));
		else
			gestionEvaluations.setGestionEvaluationsDateReception(null);
		
		if(resultSet.getTimestamp("gestion_evaluations_dateModification") != null)
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime(resultSet.getTimestamp("gestion_evaluations_dateModification")));
		else
			gestionEvaluations.setGestionEvaluationsDateModification(null);
		
		if(resultSet.getTimestamp("gestion_evaluations_dateTransmission") != null)
			gestionEvaluations.setGestionEvaluationsDateTransmission(new DateTime(resultSet.getTimestamp("gestion_evaluations_dateTransmission")));
		else
			gestionEvaluations.setGestionEvaluationsDateTransmission(null);
		
		if(resultSet.getTimestamp("gestion_evaluations_dateValidation") != null)
			gestionEvaluations.setGestionEvaluationsDateValidation(new DateTime(resultSet.getTimestamp("gestion_evaluations_dateValidation")));
		else
			gestionEvaluations.setGestionEvaluationsDateValidation(null);
		
		if(resultSet.getTimestamp("gestion_evaluations_rejet") != null)
			gestionEvaluations.setGestionEvaluationsRejet(new DateTime(resultSet.getTimestamp("gestion_evaluations_rejet")));
		else
			gestionEvaluations.setGestionEvaluationsRejet(null);
		
		return gestionEvaluations;
    }
}
