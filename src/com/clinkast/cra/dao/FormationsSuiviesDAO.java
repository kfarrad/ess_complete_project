package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.FormationsSuivies;

public interface FormationsSuiviesDAO {
	/**
	 * 
	 * @param formationsSuivies
	 * @throws DAOException
	 */
	public void creer(FormationsSuivies formationsSuivies) throws DAOException;
	/**
	 * 
	 * @param formationsSuivies
	 * @param idFormationsSuivies
	 * @return
	 * @throws DAOException
	 */
	public void modifier(FormationsSuivies formationsSuivies) throws DAOException;
	/**
	 * 
	 * @param idAutoEvaluation
	 * @return
	 * @throws DAOException
	 */
	public List<FormationsSuivies> lister(Long idAutoEvaluation) throws DAOException;
	/**
	 * 
	 * @param idFormationsSuivies
	 * @return
	 * @throws DAOException
	 */
	public FormationsSuivies trouver(Long idFormationsSuivies) throws DAOException;
	/**
	 * 
	 * @param formationsSuivies
	 * @throws DAOException
	 */
	public void supprimer(FormationsSuivies formationsSuivies) throws DAOException;
}
