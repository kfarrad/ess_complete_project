package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Contenu;

public interface ContenuDao {
	/**
 	 * Creer le contenu.
 	 *
 	 * @param contenu le contenu à ajouter 
 	 * @throws DAOException the DAO exception
 	 */
 	void creer( Contenu contenu ) throws DAOException;
	    
	 /**
 	 * Modifier le contenu.
 	 *
 	 * @param candidature le contenu à modifier
 	 * @param id l'id du contenu à modifier
 	 * @return le Contenu modifié
 	 * @throws DAOException the DAO exception
 	 */
 	Contenu modifier( Contenu contenu, Long id) throws DAOException;

	 /**
 	 * Supprimer le contenu.
 	 *
 	 * @param id 
 	 * 			id du contenu à supprimer
 	 * @throws DAOException 
 	 * 			the DAO exception
 	 */
 	void supprimer( long id ) throws DAOException;
	 
	 /**
 	 * Lister les contenus.
 	 *
 	 * @return la liste des contenus
 	 * @param debut entier représentant le début de la selection
 	 * @param nombreContenu entier représentatnt le nombre des contenu à selectionner
 	 * @throws DAOException the DAO exception
 	 */
 	List<Contenu> lister(int debut, int nombreContenu) throws DAOException;
}
