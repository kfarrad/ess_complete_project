package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.RappelGeneral;


public interface RappelGeneralDao {
	void modifier( RappelGeneral  rg, int id);
	public List<RappelGeneral> lister() throws DAOException;
	RappelGeneral trouver(long moisCourant );
}
