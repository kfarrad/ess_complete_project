package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Dates;

public class DatesDAOImpl implements DatesDAO {
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO dates ( dates_periodes, dates_periodesDe, dates_periodesAu, dates_annee, dates_creation) VALUES (?, ?, ?, ?, ?)";
	
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_KEY = "SELECT * FROM dates WHERE dates_periodesDe = ? AND dates_periodesAu = ?";
	
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM dates WHERE dates_id = ?";
	
	private static final String SQL_SELECT = "SELECT * FROM dates ORDER BY dates_annee DESC";
    
	private static final String SQL_UPDATE = "UPDATE dates SET  dates_envoi=? WHERE dates_id = ?";
	
	private DAOFactory daoFactory;
	
	public DatesDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
		}
	@Override
	public void creer(Dates dates) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();
            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, dates.getDatesPeriodes(), dates.getDatesPeriodesDe(), dates.getDatesPeriodesAu(), dates.getDatesAnnee(), new Timestamp(dates.getDatesCreation().getMillis()));
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'une date, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	                dates.setDatesId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'une date, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }


	}

	@Override
	public void modifier(Dates dates) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		new Timestamp(dates.getDatesEnvoi().getMillis()), dates.getDatesId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}

	@Override
	public List<Dates> lister() throws DAOException {
		// TODO Auto-generated method stub
		List<Dates> listdates = new ArrayList<Dates>();  
        try {
        	listdates = retrouve(SQL_SELECT);
            
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return listdates;
	}
	
	@Override
	public Dates trouver(Long idDate) throws DAOException {
		// TODO Auto-generated method stub
		Dates dates = null;  
        try {
            dates = trouve(SQL_SELECT_BY_ID, idDate);
            
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return dates;
	}
	
	public boolean isExist(Dates dates) throws DAOException {
		// TODO Auto-generated method stub
		 Dates uneDate = null; 
        try {
            uneDate = trouve(SQL_SELECT_BY_KEY, dates.getDatesPeriodesDe(), dates.getDatesPeriodesAu());
            
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        if(uneDate == null)
        	return false;
        else
        	return true;
	}
	
	private Dates map(ResultSet resultSet) throws SQLException{
		// TODO Auto-generated method stub
		Dates dates = new Dates();
		dates.setDatesId(resultSet.getLong("dates_id"));
		dates.setDatesPeriodes(resultSet.getString("dates_periodes"));
		dates.setDatesPeriodesDe(resultSet.getString("dates_periodesDe"));
		dates.setDatesPeriodesAu(resultSet.getString("dates_periodesAu"));
		dates.setDatesAnnee(resultSet.getString("dates_annee"));
		
		dates.setDatesCreation(new DateTime(resultSet.getTimestamp("dates_creation")));
		dates.setDatesNbreEnvoi(daoFactory.getGestionEvaluationsDAOImpl().nombreRenvoi(dates.getDatesId()));
		dates.setDatesNbreRecu(daoFactory.getGestionEvaluationsDAOImpl().nombreRecu(dates.getDatesId()));
		dates.setDatesNbreRejet(daoFactory.getGestionEvaluationsDAOImpl().nombreRejet(dates.getDatesId()));
		//Formate la date
		if(resultSet.getTimestamp("dates_envoi") != null)
			dates.setDatesEnvoi(new DateTime(resultSet.getTimestamp("dates_envoi")));
		else
			dates.setDatesEnvoi(null);
		
		return dates;
	}

	private List<Dates> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<Dates> dates = new ArrayList<Dates>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	dates.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return dates;
	}
	private Dates trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        Dates dates = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	dates = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return dates;
	}
	
	
	

}
