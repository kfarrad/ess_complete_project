package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.UserProfil;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserProfilDao.
 */
public interface UserProfilDao {
    
    /**
     * Creer.
     *
     * @param client the client
     * @throws DAOException the DAO exception
     */
    void creer( UserProfil client ) throws DAOException;

    /**
     * Trouver.
     *
     * @param id the id
     * @return the user profil
     * @throws DAOException the DAO exception
     */
    UserProfil trouver( long id ) throws DAOException;
    
    /**
     * Activer.
     *
     * @param id the id
     * @return the user profil
     * @throws DAOException the DAO exception
     */
    UserProfil activer(long id) throws DAOException;  
    
    /**
     * Modifier.
     *
     * @param client the client
     * @param id the id
     * @return the user profil
     * @throws DAOException the DAO exception
     */
    UserProfil modifier( UserProfil client, Long id) throws DAOException;

    /**
     * Lister.
     *
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<UserProfil> lister() throws DAOException;

    /**
     * Supprimer.
     *
     * @param client the client
     * @throws DAOException the DAO exception
     */
    void supprimer( UserProfil client ) throws DAOException;
    
    /**
     * Liste des utilisaturs par ordre alphabtique.
     */
    
    List<UserProfil> listerParOrdreAlphabetique (int offset, int noOfRecords ) throws DAOException;
}