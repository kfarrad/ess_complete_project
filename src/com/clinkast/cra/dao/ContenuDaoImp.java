package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Contenu;

public class ContenuDaoImp implements ContenuDao {

	/** The Constant SQL_INSERT INTO Contenu. */
	private static final String SQL_INSERT = "INSERT INTO 	contenu (titre, contenu_pub) VALUES (?, ?)";

	private static final String SQL_UPDATE = "UPDATE contenu SET titre = ?, contenu_pub = ? WHERE id = ?";;

	/** The dao factory. */
	private DAOFactory daoFactory;

	private int noOfRecords;

	ContenuDaoImp(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public void creer(Contenu contenu) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = initialisationRequetePreparee(connexion,
					SQL_INSERT, true, contenu.getTitre(), contenu.getContenu());
			int statut = preparedStatement.executeUpdate();

			if (statut == 0) {
				throw new DAOException(
						"Echec de la création de la candidature, aucune ligne ajoutée dans la table.");
			}
			valeursAutoGenerees = preparedStatement.getGeneratedKeys();

			if (valeursAutoGenerees.next()) {
				contenu.setId(valeursAutoGenerees.getLong(1));

			} else {
				throw new DAOException(
						"Echec de la création de la candidature en base, aucun ID auto-généré retourné.");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
					connexion);
		}

	}

	@Override
	public Contenu modifier(Contenu contenu, Long id) throws DAOException {Connection connexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet valeursAutoGenerees = null;
	try {
		connexion = daoFactory.getConnection();

		preparedStatement = initialisationRequetePreparee(connexion,
				SQL_UPDATE, true, contenu.getTitre(),
				contenu.getContenu(), id);
		int statut = preparedStatement.executeUpdate();

		if (statut == 0) {
			throw new DAOException(
					"Echec de la modification de la candidature, aucune ligne modifiée dans la table.");
		}

	} catch (SQLException e) {
		throw new DAOException(e);
	} finally {
		fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
				connexion);
	}
	return null;}

	@Override
	public void supprimer(long id) throws DAOException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		String request = "DELETE FROM `clinkastcra`.`contenu` WHERE `contenu`.`id` = "
				+ id;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DAOException(
						"Echec de la suppression du contenu, aucune ligne supprimée dans la base de données.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			fermeturesSilencieuses(valeursAutoGenerees, preparedStatement,
					connexion);
		}
	
	}

	@Override
	public List<Contenu> lister(int debut, int nombreContenu)
			throws DAOException {

		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet results = null;
		List<Contenu> contenus = new ArrayList<Contenu>();
		String request = "SELECT SQL_CALC_FOUND_ROWS * FROM contenu LIMIT "
				+ debut + ", " + nombreContenu;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			results = preparedStatement.executeQuery();
			while (results.next()) {
				contenus.add(doMapping(results));
			}
			results = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
			if (results.next())
				this.noOfRecords = results.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(results, preparedStatement, connexion);
		}
		return contenus;
	}

	private Contenu doMapping(ResultSet results) throws SQLException {

		Contenu contenu = new Contenu();
		contenu.setId(results.getLong("id"));
		contenu.setTitre(results.getString("titre"));
		contenu.setContenu(results.getString("contenu_pub"));
		contenu.setDateCreation(results.getTimestamp("date"));

		return contenu;

	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

	public Contenu getContenuById(long id) {

		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Contenu contenu = new Contenu();
		String request = "SELECT * FROM contenu WHERE id = " + id;
		try {

			connexion = daoFactory.getConnection();
			preparedStatement = connexion.prepareStatement(request);
			result = preparedStatement.executeQuery();
			if (result.next()) {
				contenu = doMapping(result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			fermeturesSilencieuses(result, preparedStatement, connexion);
		}
		return contenu;
	
	}

}
