package com.clinkast.cra.dao;

import static com.clinkast.cra.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.clinkast.cra.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.clinkast.cra.beans.Objectifs;

public class ObjectifsDAOImpl implements ObjectifsDAO {
	
	/*
	 * Constante  SQL INSERT
	 */
	private static final String SQL_INSERT = "INSERT INTO objectifs ( objectifs_libelle, objectifs_estimation, auto_evaluations_auto_evaluations_id) VALUES (?, ?, ?)";
	/*
	 * Constante SQL_UPDATE. 
	 */
	private static final String SQL_UPDATE = "UPDATE objectifs SET  objectifs_libelle=?, objectifs_estimation=? WHERE objectifs_id = ?";
	/*
	 * Constante SQL_DELETE
	 */
	private static final String SQL_DELETE = "DELETE FROM objectifs WHERE objectifs_id = ?";
	/*
	 * Constante SQL_SELECT_ALL
	 */
	private static final String SQL_SELECT_ALL = "SELECT * FROM objectifs WHERE auto_evaluations_auto_evaluations_id = ?";
	/*
	 * Constante SQL_SELECT_BY_ID
	 */
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM objectifs WHERE objectifs_id = ?";
    
	private DAOFactory daoFactory;
	
	public ObjectifsDAOImpl(DAOFactory daoFactory){
		this.daoFactory = daoFactory;
	}

	@Override
	public void creer(Objectifs objectifs) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;
        try {
            connexion = daoFactory.getConnection();            
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, 
            		objectifs.getObjectifsLibelle(), 
            		objectifs.getObjectifsEstimation(), 
            		objectifs.getAutoEvaluations().getAutoEvaluationsId());
              	int statut = preparedStatement.executeUpdate();
            	
            	if ( statut == 0 ) {
                throw new DAOException( "Échec de la création d'unobjectif, aucune ligne ajoutée dans la table." );
                }
            	/* Récupération de l'id auto-généré par la requête
            	d'insertion */
            	valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            
	            if ( valeursAutoGenerees.next() ) {            	
	            	objectifs.setObjectifsId(valeursAutoGenerees.getLong( 1 ) );

	            } else {
	                throw new DAOException( "Échec de la création d'un objectif, aucun ID auto-généré retourné." );
	            }
	       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
	}

	@Override
	public Objectifs modifier(Objectifs objectifs)
			throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false,
            		objectifs.getObjectifsLibelle(), objectifs.getObjectifsEstimation(), objectifs.getObjectifsId());
            
            int statut = preparedStatement.executeUpdate();
          
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la modification  de la formation suivie, aucune ligne modifiée dans la table." );
            }
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
		return objectifs;
	}

	@Override
	public List<Objectifs> lister(Long idAutEvaluation) throws DAOException {
		// TODO Auto-generated method stub
		List<Objectifs> objectifs = new ArrayList<Objectifs>();
		try {
            objectifs = retrouve(SQL_SELECT_ALL, idAutEvaluation);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
		return objectifs;
	}

	private Objectifs map(ResultSet resultSet) throws SQLException{
		// TODO Auto-generated method stub
		//AutoEvaluationDAOImpl autoEvaluationDAOImpl = daoFactory.getAutoEvaluationDAOImpl();
		Objectifs objectifs = new Objectifs();
		objectifs.setObjectifsId(resultSet.getLong("objectifs_id") );	    	       
		objectifs.setObjectifsLibelle(resultSet.getString("objectifs_libelle"));
		objectifs.setObjectifsEstimation(resultSet.getString("objectifs_estimation"));
		//objectifs.setAutoEvaluations(autoEvaluationDAOImpl.trouver(resultSet.getLong("auto_evaluations_auto_evaluations_id")));
       
        return objectifs;
	}

	@Override
	public Objectifs trouver(Long idObjectifs) throws DAOException {
		// TODO Auto-generated method stub
		Objectifs objectifs = null;  
        try {
            objectifs = trouve(SQL_SELECT_BY_ID, idObjectifs);
            
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        
		return objectifs;
	}

	@Override
	public void supprimer(Objectifs objectifs) throws DAOException {
		// TODO Auto-generated method stub
		Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE, true, objectifs.getObjectifsId());
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la suppression de la formation , aucune ligne supprimée de la table." );
            } else {
            	objectifs = null;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }	
	}
	
	private List<Objectifs> retrouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<Objectifs> objectifs = new ArrayList<Objectifs>();
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	objectifs.add(map( resultSet ));
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return objectifs;
	}
	private Objectifs trouve(String sql, Object...objects)throws SQLException{
		Connection connexion = null;
        PreparedStatement preparedStatement = null;
        Objectifs objectifs = null;
        ResultSet resultSet = null;
       
        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            /*
             * Préparation de la requête avec les objets passés en arguments
             * (ici, uniquement un id) et exécution.
             */
            preparedStatement = initialisationRequetePreparee( connexion, sql, false, objects );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	objectifs = map( resultSet );
            }
                       
        } catch ( SQLException e ) {
            throw new DAOException( e );
        }
        finally{
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }
		return objectifs;
	}


}
