package com.clinkast.cra.dao;

import java.util.List;

import com.clinkast.cra.beans.Rapports;

public interface RapportsDAO {
	/**
	 * 
	 * @param rapports
	 * @throws DAOException
	 */
	public void creer(Rapports rapports) throws DAOException;
	/**
	 * 
	 * @param rapports
	 * @param idCompetences
	 * @param idAutoEvaluation
	 * @return
	 * @throws DAOException
	 */
	public Rapports modifier(Rapports rapports) throws DAOException;
	/**
	 * 
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	public List<Rapports> lister(Long idEvaluation)throws DAOException;
	/**
	 * 
	 * @param idCompetences
	 * @param idEvaluation
	 * @return
	 * @throws DAOException
	 */
	public Rapports trouver_By_ID(Long idCompetences, Long idEvaluations) throws DAOException;
	public Rapports trouver_By_ALL(Long idEvaluations) throws DAOException;
	
}
