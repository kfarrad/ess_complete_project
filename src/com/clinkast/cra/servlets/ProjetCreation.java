package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.dao.ClientDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.forms.ProjetCreationForm;


// TODO: Auto-generated Javadoc
/**
 * The Class ProjetCreation.
 */
@WebServlet( "/projetCreation" )
public class ProjetCreation extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 	= 1L;

	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/admin/projetListing.jsp";
	
	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/projetCreate.jsp";

	/** The projet dao. */
	private ProjetDaoImpl          projetDao;
	
	/** The user dao. */
	private UserDaoImpl				userDao;
	
	/** The client dao. */
	private ClientDaoImpl          clientDao;
	
	/** The id_client. */
	private Long 					id_client;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */

		this.projetDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getClientDao();
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();

	}

	

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */
		
		String idClient = Constants.getValeurChamp( request, Constants.ATT_ID_CLIENT );
		id_client = Long.parseLong( idClient );
        
        /* recuperation de l'objet client à modifier */ 
        try {
        	Client client = clientDao.trouver(id_client);
        	if(client!=null) request.setAttribute( Constants.ATT_CLIENT, client );
        } catch ( DAOException e ) {
            e.printStackTrace();
        }
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@SuppressWarnings("unchecked")
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		ProjetCreationForm form = new ProjetCreationForm(projetDao);
		HttpSession session = request.getSession();
		
        
        Map<Projet, List<String>> projetsUtilisateurs  = (HashMap<Projet, List<String>>) session.getAttribute( Constants.ATT_PROJETS_USERS );
		/* Si aucune map n'existe, alors initialisation d'une nouvelle map */
		if ( projetsUtilisateurs == null ) {
			List<Projet> listeProjets = projetDao.lister();
			projetsUtilisateurs = new HashMap<Projet, List<String>>();
			for ( Projet elt : listeProjets ) {
	        	List<String> listeProjetsUsers = userDao.listerProjetsUsers(elt);
	        	
	        	projetsUtilisateurs.put( elt, listeProjetsUsers );
	        }
		}
		Projet projet = form.creerProjet(request, id_client);

		request.setAttribute( Constants.ATT_FORM, form ); 
		if(form.getErreurs().isEmpty()){
			request.setAttribute( Constants.ATT_PROJET, projet );      
	       
			
			projetsUtilisateurs.put( projet, null );
	        session.setAttribute( Constants.ATT_PROJETS_USERS, projetsUtilisateurs );
			
			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
			}
			else {
	        	// Sinon, ré-affichage du formulaire de création avec les erreurs 

				Client client = clientDao.trouver(id_client);
	        	if(client!=null) request.setAttribute( Constants.ATT_CLIENT, client );
			 this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
			}
	}
	
}
