package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.DetailsProjet;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.mails.MailSender;

@WebServlet(name="ViewBilans", urlPatterns="/viewBilans")
public class ViewBilans extends HttpServlet {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID 			= 1L;


    /** The Constant VUE. */
    public static final String VUE      		= "/pages/protected/admin/viewBilan.jsp";

    public static final String VUE_ECHEC 		= "/pages/protected/defaultUser/workingDayResult.jsp";

    /** The calendar dao. */
    private CalendarDaoImpl        		    calendarDao;

    /** The projet dao. */
    private ProjetDaoImpl					projetDao;

    /** The profil dao. */

    /** The utilisateur dao. */
    private UserDaoImpl		  			    utilisateurDao;

    /** The liste projets. */
    private List<Projet>   		   			listeProjets;
    private Utilisateur			            utilisateur;

    public static final String PARAM_ANNEE = "annee";
    public static final String PARAM_MOIS = "mois";
    public static final int NOMBRE_DE_FOIS_CONGES_PRIS_PAR_MOIS = 5;//nombre maximal de fois qu'une consultant peut prendre les congés en decallé dans un mois
    public static final String VUE_REDIRECTION = "/calendarBilan";
    public static final String VUE_REDIRECTION_ADMIN = "/gestionAutoEvaluations";
    GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;



    public void init() throws ServletException {
	/* Récupération d'une instance de nos DAO Dates et Users */
	this.calendarDao 		= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
	this.projetDao 			= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
	this.utilisateurDao 	= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();

    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	/*liste des tous les users*/
	String annee = Constants.getValeurChamp(request, PARAM_ANNEE);
	String mois = Constants.getValeurChamp(request, PARAM_MOIS);
	String emailIDs = new String();
	int intAnnee, intMois = 0;
	Map<String, Map<DetailsProjet, Map<Integer, Double>>> mapUser = new HashMap<String, Map<DetailsProjet, Map<Integer, Double>>>();
	intAnnee = Integer.parseInt(annee);
	intMois = Integer.parseInt(mois);
	/*
	 * verifie que l'année et la mois sont passés en parametre
	 */
	if(annee == null || mois == null){
	    //retour à la page bilan
	    response.sendRedirect( request.getContextPath() + VUE_REDIRECTION );
	}
	else{
	    //Mois de l'année
	    String[] month = {"Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"}; 

	    //Jours ferriés du mois selectionné
	    Calendrier jourFeriers = calendarDao.trouver(-1L, -1L, (long)intAnnee,(long) intMois);
	    List<Integer> feriers = new ArrayList<Integer>();

	    if(jourFeriers!=null){
		for(Float ferier:jourFeriers.getJours()){
		    if(ferier==-1) feriers.add(-1);
		    else feriers.add(0);
		}
	    }
	    if(feriers.isEmpty()){
		for(int i = 0; i<31; i++){
		    feriers.add(0);
		}
	    }
	    //valeurs sont passées
	    //liste tous les users
	    List<Utilisateur> utilisateurs = utilisateurDao.listerActif();
	    //Liste ds users qui n'ont pas encore validé
	    List<String> pers = new ArrayList<String>();

	    /*
	     * Pour chaque user
	     */
	    //Verifie que tous les utilisateurs ont validé leur jour
	    for(Utilisateur user : utilisateurs)
	    {
		String name = null; 
		//Recuperation de emails de admin et rh
		if(user.isAdmin() || user.isRh()) emailIDs += user.getLogin()+",";
		if(user.isUser())
		{
		    //liste de calendrier present
		    List<Calendrier> calendriers = calendarDao.listerCalendrier(user, intMois, intAnnee);
		    //Si un consultant n'a pas de projet, cal null
		    if(calendriers.size()== 0 ) name = user.getName();
		    for(Calendrier cal : calendriers){
			if(cal.getStatus() != 1)
			{
			    if(name != null) name = user.getName();
			}
		    }
		}
		if(name != null) pers.add(name);
	    }
	    if(!pers.isEmpty())
	    {
		request.setAttribute("annee" , annee );
		request.setAttribute("mois" , month[intMois]);
		request.setAttribute("pers" , pers);
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );

	    }
	    else
	    {
		for(Utilisateur user : utilisateurs)
		{

		    if(user.isUser())
		    {
			//liste des projets actifs
			listeProjets = projetDao.listerUserProjets(user);
			//liste de calendriers à definir
			List<Calendrier> listeCalendrier = new ArrayList<Calendrier>();

			//liste de calendrier present
			List<Calendrier> calendriers = calendarDao.listerCalendrier(user, intMois, intAnnee);

			//Map des projets    						
			Map<DetailsProjet, Map<Integer, Double>> mapProjets = new HashMap<DetailsProjet, Map<Integer, Double>>();// new HashMap<String, String[][]>();

			if( calendriers.size() != 0) {
			    for(Projet projet: listeProjets){
				for(Calendrier calendrier: calendriers){				 	
				    if(calendrier.getId_projet()==projet.getId()) listeCalendrier.add(calendrier);
				}    					 
			    }
			}
			//Parcours les projets et somme total_bilan et total_present
			if (!listeCalendrier.isEmpty() && !listeProjets.isEmpty())
			{
			    for (int j = 0; j < listeCalendrier.size(); j++)
			    {
				//id projet
				Long id_projet = listeCalendrier.get(j).getId_projet();
				//nom projet
				String nom_projet = null;
				//Nom du projet
				for (Projet projet : listeProjets) 
				{
				    if(projet.getNomProjet(id_projet) != null) nom_projet= projet.getNomProjet(id_projet) ;
				}
				//recherche les jours
				if (nom_projet.contains("CP") || nom_projet.contains("RTT") || nom_projet.contains("CSS") || nom_projet.contains("Arret"))
				{
				    DetailsProjet projet = new DetailsProjet();
				    int vendrediPris = 0;
				    projet.setNom(nom_projet);
				    //compte le nombre de vendredi pris en CP, aide à avoir le nombre de conges réel
				    if(nom_projet.contains("CP")){
					vendrediPris = Constants.getFridaysTaken(listeCalendrier.get(j).getJours(), intMois, intAnnee);
					projet.setNom("CP");
				    }

				    if(nom_projet.contains("RTT")) projet.setNom("RTT");
				    int debut = 0;//jour où le projet à debuter
				    double compteurJour = 0;//durée du projet soit 0.5 pour demi journée soit 1 pour journée pleine
				    //String[][]detailProjet = new String[nbre_projets_actifs][2];//tableau qui contient les infos sur un projet au mois
				    double nombreDeConges = 0;//Nombre réel de conges (CP) pris par mois
				    Map<Integer, Double> detailProjet = null;
				    //parcours les jours du mois
				    for (int i = 0; i < 31; i++) {
					//la valeur du jour est 1
					if(listeCalendrier.get(j).getJours().get(i) == 1)
					{
					    //jour de congés incrémenté
					    nombreDeConges++;

					    if(detailProjet == null) detailProjet = new HashMap<Integer, Double>();
					    //verifie que le projet n'a pas deja debuté
					    if(debut == 0)
					    {
						debut = i+1; //jour du mois où le projet debut
						//detailProjet[k][0] = String.valueOf(debut);
					    }
					    // le projet a deja debuté
					    compteurJour++;  								
					}
					else if(listeCalendrier.get(j).getJours().get(i) == 0.5)
					{
					    //jour de congés incrémenté
					    nombreDeConges+=0.5;

					    if(detailProjet == null) detailProjet = new HashMap<Integer, Double>();

					    if(debut != 0)
					    {
						//sauve la valeur compteurJour
						//detailProjet[k][1] = String.valueOf(compteurJour);
						detailProjet.put(debut, compteurJour);
						compteurJour = 0;
					    }
					    debut = i+1;
					    detailProjet.put(debut, 0.5);
					    //initialise les compteur
					    debut = 0; 
					    compteurJour = 0;
					}
					else{//listeCalendrier.get(j).getJours().get(i) == 0
					    if(debut != 0)
					    {
						Calendar mycal = new GregorianCalendar(intAnnee, intMois, 1);
						if(i+1<= mycal.getActualMaximum(Calendar.DAY_OF_MONTH)){   
						    //Lire le jour de la semaine pour savoir si cest un wekend
						    String date = (i+1)+"/"+(intMois+1)+"/"+annee;
						    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
						    DateTime dt = formatter.parseDateTime(date);
						    //System.out.println("la date est : " + dt.toString());
						    Integer day = dt.getDayOfWeek();
						    //System.out.println("le jour :" + day.toString());
						    //if(day == DateTimeConstants.SATURDAY || day == DateTimeConstants.SUNDAY)
						    //Si c'est un jour ferrié, ou un weekend, incremente le compteur
						    if(feriers.get(i)==-1 || day == DateTimeConstants.SATURDAY || day == DateTimeConstants.SUNDAY)
						    {
							if(!nom_projet.contains("RTT"))
							    compteurJour ++;
							//if(day==DateTimeConstants.SUNDAY) compteurJour --;
						    }
						    else
						    {
							//sauve la valeur compteurJour
							detailProjet.put(debut, compteurJour);
							//initialise les compteur
							debut = 0;
							compteurJour = 0;
						    }
						}

					    }    								
					}
					//Si la boucle est a la fin
					if(i == 30)
					{
					    if(debut != 0)
					    {
						//sauve la valeur compteurJour
						detailProjet.put(debut, compteurJour);
						//initialise les compteur
						debut = 0;
						compteurJour = 0;	    								
					    }
					}
				    }
				    projet.setDuree(nombreDeConges+vendrediPris);
				    //Ajout à la map projet
				    if(detailProjet != null) mapProjets.put(projet, detailProjet);    						
				}
			    }
			}
			else
			{
			    mapProjets = null;
			}
			mapUser.put(user.getName(), mapProjets); 
		    }
		}

		String email = emailIDs.substring(0, emailIDs.length()-1);
		request.setAttribute("email", email);
		request.setAttribute("annee" , annee );
		request.setAttribute("mois" , month[intMois]);
		request.setAttribute("detailsBilan" , mapUser);
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	    }
	}
    }
    public final void doPost( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
	HttpSession session = request.getSession();
	String body = Constants.getValeurChamp(request, "body");
	String titre = Constants.getValeurChamp(request, "titre");
	String receiver = Constants.getValeurChamp(request, "receiver");
	List<String> receiverEmailIDs = new ArrayList<String>();
	//Recupere l'objet user
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
	if(utilisateur!=null){
	    if(utilisateur.isAdmin())
	    {
		if(body != null && receiver != null && titre != null)
		{
		    String[] mails = receiver.split(",");
		    for(String mail : mails)
		    {
			receiverEmailIDs.add(mail);
		    }
		    //Envoi de mail
		    new MailSender(receiverEmailIDs, titre, body);
		    request.setAttribute("message" , "Mail envoyé avec succès");
		    this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		}
		else{
		    request.setAttribute("messages" , "Probleme survenu lors de l'envoi de mail");
		    this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		}
	    }
	}
	else{
	    request.setAttribute("messages" , "Probleme: l'utilisateur n'est pas identifié");
	    this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

    }
}