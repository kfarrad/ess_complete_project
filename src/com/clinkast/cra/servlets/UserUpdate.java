package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.forms.UserCreationForm;


// TODO: Auto-generated Javadoc
/**
 * The Class UserUpdate.
 */
@WebServlet( "/userUpdate" )
public class UserUpdate extends HttpServlet{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/defaultUser/userSucessUpdate.jsp";
	
	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES_ADMIN = "/pages/protected/admin/userSucessUpdateAdmin.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/defaultUser/userUpdating.jsp";
	
	/** The Constant VUE_FORM_ADMIN. */
	public static final String VUE_FORM_ADMIN   = "/pages/protected/admin/userUpdatingAdmin.jsp";


	/** The user dao. */
	private UserDaoImpl          				userDao;
	
	/** The id_profil. */
	private Long 								id_profil;
	
	/** The user insession. */
	private Utilisateur 						userInsession;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		HttpSession session = request.getSession();
		userInsession= (Utilisateur)(session.getAttribute( Constants.ATT_SESSION_USER ));

		/* Récupération du paramètre */
		String idProfil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
		

		/* Si l'id du profil n'est pas vide */
		if ( idProfil == null){
			/* Re-affichage du formulaire de modification */
			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );				
		}
		else{
			if(userInsession!= null){
				id_profil = Long.parseLong( idProfil );
				Utilisateur utilisateur = userDao.trouver(id_profil);
				if((userInsession.isAdmin())&&(userInsession.getId_profil()!=id_profil)){
					request.setAttribute(Constants.ATT_USER, utilisateur);
					this.getServletContext().getRequestDispatcher( VUE_FORM_ADMIN ).forward( request, response );
				}else{
					this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );		
				}
			}
			else  response.sendRedirect( request.getContextPath() + "/connexion" );
		}
	}


		/* (non-Javadoc)
		 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
		 */
		public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
			/* Préparation de l'objet formulaire */
			UserCreationForm form = new UserCreationForm( userDao );


			/* Traitement de la requête et récupération du bean en résultant */
			if(userInsession!= null){

				Utilisateur utilisateur = null;
				if((userInsession.isAdmin())&&(userInsession.getId_profil()!=id_profil)) utilisateur = form.modifierAdmin( request, id_profil);
				else utilisateur = form.modifier( request, userInsession.getId_profil());

				/* Ajout du bean et de l'objet métier à l'objet requête */
				request.setAttribute( Constants.ATT_USER, utilisateur ); 
				request.setAttribute( Constants.ATT_FORM, form );
				/* Si aucune erreur */
				if ( form.getErreurs().isEmpty() ) {

					/* Affichage de la fiche récapitulative */
					this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
				} else {
					/* Sinon, ré-affichage du formulaire de création avec les erreurs */
					this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
				}
			}
			else{
				
				RequestDispatcher rd = request.getRequestDispatcher("/pages/public/connexion.jsp");
				rd.forward(request, response);
			}
		}



	}
