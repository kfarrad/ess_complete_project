package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;

public class MesAutoEvaluations extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String PARAM_ID = "send";
	public static final String PARAM_PDF = "pdf";
	public static final String VUE = "/pages/protected/defaultUser/mesAutoEvaluations.jsp";
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	private Utilisateur utilisateur;
	public void init() throws ServletException {
        /* Récupération d'une instance de nos DAO Dates et Users */
		this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
		
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		String resultat = null;
        utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
        //valeur passé par get
        String value = Constants.getValeurChamp(request, PARAM_ID);
        if(value != null)
		{
			Long idUser = 0L;
			Long idAutoEvaluation = 0L;
			Long idDate = 0L;
			String[] tabValue = value.split("&", -1);
			
			idDate = Long.parseLong(tabValue[0]);
			idAutoEvaluation = Long.parseLong(tabValue[1]);
			idUser = utilisateur.getId();
			GestionEvaluations gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(idAutoEvaluation, idDate, idUser);
			if(gestionEvaluations !=null){
				gestionEvaluations.setGestionEvaluationsDateTransmission(new DateTime());
				gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
				resultat="Votre formulaire d'auto-Evaluation " + gestionEvaluations.getDates().getDatesPeriodes() + " a été transmis à la direction";
			}			
		}
        List<GestionEvaluations> listeGestionEvaluation = gestionEvaluationsDAOImpl.lister_By_User(utilisateur);
        request.setAttribute( "resultat", resultat );
        request.setAttribute( Constants.ATT_GESTION_EVALUATION, listeGestionEvaluation );
              
       this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        
        
	}

}
