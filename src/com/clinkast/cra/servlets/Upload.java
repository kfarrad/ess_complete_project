package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Fichier;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.FichierDaoImpl;
import com.clinkast.cra.forms.UploadForm;

public class Upload extends HttpServlet {
        /**
         * 
         */
        private static final long serialVersionUID                  = 1L;

        public static final String CHEMIN                           = "chemin";

        public static final String ATT_FICHIER                      = "fichier";
        
        public static final String ATT_FORM                         = "form";
        
        private FichierDaoImpl                                      fichierDao;
        

        public static final String VUE         = "/pages/protected/admin/upload.jsp";
        
        public void init() throws ServletException {
                /* Récupération d'une instance de notre DAO Utilisateur */
                this.fichierDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getFichierDao();
            }
        
        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
            /* Affichage de la page d'upload */
            this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        }

        public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
            /*
             * Lecture du paramètre 'chemin' passé à la servlet via la déclaration
             * dans le web.xml
             */
                 
            String chemin = Constants.getCheminUpload();
            /* Préparation de l'objet formulaire */
            UploadForm form = new UploadForm(fichierDao);

            /* Traitement de la requête et récupération du bean en résultant */
            Fichier fichier = form.creerFichier( request, chemin );

            /* Stockage du formulaire et du bean dans l'objet request */
            request.setAttribute( ATT_FORM, form );
            request.setAttribute( ATT_FICHIER, fichier );

            this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        }

    }