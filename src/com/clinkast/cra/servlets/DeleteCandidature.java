package com.clinkast.cra.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.CandidatureDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.ValidationTraitementCandidatureForm;

@WebServlet("/deleteCandidature")
public class DeleteCandidature extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String VUE_ERROR = "/pages/public/error_404.jsp";
	
	long id;
	
	CandidatureDaoImpl candidatureDao;
	
	public final void init() throws ServletException {
        /* Récupération d'une instance de notre DAO */
        this.candidatureDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCandidature();
    }
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id_ = req.getParameter("idCandidature");
		id = Long.valueOf(id_);
		ValidationTraitementCandidatureForm validation = new ValidationTraitementCandidatureForm(candidatureDao);
		
		Candidat candidat = validation.suppressionCandidature(id);
		
		if(candidat != null){
		    String pathCV = candidat.getPathCV();
		    if(pathCV!=null){
			String chemin = Constants.getCheminUpload();
			File file = new File(chemin + pathCV);
			file.delete();
		    }
		}
	    else this.getServletContext().getRequestDispatcher( VUE_ERROR ).forward( req, resp );
		
		resp.sendRedirect( req.getContextPath() + "/listeCandidats");
	}

}
