package com.clinkast.cra.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.xhtmlrenderer.pdf.ITextRenderer;


import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
@WebServlet(name="ViewPDF", urlPatterns="/viewPDF")
public class ViewPDF extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int TAILLE_TAMPON = 10240; // 10ko
	public static final String PARAM_ID = "id";
	public static final String VUE_REDIRECTION = "/mesAutoEvaluations";
	public static final String VUE_REDIRECTION_ADMIN = "/gestionAutoEvaluations";
	GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public void init() throws ServletException {
        /* Récupération d'une instance de nos DAO Dates et Users */
		this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
		
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
        Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
        GestionEvaluations gestionEvaluations = null;
        String value = Constants.getValeurChamp(request, PARAM_ID);
        String chemin = Constants.getCheminUpload();
       //nom du fichier
		String HTML_TO_PDF = null;//utilisateur.getProfil().getNom() +"_AUTO-EVALUATION_"+ gestionEvaluations.getDates().getDatesPeriodes() +".pdf";
		 
		if(value != null && value.indexOf("&")!= -1)
		{
			
			Long idAutoEvaluation = 0L;
			Long idDate = 0L;
			String[] tabValue= value.split("&", -1);
			Long idUser = utilisateur.getId();
			idDate = Long.parseLong(tabValue[0]);
			//La requete vient de  l'admin
			if(tabValue.length ==3)
			{
				idUser = Long.parseLong(tabValue[2]);
			}
			idAutoEvaluation = Long.parseLong(tabValue[1]);
			
		
        	//Verifier que le forumulaire existe bien
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(idAutoEvaluation, idDate, idUser);
			//Renvoi à la page des auto-evaluations
			if(gestionEvaluations == null){
				if(tabValue[2]!=null)
					response.sendRedirect( request.getContextPath() + VUE_REDIRECTION_ADMIN );
				else
					response.sendRedirect( request.getContextPath() + VUE_REDIRECTION );
				
				return;
			}
			HTML_TO_PDF = gestionEvaluations.getUser().getProfil().getNom() +"_AUTO-EVALUATION_"+idAutoEvaluation+".pdf";
			 //request.setAttribute( Constants.ATT_GESTION_EVALUATION, gestionEvaluations );
			 
			 File fichier = new File( chemin, HTML_TO_PDF );
			 /*Verifie la presence du fichier dans le repertoire*/
			 //le fichier n'existe pas ou le document a été modifié, on le crée
			 //Recupere la date de la derniere modification
			 long dateModified = gestionEvaluations.getGestionEvaluationsDateModification().getMillis();
			 if(!fichier.exists() || fichier.lastModified() < dateModified )
			 {
				//chemin de stockage
				 try{
					 String pdf = chemin + File.separator + HTML_TO_PDF;
					 OutputStream os = new FileOutputStream(pdf);
					 ITextRenderer renderer = new ITextRenderer();
					 renderer.setDocument(Constants.getUrlAppli() + "/viewAutoEvaluation?idDate=" + idDate + "&idAuto=" + idAutoEvaluation + "&idUser=" + idUser);
					 renderer.layout();
		             renderer.createPDF(os, true, 1);
		             renderer.finishPDF(); 
		             os.flush();
		             os.close();		              
		         }
		         catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				}
				
			 }
			 
			 /*le fichier existe bien */
			 /* Récupère le type du fichier */
	         String type = getServletContext().getMimeType( fichier.getName() );
	         /*
	          * Si le type de fichier est inconnu, alors on initialise un type par
	          * défaut
	          */
	         if ( type == null ) {
	        	 type = "application/octet-stream";
	         }
             /* Initialise la réponse HTTP */
             response.reset();
             response.setBufferSize( TAILLE_TAMPON );
             response.setContentType( type );
             response.setHeader( "Content-Length", String.valueOf( fichier.length() ) );
             response.setHeader( "Content-Disposition", "inline; filename=\"" + fichier.getName() + "\"" );
             /* Prépare les flux */
             BufferedInputStream entree = null;
             BufferedOutputStream sortie = null;
             try {
            	 /* Ouvre les flux */
                 entree = new BufferedInputStream( new FileInputStream( fichier ), TAILLE_TAMPON );
                 sortie = new BufferedOutputStream( response.getOutputStream(), TAILLE_TAMPON );
                /* Lit le fichier et écrit son contenu dans la réponse HTTP */
                byte[] tampon = new byte[TAILLE_TAMPON];
                int longueur;
                while ( ( longueur = entree.read( tampon ) ) > 0 ) {
                    sortie.write( tampon, 0, longueur );
                }
            }
            finally 
            {
                try {
                    sortie.close();
                } 
                catch ( IOException ignore )
                {	
                }
                try 
                {
                    entree.close();
                } 
                catch ( IOException ignore ) 
                {
                }
            }			 
		}
		else
		{
			response.sendRedirect( request.getContextPath() + VUE_REDIRECTION );
			return;
		}
	}
}
