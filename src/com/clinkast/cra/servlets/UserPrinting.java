package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserProfilDaoImpl;


@WebServlet( "/userPrinting" )
public class UserPrinting extends HttpServlet {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The Constant VUE_FORM_PRINT. */
    public static final String VUE_FORM_PRINT         = "/pages/protected/admin/userPrint.jsp";
    /** The profil dao. */
    private UserProfilDaoImpl          profilDao;
    
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, simple affichage du formulaire */
    	String id_Profil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
    	if(id_Profil!=null){
    		Long idProfil= Long.parseLong( id_Profil );
    		UserProfil profil = profilDao.trouver(idProfil);
    		request.setAttribute(Constants.ATT_PROFIL, profil);
    		}
    	this.getServletContext().getRequestDispatcher( VUE_FORM_PRINT).forward( request, response );
    	
    }
}
