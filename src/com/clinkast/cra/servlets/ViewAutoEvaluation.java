package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;

@WebServlet( "/viewAutoEvaluation" )
public class ViewAutoEvaluation extends HttpServlet {
        /**
         * 
         */
        private static final long serialVersionUID                  = 1L;
                
        public static final String VUE_AUTO_HTML        = "/pages/public/viewAutoEvaluation.jsp";
        
        //public static final String VUE_CV_HTML        = "/pages/public/cv_html.jsp";
        
        public static final String VUE_ERROR        = "/pages/public/error_404.jsp";
        
         private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
        
        
        /* (non-Javadoc)
         * @see javax.servlet.GenericServlet#init()
         */
        public void init() throws ServletException {
        	/* Récupération d'une instance de notre DAO Utilisateur */
        	this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
        }
        
        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        	String date=request.getParameter("idDate");
            String auto=request.getParameter("idAuto");
            String user=request.getParameter("idUser");
            
            GestionEvaluations gestionEvaluations = null;
                
            Long idUser = 0L;
        	Long idAutoEvaluation = 0L;
        	Long idDate = 0L;
        	idDate = Long.parseLong(date);
        	idAutoEvaluation = Long.parseLong(auto);
        	idUser = Long.parseLong(user);
        	gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(idAutoEvaluation, idDate, idUser);
        	request.setAttribute( Constants.ATT_GESTION_EVALUATION, gestionEvaluations );
            this.getServletContext().getRequestDispatcher( VUE_AUTO_HTML ).forward( request, response );
            
        }
}

