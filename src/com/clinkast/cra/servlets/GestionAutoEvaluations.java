package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;

@WebServlet(name="GestionAutoEvaluations", urlPatterns="/gestionAutoEvaluations")
public class GestionAutoEvaluations extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String VUE = "/pages/protected/admin/gestionAutoEvaluation.jsp";
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	List<GestionEvaluations> gestionEvaluations = null;
	public void init() throws ServletException{
		this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		gestionEvaluations = gestionEvaluationsDAOImpl.lister_By_ALL();
		request.setAttribute( Constants.ATT_GESTION_EVALUATION, gestionEvaluations );
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		
	}

}
