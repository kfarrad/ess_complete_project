package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.dao.ClientDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.forms.ProjetCreationForm;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjetUpdate.
 */
@WebServlet( "/projetUpdate" )
public class ProjetUpdate extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 	= 1L;
	
	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/admin/projetSuccess.jsp";
	
	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/projetUpdating.jsp";

	/** The projet dao. */
	private ProjetDaoImpl          projetDao;
	
	/** The client dao. */
	private ClientDaoImpl          clientDao;
	
	/** The id_projet. */
	private Long 					id_projet=0L;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */

		this.projetDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getClientDao();
		
	}

	

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */
		
		String idProjet = Constants.getValeurChamp( request, Constants.ATT_ID );
		id_projet = Long.parseLong( idProjet );
		/* recuperation des objets  à modifier */ 
        try {
        	Projet projet = projetDao.trouver(id_projet);
        	if(projet!=null){
        		Client client = clientDao.trouver(projet.getId_client());
        		request.setAttribute( Constants.ATT_PROJET, projet );
        		request.setAttribute( Constants.ATT_CLIENT, client );
        	}
        } catch ( DAOException e ) {
            e.printStackTrace();
        }
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}
        
	
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		ProjetCreationForm form = new ProjetCreationForm(projetDao);
		Projet projet = form.modifierProjet(request, id_projet);

		request.setAttribute( Constants.ATT_FORM, form ); 
		if(form.getErreurs().isEmpty()){

			request.setAttribute( Constants.ATT_PROJET, projet ); 
			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
			}
			else {
	        	// Sinon, ré-affichage du formulaire de création avec les erreurs 

				Client client = clientDao.trouver(projetDao.trouver(id_projet).getId_client());
	        	request.setAttribute( Constants.ATT_CLIENT, client );
			 this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
			}
	}
	 
}
