package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Competence;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Experience;
import com.clinkast.cra.beans.Formation;
import com.clinkast.cra.beans.Langue;
import com.clinkast.cra.beans.Resume;
import com.clinkast.cra.beans.TitreCV;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CompetenceDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ExperienceDaoImpl;
import com.clinkast.cra.dao.FormationDaoImpl;
import com.clinkast.cra.dao.LangueDaoImpl;
import com.clinkast.cra.dao.ResumeDaoImpl;
import com.clinkast.cra.dao.TitreCVDaoImpl;
import com.clinkast.cra.forms.ResumeForm;

@WebServlet( "/resume" )
public class IndexResume extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID                  = 1L;
    
    public static final String VUE         = "/pages/protected/defaultUser/cv_form_2.jsp";
    /** The user dao. */
    
    
    private ExperienceDaoImpl               workExpDao;
    
    private FormationDaoImpl                formationDao;
    
    private CompetenceDaoImpl               competenceDao;
    
    private LangueDaoImpl                 langueDao;
    
    private TitreCVDaoImpl                  titreCVDao;
    
    private ResumeDaoImpl                   resumeDao;
    
    private Utilisateur utilisateur;
    
    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException {
	/* Récupération d'une instance de notre DAO Utilisateur */
	this.workExpDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getExperienceDao();
	this.formationDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getFormationDao();
	this.competenceDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCompetenceDao();
	this.langueDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getLangueDao();
	this.titreCVDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getTitreCVDao();
	this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
	/* Affichage de la page d'upload */
	HttpSession session = request.getSession();
	ResumeForm form = new ResumeForm(resumeDao,titreCVDao,workExpDao,formationDao,competenceDao,langueDao);
	
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
	String idChamp=request.getParameter(Constants.PARAM_ID_RESUME);
	Long id_resume= 0L;
	if(idChamp!=null) id_resume = Long.parseLong( idChamp );       
	Long id_user = utilisateur.getId();

	String initiales = utilisateur.getProfil().getInitiales();
	try {
	    /*
	     * Titre
	     */
	    Resume resume = null;
	    if(id_resume==0L){//on cree un nouveau cv avec les initiales
		resume = form.nouveauResume(id_user, initiales);
	    }
	    else{
		
		    resume = resumeDao.trouver(id_resume);
	    }
	    
	    
	    if(resume==null){
		resume = new Resume();
		resume.setTitre(new TitreCV());
		resume.getTitre().setInitiales(initiales);
	    }
	    if(resume.getTitre()==null){
		TitreCV titreCV = new TitreCV();
		titreCV.setInitiales(initiales);
		form.sauverInitiales(titreCV, resume.getId(), id_user);
		resume.setTitre(titreCV);
	    }
	    //String initiales = utilisateur.getProfil().getInitiales();
	    
	    //request.setAttribute("initiales", initiales);
	    request.setAttribute("resume",resume);
	} catch (DAOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
	ResumeForm form = new ResumeForm(resumeDao,titreCVDao,workExpDao,formationDao,competenceDao,langueDao);
	HttpSession session = request.getSession();
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
	String idChamp=Constants.getValeurChamp(request,"id_resume");
	Long id_resume= 0L;
	if(idChamp!=null) id_resume = Long.parseLong( idChamp );                
	Long id_user = utilisateur.getId();
	try {
	    Resume resume = resumeDao.trouver(id_resume);
	    if (resume==null){
		resume = new Resume();
		resume.setExperiences(new HashMap<Long, Experience>());
		resume.setCompetences(new HashMap<Long,Competence>());
		resume.setFormations(new HashMap<Long,Formation>());
		resume.setLangues(new HashMap<Long,Langue>());
	    }
	    String initiales = utilisateur.getProfil().getInitiales();
	    TitreCV titreCV = resume.getTitre();
	    if(titreCV==null){
		titreCV = new TitreCV();
		titreCV.setInitiales(initiales);
		form.sauverInitiales(titreCV, id_resume, id_user);
	    }
	    Map<Long, Competence> competences = resume.getCompetences();
	    Map<Long, Experience> experiences = resume.getExperiences();
	    Map<Long, Formation> formations = resume.getFormations();
	    Map<Long, Langue> langues = resume.getLangues();
	    
	    String fromTitle =Constants.getValeurChamp(request, "title"); 
	    String formExp = Constants.getValeurChamp(request, "experience");
	    String formEdu = Constants.getValeurChamp(request, "education");
	    String formSkill = Constants.getValeurChamp(request, "skill");
	    String formLang = Constants.getValeurChamp(request, "language");
	    
	    if(Boolean.parseBoolean(fromTitle)){
		String saveTitle = Constants.getValeurChamp(request, "saveTitle");
		
		if(Boolean.parseBoolean(saveTitle)){ 
		    titreCV = form.sauverTitreCV(request, id_resume, id_user);
		}
		resume.setTitre(titreCV);
	    }
	    /*
	     * Experience 
	     */
	    if(Boolean.parseBoolean(formExp)){
		String saveWorkExp = Constants.getValeurChamp(request, "saveWorkExperience");
		String deleteWorkExp = Constants.getValeurChamp(request, "deleteWorkExperience");
		
		
		if(Boolean.parseBoolean(saveWorkExp)){ 
		    Experience workExp = form.sauverExperience(request, id_resume, id_user);
		    if(form.getErreurs().isEmpty()){
			experiences.put(workExp.getId(),workExp);
		    }
		}
		if(Boolean.parseBoolean(deleteWorkExp)){ 
		    List<Long> ids = form.supprimerExperience(request);
		    if(form.getErreurs().isEmpty()){
			for(Long id: ids)
			    experiences.remove(id);
		    }
		}
		resume.setExperiences(experiences);
	    }
	    /*
	     * Formation
	     */
	    if(Boolean.parseBoolean(formEdu)){
		String saveFormation = Constants.getValeurChamp(request, "saveEducation");
		String deleteFormation = Constants.getValeurChamp(request, "deleteEducation");
		if(Boolean.parseBoolean(saveFormation)){ 
		    Formation formation = form.sauverFormation(request, id_resume, id_user);
		    if(form.getErreurs().isEmpty()){
			formations.put(formation.getId(),formation);
		    }
		}
		if(Boolean.parseBoolean(deleteFormation)){ 
		    List<Long> ids = form.supprimerFormation(request);
		    if(form.getErreurs().isEmpty()){
			for(Long id: ids)
			    formations.remove(id);
		    }
		}
		resume.setFormations(formations);
	    }
	    /*
	     * Competence
	     */
	    if(Boolean.parseBoolean(formSkill)){
		String saveCompetence = Constants.getValeurChamp(request, "saveSkill");
		String deleteCompetence = Constants.getValeurChamp(request, "deleteSkill");
		if(Boolean.parseBoolean(saveCompetence)){ 
		    Competence competence = form.sauverCompetence(request,id_resume, id_user);
		    if(form.getErreurs().isEmpty()){
			competences.put(competence.getId(),competence);
		    }
		}
		if(Boolean.parseBoolean(deleteCompetence)){ 
		    List<Long> ids = form.supprimerCompetence(request);
		    if(form.getErreurs().isEmpty()){
			for(Long id: ids)
			    competences.remove(id);
		    }
		}
		
		resume.setCompetences(competences);
	    }
	    /*
	     * Language
	     */
	    if(Boolean.parseBoolean(formLang)){
			String saveLanguage = Constants.getValeurChamp(request, "saveLanguage");
			String deleteLanguage = Constants.getValeurChamp(request, "deleteLanguage");
			if(Boolean.parseBoolean(saveLanguage)){ 
			    Langue langue = form.sauverLangue(request,id_resume, id_user);
			    if(form.getErreurs().isEmpty()){
				langues.put(langue.getId(),langue);                                                
			    }
			}
			if(Boolean.parseBoolean(deleteLanguage)){ 
			    List<Long> ids = form.supprimerLangue(request);
			    if(form.getErreurs().isEmpty()){
				for(Long id: ids)
				    langues.remove(id);
			    }
			}
			resume.setLangues(langues);
	    }
	    
	    request.setAttribute("initiales", initiales);
	    
	    request.setAttribute("resume",resume);
	    
	} catch (DAOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}

