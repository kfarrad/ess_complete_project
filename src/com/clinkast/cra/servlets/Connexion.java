package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Contenu;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.ContenuDaoImp;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.forms.ConnexionForm;

// TODO: Auto-generated Javadoc
/**
 * The Class Connexion.
 */
@WebServlet( "/connexion" )
public class Connexion extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user dao. */
	private UserDaoImpl        userDao;
	
	private CongesDaoImpl congesDao;

	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/public/index.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/public/connexion.jsp";

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		
		this.congesDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getCongesDao();

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* Affichage de la page de connexion */
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) (session
				.getAttribute(Constants.ATT_SESSION_USER));
		if ((utilisateur != null)) {
			
		}
//			String login = Constants.getValeurChamp( request, Constants.PARAM_LOGIN );
//			if(login!=null)
//			{
//				utilisateur = new Utilisateur();
//				utilisateur.setLogin(login);
//				request.setAttribute( Constants.ATT_USER, utilisateur );
//			}
//
//		}
//		else{
//			session.setAttribute( Constants.ATT_SESSION_USER, null );
//
//		}
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}


	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* Préparation de l'objet formulaire */
		ConnexionForm form = new ConnexionForm(userDao);

		/* Traitement de la requête et récupération du bean en résultant */
		Utilisateur utilisateur =  form.connecterUtilisateur( request );        
		HttpSession session = request.getSession();

		/* Stockage du formulaire et du bean dans l'objet request */
		request.setAttribute( Constants.ATT_FORM, form );
		request.setAttribute( Constants.ATT_USER, utilisateur );
		
		/**
		 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
		 * Utilisateur à la session, sinon suppression du bean de la session.
		 */
		if ( form.getErreurs().isEmpty() ) {
			int nombreDemandeConge = congesDao.listerCongesByUserAndStatut(0L).size();
//			String nom = utilisateur.getId_profil().getName();
			session.setAttribute( Constants.ATT_SESSION_USER, utilisateur );
			session.setAttribute( "nombreDemandeCongeNonValide", nombreDemandeConge );
//			session.setAttribute(Constants.ATT_SESSION_NOM, nom);
//			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
			response.sendRedirect( request.getContextPath() + "/acceuil");
			

		} else {
			session.setAttribute( Constants.ATT_SESSION_USER, null );
			session.setAttribute(Constants.ATT_SESSION_NOM, null);
			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
		}
	}
}