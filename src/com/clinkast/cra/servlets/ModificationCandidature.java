package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CandidatureDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.ValidationTraitementCandidatureForm;

@WebServlet("/updateCandidature")
@MultipartConfig
public class ModificationCandidature extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private final static String VUE = "/pages/protected/RH/modificationCandidat.jsp";
	
	private CandidatureDaoImpl candidatureDao;
	private String pathCV;
	private long id;
	private String comment;
	
	@Override
	public void init() throws ServletException {
		this.candidatureDao = ((DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getCandidature();
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id_ = request.getParameter("idCandidature");		
		String [] diplomes = null;
		String [] specialites = null;
		Candidat candidat = null;
		if ( id_ != null && !id_.isEmpty() ) {
			
			try {
				id = Long.parseLong(id_);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		try {
			candidat = candidatureDao.getCandidatById(id);
			pathCV = candidat.getPathCV();
			comment = candidat.getCommentaires();
			diplomes = candidat.getDiplomes().split(";");
			specialites = candidat.getSpecilites().split(";");
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
				
		request.setAttribute("candidat", candidat);
		request.setAttribute("diplomes", diplomes);
		request.setAttribute("specialites", specialites);
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Part filePart = request.getPart("file"); 
		ValidationTraitementCandidatureForm validation = new ValidationTraitementCandidatureForm(candidatureDao);
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) (session.getAttribute(Constants.ATT_SESSION_USER));
		request.setAttribute( Constants.ATT_FORM, validation );
		validation.modificationCandidature(request, filePart, pathCV, id, comment, utilisateur.getName());
//		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
		if (validation.getErreurs().isEmpty()) {
//			request.setAttribute("statut", "success");
			response.sendRedirect( request.getContextPath() + "/listeCandidats?statutUpdate=true");
		} else {
			this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
		}

		
	}
	
}
