package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.CandidatureDaoImpl;
import com.clinkast.cra.dao.DAOFactory;

@WebServlet("/listeCandidats")
public class ListCandidat extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static String VUE = "/pages/protected/RH/listCandidats.jsp";
	
	private CandidatureDaoImpl candidatDao;
	
	@Override
	public void init() throws ServletException {
		this.candidatDao = ((DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getCandidature();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String search = req.getParameter( "search" );
//		String advancedSearch = req.getParameter( "advancedSearch" );
		String dateFrom = req.getParameter( "from" );
		String dateTo = req.getParameter( "to" );
		String searchInDocuments = req.getParameter( "searchInDocuments" );
		
		String statutUpdate = req.getParameter( "statutUpdate" );
		// page initiale
		int page = 1;
		if(req.getParameter("page") != null)
			//Récupération de l'attribut page depuis la jsp
            page = Integer.parseInt(req.getParameter("page"));
		int noOfRecords = 0;
		int noOfPages = 0;
		String textBox = req.getParameter( "searchTextBox" );
		List<Candidat> candidats;
		if (search != null && search.equals("true") && textBox != null && !textBox.isEmpty()) {
			req.setAttribute("search", true);
//			req.setAttribute("advancedSearch", true);
			req.setAttribute("searchTextBox", textBox);
			candidats = candidatDao.chercher((page-1) * Constants.getRecordsPerPage(), Constants.getRecordsPerPage(), textBox, dateFrom, dateTo, searchInDocuments );
			noOfRecords = candidatDao.getNoOfRecords();
		} else {
			candidats = candidatDao.lister((page-1) * Constants.getRecordsPerPage(), Constants.getRecordsPerPage());
			noOfRecords = candidatDao.getNoOfRecords();
		}
		noOfPages = (int) Math.ceil(noOfRecords * 1.0 / Constants.getRecordsPerPage());
		if (statutUpdate != null && statutUpdate.equals("true") )
			req.setAttribute( "statutUpdate", true );
        req.setAttribute( Constants.ATT_CANDIDATS, candidats );
        req.setAttribute("noOfPages", noOfPages);
        req.setAttribute("currentPage", page);
		this.getServletContext().getRequestDispatcher(VUE).forward(req, resp);
	}
	
	
}
