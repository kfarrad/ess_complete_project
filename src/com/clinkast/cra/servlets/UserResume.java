package com.clinkast.cra.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Resume;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ResumeDaoImpl;

@WebServlet( "/userResume" )
public class UserResume extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID                  = 1L;
    
    public static final String VUE         = "/pages/protected/defaultUser/resume_list.jsp";        
    
    public static final String VUE_ERROR        = "/pages/public/error_404.jsp";
    /** The user dao. */
    private ResumeDaoImpl                  resumeDao;     
    
    private Utilisateur utilisateur;
    
    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException {
	/* Récupération d'une instance de notre DAO Utilisateur */
	this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
	/* Affichage de la page d'upload */
	HttpSession session = request.getSession();
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
	
	
	Long id_user = utilisateur.getId();
	try {
	    List<Resume> listResume = resumeDao.lister(id_user);
	    request.setAttribute("listResume", listResume);
	} catch (DAOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
	
	HttpSession session = request.getSession();
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER)); 
	Long id_user = utilisateur.getId();
	
	String idChamp=request.getParameter("delete");
	Long id_resume= 0L;
	if(idChamp!=null) id_resume = Long.parseLong( idChamp );
	try {
	    if(resumeDao.verifier(id_resume, id_user)){
		
			Resume resume = resumeDao.supprimer(id_resume);
			if(resume!=null){
			    String resumePdf = resume.getPdf();
			    if(resumePdf!=null){
				String chemin = Constants.getCheminUpload();
				File file = new File(chemin + File.separator+ resumePdf);
				file.delete();
			    }
			}
	    } else {
	    	this.getServletContext().getRequestDispatcher( VUE_ERROR ).forward( request, response );
	    }
	    
	    
	}catch (DAOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    
	}catch(Exception e){
	    
	    e.printStackTrace();
	    
	}
	response.sendRedirect( request.getContextPath() + "/userResume");
	
    }
    
}

