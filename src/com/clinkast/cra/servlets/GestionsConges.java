package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionConges;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionCongesDAO;
import com.clinkast.cra.dao.UserDaoImpl;

@WebServlet("/gestionsConges")
public class GestionsConges extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String VUE_REDIRECTION = "/userCalendar";
	private GestionCongesDAO gestionCongesDAO;
	private UserDaoImpl	utilisateurDao;
	private GestionConges gestionConges;
	
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		//this.calendarDao	 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		//this.projetDao 		 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.utilisateurDao  = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		//this.profilDao 		 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		this.gestionCongesDAO = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getGestionConges();
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/*
		 * Traitement sur l'initialisation des jours des congés
		 */
		//recuperation des elements du formulaire
		String id_profil = Constants.getValeurChamp(request, "idUser");
		String txtRTT = Constants.getValeurChamp(request, "txtRTT");
		String txtCP = Constants.getValeurChamp(request, "txtCP");
		
		//le formulaire a été envoyé
		if(id_profil != null){
			//Recherche le user concerné
			Long idProfil = Long.parseLong(id_profil);
			Utilisateur user = utilisateurDao.trouver(idProfil);
			//le user existe
			if(user != null){
				//recupere son objet conges
				gestionConges = gestionCongesDAO.trouver_by_user(user.getId_profil());
				float RTT = Float.parseFloat(txtRTT);
				float CP = Float.parseFloat(txtCP);
				//si gestionConges est null, alors on l'initialise pour la premiere fois
				if(gestionConges == null){
					//RTT et CP different de zero
					if(RTT > 0 && CP > 0){
						GestionConges uneGestionConges = new GestionConges();
						uneGestionConges.setUser(user);
						uneGestionConges.setrTTInitial(RTT);
						uneGestionConges.setcPInitial(CP);
						uneGestionConges.setrTTAttente(0);
						uneGestionConges.setcPAttente(0);
						uneGestionConges.setDateValidation(new DateTime());
						uneGestionConges.setValide(1);
						uneGestionConges.setHistorique(null);
						gestionCongesDAO.creer(uneGestionConges);
						
						request.setAttribute("message", "Initialisation des congés est effective");
					}
				}
				else if(RTT != gestionConges.getrTTInitial() || CP != gestionConges.getcPInitial()){
					//s'il ya eu modification
					if(RTT != gestionConges.getrTTInitial())gestionConges.setrTTInitial(RTT);
					if(CP != gestionConges.getcPInitial())gestionConges.setcPInitial(CP);
					gestionConges.setDateValidation(new DateTime());
					gestionConges.setValide(1);	
					gestionCongesDAO.modifier(gestionConges);
					
					request.setAttribute("message", "Initialisation des congés est effective");
				}				
			}
		}
		/*fin traitement*/
		response.sendRedirect( request.getContextPath() + VUE_REDIRECTION );
	}

}
