package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Contenu;
import com.clinkast.cra.dao.ContenuDaoImp;
import com.clinkast.cra.dao.DAOFactory;

@WebServlet("/voirContenu")
public class ListPublication extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The content dao. */
	private ContenuDaoImp        contenuDao;
	
	private static final String VUE = "/pages/protected/admin/listPublication.jsp";

	@Override
	public void init() throws ServletException {
		this.contenuDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getContenu();
		( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String statutUpdate = request.getParameter( "statutUpdate" );
		List<Contenu> contenus = contenuDao.lister(0, 10);
		request.setAttribute("contenus", contenus);
		if (statutUpdate != null && statutUpdate.equals("true") )
			request.setAttribute( "statutUpdate", true );
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

}
