package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;

@WebServlet("/userProfil")
public class UserProfil extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
    
    /** The Constant VUE_FORM. */
    public static final String VUE_FORM         = "/pages/protected/defaultUser/userProfil.jsp";
    
    /** The user dao. */
    private UserDaoImpl					userDao;
    
    /** The user. */
    private Utilisateur 				user;
    
    /** The id_profil. */
    private Long			id_profil;
   
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
    }
    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, simple affichage du formulaire */
    	String idProfil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
        this.id_profil = Long.parseLong( idProfil );
        
        /* recuperation de l'objet profil à modifier */ 
        user = userDao.trouver(id_profil);
        request.setAttribute( Constants.ATT_USER, user );
      
        this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
    }
}
