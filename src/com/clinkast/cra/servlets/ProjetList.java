package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;


// TODO: Auto-generated Javadoc
/**
 * The Class ProjetList.
 */
@WebServlet( "/projetList" )
public class ProjetList extends HttpServlet {
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant VUE. */
	public static final String VUE      = "/pages/protected/admin/projetListing.jsp";
	

	/** The projet dao. */
	private ProjetDaoImpl          projetDao;
	
	/** The user dao. */
	private UserDaoImpl				userDao;
	
	
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */

		this.projetDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();

	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, affichage de la liste des clients */
    	
    	/* Récupération de la Map des profils enregistrés en session */
       
        Map<Projet, List<String>> projetsUtilisateurs   = new HashMap<Projet, List<String>>();
        List<Projet> listeProjets = projetDao.lister();
        for ( Projet elt : listeProjets ) {
        	List<String> listeProjetsUsers = userDao.listerProjetsUsers(elt);
        	
        	projetsUtilisateurs.put( elt, listeProjetsUsers );
        }
        request.setAttribute( Constants.ATT_PROJETS_USERS, projetsUtilisateurs );
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        
        
	}
}
