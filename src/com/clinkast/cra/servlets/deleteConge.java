package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.DAOFactory;

@WebServlet("/deleteConge")
public class deleteConge extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	CongesDaoImpl congeDao;

	private long idConge;

	private Utilisateur utilisateur;
	
	@Override
	public void init() throws ServletException {
		congeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCongesDao();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		HttpSession session = req.getSession();
		utilisateur = (Utilisateur) (session
				.getAttribute(Constants.ATT_SESSION_USER));
		
		String idConge_ = req.getParameter("idConge");
		
		try {
			idConge = Long.valueOf(idConge_);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		
		if (utilisateur.isAdmin() || utilisateur.isRh())
		{
			congeDao.supprimer(idConge, true);	
			resp.sendRedirect( req.getContextPath() + "/HistoriqueConges");
		}
			
		
		if (utilisateur.isUser())
		{
			congeDao.supprimer(idConge, true);
			resp.sendRedirect( req.getContextPath() + "/gestionCongesListe");
		}
	}

}
