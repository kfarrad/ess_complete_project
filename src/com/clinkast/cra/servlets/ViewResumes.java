package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Resume;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.ResumeDaoImpl;

@WebServlet( "/viewResumes" )
public class ViewResumes extends HttpServlet {
        /**
         * 
         */
        private static final long serialVersionUID                  = 1L;
                
        public static final String VUE_CV_HTML        = "/pages/public/viewAutoEvaluation.jsp";
        
        public static final String VUE_ERROR        = "/pages/public/error_404.jsp";
        
        private ResumeDaoImpl                     resumeDao;
        
        private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
        
        
        /* (non-Javadoc)
         * @see javax.servlet.GenericServlet#init()
         */
        public void init() throws ServletException {
                /* Récupération d'une instance de notre DAO Utilisateur */
                //this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();
                this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
               }
        
        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        	String date=request.getParameter("idDate");
            String auto=request.getParameter("idAuto");
            String user=request.getParameter("idUser");
            
            GestionEvaluations gestionEvaluations = null;
                
            Long idUser = 0L;
        	Long idAutoEvaluation = 0L;
        	Long idDate = 0L;
        	idDate = Long.parseLong(date);
        	idAutoEvaluation = Long.parseLong(auto);
        	idUser = Long.parseLong(user);
        	System.out.println("date2 " + idDate);
        	System.out.println("auto2 " + idAutoEvaluation);
        	System.out.println("idUser " + idUser);
        	gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(idAutoEvaluation, idDate, idUser);
        	request.setAttribute( Constants.ATT_GESTION_EVALUATION, gestionEvaluations );
            this.getServletContext().getRequestDispatcher( VUE_CV_HTML ).forward( request, response );
            
        }
}

