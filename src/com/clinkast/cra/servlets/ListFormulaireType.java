package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Dates;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.CompetencesDAOImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.DatesDAOImpl;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.RapportsDAOImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.forms.FormulaireTypeForm;


public class ListFormulaireType extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String VUE = "/pages/protected/admin/listFormType.jsp";
	public static final String ATT_FORM = "form";
	private DatesDAOImpl datesDAOImpl;
	private UserDaoImpl userDaoImpl;
	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	private CompetencesDAOImpl competencesDAOImpl;
	private RapportsDAOImpl rapportsDAOImpl;
	
	private List<Dates> listeDates;
	//private Dates dates = null;
	
	public void init() throws ServletException {
        /* Récupération d'une instance de nos DAO Dates et Users */
    	 this.datesDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getDatesDAOImpl();
    	 this.userDaoImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getUserDao();
    	 this.autoEvaluationDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getAutoEvaluationDAOImpl();
    	 this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
    	 this.competencesDAOImpl =((DAOFactory)getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getCompetencesDAOImpl();
    	 this.rapportsDAOImpl =((DAOFactory)getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getRapportsDAOImpl();
    }
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		this.listeDates = datesDAOImpl.lister();
		/*Faire les stat*/
		
	 	request.setAttribute( Constants.ATT_LISTDATES, listeDates );
       	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		/* Préparation de l'objet formulaire */
    	FormulaireTypeForm form = new FormulaireTypeForm(datesDAOImpl, userDaoImpl, autoEvaluationDAOImpl, gestionEvaluationsDAOImpl, competencesDAOImpl, rapportsDAOImpl);
    	
        /* Traitement de la requête et récupération du bean en résultant */
    	String value=request.getParameter("action").trim();
    	
    	
    	if(value.equals("send"))
		{
			form.envoyerDates(request);    		
    	}
    	else if(value.equals("creer")){
			form.creerDates( request );
			
		}
    	else{
    		//A completer
    	}
        
        
        /* Ajout du bean et de l'objet métier à l'objet requête */
    	
        this.listeDates = datesDAOImpl.lister();
        request.setAttribute( ATT_FORM, form );
 		request.setAttribute( Constants.ATT_LISTDATES, listeDates );
        
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
		
	}
}
