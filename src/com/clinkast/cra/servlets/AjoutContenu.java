package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.ContenuDaoImp;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.CreationContenuForm;

@WebServlet("/ajouterContenu")
public class AjoutContenu extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String VUE = "/pages/protected/admin/ajoutPublication.jsp";

	ContenuDaoImp contenuDao;

	public final void init() throws ServletException {
		/* Récupération d'une instance de notre DAO */
		this.contenuDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getContenu();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		CreationContenuForm creationContenu = new CreationContenuForm(contenuDao);
		creationContenu.creationContenu(request);
		if (creationContenu.getErreurs().isEmpty()) {
//			request.setAttribute("statut", "success");
			response.sendRedirect( request.getContextPath() + "/voirContenu?statutCreation=true");
		} else {
			this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
		}
		
	}

}
