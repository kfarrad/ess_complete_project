package com.clinkast.cra.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.ClientDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.ClientCreationForm;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientCreation.
 */
@WebServlet( "/clientCreation" )
public class ClientCreation extends HttpServlet {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    /** The Constant VUE_SUCCES. */
    public static final String VUE_SUCCES      = "/clientList";
    
    /** The Constant VUE_FORM. */
    public static final String VUE_FORM        = "/pages/protected/admin/clientCreate.jsp";
    
    /** The client dao. */
    private ClientDaoImpl clientDao;
    
    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public final void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getClientDao();
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public final void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, simple affichage du formulaire */
        this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public final void doPost( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
    	ClientCreationForm form = new ClientCreationForm(clientDao);

        /* Traitement de la requête et récupération du bean en résultant */
        Client client = form.creerClient( request );

        /* Ajout du bean et de l'objet métier à l'objet requête */
        request.setAttribute( Constants.ATT_CLIENT, client );
        request.setAttribute( Constants.ATT_FORM, form );

        /* Si aucune erreur */
        if ( form.getErreurs().isEmpty() ) {
           
            /* Affichage de la fiche récapitulative */

    		response.sendRedirect( request.getContextPath() + VUE_SUCCES );
        } else {
            /* Sinon, ré-affichage du formulaire de création avec les erreurs */
            this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
        }
    }
}