package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;












import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
//import com.clinkast.cra.beans.Constants;
//import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ResumeDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class UserProfilList.
 */
public class UserProfilList extends HttpServlet {
        
        /** The Constant serialVersionUID. */
        private static final long serialVersionUID 		= 1L;
        
        /** The Constant VUE. */
        public static final String VUE      			 =	 "/pages/protected/admin/userListing.jsp";
        
        /** The profil dao. */
        private UserProfilDaoImpl          profilDao;
        
        /** The user dao. */
        private UserDaoImpl					userDao;
        
        private ResumeDaoImpl             resumeDao;
        
        
        
        
        /* (non-Javadoc)
         * @see javax.servlet.GenericServlet#init()
         */
        public void init() throws ServletException {
                /* Récupération d'une instance de nos DAO Client et Commande */
                this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
                this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
                this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();
        }
        
        
        /* (non-Javadoc)
         * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
         */
        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
               
                //HttpSession session = request.getSession();
        	
        		int page = 1;
        		int recordsPerPage = 10;
        		if(request.getParameter("page")!=null)
        			page = Integer.parseInt(request.getParameter("page"));
        		
        		/* À la réception d'une requête GET, affichage de la liste des profils */
                
                /* Récupération de la Map des profils enregistrés en session */
        		
                Map<Long,List<String>> usersCV = resumeDao.listerUserCV();
                //List<UserProfil> listeProfils = profilDao.lister();
                List<UserProfil> listeProfils = profilDao.listerParOrdreAlphabetique((page-1)*recordsPerPage,
                								recordsPerPage);
                int noOfRecords = profilDao.getNoOfRecords();
                int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                
                Map<UserProfil, Utilisateur> salariers = new TreeMap<UserProfil, Utilisateur>();
                
                for (UserProfil profil : listeProfils) {
                        Utilisateur utilisateur = userDao.trouver(profil.getId());
                        if(utilisateur!=null){
                                salariers.put( profil, utilisateur );
                                Long id_user = utilisateur.getId();
                                if(!usersCV.containsKey(id_user)) usersCV.put(id_user, new ArrayList<String>());                                
                                
                        }
                        else salariers.put(profil,null);
                }
                
                //Affichage par ordre alphabétique et pagination
                
                
                
                /* Utilisateur utilisateur= (Utilisateur)session.getAttribute(Constants.ATT_SESSION_USER);

            if((utilisateur!=null)&(utilisateur.isAdmin())){
            	System.out.println("test profiListing :");
            	profils.remove(utilisateur.getId_profil());
            }*/
                
                request.setAttribute("mapUserCV", usersCV);
                request.setAttribute("salariers", salariers);
                request.setAttribute("noOfPages", noOfPages);
                request.setAttribute("currentPage", page);
                
                
                this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        }
}