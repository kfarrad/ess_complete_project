package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


// TODO: Auto-generated Javadoc
/**
 * The Class Deconnexion.
 */
@WebServlet( "/deconnexion" )
public class Deconnexion extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant VUE_ACCEUIL. */
	public static final String VUE_ACCEUIL         = "/connexion";

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Récupération et destruction de la session en cours */
        HttpSession session = request.getSession();
        session.invalidate();

        /* Redirection vers acceuil! */

		response.sendRedirect( request.getContextPath() + VUE_ACCEUIL );
        //this.getServletContext().getRequestDispatcher( VUE_ACCEUIL ).forward( request, response );
    }
}