package com.clinkast.cra.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;












import javax.servlet.http.Part;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Role;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.UserProfilCreationForm;


// TODO: Auto-generated Javadoc
/**
 * The Class UserProfilUpdate.
 */
@MultipartConfig
public class UserProfilUpdate extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	    
    /** The Constant VUE_SUCCES. */
    public static final String VUE_SUCCES       = "/pages/protected/admin/userSucessUpdateAdmin.jsp";
    
    /** The Constant VUE_FORM. */
    public static final String VUE_FORM         = "/pages/protected/admin/userProfilUpdating.jsp";

    /** The profil dao. */
    private UserProfilDaoImpl          profilDao;
    
    /** The user dao. */
    private UserDaoImpl					userDao;
    
    /** The user. */
    private Utilisateur 				user;
    
    /** The id_profil. */
    private Long			id_profil;
    
    private String fullPathCarteVitale = "";
    private String fullPathCNI = "";
    private String fullPathPhoto = "";

    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
        this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
        
    }
    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, simple affichage du formulaire */
    	String idProfil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
        this.id_profil = Long.parseLong( idProfil );
        
        /* recuperation de l'objet profil à modifier */ 
        try {
        	UserProfil profil = profilDao.trouver(id_profil);
        	fullPathCarteVitale = profil.getCarte_vitale();
        	fullPathCNI = profil.getCni();
        	fullPathPhoto = profil.getPhotoProfil();
        	user = userDao.trouver(id_profil);
        	
        	/* Mettre la date au bon format avant de l'envoyer à la jsp*/
        	if (profil.getDob() != null) {
        		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        		request.setAttribute("dateParsed",format.format(profil.getDob()));
    		}
        	
        	request.setAttribute( Constants.ATT_PROFIL, profil );
        	request.setAttribute( Constants.ATT_USER, user );
        } catch ( DAOException e ) {
            e.printStackTrace();
        }
        
        this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @SuppressWarnings("unchecked")
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	
    	/* traitement du role avec userDao */
    	String role = Constants.getValeurChamp( request, "typeCompte" );
       if(role!=null) {
    	   user.setRole(Role.valueOf(role ));
    	   userDao.modifierRole(user, id_profil);
    	   
    	   List<Utilisateur> utilisateurs = (ArrayList<Utilisateur>)(session.getAttribute(Constants.ATT_USERS));
    	   if ( utilisateurs == null ) {
           	utilisateurs = new ArrayList<Utilisateur>();
    	   }
            utilisateurs.add( user);

            session.setAttribute(Constants.ATT_USERS, utilisateurs );
          
       }
       
       /* Récupération de la carte vitale*/
       Part carteVitalePartFile = request.getPart("carteVitaleFile");
       Part cNIPartFile = request.getPart("CNIfile");
       Part photProfilPartFile = request.getPart("profilFile");
       
       /* Traitement du profil */
        UserProfilCreationForm form = new UserProfilCreationForm( profilDao );

		/* Traitement de la requête et récupération du bean en résultant */
        UserProfil profil = form.modifierUserProfil( request, id_profil, carteVitalePartFile, cNIPartFile,photProfilPartFile, fullPathCarteVitale, fullPathCNI, fullPathPhoto);

        request.setAttribute( Constants.ATT_FORM, form );
        
        /* Ajout du bean et de l'objet métier à l'objet requête */
        request.setAttribute( Constants.ATT_PROFIL, profil );
        request.setAttribute( Constants.ATT_FORM, form );

        /* Si aucune erreur */
        if ( form.getErreurs().isEmpty() ) {
            /*//Alors récupération de la map des profils dans la session 
            
            List< UserProfil> profils = (ArrayList<UserProfil>) session.getAttribute( Constants.ATT_SESSION_PROFILS );
            // Si aucune map n'existe, alors initialisation d'une nouvelle map 
            if ( profils == null ) {
                profils = new ArrayList<UserProfil>();
            }
           
            //Puis ajout du profil courant dans la map 
            profils.add( profil );
            //Et enfin (ré)enregistrement de la map en session 
            session.setAttribute( Constants.ATT_SESSION_PROFILS, profils );
*/
            /* Affichage de la fiche récapitulative */
            this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
        } else {
            /* Sinon, ré-affichage du formulaire de création avec les erreurs */
            this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
        }
    } 
   
}
