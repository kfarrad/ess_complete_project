package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Resume;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ResumeDaoImpl;

@WebServlet( "/viewResume" )
public class ViewResume extends HttpServlet {
        /**
         * 
         */
        private static final long serialVersionUID                  = 1L;
                
        public static final String VUE_CV_HTML        = "/pages/public/cv_html.jsp";
        
        public static final String VUE_ERROR        = "/pages/public/error_404.jsp";
        
        private ResumeDaoImpl                     resumeDao;
        
        
        
        
        /* (non-Javadoc)
         * @see javax.servlet.GenericServlet#init()
         */
        public void init() throws ServletException {
                /* Récupération d'une instance de notre DAO Utilisateur */
                this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();
               }
        
        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
                String idChamp=request.getParameter("id");
                Long id_resume= 0L;
                if(idChamp!=null) id_resume = Long.parseLong( idChamp );
                if(id_resume!=0L){
                        try {
                                Resume resume = resumeDao.trouver(id_resume);
                                if(resume==null) resume = new Resume();
                                request.setAttribute("resume",resume);
                        } catch (DAOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                        }
                        this.getServletContext().getRequestDispatcher( VUE_CV_HTML ).forward( request, response );
                        
                }
                else this.getServletContext().getRequestDispatcher( VUE_ERROR ).forward( request, response );
                
        }
}

