package com.clinkast.cra.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionConges;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.RappelGeneral;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionCongesDAO;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.RappelGeneralDao;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.CalendarCreationForm;
import com.clinkast.cra.mails.MailSender;

// TODO: Auto-generated Javadoc
/**
 * The Class UserCalendar.
 */
@WebServlet( "/userCalendar" )
public class UserCalendar  extends HttpServlet{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 			= 1L;
	private static final long ID_RTT = 5L;
	private static final long ID_CP = 7L;

	/** The Constant CHAMP_VALIDER. */
	private static final String CHAMP_VALIDER	 		= "valider";

	/** The Constant CHAMP_REFUSER. */
	private static final String CHAMP_REFUSER	 		= "refuser";

	/*private static final String CHAMP_ANNEE_STATUS		= "anneeStatus";
	private static final String CHAMP_MOIS_STATUS		= "moisStatus";*/
	/** The Constant CHAMP_IS_VALID. */
	private static final String CHAMP_IS_VALID		= "isvalid";



	/** The Constant VUE. */
	public static final String VUE      				= "/userCalendar";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         		= "/pages/protected/defaultUser/workingDay.jsp";

	/** The Constant VUE_ECHEC. */
	public static final String VUE_ECHEC				= "/pages/protected/defaultUser/imprevu.jsp";

	/** The calendar dao. */
	private CalendarDaoImpl        calendarDao;

	/** The projet dao. */
	private ProjetDaoImpl          projetDao;

	/** The utilisateur dao. */
	private UserDaoImpl	           utilisateurDao;

	/** The profil dao. */
	private UserProfilDaoImpl      profilDao;

	/** The utilisateur. */
	private Utilisateur			   utilisateur;

	/** The user in process. */
	private Utilisateur 		   userInProcess;

	/** The liste projets. */
	private List<Projet>   		   listeProjets;

	private GestionCongesDAO gestionCongesDAO;
	private GestionConges gestionConges = null;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.calendarDao	 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.projetDao 		 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.utilisateurDao  = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		this.profilDao 		 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		this.gestionCongesDAO = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getGestionConges();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		/* À la réception d'une requête GET, simple affichage du formulaire */

		HttpSession session = request.getSession();
		//recuperation annee et mois à traiter
		String anneeT = Constants.getValeurChamp(request, "annee" );
		String moisT = Constants.getValeurChamp(request, "mois" );
		int iYear= Constants.signedIntconv(anneeT);
		int iMonth= Constants.signedIntconv(moisT);
		//utilisateur en session?
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		if(utilisateur!=null){
			if(utilisateur.isUser()){
				userInProcess =utilisateur;
				Calendar ca = new GregorianCalendar();
				int mois = ca.get(Calendar.MONTH);
				int annee = ca.get(Calendar.YEAR);

				if(iYear==-1)
				{
					iYear=annee;
				}
				if(iMonth==-1)
				{
					iMonth=mois;
				}

				if(iYear>=annee && iMonth>=mois)  listeProjets =projetDao.listerUserProjetsActif(userInProcess);
				else listeProjets =projetDao.listerUserProjets(userInProcess);


				Calendrier jourFeriers = calendarDao.trouver(-1L, -1L, (long)iYear,(long) iMonth);
				List<Integer> feriers = new ArrayList<Integer>();

				if(jourFeriers!=null){
					int i=1;

					for(Float ferier:jourFeriers.getJours()){
						if(ferier==-1) feriers.add(i);
						i++;
					}
				}
				List<Calendrier> listeCalendrier = calendarDao.listerCalendrier(userInProcess, iMonth, iYear);
				Map<Projet, Calendrier> utilisateurCalendrier   = new HashMap<Projet, Calendrier>();
				for(Projet projet:listeProjets){
					for(Calendrier calendrier: listeCalendrier){
						if(calendrier.getId_projet()==projet.getId()) utilisateurCalendrier.put(projet, calendrier);
					}
				}
				/*
				 * GestionConges
				 */
				gestionConges = gestionCongesDAO.trouver_by_user(utilisateur.getProfil().getId());
				if(gestionConges!=null){
					gestionConges.setCpAcquis(majCPorRTT(true));
					gestionConges.setRttAcquis(majCPorRTT(false));
				}
				request.setAttribute("idProfilUser", utilisateur.getProfil().getId());
				request.setAttribute("gestionConges", gestionConges);

				request.setAttribute( Constants.ATT_UTILISATEUR_CALENDRIER, utilisateurCalendrier );
				request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier );
				request.setAttribute( Constants.ATT_PROJETS, listeProjets );

				request.setAttribute( Constants.ATT_ANNEE, iYear );
				request.setAttribute( Constants.ATT_MOIS, iMonth );
				request.setAttribute( Constants.ATT_FERIERS, feriers );

			}else{

				String id_userProfil= request.getParameter("id");

				Long idUser = 0L;
				if (id_userProfil!=null) {
					idUser= Long.parseLong(id_userProfil);
					userInProcess = utilisateurDao.trouver(idUser);
					request.setAttribute("id", idUser);
					//listeProjets =projetDao.listerUserProjets(userInProcess);

					gestionConges = gestionCongesDAO.trouver_by_user(idUser);
				}

				/*
				 * GestionConges
				 */

				if(gestionConges!=null){
					gestionConges.setCpAcquis(majCPorRTT(true));
					gestionConges.setRttAcquis(majCPorRTT(false));
				}
				//envoi du profil user courant
				request.setAttribute("idProfilUser", idUser);
				//envoi des info conges du user courant

				request.setAttribute("gestionConges", gestionConges);

				request.setAttribute("projetDao" , projetDao );
				request.setAttribute("calendarDao", calendarDao);
				request.setAttribute("utilisateurDao", utilisateurDao  );
				request.setAttribute("profilDao", profilDao);
				request.setAttribute( Constants.ATT_ANNEE, iYear );
				request.setAttribute( Constants.ATT_MOIS, iMonth );
			}

		}else {
			session.setAttribute( Constants.ATT_SESSION_USER, null );
		}
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}


	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {


		CalendarCreationForm form= new CalendarCreationForm(calendarDao);
		request.setAttribute("projetDao" , projetDao );
		request.setAttribute("calendarDao", calendarDao);
		request.setAttribute("utilisateurDao", utilisateurDao  );
		request.setAttribute("profilDao", profilDao);
		Calendar calend = new GregorianCalendar();

		int moisCourant = calend.get(Calendar.MONTH);
		int anneeCourante = calend.get(Calendar.YEAR);
		float total_rtt = 0;
		float total_cp = 0;
		/*Modification du status en fonction de la valeur du champ valider*/
		String refuser = Constants.getValeurChamp(request, CHAMP_REFUSER);
		String valider = Constants.getValeurChamp(request, CHAMP_VALIDER);
		String anneeS = Constants.getValeurChamp(request, "anneeStatus");
		String moisS = Constants.getValeurChamp(request, "moisStatus");
		//valider le calendar pour admin
		if((anneeS!=null) &&(moisS!=null)){
			System.out.println("passe ici 1");

			Long anneeStatus = Long.parseLong(anneeS);
			Long moisStatus = Long.parseLong(moisS);
			/*//recuperation id utilisateur
			Long idUserProfil = Long.parseLong(Constants.getValeurChamp(request, "userId"));
			userInProcess = utilisateurDao.trouver(idUserProfil);
			 */

			String id_userProfil= request.getParameter("id");
			if (id_userProfil!=null) {
				Long userProfilId = Long.parseLong(id_userProfil);
				userInProcess = utilisateurDao.trouver(userProfilId);
				request.setAttribute("id", userProfilId);
			}else{
				if(userInProcess==null){
					String id_profilEnCours = request.getParameter("userId");
					if(id_profilEnCours!=null){
						Long userProfilId = Long.parseLong(id_profilEnCours);
						userInProcess = utilisateurDao.trouver(userProfilId);
						request.setAttribute("id_profilEnCours", userProfilId);
					}
				}
			}
			if(anneeStatus>=anneeCourante && moisStatus>=moisCourant) listeProjets =projetDao.listerUserProjetsActif(userInProcess);
			else listeProjets =projetDao.listerUserProjets(userInProcess);

			if(Boolean.parseBoolean(valider))
			{
				form.setStatusCalendar(userInProcess.getId(), listeProjets, anneeStatus, moisStatus, 2L);
				//La direction a valide le conges, le systeme decremente les jours
				if(form.getErreurs().isEmpty())
				{
					//Concatenation pour composer l'historique
					DateTime laDate = new DateTime();

					try {
						String historique = gestionConges.getHistorique()+"||"+laDate+"_"+gestionConges.getrTTInitial()+"_"+gestionConges.getcPInitial();
						gestionConges = gestionCongesDAO.trouver_by_user(userInProcess.getProfil().getId());
						float rtt = gestionConges.getrTTInitial() + 0.75F - gestionConges.getrTTAttente();
						float cp = gestionConges.getcPInitial() + 2.5F - gestionConges.getcPAttente();
						gestionConges.setrTTInitial(rtt);
						gestionConges.setcPInitial(cp);
						gestionConges.setValide(1);
						gestionConges.setDateValidation(laDate);
						gestionConges.setHistorique(historique);
						gestionCongesDAO.modifier(gestionConges);

					} catch (Exception e) {
					}

				}
			}
			if(Boolean.parseBoolean(refuser)) form.setStatusCalendar(userInProcess.getId(), listeProjets, anneeStatus, moisStatus, 0L);


			List<Calendrier> listeCalendrier= calendarDao.listerCalendrier(userInProcess, moisStatus.intValue(), anneeStatus.intValue());
			/*
			 * GestionConges
			 */
			gestionConges = gestionCongesDAO.trouver_by_user(userInProcess.getProfil().getId());
			request.setAttribute("idProfilUser", userInProcess.getProfil().getId());
			request.setAttribute("gestionConges", gestionConges);

			request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier );
			request.setAttribute( Constants.ATT_PROJETS, listeProjets );
			request.setAttribute( Constants.ATT_ANNEE, anneeStatus.intValue() );
			request.setAttribute( Constants.ATT_MOIS, moisStatus.intValue() );
			request.setAttribute("id", userInProcess.getId_profil());
			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );

		}
		//creer et sousmettre le calendar 
		else{
			Calendar cal = Calendar.getInstance();		
			Long annee = Long.parseLong( Constants.getValeurChamp(request, "annee") );
			Long mois = Long.parseLong(Constants.getValeurChamp(request, "mois"));

			if((utilisateur!=null)){
				if(utilisateur.isUser()) userInProcess = utilisateur;
				if(userInProcess==null){
					response.sendRedirect( request.getContextPath() + VUE );
				}
				else{

					if(annee>=anneeCourante && mois>=moisCourant) listeProjets =projetDao.listerUserProjetsActif(userInProcess);
					else listeProjets =projetDao.listerUserProjets(userInProcess);
					Calendrier jourFeriers = calendarDao.trouver(-1L, -1L, (long)annee,(long) mois);
					List<Integer> feriers = new ArrayList<Integer>();

					if(jourFeriers!=null){
						int i=1;

						for(Float ferier:jourFeriers.getJours()){
							if(ferier==-1) feriers.add(i);
							i++;
						}
					}


					List<Calendrier> listeCalendrier = form.creerCalendar(request, userInProcess.getId(), listeProjets);

					if ( form.getErreurs().isEmpty() ) {	
						String isvalid = Constants.getValeurChamp(request, CHAMP_IS_VALID);
						//soumettre la calendar
						if(Boolean.parseBoolean(isvalid)){ 
							Calendar ca = new GregorianCalendar();
							int iTDay=ca.get(Calendar.DATE);
							if(iTDay>=20){
								//on recupère les emails des destinataires(RH et admin)
								List<Utilisateur> listeUsers= utilisateurDao.lister();
								List<String> receiverEmailIDs = new ArrayList<String>();
								int k=0;
								for (int i = 0; i < listeUsers.size(); i++) {
									if(!listeUsers.get(i).isUser()){
										receiverEmailIDs.add(k++,listeUsers.get(i).getLogin());
									}

								}
								cal.set(Calendar.YEAR, 2008);
								cal.set(Calendar.MONTH, mois.intValue());
								cal.set(Calendar.DAY_OF_MONTH, 01);
								//			                                        String subject= "Calendar validation";
								//			                                        String body = userInProcess.getLogin()+" has validate his calendar for the month: "
								//			                                                        + new SimpleDateFormat("MMMM").format(cal.getTime()) 
								//			                                                        + " " + annee.intValue() ;


								//new MailSender(receiverEmailIDs, subject, body);
							}
							form.setStatusCalendar(userInProcess.getId(), listeProjets, annee, mois, 1L);
							/*Utilisateur a sauvé des jours, on le repercute dans gestionconges*/
							//Recuperation de l'annee et le mois de la derniere initialisation
							gestionConges = gestionCongesDAO.trouver_by_user(userInProcess.getProfil().getId());
							if(gestionConges != null){
								Long annee_conges = (Long)(long)gestionConges.getDateValidation().getYear();
								Long mois_conges = (Long)(long)gestionConges.getDateValidation().getMonthOfYear();
								//vendredi pris pour le nombre de congés réel
								int vendrediPris = Constants.getFridaysTaken(form.getJoursCP(listeCalendrier, listeProjets), mois.intValue(), annee.intValue());
								try {
									total_rtt = calendarDao.count_jour(userInProcess.getId(), ID_RTT, annee_conges, mois_conges-1);
								} catch (DAOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();													
								}
								try {
									total_cp = calendarDao.count_jour(userInProcess.getId() ,ID_CP, annee_conges, mois_conges-1);
								} catch (DAOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								gestionConges.setcPAttente(total_cp + vendrediPris);
								gestionConges.setrTTAttente(total_rtt);
								gestionConges.setValide(0);
								gestionCongesDAO.modifier(gestionConges);
							}
						}




						listeCalendrier= calendarDao.listerCalendrier(userInProcess, mois.intValue(), annee.intValue());

						request.setAttribute( Constants.ATT_FERIERS, feriers );
						request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier ); 
						request.setAttribute( Constants.ATT_PROJETS, listeProjets );
						request.setAttribute( Constants.ATT_ANNEE, annee.intValue() );
						request.setAttribute( Constants.ATT_MOIS, mois.intValue() );
						request.setAttribute("id", userInProcess.getId_profil());

						/*
						 * GestionConges
						 */

						gestionConges = gestionCongesDAO.trouver_by_user(userInProcess.getProfil().getId());
						if(gestionConges!=null){
							gestionConges.setCpAcquis(majCPorRTT(true));
							gestionConges.setRttAcquis(majCPorRTT(false));
						}
						request.setAttribute("idProfilUser", userInProcess.getProfil().getId());
						request.setAttribute("gestionConges", gestionConges);


						this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );


					} else{
						request.setAttribute( Constants.ATT_FORM, form );
						this.getServletContext().getRequestDispatcher( VUE_ECHEC ).forward( request, response );

					}
				}
			}else response.sendRedirect( request.getContextPath() + VUE );


		}

	}

	private double majCPorRTT(boolean cp){
		//if cp true , on calcul les CP gagnés pour la date actuelle danxs le mois en cours sinon les RTT gagnés
		Calendar calend = new GregorianCalendar();
		int jour = calend.get(Calendar.DAY_OF_MONTH);
		if(cp) return (int)(((jour * 2.5)/calend.getActualMaximum(Calendar.DAY_OF_MONTH))*4.0 +0.5)/4.0;
		else return (int)(((jour * 0.75)/calend.getActualMaximum(Calendar.DAY_OF_MONTH))*4.0 +0.5)/4.0;

	}

}