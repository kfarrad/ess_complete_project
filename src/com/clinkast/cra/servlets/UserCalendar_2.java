		package com.clinkast.cra.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;










import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.CalendarCreationForm;
import com.clinkast.cra.mails.MailSender;

// TODO: Auto-generated Javadoc
/**
 * The Class UserCalendar.
 */
@WebServlet( "/userCalendar_2" )
public class UserCalendar_2  extends HttpServlet{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 			= 1L;
	
	/** The Constant CHAMP_VALIDER. */
	private static final String CHAMP_VALIDER	 		= "valider";
	
	/** The Constant CHAMP_REFUSER. */
	private static final String CHAMP_REFUSER	 		= "refuser";

	/*private static final String CHAMP_ANNEE_STATUS		= "anneeStatus";
	private static final String CHAMP_MOIS_STATUS		= "moisStatus";*/
	/** The Constant CHAMP_IS_VALID. */
	private static final String CHAMP_IS_VALID		= "isvalid";


	
	/** The Constant VUE. */
	public static final String VUE      				= "/userCalendar";
	
	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         		= "/pages/protected/defaultUser/workingDay.jsp";
	
	/** The Constant VUE_ECHEC. */
	public static final String VUE_ECHEC				= "/pages/protected/defaultUser/imprevu.jsp";

	/** The calendar dao. */
	private CalendarDaoImpl        calendarDao;
	
	/** The projet dao. */
	private ProjetDaoImpl          projetDao;
	
	/** The utilisateur dao. */
	private UserDaoImpl	           utilisateurDao;
	
	/** The profil dao. */
	private UserProfilDaoImpl      profilDao;
	
	/** The utilisateur. */
	private Utilisateur			   utilisateur;
	
	/** The user in process. */
	private Utilisateur 		   userInProcess;
	
	/** The liste projets. */
	private List<Projet>   		   listeProjets;
	
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.calendarDao	 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.projetDao 		 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.utilisateurDao  = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		this.profilDao 		 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */

		HttpSession session = request.getSession();

		String anneeT = Constants.getValeurChamp(request, "annee" );
		String moisT = Constants.getValeurChamp(request, "mois" );
		int iYear= Constants.signedIntconv(anneeT);
		int iMonth= Constants.signedIntconv(moisT);
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		if(utilisateur!=null){
			if(utilisateur.isUser()){
				userInProcess =utilisateur;
				listeProjets =projetDao.listerUserProjets(userInProcess);

				Calendar ca = new GregorianCalendar();
				int mois = ca.get(Calendar.MONTH);
				int annee = ca.get(Calendar.YEAR);

				if(iYear==-1)
				{
					iYear=annee;
				}
				if(iMonth==-1)
				{
					iMonth=mois;
				}

				List<Calendrier> listeCalendrier= calendarDao.listerCalendrier(userInProcess, iMonth, iYear);
				Map<Projet, Calendrier> utilisateurCalendrier   = new HashMap<Projet, Calendrier>();
				for(Projet projet:listeProjets){
					utilisateurCalendrier.put(projet, new Calendrier());
					for(Calendrier calendrier: listeCalendrier){
						if(calendrier.getId_projet()==projet.getId()) utilisateurCalendrier.put(projet, calendrier);
					}
				}
				ca = new GregorianCalendar();
				ca.set(Calendar.YEAR, 2008);
				ca.set(Calendar.MONTH, iMonth);
				ca.set(Calendar.DAY_OF_MONTH, 01);
				
				String moisFormat =new SimpleDateFormat("MMMM").format(ca.getTime());
				
				Map<Integer,String> mapMois = new HashMap<Integer,String>();
				for (int im = 0; im <= 11; im++) {
					ca.set(Calendar.MONTH, im);
					mapMois.put(im, new SimpleDateFormat("MMMM").format(ca.getTime()));
				}
				//satus
				 Long statusCal = 0L;
				
				//ligne total du calendrier
				List<Float> total = new ArrayList<Float>();
				 Double total_jours =0.0;
				 
					 for (int j = 0; j < 31; j++) {
				  	Float sum=0.0F;
				  	if(!listeCalendrier.isEmpty()){
				 		for(int i=0; i<listeCalendrier.size();i++){
							sum+=listeCalendrier.get(i).getJours().get(j);

						 	 total_jours+=listeCalendrier.get(i).getTotal_jour();
				 		}}
				  		statusCal=listeCalendrier.get(0).getStatus();
				 		total.add(j, sum);
					 }
				 	
					
				// weekend
				ArrayList<Integer> list = new ArrayList<Integer>();
				ca = new GregorianCalendar (iYear, iMonth, 1); 
				 do {
					    // get the day of the week for the current day
					    int day = ca.get(Calendar.DAY_OF_WEEK);
					    // check if it is a Saturday or Sunday
					    if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
					        // print the day - but you could add them to a list or whatever
					        list.add(ca.get(Calendar.DAY_OF_MONTH));	   }
					    // advance to the next day
					    ca.add(Calendar.DAY_OF_YEAR, 1);
					}  while (ca.get(Calendar.MONTH) == iMonth);
			
				 
				request.setAttribute( Constants.ATT_UTILISATEUR_CALENDRIER, utilisateurCalendrier );
				request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier );
				request.setAttribute( Constants.ATT_PROJETS, listeProjets );
				request.setAttribute( "iYear", iYear );
				request.setAttribute( "iMonth", iMonth );
				request.setAttribute( "moisFormat", moisFormat );
				request.setAttribute( "mapMois", moisFormat );
				request.setAttribute( "total", total );
				request.setAttribute( "total_jours", total_jours );
				request.setAttribute( "weekend", list );
				request.setAttribute("status", statusCal);

			}else{

				String id_userProfil= request.getParameter("id");
				if (id_userProfil!=null) {
					Long userProfilId = Long.parseLong(id_userProfil);
					userInProcess = utilisateurDao.trouver(userProfilId);
					request.setAttribute("id", userProfilId);
					listeProjets =projetDao.listerUserProjets(userInProcess);
				}
				request.setAttribute("projetDao" , projetDao );
				request.setAttribute("calendarDao", calendarDao);
				request.setAttribute("utilisateurDao", utilisateurDao  );
				request.setAttribute("profilDao", profilDao);
				request.setAttribute( Constants.ATT_ANNEE, iYear );
				request.setAttribute( Constants.ATT_MOIS, iMonth );
			}

		}else {
			session.setAttribute( Constants.ATT_SESSION_USER, null );
		}
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}


	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		CalendarCreationForm form= new CalendarCreationForm(calendarDao);
		request.setAttribute("projetDao" , projetDao );
		request.setAttribute("calendarDao", calendarDao);
		request.setAttribute("utilisateurDao", utilisateurDao  );
		request.setAttribute("profilDao", profilDao);
		
		/*Modification du status en fonction de la valeur du champ valider*/
		String refuser = Constants.getValeurChamp(request, CHAMP_REFUSER);
		String valider = Constants.getValeurChamp(request, CHAMP_VALIDER);
		String anneeS = Constants.getValeurChamp(request, "anneeStatus");
		String moisS = Constants.getValeurChamp(request, "moisStatus");
		//valider le calendar pour admin
		if((anneeS!=null) &&(moisS!=null)){

			Long anneeStatus = Long.parseLong(anneeS  );
			Long moisStatus = Long.parseLong(moisS);
			/*//recuperation id utilisateur
			Long idUserProfil = Long.parseLong(Constants.getValeurChamp(request, "userId"));
			userInProcess = utilisateurDao.trouver(idUserProfil);
			*/

			if(Boolean.parseBoolean(valider)) form.setStatusCalendar(userInProcess.getId(), listeProjets, anneeStatus, moisStatus, 2L);
			if(Boolean.parseBoolean(refuser)) form.setStatusCalendar(userInProcess.getId(), listeProjets, anneeStatus, moisStatus, 0L);
			listeProjets =projetDao.listerUserProjets(userInProcess);
			List<Calendrier> listeCalendrier= calendarDao.listerCalendrier(userInProcess, moisStatus.intValue(), anneeStatus.intValue());

			request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier ); 
			request.setAttribute( Constants.ATT_PROJETS, listeProjets );
			request.setAttribute( Constants.ATT_ANNEE, anneeStatus.intValue() );
			request.setAttribute( Constants.ATT_MOIS, moisStatus.intValue() );
			request.setAttribute("id", userInProcess.getId_profil());
			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );

		}
		//creer et sousmettre le calendar 
		else{
			Calendar cal = Calendar.getInstance();		
			Long annee = Long.parseLong( Constants.getValeurChamp(request, "annee") );
			Long mois = Long.parseLong(Constants.getValeurChamp(request, "mois"));

			if((utilisateur!=null)){
				if(utilisateur.isUser()) userInProcess = utilisateur;
				listeProjets =projetDao.listerUserProjets(userInProcess);
				form.creerCalendar(request, userInProcess.getId(), listeProjets);

				if ( form.getErreurs().isEmpty() ) {	
					String isvalid = Constants.getValeurChamp(request, CHAMP_IS_VALID);
					//soumettre la calendar
					if(Boolean.parseBoolean(isvalid)){ 
						Calendar ca = new GregorianCalendar();
						 int iTDay=ca.get(Calendar.DATE);
						 if(iTDay>=20){
						//on recupère les emails des destinataires(RH et admin)
						List<Utilisateur> listeUsers= utilisateurDao.lister();
						List<String> receiverEmailIDs = new ArrayList<String>();
						int k=0;
						for (int i = 0; i < listeUsers.size(); i++) {
							if(!listeUsers.get(i).isUser()){
								receiverEmailIDs.add(k++,listeUsers.get(i).getLogin());
							}
							
						}
						cal.set(Calendar.YEAR, 2008);
						cal.set(Calendar.MONTH, mois.intValue());
						cal.set(Calendar.DAY_OF_MONTH, 01);
						
						String subject= "Calendar validation";
						String body = userInProcess.getLogin()+" has validate his calendar for the month: "
								+ new SimpleDateFormat("MMMM").format(cal.getTime()) 
								+ " " + annee.intValue() ;
						
						
						new MailSender(receiverEmailIDs, subject, body);
						 }
						form.setStatusCalendar(userInProcess.getId(), listeProjets, annee, mois, 1L);		 
					}

					List<Calendrier> listeCalendrier= calendarDao.listerCalendrier(userInProcess, mois.intValue(), annee.intValue());

					request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier ); 
					request.setAttribute( Constants.ATT_PROJETS, listeProjets );
					request.setAttribute( Constants.ATT_ANNEE, annee.intValue() );
					request.setAttribute( Constants.ATT_MOIS, mois.intValue() );
					request.setAttribute("id", userInProcess.getId_profil());
					this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );


				} else{
					request.setAttribute( Constants.ATT_FORM, form );
					this.getServletContext().getRequestDispatcher( VUE_ECHEC ).forward( request, response );

				}
			}else response.sendRedirect( request.getContextPath() + VUE );
				

		}
		
	}



}