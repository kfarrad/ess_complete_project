package com.clinkast.cra.servlets;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;







import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;







import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.UserCreationForm;
import com.clinkast.cra.mails.MailSender;




// TODO: Auto-generated Javadoc
/**
 * The Class UserCreation.
 */
public class UserCreation extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 	= 1L;
	
	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/admin/userPrint.jsp";
	
	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/userCreate.jsp";

	/** The user dao. */
	private UserDaoImpl        			userDao;
	
	/** The profil dao. */
	private UserProfilDaoImpl      		profilDao;
	
	private ProjetDaoImpl				projetDao;
	
	/** The profil. */
	private UserProfil 					profil;
	
	/** The id_profil user. */
	private Long 						id_profilUser;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		this.projetDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */
		String idProfil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
		if(idProfil!=null){
			this.id_profilUser = Long.parseLong( idProfil );        

			/* recuperation de l'objet profil à modifier */ 
			try {
				profil = profilDao.trouver(id_profilUser);
				if(profil!=null){
					Utilisateur utilisateur = userDao.trouver(id_profilUser);

					if(utilisateur==null){
						Utilisateur userNew = new Utilisateur();
						userNew.setProfil(profil);
						userNew.setLogin(profil.getEmail());
						request.setAttribute( Constants.ATT_USER, userNew );
						this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
					}

					else {
						request.setAttribute( Constants.ATT_PROFIL, profil );
						request.setAttribute( Constants.ATT_USER, utilisateur );						
						this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
					}
				}
				else this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );

			} catch ( DAOException e ) {
				e.printStackTrace();
			}
		}
		else this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	//@SuppressWarnings("unchecked")
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
		/* Préparation de l'objet formulaire */
		UserCreationForm form = new UserCreationForm( userDao );
		String idProfil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
		if(idProfil!=null)			this.id_profilUser = Long.parseLong( idProfil ); 
		profil = profilDao.trouver(id_profilUser);
		/* Traitement de la requête et récupération du bean en résultant */
		Utilisateur utilisateur = form.creerUser( request, profil);
		
		/* Ajout du bean et de l'objet métier à l'objet requête */
		
		request.setAttribute( Constants.ATT_FORM, form );
		request.setAttribute( Constants.ATT_PROFIL, profil );

		/* attribution projets par défauts */
		if ( form.getErreurs().isEmpty() ) {
			request.setAttribute( Constants.ATT_USER, utilisateur );
			List<Projet> listeProjets= projetDao.lister();
			 for(Projet projet: listeProjets){
					 if(projet.getNom().contains("CP") || projet.getNom().contains("RTT")
							 || projet.getNom().contains("CSS")|| projet.getNom().contains("maladie")){
						 projetDao.attribution(utilisateur.getId(), projet.getId());
					 }
						
			 }
			/* envoi d'un email à l'utilisateur */
			List<String> receiverEmailIDs = new ArrayList<String>();
			receiverEmailIDs.add(utilisateur.getLogin());
			String subject= "Creation d'un compte";
			String body ="Bonjour "+ profil.getName()+ ",\n \n votre compte a été crée dans l'application de gestion des plannings de Clinkast \n"
					+ "Vos identifiants sont les suivants: \n"+
					"Login :"+ utilisateur.getLogin()+ "\n"+
					"Mot de passe : "+ utilisateur.getMotDePasse() +"\n"+
					"Lien d'accés:"+ Constants.LIEN_CRA+
					"\nBonne navigation dans l'application."+
					"\nBien cordialement, Clinkast Team";
					
			
			new MailSender(receiverEmailIDs, subject, body);
			/* Affichage de la fiche récapitulative */
			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
		} else {
			utilisateur.setLogin(profil.getEmail());
			request.setAttribute( Constants.ATT_USER, utilisateur );
			/* Sinon, ré-affichage du formulaire de création avec les erreurs */
			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
		}
	}

	
}