package com.clinkast.cra.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.xhtmlrenderer.pdf.ITextRenderer;
import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;

@WebServlet(name="ViewBilan", urlPatterns="/viewBilan")
public class ViewBilan extends HttpServlet {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 			= 1L;


	/** The Constant VUE. */
	public static final String VUE      		= "/pages/protected/admin/viewBilan.jsp";
	
	public static final String VUE_SUCCES 		= "/pages/protected/RH/mailBilanSuccess.jsp";

	/** The calendar dao. */
	private CalendarDaoImpl        		    calendarDao;
	
	/** The projet dao. */
	private ProjetDaoImpl					projetDao;
	
	/** The profil dao. */
	
	/** The utilisateur dao. */
	private UserDaoImpl		  			    utilisateurDao;
	
	/** The liste projets. */
	private List<Projet>   		   			listeProjets;
	
	public static final int TAILLE_TAMPON = 10240; // 10ko
	public static final String PARAM_ANNEE = "annee";
	public static final String PARAM_MOIS = "mois";
	public static final String VUE_REDIRECTION = "/calendarBilan";
	public static final String VUE_REDIRECTION_ADMIN = "/gestionAutoEvaluations";
	GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public void init() throws ServletException {
        /* Récupération d'une instance de nos DAO Dates et Users */
		this.calendarDao 		= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.projetDao 			= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.utilisateurDao 	= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		/*liste des tous les users*/
        String annee = Constants.getValeurChamp(request, PARAM_ANNEE);
        String mois = Constants.getValeurChamp(request, PARAM_MOIS);
        
        int intAnnee, intMois, nbre = 0;
        intAnnee = Integer.parseInt(annee);
        intMois = Integer.parseInt(mois);
        /*
         * verifie que l'année et la mois sont passés en parametre
         */
        if(annee == null || mois == null){
        	//retour à la page bilan
        	response.sendRedirect( request.getContextPath() + VUE_REDIRECTION );
        }
        else{
        		
	        String chemin = Constants.getCheminUpload();
	       //nom du fichier
	        String HTML_TO_PDF = null;//utilisateur.getProfil().getNom() +"_AUTO-EVALUATION_"+ gestionEvaluations.getDates().getDatesPeriodes() +".pdf";
			 
			HTML_TO_PDF = "bilan_"+ annee + "_"+ mois+ ".pdf";
			
			File fichier = new File( chemin, HTML_TO_PDF );
			/*Verifie la presence du fichier dans le repertoire*/
			//crée a chaque fois le fichier dans le repertoire, supprime l'ancen et crée un nouveau
			//chemin de stockage
			try{
				String pdf = chemin + File.separator + HTML_TO_PDF;
				OutputStream os = new FileOutputStream(pdf);
				ITextRenderer renderer = new ITextRenderer();
				renderer.setDocument(Constants.getUrlAppli() + "/viewBilans?annee=" + annee + "&mois=" + mois + "html=true");
				renderer.layout();
			    renderer.createPDF(os, true, 1);
			    renderer.finishPDF(); 
			    os.flush();
			    os.close();		              
		    }
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
					
					
			/*le fichier existe bien */
			/* Récupère le type du fichier */
			String type = getServletContext().getMimeType( fichier.getName() );
			/*
			 * Si le type de fichier est inconnu, alors on initialise un type par
			 * défaut
			*/
			if ( type == null ) {
				type = "application/octet-stream";
			}
			/* Initialise la réponse HTTP */
			response.reset();
			response.setBufferSize( TAILLE_TAMPON );
			response.setContentType( type );
			response.setHeader( "Content-Length", String.valueOf( fichier.length() ) );
			response.setHeader( "Content-Disposition", "inline; filename=\"" + fichier.getName() + "\"" );
			/* Prépare les flux */
			BufferedInputStream entree = null;
			BufferedOutputStream sortie = null;
			try {
				/* Ouvre les flux */
				entree = new BufferedInputStream( new FileInputStream( fichier ), TAILLE_TAMPON );
				sortie = new BufferedOutputStream( response.getOutputStream(), TAILLE_TAMPON );
				/* Lit le fichier et écrit son contenu dans la réponse HTTP */
				byte[] tampon = new byte[TAILLE_TAMPON];
				int longueur;
				while ( ( longueur = entree.read( tampon ) ) > 0 ) {
					sortie.write( tampon, 0, longueur );
	            }
			}
			finally 
			{
				try {
					sortie.close();
				} 
				catch ( IOException ignore )
				{	
				}
				try 
				{
					entree.close();
	            } 
				catch ( IOException ignore ) 
				{
				}
			}	  		 
		}
	}
}