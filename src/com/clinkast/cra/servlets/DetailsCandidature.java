package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.CandidatureDaoImpl;
import com.clinkast.cra.dao.DAOFactory;

@WebServlet("/detailCandidature")
public class DetailsCandidature extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CandidatureDaoImpl candidature;
	
	
	@Override
	public void init() throws ServletException {
		candidature = ((DAOFactory) (getServletContext().getAttribute(Constants.CONF_DAO_FACTORY))).getCandidature();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String idCandidature = req.getParameter("idCandidature");
		long id = Long.valueOf(idCandidature);
		Candidat candidat = candidature.getCandidatById(id);
		req.setAttribute("commentaire", candidat.getCommentaires());
		resp.sendRedirect( req.getContextPath() + "/listeCandidats?details=true");
	}
}
