package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Conges;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;

@WebServlet("/CalendarCongesAll")
public class CalendarCongesAll extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/adminCalendarCongesAll.jsp";
	
	public static final String VUE_FORM_RH         = "/pages/protected/RH/adminCalendarCongesAll.jsp";	
	CongesDaoImpl congesDao;
	
	List<Conges> congesList;
	
	/** The utilisateur dao. */
	private UserDaoImpl	           utilisateurDao;


	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		this.congesDao = ( (DAOFactory) getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getCongesDao();
		this.utilisateurDao  = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();

		//recuperation annee et mois à traiter
		String anneeT = Constants.getValeurChamp(request, "annee" );
		String moisT = Constants.getValeurChamp(request, "mois" );
		int iYear= Constants.signedIntconv(anneeT);
		int iMonth= Constants.signedIntconv(moisT); System.out.println("iYear:"+iYear + " iMonth : " + iMonth);

		
		//Si DefaultUser on envoie les infos vers la JSP UserCalendarConges
	
				request.setAttribute("congesDao", congesDao);
				
				//request.setAttribute("listeUtilisateurs", listeUtilisateurs);
				request.setAttribute("annee" , iYear);
				request.setAttribute("mois" , iMonth);
				request.setAttribute("utilisateurDao", utilisateurDao );
	
		
		
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward( request, response );
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 
	}
	
	

}
