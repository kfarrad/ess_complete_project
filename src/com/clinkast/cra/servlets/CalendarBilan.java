package com.clinkast.cra.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionConges;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.RappelGeneral;
//import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.GestionCongesDAO;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.RappelGeneralDao;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.mails.MailSender;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarBilan.
 */
@WebServlet("/calendarBilan")
public class CalendarBilan extends HttpServlet{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 			= 1L;


	/** The Constant VUE. */
	public static final String VUE      		= "/pages/protected/defaultUser/workingDayResult.jsp";

	public static final String VUE_SUCCES 		= "/pages/protected/RH/mailBilanSuccess.jsp";

	/** The calendar dao. */
	private CalendarDaoImpl        		    calendarDao;

	/** The projet dao. */
	private ProjetDaoImpl					projetDao;

	/** The profil dao. */
	private UserProfilDaoImpl               profilDao;

	/** The utilisateur dao. */
	private UserDaoImpl		  			    utilisateurDao;

	/** The utilisateur. */
	private Utilisateur			            utilisateur;	

	/** The liste projets. */
	private List<Projet>   		   			listeProjets;
	private GestionCongesDAO gestionCongesDAO;
	private GestionConges gestionConges = null;

	/** The rappel_general. */
	private RappelGeneralDao rappelGeneralDao;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public final void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.calendarDao 		= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.projetDao 			= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.utilisateurDao 	= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		this.profilDao 			= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		this.gestionCongesDAO = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getGestionConges();
		this.rappelGeneralDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getRappelGeneral();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public final void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */

		HttpSession session = request.getSession();
		String anneeT = Constants.getValeurChamp(request, "annee" );
		String moisT = Constants.getValeurChamp(request, "mois" );
		int iYear= Constants.signedIntconv(anneeT);
		int iMonth= Constants.signedIntconv(moisT);

		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		if(utilisateur!=null){
			if(utilisateur.isUser()){
				listeProjets =projetDao.listerUserProjets(utilisateur);
				List<Projet> listeProjetsActif =projetDao.listerUserProjets(utilisateur);
				Calendar ca = new GregorianCalendar();
				int mois = ca.get(Calendar.MONTH);
				int annee = ca.get(Calendar.YEAR);

				if(iYear==-1)
				{
					iYear=annee;
				}
				if(iMonth==-1)
				{
					iMonth=mois;
				}

				List<Calendrier> listeCalendrier= calendarDao.listerCalendrier(utilisateur, iMonth, iYear);

				request.setAttribute( Constants.ATT_CALENDRIER, listeCalendrier );
				if(iYear>=annee && iMonth>=mois) {
					request.setAttribute( Constants.ATT_PROJETS, listeProjetsActif );
				}
				else {
					request.setAttribute( Constants.ATT_PROJETS, listeProjets );
				}
				request.setAttribute( Constants.ATT_ANNEE, iYear );
				request.setAttribute( Constants.ATT_MOIS, iMonth );
				gestionConges = gestionCongesDAO.trouver_by_user(utilisateur.getProfil().getId());
				request.setAttribute("gestionConges", gestionConges);


			}else{

				String id_user= request.getParameter("id");
				if (id_user!=null) {
					Long id = Long.parseLong(id_user);
					request.setAttribute("id", id);
					listeProjets =projetDao.listerUserProjets(utilisateurDao.trouver(id));
				}
				request.setAttribute("projetDao" , projetDao );
				request.setAttribute("calendarDao", calendarDao);
				request.setAttribute("utilisateurDao", utilisateurDao  );
				request.setAttribute("profilDao", profilDao);
				request.setAttribute( Constants.ATT_ANNEE, iYear );
				request.setAttribute( Constants.ATT_MOIS, iMonth );
			}

		}
		else {
			session.setAttribute( Constants.ATT_SESSION_USER, null );
		}

		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public final void doPost( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */

		HttpSession session = request.getSession();
		String anneeT = Constants.getValeurChamp(request, "annee" );
		String moisT = Constants.getValeurChamp(request, "mois" );
		int iYear= Constants.signedIntconv(anneeT);
		int iMonth= Constants.signedIntconv(moisT);
		String resultat = null;
		if(utilisateur!=null){
			if(!utilisateur.isUser()){
				Calendar cal = new GregorianCalendar();
				String id_user= request.getParameter("id");

				if (id_user!=null) {

					Long id = Long.parseLong(id_user);
					request.setAttribute("id", id);
					Utilisateur userInProcess =utilisateurDao.trouver(id);
					//UserProfil profil = profilDao.trouver(userInProcess.getId_profil());
					List<String> receiverEmailIDs = new ArrayList<String>();
					receiverEmailIDs.add(userInProcess.getLogin());
					cal.set(Calendar.YEAR, 2008);
					cal.set(Calendar.MONTH, iMonth);
					cal.set(Calendar.DAY_OF_MONTH, 01);

					String subject = "Validation de votre calendrier";
					String body = "Bonjour cher clinkastien, \n"
							+ "\nNous vous rappellons que vous n'avez pas encore fait valider votre calendrier"
							+ " pour ce mois de "
							+ new SimpleDateFormat("MMMM").format(cal.getTime())
							+ " "
							+ iYear
							+ "."
							+ "\nMerci de le remplir et de le soumettre dans les plus bref délais."
							+ "\nVous pouvez vous connecter à votre compte via le lien suivant :"
							+ "http://intranet.clinkast.fr:81/ess/"
							+ "\nCordialement,\nClinkastTeam";
					new MailSender(receiverEmailIDs, subject, body);
					resultat = "Votre message a été envoyé!";
				}

				String all = request.getParameter("all");
				if(Boolean.parseBoolean(all)) {
					cal = new GregorianCalendar();
					List<String> receiverEmailIDs = new ArrayList<String>();
					List<String> LoginUserSoumis = new ArrayList<String>();
					List<Utilisateur> utilisateurs = utilisateurDao.lister();
					for (Utilisateur user : utilisateurs) {
						if(user.isUser() && user.isActif()) {
							List<Calendrier> calendriers = calendarDao
									.listerCalendrier(user, iMonth, iYear);
							if (calendriers.isEmpty())
								receiverEmailIDs.add(user.getLogin());
							else if(calendriers.get(0).getStatus() == 0 )
								receiverEmailIDs.add(user.getLogin());
							else if(calendriers.get(0).getStatus() == 1) 

								LoginUserSoumis.add(user.getLogin());
						}
					}

					cal.set(Calendar.YEAR, 2008);
					cal.set(Calendar.MONTH, iMonth);
					cal.set(Calendar.DAY_OF_MONTH, 01);
					String subject = "Validation de votre calendrier";
					String body = "Bonjour cher clinkastien, \n"
							+ "\nNous vous rappellons que vous n'avez pas encore fait valider votre calendrier"
							+ " pour ce mois de "
							+ new SimpleDateFormat("MMMM").format(cal.getTime())
							+ " "
							+ iYear
							+ "."
							+ "\nMerci de le remplir et de le soumettre dans les plus bref délais."
							+ "\nVous pouvez vous connecter à votre compte via le lien suivant :"
							+ "http://intranet.clinkast.fr:81/ess/"
							+ "\nCordialement,\nClinkastTeam";				
					if(!receiverEmailIDs.isEmpty()){
						new MailSender(receiverEmailIDs, subject, body);
						resultat = "Votre message a été envoyé!";
					}
					else resultat = "Auncun message n'a été envoyé!";
					receiverEmailIDs.clear();
					List<Utilisateur> utilisateurAdmin = utilisateurDao.lister();
					for (Utilisateur user : utilisateurAdmin) {
						if(user.isAdmin() || user.isRh()) 
							receiverEmailIDs.add(user.getLogin());
						subject = "Rappel Soumission Calendrier";
						body = "Bonjour cher admin, \n"
								+ "\nVeuillez valider le CRA des utilisateurs ci-dessous qui ont un statut soumis:"
								+"\n"+String.join(" et ", LoginUserSoumis)
								+ "\npour ce mois de\t"
								+ new SimpleDateFormat("MMMM").format(cal.getTime())
								+ " "
								+ iYear
								+ "."
								+ "\nVous pouvez vous connecter à votre compte via le lien suivant :"
								+ "http://intranet.clinkast.fr:81/ess/"
								+ "\nCordialement,\nClinkast Team";				
						if(!LoginUserSoumis.isEmpty()){
							new MailSender(receiverEmailIDs, subject, body);
						}

					}

					request.setAttribute("projetDao" , projetDao );
					request.setAttribute("calendarDao", calendarDao);
					request.setAttribute("utilisateurDao", utilisateurDao  );
					request.setAttribute("profilDao", profilDao);
					request.setAttribute( Constants.ATT_ANNEE, iYear );
					request.setAttribute( Constants.ATT_MOIS, iMonth );
				}

			}
			else {
				session.setAttribute( Constants.ATT_SESSION_USER, null );
			}
			request.setAttribute("resultat", resultat);
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );

		}

	}}
