package com.clinkast.cra.servlets;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.ClientDaoImpl;
import com.clinkast.cra.dao.DAOFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientList.
 */
@WebServlet( "/clientList" )
public class ClientList extends HttpServlet {
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /** The Constant VUE. */
    public static final String VUE        = "/pages/protected/admin/clientListing.jsp";
    
    
    /** The client dao. */
    private ClientDaoImpl         clientDao;



    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException {
        /* Récupération d'une instance de nos DAO Client et Commande */
    	 this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getClientDao();
    }
    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, affichage de la liste des clients */
    	
    	List<Client> listeClients = clientDao.lister();
        Map<Long, Client> clients = new HashMap<Long, Client>();
        for ( Client client : listeClients ) {
        	clients.put( client.getId(), client );
        }
        request.setAttribute( Constants.ATT_CLIENTS, clients );
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}
