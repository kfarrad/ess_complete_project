package com.clinkast.cra.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ResumeDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;

@WebServlet( "/xmlResume" )
public class ViewXmlResume extends HttpServlet {
        /**
         * 
         */
        private static final long serialVersionUID                  = 1L;
        
        public static final String VUE         = "/pages/protected/defaultUser/cv_form_2.jsp";
        
        public static final String VUE_ERREUR  = "/pages/public/error_500.jsp";
       
        private ResumeDaoImpl                   resumeDao;
        
        /** The utilisateur dao. */
        private UserDaoImpl		  			    utilisateurDao;
        
        
        /** The resultat. */
        private String              resultat;
        
        /** The erreurs. */
        private Map<String, String> erreurs         = new HashMap<String, String>();
        
        /**
         * Gets the erreurs.
         *
         * @return the erreurs
         */
        public Map<String, String> getErreurs() {
                return erreurs;
        }
        
        /**
         * Sets the erreur.
         *
         * @param champ the champ
         * @param message the message
         */
        private void setErreur( String champ, String message ) {
            erreurs.put( champ, message );
        }
        
        
        /**
         * Gets the resultat.
         *
         * @return the resultat
         */
        public String getResultat() {
                return resultat;
        }
        
        
        
        /* (non-Javadoc)
         * @see javax.servlet.GenericServlet#init()
         */
        public void init() throws ServletException {
                /* Récupération d'une instance de notre DAO Utilisateur */
                this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();  
                this.utilisateurDao 	= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
        }
        
        public void Comments(){
        /*public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
                 Affichage de la page d'upload 
                HttpSession session = request.getSession();
                Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));

                String idChamp=request.getParameter("idResume");
                Long id_resume= 0L;
                if(idChamp!=null) id_resume = Long.parseLong( idChamp );
                
                Long id_user=utilisateur.getId();
                
                UserProfil profil = new UserProfil();
                Resume resume = new Resume();
               
                try {

                        resume.setTitle(profilDao.trouver(utilisateur.getId_profil()).getName());
                        resume.setCompetences(competenceDao.lister(utilisateur.getId()));
                        resume.setExperiences(workExpDao.lister(utilisateur.getId()));
                        resume.setFormations(formationDao.lister(utilisateur.getId()));
                        resume.setLangues(langueDao.lister(utilisateur.getId()));
                } catch (DAOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
                try {
                        JAXBContext context = JAXBContext.newInstance(Resume.class);
                        Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                        marshaller.marshal(resume, System.out);
                } catch (PropertyException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                } catch (JAXBException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
                String chemin = Constants.getCheminUpload();
                try {
                         TransformerFactory tFactory = TransformerFactory.newInstance();
                        Transformer transformer = tFactory.newTransformer(new StreamSource("sample.xsl"));
                        transformer.transform(new StreamSource("sample.xml"),new StreamResult(new FileOutputStream("sample.html")));
                          //String File_To_Convert = "test.html";
                        
                         Create a TransformerFactory object            
                        //TransformerFactory tFactory = TransformerFactory.newInstance();
                         Get the incoming XSLT file 
                        // Transformer transformer = tFactory.newTransformer(new StreamSource(chemin +File.separator
                        //                +"xml" + File.separator+ "resume.xsl"));
                         Get the XML file and apply the XSLT transformation to convert to HTML 
                        //transformer.transform(new StreamSource("sample.xml"),new StreamResult(new FileOutputStream(
                        //               chemin +File.separator +"xml" + File.separator+"test.html")));
                        
                        
                        //String url = new File(chemin +File.separator + File_To_Convert).toURI().toURL().toString();
                        //System.out.println(""+url);
                        
                         URL pageURL = new URL("http://localhost:8080/ess/viewResume?id=" + utilisateur.getId_profil());  
                        System.out.println(pageURL.toString());
                        BufferedReader in = new BufferedReader(new InputStreamReader(pageURL.openStream()));  
                        BufferedWriter bw = new BufferedWriter(new FileWriter("myTempFile"+request.getSession().getId()+".html"));  
                        String inputLine;  
                                      
                        while ((inputLine = in.readLine()) != null)  
                              bw.write(inputLine);  
                          
                        in.close();  
                        bw.flush();  
                        bw.close();  
                                  
                                  
                        String inputFile = "myTempFile"+request.getSession().getId()+".html";  
                        String url = new File(inputFile).toURI().toURL().toString();  
                        String outputFile = "pdfSave.pdf";  
                        OutputStream os = new FileOutputStream(outputFile);  
                        ITextRenderer renderer = new ITextRenderer();  
                        renderer.setDocument(url);  
                        renderer.layout();  
                          
                         
                            renderer.createPDF(os);  
                        profil = profilDao.trouver(utilisateur.getLogin());
                        
                        resumeDao.verifier(id_resume,id_user);
                        String HTML_TO_PDF = profil.getNom()+"_"+ id_user+"_"+ id_resume +".pdf";
                        OutputStream os = new FileOutputStream(chemin + File.separator + HTML_TO_PDF);       
                        ITextRenderer renderer = new ITextRenderer();
                        //Document document = XMLResource. load(new ByteArrayInputStream(outputStream.getBytes())).getDocument();
                        
                        
                        renderer.setDocument("http://localhost:8080/ess/viewResume?id=" + utilisateur.getId_profil());      
                        renderer.layout();
                        renderer.createPDF(os, true, 1);
                        renderer.finishPDF();  
                        os.close();  
                } catch (DocumentException e) {
                        e.printStackTrace();
                } 
                response.sendRedirect( request.getContextPath() + "/download/" + profil.getNom()+"_"+ id_user +"_"+ id_resume +".pdf");
        }*/
        }

        public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
                //Affichage de la page d'upload 
                HttpSession session = request.getSession();
                Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
        	
                //get url
                String scheme = request.getScheme();             // http
		String serverName = request.getServerName();     // hostname.com
		int serverPort = request.getServerPort();        // 80
		String contextPath = request.getContextPath();   // /mywebapp
		StringBuffer url =  new StringBuffer();
		url.append(scheme).append("://").append(serverName);

		if ((serverPort != 80) && (serverPort != 443)) {
			url.append(":").append(serverPort);
		}
		
		url.append(contextPath);
		
                String idChamp=request.getParameter(Constants.PARAM_ID_RESUME);
                Long id_resume= 0L;
                if(idChamp!=null) id_resume = Long.parseLong( idChamp );                
                Long id_user = utilisateur.getId();
                try {
                        String chemin = Constants.getCheminUpload();
                        if(resumeDao.verifier(id_resume,id_user) || utilisateur.isAdmin()){
                        Long resume_User_Id= resumeDao.trouver(id_resume).getId_user();
                        String HTML_TO_PDF = utilisateurDao.trouver_ID(resume_User_Id).getProfil().getNom() +"_"+ resume_User_Id+"_"+ id_resume +".pdf";
                        OutputStream os = new FileOutputStream(chemin + File.separator + HTML_TO_PDF);       
                        ITextRenderer renderer = new ITextRenderer();
                        //Document document = XMLResource. load(new ByteArrayInputStream(outputStream.getBytes())).getDocument();
                        url.append("/viewResume?id=" + id_resume);
                        renderer.setDocument(url.toString());      
                        renderer.layout();
                        renderer.createPDF(os, true, 1); 
                        renderer.finishPDF();  
                        os.close();  
                        
                        resumeDao.creerPdf(id_resume,HTML_TO_PDF);
                        response.sendRedirect( request.getContextPath() + "/download/"+ HTML_TO_PDF);
                        }
                        else setErreur( "imprévu", "Erreur imprévue lors de la Creation." );
                }catch ( Exception e ) {
                        setErreur( "imprévu", "Erreur imprévue lors de la Creation." );
                        resultat ="Échec de la Création du CV : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
                        e.printStackTrace();
                }
                if(!this.getErreurs().isEmpty()) this.getServletContext().getRequestDispatcher( VUE_ERREUR ).forward( request, response );
        }
}
