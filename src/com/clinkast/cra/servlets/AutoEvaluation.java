package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.CompetencesDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.FormationsDemandesDAOImp;
import com.clinkast.cra.dao.FormationsSuiviesDAOImpl;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.ObjectifsDAOImpl;
import com.clinkast.cra.dao.RapportsDAOImpl;
import com.clinkast.cra.dao.SystemesActivitesDAOImpl;
import com.clinkast.cra.forms.ActivitesForm;
import com.clinkast.cra.forms.ExigencesForm;
import com.clinkast.cra.forms.FormationDemandesForm;
import com.clinkast.cra.forms.FormationSuiviesForm;
import com.clinkast.cra.forms.ObjectifsForm;
import com.clinkast.cra.forms.ResumeAutoEvaluationForm;

public class AutoEvaluation extends HttpServlet {
	public static final String PARAM_ACTIVITE = "activite";
	public static final String PARAM_ID = "id";
	public static final String CHAMP_ACTION = "champs[action]";
	public static final String ATT_DELETE = "delete";
	public static final String VUE = "/pages/protected/defaultUser/autoEvaluation_form.jsp";
	public static final String VUE_REDIRECTION = "/mesAutoEvaluations";
	public static final String ATT_CAT_CONSULTANT = "Metier du consultant";
	public static final String ATT_CAT_CLIENT = "Client";
	public static final String ATT_CAT_ENGAGEMENT = "Engagement et Qualités Individuelles";
	public static final String ATT_CAT_DEVELOPPEMENT = "Développement des Hommes et Management";
	public static final String CHAMP_ID = "rapport[id_auto]";
	GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	SystemesActivitesDAOImpl systemesActivitesDAOImpl;
	FormationsSuiviesDAOImpl formationsSuiviesDAOImpl;
	FormationsDemandesDAOImp formationsDemandesDAOImp;
	ObjectifsDAOImpl objectifsDAOImpl;
	CompetencesDAOImpl competencesDAOImpl;
	RapportsDAOImpl rapportsDAOImpl;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void init() throws ServletException {
        /* Récupération d'une instance de nos DAO Dates et Users */
		this.gestionEvaluationsDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getGestionEvaluationsDAOImpl();
		this.autoEvaluationDAOImpl = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getAutoEvaluationDAOImpl();
		this.systemesActivitesDAOImpl = ((DAOFactory) getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getSystemesActivitesDAOImpl();
		this.formationsSuiviesDAOImpl = ((DAOFactory) getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getFormationsSuiviesDAOImpl();
		this.objectifsDAOImpl = ((DAOFactory) getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getObjectifsDAOImpl();
		this.competencesDAOImpl = ((DAOFactory) getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getCompetencesDAOImpl();
		this.rapportsDAOImpl = ((DAOFactory) getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getRapportsDAOImpl();
		this.formationsDemandesDAOImp =((DAOFactory)getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getFormationsDemandesDAOImp();
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
        Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
        GestionEvaluations gestionEvaluations = null;
        String value = Constants.getValeurChamp(request, PARAM_ID);
		if(value != null)
		{
			Long idUser = 0L;
			Long idAutoEvaluation = 0L;
			Long idDate = 0L;
			String[] tabValue = value.split("&", -1);
			
			idDate = Long.parseLong(tabValue[0]);
			idAutoEvaluation = Long.parseLong(tabValue[1]);
			idUser = utilisateur.getId();
				
			 gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(idAutoEvaluation, idDate, idUser);
			 request.setAttribute( Constants.ATT_GESTION_EVALUATION, gestionEvaluations );
			 
			 //Liste toutes les rapports de competences
			 //List<Rapports> rapports= rapportsDAOImpl.lister(idAutoEvaluation);
			 //request.setAttribute( Constants.ATT_RAPPORTS_COMPETENCES, rapports );
		}
		//s'il n'ya pas de parametre ou gestionEvaluations est null
		if(value == null || gestionEvaluations == null || gestionEvaluations.getGestionEvaluationsDateTransmission() != null){
			/* Redirection vers la page publique */
			response.sendRedirect( request.getContextPath() + VUE_REDIRECTION );			
		}
		else
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		GestionEvaluations gestionEvaluations = null;
		
		/*
		 * Recupere le parametre de l'url
		*/
		try{
			/*Recuperation du formulaire envoyé*/
			String formType =Constants.getValeurChamp(request, "formType");
			
			/*	Traitement synthese d'activité */
			if(!formType.isEmpty() && formType.equals("ACT")){
				ActivitesForm form = new ActivitesForm(gestionEvaluationsDAOImpl, autoEvaluationDAOImpl, systemesActivitesDAOImpl);
				/*Action transmise*/
				String action = Constants.getValeurChamp(request, CHAMP_ACTION);
				//sauve pour ajout ou modification
				if(action.equals("sauve")){
					gestionEvaluations = form.sauveActivites(request);
				}
				if(action.equals(ATT_DELETE)){
					gestionEvaluations = form.supprimeActivites(request);
				}
				//Ajout form dans l'objet request
				request.setAttribute( Constants.ATT_FORM, form );
			}
			/*	Traitement formation suivie	*/
			else if(formType.equals("FMS"))
			{
				FormationSuiviesForm form = new FormationSuiviesForm(gestionEvaluationsDAOImpl, autoEvaluationDAOImpl, formationsSuiviesDAOImpl);
				/*Action transmise*/
				String action = Constants.getValeurChamp(request, CHAMP_ACTION);
				//sauve pour ajout ou modification
				if(action.equals("sauve")){
					gestionEvaluations = form.sauveFormationSuivies(request);
				}
				if(action.equals(ATT_DELETE)){
					gestionEvaluations = form.supprimeFormationSuivies(request);
				}
				//Ajout form dans l'objet request
				request.setAttribute( Constants.ATT_FORM, form );				
			}
			else if(formType.equals("OBJ"))
			{
				ObjectifsForm form = new ObjectifsForm(gestionEvaluationsDAOImpl, autoEvaluationDAOImpl, objectifsDAOImpl);
				/*Action transmise*/
				String action = Constants.getValeurChamp(request, CHAMP_ACTION);
				//sauve pour ajout ou modification
				if(action.equals("sauve")){
					gestionEvaluations = form.sauveObjectif(request);
				}
				if(action.equals(ATT_DELETE)){
					gestionEvaluations = form.supprimeObjectif(request);
				}
				//Ajout form dans l'objet request
				request.setAttribute( Constants.ATT_FORM, form );
			}
			else if(formType.equals("EXI")){
				ExigencesForm form = new ExigencesForm(gestionEvaluationsDAOImpl, autoEvaluationDAOImpl, competencesDAOImpl, rapportsDAOImpl); 
				gestionEvaluations = form.modifier(request);
				request.setAttribute( Constants.ATT_FORM, form );
			}
			else if(formType.equals("FDD")){
				FormationDemandesForm form = new FormationDemandesForm(gestionEvaluationsDAOImpl, autoEvaluationDAOImpl, formationsDemandesDAOImp);
				/*Action transmise*/
				String action = Constants.getValeurChamp(request, CHAMP_ACTION);
				//sauve pour ajout ou modification
				if(action.equals("sauve")){
					gestionEvaluations = form.sauveFormationDemandes(request);
				}
				if(action.equals(ATT_DELETE)){
					gestionEvaluations = form.supprimeFormationDemandes(request);
				}
				//Ajout form dans l'objet request
				request.setAttribute( Constants.ATT_FORM, form );	
			}
			else if(formType.equals("RES"))
			{
				ResumeAutoEvaluationForm form = new ResumeAutoEvaluationForm(gestionEvaluationsDAOImpl, autoEvaluationDAOImpl);
				gestionEvaluations = form.modifier(request);
				//Ajout form dans l'objet request
				request.setAttribute( Constants.ATT_FORM, form );	
			}
				
		}
		catch (DAOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		/*Traite les valeurs passé par get*/
		request.setAttribute( Constants.ATT_GESTION_EVALUATION, gestionEvaluations );
		
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

}
