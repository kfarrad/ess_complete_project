package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;




import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.ContactMailForm;

@WebServlet( "/contactMail" )
@MultipartConfig(fileSizeThreshold=1024*1024, 
maxFileSize=1024*1024*10, maxRequestSize=1024*1024*10*5)
public class ContactMail extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/RH/mailSuccess.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/RH/mail.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM_USER         = "/pages/protected/defaultUser/mailToRh.jsp";

	/** The user dao. */
	private UserDaoImpl          				userDao;
	private UserProfilDaoImpl					profilDao;

	/** The id_profil. */
	//private Long 								id_profil;

	/** The user insession. */
	private Utilisateur 						userInsession;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		HttpSession session = request.getSession();
		userInsession= (Utilisateur)(session.getAttribute( Constants.ATT_SESSION_USER ));
		
		List<Utilisateur> utilisateurs = userDao.lister();
		List<Utilisateur> listeUtilisateurs = new ArrayList<Utilisateur>();
		for (Utilisateur elt : utilisateurs) {

			if (elt.isUser())
				listeUtilisateurs.add( elt);
		}


		request.setAttribute(Constants.ATT_LISTE_USER, listeUtilisateurs);
		if(!userInsession.isUser())
			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );		
		else this.getServletContext().getRequestDispatcher( VUE_FORM_USER ).forward( request, response );		


	}
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* Préparation de l'objet formulaire */

		ContactMailForm form = new ContactMailForm(userDao, profilDao);
		form.sendMailFile(request, userInsession);
		request.setAttribute(Constants.ATT_FORM, form);

		if ( form.getErreurs().isEmpty() ) {
			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );	
		}
		else{
			request.setAttribute(Constants.ATT_FORM, form);
			if(!userInsession.isUser())
				this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );		
			else this.getServletContext().getRequestDispatcher( VUE_FORM_USER ).forward( request, response );	
		}
	}
}
