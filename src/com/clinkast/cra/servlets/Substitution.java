package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class Substitution.
 */
@WebServlet( "/substitution" )
public class Substitution extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user dao. */
	private UserDaoImpl        userDao;
	
	private CongesDaoImpl congesDao;
	
	private Utilisateur utilisateur;

	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/public/index.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/defaultUser/substitution.jsp";

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		

		this.congesDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getCongesDao();

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		/* À la réception d'une requête GET, simple affichage du formulaire */
		HttpSession session = request.getSession();
		request.setAttribute("utilisateurDao", userDao);
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		//doPost(request, response);
	}


	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* Préparation de l'objet formulaire */
		

		String id = request.getParameter("id");
		if ((id != null) && (!"".equals(id) )) {
			/* Traitement de la requête et récupération du bean en résultant */  
			HttpSession session = request.getSession();
			Utilisateur utilisateur = new Utilisateur();
			utilisateur = userDao.trouver(Long.valueOf(id));
			int nombreDemandeConge = congesDao.listerCongesByUserAndStatut(0L).size();
			session.setAttribute( Constants.ATT_SESSION_USER, utilisateur );
			session.setAttribute( "nombreDemandeCongeNonValide", nombreDemandeConge );
		}
		response.sendRedirect( request.getContextPath() + "/acceuil");
			

	}
}