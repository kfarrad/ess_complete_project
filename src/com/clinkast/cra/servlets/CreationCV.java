package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CandidatureDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.ValidationTraitementCandidatureForm;

@WebServlet("/creationCV")
@MultipartConfig
public class CreationCV extends HttpServlet {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;

	public static final String VUE = "/pages/protected/RH/creationCV.jsp";

	CandidatureDaoImpl candidatureDao;
	
	public final void init() throws ServletException {
        /* Récupération d'une instance de notre DAO */
        this.candidatureDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCandidature();
    }
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Part filePart = request.getPart("file"); 
		ValidationTraitementCandidatureForm validation = new ValidationTraitementCandidatureForm(candidatureDao);
		
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) (session.getAttribute(Constants.ATT_SESSION_USER));
		request.setAttribute( Constants.ATT_FORM, validation );
		Candidat candidat = validation.creationCandidature(request, filePart, utilisateur.getName());
		String [] diplomes = candidat.getDiplomes().split(";");
		String [] specialites = candidat.getSpecilites().split(";");
		// Ne pas envoyer l'objet candidat si il a été crée pour initialiser le formulaire de création
		if (!validation.isCreationStatut()) {
			request.setAttribute( Constants.ATT_CANDIDAT, candidat );
			request.setAttribute(Constants.ATT_DIPLOMES_CANDIDAT, diplomes);
			request.setAttribute(Constants.ATT_SPECIALITES_CANDIDAT, specialites);
		}
		request.setAttribute("creationStatut", validation.isCreationStatut());
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

}
