package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Contenu;
import com.clinkast.cra.dao.ContenuDaoImp;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.CreationContenuForm;

@WebServlet("/modificationPublication")
public class ModificationContenu extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String VUE = "/pages/protected/admin/modificationPublication.jsp";
	
	private ContenuDaoImp contenuDao;
	private long id;
	
	@Override
	public void init() throws ServletException {
		contenuDao = ((DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY )).getContenu();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String id_ = req.getParameter("idPublication");
		try {
			id = Long.valueOf(id_);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		Contenu contenu = contenuDao.getContenuById(id);
		req.setAttribute("contenu", contenu);
		
		req.getRequestDispatcher(VUE).forward(req, resp);
		
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		CreationContenuForm creationContenu = new CreationContenuForm(contenuDao);
		creationContenu.modificationContenu(request, id);
		if (creationContenu.getErreurs().isEmpty()) {
//			request.setAttribute("statut", "success");
			response.sendRedirect( request.getContextPath() + "/voirContenu?statutUpdate=true");
		} else {
			this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
		}
	}
	
}
