package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserProfilDaoImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class UserProfilSuppression.
 */
@WebServlet( "/userProfilSuppression" )
public class UserProfilSuppression extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant VUE. */
	public static final String VUE              = "/userProfilList";

	/** The profil dao. */
	private UserProfilDaoImpl profilDao;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.profilDao =  ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* Récupération du paramètre */
		String idProfil = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
		Long id_profil = Long.parseLong( idProfil );

		/* Si l'id du profil n'est pas vide */
		if ( id_profil != null){
			UserProfil profil = profilDao.trouver(id_profil);
			if(profil!=null){
				try {
	        		profilDao.supprimer(profil);
				}catch ( DAOException e ) {
                e.printStackTrace();
                }
			
        	}
		}

        /* Redirection vers la fiche récapitulative */
        response.sendRedirect( request.getContextPath() + VUE );
	}
	}
