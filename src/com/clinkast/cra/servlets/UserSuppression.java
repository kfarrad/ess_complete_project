package com.clinkast.cra.servlets;

import java.io.IOException;







import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;










import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserDaoImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class UserSuppression.
 */
public class UserSuppression extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3912264567087989740L;

	/** The Constant VUE. */
	public static final String VUE              = "/userProfilList";

	/** The user dao. */
	private UserDaoImpl          userDao;
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* Récupération du paramètre */
		String idClient = Constants.getValeurChamp( request, Constants.PARAM_ID_PROFIL );
		Long id_profil = Long.parseLong( idClient );

		/* Si l'id du profil n'est pas vide */
		if ( id_profil != null) {
			try {
				Utilisateur utilisateur =userDao.trouver(id_profil);
				/* Alors desactivation du  compte de la BDD */
				if (utilisateur!=null)			
				        userDao.supprimer( utilisateur );

			} catch ( DAOException e ) {
				e.printStackTrace();
			}

		}

		/* Redirection vers la fiche récapitulative */
		response.sendRedirect( request.getContextPath() + VUE );
	}

	
}


