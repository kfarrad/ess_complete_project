package com.clinkast.cra.servlets;

import java.io.IOException;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;



import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;



import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;


// TODO: Auto-generated Javadoc
/**
 * The Class ProjetAttribution.
 */
@WebServlet( "/projetAttribution" )
public class ProjetAttribution extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 	= 1L;

	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/projetList";
	
	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/projetAttribute.jsp";

	/** The projet dao. */
	private ProjetDaoImpl          projetDao;
	
	/** The user dao. */
	private UserDaoImpl				userDao;
	
	/** The profil dao. */
	private UserProfilDaoImpl		profilDao;
	
	/** The projet. */
	private Projet 	projet;
	

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */

		this.projetDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.userDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
		this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();

	}


	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */
		String idProjet = Constants.getValeurChamp( request, Constants.ATT_ID );
		Long id=0L;
		/* recuperation de l'objet profil à modifier */ 
		try {
			if(idProjet!=null) id = Long.parseLong(idProjet);
			projet = projetDao.trouver(id);
			if(projet!=null){
				request.setAttribute( Constants.ATT_PROJET, projet );        
				List<UserProfil> utilisateurs = profilDao.lister();
				List<String> projetUsers = userDao.listerProjetsUsers(projet);
				request.setAttribute( Constants.ATT_USERS, utilisateurs );
				request.setAttribute(Constants.ATT_PROJE_USERS, projetUsers);

				
			}
		} catch ( DAOException e ) {
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	//@SuppressWarnings("unchecked")
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		//HttpSession session = request.getSession();
		String[ ] usersList = (String[ ])request.getParameterValues( "userslist" ) ;
		List<Utilisateur> listeUsers = userDao.lister();
		if(usersList!=null){
			try{
				for (String idProfil : usersList) {
					Utilisateur utilisateur = userDao.trouver(Long.parseLong(idProfil));
					if((utilisateur!=null)&&(!projetDao.trouverAttribution(utilisateur.getId(), projet.getId()))) 
						projetDao.attribution(utilisateur.getId(), projet.getId());
					listeUsers.remove(listeUsers.indexOf(utilisateur));
				}
				
				for(Utilisateur eltNonCheck: listeUsers){
					if(projetDao.trouverAttribution(eltNonCheck.getId(), projet.getId()))
						projetDao.supprimerAttribution(eltNonCheck.getId(),projet.getId());
				}	
				
				/*Map<Projet, List<String>> projetsUtilisateurs  = (HashMap<Projet, List<String>>) session.getAttribute( Constants.ATT_PROJETS_USERS );
				if(projetsUtilisateurs==null) projetsUtilisateurs   = new HashMap<Projet, List<String>>();
				List<String> listeProjetUsers = userDao.listerProjetsUsers(projet);
				projetsUtilisateurs.put(projet, listeProjetUsers);
				session.setAttribute( Constants.ATT_PROJETS_USERS, projetsUtilisateurs );

				this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
				*/
				

				/* Redirection vers la fiche récapitulative */
				response.sendRedirect( request.getContextPath() + VUE_SUCCES );
				
				
			} catch ( DAOException e ) {
				e.printStackTrace();
			}
		}
		else {
			if(listeUsers!=null){
				for(Utilisateur eltNonCheck: listeUsers){
					if(projetDao.trouverAttribution(eltNonCheck.getId(), projet.getId()))
							projetDao.supprimerAttribution(eltNonCheck.getId(),projet.getId());
					}	
			}
			response.sendRedirect( request.getContextPath() + VUE_SUCCES );
			
		}
	}

}
