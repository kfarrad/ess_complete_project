package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.CalendarCreationForm;


@WebServlet( "/holidays" )
public class Holidays extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/admin/ferierSuccess.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/holiday.jsp";

	private CalendarDaoImpl        calendarDao;


	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.calendarDao	 = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */

		String anneeT = Constants.getValeurChamp(request, "annee" );
		String moisT = Constants.getValeurChamp(request, "mois" );
		int iYear= Constants.signedIntconv(anneeT);
		int iMonth= Constants.signedIntconv(moisT);


		Calendar ca = new GregorianCalendar();
		int mois = ca.get(Calendar.MONTH);
		int annee = ca.get(Calendar.YEAR);

		if(iYear==-1)
		{
			iYear=annee;
		}
		if(iMonth==-1)
		{
			iMonth=mois;
		}
		Calendrier calendrier = calendarDao.trouver(-1L, -1L, (long)iYear,(long) iMonth);
		List<Integer> feriers = new ArrayList<Integer>();
		
		if(calendrier!=null){
			int i=1;
		
		for(Float ferier:calendrier.getJours()){
			if(ferier==-1) feriers.add(i);
			i++;
		}
		}
		
		request.setAttribute( Constants.ATT_ANNEE, iYear );
		request.setAttribute( Constants.ATT_MOIS, iMonth );
		request.setAttribute( Constants.ATT_FERIERS, feriers );
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );

	}

	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		CalendarCreationForm form= new CalendarCreationForm(calendarDao);
		form.joursFerier(request);
		request.setAttribute( Constants.ATT_FORM, form ); 
		if(form.getErreurs().isEmpty()){
			/* Redirection vers la fiche récapitulative */
			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
		}
		else {
			// Sinon, ré-affichage du formulaire de création avec les erreurs 

			this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
		}
	}

}
