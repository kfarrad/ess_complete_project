package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Contenu;
import com.clinkast.cra.dao.ContenuDaoImp;
import com.clinkast.cra.dao.DAOFactory;
@WebServlet("/acceuil")
public class Acceuil extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final static String VUE = "/pages/public/index.jsp";
	
	private ContenuDaoImp contenuDao;
	
	@Override
	public void init() throws ServletException {
		contenuDao = ((DAOFactory)this.getServletContext().getAttribute(Constants.CONF_DAO_FACTORY)).getContenu();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<Contenu> contenu = new ArrayList<Contenu>();
		contenu = contenuDao.lister(0, 10);
		req.setAttribute("contenus", contenu);
		this.getServletContext().getRequestDispatcher(VUE).forward(req, resp);
	}

}
