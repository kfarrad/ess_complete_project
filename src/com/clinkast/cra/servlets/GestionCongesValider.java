package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Conges;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.mails.MailSender;

/**
 * Servlet implementation class GestionCongesValider
 */
@WebServlet("/gestionCongesValider")
public class GestionCongesValider extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE_REDIRECTION = "/pages/protected/defaultUser/congesCraListe.jsp";
	/** The calendar dao. */
	private CalendarDaoImpl calendarDao;
	private CongesDaoImpl congesDao;
	private UserDaoImpl utilisateurDao;
	private ProjetDaoImpl projetDao;
	private Utilisateur utilisateur;

	// private Calendrier calendrier;

	private long idConge;

	public void init() throws ServletException {
		/* RÃ©cupÃ©ration d'une instance de notre DAO Utilisateur */
		// this.calendarDao = ( (DAOFactory) getServletContext().getAttribute(
		// Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.calendarDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getCalendarDao();
		this.projetDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getProjetDao();
		this.utilisateurDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getUserDao();
		// this.profilDao = ( (DAOFactory) getServletContext().getAttribute(
		// Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
		this.congesDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getCongesDao();
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionCongesValider() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String idConge_ = request.getParameter("idConge");
		String accepter_ = request.getParameter("accepter");
		String statutValidation = request.getParameter( "statutValidation" );

		int accepter = -1;
		try {
			idConge = Long.valueOf(idConge_);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		try {
			accepter = Integer.valueOf(accepter_);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		
		if (accepter == 1) {
			if (statutValidation != null && statutValidation.equals("true") )
				request.setAttribute( "statutValidation", true );
			
			Conges conge = congesDao.trouver(idConge);
			
			utilisateur = utilisateurDao.trouver_ID(conge.getId_user());

			List<Projet> projets = projetDao.listerUserProjetsActif(utilisateur);

			Projet projet = new Projet();
			
			List<Calendrier> calendriers = calendarDao.lister(conge.getId_user(),conge.getMois(), conge.getAnnee() );

			List<Float> jours_par_defaut = new ArrayList<Float>();
			
			List<Float> jours_projet_facture = new ArrayList<Float>();
			
			double total_jour_projet_facture = 0;
			double total_weekends_projet_facture = 0;

			for (int i = 1; i <= 31; i++) {
				jours_par_defaut.add(0f);
			}
			
			
			int year = (int) (long) conge.getAnnee();
			int month = (int) (long) conge.getMois();
			Calendar cal = new GregorianCalendar(year, month, 1);
			do {
			    // get the day of the week for the current day
			    int day = cal.get(Calendar.DAY_OF_WEEK);
			    // check if it is a Saturday or Sunday
			    if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
			        // print the day - but you could add them to a list or whatever
			        total_weekends_projet_facture++;
			    }
			    // advance to the next day
			    cal.add(Calendar.DAY_OF_YEAR, 1);
			}  while (cal.get(Calendar.MONTH) == month);
			
			
//			Calendar c = Calendar.getInstance();
//			c.set((int) (long) conge.getAnnee(), (int) (long) conge.getMois(),1);
//			int day = c.get(Calendar.DAY_OF_WEEK);
		    // check if it is a Saturday or Sunday
		    
			for (int i = 0; i < 31; i++) {
				if (conge.getJours().get(i).equals(new Float(1))) {
					jours_projet_facture.add(0f);
				} else if(conge.getJours().get(i).equals(new Float(0))) {
					jours_projet_facture.add(1f);
						total_jour_projet_facture++;
				}  else if(conge.getJours().get(i).equals(new Float(0.5))) {
					jours_projet_facture.add(0.5f);
					total_jour_projet_facture = total_jour_projet_facture + 0.5 ;
				}
			}
					
			if (calendriers.isEmpty()) {

				for (Projet projet_ : projets) {

					if (projet_.getId().equals(conge.getId_motif())) {
						projet = projet_;
						Calendrier calendrier = new Calendrier();
						calendrier.setId_user(conge.getId_user());
						calendrier.setStatus(0L);
						calendrier.setId_projet(conge.getId_motif());
						calendrier.setAnnee(conge.getAnnee());
						calendrier.setMois(conge.getMois());
						calendrier.setTotal_jour(conge.getTotal_jour());
						calendrier.setJours(conge.getJours());

						calendarDao.creer(calendrier);
						
					} else {
						Calendrier calendrier = new Calendrier();
						if (Constants.getIdProjetsNonFactures().contains((int) (long) projet_.getId())) {
							calendrier.setId_user(conge.getId_user());
							calendrier.setStatus(0L);
							calendrier.setId_projet(projet_.getId());
							calendrier.setAnnee(conge.getAnnee());
							calendrier.setMois(conge.getMois());
							calendrier.setTotal_jour(0D);
							calendrier.setJours(jours_par_defaut);
							
						} else {
							calendrier.setId_user(conge.getId_user());
							calendrier.setStatus(0L);
							calendrier.setId_projet(projet_.getId());
							calendrier.setAnnee(conge.getAnnee());
							calendrier.setMois(conge.getMois());
							calendrier.setTotal_jour(total_jour_projet_facture - total_weekends_projet_facture);
							calendrier.setJours(jours_projet_facture);
						}
						
						calendarDao.creer(calendrier);

					}

				}

			} else {
				
				for (Projet projet_ : projets) {

					if (projet_.getId().equals(conge.getId_motif())) {
						List<Float> tmpJours = new ArrayList<Float>();
						Calendrier tmpCalendar = calendarDao.trouver(conge.getId_user(), projet_.getId(), conge.getAnnee(), conge.getMois());
						for ( int i = 0; i < 31; i++ ) {
							if (tmpCalendar.getJours().get(i).equals(conge.getJours().get(i))
								|| conge.getJours().get(i).equals(new Float(0)))
								tmpJours.add(i, tmpCalendar.getJours().get(i));
							else {
								tmpJours.add(i, conge.getJours().get(i));
							}
						}
						projet = projet_;
						Calendrier calendrier = new Calendrier();
						calendrier.setId_user(conge.getId_user());
						calendrier.setStatus(0L);
						calendrier.setId_projet(conge.getId_motif());
						calendrier.setAnnee(conge.getAnnee());
						calendrier.setMois(conge.getMois());
						calendrier.setTotal_jour(conge.getTotal_jour());
						calendrier.setJours(tmpJours);

						calendarDao.modifier(calendrier, conge.getId_user(), projet_.getId(), conge.getAnnee(), conge.getMois());

					} else {
						double compteur = 0;
						Calendrier calendrier = new Calendrier();
						if (!Constants.getIdProjetsNonFactures().contains((int) (long) projet_.getId())) {
							Calendrier projetFacture = calendarDao.trouver(conge.getId_user(),projet_.getId(),conge.getAnnee(),conge.getMois());
							for (int i = 0; i < 31; i++) {
								if (conge.getJours().get(i).equals(new Float(1)) && projetFacture.getJours().get(i).equals(new Float(1))) {
									jours_projet_facture.set(i, new Float(0));
									compteur++;
								} else if (conge.getJours().get(i).equals(new Float(0.5))) {
									jours_projet_facture.set(i, new Float(0.5));
									if (projetFacture.getJours().get(i).equals(new Float(0))) {
										compteur = compteur - 0.5;
									} else
										compteur = compteur + 0.5;
								} else if (projetFacture.getJours().get(i).equals(new Float(0.5))) {
									jours_projet_facture.set(i, new Float(0.5));
									
								} else {
									if (projetFacture.getJours().get(i).equals(new Float(0))) {
										jours_projet_facture.set(i, new Float(0));
									}
								}
							}
							
							calendrier.setId_user(conge.getId_user());
							calendrier.setStatus(0L);
							calendrier.setId_projet(projet_.getId());
							calendrier.setAnnee(conge.getAnnee());
							calendrier.setMois(conge.getMois());
							calendrier.setTotal_jour(projetFacture.getTotal_jour() - compteur);
							calendrier.setJours(jours_projet_facture);
							
							calendarDao.modifier(calendrier, conge.getId_user(), projet_.getId(), conge.getAnnee(), conge.getMois());
							
						}
					}

				}

			}
			
			congesDao.setStatus(idConge, 1L);
			
			 List<String> receiverEmailIDs = new ArrayList<String>();
			    receiverEmailIDs.add(utilisateur.getLogin());
			
			String subject = "Validation de demande de congé";
			String body = "Bonjour, \n"
					+ "\nLa demande de congé: "
					+ "\n----------------------------------------------------------------------"
					+ "\n Nom de congé: " + projet.getNom() + ""
					+ "\n Période: "+ conge.getPeriode() + ""
					+ "\n Total des jours: "+conge.getTotal_jour() + ""
					+ "\n----------------------------------------------------------------------"
					+ "\n a été validée par l'administrateur"
					+ "\n\nCordialement,\nClinkastTeam";

			new MailSender(receiverEmailIDs, subject, body);
			
			response.sendRedirect( request.getContextPath() + "/gestionCongesListe?statutValidation=true");

		} else if (accepter == 0) {
			congesDao.setStatus(idConge, -1L);
			response.sendRedirect( request.getContextPath() + "/gestionCongesListe?statutValidation=true");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		/*
		 * String periode = request.getParameter("idPeriode");
		 * System.out.println("periode: "+periode); List<Conges> congeListe =
		 * congesDao.listerCongesValide(periode);
		 * 
		 * long status= 2;
		 * 
		 * 
		 * for (Conges conge: congeListe) { calendrier= new Calendrier();
		 * calendrier.setId(conge.getId()); calendrier.setStatus(status);
		 * calendrier.setId_user(conge.getId_user());
		 * calendrier.setId_projet(conge.getId_motif());
		 * calendrier.setAnnee(conge.getAnnee());
		 * calendrier.setMois(conge.getMois());
		 * calendrier.setTotal_jour(conge.getTotal_jour());
		 * calendrier.setJours(conge.getJours()); }
		 * 
		 * calendarDao.creer(calendrier);
		 */

		this.getServletContext().getRequestDispatcher(VUE_REDIRECTION)
				.forward(request, response);
	}

}
