package com.clinkast.cra.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.ClientDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.forms.ClientCreationForm;


@WebServlet( "/clientUpdate" )
public class ClientUpdate extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 	= 1L;

	/** The Constant VUE_SUCCES. */
	public static final String VUE_SUCCES       = "/pages/protected/admin/clientSuccess.jsp";

	/** The Constant VUE_FORM. */
	public static final String VUE_FORM         = "/pages/protected/admin/clientUpdating.jsp";

	/** The client dao. */
	private ClientDaoImpl          clientDao;

	public void init() throws ServletException {
		this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getClientDao();

	}

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */

		String id_client = Constants.getValeurChamp( request, Constants.ATT_ID_CLIENT );
		Long idClient = 0L;
		if(id_client!=null) idClient= Long.parseLong( id_client );
		/* recuperation des objets  à modifier */ 
		try {
			Client client = clientDao.trouver(idClient);
			if(client!=null){request.setAttribute( Constants.ATT_CLIENT, client );
			}
		} catch ( DAOException e ) {
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		ClientCreationForm form = new ClientCreationForm(clientDao);
		String id_client = Constants.getValeurChamp( request, Constants.ATT_ID_CLIENT );
		Long idClient = 0L;
		if(id_client!=null) idClient= Long.parseLong( id_client );

		Client client =form.modifierClient(request, idClient);

		request.setAttribute( Constants.ATT_FORM, form ); 
		request.setAttribute( Constants.ATT_CLIENT, client);
		if(form.getErreurs().isEmpty()){
			/* Redirection vers la fiche récapitulative */
		    
			this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
			}
			else {
	        	// Sinon, ré-affichage du formulaire de création avec les erreurs 

			 this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
			}
	
	}
}