package com.clinkast.cra.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Conges;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
/**
 * Servlet implementation class HistoriqueConges
 */
@WebServlet("/HistoriqueConges")
public class HistoriqueConges extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE_REDIRECTION = "/pages/protected/admin/congesHistorique.jsp";
	private CongesDaoImpl congesDao;
	private Utilisateur utilisateur;

	public void init() throws ServletException {
		this.congesDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getCongesDao();
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HistoriqueConges() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		utilisateur = (Utilisateur) (session
				.getAttribute(Constants.ATT_SESSION_USER));
		Long id_user = utilisateur.getId();

		String statutUpdate = request.getParameter("statutUpdate");
		int page = 1;
		try {
				page = Integer.parseInt(request.getParameter("page"));
		    } 
		catch (NumberFormatException e) {
		
		    } 
		catch (NullPointerException e) {
		     }
		int noOfRecords = 0;
		int noOfPages = 0;

		int nombreDemandeConge = congesDao.listerCongesByUserAndStatut(0L)
				.size();
		// String nom = utilisateur.getId_profil().getName();
		session.setAttribute("nombreDemandeCongeNonValide", nombreDemandeConge);

		if (utilisateur.isUser()) {
			List<Conges> congeListe = congesDao.listerCongesByUser(id_user, (page-1) * Constants.getRecordsPerPage(), Constants.getRecordsPerPage());
			System.out.println(congeListe.toString());
			noOfRecords = congesDao.getNoOfRecords();
			noOfPages = (int) Math.ceil(noOfRecords * 1.0 / Constants.getRecordsPerPage());
			// for (Conges conges : congeListe) {
			// congesListe
			// .put(projetDao.trouver(conges.getId_motif()), conges);
			// }

			request.setAttribute("congesListe", congeListe);
			request.setAttribute("noOfPages", noOfPages);
			request.setAttribute("currentPage", page);
			this.getServletContext().getRequestDispatcher(VUE_REDIRECTION)
			.forward(request, response);

		} else {
			String statutValidation = request.getParameter("statutValidation");

			if (statutValidation != null && statutValidation.equals("true"))
			request.setAttribute("statutValidation", true);
			request.setAttribute("congesListe", congesDao.listerCongesByUserByStatusNotZero((page-1) * Constants.getRecordsPerPage(), Constants.getRecordsPerPage()));
			noOfRecords = congesDao.getNoOfRecords();
			noOfPages = (int) Math.ceil(noOfRecords * 1.0 / Constants.getRecordsPerPage());
			request.setAttribute("noOfPages", noOfPages);
			request.setAttribute("currentPage", page);

			if (statutUpdate != null && statutUpdate.equals("true") )
				request.setAttribute( "statutUpdate", true );

			this.getServletContext().getRequestDispatcher(VUE_REDIRECTION)
			.forward(request, response);

		}

	}

}
