package com.clinkast.cra.servlets;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Resume;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ResumeDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;

public class ListCV extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID                  = 1L;
    
    public static final String VUE         = "/pages/protected/admin/listCV.jsp";        
    
    public static final String VUE_ERROR        = "/pages/public/error_404.jsp";
    
    public static final String VUE_REDIRECT      		= "/pages/public/accessDenied.jsp";
    /** The user dao. */
    private ResumeDaoImpl                  resumeDao;     
    
    /** The utilisateur dao. */
    private UserDaoImpl		  			    utilisateurDao;
    
    private Utilisateur utilisateur;
    
    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException {
	/* Récupération d'une instance de notre DAO Utilisateur */
	this.resumeDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getResumeDao();
	this.utilisateurDao 	= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
	/* Affichage de la page d'upload */
	HttpSession session = request.getSession();
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
	if(utilisateur==null) 
		this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
	if(utilisateur.isUser())
		this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
	if(utilisateur.isAdmin()){
		request.setAttribute("resumeDao" , resumeDao );
		List<Utilisateur> liste_Utilisateurs = utilisateurDao.lister();
		ArrayList<Long> liste_Id_User = new ArrayList<Long>();
		try {
			for(Utilisateur user : liste_Utilisateurs){
				Long id_User=user.getId();
				liste_Id_User.add(id_User);
			    List<Resume> listResume = resumeDao.lister(id_User);
			    request.setAttribute("listResume_"+id_User.toString(), listResume);
			    request.setAttribute("userName_"+id_User.toString(), user.getName());
			} 
			request.setAttribute("listeIdUser", liste_Id_User);
		} catch (DAOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    	}
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
	
	HttpSession session = request.getSession();
	utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
	if(utilisateur==null) 
		this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
	if(utilisateur.isUser())
		this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
	if(utilisateur.isAdmin()){
	
	String idChamp=request.getParameter("delete");
	Long id_resume= 0L;
	if(idChamp!=null) id_resume = Long.parseLong( idChamp );
	try {

		Resume resume = resumeDao.supprimer(id_resume);
		if(resume!=null){
		    String resumePdf = resume.getPdf();
		    if(resumePdf!=null){
			String chemin = Constants.getCheminUpload();
			File file = new File(chemin + File.separator+ resumePdf);
			file.delete();
		    }
		}
	    else this.getServletContext().getRequestDispatcher( VUE_ERROR ).forward( request, response );
	    
	    
	}catch (DAOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    
	}catch(Exception e){
	    
	    e.printStackTrace();
	    
	}
	response.sendRedirect( request.getContextPath() + "/listCV");
	
    }
    }
    
}

