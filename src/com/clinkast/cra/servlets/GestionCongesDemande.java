package com.clinkast.cra.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.clinkast.cra.beans.Conges;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CongesDaoImpl;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.ContactMailForm;
import com.clinkast.cra.mails.MailSender;

/**f
 * Servlet implementation class GestionCongesCra
 */
@WebServlet("/gestionCongesDemande")
public class GestionCongesDemande extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String VUE = "/pages/protected/defaultUser/congesCra.jsp";
	private CongesDaoImpl congesDao;
	private ProjetDaoImpl projetDao;
	private Utilisateur utilisateur;
	private UserProfilDaoImpl profilDao;

	private UserDaoImpl userDao;

	public void init() throws ServletException {
		/* RÃ©cupÃ©ration d'une instance de notre DAO Utilisateur */
		// this.calendarDao = ( (DAOFactory) getServletContext().getAttribute(
		// Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.userDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getUserDao();
		this.projetDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getProjetDao();
		((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getUserDao();
		this.profilDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getUserProfilDao();
		this.congesDao = ((DAOFactory) getServletContext().getAttribute(
				Constants.CONF_DAO_FACTORY)).getCongesDao();
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionCongesDemande() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String statutAjout = request.getParameter("statutAjout");

		HttpSession session = request.getSession();

		utilisateur = (Utilisateur) (session
				.getAttribute(Constants.ATT_SESSION_USER));

		List<Projet> projetsConges = projetDao.listerConges();
		request.setAttribute("projetConges", projetsConges);
		request.setAttribute("utilisateurDao", userDao);

		if (statutAjout != null && statutAjout.equals("true"))
			request.setAttribute("statutAjout", true);

		this.getServletContext().getRequestDispatcher(VUE)
				.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		utilisateur = (Utilisateur) (session
				.getAttribute(Constants.ATT_SESSION_USER));
		Long id_user = utilisateur.getId();
		String id_user2 = utilisateur.getName();
		
		

		String idSalarie = request.getParameter("salarie");
		if ((idSalarie != null) && (!"".equals(idSalarie) )) {
			
			/*La méthode "trouver" renvoie un utilisateur à partir d'un Id_profil
			passé en paramètre, récupéré depuis la liste déroulante.*/
			id_user = (userDao.trouver(Long.valueOf(idSalarie))).getId();
			id_user2 = (userDao.trouver(Long.valueOf(idSalarie))).getName();
			
		}

		// Long id_user2= utilisateur.getId_profil();

		String dateFrom = request.getParameter("fromDate");
		String dateTo = request.getParameter("toDate");
		String statut = request.getParameter("statut");
		List<Conges> listConges = new ArrayList<Conges>();

		/* String periode=request.getParameter( "periode" ); */
		String periode = "[" + dateFrom + "," + dateTo + "]";

		String motif = request.getParameter("motif");
		String journee = request.getParameter("journee");

		if (motif != null) {

			request.setAttribute("motif", motif);
		}

		if (statut != null) {

			request.setAttribute("statut", statut);
		}

		String jourD = null;
		String moisD = null;
		String anneeD = null;

		String jourF = null;
		String moisF = null;
		String annesF = null;

		String[] sD = dateFrom.split("/");
		String[] sF = dateTo.split("/");

		jourD = sD[0];
		moisD = sD[1];
		anneeD = sD[2];

		jourF = sF[0];
		moisF = sF[1];
		annesF = sF[2];

		System.out.println("Jours " + sD[0]);

		float fD = Float.parseFloat(jourD);
		float fF = Float.parseFloat(jourF);

		int ifD = Integer.parseInt(jourD);
		int ifF = Integer.parseInt(jourF);

		float mD = Float.parseFloat(moisD);
		float mF = Float.parseFloat(moisF);

		int imD = Integer.parseInt(moisD);
		int imF = Integer.parseInt(moisF);

		float aD = Float.parseFloat(anneeD);
		float aF = Float.parseFloat(annesF);

		int iaD = Integer.parseInt(anneeD);
		int iaF = Integer.parseInt(annesF);

		float finalJour = fF - fD;
		double JTotale = finalJour + 1;

		
		// cas ou les annÃ©es& mois et jours sont les memes
		
		if (aD == aF && mD == mF && fD == fF) {

			Conges conges = new Conges();

			// Calcule du jour

			List<Float> jours = new ArrayList<Float>();

			for (int i = 1; i <= 31; i++) {
				if (i == fD) {
					if (Float.parseFloat(journee) == 1)
						jours.add((int) i - 1, 1.0f);
					else {
						jours.add((int) i - 1, 0.5f);
						JTotale = JTotale - 0.5D;
					}
				} else
					jours.add(0.0f);

			}

			conges.setId_motif(Long.valueOf(motif));
			conges.setStatus(Long.getLong(statut));
			conges.setJours(jours);
			conges.setMois((long) mD - 1);
			conges.setAnnee(Long.parseLong(anneeD));
			conges.setId_user(id_user);
			conges.setTotal_jour(JTotale);
			conges.setPeriode(periode);
			conges.setUtilisateur(id_user2);
			listConges.add(conges);
		}
		
		// cas ou les annÃ©es& mois sont les memes mais ls jours diffÃ©rents.

		if (aD == aF && mD == mF && fD < fF) {

			Conges conges = new Conges();

			// Calcule du jour

			List<Float> jours = new ArrayList<Float>();

			for (int i = 1; i <= 31; i++) {
				if ((i >= fD) && (i <= fF))
					jours.add((int) i - 1, 1.0f);
				else
					jours.add(0.0f);

			}

			conges.setId_motif(Long.valueOf(motif));
			conges.setStatus(Long.getLong(statut));
			conges.setJours(jours);
			conges.setMois((long) mD - 1);
			conges.setAnnee(Long.parseLong(anneeD));
			conges.setId_user(id_user);
			conges.setTotal_jour(JTotale);
			conges.setPeriode(periode);
			conges.setUtilisateur(id_user2);
			listConges.add(conges);
		}

		// cas ou les mois sont diffÃ©rents

		else if (aD == aF && mD < mF) {

			List<Calendar> mycal = new ArrayList<Calendar>();
			for (int i = 1; i <= mF - mD; i++) {

				// Create a calendar object and set year and month
				Calendar mycalen = new GregorianCalendar(iaD, imD + i, ifD);
				mycal.add(mycalen);

			}

			// le jour du mois debut
			// Get the number of days in that month
			int daysInMonth = mycal.get(0).getActualMaximum(
					Calendar.DAY_OF_MONTH);

			List<Float> joursD = new ArrayList<Float>();

			List<Float> joursF = new ArrayList<Float>();
			for (int i = 1; i <= 31; i++) {
				if (i >= fD && (i <= daysInMonth))
					joursD.add((int) i - 1, 1.0f);
				else
					joursD.add(0.0f);

			}
			Conges congesD = new Conges();
			Conges congesF = new Conges();

			congesD.setId_motif(Long.valueOf(motif));
			congesD.setStatus(Long.getLong(statut));
			congesD.setJours(joursD);
			congesD.setMois((long) mD - 1);
			congesD.setAnnee(Long.parseLong(anneeD));
			congesD.setId_user(id_user);
			congesD.setTotal_jour((double) (daysInMonth - fD));
			congesD.setPeriode(periode);
			congesD.setUtilisateur(id_user2);
			listConges.add(congesD);

			// le jour du mois du milieu
			if (mF - mD > 1) {
				for (int k = 1; k < mF - mD; k++) {
					int daysInMonth1 = mycal.get(k).getActualMaximum(
							Calendar.DAY_OF_MONTH);
					List<Float> joursK = new ArrayList<Float>();
					Conges congesK = new Conges();

					for (int i = 1; i <= 31; i++) {
						if (i >= 1 && i <= daysInMonth1)
							joursK.add((int) i - 1, 1.0f);
						else
							joursK.add(0.0f);

					}

					congesK.setId_motif(Long.valueOf(motif));
					congesK.setStatus(Long.getLong(statut));
					congesK.setJours(joursK);
					// congesK.setMois((long) mycal.get(k).get(Calendar.MONTH));
					congesK.setMois((long) daysInMonth1 - 1);
					congesK.setAnnee(Long.parseLong(anneeD));
					congesK.setId_user(id_user);
					congesK.setTotal_jour((double) daysInMonth1);
					congesK.setPeriode(periode);
					congesK.setUtilisateur(id_user2);
					listConges.add(congesK);

				}
			}

			// // le jour du mois fin
			for (int i = 1; i <= 31; i++) {
				if (i >= 1 && i <= ifF)
					joursF.add((int) i - 1, 1.0f);
				else
					joursF.add(0.0f);

			}

			congesF.setId_motif(Long.valueOf(motif));
			congesF.setStatus(Long.getLong(statut));
			congesF.setJours(joursF);
			congesF.setMois((long) mF - 1);
			congesF.setAnnee(Long.parseLong(anneeD));
			congesF.setId_user(id_user);
			congesF.setTotal_jour((double) fF);
			congesF.setPeriode(periode);
			congesF.setUtilisateur(id_user2);
			listConges.add(congesF);
		}

		// cas ou les annÃ©es sont diffÃ©rents.

		else if (aD < aF) {
			List<Calendar> mycal1 = new ArrayList<Calendar>();
			List<Calendar> mycal2 = new ArrayList<Calendar>();

			for (int i = imD; i <= 12; i++) {
				Calendar mycalen1 = new GregorianCalendar(iaD, i - 1, 1);
				mycal1.add(mycalen1);
			}
			for (int j = 0; j <= imF; j++) {
				Calendar mycalen2 = new GregorianCalendar(iaF, j - 1, 1);
				mycal2.add(mycalen2);
			}

			// AnnÃ©e debut, les mois de l'annÃ©e debut

			for (int j = 1; j <= 12 - mD; j++) {
				int daysInMonth1 = mycal1.get(j).getActualMaximum(
						Calendar.DAY_OF_MONTH);

				List<Float> joursAD = new ArrayList<Float>();

				Conges congesAD = new Conges();

				for (int i = 1; i <= 31; i++) {
					if (i >= 1 && i <= daysInMonth1)
						joursAD.add((int) i - 1, 1.0f);
					else
						joursAD.add(0.0f);

				}

				congesAD.setId_motif(Long.valueOf(motif));
				congesAD.setStatus(Long.getLong(statut));
				congesAD.setJours(joursAD);
				// congesAD.setMois((long) mycal1.get(j).get(Calendar.MONTH)+1);
				congesAD.setMois((long) mycal1.get(j).get(Calendar.MONTH));
				congesAD.setAnnee(Long.parseLong(anneeD));
				congesAD.setId_user(id_user);
				congesAD.setTotal_jour((double) daysInMonth1);
				congesAD.setPeriode(periode);
				congesAD.setUtilisateur(id_user2);
				listConges.add(congesAD);
			}

			// AnnÃ©e fin, les mois de l'annÃ©e fin
			for (int jm = 1; jm <= mF - 1; jm++) {
				Conges congesAF = new Conges();

				int daysInMonth2 = mycal2.get(jm).getActualMaximum(
						Calendar.DAY_OF_MONTH);
				List<Float> joursAF = new ArrayList<Float>();

				for (int i = 1; i <= 31; i++) {
					if (i >= 1 && i <= daysInMonth2)
						joursAF.add((int) i - 1, 1.0f);
					else
						joursAF.add(0.0f);

				}
				congesAF.setId_motif(Long.valueOf(motif));
				congesAF.setStatus(Long.getLong(statut));
				congesAF.setJours(joursAF);
				// congesAF.setMois((long)
				// mycal2.get(jm).get(Calendar.MONTH)+1);
				congesAF.setMois((long) mycal2.get(jm).get(Calendar.MONTH));
				congesAF.setAnnee(Long.parseLong(annesF));
				congesAF.setId_user(id_user);
				congesAF.setTotal_jour((double) daysInMonth2);
				congesAF.setPeriode(periode);
				congesAF.setUtilisateur(id_user2);
				listConges.add(congesAF);
			}

			// AnnÃ©e debut, les jours du mois debut
			int daysInMonth3 = mycal1.get(0).getActualMaximum(
					Calendar.DAY_OF_MONTH);

			List<Float> joursAD1 = new ArrayList<Float>();

			Conges congesAD1 = new Conges();

			for (int i = 1; i <= 31; i++) {
				if (i >= ifD && i <= daysInMonth3)
					joursAD1.add((int) i - 1, 1.0f);
				else
					joursAD1.add(0.0f);
			}

			congesAD1.setId_motif(Long.valueOf(motif));
			congesAD1.setStatus(Long.getLong(statut));
			congesAD1.setJours(joursAD1);
			congesAD1.setMois((long) mD - 1);
			congesAD1.setAnnee(Long.parseLong(anneeD));
			congesAD1.setId_user(id_user);
			congesAD1.setTotal_jour((double) daysInMonth3 - fD + 1);
			congesAD1.setPeriode(periode);
			congesAD1.setUtilisateur(id_user2);
			listConges.add(congesAD1);

			List<Float> joursAD2 = new ArrayList<Float>();

			Conges congesAD2 = new Conges();

			for (int i = 1; i <= 31; i++) {
				if (i >= 1 && i <= ifF)
					joursAD2.add((int) i - 1, 1.0f);
				else
					joursAD2.add(0.0f);
			}

			congesAD2.setId_motif(Long.valueOf(motif));
			congesAD2.setStatus(Long.getLong(statut));
			congesAD2.setJours(joursAD2);
			congesAD2.setMois((long) mF - 1);
			congesAD2.setAnnee(Long.parseLong(annesF));
			congesAD2.setId_user(id_user);
			congesAD2.setTotal_jour((double) fF);
			congesAD2.setPeriode(periode);
			congesAD2.setUtilisateur(id_user2);
			listConges.add(congesAD2);
		}

		for (Conges cg : listConges) {
			congesDao.creer(cg);
		}

		// ContactMailForm form = new ContactMailForm(userDao, profilDao);
		// form.sendMail(request, utilisateur);
		// String to = request.getParameter("to");
		// String subject = request.getParameter("subject");
		// String body = request.getParameter("body");

		List<String> receiverEmailIDs = new ArrayList<String>();

		List<Utilisateur> utilisateurs = userDao.lister();
		for (Utilisateur user : utilisateurs) {
			if (user.isAdmin() || user.isRh()) {
				receiverEmailIDs.add(user.getLogin());
			}
		}
		String subject = "Demande de validation de congé";
		String body = "Bonjour, \n"
				+ "\nUne nouvelle demande de congé a été enregistrée pour :\n"
				+ (userDao.trouver(Long.valueOf(idSalarie))).getLogin()
				+ "\nVeuillez vous connecter à votre compte via le lien suivant :http://intranet.clinkast.fr:81/ess/"
				+ "\n et vous rendez à la rubrique liste des demandes des congés "
				+ "afin de valider cette demande de congé"
				+ "\n\nCordialement,\nClinkastTeam";

		new MailSender(receiverEmailIDs, subject, body);
		response.sendRedirect(request.getContextPath()
				+ "/gestionCongesDemande?statutAjout=true");

	}

}
