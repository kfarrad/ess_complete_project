package com.clinkast.cra.servlets;

import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;






import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;












import javax.servlet.http.Part;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.forms.UserProfilCreationForm;

// TODO: Auto-generated Javadoc
/**
 * The Class Inscription.
 */
@WebServlet( "/inscription" )
@MultipartConfig
public class Inscription extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    /** The Constant VUE_SUCCES. */
    public static final String VUE_SUCCES       = "/pages/protected/admin/userCreate.jsp";
    
    /** The Constant VUE_FORM. */
    public static final String VUE_FORM         = "/pages/public/inscription.jsp";

    /** The profil dao. */
    private UserProfilDaoImpl          profilDao;
    
    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.profilDao = ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserProfilDao();
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* À la réception d'une requête GET, simple affichage du formulaire */
        this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    //@SuppressWarnings("unchecked")
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /*
         * Lecture du paramètre 'chemin' passé à la servlet via la déclaration
         * dans le web.xml
         */
        //String chemin = this.getServletConfig().getInitParameter( CHEMIN );

        /* Préparation de l'objet formulaire */
        UserProfilCreationForm form = new UserProfilCreationForm( profilDao );
        
        /* Récupération de la carte vitale*/
        Part carteVitalePartFile = request.getPart("carteVitaleFile");
        Part cNIPartFile = request.getPart("CNIfile");
        Part photProfilPartFile = request.getPart("profilFile");

        /* Traitement de la requête et récupération du bean en résultant */
        UserProfil profil = form.creerProfil( request, 0L, carteVitalePartFile, cNIPartFile, photProfilPartFile );

        /* Ajout du bean et de l'objet métier à l'objet requête */
        request.setAttribute( Constants.ATT_PROFIL, profil );
        request.setAttribute( Constants.ATT_FORM, form );

        /* Si aucune erreur */
        if ( form.getErreurs().isEmpty() ) {
           /* //Alors récupération de la map des profils dans la session 
            HttpSession session = request.getSession();
            List< UserProfil> profils = (List< UserProfil>) session.getAttribute( Constants.ATT_SESSION_PROFILS );
            // Si aucune map n'existe, alors initialisation d'une nouvelle map 
            if ( profils == null ) {
                profils = new ArrayList<UserProfil>();
            }
            // Puis ajout du profil courant dans la map 
            profils.add( profil );
            
            //Et enfin (ré)enregistrement de la map en session 
            session.setAttribute( Constants.ATT_SESSION_PROFILS, profils );*/
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setLogin(profil.getEmail());
            utilisateur.setProfil(profil);
            utilisateur.setMotDePasse("");
            
            request.setAttribute( Constants.ATT_USER, utilisateur );
            /* Affichage de la fiche récapitulative */
            this.getServletContext().getRequestDispatcher( VUE_SUCCES ).forward( request, response );
        } else {
            /* Sinon, ré-affichage du formulaire de création avec les erreurs */
            this.getServletContext().getRequestDispatcher( VUE_FORM ).forward( request, response );
        }
    }

}
