package com.clinkast.cra.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOConfigurationException;
import com.clinkast.cra.dao.DAOFactory;
import com.clinkast.cra.dao.ProjetDaoImpl;
import com.clinkast.cra.dao.UserDaoImpl;

@MultipartConfig
public class BilanFinancier extends HttpServlet {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID 	= 1L;
	
    /** The Constant VUE. */
    public static final String VUE      		= "/pages/protected/admin/bilanFinancier.jsp";
    public static final String VUE_REDIRECT      		= "/pages/public/accessDenied.jsp";
    
    /** The calendar dao. */
    private CalendarDaoImpl        		    calendarDao;
    
    /** The projet dao. */
    private ProjetDaoImpl					projetDao;
    
    /** The utilisateur dao. */
    private UserDaoImpl		  			    utilisateurDao;
    
    /** The utilisateur. */
    private Utilisateur			            utilisateur;	
    
    /** The Constant FICHIER_PROPERTIES. */
    private static String FICHIER_PROPERTIES       = null;
    private static String fichierexcel       = null;
    /** The Constant PROPERTY_URL. */
    private static final String PROPERTY_BILAN             = "BILAN_EXCEL_EXPORT";
    
	public static final String PARAM_ANNEE = "annee";
	public static final String PARAM_MOIS = "mois";
	public static final String PARAM_ANNEE2 = "annee2";
	public static final String PARAM_MOIS2 = "mois2";
	public static final String id_User="Id_User";
	public static final String id_Projet="Id_Projet";
	public static final String name_User = "Name_User";
	public static final String name_Projet="Name_Projet";
	public static final String nb_Jours="Nb_Jours";
	public static final String CJM="CJM";
	public static final String TJM="TJM";
	public static final String Bilan="Bilan";
	public static final String bilan_General="Bilan G�n�ral";
	public static final int col_Bilan_General=0;
	private static int nb_Row = 0;
	// Ne pas retirer ID_USER,ID_PROJET,NB_JOURS,CJM,TJM ET BILAN //
	public static final String [] col={id_User,id_Projet,name_User,name_Projet,nb_Jours,CJM,TJM,"",Bilan};
	public static final char [] excel_Col={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	
    /* (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    public final void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.calendarDao 		= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getCalendarDao();
		this.projetDao 			= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getProjetDao();
		this.utilisateurDao 	= ( (DAOFactory) getServletContext().getAttribute( Constants.CONF_DAO_FACTORY ) ).getUserDao();
    }

    public final void doGet( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */
	  	HttpSession session = request.getSession();
	  	
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		if(utilisateur==null) 
			this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
		if(utilisateur.isUser())
			this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
		if(utilisateur.isAdmin()){
			request.setAttribute("projetDao" , projetDao );
			request.setAttribute("calendarDao", calendarDao);
			request.setAttribute("utilisateurDao", utilisateurDao );
    		}
		
		
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
			
			   
	  }
	  
    public final void doPost( final HttpServletRequest request, final HttpServletResponse response ) throws ServletException, IOException {
		/* À la réception d'une requête GET, simple affichage du formulaire */
		HttpSession session = request.getSession();
		String p_Annee = Constants.getValeurChamp(request, PARAM_ANNEE);
        String p_Mois = Constants.getValeurChamp(request, PARAM_MOIS);
        String p_Annee2 = Constants.getValeurChamp(request, PARAM_ANNEE2);
        String p_Mois2 = Constants.getValeurChamp(request, PARAM_MOIS2);
        nb_Row=0;
		int iYear= Constants.signedIntconv(p_Annee);
		int iMonth= Constants.signedIntconv(p_Mois);
		int iYear2= Constants.signedIntconv(p_Annee2);
		int iMonth2= Constants.signedIntconv(p_Mois2);

		Calendar ca = new GregorianCalendar();
		int mois = ca.get(Calendar.MONTH);
		int annee = ca.get(Calendar.YEAR);
		
		if(iYear==-1)
		{
			iYear=annee;
		}
		if(iMonth==-1)
		{
			iMonth=mois;
		}
		
		if(iYear==-1)
		{
			iYear2=annee;
		}
		if(iMonth==-1)
		{
			iMonth2=mois;
		}
		
		if(iMonth>iMonth2)
			if(iYear>=iYear2)
				iMonth=iMonth2;
		if(iYear>iYear2)
			iYear=iYear2;
		
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		if(utilisateur==null) 
			this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
		if(!utilisateur.isAdmin())
			this.getServletContext().getRequestDispatcher( VUE_REDIRECT ).forward( request, response );
		if(utilisateur.isAdmin()){
			request.setAttribute("projetDao" , projetDao );
			request.setAttribute("calendarDao", calendarDao);
			request.setAttribute("utilisateurDao", utilisateurDao );
			List<Utilisateur> liste_Utilisateurs = utilisateurDao.lister();
			
			if(request.getParameter("submit")!=null){
				if(request.getParameter("submit").equals("export")){
					HSSFWorkbook workbook = new HSSFWorkbook();
					HSSFSheet sheet = workbook.createSheet("Bilan Financier");
					HSSFFont font = workbook.createFont();
					font.setColor(HSSFColor.WHITE.index);
					font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

					HSSFRow row = sheet.createRow(nb_Row++);
					HSSFCell cell = null;
					for(int i=0;i<col.length;++i){
						cell = row.createCell(i);
						cell.setCellValue(col[i]);
					}
					
					HSSFCellStyle style_Row_Pair = workbook.createCellStyle();
					HSSFCellStyle style_Row_Impair = workbook.createCellStyle();
					HSSFCellStyle style_Bilan = workbook.createCellStyle();
					HSSFCellStyle style_Header_Bilan = workbook.createCellStyle();
					style_Row_Pair.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
					style_Row_Pair.setFillPattern(CellStyle.SOLID_FOREGROUND);
					style_Row_Impair.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					style_Row_Impair.setFillPattern(CellStyle.SOLID_FOREGROUND);
					style_Bilan.setFillForegroundColor(HSSFColor.GOLD.index);
					style_Bilan.setFillPattern(CellStyle.SOLID_FOREGROUND);
					style_Header_Bilan.setFillForegroundColor(HSSFColor.ORANGE.index);
					style_Header_Bilan.setFillPattern(CellStyle.SOLID_FOREGROUND);
					style_Header_Bilan.setFont(font);
					
					
					char col_Excel_CJM = 0,col_Excel_TJM=0,col_Excel_Nb_Jours=0,col_Bilan=0;
					for(int i=0;i<col.length;++i){
						if(col[i]==Bilan)
							col_Bilan=excel_Col[i];
						else if(col[i]==CJM)
							col_Excel_CJM=excel_Col[i];
						else if(col[i]==TJM)
							col_Excel_TJM=excel_Col[i];
						else if(col[i]==nb_Jours)
							col_Excel_Nb_Jours=excel_Col[i];
					}
					for(Utilisateur user : liste_Utilisateurs){
						// Si le parametre dont le nom est l'id de l'user existe on continue
						if(request.getParameter(user.getId().toString())!=null){
							Long id_User=user.getId();
							int nb_Projet = Integer.parseInt(request.getParameter(user.getId()+"_Number_Of_Projet"));
						
							for(int i = 0; i<nb_Projet;++i){
								row = sheet.createRow(nb_Row++);
								String number;
								//Construction d'une ligne pour un couple user projet
								for(int j = 0;j<col.length;++j) {
									cell= row.createCell(j);
									if(col[j]==BilanFinancier.id_User){
										cell.setCellValue(Double.parseDouble(request.getParameter(id_User.toString())));
									}	
									else if(col[j]==id_Projet) {
										cell.setCellValue(Double.parseDouble(request.getParameter(id_User+"_Id_Projet_"+i)));
									}
									else if(col[j]==name_User){
										cell.setCellValue(request.getParameter(id_User+"_Name"));
									}
									else if(col[j]==name_Projet){
										cell.setCellValue(request.getParameter(id_User+"_Nom_Projet_"+i));}
									else if(col[j]==nb_Jours){
										col_Excel_Nb_Jours=excel_Col[j];
										cell.setCellValue(Double.parseDouble(request.getParameter(id_User+"_Nb_Jours_Projet_"+i)));}
									else if(col[j]==CJM){
										number = request.getParameter("CJM_"+id_User+"_Projet_"+i);
										if(number.isEmpty())
											number="0.0";
										number = number.replace(',', '.');
										cell.setCellValue(Double.parseDouble(number));
										col_Excel_CJM=excel_Col[j];
										String s ="CJM_"+id_User+"_Id_Projet_"+request.getParameter(id_User+"_Id_Projet_"+i);
										request.setAttribute(s,Double.parseDouble(number));
										}
									else if(col[j]==TJM){
										number = request.getParameter("TJM_"+id_User+"_Projet_"+i);
										if(number.isEmpty())
											number="0.0";
										number = number.replace(',', '.');
										cell.setCellValue(Double.parseDouble(number));
										col_Excel_TJM=excel_Col[j];
										String s ="TJM_"+id_User+"_Id_Projet_"+request.getParameter(id_User+"_Id_Projet_"+i);
										request.setAttribute(s,Double.parseDouble(number));
									}
									else if(col[j]==Bilan){
										cell.setCellFormula("("+col_Excel_TJM+nb_Row+"-"+col_Excel_CJM+nb_Row+")*"+col_Excel_Nb_Jours+nb_Row);
										cell.setCellStyle(style_Bilan);
									}
								}
								color_Row_Pair_Odd(sheet,nb_Row-1,style_Row_Pair,style_Row_Impair);
								
							}
						}
					}
					
					//Bilan general
					create_Bilan_General(sheet,col_Bilan,style_Header_Bilan,style_Bilan);
					
					// AUTOSIZE
					for(int i = 0;i<col.length;++i){
						if(col[i]!="")
							sheet.autoSizeColumn(i);
					}
					
					HSSFCellStyle header = workbook.createCellStyle();
					header.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
					header.setFillPattern(CellStyle.SOLID_FOREGROUND);
					header.setFont(font);
					color_Header(sheet,0,header,style_Header_Bilan);
					
					//ECRITURE DU FICHIER
						Properties properties = new Properties();
						String path_Export;
						String file_Name="Bilan_"+iYear+"_"+(iMonth+1)+"-"+iYear2+"_"+(iMonth2+1)+".xls";
		                String chemin = System.getProperty("cra.confpath");
		                FICHIER_PROPERTIES = chemin  + File.separator + "cra.properties";
                       
                	 	InputStream fichierProperties = new FileInputStream(new File(FICHIER_PROPERTIES));
                        properties.load( fichierProperties );        
                        path_Export = properties.getProperty(PROPERTY_BILAN);
	                    

						try {
						fichierexcel=path_Export+File.separator+file_Name;
						
					    FileOutputStream out = 
					            new FileOutputStream(new File(fichierexcel));
					    workbook.write(out);
					    out.close();
					    System.out.println("Excel written successfully...");
						request.setAttribute("msg_Import", "Excel written successfully");

					   
					} catch (FileNotFoundException e) {
						request.setAttribute("msg_Import", "Erreur lors de l'�criture du fichier Excel, v�rifier qu'il n'est pas d�j� ouvert");
					    e.printStackTrace();

					} catch (IOException e) {
					    e.printStackTrace();
					}
					finally{  
						 workbook.close();
						 this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
					}
				}
				
				// IMPORT
				else if(request.getParameter("submit").equals("import")){
					if(request.getPart("file")!=null)
					{
						try {
							Part filePart=request.getPart("file");
							HSSFWorkbook workbook = new HSSFWorkbook(filePart.getInputStream());
							HSSFSheet sheet = workbook.getSheetAt(0);
							HSSFRow row; 
							HSSFCell cell = null;
							String CJM,TJM;
							Iterator rows = sheet.rowIterator();
							int col_Id_User = 0,col_Id_Projet=0,col_CJM=0,col_TJM=0;
							if(rows.hasNext()){
								row = (HSSFRow) rows.next();
								int i = 0;
								Iterator cellIterator = row.cellIterator();
								while(cellIterator.hasNext()) {
									cell = (HSSFCell) cellIterator.next();
									if(cell.getCellType()==HSSFCell.CELL_TYPE_STRING){
										String s = cell.getStringCellValue();
										if(s.equals(id_User)) col_Id_User=i;
										else if(s.equals(id_Projet)) col_Id_Projet=i;
										else if(s.equals(BilanFinancier.CJM)) col_CJM=i;
										else if(s.equals(BilanFinancier.TJM))col_TJM=i;
									}
									i++;
								}
							}
							while(rows.hasNext()){
								row=(HSSFRow) rows.next();
								int lastCell = row.getLastCellNum()-1;
								cell = row.getCell(lastCell);
								if(cell.getCellType()==HSSFCell.CELL_TYPE_STRING)
									if(cell.getStringCellValue().equals(bilan_General))
										break;
								CJM="CJM_";
								TJM="TJM_";
								cell = row.getCell(col_Id_User);
								if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
								{
									CJM+=(long)cell.getNumericCellValue()+"_Id_Projet_";
									TJM+=(long)cell.getNumericCellValue()+"_Id_Projet_";
								}
								cell = row.getCell(col_Id_Projet);
								if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
								{
									CJM+=(long)cell.getNumericCellValue();
									TJM+=(long)cell.getNumericCellValue();
								}
								cell = row.getCell(col_CJM);
								if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
								{
									request.setAttribute(CJM, cell.getNumericCellValue());
								}
								cell = row.getCell(col_TJM);
								if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
								{
									request.setAttribute(TJM, cell.getNumericCellValue());
								}
							}
							workbook.close();
							System.out.println("Excel import successful");
							request.setAttribute("msg_Import", "Excel import successful");
						}
						catch (Exception e) {
							request.setAttribute("msg_Import", "Erreur lors de l'importation du fichier Excel");
						    e.printStackTrace();
						}
						finally {
							this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
						}
							
					}
				}
			}
		}
	} 
    

	//COLOR LES CELLULES DE LA LIGNE CURRENTROW SAUF SI COL BILAN OU VIDE
    private void color_Row_Pair_Odd(HSSFSheet sheet,int currentRow,HSSFCellStyle Pair, HSSFCellStyle Impair){

		HSSFRow row=sheet.getRow(currentRow);
		for(int k = 0; k<col.length;++k){
			if(col[k]!=Bilan && col[k]!=""){
				HSSFCell cell=row.getCell(k);
				if(k%2==0) {
					cell.setCellStyle(Pair);
				}
				else cell.setCellStyle(Impair);
			}
		}
    }
    
	//COLOR LES CELLULES DE TITRES EN GENERAL LA 1ERE LIGNE
    private void color_Header(HSSFSheet sheet,int currentRow,HSSFCellStyle header, HSSFCellStyle header_Bilan){
    	HSSFRow row=sheet.getRow(currentRow);
		if (row != null) {
			for(int i=0;i<col.length;++i){
				if(col[i]!=Bilan && col[i]!=""){
				HSSFCell cell=row.getCell(i);
				cell.setCellStyle(header);
				}
				else if (col[i]==Bilan) {
				HSSFCell cell=row.getCell(i);
				cell.setCellStyle(header_Bilan);
				}
			}
		}
    }
    
    //CREATION DU BILAN GENERAL
    private void create_Bilan_General(HSSFSheet sheet,char col_Bilan,HSSFCellStyle style_Header, HSSFCellStyle style_Contents){
    	int last_Row=nb_Row;
		nb_Row++;
		//Creation du titre de la cellule bilan g�n�ral
		HSSFRow row = sheet.createRow(nb_Row++);
		HSSFCell cell=row.createCell(col_Bilan_General);
		cell.setCellValue(bilan_General);
		cell.setCellStyle(style_Header);
		//Creation de la cellule contenant la valeur du bilan g�n�ral
		row = sheet.createRow(nb_Row++);
		cell=row.createCell(col_Bilan_General);
		cell.setCellFormula("SUM("+col_Bilan+"2:"+col_Bilan+last_Row+")");
		cell.setCellStyle(style_Contents);
		}
    
}
		    


