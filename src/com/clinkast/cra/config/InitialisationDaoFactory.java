package com.clinkast.cra.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.clinkast.cra.dao.DAOFactory;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating InitialisationDao objects.
 *
 * @author Oliver
 */
public class InitialisationDaoFactory implements ServletContextListener {
    
    /** The Constant ATT_DAO_FACTORY. */
    private static final String ATT_DAO_FACTORY = "daofactory";

    /** The dao factory. */
    private DAOFactory          daoFactory;

    /* (non-Javadoc)
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized( ServletContextEvent event ) {
        /* Récupération du ServletContext lors du chargement de l'application */
        ServletContext servletContext = event.getServletContext();
        /* Instanciation de notre DAOFactory */
        this.daoFactory = DAOFactory.getInstance();
        /* Enregistrement dans un attribut ayant pour portée toute l'application */
        servletContext.setAttribute( ATT_DAO_FACTORY, this.daoFactory );
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed( ServletContextEvent event ) {
        /* Rien à réaliser lors de la fermeture de l'application... */
    	DAOFactory.shutdownConnPool();
    }
}