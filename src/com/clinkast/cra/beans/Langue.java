package com.clinkast.cra.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="langue")
public class Langue implements Serializable{

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The client id. */
        private Long   id;
        
        private Long   id_resume;
        
        private String nom;
        
        private String aisance;

        public String getNom() {
                return nom;
        }

        public void setNom(String nom) {
                this.nom = nom;
        }

        public String getAisance() {
                return aisance;
        }

        public void setAisance(String aisance) {
                this.aisance = aisance;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Long getId_resume() {
                return id_resume;
        }

        public void setId_resume(Long id_user) {
                this.id_resume = id_user;
        }
}
