package com.clinkast.cra.beans;

// Generated 18 sept. 2014 10:26:09 by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;


/**
 * Competences generated by hbm2java
 */
public class Competences implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long competencesId;
	private String competencesLibelle;
	private String competencesCategorie;
	private Set<Rapports> rapportses = new HashSet<Rapports>(0);

	public Competences() {
	}

	public Competences(String competencesLibelle, String competencesCategorie,
			Set<Rapports> rapportses) {
		this.competencesLibelle = competencesLibelle;
		this.competencesCategorie = competencesCategorie;
		this.rapportses = rapportses;
	}

	public Long getCompetencesId() {
		return this.competencesId;
	}

	public void setCompetencesId(Long competencesId) {
		this.competencesId = competencesId;
	}

	public String getCompetencesLibelle() {
		return this.competencesLibelle;
	}

	public void setCompetencesLibelle(String competencesLibelle) {
		this.competencesLibelle = competencesLibelle;
	}

	public String getCompetencesCategorie() {
		return this.competencesCategorie;
	}

	public void setCompetencesCategorie(String competencesCategorie) {
		this.competencesCategorie = competencesCategorie;
	}

	public Set<Rapports> getRapportses() {
		return this.rapportses;
	}

	public void setRapportses(Set<Rapports> rapportses) {
		this.rapportses = rapportses;
	}

}
