package com.clinkast.cra.beans;

import java.io.File;
import java.io.Serializable;

/**
 * This class defines informations about candidates
 *
 */
public class Candidat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The candidate id. */
	private Long id;

	/** the candidate first name */
	private String nom;

	/** the candidates last name */
	private String prenom;

	/** the candidates mail */
	private String mail;

	/** the candidates telephone number */
	private String telephone;

	/** the candidates degrees */
	private String diplomes;

	/** the candidate skills */
	private String specialites;

	/** the candidate resume name */
	private String pathCV;

	/** the candidate pertinence */
	private String pertinence;
	
	/** the candidate comments */
	private String commentaires;
	
	/** the candidate creation date */
	private String dateCreation;
	
	/** the candidate updated date */
	private String dateModification;

	public Candidat(String nom2, String prenom2, String telephone2, String mail2, String diplome, String specialite,
			String pathCV, String commentaire) {
		this.nom = nom2;
		this.prenom = prenom2;
		this.telephone = telephone2;
		this.mail = mail2;
		this.diplomes = diplome;
		this.specialites = specialite;
		this.pathCV = pathCV;
		this.commentaires = commentaire;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Candidat))
			return false;
		Candidat otherMyClass = (Candidat) other;
		return (otherMyClass.id == this.id);
	}

	public Candidat() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getDiplomes() {
		return diplomes;
	}

	public String getDiplomesHtml() {
		String res = "<ul>";
		String [] diplms = diplomes.split(";");
		for (int i = 0; i < diplms.length; i++) {
			res = res + "<li>" + diplms[i] + "</li>";
		}
		return res + "</ul>";
	}
	
	public String getSpecialitessHtml() {
		String res = "<ul>";
		String [] specls = specialites.split(";");
		for (int i = 0; i < specls.length; i++) {
			res = res + "<li>" + specls[i] + "</li>";
		}
		return res + "</ul>";
	}

	public void setDiplomes(String diplomes) {
		this.diplomes = diplomes;
	}

	public String getSpecilites() {
		return specialites;
	}

	public void setSpecilites(String specilites) {
		this.specialites = specilites;
	}

	public String getPathCV() {
		return pathCV;
	}

	public void setPathCV(String pathCV) {
		this.pathCV = pathCV;
	}

	public String getNomCV() {
		return pathCV.substring(pathCV.lastIndexOf(File.separator)+1, pathCV.length());
	}

	public void setPertinence(String pertinence) {
		this.pertinence = pertinence;
	}
	
	public String getPertinence() {
		return this.pertinence;
	}
	
	public void setCommentaires(String commentaire) {
		this.commentaires = commentaire;
	}
	
	public String getCommentaires() {
		return this.commentaires;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getDateModification() {
		return dateModification;
	}

	public void setDateModification(String dateModification) {
		this.dateModification = dateModification;
	}

}
