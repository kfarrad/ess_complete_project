package com.clinkast.cra.beans;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Calendrier for the planning management of a user
 */
public class Conges implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id of the calendar. */
	private Long   	id;
	
	/** The status of the calendar. */
	private Long    status;
	
	/** The id_projet of the calendar. */
	private Long   	id_motif;
	
	/** The id_user of the calendar. */
	private Long   	id_user;
	
	/** The year of the calendar. */
	private Long   	annee;

	private String utilisateur;
	
	private String typeConge;
	
	private boolean actif;	

	/** The month of the calendar. */
	private Long   	mois;
	
	/** The number of business days in a month . */
	private Double   	total_jour;
	
	/** The List of days in a month. */
	private List<Float> jours;
	

	private String periode;
	
	
	public String getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
	 public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getId_motif() {
		return id_motif;
	}

	public void setId_motif(Long id_motif) {
		this.id_motif = id_motif;
	}

	public Long getId_user() {
		return id_user;
	}

	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}

	public Long getAnnee() {
		return annee;
	}

	public void setAnnee(Long annee) {
		this.annee = annee;
	}

	public Long getMois() {
		return mois;
	}

	public void setMois(Long mois) {
		this.mois = mois;
	}

	public Double getTotal_jour() {
		return total_jour;
	}

	public void setTotal_jour(Double total_jour) {
		this.total_jour = total_jour;
	}

	public List<Float> getJours() {
		return jours;
	}

	public void setJours(List<Float> jours) {
		this.jours = jours;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* (non-Javadoc)
 	 * @see java.lang.Object#hashCode()
 	 */
	
	
 	@Override
	 public int hashCode() {
	  return id.intValue();
	 }

	public Conges() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Conges(Long id, Long status, Long id_motif, Long id_user, Long annee, Long mois, Double total_jour,
			List<Float> jours, String periode) {
		super();
		this.id = id;
		this.status = status;
		this.id_motif = id_motif;
		this.id_user = id_user;
		this.annee = annee;
		this.mois = mois;
		this.total_jour = total_jour;
		this.jours = jours;
		this.periode = periode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	 public boolean equals(Object obj) {
	  if (obj instanceof Calendrier) {
		  Calendrier calendrier = (Calendrier) obj;
	   return calendrier.getId() == id;
	  }

	  return false;
	 }
	

	@Override
	public String toString(){
		return "[calendrier-> Id : "+ this.id
				+ "id_projet: "+ this.id_motif;
			
		
	}

	public void setTypeConge(String string) {
		this.typeConge = string;
		
	}

	public String getTypeConge() {
		return typeConge;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}
}
