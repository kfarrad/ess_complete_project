package com.clinkast.cra.beans;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Utilisateur is the representation of the user.
 */
public class Utilisateur implements Serializable {
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/* Propriétés du bean */
	/** The user id. */
	private Long   	id;
	
	/** The user id_profil. */
	//private Long   	id_profil;
    

    private UserProfil profil;
    
    /** The user login. */
    private String 	login;
    
    /** The user password. */
    private String 	motDePasse;
    
    /** The user role. */
    private Role 	role;
    
    /** The user status. */
    private boolean actif;	

    
    /**
     * Checks if the user is admin.
     *
     * @return true, if the user is admin
     */
    public boolean isAdmin() {
    	return Role.ADMIN.equals(role);
    }

    /**
     * Checks if the user is user.
     *
     * @return true, if the user is user
     */
    public boolean isUser() {
    	return Role.USER.equals(role);
    }

    /**
     * Checks if the user is  rh.
     *
     * @return true, if the user is rh
     */
    public boolean isRh() {
    	return Role.RH.equals(role);
    }
    
    /**
     * Sets the user login.
     *
     * @param login the new user login
     */
    public void setLogin( String login ) {
        this.login = login;
    }

    /**
     * Gets the user login.
     *
     * @return the user login
     */
    public String getLogin() {
        return login;
    }

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the user id.
	 *
	 * @param id the new user id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	public UserProfil getProfil() {
            return profil;
    }

    public void setProfil(UserProfil profil) {
            this.profil = profil;
    }
	/**
	 * Gets the user password.
	 *
	 * @return the user password
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * Sets the user password.
	 *
	 * @param motDePasse the new user password
	 */
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	/**
	 * Gets the user role.
	 *
	 * @return the user role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Sets the user role.
	 *
	 * @param role the new user role
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	
	/**
	 * Checks if the user status is actif.
	 *
	 * @return true, if the user status is actif
	 */
	public boolean isActif() {
		return actif;
	}

	/**
	 * Sets the user statusactif.
	 *
	 * @param actif the new user status
	 */
	public void setActif(boolean actif) {
		this.actif = actif;
	}
	
	/* (non-Javadoc)
 	 * @see java.lang.Object#hashCode()
 	 */
 	@Override
	 public int hashCode() {
	  return id.intValue();
	 }

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	 public boolean equals(Object obj) {
	  if (obj instanceof Utilisateur) {
		  Utilisateur utilisateur = (Utilisateur) obj;
		  return utilisateur.getId() == id;
	  }

	  return false;
	 }
	        
	public String getName(){
	        return profil.getName();
	}
	public Long getId_profil() {
            return profil.getId();
    }
        
}