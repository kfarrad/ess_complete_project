package com.clinkast.cra.beans;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Client.
 */
public class Client implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The client id. */
	private Long   id;
    
    /** The client name. */
    private String nom;
    
    /** The client adress. */
    private String adresse;
    
	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the client id.
	 *
	 * @param id the new client id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the client name.
	 *
	 * @return the client name
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Sets the client name.
	 *
	 * @param nom the new client name
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Gets the client adress.
	 *
	 * @return the client adress
	 */
	public String getAdresse() {
		return adresse;
	}
	
	/**
	 * Sets the client adress.
	 *
	 * @param adresse the new client adress
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	@Override
    public String toString(){
        return "[client-> Id : "+ this.id
                + " Nom: "+ this.nom
                + " Adresse: "+ this.adresse;
            
        
    }
	
}
