package com.clinkast.cra.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants for all constants in the project.
 */
public class Constants {

	/** The Constant CONF_DAO_FACTORY. */

	public static final Properties PROPERTIES = new Properties();

	public static final String CONF_DAO_FACTORY = "daofactory";

	/** La suite du chemin des candidature (CV en extension PDF) */
	public final static String CANDIDATURE_PDF_FILES_PATH = "Candidature"
			+ File.separator + "PDF_Files" + File.separator;
	/** La suite du chemin des candidature (CV en extension DOCX) */
	public final static String CANDIDATURE_DOCX_FILES_PATH = "Candidature"
			+ File.separator + "DOCX_Files" + File.separator;
	/** La suite du chemin des candidature (CV en extension DOC) */
	public final static String CANDIDATURE_DOC_FILES_PATH = "Candidature"
			+ File.separator + "DOC_Files" + File.separator;

	/** La suite du chemin des candidature (CV en extension PDF) */
	public final static String CANDIDATURE_PDF_FILES_PATH_Windows = "Candidature"
			+ File.separator
			+ File.separator
			+ "PDF_Files"
			+ File.separator
			+ File.separator;
	/** La suite du chemin des candidature (CV en extension DOCX) */
	public final static String CANDIDATURE_DOCX_FILES_PATH_Windows = "Candidature"
			+ File.separator
			+ File.separator
			+ "DOCX_Files"
			+ File.separator
			+ File.separator;
	/** La suite du chemin des candidature (CV en extension DOCX) */
	public final static String CANDIDATURE_DOC_FILES_PATH_Windows = "Candidature"
			+ File.separator
			+ File.separator
			+ "DOC_Files"
			+ File.separator
			+ File.separator;

	public static final String ADMINISTRATION_FILES_PATH = "Administration"
			+ File.separator;

	private static final String PROPERTY_UPLOAD_PATH = "UPLOAD_PATH";
	private static final String PROPERTY_URL_APPLI = "URL_APPLI";
	private static final String PROPERTY_RECORDS_PER_PAGE = "RECORDS_PER_PAGE";

	/** The Constant ATT_FORM. */
	public static final String ATT_FORM = "form";

	/** The Constant ATT_SESSION_USER. */
	public static final String ATT_SESSION_USER = "sessionUser";

	/** The Constant ATT_SESSION_PROJETS. */
	public static final String ATT_SESSION_PROJETS = "sessionProjets";

	/** The Constant ATT_SESSION_CALENDRIER. */
	public static final String ATT_SESSION_CALENDRIER = "sessionCalendrier";

	/** The Constant ATT_SESSION_CLIENTS. */
	public static final String ATT_CLIENTS = "clients";

	public static final String ATT_SESSION_PROFILS = "profils";

	/** The Constant ATT_PROJETS. */
	public static final String ATT_PROJETS = "projets";

	/** The Constant ATT_CALENDRIER. */
	public static final String ATT_CALENDRIER = "calendriers";

	/** The Constant ATT_ANNEE. */
	public static final String ATT_ANNEE = "annee";

	/** The Constant ATT_MOIS. */
	public static final String ATT_MOIS = "mois";

	/** The Constant ATT_TOTAL. */
	public static final String ATT_TOTAL = "total";

	/** The Constant ATT_CLIENT. */
	public static final String ATT_CLIENT = "client";

	/** The Constant ATT_PROJET. */
	public static final String ATT_PROJET = "projet";

	/** The Constant ATT_ID_CLIENT. */
	public static final String ATT_ID_CLIENT = "idClient";

	/** The Constant ATT_PROJETS_USERS. */
	public static final String ATT_PROJETS_USERS = "projetUsers";

	/** The Constant ATT_USERS. */
	public static final String ATT_USERS = "utilisateurs";

	public static final String ATT_LISTE_USER = "listeUtilisateurs";

	/** The Constant ATT_NOM_PROJET. */
	public static final String ATT_NOM_PROJET = "nomProjet";

	/** The Constant PARAM_ID_PROFIL. */
	public static final String PARAM_ID_PROFIL = "idProfil";

	public static final String PARAM_LOGIN = "login";

	/** The Constant ATT_SESSION_NOM. */
	public static final String ATT_SESSION_NOM = "nom";

	/** The Constant ATT_ID. */
	public static final String ATT_ID = "id";

	/** The Constant ATT_USER. */
	public static final String ATT_USER = "user";

	public static final String ATT_PROFIL = "profil";

	public static final String ATT_SESSION_USERS = "users";

	public static final String PARAM_ID_PRINT = "idPrint";

	public static final String ATT_PROJE_USERS = "projetUsers";

	public static final String ATT_UTILISATEUR_CALENDRIER = "utilisateur_calendrier";

	public static final String ATT_FERIERS = "feriers";

	public static final String ATTR_SEPARATOR_DATE = " au ";

	public static final String PARAM_ID_RESUME = "idResume";

	public static final String ATT_LISTDATES = "listeDates";
	/** Constantes liste des auto-evaluation par utilisateur */
	public static final String ATT_GESTION_EVALUATION = "gestionEvaluations";

	public static final String ATT_AUTO_EVALUATION = "autoEvaluation";

	public static final String ATT_RAPPORTS_COMPETENCES = "rapports";
	public static final String ATT_MESSAGE = "message";

	// Champs correspondants au formulaire de l'ajout d'un CV
	/** Candidate first name attribute */
	public static final String ATT_NOM_CANDIDAT = "nomCandidat";
	/** Candidate last name attribute */
	public static final String ATT_PRENOM_CANDIDAT = "prenomCandidat";
	/** Candidate phone attribute */
	public static final String ATT_TELEPHONE_CANDIDAT = "telephoneCandidat";
	/** Candidate mail attribute */
	public static final String ATT_MAIL_CANDIDAT = "emailCandidat";
	/** Candidate degrees attribute */
	public static final String ATT_DIPLOME_CANDIDAT = "diplomeCandidat";
	/** Candidate skills attribute */
	public static final String ATT_SPECIALITE_CANDIDAT = "specialiteCandidat";
	/** attribute to get the degrees result */
	public static final String ATT_DIPLOMES_CANDIDAT = "diplomes";
	/** attribute to get the skills result */
	public static final String ATT_SPECIALITES_CANDIDAT = "specialites";
	/** Candidate resume attribute */
	public static final String ATT_CV_CANDIDAT = "CVCandidat";
	/** Candidate list attribute */
	public static final String ATT_CANDIDATS = "candidats";
	/** Candidate attribute */
	public static final String ATT_CANDIDAT = "candidat";
	/** Number of candidates to display per page */
	public static final int DEFAULT_RECORDS_PER_PAGE = 10;
	/** attribute to get the comment */
	public static final String ATT_COMMENT_CANDIDAT = "commentaireCandidat";
	/** Candidate first name attribute */
	public static final String ATT_TITRE_CONTENU = "titreContenu";
	/** Candidate first name attribute */
	public static final String ATT_CONTENU = "contenu";

	private static final String PROJET_NON_FACTURES = "PROJET_NON_FACTURES";
	
	public static final String LIEN_CRA = "http://intranet.clinkast.fr:81/ess/connexion";

	public static final String MAIL_ADMIN = "khaoulafarrad24@gmail.com";
	/**
	 * Gets the valeur champ.
	 *
	 * @param request
	 *            the request
	 * @param nomChamp
	 *            the name of the string parameter
	 * @return the value of a request parameter as a String, or null if the
	 *         parameter does not exist
	 */
	public static String getValeurChamp(HttpServletRequest request,
			String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}

	/**
	 * Fonction utilitaire permettant de retourner le nom du fichier à partir de
	 * l'objet Part passé en paramètre
	 * 
	 * @param Part
	 *            filePart
	 * @return String le nom du fichier
	 * */
	public static String getFileName(Part filePart) {
		if (filePart != null) {
			String header = filePart.getHeader("content-disposition");
			String fileName = null;
			String[] contentDispositionHeader = header.split(";");
			for (String name : contentDispositionHeader) {
				if ((name.trim().startsWith("filename"))) {
					String[] tmp = name.split("=");
					fileName = tmp[1].trim().replaceAll("\"", "");
				}
			}
			return fileName;
		}
		return "";
	}

	/**
	 * Fonction utilitaire permettant de supprimer les caratères spéciaux dans
	 * la chaine donnée en paramètre
	 * */
	public static String removeSpecChar(String champ) {
		if (champ != null)
			return champ.replaceAll("[^-_çéèàêâùûîôa-zA-Z0-9\\s]+", "");
		return "";

	}

	/**
	 * Signed intconv.
	 *
	 * @param inv
	 *            the string inv
	 * @return the int value contains int the string
	 */
	public static int signedIntconv(String inv) {
		int conv = -1;

		try {
			conv = Integer.parseInt(inv);
		} catch (Exception e) {
		}
		return conv;
	}

	static {
		String confPath = System.getProperty("cra.confpath");
		String FICHIER_PROPERTIES = confPath + File.separator
				+ "cra.properties";
		try {
			InputStream fichierProperties = new FileInputStream(new File(
					FICHIER_PROPERTIES));
			PROPERTIES.load(fichierProperties);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * gets the path of uploaded files depending on type argument
	 * 
	 * @return String: the full path from properties files
	 * */
	public static String getCheminUpload() throws FileNotFoundException,
			IOException {
		return PROPERTIES.getProperty(PROPERTY_UPLOAD_PATH) + File.separator;
	}

	public static String getUrlAppli() throws FileNotFoundException,
			IOException {
		return PROPERTIES.getProperty(PROPERTY_URL_APPLI);
	}

	public static List<Integer> getIdProjetsNonFactures()
			throws FileNotFoundException, IOException {
		List<Integer> listId = new ArrayList<Integer>();
		String[] list = PROPERTIES.getProperty(PROJET_NON_FACTURES).split(",");
		for (int i = 0; i < list.length; i++) {
			listId.add(Integer.valueOf(list[i].trim()));
		}

		return listId;
	}

	public static int getRecordsPerPage() throws FileNotFoundException,
			IOException {
		return Integer.valueOf(PROPERTIES
				.getProperty(PROPERTY_RECORDS_PER_PAGE));
	}

	public static int getFridaysTaken(List<Float> jours, int mois, int annee) {
		int fridays = 0;
		if (jours != null) {
			// Lire le jour de la semaine pour savoir si cest un wekend
			for (int i = 0; i < jours.size(); i++) {
				if (jours.get(i) == 1) {
					String date = (i + 1) + "/" + (mois + 1) + "/" + annee;
					DateTimeFormatter formatter = DateTimeFormat
							.forPattern("dd/MM/yyyy");
					DateTime dt = formatter.parseDateTime(date);
					// System.out.println("la date est : " + dt.toString());
					Integer day = dt.getDayOfWeek();
					// System.out.println("le jour :" + day.toString());
					// if(day == DateTimeConstants.SATURDAY || day ==
					// DateTimeConstants.SUNDAY)
					// Si c'est un jour ferrié, ou un weekend, incremente le
					// compteur
					if (day == DateTimeConstants.FRIDAY) {
						fridays++;
					}
				}
			}
		}
		return fridays;

	}

}
