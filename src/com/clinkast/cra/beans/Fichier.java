package com.clinkast.cra.beans;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Fichier.
 */
public class Fichier implements Serializable{

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The file id. */
        private Long    id;

        /** The description. */
        private String description;
        
        /** The nom. */
        private String nom;

        /**
         * Gets the description.
         *
         * @return the description
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the description.
         *
         * @param description the new description
         */
        public void setDescription( String description ) {
            this.description = description;
        }

        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return nom;
        }

        /**
         * Sets the nom.
         *
         * @param nom the new nom
         */
        public void setNom( String nom ) {
            this.nom = nom;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }
    }