package com.clinkast.cra.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Contenu  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;

	private String titre;

	private String contenu;
	
	private Timestamp dateCreation;
	
	public Timestamp getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Timestamp dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Contenu() {
		this.titre = "";
		this.contenu = "";
	}
	
	public Contenu(String titre, String contenu) {
		this.titre = titre;
		this.contenu = contenu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	
	public String dateFormat() {
		SimpleDateFormat format = new SimpleDateFormat("EEE d MMM 'à' HH:mm:ss ");
		return format.format(dateCreation);
//		return d[0] + " à  " + d[1];
	}

}
