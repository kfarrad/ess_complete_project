
package com.clinkast.cra.beans;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class RappelGeneral is the representation of the rappel_general.
 */
public class RappelGeneral implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/* Propriétés du bean */
	/** The  id. */
	private int   	id;

	/** The user status. */
	private int status;
	
	public int getId() {
		return id;
	}

	public int getStatus() {
		return status;
	}
	public void setId(int id) {
		this.id = id;
	}

	public void setStatus(int status) {
		this.status = status;
	}


}
