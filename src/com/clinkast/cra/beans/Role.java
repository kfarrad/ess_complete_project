package com.clinkast.cra.beans;

// TODO: Auto-generated Javadoc
/**
 * The Enum Role.
 */
public enum Role {
        
        /** The admin. */
        ADMIN, 
        /** The user. */
        USER, 
        /** The rh. */
        RH;
}
