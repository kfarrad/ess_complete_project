package com.clinkast.cra.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="competence")
public class Competence implements Serializable{

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The client id. */
        private Long   id;
        
        private Long   id_resume;
        
        private String nom;
        
        private String Liste;
        
        private Double annees_experience;

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getNom() {
                return nom;
        }

        public void setNom(String nom) {
                this.nom = nom;
        }

        public String getListe() {
                return Liste;
        }

        public void setListe(String liste) {
                Liste = liste;
        }

        public Double getAnnees_experience() {
                return annees_experience;
        }

        public void setAnnees_experience(Double annees_experience) {
                this.annees_experience = annees_experience;
        }

        public Long getId_resume() {
                return id_resume;
        }

        public void setId_resume(Long id_resume) {
                this.id_resume = id_resume;
        }
        
}
