package com.clinkast.cra.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class Resume implements Serializable{

        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        private Long id;
        
        private Long id_user;
        
        private TitreCV titre;
        
        private Map<Long,Experience> experiences;
        
        private Map<Long,Formation> formations;
        
        private Map<Long,Competence> competences;
        
        private Map<Long,Langue> langues;
        
        private Date date;

        private String pdf;
 
        public TitreCV getTitre() {
                return titre;
        }

        public void setTitre(TitreCV titre) {
                this.titre = titre;
        }

        public Map<Long,Experience> getExperiences() {
                return experiences;
        }

        public void setExperiences(Map<Long,Experience> experiences) {
                this.experiences = experiences;
        }

        public Map<Long,Formation> getFormations() {
                return formations;
        }

        public void setFormations(Map<Long,Formation> formations) {
                this.formations = formations;
        }

        public Map<Long,Competence> getCompetences() {
                return competences;
        }

        public void setCompetences(Map<Long,Competence> competences) {
                this.competences = competences;
        }

        public Map<Long,Langue> getLangues() {
                return langues;
        }

        public void setLangues(Map<Long,Langue> langues) {
                this.langues = langues;
        }

        public Long getId_user() {
                return id_user;
        }

        public void setId_user(Long id_user) {
                this.id_user = id_user;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public Date getDate() {
                return date;
        }

        public void setDate(Date date) {
                this.date = date;
        }

        public String getPdf() {
                return pdf;
        }

        public void setPdf(String pdf) {
                this.pdf = pdf;
        }

        
}
