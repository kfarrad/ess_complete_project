package com.clinkast.cra.beans;

import java.io.Serializable;
import java.sql.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class UserProfil is the representation of the user profil .
 */
public class UserProfil implements Serializable, Comparable <UserProfil>{
        
        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The user profil id. */
        private Long   id;
        
        /** The user first name. */
        private String nom;
        
        /** The user last name. */
        private String prenom;
        
        /** The user adresse. */
        private String adresse;
        
        /** The user email. */
        private String email;
        
        /** The user telephone. */
        private String telephone;
        
        /** The user date of birth. */
        private Date dob;
        
        /** Numéro de sécurité sociale. */
        private String numero_ssn;
        
        /** Carte vitale. */
        private String carte_vitale;
        
        /** Carte d'identité. */
        private String cni;
        
        /** Carte d'identité. */
        private String photoProfil;
        
        
        /**
         * Gets the user profil id.
         *
         * @return the user profil id
         */
        public Long getId() {
                return id;
        }
        
        /**
         * Sets the user profil id.
         *
         * @param id the new user profil id
         */
        public void setId(Long id) {
                this.id = id;
        }
        
        /**
         * Gets the user first name.
         *
         * @return the user first name
         */
        public String getNom() {
                return nom;
        }
        
        /**
         * Sets the user first name.
         *
         * @param nom the new user first name
         */
        public void setNom(String nom) {
                this.nom = nom;
        }
        
        /**
         * Gets the user last name.
         *
         * @return the user last name
         */
        public String getPrenom() {
                return prenom;
        }
        
        /**
         * Sets the user last name.
         *
         * @param prenom the new user last name
         */
        public void setPrenom(String prenom) {
                this.prenom = prenom;
        }
        
        /**
         * Gets the user telephone.
         *
         * @return the user telephone
         */
        public String getTelephone() {
                return telephone;
        }
        
        /**
         * Sets the user telephone.
         *
         * @param telephone the new user telephone
         */
        public void setTelephone(String telephone) {
                this.telephone = telephone;
        }
        
        /**
         * Gets the user adress.
         *
         * @return the user adress
         */
        public String getAdresse() {
                return adresse;
        }
        
        /**
         * Sets the user adress.
         *
         * @param client the new user adress
         */
        public void setAdresse(String client) {
                this.adresse = client;
        }
        
        /**
         * Gets the user email.
         *
         * @return the  user email
         */
        public String getEmail() {
                return email;
        }
        
        /**
         * Sets the user email.
         *
         * @param email the new user email
         */
        public void setEmail(String email) {
                this.email = email;
        }
        
        public Date getDob() {
			return dob;
		}

		public void setDob(Date date) {
			this.dob = date;
		}

		public String getNumero_ssn() {
			return numero_ssn;
		}

		public void setNumero_ssn(String numero_ssn) {
			this.numero_ssn = numero_ssn;
		}

		public String getCarte_vitale() {
			return carte_vitale;
		}

		public void setCarte_vitale(String carte_vitale) {
			this.carte_vitale = carte_vitale;
		}

		public String getPhotoProfil() {
			return photoProfil;
		}

		public void setPhotoProfil(String photoProfil) {
			this.photoProfil = photoProfil;
		}

		public String getCni() {
			return cni;
		}

		public void setCni(String cni) {
			this.cni = cni;
		}
        
        /**
         * Gets the user full name.
         *
         * @return the user full name
         */
        public String getName(){
                if(this.getPrenom()!=null)	return this.getPrenom() + " " +this.getNom() ;
                return this.getNom();
        }
        
        
        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
                return id.intValue();
        }
        
        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
                if (obj instanceof UserProfil) {
                        UserProfil profil = (UserProfil) obj;
                        return profil.getId() == id;
                }
                
                return false;
        }
        
        public String getInitiales() {
                // TODO Auto-generated method stub
                if(this.getPrenom()!=null)  return this.getPrenom().charAt(0) + "." +this.getNom().charAt(0) ;
                return this.nom.charAt(0)+"..";
        }
        
        public boolean isBirthDay() {
        	java.util.Date d = new java.util.Date();
        	if (dob != null) {
        		return dob.getMonth() == d.getMonth();
        	}
        	return false;
        }

		@Override
		public int compareTo(UserProfil arg0) {
			// TODO Auto-generated method stub
			return this.nom.compareTo(arg0.nom);
		}
}
