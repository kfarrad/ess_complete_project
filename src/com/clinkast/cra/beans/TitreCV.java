package com.clinkast.cra.beans;

import java.io.Serializable;

public class TitreCV implements Serializable {
        
        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        /* Propriétés du bean */
        /** The user id. */
        private Long    id;
        
        private Long id_resume;
        
        private String initiales;
        
        private String nom;
        
        private String sous_titre;

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        /**
         * @return the nom
         */
        public String getNom() {
                return nom;
        }

        /**
         * @param nom the nom to set
         */
        public void setNom(String nom) {
                this.nom = nom;
        }

        /**
         * @return the sous_titre
         */
        public String getSous_titre() {
                return sous_titre;
        }

        /**
         * @param sous_titre the sous_titre to set
         */
        public void setSous_titre(String sous_titre) {
                this.sous_titre = sous_titre;
        }

        /**
         * @return the id_resume
         */
        public Long getId_resume() {
                return id_resume;
        }

        /**
         * @param id_resume the id_resume to set
         */
        public void setId_resume(Long id_resume) {
                this.id_resume = id_resume;
        }

        /**
         * @return the initiales
         */
        public String getInitiales() {
                return initiales;
        }

        /**
         * @param initiales the initiales to set
         */
        public void setInitiales(String initiales) {
                this.initiales = initiales;
        }
        
}
