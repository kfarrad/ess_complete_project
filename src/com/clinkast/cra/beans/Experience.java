package com.clinkast.cra.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class Experience.
 */

@XmlRootElement(name="experience")
public class Experience implements Serializable{

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The project id. */
        private Long    id;
        
        private Long    id_resume;
        
        private String Entreprise;
        
        private String titre_poste;
        
        private String date_debut;
        
        private String date_fin;
        
        private String description;

        /**
         * @return the entreprise
         */
        public String getEntreprise() {
                return Entreprise;
        }

        /**
         * @param entreprise the entreprise to set
         */
        public void setEntreprise(String entreprise) {
                Entreprise = entreprise;
        }

        /**
         * @return the id
         */
        public Long getId() {
                return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Long id) {
                this.id = id;
        }

        /**
         * @return the titre_poste
         */
        public String getTitre_poste() {
                return titre_poste;
        }

        /**
         * @param titre_poste the titre_poste to set
         */
        public void setTitre_poste(String titre_poste) {
                this.titre_poste = titre_poste;
        }

        /**
         * @return the date_debut
         */
        public String getDate_debut() {
                return date_debut;
        }

        /**
         * @param date_debut the date_debut to set
         */
        public void setDate_debut(String date_debut) {
                this.date_debut = date_debut;
        }

        /**
         * @return the date_fin
         */
        public String getDate_fin() {
                return date_fin;
        }

        /**
         * @param date_fin the date_fin to set
         */
        public void setDate_fin(String date_fin) {
                this.date_fin = date_fin;
        }

        /**
         * @return the description
         */
        public String getDescription() {
                return description;
        }

        /**
         * @param description the description to set
         */
        public void setDescription(String description) {
                this.description = description;
        }

        /**
         * @return the id_resume
         */
        public Long getId_resume() {
                return id_resume;
        }

        /**
         * @param id_resume the id_resume to set
         */
        public void setId_resume(Long id_resume) {
                this.id_resume = id_resume;
        }
        
}
