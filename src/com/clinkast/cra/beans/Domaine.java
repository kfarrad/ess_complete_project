package com.clinkast.cra.beans;

import java.io.Serializable;

public class Domaine implements Serializable{

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The client id. */
        private Long   id;
        
        /** The client name. */
        private String nom;

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getNom() {
                return nom;
        }

        public void setNom(String nom) {
                this.nom = nom;
        }
        
}
