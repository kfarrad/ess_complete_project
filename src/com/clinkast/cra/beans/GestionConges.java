package com.clinkast.cra.beans;

import org.joda.time.DateTime;

/**
 * 
 * @author test
 * GestionConges
 */
public class GestionConges {
	
	public GestionConges(Long id, Utilisateur user, Projet projet,
			int rTTInitial, int rTTAttente, int cPInitial, int cPAttente,
			DateTime dateValidation, String historique, int isValide) {
		this.id = id;
		this.user = user;
		this.projet = projet;
		this.rTTInitial = rTTInitial;
		this.rTTAttente = rTTAttente;
		this.cPInitial = cPInitial;
		this.cPAttente = cPAttente;
		this.dateValidation = dateValidation;
		this.historique = historique;
		this.isValide = isValide;
	}
	public GestionConges() {
	}
	private Long id; //Identifiant
	private Utilisateur user;//Identifiant de l'utilisteur
	private Projet projet;//Libellé du congès (RTT ou CP) 
	private float rTTInitial;//Nombre de jour restant validé par la direction
	private float rTTAttente;//Nombre de jour restant en attendant la validation
	private float cPInitial;//Nombre de jour restant validé par la direction
	private float cPAttente;//Nombre de jour restant en attendant la validation
	private DateTime dateValidation;//Date à la quelle la direction initialise la variable jInitial
	private String historique;//historiqe de toutes les modification,
	private int isValide;
	
	private double rttAcquis;
	private double cpAcquis;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Utilisateur getUser() {
		return user;
	}
	public void setUser(Utilisateur user) {
		this.user = user;
	}
	public Projet getProjet() {
		return projet;
	}
	public void setProjet(Projet projet) {
		this.projet = projet;
	}
	public float getrTTInitial() {
		return rTTInitial;
	}
	public void setrTTInitial(float rTTInitial) {
		this.rTTInitial = rTTInitial;
	}
	public float getrTTAttente() {
		return rTTAttente;
	}
	public void setrTTAttente(float rTTAttente) {
		this.rTTAttente = rTTAttente;
	}
	public float getcPInitial() {
		return cPInitial;
	}
	public void setcPInitial(float cPInitial) {
		this.cPInitial = cPInitial;
	}
	public float getcPAttente() {
		return cPAttente;
	}
	public void setcPAttente(float total_cp) {
		this.cPAttente = total_cp;
	}
	public DateTime getDateValidation() {
		return dateValidation;
	}
	public void setDateValidation(DateTime dateValidation) {
		this.dateValidation = dateValidation;
	}
	public String getHistorique() {
		return historique;
	}
	public void setHistorique(String historique) {
		this.historique = historique;
	}
	public int getValide() {
		return isValide;
	}
	public void setValide(int isValide) {
		this.isValide = isValide;
	}
	public double getRttAcquis() {
		return rttAcquis;
	}
	public void setRttAcquis(double rttAcquis) {
		this.rttAcquis = rttAcquis;
	}
	public double getCpAcquis() {
		return cpAcquis;
	}
	public void setCpAcquis(double cpAcquis) {
		this.cpAcquis = cpAcquis;
	}
}
