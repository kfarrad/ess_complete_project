package com.clinkast.cra.beans;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Projet.
 */
public class Projet implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The project id. */
	private Long   	id;
	
	/** The project id_client. */
	private Long   	id_client;
	
//	private Client client;
    
    /** The project name. */
    private String 	nom;
	
	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the project id.
	 *
	 * @param id the new project id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the project id_client.
	 *
	 * @return the project id_client
	 */
	public Long getId_client() {
		return id_client;
	}
	
	/**
	 * Sets the id_client.
	 *
	 * @param id_client the new id_client
	 */
	public void setId_client(Long id_client) {
		this.id_client = id_client;
	}
	
	/**
	 * Gets the project name.
	 *
	 * @return the project name
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Sets the project name.
	 *
	 * @param nom the new project name
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Gets the project name.
	 *
	 * @param id the project id
	 * @return the project name if it does exist, or null if it doesn't.
	 */
	public String getNomProjet(Long id) {
		if(this.id==id) return nom;
		return null;
	}
	
	
	 /* (non-Javadoc)
 	 * @see java.lang.Object#hashCode()
 	 */
 	@Override
	 public int hashCode() {
	  return id.intValue();
	 }

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	 public boolean equals(Object obj) {
	  if (obj instanceof Projet) {
	   Projet projet = (Projet) obj;
	   return projet.getId() == id;
	  }

	  return false;
	 }
	
	@Override
	public String toString(){
		return "[projet-> Id : "+ this.id
				+ " Nom: "+ this.nom;
			
		
	}
	 
}
