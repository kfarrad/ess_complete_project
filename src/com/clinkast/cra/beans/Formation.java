package com.clinkast.cra.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="formation")
public class Formation implements Serializable{

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        
        /** The client id. */
        private Long   id;
        
        private Long id_resume;
        
        private String domaine;
        
        private Long annee;
        
        private String specialisation;
        
        private String institut;
        
        private String moyenne;
        
        private String date_debut;
        
        private String date_fin;

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

      
        public Long getAnnee() {
                return annee;
        }

        public void setAnnee(Long annee) {
                this.annee = annee;
        }

        public String getSpecialisation() {
                return specialisation;
        }

        public void setSpecialisation(String specialisation) {
                this.specialisation = specialisation;
        }

        public String getInstitut() {
                return institut;
        }

        public void setInstitut(String institut) {
                this.institut = institut;
        }

        public String getMoyenne() {
                return moyenne;
        }

        public void setMoyenne(String moyenne) {
                this.moyenne = moyenne;
        }

        public String getDate_debut() {
                return date_debut;
        }

        public void setDate_debut(String date_debut) {
                this.date_debut = date_debut;
        }

        public String getDate_fin() {
                return date_fin;
        }

        public void setDate_fin(String date_fin) {
                this.date_fin = date_fin;
        }

        public Long getId_resume() {
                return id_resume;
        }

        public void setId_resume(Long id_user) {
                this.id_resume = id_user;
        }
        
        

        @Override
        public String toString(){
            return "[formation->\nId : "+ this.id
                    + "\nid_user: "+ this.id_resume
                    + "\ndomaine: "+ this.domaine
                    + "\ninstitut: "+ this.institut
                    + "\nspecialisation: "+ this.specialisation
                    + "\nmoyenne: " +this.moyenne
                    + "\ndate_debut: " +this.date_debut
                    + "\ndate_fin: " +this.date_fin;                
            
        }

        public String getDomaine() {
                return domaine;
        }

        public void setDomaine(String domaine) {
                this.domaine = domaine;
        }
}
