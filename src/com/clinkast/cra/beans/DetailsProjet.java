package com.clinkast.cra.beans;

public class DetailsProjet {

	private String nom;
	private int debut;
	private int fin;
	private double duree;


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDebut() {
		return debut;
	}

	public void setDebut(int debut) {
		this.debut = debut;
	}

	public int getFin() {
		return fin;
	}

	public void setFin(int fin) {
		this.fin = fin;
	}

	public double getDuree() {
		return duree;
	}

	public void setDuree(double duree) {
		this.duree = duree;
	}

	@Override public boolean equals(Object other){
		if(this == other) return true;
		if(other == null || (this.getClass() != other.getClass())){
			return false;
		}
		DetailsProjet project = (DetailsProjet) other;
		return (this.nom!= null && nom.equals(project.nom));
	}

	@Override public int hashCode(){
		int result = 0;
		result = 31*result + (this.nom !=null ? this.nom.hashCode() : 0);
		return result;
	}

}
