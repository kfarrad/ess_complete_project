package com.clinkast.cra.beans;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Calendrier for the planning management of a user
 */
public class Calendrier implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id of the calendar. */
	private Long   	id;
	
	/** The status of the calendar. */
	private Long    status;
	
	/** The id_projet of the calendar. */
	private Long   	id_projet;
	
	/** The id_user of the calendar. */
	private Long   	id_user;
	
	/** The year of the calendar. */
	private Long   	annee;
	
	/** The month of the calendar. */
	private Long   	mois;
	
	/** The number of business days in a month . */
	private Double   	total_jour;
	
	/** The List of days in a month. */
	private List<Float> jours;

	/**
	 * Gets the Calendar id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the calendar id.
	 *
	 * @param id the new calendar id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the calendar id_projet.
	 *
	 * @return the calendar id_projet
	 */
	public Long getId_projet() {
		return id_projet;
	}
	
	/**
	 * Sets the calendar id_projet.
	 *
	 * @param id_projet the new calendar id_projet
	 */
	public void setId_projet(Long id_projet) {
		this.id_projet = id_projet;
	}
	
	/**
	 * Gets the calendar id_user.
	 *
	 * @return the calendar id_user
	 */
	public Long getId_user() {
		return id_user;
	}
	
	/**
	 * Sets the calendar id_user.
	 *
	 * @param id_user the new calendar id_user
	 */
	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}
	
	/**
	 * Gets the calendar year.
	 *
	 * @return the calendar year
	 */
	public Long getAnnee() {
		return annee;
	}
	
	/**
	 * Sets the calendar year.
	 *
	 * @param annee the new calendar year
	 */
	public void setAnnee(Long annee) {
		this.annee = annee;
	}
	
	/**
	 * Gets the calendar month.
	 *
	 * @return the calendar month
	 */
	public Long getMois() {
		return mois;
	}
	
	/**
	 * Sets the calendar month.
	 *
	 * @param mois the new calendar month
	 */
	public void setMois(Long mois) {
		this.mois = mois;
	}
	
	/**
	 * Gets the number of business days.
	 *
	 * @return the number of business days
	 */
	public Double getTotal_jour() {
		return total_jour;
	}
	
	/**
	 * Sets the number of business days.
	 *
	 * @param total_jour the number of business days
	 */
	public void setTotal_jour(Double total_jour) {
		this.total_jour = total_jour;
	}
	
	/**
	 * Gets the List of day in a month.
	 *
	 * @return the the List of day in a month
	 */
	public List<Float> getJours() {
		return jours;
	}
	
	/**
	 * Sets the List of day in a month.
	 *
	 * @param jours the the List of day in a month
	 */
	public void setJours(List<Float> jours) {
		this.jours = jours;
	}
	
	/**
	 * Gets the calendar status.
	 *
	 * @return the calendar status
	 */
	public Long getStatus() {
		return status;
	}
	
	/**
	 * Sets the calendar status.
	 *
	 * @param status the new calendar status
	 */
	public void setStatus(Long status) {
		this.status = status;
	}
	
	 /* (non-Javadoc)
 	 * @see java.lang.Object#hashCode()
 	 */
 	@Override
	 public int hashCode() {
	  return id.intValue();
	 }

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	 public boolean equals(Object obj) {
	  if (obj instanceof Calendrier) {
		  Calendrier calendrier = (Calendrier) obj;
	   return calendrier.getId() == id;
	  }

	  return false;
	 }
	

	@Override
	public String toString(){
		return "[calendrier-> Id : "+ this.id
				+ "id_projet: "+ this.id_projet;
			
		
	}
}
