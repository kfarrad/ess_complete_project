package com.clinkast.cra.mails;


import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

// TODO: Auto-generated Javadoc
/**
 * The Class MailSender.
 */
public class MailSender
{

	/** The sender email id. */
	private final String senderEmailID = "cra@clinkast.fr";

	/** The sender password. */
	private final String senderPassword = "craclinkas";

	/** The email smt pserver. */
	private final String emailSMTPserver = "smtp.gmail.com";

	/** The email server port. */
	private final String emailServerPort = "465";

	/** The receiver email i ds. */
	List<String> receiverEmailIDs = null;

	/** The email subject. */
	String emailSubject = null;

	/** The email body. */
	String emailBody = null;

	/**
	 * Instantiates a new mail sender.
	 *
	 * @param receiverEmailIDs the receiver email i ds
	 * @param emailSubject the email subject
	 * @param emailBody the email body
	 */
	public MailSender(List<String> receiverEmailIDs, String emailSubject, String emailBody)
	{
		this.receiverEmailIDs=receiverEmailIDs;
		this.emailSubject=emailSubject;
		this.emailBody=emailBody;


		Properties props = new Properties();
		props.put("mail.smtp.user",senderEmailID);
		props.put("mail.smtp.host", emailSMTPserver);
		props.put("mail.smtp.port", emailServerPort);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.socketFactory.port", emailServerPort);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");

		//SecurityManager security = System.getSecurityManager();

		try
		{
			Authenticator auth = new SMTPAuthenticator();
			Session session = Session.getInstance(props, auth);
			// session.setDebug(true);

			MimeMessage msg = new MimeMessage(session);
			msg.setText(emailBody);
			msg.setSubject(emailSubject);
			msg.setFrom(new InternetAddress(senderEmailID));
			InternetAddress[] receivers  = new InternetAddress[receiverEmailIDs.size()];
			for (int i = 0; i < receivers.length; i++) {
				receivers[i] = new InternetAddress(receiverEmailIDs.get(i));
			}
			msg.setRecipients(Message.RecipientType.TO, receivers);
			//msg.addRecipient(Message.RecipientType.TO,new InternetAddress(receiverEmailID));
			Transport.send(msg);
		}
		catch (Exception mex)
		{
			mex.printStackTrace();
		}


	}
	
	public MailSender(List<String> receiverEmailIDs, String emailSubject, String emailBody,String path,String filename) throws IOException
	{
		this.receiverEmailIDs=receiverEmailIDs;
		this.emailSubject=emailSubject;
		this.emailBody=emailBody;


		Properties props = new Properties();
		props.put("mail.smtp.user",senderEmailID);
		props.put("mail.smtp.host", emailSMTPserver);
		props.put("mail.smtp.port", emailServerPort);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.socketFactory.port", emailServerPort);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");

		//SecurityManager security = System.getSecurityManager();

		try
		{
			Authenticator auth = new SMTPAuthenticator();
			Session session = Session.getInstance(props, auth);
			// session.setDebug(true);

			MimeMessage msg = new MimeMessage(session);
			//msg.setText(emailBody);
			msg.setSubject(emailSubject);
			msg.setFrom(new InternetAddress(senderEmailID));
			InternetAddress[] receivers  = new InternetAddress[receiverEmailIDs.size()];
			for (int i = 0; i < receivers.length; i++) {
				receivers[i] = new InternetAddress(receiverEmailIDs.get(i));
			}
			msg.setRecipients(Message.RecipientType.TO, receivers);
			//msg.addRecipient(Message.RecipientType.TO,new InternetAddress(receiverEmailID));
			
			// creates message part
	        MimeBodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setText(emailBody);
	 
	        // creates multi-part
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	 
	        // adds attachments
			
				
	        MimeBodyPart attachementPart = new MimeBodyPart();
	        FileDataSource source = new FileDataSource(path);
	        attachementPart.setDataHandler(new DataHandler(source));
	        attachementPart.setFileName(filename);
	        multipart.addBodyPart(attachementPart);
	    	
	        // sets the multi-part as e-mail's content
	        msg.setContent(multipart);	
			
			
			Transport.send(msg);
			Files.delete(Paths.get(path));
			
		}
		catch (Exception mex)
		{

			mex.printStackTrace();
			throw new IOException(mex);
		}


	}

	/**
	 * The Class SMTPAuthenticator.
	 */
	public class SMTPAuthenticator extends javax.mail.Authenticator
	{

		/* (non-Javadoc)
		 * @see javax.mail.Authenticator#getPasswordAuthentication()
		 */
		public PasswordAuthentication getPasswordAuthentication()
		{
			return new PasswordAuthentication(senderEmailID, senderPassword);
		}
	}
	
	
}
