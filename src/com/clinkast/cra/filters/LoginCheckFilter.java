package com.clinkast.cra.filters;

import java.io.IOException;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;

// TODO: Auto-generated Javadoc
/**
 * Servlet Filter implementation class UserCheckFilter.
 */
/**
 * @author Oliver
 *
 */
public class LoginCheckFilter extends AbstractFilter implements Filter {
    
    /** The allowed ur is. */
    private static List<String> allowedURIs;
    
    /**
     * Inits the.
     *
     * @param fConfig the f config
     * @throws ServletException the servlet exception
     * @see Filter#init(FilterConfig)
     */
    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
	if(allowedURIs == null){
	    allowedURIs = new ArrayList<String>();
	    allowedURIs.add(fConfig.getInitParameter("loginActionURI"));
	    allowedURIs.add("/pages/protected/defaultUser/acceuil_utilisateur.jsp");   
	    allowedURIs.add("/resources/css/style.css");
	    allowedURIs.add("/resources/css/jquery.tablesorter.css");
	    allowedURIs.add("/resources/script/script.js");
	    allowedURIs.add("/resources/javascript/jquerry.js");
	    allowedURIs.add("/resources/images/access-denied.png");
	    allowedURIs.add("/resources/images/activation_false.png");
	    allowedURIs.add("/resources/images/activation.png");
	    allowedURIs.add("/resources/images/modification.png");
	    allowedURIs.add("/resources/images/supprimer.png");
	    allowedURIs.add("/connexion");
	    allowedURIs.add("/viewResume");
	    allowedURIs.add("/viewAutoEvaluation");
	    allowedURIs.add("/");
	}
    }
    
    /**
     * Destroy.
     *
     * @see Filter#destroy()
     */
    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {
    }
    
    /**
     * Do filter.
     *
     * @param request the request
     * @param response the response
     * @param chain the chain
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	HttpServletRequest req = (HttpServletRequest) request;
	HttpSession session = req.getSession();
	List<String> allowedURIsFinal = new ArrayList<String>();
	for(String uri:allowedURIs ){         
	    allowedURIsFinal.add(req.getContextPath()+uri);
	}
	

	
	String chemin = req.getRequestURI().substring( req.getContextPath().length() );
	if ( chemin.startsWith( "/resources" ) || allowedURIsFinal.contains(req.getRequestURI()) ) {	    
	    chain.doFilter( request, response );
	    return;
	}
	
	Utilisateur utilisateur = (Utilisateur) session.getAttribute(Constants.ATT_SESSION_USER);
		
	if (utilisateur == null && !req.getRequestURI().contains("connexion")) {
	    doLogin(request, response, req);
	    return;
	}
	
	if (!session.isNew() && utilisateur == null && !allowedURIsFinal.contains(req.getRequestURI()) && !req.getRequestURI().contains("connexion")) {
	    doLogin(request, response, req);
	    return;
	}
	chain.doFilter(request, response);
    }
}