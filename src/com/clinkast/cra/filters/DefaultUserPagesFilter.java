package com.clinkast.cra.filters;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultUserPagesFilter.
 *
 * @author Oliver
 */
public class DefaultUserPagesFilter extends AbstractFilter implements Filter {
	
 /* (non-Javadoc)
 * @see javax.servlet.Filter#destroy()
 */
@Override
 public void destroy() {

 }

 /* (non-Javadoc)
 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
 */
@Override
 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
  HttpServletRequest req = (HttpServletRequest) request;
  Utilisateur utilisateur = (Utilisateur) req.getSession(true).getAttribute(Constants.ATT_SESSION_USER);

  if(!utilisateur.isUser() && !utilisateur.isAdmin() && !utilisateur.isRh()){
   accessDenied(request, response, req);
   return;
  }
 
  String chemin = req.getRequestURI().substring( req.getContextPath().length() );
  if ( chemin.startsWith( "/resources" ) ) {
      chain.doFilter( request, response );
      return;
  }
  
  chain.doFilter(request, response);
 }

 /* (non-Javadoc)
 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
 */
@Override
 public void init(FilterConfig arg0) throws ServletException {

 }
}
