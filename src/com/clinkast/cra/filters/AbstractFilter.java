package com.clinkast.cra.filters;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;


// TODO: Auto-generated Javadoc
/**
 * The Class AbstractFilter.
 *
 * @author Oliver
 */
public class AbstractFilter {

 /**
  * Instantiates a new abstract filter.
  */
public AbstractFilter() {
  super();
 }

 /**
  * Do login.
  *
  * @param request the request
  * @param response the response
  * @param req the req
  * @throws ServletException the servlet exception
  * @throws IOException Signals that an I/O exception has occurred.
  */
protected void doLogin(ServletRequest request, ServletResponse response, HttpServletRequest req) throws ServletException, IOException {
  RequestDispatcher rd = req.getRequestDispatcher("/pages/public/connexion.jsp");
  rd.forward(request, response);
 }

 /**
  * Access denied.
  *
  * @param request the request
  * @param response the response
  * @param req the req
  * @throws ServletException the servlet exception
  * @throws IOException Signals that an I/O exception has occurred.
  */
protected void accessDenied(ServletRequest request, ServletResponse response, HttpServletRequest req) throws ServletException, IOException {
  RequestDispatcher rd = req.getRequestDispatcher("/pages/public/accessDenied.jsp");
  rd.forward(request, response);
 }
}