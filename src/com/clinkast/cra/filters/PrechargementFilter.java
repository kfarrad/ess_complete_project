package com.clinkast.cra.filters;

import java.io.IOException;
/*import java.util.HashMap;
import java.util.List;
import java.util.Map;*/

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
// TODO: Auto-generated Javadoc
//import javax.servlet.http.HttpSession;


//import com.clinkast.cra.beans.UserProfil;
//import com.clinkast.cra.dao.UserProfilDao;
//import com.clinkast.cra.dao.DAOFactory;

/**
 * The Class PrechargementFilter.
 *
 * @author Oliver
 */
public class PrechargementFilter implements Filter {
    
    /** The Constant CONF_DAO_FACTORY. */
    public static final String CONF_DAO_FACTORY      = "daofactory";
    
    /** The Constant ATT_SESSION_PROFILS. */
    public static final String ATT_SESSION_PROFILS   = "profils";
    

   // private UserProfilDao          clientDao;


    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init( FilterConfig config ) throws ServletException {
        /* Récupération d'une instance de nos DAO Client et Commande */
        //this.clientDao = ( (DAOFactory) config.getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getUserProfilDao();
       
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter( ServletRequest req, ServletResponse res, FilterChain chain ) throws IOException,
            ServletException {
        /* Cast de l'objet request */
        HttpServletRequest request = (HttpServletRequest) req;        
        System.out.println("test 1er");
        /* Pour terminer, poursuite de la requête en cours */
        chain.doFilter( request, res );
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {
    }
}