package com.clinkast.cra.forms;

import java.util.HashMap;
import java.util.Map;

public class AbstractForm {

	/** The resultat. */
	protected String              resultat;

	/** The erreurs. */
	protected Map<String, String> erreurs; 

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	AbstractForm(){
		this.erreurs = new HashMap<String, String>();
	}
	public void setErreur( String champ, String message ) {
		if(erreurs==null) erreurs = new HashMap<String, String>();
		erreurs.put( champ, message );
	}
}
