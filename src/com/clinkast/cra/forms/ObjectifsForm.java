package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Objectifs;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.ObjectifsDAOImpl;

public class ObjectifsForm extends AbstractForm{
	/*	Constantes */
	//public static final String PARAM_ID = "id";
	public static final String CHAMP_ID = "objectifs[id_auto]";
	public static final String CHAMP_OBJ_INTITULE = "objectif[name]";
	public static final String CHAMP_OBJ_ESTIMATION = "objectif[estimation]";
	public static final String CHAMP_OBJ_ID = "objectifs[id_objectif]";
	public static final String CHAMP_TAB_OBJ_ID = "objectifs_id_objectif";
	
	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private ObjectifsDAOImpl objectifsDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public ObjectifsForm(GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl, AutoEvaluationDAOImpl autoEvaluationDAOImpl, ObjectifsDAOImpl objectifsDAOImpl){
		super();
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
		this.objectifsDAOImpl = objectifsDAOImpl;
	}

	public GestionEvaluations sauveObjectif(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String obj_intitule = Constants.getValeurChamp(request, CHAMP_OBJ_INTITULE);
		String obj_estimation = Constants.getValeurChamp(request, CHAMP_OBJ_ESTIMATION);
		String obj_id = Constants.getValeurChamp(request, CHAMP_OBJ_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;
		
		Objectifs objectifs = new Objectifs();
		
		//Convertir les id en Long
		Long evaluationId = Long.parseLong(autoEvaluationId);
		Long objectifs_id = Long.parseLong(obj_id);
		objectifs.setAutoEvaluations(autoEvaluationDAOImpl.trouver(evaluationId));
		objectifs.setObjectifsLibelle(obj_intitule);
		objectifs.setObjectifsEstimation(obj_estimation);
				
		try{
			//Si l'id n'existe pas alors on cree l'element
			if(objectifs_id == 0){
				//nouvelle activité
				objectifsDAOImpl.creer(objectifs);
				resultat = "L' objectif a été créé";
			}
			else if(objectifs_id != null && objectifs_id instanceof Long){
				//on ajoute id
				objectifs.setObjectifsId(objectifs_id);
				//on modifie les attributs de l'activité
				objectifsDAOImpl.modifier(objectifs);
				resultat = "L' objectif a été modifié";
			}
			gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
			
			
		}
		catch (DAOException e) {
			// TODO: handle exception
			setErreur("imprevu dans objectifsDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}		
		return gestionEvaluations;
	}
	
	/*Supprimer un objectif*/
	public GestionEvaluations supprimeObjectif(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String[] deleteId = (String[])request.getParameterValues( CHAMP_TAB_OBJ_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;		
		Long evaluationId = Long.parseLong(autoEvaluationId);
		int compteur = 0;
		
		if(deleteId != null)
		{
			try{
				for(String id : deleteId){
					Long obj_id = Long.parseLong(id);
					Objectifs objectifs = objectifsDAOImpl.trouver(obj_id);
					objectifsDAOImpl.supprimer(objectifs);
					compteur ++;
				}
				gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
				
				resultat = compteur + (compteur >= 2 ? " objectifs ont " : " objectif a ") + "été supprimé";
			}
			catch (DAOException e) {
				// TODO: handle exception
				setErreur("imprevu dans ObjectifsDAOImpl: ", e.getMessage());
				resultat = e.getMessage();
			}
		}
		return gestionEvaluations;
	}
	
	
	private GestionEvaluations updateGestionAutoEvaluation(HttpSession session, Long evaluationId){

		GestionEvaluations gestionEvaluations = null;
		Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		
		//Mis à jour de gestionAutoEvaluation
		try{		
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(evaluationId, utilisateur.getId());
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime());
			gestionEvaluations.setGestionEvaluationsNbreModification(gestionEvaluations.getGestionEvaluationsNbreModification()+1);
			gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
		}
		catch (Exception e) {
			// TODO: handle exception
			setErreur("imprevu dans GestionEvaluationDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}
		return gestionEvaluations;
	}


}
