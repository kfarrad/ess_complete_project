package com.clinkast.cra.forms;

/*
 import java.io.BufferedInputStream;
 import java.io.BufferedOutputStream;
 import java.io.File;
 import java.io.FileOutputStream;
 import java.io.IOException;
 import java.io.InputStream;
 */
//import java.util.Collection;
//import javax.servlet.ServletException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.Part;

import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.FileUtils;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.UserProfilDaoImpl;

// TODO: Auto-generated Javadoc
//import eu.medsea.mimeutil.MimeUtil;

/**
 * The Class UserProfilCreationForm.
 */
public final class UserProfilCreationForm extends AbstractForm {

	private static final Set<String> EXTENSIONS = new HashSet<String>(
			Arrays.asList(new String[] { "pdf", "doc", "docx", "gif", "png",
					"jpg", "jpeg" }));

	/** The Constant CHAMP_NOM. */
	private static final String CHAMP_NOM = "nomProfil";

	/** The Constant CHAMP_PRENOM. */
	private static final String CHAMP_PRENOM = "prenomProfil";

	/** The Constant CHAMP_Adresse. */
	private static final String CHAMP_Adresse = "adresseProfil";

	/** The Constant CHAMP_TELEPHONE. */
	private static final String CHAMP_TELEPHONE = "telephoneProfil";

	/** The Constant CHAMP_EMAIL. */
	private static final String CHAMP_EMAIL = "emailProfil";

	/** The Constant CHAMP_EMAIL. */
	private static final String CHAMP_DOB = "dobProfil";

	/** The Constant CHAMP_EMAIL. */
	private static final String CHAMP_NUMEROSSN = "numero_snnProfil";

	/** The Constant CHAMP_EMAIL. */
	private static final String CHAMP_CARTE_VITALE = "carte_vitaleProfil";

	/** The Constant CHAMP_EMAIL. */
	private static final String CHAMP_CNI = "cniProfil";

	/** The profil dao. */
	private UserProfilDaoImpl profilDao;

	private String nom = "";

	private String prenom = "";

	/**
	 * Instantiates a new user profil creation form.
	 *
	 * @param profilDao
	 *            the profil dao
	 */
	public UserProfilCreationForm(UserProfilDaoImpl profilDao) {
		super();
		this.profilDao = profilDao;
	}

	/**
	 * Modifier user profil.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @return the user profil
	 */
	public UserProfil modifierUserProfil(HttpServletRequest request, long id,
			Part carteVitalePart, Part cniPart, Part photoPart,
			String fullPathCarteVitale, String fullPathCNI, String fullPathPhoto) {
		nom = Constants.getValeurChamp(request, CHAMP_NOM);
		prenom = Constants.getValeurChamp(request, CHAMP_PRENOM);
		String adresse = Constants.getValeurChamp(request, CHAMP_Adresse);
		String telephone = Constants.getValeurChamp(request, CHAMP_TELEPHONE);
		String date_naissance = Constants.getValeurChamp(request, CHAMP_DOB);
		String numero_ssn = Constants.getValeurChamp(request, CHAMP_NUMEROSSN);
		String carte_vitale = Constants.getFileName(carteVitalePart);
		String cni = Constants.getFileName(cniPart);
		String photo = Constants.getFileName(photoPart);
		// String email = getValeurChamp( request, CHAMP_EMAIL );

		UserProfil profil = new UserProfil();
		UserProfil profilModifier = new UserProfil();

		traiterNom(nom, profil);
		traiterPrenom(prenom, profil);
		traiterAdresse(adresse, profil);
		traiterTelephone(telephone, profil);
		traiterDateNaissance(date_naissance, profil);
		traiterNumeroSNN(numero_ssn, profil);
		traiterCarteVitale(carte_vitale, profil);
		traiterCNI(cni, profil);
		traiterPhoto(photo, profil);
		String chemin = "";

		// traiterEmail( email, profil );
		try {
			if (erreurs.isEmpty()) {
				System.out.println("FileName(carteVitalePart): "
						+ Constants.getFileName(carteVitalePart));
				if (!Constants.getFileName(carteVitalePart).isEmpty()) {
					System.out.println("fullPathCarteVitale: "
							+ fullPathCarteVitale);
					if (!fullPathCarteVitale.isEmpty()) {
						try {
							// Suppression d'ancien CV en cas de modification
							chemin = Constants.getCheminUpload();
							File file = new File(chemin + fullPathCarteVitale);
							file.delete();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					validationImportCarteVitale(carteVitalePart);

					profil.setCarte_vitale(Constants.ADMINISTRATION_FILES_PATH
							+ nom.trim() + '_' + prenom.trim() + File.separator
							+ Constants.getFileName(carteVitalePart));

				} else {
					profil.setCarte_vitale(fullPathCarteVitale);
				}

				System.out.println("FileName (cniPart): "
						+ Constants.getFileName(cniPart));
				if (!Constants.getFileName(cniPart).isEmpty()) {
					System.out.println("fullPathCNI: " + fullPathCNI);
					if (!fullPathCNI.isEmpty()) {
						try {
							// Suppression d'ancien CV en cas de modification
							chemin = Constants.getCheminUpload();
							File file = new File(chemin + fullPathCNI);
							file.delete();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					validationImportCNI(cniPart);

					profil.setCni(Constants.ADMINISTRATION_FILES_PATH
							+ nom.trim() + '_' + prenom.trim() + File.separator
							+ Constants.getFileName(cniPart));

				} else {
					profil.setCni(fullPathCNI);
				}

				System.out.println("FileName (photoPart): "
						+ Constants.getFileName(photoPart));
				if (!Constants.getFileName(photoPart).isEmpty()) {
					System.out.println("fullPathPhoto: " + fullPathPhoto);
					if (!fullPathPhoto.isEmpty()) {
						try {
							// Suppression d'ancien CV en cas de modification
							chemin = Constants.getCheminUpload();
							File file = new File(chemin + fullPathPhoto);
							file.delete();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					validationImportPhoto(photoPart);

					profil.setPhotoProfil(Constants.ADMINISTRATION_FILES_PATH
							+ nom.trim() + '_' + prenom.trim() + File.separator
							+ Constants.getFileName(photoPart));

				} else {
					profil.setPhotoProfil(fullPathPhoto);
				}

				// if (!Constants.getFileName(cniPart).isEmpty()) {
				// if (fullPathCNI == null || fullPathCNI.isEmpty()) {
				// validationImportCNI(cniPart);
				// } else {
				// try {
				// // Suppression d'ancien CV en cas de modification
				// FileUtils.deleteDirectory(new File(Constants
				// .getCheminUpload()
				// + Constants.ADMINISTRATION_FILES_PATH
				// + nom.trim()
				// + '_'
				// + prenom.trim()
				// + File.separator));
				// } catch (FileNotFoundException e) {
				// e.printStackTrace();
				// } catch (IOException e) {
				// e.printStackTrace();
				// }
				// validationImportCNI(cniPart);
				// }
				//
				// } else {
				// profil.setCni(fullPathCNI);
				// }
				//
				// if (!Constants.getFileName(photoPart).isEmpty()) {
				// if (fullPathPhoto == null || fullPathPhoto.isEmpty()) {
				// validationImportPhoto(photoPart);
				// } else {
				// try {
				// // Suppression d'ancien CV en cas de modification
				// FileUtils.deleteDirectory(new File(Constants
				// .getCheminUpload()
				// + Constants.ADMINISTRATION_FILES_PATH
				// + nom.trim()
				// + '_'
				// + prenom.trim()
				// + File.separator));
				// } catch (FileNotFoundException e) {
				// e.printStackTrace();
				// } catch (IOException e) {
				// e.printStackTrace();
				// }
				// validationImportPhoto(photoPart);
				// }
				//
				// } else {
				// profil.setPhotoProfil(fullPathPhoto);
				// }

				profilDao.modifier(profil, id);
				profilModifier = profilDao.trouver(id);
				resultat = "Succès de la modification du profil.";
			}
		} catch (DAOException e) {
			setErreur("imprévu", "Erreur imprévue lors de la modification.");
			resultat = "Échec de la modification du profil : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}

		return profilModifier;
	}

	/**
	 * Creer profil.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @return the user profil
	 */
	public UserProfil creerProfil(HttpServletRequest request, Long id,
			Part carteVitalePart, Part cniPart, Part photoPart) {
		nom = Constants.getValeurChamp(request, CHAMP_NOM);
		prenom = Constants.getValeurChamp(request, CHAMP_PRENOM);
		String adresse = Constants.getValeurChamp(request, CHAMP_Adresse);
		String telephone = Constants.getValeurChamp(request, CHAMP_TELEPHONE);
		String email = Constants.getValeurChamp(request, CHAMP_EMAIL);
		String date_naissance = Constants.getValeurChamp(request, CHAMP_DOB);
		String numero_ssn = Constants.getValeurChamp(request, CHAMP_NUMEROSSN);
		String carte_vitale = Constants.getFileName(carteVitalePart);
		String cni = Constants.getFileName(cniPart);
		String photo = Constants.getFileName(photoPart);

		UserProfil profil = new UserProfil();

		traiterNom(nom, profil);
		traiterPrenom(prenom, profil);
		traiterAdresse(adresse, profil);
		traiterTelephone(telephone, profil);
		traiterEmail(email, profil);
		traiterDateNaissance(date_naissance, profil);
		traiterNumeroSNN(numero_ssn, profil);
		traiterCarteVitale(carte_vitale, profil);
		traiterCNI(cni, profil);
		traiterPhoto(photo, profil);

		if (carteVitalePart != null
				&& !Constants.getFileName(carteVitalePart).isEmpty())
			validationImportCarteVitale(carteVitalePart);
		if (cniPart != null && !Constants.getFileName(cniPart).isEmpty())
			validationImportCNI(cniPart);
		if (photoPart != null && !Constants.getFileName(photoPart).isEmpty())
			validationImportPhoto(photoPart);

		try {
			if (erreurs.isEmpty()) {
				profilDao.creer(profil);
				resultat = "Succès de la création du profil.";
			} else {
				// TODO traitement d'echec de l'ajout alors que le fichier a été
				// importer
				// File file = new File(chemin +
				// Constants.ADMINISTRATION_FILES_PATH + nom + prenom +
				// File.separator);
				// String[]entries = file.list();
				// for(String s: entries){
				// File currentFile = new File(file.getPath(),s);
				// currentFile.delete();
				// }
				// file.delete();
				try {
					FileUtils.deleteDirectory(new File(Constants
							.getCheminUpload()
							+ Constants.ADMINISTRATION_FILES_PATH
							+ nom.trim()
							+ '_' + prenom.trim() + File.separator));
					resultat = "Échec de la création du profil.";
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (DAOException e) {
			setErreur("imprévu", "Erreur imprévue lors de la création.");
			resultat = "Échec de la création du profil : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
			try {
				FileUtils.deleteDirectory(new File(Constants.getCheminUpload()
						+ Constants.ADMINISTRATION_FILES_PATH + nom.trim()
						+ '_' + prenom.trim() + File.separator));
			} catch (FileNotFoundException e1) {
			} catch (IOException e1) {
			}
		}

		return profil;
	}

	private void traiterCNI(String cni, UserProfil profil) {
		String fullPath = "";
		fullPath = validationCni(cni);
		profil.setCni(fullPath);
	}

	private String validationCni(String cni) {
		if (cni != null && !cni.isEmpty())
			return Constants.ADMINISTRATION_FILES_PATH + nom.trim() + '_'
					+ prenom.trim() + File.separator + cni;
		return "";
	}

	private void traiterPhoto(String photo, UserProfil profil) {
		String fullPath = "";
		fullPath = validationPhoto(photo);
		profil.setPhotoProfil(fullPath);
	}

	private String validationPhoto(String photo) {
		if (photo != null && !photo.isEmpty())
			return Constants.ADMINISTRATION_FILES_PATH + nom.trim() + '_'
					+ prenom.trim() + File.separator + photo;
		return "";
	}

	private void traiterCarteVitale(String carte_vitale, UserProfil profil) {
		String fullPath = validationCarteVitale(carte_vitale);
		profil.setCarte_vitale(fullPath);

	}

	private String validationCarteVitale(String carte_vitale) {
		if (carte_vitale != null && !carte_vitale.isEmpty())
			return Constants.ADMINISTRATION_FILES_PATH + nom.trim() + '_'
					+ prenom.trim() + File.separator + carte_vitale;
		return "";

	}

	private void traiterNumeroSNN(String numero_ssn, UserProfil profil) {
		profil.setNumero_ssn(Constants.removeSpecChar(numero_ssn));
	}

	private void traiterDateNaissance(String date_naissance, UserProfil profil) {
		profil.setDob(validationDateNaissance(date_naissance));
	}

	private java.sql.Date validationDateNaissance(String date_naissance) {
		return getSqlFormatDateFrom(date_naissance);
	}

	/**
	 * Traiter nom.
	 *
	 * @param nom
	 *            the nom
	 * @param profil
	 *            the profil
	 */
	private void traiterNom(String nom, UserProfil profil) {
		try {
			validationNom(nom);
		} catch (FormValidationException e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		profil.setNom(nom);
	}

	/**
	 * Traiter prenom.
	 *
	 * @param prenom
	 *            the prenom
	 * @param profil
	 *            the profil
	 */
	private void traiterPrenom(String prenom, UserProfil profil) {
		try {
			validationPrenom(prenom);
		} catch (FormValidationException e) {
			setErreur(CHAMP_PRENOM, e.getMessage());
		}
		profil.setPrenom(prenom);
	}

	/**
	 * Traiter adresse.
	 *
	 * @param adresse
	 *            the adresse
	 * @param profil
	 *            the profil
	 */
	private void traiterAdresse(String adresse, UserProfil profil) {
		try {
			validationAdresse(adresse);
		} catch (FormValidationException e) {
			setErreur(CHAMP_Adresse, e.getMessage());
		}
		profil.setAdresse(adresse);
	}

	/**
	 * Traiter telephone.
	 *
	 * @param telephone
	 *            the telephone
	 * @param profil
	 *            the profil
	 */
	private void traiterTelephone(String telephone, UserProfil profil) {
		try {
			validationTelephone(telephone);
		} catch (FormValidationException e) {
			setErreur(CHAMP_TELEPHONE, e.getMessage());
		}
		profil.setTelephone(telephone);
	}

	/**
	 * Traiter email.
	 *
	 * @param email
	 *            the email
	 * @param profil
	 *            the profil
	 */
	private void traiterEmail(String email, UserProfil profil) {
		try {
			validationEmail(email);
		} catch (FormValidationException e) {
			setErreur(CHAMP_EMAIL, e.getMessage());
		}
		profil.setEmail(email);
	}

	/*
	 * private void traiterImage( UserProfil profil, HttpServletRequest request,
	 * String chemin ) { String image = null; try { image = validationImage(
	 * request, chemin ); } catch ( FormValidationException e ) { setErreur(
	 * CHAMP_IMAGE, e.getMessage() ); } profil.setImage( image ); }
	 */
	/**
	 * Validation nom.
	 *
	 * @param nom
	 *            the nom
	 * @throws FormValidationException
	 *             the form validation exception
	 */
	private void validationNom(String _nom) throws FormValidationException {
		String nom = Constants.removeSpecChar(_nom);
		if (nom != null) {
			if (nom.length() < 2) {
				throw new FormValidationException(
						"Le nom d'utilisateur doit contenir au moins 2 caractères.");
			}
		} else {
			throw new FormValidationException(
					"Merci d'entrer un nom d'utilisateur.");
		}
	}

	/**
	 * Validation prenom.
	 *
	 * @param prenom
	 *            the prenom
	 * @throws FormValidationException
	 *             the form validation exception
	 */
	private void validationPrenom(String _prenom)
			throws FormValidationException {
		String prenom = Constants.removeSpecChar(_prenom);
		if (prenom != null && prenom.length() < 2) {
			throw new FormValidationException(
					"Le prénom d'utilisateur doit contenir au moins 2 caractères.");
		}
	}

	/**
	 * Validation adresse.
	 *
	 * @param adresse
	 *            the adresse
	 * @throws FormValidationException
	 *             the form validation exception
	 */
	private void validationAdresse(String _adresse)
			throws FormValidationException {
		String adresse = Constants.removeSpecChar(_adresse);
		if (adresse != null) {
			if (adresse.length() < 2) {
				throw new FormValidationException(
						"L'adresse doit contenir au moins 2 caractères.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer une adresse.");
		}
	}

	/**
	 * Validation telephone.
	 *
	 * @param telephone
	 *            the telephone
	 * @throws FormValidationException
	 *             the form validation exception
	 */
	private void validationTelephone(String telephone)
			throws FormValidationException {
		if (telephone != null) {
			if (!telephone.matches("^\\d+$")) {
				throw new FormValidationException(
						"Le numéro de téléphone doit uniquement contenir des chiffres.");
			} else if (telephone.length() < 4) {
				throw new FormValidationException(
						"Le numéro de téléphone doit contenir au moins 4 chiffres.");
			}
		} else {
			throw new FormValidationException(
					"Merci d'entrer un numéro de téléphone.");
		}
	}

	/**
	 * Validation email.
	 *
	 * @param email
	 *            the email
	 * @throws FormValidationException
	 *             the form validation exception
	 */
	private void validationEmail(String email) throws FormValidationException {
		if (email == null)
			throw new FormValidationException(
					"Merci d'entrer une adresse mail.");
		else {
			if (!email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new FormValidationException(
						"Merci de saisir une adresse mail valide.");
			} else {
				if (profilDao.trouver(email) != null)
					throw new FormValidationException(
							"Adresse email déjà existante.");

			}

		}
	}

	private void validationImportCarteVitale(Part filePart) {
		String fileName = Constants.getFileName(filePart);
		File uploads = null;
		// if (getExtension(fileName).equals("PDF")
		// || getExtension(fileName).equals("pdf")) {
		try {
			uploads = new File(Constants.getCheminUpload()
					+ Constants.ADMINISTRATION_FILES_PATH + File.separator
					+ nom.trim() + '_' + prenom.trim() + File.separator);
			// Creer le répértoire s'il n'éxiste pas
			uploads.mkdirs();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (InputStream input = filePart.getInputStream()) {
			File file = new File(uploads, fileName);
			Files.copy(input, file.toPath());
		} catch (FileAlreadyExistsException e) {
		} catch (AccessDeniedException e1) {
			setErreur("carte_vitaleProfil",
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			setErreur("carte_vitaleProfil",
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
			e.printStackTrace();
		}

	}

	private void validationImportCNI(Part cniPart) {

		String fileName = Constants.getFileName(cniPart);
		File uploads = null;
		// if (getExtension(fileName).equals("PDF")
		// || getExtension(fileName).equals("pdf")) {
		try {
			uploads = new File(Constants.getCheminUpload()
					+ Constants.ADMINISTRATION_FILES_PATH + File.separator
					+ nom.trim() + '_' + prenom.trim() + File.separator);
			// Creer le répértoire s'il n'éxiste pas
			uploads.mkdirs();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (InputStream input = cniPart.getInputStream()) {
			File file = new File(uploads, fileName);
			Files.copy(input, file.toPath());
		} catch (FileAlreadyExistsException e) {
		} catch (AccessDeniedException e1) {
			setErreur("cniProfil",
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			setErreur("cniProfil",
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
			e.printStackTrace();
		}

	}

	private void validationImportPhoto(Part photoPart) {

		String fileName = Constants.getFileName(photoPart);
		File uploads = null;
		// if (getExtension(fileName).equals("PDF")
		// || getExtension(fileName).equals("pdf")) {
		try {
			uploads = new File(Constants.getCheminUpload()
					+ Constants.ADMINISTRATION_FILES_PATH + File.separator
					+ nom.trim() + '_' + prenom.trim() + File.separator);
			// Creer le répértoire s'il n'éxiste pas
			uploads.mkdirs();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (InputStream input = photoPart.getInputStream()) {
			File file = new File(uploads, fileName);
			Files.copy(input, file.toPath());
		} catch (FileAlreadyExistsException e) {

		} catch (AccessDeniedException e1) {
			setErreur("photoProfil",
					"Veuillez verifier la selection d'un fichier valide");
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			setErreur("photoProfil",
					"Veuillez verifier la selection d'un fichier valide");
			e.printStackTrace();
		}

	}

	private java.sql.Date getSqlFormatDateFrom(String date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (date != null && !date.isEmpty()) {
			try {
				if (date != null && !date.isEmpty()) {
					String[] tmp = date.split("/");
					if (tmp.length == 3) {
						Date parsed = format.parse(tmp[2] + "-" + tmp[1] + "-"
								+ tmp[0]);
						return new java.sql.Date(parsed.getTime());
					}
				}

			} catch (ParseException e) {
				e.printStackTrace();
			}

		}

		return null;
	}

}