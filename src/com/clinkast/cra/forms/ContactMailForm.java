package com.clinkast.cra.forms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.UserDaoImpl;
import com.clinkast.cra.dao.UserProfilDaoImpl;
import com.clinkast.cra.mails.MailSender;

// TODO: Auto-generated Javadoc
/**
 * The Class ContactMailForm.
 */
public class ContactMailForm extends AbstractForm{


	/** The Constant CHAMP_TO. */
	private static final String CHAMP_TO				= "to";

	/** The Constant CHAMP_SUBJECT. */
	private static final String CHAMP_SUBJECT			= "subject";

	/** The Constant CHAMP_DESCRIPTION. */
	private static final String CHAMP_BODY	 			= "body";
	
	private static final String UPLOAD_DIR = "mailsFiles";

	private UserDaoImpl          				userDao;
	private UserProfilDaoImpl					profilDao;

	/**
	 * Instantiates a new user creation form.
	 *
	 * @param userDao the user dao
	 */
	public ContactMailForm(UserDaoImpl userDao, UserProfilDaoImpl profilDao) {
		super();
		this.userDao = userDao;
		this.profilDao = profilDao;
	}

	/**
	 * Gets the erreurs.
	 *
	 * @return the erreurs
	 */
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	/**
	 * Gets the resultat.
	 *
	 * @return the resultat
	 */
	public String getResultat() {
		return resultat;
	}
	
	public void sendMail(HttpServletRequest request, Utilisateur utilisateur){
		String to = request.getParameter(CHAMP_TO);
		String subject = request.getParameter(CHAMP_SUBJECT);
		String body = request.getParameter(CHAMP_BODY);
		String entete = "";
		List<Utilisateur> utilisateurs = userDao.lister();			
		
		if(!utilisateur.isUser()) traiterTo(to);
		validationSubject(subject);
		traiterBody(  body );
		
		
		if ( erreurs.isEmpty() ) {
		List<String> receiverEmailIDs = new ArrayList<String>();
		
		if(!utilisateur.isUser()) receiverEmailIDs.add(to);
		else{
			UserProfil profil = profilDao.trouver(utilisateur.getLogin());
			for (Utilisateur elt : utilisateurs) {				
				if (!elt.isUser())
					receiverEmailIDs.add( elt.getLogin());
				}
			entete = "From : "+ profil.getName()+"\n";
		}
		
		try
		{
		new MailSender(receiverEmailIDs,subject, entete+"\n"+body);
		resultat = "Votre message a été envoyé";
		}catch (Exception e)
		{
			setErreur( CHAMP_TO, e.getMessage() );
		}
	}
	}
	
	
	public void sendMailFile(HttpServletRequest request, Utilisateur utilisateur){
		String to = request.getParameter(CHAMP_TO);
		String subject = request.getParameter(CHAMP_SUBJECT);
		String body = request.getParameter(CHAMP_BODY);
		String entete = "";
		List<Utilisateur> utilisateurs = userDao.lister();
		
				
		if(!utilisateur.isUser()) traiterTo(to);
		validationSubject(subject);
		traiterBody(  body );
		
		Part filePart=null;
		String filename =null;
		String path="";
		try {
			filePart = request.getPart("attachment");		
			if(filePart.getSize()!=0){
				filePart.getContentType();
				// gets absolute path of the web application
		        String applicationPath = request.getServletContext().getRealPath("");
		        // constructs path of the directory to save uploaded file
		        String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;
		         
		        // creates the save directory if it does not exists
		        File fileSaveDir = new File(uploadFilePath);
		        if (!fileSaveDir.exists()) {
		            fileSaveDir.mkdirs();
		        }
		        //System.out.println("Upload File Directory="+fileSaveDir.getAbsolutePath());
		        filename = getFilename(filePart);
		        path = uploadFilePath + File.separator + filename;
				filePart.write(path); 
				
				
			}
	    
		} catch (IllegalStateException | IOException | ServletException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try
		{
		if ( erreurs.isEmpty() ) {
		List<String> receiverEmailIDs = new ArrayList<String>();
		
		if(!utilisateur.isUser()) receiverEmailIDs.add(to);
		else{
			UserProfil profil = profilDao.trouver(utilisateur.getLogin());
			for (Utilisateur elt : utilisateurs) {				
				if (!elt.isUser())
					receiverEmailIDs.add( elt.getLogin());
				}
			entete = "From : "+ profil.getName()+"\n";
		}
		
		
			if(filePart.getSize()!=0) new MailSender(receiverEmailIDs,subject, entete+"\n"+body, path, filename);
			else new MailSender(receiverEmailIDs,subject, entete+"\n"+body);
			resultat = "Votre message a été envoyé";
		}}catch (Exception e)
		{

			e.printStackTrace();
			resultat = "Echec de l'envoi du message ";
			setErreur( CHAMP_BODY, resultat );
		}
	}
	
	
	private void traiterTo( String to ) {
        try {
        	validationTo(to);
        } catch ( FormValidationException e ) {
        	setErreur( CHAMP_TO, e.getMessage() );
            
        }
       
    }
	
	private void validationTo( String login ) throws FormValidationException {
		if ( login == null) throw new FormValidationException( "Merci de selectionner un destinataire." );
    }
	
	
	private void validationSubject( String subject )  {
		if ( subject == null) subject=" ";
    }
	
	
	private void traiterBody( String body ) {
        try {
        	validationBody(body);
        } catch ( FormValidationException e ) {
        	setErreur( CHAMP_BODY, e.getMessage() );
            
        }
       
    }
	
	private void validationBody( String body ) throws FormValidationException {
		if ( (body == null) || body.length()==0){
			System.out.println("body null?...");
			throw new FormValidationException( "Merci de saisir un message à envoyer." );
		}
    }
	
    
    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }
   
}
