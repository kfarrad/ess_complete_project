package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.UserDaoImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class ConnexionForm.
 */
public final class ConnexionForm extends AbstractForm{
	
	/** The Constant CHAMP_LOGIN. */
	private static final String CHAMP_LOGIN  = "login";
	
	/** The Constant CHAMP_PASS. */
	private static final String CHAMP_PASS   = "motdepasse";

	/** The user dao. */
	private UserDaoImpl           userDao;

	/**
	 * Instantiates a new connexion form.
	 *
	 * @param clientDao the client dao
	 */
	public ConnexionForm( UserDaoImpl clientDao ) {
		super();
		this.userDao = clientDao;
	}

	/**
	 * Connecter utilisateur.
	 *
	 * @param request the request
	 * @return the utilisateur
	 */
	public Utilisateur connecterUtilisateur( HttpServletRequest request ) {
		/* Récupération des champs du formulaire */
		String login = Constants.getValeurChamp( request, CHAMP_LOGIN );
		String motDePasse = Constants.getValeurChamp( request, CHAMP_PASS );

		/* Validation du champ email. */
		try {
			validationLogin( login );
		} catch ( Exception e ) {
			setErreur( CHAMP_LOGIN, e.getMessage() );
		}


		/* Validation du champ mot de passe. */
		try {
			validationMotDePasse( motDePasse );
		} catch ( Exception e ) {
			setErreur( CHAMP_PASS, e.getMessage() );
		}


		Utilisateur utilisateur = new Utilisateur();
		/* Initialisation du résultat global de la validation. */
		if ( erreurs.isEmpty() ) {
			try{
				utilisateur =validationConnection(login,motDePasse);
			}
			catch ( Exception e ) {
				setErreur( CHAMP_LOGIN, e.getMessage() );
			}

			if ( erreurs.isEmpty() ) resultat = "Succès de la connexion.";
		}
		else
		{
			resultat = "Échec de la connexion.";
		}


		return utilisateur;
	}

	/**
	 * Valide l'adresse email saisie.
	 *
	 * @param login the login
	 * @throws Exception the exception
	 */
	private void validationLogin( String login ) throws Exception {
		if ( login != null && !login.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
			throw new Exception( "Merci de saisir une adresse mail valide." );
		}
	}

	/**
	 * Valide le mot de passe saisi.
	 *
	 * @param motDePasse the mot de passe
	 * @throws Exception the exception
	 */
	private void validationMotDePasse( String motDePasse ) throws Exception {
		if ( motDePasse != null ) {
			if ( motDePasse.length() < 3 ) {
				throw new Exception( "Le mot de passe doit contenir au moins 3 caractères." );
			}
		} else {
			throw new Exception( "Merci de saisir votre mot de passe." );
		}
	}

	/**
	 * Valide la connexion.
	 *
	 * @param email the email
	 * @param motDePasse the mot de passe
	 * @return the utilisateur
	 * @throws Exception the exception
	 */
	private Utilisateur validationConnection( String email, String motDePasse) throws Exception {
		Utilisateur utilisateur = userDao.estUtilisateur(email, motDePasse); 
		if(utilisateur == null){    		
			throw new Exception( "nom d'utilisateur et/ou mot de passe invalide." );
		}
		else
			if(!utilisateur.isActif())  throw new Exception( "compte invalide." );
		return utilisateur;

	}

		
}