package com.clinkast.cra.forms;

// TODO: Auto-generated Javadoc
/**
 * The Class FormValidationException.
 */
public class FormValidationException extends Exception {
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/*
     * Constructeur
     */
    /**
	 * Instantiates a new form validation exception.
	 *
	 * @param message the message
	 */
	public FormValidationException( String message ) {
        super( message );
    }
}