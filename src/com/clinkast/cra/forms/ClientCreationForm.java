package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Client;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.ClientDaoImpl;
import com.clinkast.cra.dao.DAOException;

// TODO: Auto-generated Javadoc
/**
 * The Class ClientCreationForm.
 */
public class ClientCreationForm extends AbstractForm{

	/** The Constant CHAMP_NOM. */
	private static final String CHAMP_NOM  		= "nom";
	
	/** The Constant CHAMP_ADRESSE. */
	private static final String CHAMP_ADRESSE   = "adresse";
	
	/** The client dao. */
	private ClientDaoImpl           clientDao;

	/**
	 * Instantiates a new client creation form.
	 *
	 * @param clientDao the client dao
	 */
	public ClientCreationForm( ClientDaoImpl clientDao ) {
		super();
		this.clientDao = clientDao;
	}

	
	/**
	 * Creer client.
	 *
	 * @param request the request
	 * @return the client
	 */
	public Client creerClient( HttpServletRequest request) {
		String nom = Constants.getValeurChamp( request, CHAMP_NOM );
		String adresse = Constants.getValeurChamp( request, CHAMP_ADRESSE );


		Client client = new Client();

		traiterNom( nom, client );		
		traiterAdresse( adresse, client );

		try {
			if ( erreurs.isEmpty() ) {
				Client myClient= clientDao.trouver(nom);
				if(myClient==null){
					clientDao.creer( client );
					resultat = "Succès de la création du client.";
				}
				else
				{
					setErreur( CHAMP_NOM, "client déjà existant" );
				}
				} 
				else {
					resultat = "Échec de la création du client.";
				}
			
		} catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la création." );
			resultat ="Échec de la création du client : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}


		return client;
	}


	/**
	 * Modifier client.
	 *
	 * @param request the request
	 * @param id the id
	 * @return the client
	 */
	public Client modifierClient(HttpServletRequest request, long id)
	{
		String nom = Constants.getValeurChamp( request, CHAMP_NOM );
		String adresse = Constants.getValeurChamp( request, CHAMP_ADRESSE );

		
		Client client = new Client();

		traiterNom( nom, client );
		traiterAdresse( adresse, client );
		System.out.println(client.toString());

		try {
			if ( erreurs.isEmpty() ) {
				clientDao.modifier(client, id);
				resultat = "Succès de la modification du client.";
			}
		}catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la modification." );
			resultat ="Échec de la modification du client : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}


		return client;
	}

	/**
	 * Traiter nom.
	 *
	 * @param nom the nom
	 * @param client the client
	 */
	private void traiterNom( String nom, Client client ) {
		try {
			validationNom( nom );
		} catch ( FormValidationException e ) {
			setErreur( CHAMP_NOM, e.getMessage() );
		}
		client.setNom( nom );
	}

	/**
	 * Traiter adresse.
	 *
	 * @param adresse the adresse
	 * @param client the client
	 */
	private void traiterAdresse( String adresse, Client client ) {
		try {
			validationAdresse( adresse );
		} catch ( FormValidationException e ) {
			setErreur( CHAMP_ADRESSE, e.getMessage() );
		}
		client.setAdresse( adresse );
	}





	/**
	 * Validation nom.
	 *
	 * @param nom the nom
	 * @throws FormValidationException the form validation exception
	 */
	private void validationNom( String nom ) throws FormValidationException {
		if ( nom != null ) {
			if ( nom.length() < 2 ) {
				throw new FormValidationException( "Le nom doit contenir au moins 2 caractères." );
			}
		} else {
			throw new FormValidationException( "Merci d'entrer un nom de client." );
		}
	}

	/**
	 * Validation adresse.
	 *
	 * @param adresse the adresse
	 * @throws FormValidationException the form validation exception
	 */
	private void validationAdresse( String adresse ) throws FormValidationException {
		if ( adresse != null ) {
			if ( adresse.length() < 2 ) {
				throw new FormValidationException( "L'adresse du client doit contenir au moins 2 caractères." );
			}
		} else {
			throw new FormValidationException( "Merci d'entrer une adresse de client." );
		}
	}


}
