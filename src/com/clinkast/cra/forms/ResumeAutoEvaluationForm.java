package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.AutoEvaluations;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;

public class ResumeAutoEvaluationForm extends AbstractForm{
	public static final String CHAMP_ID = "autoEvaluations[id_auto]";
	public static final String ATT_RES_AUTRESMOYENS= "resume[autoEvaluationsAutresMoyens]";
	public static final String ATT_RES_ELEMENTSSIGNIF = "resume[autoEvaluationsElementsSignifications]";
	public static final String ATT_RES_BILANCLINKAST = "resume[autoEvaluationsBilanClinkastAttentes]";
	public static final String ATT_RES_BILANMESATTENTES = "resume[autoEvaluationsBilanMesAttentes]";
	public static final String ATT_RES_ATOUTPROGRES = "resume[autoEvaluationsAtoutPourProgresser]";
	public static final String ATT_RES_AXEPROGRES = "resume[autoEvaluationsAxesAprogresser]";
	public static final String ATT_RES_SOUHAITCOURT = "resume[autoEvaluationsSouhaitsCourtMoyen]";
	public static final String ATT_RES_ELEMENTSIMPORT = "resume[autoEvaluationsElemetsImportant]";
	public static final String ATT_RES_RESUME = "resume[autoEvaluationsResume]";
	
		private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public ResumeAutoEvaluationForm(GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl, AutoEvaluationDAOImpl autoEvaluationDAOImpl){
		super();
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
	}
	
	public GestionEvaluations modifier(HttpServletRequest request){
		/*Lire les champs de request*/
		
		Long autoEvaluationId = Long.parseLong(Constants.getValeurChamp(request, CHAMP_ID));
		String autoEvaluationsAutresMoyens = Constants.getValeurChamp(request, ATT_RES_AUTRESMOYENS);
		String autoEvaluationsElementsSignifications = Constants.getValeurChamp(request, ATT_RES_ELEMENTSSIGNIF);
		String autoEvaluationsBilanClinkastAttentes = Constants.getValeurChamp(request, ATT_RES_BILANCLINKAST);
		String autoEvaluationsBilanMesAttentes = Constants.getValeurChamp(request, ATT_RES_BILANMESATTENTES);
		String autoEvaluationsAtoutPourProgresser = Constants.getValeurChamp(request, ATT_RES_ATOUTPROGRES);
		String autoEvaluationsAxesAprogresser = Constants.getValeurChamp(request, ATT_RES_AXEPROGRES);
		String autoEvaluationsSouhaitsCourtMoyen = Constants.getValeurChamp(request, ATT_RES_SOUHAITCOURT);
		String autoEvaluationsElemetsImportant = Constants.getValeurChamp(request, ATT_RES_ELEMENTSIMPORT);
		String autoEvaluationsResume = Constants.getValeurChamp(request, ATT_RES_RESUME);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;		
		
        if(autoEvaluationId instanceof Long && autoEvaluationId != null)
        {
        	try{
        		
	        	AutoEvaluations autoEvaluation = autoEvaluationDAOImpl.trouver(autoEvaluationId);
				
				autoEvaluation.setAutoEvaluationsAutresMoyens(autoEvaluationsAutresMoyens);
				autoEvaluation.setAutoEvaluationsAtoutPourProgresser(autoEvaluationsAtoutPourProgresser);
				autoEvaluation.setAutoEvaluationsAxesAprogresser(autoEvaluationsAxesAprogresser);
				autoEvaluation.setAutoEvaluationsBilanClinkastAttentes(autoEvaluationsBilanClinkastAttentes);
				autoEvaluation.setAutoEvaluationsBilanMesAttentes(autoEvaluationsBilanMesAttentes);
				autoEvaluation.setAutoEvaluationsElementsSignifications(autoEvaluationsElementsSignifications);
				autoEvaluation.setAutoEvaluationsElemetsImportant(autoEvaluationsElemetsImportant);
				autoEvaluation.setAutoEvaluationsResume(autoEvaluationsResume);
				autoEvaluation.setAutoEvaluationsSouhaitsCourtMoyen(autoEvaluationsSouhaitsCourtMoyen);
				
				autoEvaluationDAOImpl.modifier(autoEvaluation);
				resultat = "modification avec succès";
        	}
        	catch (DAOException e) {
				// TODO: handle exception
        		setErreur("Erreur imprevu", e.getMessage());
            	resultat = "Une erreur est revenue, aucune modification n'a été faite sur les données";
			} 
        }
        else
        {
        	setErreur("Identifiant formulaire", "Le formulaire n'est pas identifié");
        	resultat = "Le formulaire n'est pas identifié";
        }
        gestionEvaluations = updateGestionAutoEvaluation(session, autoEvaluationId);
		return gestionEvaluations;
	}
	
	private GestionEvaluations updateGestionAutoEvaluation(HttpSession session, Long evaluationId){

		GestionEvaluations gestionEvaluations = null;
		Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		
		//Mis à jour de gestionAutoEvaluation
		try{		
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(evaluationId, utilisateur.getId());
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime());
			gestionEvaluations.setGestionEvaluationsNbreModification(gestionEvaluations.getGestionEvaluationsNbreModification()+1);
			gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
		}
		catch (Exception e) {
			// TODO: handle exception
			setErreur("imprevu dans GestionEvaluationDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}
		return gestionEvaluations;
	}


}
