package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.ProjetDaoImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjetCreationForm.
 */
public class ProjetCreationForm extends AbstractForm{

	/** The Constant CHAMP_NOM. */
	private static final String CHAMP_NOM  		= "nom";
	
	/** The projet dao. */
	private ProjetDaoImpl           projetDao;

	/**
	 * Instantiates a new projet creation form.
	 *
	 * @param projetDao the projet dao
	 */
	public ProjetCreationForm( ProjetDaoImpl projetDao ) {
		super();
		this.projetDao = projetDao;
	}
	
	/**
	 * Creer projet.
	 *
	 * @param request the request
	 * @param id_client the id_client
	 * @return the projet
	 */
	public Projet creerProjet( HttpServletRequest request, Long id_client ) {
		String nom = Constants.getValeurChamp( request, CHAMP_NOM );
		Projet projet = new Projet();
		traiterNom( nom, projet );		

		try {
			if ( erreurs.isEmpty() ) {        
				Projet myProjet = projetDao.trouver(nom);
				if(myProjet==null){
				projetDao.creer( projet, id_client );
				resultat = "Succès de la création du projet.";
				}
				else
				{
					setErreur( CHAMP_NOM, "projet déjà existant" );
				}
			} else {
				resultat = "Échec de la création du projet.";
			}
		} catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la création." );
			resultat ="Échec de la création du projet : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}


		return projet;
	}
	
	
	/**
	 * Modifier projet.
	 *
	 * @param request the request
	 * @param id the id
	 * @return the projet
	 */
	public Projet modifierProjet(HttpServletRequest request, long id)
	{
		String nom = Constants.getValeurChamp( request, CHAMP_NOM );		
		Projet projet = new Projet();
		traiterNom( nom, projet );
		
		try {
			if ( erreurs.isEmpty() ) {
				projetDao.modifier(projet, id);
				resultat = "Succès de la modification du projet.";
			}
		}catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la modification." );
			resultat ="Échec de la modification du projet : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}


		return projet;
	}
	
	/**
	 * User projet.
	 *
	 * @param id_user the id_user
	 * @param id_projet the id_projet
	 */
	public void userProjet(Long id_user, Long id_projet){
		try{

			projetDao.attribution(id_user, id_projet);
		}catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de l'attribution du projet à un utilisateur." );
			resultat ="Échec de l'attribution du projet à un utilisateur : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}
	}
	
	/**
	 * Traiter nom.
	 *
	 * @param nom the nom
	 * @param projet the projet
	 */
	private void traiterNom( String nom, Projet projet ) {
		try {
			validationNom( nom );
		} catch ( FormValidationException e ) {
			setErreur( CHAMP_NOM, e.getMessage() );
		}
		projet.setNom( nom );
	}
	
	/**
	 * Validation nom.
	 *
	 * @param nom the nom
	 * @throws FormValidationException the form validation exception
	 */
	private void validationNom( String nom ) throws FormValidationException {
		if ( nom != null ) {
			if ( nom.length() < 2 ) {
				throw new FormValidationException( "Le nom d'utilisateur doit contenir au moins 2 caractères." );
			}
		} else {
			throw new FormValidationException( "Merci d'entrer un nom d'utilisateur." );
		}
	}

	
	
	
}
