package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.SystemesActivites;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.SystemesActivitesDAOImpl;

/**
 * @author Oliver
 *
 */
public class ActivitesForm extends AbstractForm{

	/*	Constantes */
	public static final String PARAM_ID = "id";
	public static final String CHAMP_ID = "activites[id_auto]";
	public static final String CHAMP_ACT_PERIODE_DE = "activites[periode_de]";
	public static final String CHAMP_ACT_PERIODE_AU = "activites[periode_au]";
	public static final String CHAMP_ACT_MISSIONS = "activites[missions]";
	public static final String CHAMP_ACT_RESPONSABLE = "activites[responsable]";
	public static final String CHAMP_ACT_ENTRETIEN = "activites[entretien]";
	public static final String CHAMP_ACT_ID = "activites[id_activite]";
	public static final String CHAMP_TAB_ACT_ID = "activites_id_activite";
	
	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private SystemesActivitesDAOImpl systemesActivitesDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public ActivitesForm(GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl, AutoEvaluationDAOImpl autoEvaluationDAOImpl, SystemesActivitesDAOImpl systemesActivitesDAOImpl){
		super();
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
		this.systemesActivitesDAOImpl = systemesActivitesDAOImpl;
	}
	
	public GestionEvaluations sauveActivites(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String periode_de = Constants.getValeurChamp(request, CHAMP_ACT_PERIODE_DE);
		String periode_au = Constants.getValeurChamp(request, CHAMP_ACT_PERIODE_AU);
		String act_mission = Constants.getValeurChamp(request, CHAMP_ACT_MISSIONS);
		String act_responsable = Constants.getValeurChamp(request, CHAMP_ACT_RESPONSABLE);
		String act_entretien = Constants.getValeurChamp(request, CHAMP_ACT_ENTRETIEN);
		String act_id = Constants.getValeurChamp(request, CHAMP_ACT_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;
		
		/*Valeur de la variable entretien*/
		boolean entretien = ((Integer.parseInt(act_entretien) == 1) ? true: false);
		
		SystemesActivites systemesActivites = new SystemesActivites();
		
		//Convertir les id en Long
		Long evaluationId = Long.parseLong(autoEvaluationId);
		Long activite_id = Long.parseLong(act_id);
		systemesActivites.setAutoEvaluations(autoEvaluationDAOImpl.trouver(evaluationId));
		systemesActivites.setSystemesActivitesPeriodeDe(periode_de);
		systemesActivites.setSystemesActivitesPeriodeAu(periode_au); 
		systemesActivites.setSystemesActivitesMissions(act_mission);
		systemesActivites.setSystemesActivitesResponsable(act_responsable);
		systemesActivites.setSystemesActivitesEntretien(entretien);
				
		try{
			//Si l'id n'existe pas alors on cree l'element
			if(activite_id == 0){
				//nouvelle activité
				systemesActivitesDAOImpl.creer(systemesActivites);
				resultat = "L'activité a été créé";
			}
			else{
				//on ajoute id
				systemesActivites.setSystemesActivitesId(activite_id);
				//on modifie les attributs de l'activité
				systemesActivitesDAOImpl.modifier(systemesActivites);
				resultat = "L'activité a été modifié";
			}
			gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
			
			
		}
		catch (DAOException e) {
			// TODO: handle exception
			setErreur("imprevu dans SystemesActivitesDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}		
		return gestionEvaluations;
	}
	
	/*Supprimer une activité*/
	public GestionEvaluations supprimeActivites(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String[] deleteId = (String[])request.getParameterValues( CHAMP_TAB_ACT_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;		
		Long evaluationId = Long.parseLong(autoEvaluationId);
		int compteur = 0;
		
		if(deleteId != null)
		{
			try{
				for(String id : deleteId){
					Long act_id = Long.parseLong(id);
					SystemesActivites systemesActivites =systemesActivitesDAOImpl.trouver(act_id);
					systemesActivitesDAOImpl.supprimer(systemesActivites);
					compteur ++;
				}
				gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
				
				resultat = compteur + (compteur >= 2 ? " activités ont " : " activité a ") + "été supprimé";
			}
			catch (DAOException e) {
				// TODO: handle exception
				setErreur("imprevu dans SystemesActivitesDAOImpl: ", e.getMessage());
				resultat = e.getMessage();
			}
		}
		return gestionEvaluations;
	}
	
	
	private GestionEvaluations updateGestionAutoEvaluation(HttpSession session, Long evaluationId){

		GestionEvaluations gestionEvaluations = null;
		Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		
		//Mis à jour de gestionAutoEvaluation
		try{		
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(evaluationId, utilisateur.getId());
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime());
			gestionEvaluations.setGestionEvaluationsNbreModification(gestionEvaluations.getGestionEvaluationsNbreModification()+1);
			gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
		}
		catch (Exception e) {
			// TODO: handle exception
			setErreur("imprevu dans GestionEvaluationDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}
		return gestionEvaluations;
	}


}
