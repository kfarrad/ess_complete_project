package com.clinkast.cra.forms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.clinkast.cra.beans.Candidat;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.CandidatureDaoImpl;
import com.clinkast.cra.dao.DAOException;

/**
 * This class manage all errors during the process of adding new resume
 * 
 * @author Clinkast
 */
public class ValidationTraitementCandidatureForm {

	private static final Set<String> EXTENSIONS = new HashSet<String>(
			Arrays.asList(new String[] { "pdf", "PDF", "doc", "DOC", "docx",
					"DOCX" }));

	/** The errors generated after the validation of the form. */
	private Map<String, String> erreurs = new HashMap<String, String>();

	/** The result of the validation of the form. */
	private String resultat;
	/** the list of fields errors */
	private List<String> errorList = new ArrayList<String>();

	/** The candidature Dao */
	private CandidatureDaoImpl candidatureDao;

	private boolean creationStatut = false;

	public boolean isCreationStatut() {
		return creationStatut;
	}

	public void setResultat(String resultat) {
		this.resultat = resultat;
	}

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public ValidationTraitementCandidatureForm(CandidatureDaoImpl candidatureDao) {
		this.candidatureDao = candidatureDao;
	}

	/**
	 * <p>
	 * creates the candidature in database and upload resume after validating
	 * empty inputs and uploaded file
	 * </p>
	 * 
	 * @param request
	 *            the request to retrieve data from jsp
	 * @param filePart
	 *            the Part to to get the file to upload
	 */
	public Candidat creationCandidature(HttpServletRequest request,
			Part filePart, String userName) {

		// RÃ©cuperation des champs du formulaire
		String nom = Constants.getValeurChamp(request,
				Constants.ATT_NOM_CANDIDAT);
		String prenom = Constants.getValeurChamp(request,
				Constants.ATT_PRENOM_CANDIDAT);
		String telephone = Constants.getValeurChamp(request,
				Constants.ATT_TELEPHONE_CANDIDAT);
		String mail = Constants.getValeurChamp(request,
				Constants.ATT_MAIL_CANDIDAT);
		String[] diplomes = Constants.getValeurChamp(request,
				Constants.ATT_DIPLOMES_CANDIDAT).split(";");
		String[] specialites = Constants.getValeurChamp(request,
				Constants.ATT_SPECIALITES_CANDIDAT).split(";");
		String nom_CV = Constants.getFileName(filePart);
		String commentaire = Constants.getValeurChamp(request,
				Constants.ATT_COMMENT_CANDIDAT);
		String fullPath = "";
		String comment = (commentaire == null || commentaire.isEmpty()) ? "(Pas de commentaire)"
				: commentaire;
		String commentaires = "<li> Crée par: <b>" + userName
				+ "</b><br /> Le: <b>" + dateFormat(new Date())
				+ "</b><br /> Commentaires: <b>" + comment + "</b></li><br />";
		String diplome = "";
		String specialite = "";

		for (int i = 0; i < diplomes.length; i++) {
			diplome = diplome + diplomes[i] + ";";
		}

		for (int i = 0; i < specialites.length; i++) {
			specialite = specialite + specialites[i] + ";";
		}

		validationChampVide(nom, prenom, telephone, mail, diplome, specialite);
		fullPath = validationExtention(nom_CV);
		validationEmail(mail);
		validationImportCV(filePart);

		if (errorList.contains("nom"))
			setErreur(Constants.ATT_NOM_CANDIDAT,
					"Merci de remplir le champ nom");

		if (errorList.contains("prenom"))
			setErreur(Constants.ATT_PRENOM_CANDIDAT,
					"Merci de remplir le champ prenom");

		if (errorList.contains("telephone"))
			setErreur(Constants.ATT_TELEPHONE_CANDIDAT,
					"Merci de remplir le champ telephone");

		if (errorList.contains("mail"))
			setErreur(Constants.ATT_MAIL_CANDIDAT,
					"Merci de remplir le champ mail");

		if (errorList.contains("mailValide"))
			setErreur(Constants.ATT_MAIL_CANDIDAT,
					"Merci de renseigner un mail valide");

		if (errorList.contains("diplome"))
			setErreur(Constants.ATT_DIPLOME_CANDIDAT,
					"Merci de remplir le champ diplôme");

		if (errorList.contains("specialite"))
			setErreur(Constants.ATT_SPECIALITE_CANDIDAT,
					"Merci de remplir le champ spécialité");

		Candidat candidature = new Candidat(nom, prenom, telephone, mail,
				diplome, specialite, fullPath, commentaires);
		try {
			if (erreurs.isEmpty()) {
				candidatureDao.creer(candidature);
				resultat = "Création du CV réussie !";
				creationStatut  = true;
			} else {
				String chemin = Constants.getCheminUpload();
				File file = new File(chemin + fullPath);
				file.delete();
				resultat = "Echec de la création du CV !";
			}
		} catch (DAOException e) {
			String chemin = "";
			try {
				chemin = Constants.getCheminUpload();
			} catch (FileNotFoundException e1) {
			} catch (IOException e1) {
			}
			File file = new File(chemin + fullPath);
			file.delete();
			setErreur("imprévu", "Erreur imprévue lors de la création.");
			resultat = "Echec de la création du CV : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return candidature;
	}

	/**
	 * <p>
	 * updates the candidature in database and probably upload new resume after
	 * validating empty inputs and uploaded file
	 * </p>
	 * 
	 * @param request
	 *            the request to retrieve data from jsp
	 * @param filePart
	 *            the Part to get the file to upload (if there is an update)
	 * @param id
	 *            the id of Candidature to update
	 */
	public Candidat modificationCandidature(HttpServletRequest request,
			Part filePart, String pathCV, long id, String comment,
			String userName) {
		// RÃ©cuperation des champs du formulaire
		String nom = Constants.removeSpecChar(Constants.getValeurChamp(request,
				Constants.ATT_NOM_CANDIDAT));
		String prenom = Constants.removeSpecChar(Constants.getValeurChamp(request,
				Constants.ATT_PRENOM_CANDIDAT));
		String telephone = Constants.removeSpecChar(Constants.getValeurChamp(request,
				Constants.ATT_TELEPHONE_CANDIDAT));
		String mail = Constants.getValeurChamp(request,
				Constants.ATT_MAIL_CANDIDAT);
		String[] diplomes = Constants.getValeurChamp(request,
				Constants.ATT_DIPLOMES_CANDIDAT).split(";");
		String[] specialites = Constants.getValeurChamp(request,
				Constants.ATT_SPECIALITES_CANDIDAT).split(";");
		String commentaire = Constants.removeSpecChar(Constants.getValeurChamp(request,
				Constants.ATT_COMMENT_CANDIDAT));
		String fullPath = "";
		String commentValue = (commentaire == null || commentaire.isEmpty()) ? "(Pas de commentaire)"
				: commentaire;
		String commentaires = comment + "<li> Modifié par <b>" + userName
				+ "</b><br /> le <b>" + dateFormat(new Date())
				+ "</b><br /> commentaires: <b>" + commentValue
				+ "</b></li><br />";

		String diplome = "";
		String specialite = "";

		for (int i = 0; i < diplomes.length; i++) {
			diplome = diplome + Constants.removeSpecChar(diplomes[i]) + ";";
		}

		for (int i = 0; i < specialites.length; i++) {
			specialite = specialite + Constants.removeSpecChar(specialites[i]) + ";";
		}
		
		try {
			validationChampVide(nom, prenom, telephone, mail, diplome,
					specialite);
			validationEmail(mail);
			if (!Constants.getFileName(filePart).isEmpty()) {
				fullPath = validationExtention(Constants.getFileName(filePart));
				// Suppression d'ancien CV en cas de modification 
				String chemin = Constants.getCheminUpload();
				File file = new File(chemin + pathCV);
				file.delete();
				validationImportCV(filePart);
			} else {
				fullPath = pathCV;
			}
		} catch (Exception e) {
			if (errorList.contains("nom"))
				setErreur(Constants.ATT_NOM_CANDIDAT,
						"Merci de remplir le champ nom");

			if (errorList.contains("prenom"))
				setErreur(Constants.ATT_PRENOM_CANDIDAT,
						"Merci de remplir le champ prenom");

			if (errorList.contains("telephone"))
				setErreur(Constants.ATT_TELEPHONE_CANDIDAT,
						"Merci de remplir le champ telephone");

			if (errorList.contains("mail"))
				setErreur(Constants.ATT_MAIL_CANDIDAT,
						"Merci de remplir le champ mail");

			if (errorList.contains("mailValide"))
				setErreur(Constants.ATT_MAIL_CANDIDAT,
						"Merci de renseigner un mail valide");

			if (errorList.contains("diplome"))
				setErreur(Constants.ATT_DIPLOME_CANDIDAT,
						"Merci de remplir le champ diplôme");

			if (errorList.contains("specialite"))
				setErreur(Constants.ATT_SPECIALITE_CANDIDAT,
						"Merci de remplir le champ spécialité");
		}
		
		Candidat candidature = new Candidat(nom, prenom, telephone, mail,
				diplome, specialite, fullPath, commentaires);

		try {
			if (erreurs.isEmpty()) {
				candidatureDao.modifier(candidature, id);
				resultat = "Modification du CV rÃ©ussie !";
			} else {
				resultat = "Echec de la modification du CV !";
			}
		} catch (DAOException e) {
			setErreur("imprÃ©vu", "Erreur imprÃ©vue lors de la modification.");
			resultat = "Echec de la modification du CV : une erreur imprÃ©vue est survenue, merci de rÃ©essayer dans quelques instants.";
			e.printStackTrace();
		}
		
		return candidature;
	}

	/**
	 * <p>
	 * deletes the candidature in database and delete resume from a disc
	 * </p>
	 * 
	 * @param id
	 *            the id of Candidature to delete
	 */
	public Candidat suppressionCandidature(long id) {
		Candidat candidat = null;
		try {
			candidat = candidatureDao.supprimer(id);
			resultat = "Suppression du CV réussie !";
		} catch (DAOException e) {
			setErreur("imprévu", "Erreur imprévue lors de la modification.");
			resultat = "Echec de la modification du CV : une erreur imprÃ©vue est survenue, merci de rÃ©essayer dans quelques instants.";
			e.printStackTrace();
		}
		return candidat;
	}

	/** The extension of the file to upload */
	private String getExtension(String fileName) {
		String extension = "";
		if (fileName != null) {
			extension = fileName.substring(fileName.lastIndexOf(".") + 1,
					fileName.length());
		}
		return extension;
	}

	private String validationExtention(String nomcv) {
		if (!EXTENSIONS.contains(getExtension(nomcv))) {
			setErreur(Constants.ATT_CV_CANDIDAT,
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
		}
		if (getExtension(nomcv).equals("PDF")
				|| getExtension(nomcv).equals("pdf"))
			return Constants.CANDIDATURE_PDF_FILES_PATH + nomcv;
		if (getExtension(nomcv).equals("DOCX")
				|| getExtension(nomcv).equals("docx"))
			return Constants.CANDIDATURE_DOCX_FILES_PATH + nomcv;
		if (getExtension(nomcv).equals("DOC")
				|| getExtension(nomcv).equals("doc"))
			return Constants.CANDIDATURE_DOC_FILES_PATH + nomcv;
		return null;

	}

	private void validationChampVide(String nom, String prenom,
			String telephone, String mail, String diplome, String specialite) {
		if (nom == null || nom.isEmpty()) {
			errorList.add("nom");
		}
		if (prenom == null || prenom.isEmpty()) {
			errorList.add("prenom");
		}
		if (telephone == null || telephone.isEmpty())
			errorList.add("telephone");
		if (mail == null || mail.isEmpty())
			errorList.add("mail");
		if (diplome == null || diplome.isEmpty())
			errorList.add("diplome");
		if (specialite == null || specialite.isEmpty())
			errorList.add("specialite");

	}

	/**
	 * Valide l'adresse email saisie.
	 * 
	 * @param login
	 *            the login
	 * @throws Exception
	 *             the exception
	 */
	private void validationEmail(String email) {
		if (email != null
				&& !email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
			errorList.add("mailValide");
		}
	}

	/**
	 * Sets the erreur.
	 *
	 * @param champ
	 *            the champ
	 * @param message
	 *            the message
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	private void validationImportCV(Part filePart) {
		String fileName = Constants.getFileName(filePart);
		File uploads = null;
		if (getExtension(fileName).equals("PDF")
				|| getExtension(fileName).equals("pdf")) {
			try {
				uploads = new File(Constants.getCheminUpload()
						+ Constants.CANDIDATURE_PDF_FILES_PATH);
				//Creer le répértoire s'il n'éxiste pas 
				uploads.mkdirs();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (getExtension(fileName).equals("DOCX")
				|| getExtension(fileName).equals("docx")) {
			try {
				uploads = new File(Constants.getCheminUpload()
						+ Constants.CANDIDATURE_DOCX_FILES_PATH);
				uploads.mkdirs();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (getExtension(fileName).equals("DOC")
				|| getExtension(fileName).equals("doc")) {
			try {
				uploads = new File(Constants.getCheminUpload()
						+ Constants.CANDIDATURE_DOC_FILES_PATH);
				uploads.mkdirs();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try (InputStream input = filePart.getInputStream()) {
			File file = new File(uploads, fileName);
			Files.copy(input, file.toPath());
		} catch (FileAlreadyExistsException e) {
			setErreur(Constants.ATT_CV_CANDIDAT,
					"Le fichier que vous essayez d'importer existe dèja !");
			e.printStackTrace();
		} catch (AccessDeniedException e1) {
			setErreur(Constants.ATT_CV_CANDIDAT,
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			setErreur(Constants.ATT_CV_CANDIDAT,
					"Veuillez verifier la selection d'un fichier valide (.pdf, .doc, .docx)");
			e.printStackTrace();
		}

	}

	private String dateFormat(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY kk:mm:ss");
		String[] d = format.format(date).split(" ");
		return d[0] + " à  " + d[1];
	}

}
