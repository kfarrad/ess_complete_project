package com.clinkast.cra.forms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Competence;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Experience;
import com.clinkast.cra.beans.Formation;
import com.clinkast.cra.beans.Langue;
import com.clinkast.cra.beans.Resume;
import com.clinkast.cra.beans.TitreCV;
import com.clinkast.cra.dao.CompetenceDaoImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.ExperienceDaoImpl;
import com.clinkast.cra.dao.FormationDaoImpl;
import com.clinkast.cra.dao.LangueDaoImpl;
import com.clinkast.cra.dao.ResumeDaoImpl;
import com.clinkast.cra.dao.TitreCVDaoImpl;

public class ResumeForm extends AbstractForm{
	/*
	 * Titre
	 */
	private static final String CHAMP_TITLE_ID              ="title[id]";

	private static final String CHAMP_TITLE_INITIAL         ="title[initial]";

	private static final String CHAMP_TITLE_NAME            ="title[name]";

	private static final String CHAMP_TITLE_SUB             ="title[sub]";
	/*
	 * Experience professionnelle
	 */
	private static final String CHAMP_EXP_ID                ="experience[id]";

	private static final String CHAMP_EXP_ENTREPRISE        ="experience[employer]";

	private static final String CHAMP_EXP_TITRE_POSTE       ="experience[jobtitle]";

	private static final String CHAMP_EXP_DATE_DEBUT        ="experience[from_date]";

	private static final String CHAMP_EXP_DATE_FIN          ="experience[to_date]";

	private static final String CHAMP_EXP_DESCRIPTION       ="experience[comments]";


	/*
	 * Formation 
	 */
	private static final String CHAMP_EDU_ID                ="education[id]";

	private static final String CHAMP_EDU_DOMAINE        ="education[code]";

	private static final String CHAMP_EDU_INSTITUT          ="education[institute]";

	private static final String CHAMP_EDU_SPECIALISATION    ="education[major]";

	private static final String CHAMP_EDU_DATE_DEBUT        ="education[start_date]";

	private static final String CHAMP_EDU_DATE_FIN          ="education[end_date]";

	private static final String CHAMP_EDU_ANNEE             ="education[year]";

	private static final String CHAMP_EDU_MOYENNE           ="education[gpa]";

	/*
	 * Competence 
	 */
	private static final String CHAMP_SKILL_ID              ="skill[id]";

	private static final String CHAMP_SKILL_NAME            ="skill[name]";

	private static final String CHAMP_SKILL_LIST            ="skill[list]";

	private static final String CHAMP_SKILL_YEARS           ="skill[years_of_exp]";

	/*
	 * Langue
	 */
	private static final String CHAMP_LAN_ID              ="language[id]";

	private static final String CHAMP_LAN_NAME            ="language[name]";

	private static final String CHAMP_LAN_FLUENCY        ="language[fluency]";

	private TitreCVDaoImpl    titreCVDao;
	private ExperienceDaoImpl workExpDao;
	private FormationDaoImpl  formationDao;
	private CompetenceDaoImpl competenceDao;
	private LangueDaoImpl     langueDao;
	private ResumeDaoImpl     resumeDao;



	public ResumeForm(ResumeDaoImpl resumeDao, TitreCVDaoImpl titreCVDao, ExperienceDaoImpl workExpDao,FormationDaoImpl formationDao,
			CompetenceDaoImpl competenceDao, LangueDaoImpl langueDao){
		super();
		this.titreCVDao = titreCVDao;
		this.workExpDao = workExpDao;
		this.formationDao = formationDao;
		this.competenceDao = competenceDao;
		this.langueDao = langueDao;
		this.resumeDao = resumeDao;
	}


	public Long creerResume(Long id_user){
		Resume resume = new Resume();
		resume.setId_user(id_user);
		resume.setDate(Calendar.getInstance().getTime());
		resumeDao.creer(resume);
		return resume.getId();
	}

	public Resume nouveauResume(Long id_user, String initiales){

		Resume resume = new Resume();
		TitreCV titreCV = new TitreCV();
		titreCV.setInitiales(initiales);

		resume.setId_user(id_user);
		resume.setDate(Calendar.getInstance().getTime());
		try{
			resumeDao.creer(resume);
			titreCV.setId_resume(resume.getId());
			titreCV.setNom("...");
			titreCV.setSous_titre("...");
			titreCVDao.creer(titreCV);
		}
		catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
		resume.setTitre(titreCV);
		return resume;
	}
	public void editerDate(Long id_resume){
		Resume resume = new Resume();
		resume.setId(id_resume);
		resume.setDate(Calendar.getInstance().getTime());
		resumeDao.modifier(resume);
	}

	public void sauverInitiales(TitreCV titreCV, Long id_resume, Long id_user){	
		titreCV.setId_resume(id_resume);
		titreCV.setNom("...");
		titreCV.setSous_titre("...");
		try{
			titreCVDao.creer(titreCV);
		}catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
	}

	public TitreCV sauverTitreCV(HttpServletRequest request, Long id_resume, Long id_user){
		TitreCV titreCV = new TitreCV();
		if(id_resume==0L) id_resume=creerResume(id_user);
		Long id= 0L;
		String idChamp = Constants.getValeurChamp(request,CHAMP_TITLE_ID);
		String initiales = Constants.getValeurChamp(request,CHAMP_TITLE_INITIAL);
		String nom = Constants.getValeurChamp(request,CHAMP_TITLE_NAME);
		String sous_titre = Constants.getValeurChamp(request,CHAMP_TITLE_SUB);
		if(idChamp!=null && idChamp.length()!=0) id = Long.parseLong( idChamp  );
		titreCV.setInitiales(initiales);
		titreCV.setNom(nom);
		titreCV.setSous_titre(sous_titre);
		titreCV.setId_resume(id_resume);
		try{
			if(id==0L) titreCVDao.creer(titreCV);
			else {
				titreCV.setId(id);
				titreCVDao.modifier(titreCV);
				editerDate(id_resume);
			}
		}
		catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
		return titreCV;
	}

	public Experience sauverExperience(HttpServletRequest request, Long id_resume, Long id_user){
		Experience workExp = new Experience();
		if(id_resume==0L) id_resume=creerResume(id_user);
		Long id=0L;
		String idChamp = Constants.getValeurChamp(request,CHAMP_EXP_ID);
		String entreprise = Constants.getValeurChamp(request,CHAMP_EXP_ENTREPRISE);
		String titre_poste = Constants.getValeurChamp(request,CHAMP_EXP_TITRE_POSTE);
		String date_debut = Constants.getValeurChamp(request,CHAMP_EXP_DATE_DEBUT);
		String date_fin = Constants.getValeurChamp(request,CHAMP_EXP_DATE_FIN);
		String description = Constants.getValeurChamp(request,CHAMP_EXP_DESCRIPTION);

		if(idChamp!=null && idChamp.length()!=0) id = Long.parseLong( idChamp  );
		workExp.setEntreprise(entreprise);
		workExp.setTitre_poste(titre_poste);
		traiterDate(date_debut);
		workExp.setDate_debut(date_debut);

		traiterDate(date_fin);
		workExp.setDate_fin(date_fin);

		workExp.setDescription(description);
		workExp.setId_resume(id_resume);

		try{
			if(id==0L)
				workExpDao.creer(workExp);

			else {
				workExpDao.modifier(workExp, id);
				editerDate(id_resume);
			}
		}
		catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
		return workExp; 
	}

	private void traiterDate( String date) {
		if ( !date.matches( "(0?[1-9]|1[012])/((19|20)\\d\\d)" ) )
			date="";

	}


	public Formation sauverFormation(HttpServletRequest request, Long id_resume, Long id_user){
		Formation formation = new Formation();
		if(id_resume==0L) id_resume=creerResume(id_user);
		Long id=0L;
		Long annee = null;
		String idChamp = Constants.getValeurChamp(request,CHAMP_EDU_ID);
		String domaine = Constants.getValeurChamp(request,CHAMP_EDU_DOMAINE);
		String institut = Constants.getValeurChamp(request,CHAMP_EDU_INSTITUT);
		String specialisation = Constants.getValeurChamp(request,CHAMP_EDU_SPECIALISATION);
		String moyenneChamp = Constants.getValeurChamp(request,CHAMP_EDU_MOYENNE);
		String anneeChamp =  Constants.getValeurChamp(request,CHAMP_EDU_ANNEE);
		String date_debut = Constants.getValeurChamp(request,CHAMP_EDU_DATE_DEBUT);
		String date_fin = Constants.getValeurChamp(request,CHAMP_EDU_DATE_FIN);
		if(idChamp!=null && idChamp.length()!=0) id = Long.parseLong( idChamp  );    
		if(anneeChamp!=null) annee = Long.parseLong( anneeChamp  );
		formation.setDomaine(domaine);
		formation.setInstitut(institut);
		formation.setSpecialisation(specialisation);
		formation.setMoyenne(moyenneChamp);
		formation.setAnnee(annee);

		formation.setDate_debut(date_debut);

		formation.setDate_fin(date_fin);

		formation.setId_resume(id_resume);
		try{
			if(id==0L)
				formationDao.creer(formation);

			else {
				formationDao.modifier(formation, id);
				editerDate(id_resume);
			}
		}
		catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
		return formation; 
	}

	public Competence sauverCompetence(HttpServletRequest request, Long id_resume, Long id_user){
		Competence competence = new Competence();
		if(id_resume==0L) id_resume=creerResume(id_user);
		Long id=0L;
		Double annee=null;
		String idChamp = Constants.getValeurChamp(request,CHAMP_SKILL_ID);
		String nom = Constants.getValeurChamp(request,CHAMP_SKILL_NAME);
		String liste = Constants.getValeurChamp(request,CHAMP_SKILL_LIST);
		String anneeChamp = Constants.getValeurChamp(request,CHAMP_SKILL_YEARS);
		if(idChamp!=null && idChamp.length()!=0) id = Long.parseLong( idChamp  );
		if(anneeChamp!=null) annee = Double.parseDouble(anneeChamp);

		competence.setNom(nom);
		competence.setListe(liste);
		competence.setAnnees_experience(annee);
		competence.setId_resume(id_resume);
		try{
			if(id==0L)
				competenceDao.creer(competence);

			else {
				competence.setId(id);
				competenceDao.modifier(competence);
				editerDate(id_resume);
			}
		}
		catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
		return competence;
	}

	public Langue sauverLangue(HttpServletRequest request, Long id_resume, Long id_user){
		Langue langue = new Langue();
		if(id_resume==0L) id_resume=creerResume(id_user);
		Long id=0L;
		String idChamp = Constants.getValeurChamp(request,CHAMP_LAN_ID);
		String nom = Constants.getValeurChamp(request,CHAMP_LAN_NAME);
		String aisance = Constants.getValeurChamp(request,CHAMP_LAN_FLUENCY);
		if(idChamp!=null && idChamp.length()!=0) id = Long.parseLong( idChamp  );


		langue.setNom(nom);
		langue.setAisance (aisance);
		langue.setId_resume(id_resume);
		try{
			if(id==0L)
				langueDao.creer(langue);

			else {
				langue.setId(id);
				langueDao.modifier(langue);
				editerDate(id_resume);
			}
		}
		catch(DAOException e){
			setErreur("imprevus", e.getMessage());
			e.printStackTrace();
		}
		return langue;
	}




	public List<Long> supprimerExperience(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String[ ] deleteIds = (String[ ])request.getParameterValues( "delWorkExp" );
		List<Long> idDelSuccess = new ArrayList<Long>();
		String idResume=Constants.getValeurChamp(request,"id_resume");
		Long id_resume= 0L;
		if(idResume!=null) id_resume = Long.parseLong( idResume );          
		if(deleteIds!=null){
			try{
				for (String idChamp : deleteIds) {
					Long id = Long.parseLong(idChamp);
					workExpDao.supprimer(id);
					idDelSuccess.add(id);
					editerDate(id_resume);
				}
			}catch(DAOException e){
				setErreur("imprevus", e.getMessage());
				e.printStackTrace();
			}
		}
		return idDelSuccess;
	}

	public List<Long> supprimerFormation(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String[ ] deleteIds = (String[ ])request.getParameterValues( "delEdu" );
		List<Long> idDelSuccess = new ArrayList<Long>();
		String idResume=Constants.getValeurChamp(request,"id_resume");
		Long id_resume= 0L;
		if(idResume!=null) id_resume = Long.parseLong( idResume );  
		if(deleteIds!=null){
			try{
				for (String idChamp : deleteIds) {
					Long id = Long.parseLong(idChamp);
					formationDao.supprimer(id);
					idDelSuccess.add(id);
					editerDate(id_resume);
				}
			}catch(DAOException e){
				setErreur("imprevus", e.getMessage());
				e.printStackTrace();
			}
		}
		return idDelSuccess;
	}

	public List<Long> supprimerCompetence(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String[ ] deleteIds = (String[ ])request.getParameterValues( "delSkill" );
		List<Long> idDelSuccess = new ArrayList<Long>();
		String idResume=Constants.getValeurChamp(request,"id_resume");
		Long id_resume= 0L;
		if(idResume!=null) id_resume = Long.parseLong( idResume );  
		if(deleteIds!=null){
			try{
				for (String idChamp : deleteIds) {
					Long id = Long.parseLong(idChamp);
					competenceDao.supprimer(id);
					idDelSuccess.add(id);
					editerDate(id_resume);
				}
			}catch(DAOException e){
				setErreur("imprevus", e.getMessage());
				e.printStackTrace();
			}
		}
		return idDelSuccess;
	}

	public List<Long> supprimerLangue(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String[ ] deleteIds = (String[ ])request.getParameterValues( "delLang" );
		List<Long> idDelSuccess = new ArrayList<Long>();
		String idResume=Constants.getValeurChamp(request,"id_resume");
		Long id_resume= 0L;
		if(idResume!=null) id_resume = Long.parseLong( idResume );  
		if(deleteIds!=null){
			try{
				for (String idChamp : deleteIds) {
					Long id = Long.parseLong(idChamp);
					langueDao.supprimer(id);
					idDelSuccess.add(id);
					editerDate(id_resume);
				}
			}catch(DAOException e){
				setErreur("imprevus", e.getMessage());
				e.printStackTrace();
			}
		}
		return idDelSuccess;

	}

}
