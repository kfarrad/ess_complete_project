package com.clinkast.cra.forms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Competences;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Rapports;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.CompetencesDAOImpl;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.RapportsDAOImpl;

public class ExigencesForm extends AbstractForm{
	public static final String CHAMP_ID = "rapport[id_auto]";
	public static final String ATT_CAT_CONSULTANT = "Metier du consultant";
	public static final String ATT_CAT_CLIENT = "Client";
	public static final String ATT_CAT_ENGAGEMENT = "Engagement et Qualités Individuelles";
	public static final String ATT_CAT_DEVELOPPEMENT = "Développement des Hommes et Management";
	
	private CompetencesDAOImpl competencesDAOImpl;
	private RapportsDAOImpl rapportsDAOImpl;
	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public ExigencesForm(GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl, AutoEvaluationDAOImpl autoEvaluationDAOImpl, CompetencesDAOImpl competencesDAOImpl, RapportsDAOImpl rapportsDAOImpl){
		super();
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
		this.competencesDAOImpl = competencesDAOImpl;
		this.rapportsDAOImpl = rapportsDAOImpl;
	}
	
	public GestionEvaluations modifier(HttpServletRequest request){
		/*Lire les champs de request*/
		List<Competences> competences = null;
		String parametre = null;
		Long autoEvaluationId = Long.parseLong(Constants.getValeurChamp(request, CHAMP_ID));
		String categorie = Constants.getValeurChamp(request, "rapport[cat]");
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;		
		
		
		//Equivalence de la valeur passée en parametre
		parametre = (categorie.equals("Ctt") ? ATT_CAT_CONSULTANT : (categorie.equals("Clt") ? ATT_CAT_CLIENT : (categorie.equals("Eng")? ATT_CAT_ENGAGEMENT : (categorie.equals("Dev") ? ATT_CAT_DEVELOPPEMENT : null))));
		
		if(parametre != null)
		{
			try{
				competences = competencesDAOImpl.lister(parametre);
				int compteur = 0;//compte le nombre de lignes modifiés en base
				
				/*Ayant le nombre de competences par axes, je vais une boucle, le premier indice du tableau rapport envoyé par le formulaire correspond à la 1ere ligne du tableau de competences par categorie et ainsi de suite*/
				for(Competences competence : competences){
					
					boolean update = false;
					Rapports rapport = rapportsDAOImpl.trouver_By_ID(competence.getCompetencesId(), autoEvaluationId);
					String rapport_niveau = Constants.getValeurChamp(request, "rapport["+competence.getCompetencesId()+"][niveau]");
					String rapport_commentaire = Constants.getValeurChamp(request, "rapport["+competence.getCompetencesId()+"][commanetaire]");
					
					rapport.setAutoEvaluations(autoEvaluationDAOImpl.trouver(autoEvaluationId));
					rapport.setCompetences(competence);
					//Pour limiter les access `a base, on teste si les valeurs envoyées par le formulaire sont different à ceux deja en base
					//si different, alors mise à jour
					if((rapport_commentaire!=null && !rapport_commentaire.equals((rapport.getRapportsCommentaire()))) || (rapport_commentaire==null && rapport.getRapportsCommentaire() !=null)){
						rapport.setRapportsCommentaire(rapport_commentaire);
						update = true;
					}
					if((rapport_niveau!=null && !rapport_niveau.equals((rapport.getRapportsNiveau()))) || (rapport_niveau == null && rapport.getRapportsNiveau() != null))
					{
						rapport.setRapportsNiveau(rapport_niveau);
						update = true;
					}
					
					if(update == true){
						compteur ++;
						rapportsDAOImpl.modifier(rapport);						
					}
					rapport = null;							
				}
				if(compteur > 0)
				{									
					resultat = compteur + (compteur >= 2 ? " competences ont " : " compétence a ") + "été modifé";
				}
				gestionEvaluations = updateGestionAutoEvaluation(session, autoEvaluationId);
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();						
			}
		}
		return gestionEvaluations;
	}
	
	private GestionEvaluations updateGestionAutoEvaluation(HttpSession session, Long evaluationId){

		GestionEvaluations gestionEvaluations = null;
		Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		
		//Mis à jour de gestionAutoEvaluation
		try{		
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(evaluationId, utilisateur.getId());
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime());
			gestionEvaluations.setGestionEvaluationsNbreModification(gestionEvaluations.getGestionEvaluationsNbreModification()+1);
			gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
		}
		catch (Exception e) {
			// TODO: handle exception
			setErreur("imprevu dans GestionEvaluationDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}
		return gestionEvaluations;
	}
	
}
