package com.clinkast.cra.forms;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Fichier;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.FichierDaoImpl;

public class UploadForm extends AbstractForm{
        private static final String CHAMP_DESCRIPTION = "description";
        private static final String CHAMP_FICHIER     = "fichier";
        private static final String CHAMP_NOM         = "nom";
        private static final int    TAILLE_TAMPON     = 10240;                        // 10 ko
        private FichierDaoImpl                        fichierDao;
        
        
        public UploadForm(FichierDaoImpl fichierDao){
        	super();
                this.fichierDao = fichierDao;
        }
        
        public Fichier creerFichier( HttpServletRequest request, String chemin ) {
                Fichier fichier = new Fichier();
                
                /* Récupération du champ de description du formulaire */
                String description = Constants.getValeurChamp( request, CHAMP_DESCRIPTION );
                
                traiterDescription(description, fichier);
                String nomFichier =traiterFichier(request, chemin, fichier);
                
                
                /* Initialisation du résultat global de la validation. */
                if ( erreurs.isEmpty() ) {
                        Fichier monFichier = fichierDao.trouver(nomFichier);
                        if(monFichier==null){
                                fichierDao.creer( fichier);
                                resultat = "Succès de la création du projet.";
                        }
                        else
                        {
                                setErreur( CHAMP_FICHIER, "fichier déjà existant" );
                        }
                        resultat = "Succès de l'envoi du fichier.";
                } else {
                        resultat = "Échec de l'envoi du fichier.";
                }
                
                return fichier;
        }
        
        public Fichier modifierNomFichier(HttpServletRequest request, Long id, String chemin)
        {
            String description = Constants.getValeurChamp( request, CHAMP_DESCRIPTION );   
            String nom = Constants.getValeurChamp( request, CHAMP_NOM );
            Fichier fichier = new Fichier();
            traiterDescription(description, fichier);
            traiterNom(nom, fichier, chemin);
            try {
                if ( erreurs.isEmpty() ) {
                    Fichier monFichier= fichierDao.trouver(id);
                    File oldfile =new File(chemin + File.separator + monFichier.getNom());
                    File newfile =new File(chemin + File.separator + nom);
                 
                        if(oldfile.renameTo(newfile)){
                            fichierDao.modifier(fichier, id);
                            resultat = "Succès de la modification du nom du fichier.";
                        }else{
                            resultat = "Echec de la modification du nom du fichier.";
                        }
                    
                }
            }catch ( DAOException e ) {
                setErreur( "imprévu", "Erreur imprévue lors de la modification." );
                resultat ="Échec de la modification du projet : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
                e.printStackTrace();
            }


            return fichier;
        }
        
        private void traiterDescription( String description, Fichier fichier ){
                try {
                        validationDescription( description );
                } catch ( Exception e ) {
                        setErreur( CHAMP_DESCRIPTION, e.getMessage() );
                }
                fichier.setDescription( description );
        }
        
        private void traiterNom( String nom, Fichier fichier,String chemin ){
                try {
                        validationNom( nom );
                } catch ( Exception e ) {
                        setErreur( CHAMP_NOM, e.getMessage() );
                }
                fichier.setNom( nom );
        }
        private String traiterFichier( HttpServletRequest request, String chemin, Fichier fichier ) {
                
                /*
                 * Récupération du contenu du champ fichier du formulaire. Il faut ici
                 * utiliser la méthode getPart(), comme nous l'avions fait dans notre
                 * servlet auparavant.
                 */
                String nomFichier = null;
                InputStream contenuFichier = null;
                try {
                        Part part = request.getPart( CHAMP_FICHIER );
                        nomFichier = getNomFichier( part );
                        
                        /*
                         * Si la méthode a renvoyé quelque chose, il s'agit donc d'un
                         * champ de type fichier (input type="file").
                         */
                        if ( nomFichier != null && !nomFichier.isEmpty() ) {
                                /*
                                 * Antibug pour Internet Explorer, qui transmet pour une
                                 * raison mystique le chemin du fichier local à la machine
                                 * du client...
                                 * 
                                 * Ex : C:/dossier/sous-dossier/fichier.ext
                                 * 
                                 * On doit donc faire en sorte de ne sélectionner que le nom
                                 * et l'extension du fichier, et de se débarrasser du
                                 * superflu.
                                 */
                                nomFichier = nomFichier.substring( nomFichier.lastIndexOf( '/' ) + 1 )
                                                .substring( nomFichier.lastIndexOf( '\\' ) + 1 );
                                
                                /* Récupération du contenu du fichier */
                                contenuFichier = part.getInputStream();
                                
                        }
                } catch ( IllegalStateException e ) {
                        /*
                         * Exception retournée si la taille des données dépasse les limites
                         * définies dans la section <multipart-config> de la déclaration de
                         * notre servlet d'upload dans le fichier web.xml
                         */
                        e.printStackTrace();
                        setErreur( CHAMP_FICHIER, "Les données envoyées sont trop volumineuses." );
                } catch ( IOException e ) {
                        /*
                         * Exception retournée si une erreur au niveau des répertoires de
                         * stockage survient (répertoire inexistant, droits d'accès
                         * insuffisants, etc.)
                         */
                        e.printStackTrace();
                        setErreur( CHAMP_FICHIER, "Erreur de configuration du serveur." );
                } catch ( ServletException e ) {
                        /*
                         * Exception retournée si la requête n'est pas de type
                         * multipart/form-data. Cela ne peut arriver que si l'utilisateur
                         * essaie de contacter la servlet d'upload par un formulaire
                         * différent de celui qu'on lui propose... pirate ! :|
                         */
                        e.printStackTrace();
                        setErreur( CHAMP_FICHIER,
                                        "Ce type de requête n'est pas supporté, merci d'utiliser le formulaire prévu pour envoyer votre fichier." );
                }
                
                /* Validation du champ fichier. */
                try {
                        validationFichier(nomFichier, contenuFichier);
                } catch ( Exception e ) {
                        setErreur( CHAMP_FICHIER, e.getMessage() );
                }
                fichier.setNom( nomFichier );
                
                /* Si aucune erreur n'est survenue jusqu'à présent */
                if ( erreurs.isEmpty() ) {
                        /* Écriture du fichier sur le disque */ 
                        try {
                                ecrireFichier( contenuFichier, nomFichier, chemin );
                        } catch ( Exception e ) {
                                setErreur( CHAMP_FICHIER, "Erreur lors de l'écriture du fichier sur le disque." );
                        }
                }
                return nomFichier;
        }
        
        
        
        /*
         * Valide la description saisie.
         */
        private void validationDescription( String description ) throws Exception {
                if ( description != null ) {
                        if ( description.length() < 15 ) {
                                throw new Exception( "La phrase de description du fichier doit contenir au moins 15 caractères." );
                        }
                } else {
                        throw new Exception( "Merci d'entrer une phrase de description du fichier." );
                }
        }
        
        private void validationNom( String nom ) throws Exception {
                if ( nom != null ) {
                        if ( nom.length() < 15 ) {
                                throw new Exception( "Le nom du fichier doit contenir au moins 15 caractères." );
                        }
                        
                        if ( !nom.matches( "^[a-zA-Z_\\.0-9]$" ) ) {
                                throw new FormValidationException( "Merci de saisir un nom sans cractères spéciaux." );
                            }
                }
        }
        
        /*
         * Valide le fichier envoyé.
         */
        private void validationFichier( String nomFichier, InputStream contenuFichier ) throws Exception {
                if ( nomFichier == null || contenuFichier == null ) {
                        throw new Exception( "Merci de sélectionner un fichier à envoyer." );
                }
        }
        
                  
        
        /*
         * Méthode utilitaire qui a pour unique but d'analyser l'en-tête
         * "content-disposition", et de vérifier si le paramètre "filename" y est
         * présent. Si oui, alors le champ traité est de type File et la méthode
         * retourne son nom, sinon il s'agit d'un champ de formulaire classique et
         * la méthode retourne null.
         */
        private static String getNomFichier( Part part ) {
                /* Boucle sur chacun des paramètres de l'en-tête "content-disposition". */
                for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
                        /* Recherche de l'éventuelle présence du paramètre "filename". */
                        if ( contentDisposition.trim().startsWith( "filename" ) ) {
                                /*
                                 * Si "filename" est présent, alors renvoi de sa valeur,
                                 * c'est-à-dire du nom de fichier sans guillemets.
                                 */
                                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
                        }
                }
                /* Et pour terminer, si rien n'a été trouvé... */
                return null;
        }
        
        /*
         * Méthode utilitaire qui a pour but d'écrire le fichier passé en paramètre
         * sur le disque, dans le répertoire donné et avec le nom donné.
         */
        private void ecrireFichier( InputStream contenu, String nomFichier, String chemin ) throws Exception {
                /* Prépare les flux. */
                BufferedInputStream entree = null;
                BufferedOutputStream sortie = null;
                try {
                        /* Ouvre les flux. */
                        entree = new BufferedInputStream( contenu, TAILLE_TAMPON );
                        sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + File.separator + nomFichier ) ),
                                        TAILLE_TAMPON );
                        
                        /*
                         * Lit le fichier reçu et écrit son contenu dans un fichier sur le
                         * disque.
                         */
                        byte[] tampon = new byte[TAILLE_TAMPON];
                        int longueur = 0;
                        while ( ( longueur = entree.read( tampon ) ) > 0 ) {
                                sortie.write( tampon, 0, longueur );
                        }
                } finally {
                        try {
                                sortie.close();
                        } catch ( IOException ignore ) {
                        }
                        try {
                                entree.close();
                        } catch ( IOException ignore ) {
                        }
                }
        }
}