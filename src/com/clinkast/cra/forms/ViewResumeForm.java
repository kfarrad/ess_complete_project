package com.clinkast.cra.forms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.dao.ResumeDaoImpl;

public class ViewResumeForm extends AbstractForm{

	private static final String PARAM_ID_RESUME = "idResume";

	private ResumeDaoImpl resumeDao;

	public ViewResumeForm(ResumeDaoImpl resumeDao){
		super();
		this.resumeDao = resumeDao;
	}

	public void CreerPdf(HttpServletRequest request, Long id_user, String nom){

		String idChamp=request.getParameter(PARAM_ID_RESUME);
		Long id_resume= 0L;
		if(idChamp!=null) id_resume = Long.parseLong( idChamp );                

		try {
			String chemin = Constants.getCheminUpload();
			if(resumeDao.verifier(id_resume,id_user)){
				String HTML_TO_PDF = nom +"_"+ id_user+"_"+ id_resume +".pdf";
				OutputStream os = new FileOutputStream(chemin + File.separator + HTML_TO_PDF);       
				ITextRenderer renderer = new ITextRenderer();
				//Document document = XMLResource. load(new ByteArrayInputStream(outputStream.getBytes())).getDocument();


				renderer.setDocument(Constants.getUrlAppli()+ "/viewResume?id=" + id_resume);      
				renderer.layout();
				renderer.createPDF(os, true, 1);
				renderer.finishPDF();  
				os.close();  
			}
			else setErreur( "imprévu", "Erreur imprévue lors de la Creation." );
		}catch ( Exception e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la Creation." );
			resultat ="Échec de la Création du CV : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}

	}
}