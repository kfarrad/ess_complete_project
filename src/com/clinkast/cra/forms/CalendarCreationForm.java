package com.clinkast.cra.forms;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Calendrier;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Projet;
import com.clinkast.cra.dao.CalendarDaoImpl;
import com.clinkast.cra.dao.DAOException;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarCreationForm.
 */
public class CalendarCreationForm extends AbstractForm{

	/** The Constant CHAMP_PROJET. */
	private static final String CHAMP_PROJET			= "projet_";

	/** The Constant CHAMP_ANNEE. */
	private static final String CHAMP_ANNEE 			= "annee";

	/** The Constant CHAMP_MOIS. */
	private static final String CHAMP_MOIS	 			= "mois";

	/** The calendar dao. */
	private CalendarDaoImpl           calendarDao;

	/**
	 * Instantiates a new calendar creation form.
	 *
	 * @param calendarDao the calendar dao
	 */
	public CalendarCreationForm( CalendarDaoImpl calendarDao ) {
		super();
		this.calendarDao = calendarDao;
	}

	/**
	 * Sets the status calendar.
	 *
	 * @param id_user the id_user
	 * @param projets the projets
	 * @param annee the annee
	 * @param mois the mois
	 * @param status the status
	 */
	public void setStatusCalendar (Long id_user, List<Projet> projets, Long annee, Long mois, Long status){
		for(int j=0; j<projets.size(); j++){

			calendarDao.setStatus(id_user, projets.get(j).getId(), annee, mois, status);
		}
	}



	/**
	 * Creer calendar.
	 *
	 * @param request the request
	 * @param id_user the id_user
	 * @param projets the projets
	 * @return the list
	 */
	public List<Calendrier> creerCalendar(HttpServletRequest request, Long id_user, List<Projet> projets){

		List<Calendrier> listeCalendar= new ArrayList<Calendrier>();

		for(int j=1; j<=projets.size(); j++){
			Calendrier calendrier= new Calendrier();
			List<Float> jours= new ArrayList<Float>();

			Long id_projet = projets.get(j-1).getId();			
			//traitemnt annee et mois
			String anneeJ = Constants.getValeurChamp(request, CHAMP_ANNEE);			
			String moisJ = Constants.getValeurChamp(request, CHAMP_MOIS);

			Long annee =traiterAnnee(anneeJ, calendrier);
			Long mois = traiterMois(moisJ,calendrier);	

			// recuperation des id user et projet
			calendrier.setId_user(id_user);
			calendrier.setId_projet(id_projet);

			//traitement des jours du calendrier par projet
			traiterJours(request, jours, calendrier,j);

			//verifie que l'annee et le mois sont au format Long
			if((annee != 0L)&(mois != null)){
				try{
					//Si le calendrier n'existe pas, alors on le crée
					if(calendarDao.trouver(id_user, id_projet, annee, mois)== null) 
					{
						calendarDao.creer(calendrier);

					}
					else {
						calendarDao.modifier(calendrier, id_user, id_projet, annee, mois);
					}
					listeCalendar.add(j-1,calendrier);
				}
				catch(DAOException e){
					setErreur("imprevus", e.getMessage());
					e.printStackTrace();
				}
			}
		}

		return listeCalendar;
	}

	/**
	 * @param listeCalendar cra du mois
	 * @return liste de jours pour les Congés payés
	 */
	public List<Float> getJoursCP(List<Calendrier> listeCalendrier, List<Projet> listeProjets){
		Long id_projet = null;

		for (Projet projet : listeProjets){
			if(projet.getNom().contains("CP")){
				id_projet= projet.getId();
				break;
			}
		}

		for (int j = 0; j < listeCalendrier.size(); j++){
			if(listeCalendrier.get(j).getId_projet()==id_projet) return listeCalendrier.get(j).getJours();

		}

		return null;
	}
	public List<Integer> joursFerier(HttpServletRequest request){
		List<Integer> feriers = new ArrayList<Integer>();
		List<Float> jours = new ArrayList<Float>();
		for(int i=0;i<31;i++) jours.add(i,0F);
		String[ ] joursFeriers = (String[ ])request.getParameterValues( "holiday" );
		//traitemnt annee et mois
		String anneeJ = Constants.getValeurChamp(request, CHAMP_ANNEE);			
		String moisJ = Constants.getValeurChamp(request, CHAMP_MOIS);

		Calendrier calendrier= new Calendrier();
		Long annee =traiterAnnee(anneeJ, calendrier);
		Long mois = traiterMois(moisJ,calendrier);	
		if(joursFeriers!=null){
			try{

				for (String jour : joursFeriers) {
					feriers.add(Integer.parseInt(jour));
					jours.add(Integer.parseInt(jour)-1,-1F);
				}

			}
			catch(DAOException e){
				setErreur("imprevus", e.getMessage());
				e.printStackTrace();
			}

		}

		Calendrier unCalendrier= calendarDao.trouver(-1L, -1L, annee, mois);
		if(unCalendrier==null) calendarDao.setFerier(-1L, -1L, annee, mois, jours);
		else calendarDao.updateFerier(unCalendrier.getId(), jours);
		resultat="les jours fériés ont bien été définis";
		return feriers;
	}



	/**
	 * Traiter annee.
	 *
	 * @param anneeJ the annee j
	 * @param calendrier the calendrier
	 * @return the long
	 */
	private Long traiterAnnee( String anneeJ, Calendrier calendrier ) {
		Long annee = 0L;
		if(anneeJ!=null){
			annee=Long.parseLong(anneeJ);
			calendrier.setAnnee(annee);
		}
		return annee;
	}

	/**
	 * Traiter mois.
	 *
	 * @param moisJ the mois j
	 * @param calendrier the calendrier
	 * @return the long
	 */
	private Long traiterMois( String moisJ, Calendrier calendrier ) {
		Long mois = 0L;
		if(moisJ!=null){
			mois=Long.parseLong(moisJ);
			calendrier.setMois(mois);
		}
		return mois;
	}

	/**
	 * Traiter jours.
	 *
	 * @param request the request
	 * @param jours the jours
	 * @param calendrier the calendrier
	 * @param j the j
	 */
	private void traiterJours( HttpServletRequest request, List<Float> jours, Calendrier calendrier, int j) {
		Double total_jour = 0.0;
		for(int i=1; i<=31; i++){
			String projetIJ = Constants.getValeurChamp( request, CHAMP_PROJET+ i+ "_"+ j );

			if(projetIJ!=null){
				jours.add(i-1,Float.parseFloat(projetIJ));
				total_jour+=Float.parseFloat(projetIJ);
			}
			else 
				jours.add(i-1,0.0F);

		}

		calendrier.setTotal_jour(total_jour);
		calendrier.setJours(jours);
	}

}
