package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.FormationsDemandes;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.FormationsDemandesDAOImp;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;

public class FormationDemandesForm extends AbstractForm{
	/*	Constantes */
	//public static final String PARAM_ID = "id";
	public static final String CHAMP_ID = "formationDemandes[id_auto]";
	public static final String CHAMP_FDD_INTITULE = "formationDemandes[intitule]";
	public static final String CHAMP_FDD_OBJECTIF = "formationDemandes[objectif]";
	public static final String CHAMP_FDD_ID = "formationDemandes[id_formationDemande]";
	public static final String CHAMP_TAB_FDD_ID = "formationDemande_id_formations";
	
	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private FormationsDemandesDAOImp formationsDemandesDAOImp;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public FormationDemandesForm(GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl, AutoEvaluationDAOImpl autoEvaluationDAOImpl, FormationsDemandesDAOImp formationsDemandesDAOImp){
		super();
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
		this.formationsDemandesDAOImp = formationsDemandesDAOImp;
	}

	public GestionEvaluations sauveFormationDemandes(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String fdd_intitule = Constants.getValeurChamp(request, CHAMP_FDD_INTITULE);
		String fdd_objectif = Constants.getValeurChamp(request, CHAMP_FDD_OBJECTIF);
		String fdd_id = Constants.getValeurChamp(request, CHAMP_FDD_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;
		
		FormationsDemandes formationsDemandes = new FormationsDemandes();
		
		//Convertir les id en Long
		Long evaluationId = Long.parseLong(autoEvaluationId);
		Long formationDemande_id = Long.parseLong(fdd_id);
		formationsDemandes.setAutoEvaluations(autoEvaluationDAOImpl.trouver(evaluationId));
		formationsDemandes.setFormationsDemandesLibelle(fdd_intitule);
		formationsDemandes.setFormationsDemandesObjectifs(fdd_objectif);
				
		try{
			//Si l'id n'existe pas alors on cree l'element
			if(formationDemande_id == 0){
				//nouvelle activité
				formationsDemandesDAOImp.creer(formationsDemandes);
				resultat = "La formation demandée a été créé";
			}
			else if(formationDemande_id != null){
				//on ajoute id
				formationsDemandes.setFormationsDemandesId(formationDemande_id);
				//on modifie les attributs de l'activité
				formationsDemandesDAOImp.modifier(formationsDemandes);
				resultat = "La formation demandée a été modifié";
			}
			gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
			
			
		}
		catch (DAOException e) {
			// TODO: handle exception
			setErreur("imprevu dans formationsDemandesDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}		
		return gestionEvaluations;
	}
	
	/*Supprimer une activité*/
	public GestionEvaluations supprimeFormationDemandes(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String[] deleteId = (String[])request.getParameterValues( CHAMP_TAB_FDD_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;		
		Long evaluationId = Long.parseLong(autoEvaluationId);
		int compteur = 0;
		
		if(deleteId != null)
		{
			try{
				for(String id : deleteId){
					Long fdd_id = Long.parseLong(id);
					FormationsDemandes formationsDemandes = formationsDemandesDAOImp.trouver(fdd_id);
					formationsDemandesDAOImp.supprimer(formationsDemandes);
					compteur ++;
				}
				gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
				
				resultat = compteur + (compteur >= 2 ? " formations demandées ont " : " formation demandé a ") + "été supprimé";
			}
			catch (DAOException e) {
				// TODO: handle exception
				setErreur("imprevu dans FormationsDemandesDAOImpl: ", e.getMessage());
				resultat = e.getMessage();
			}
		}
		return gestionEvaluations;
	}
	
	
	private GestionEvaluations updateGestionAutoEvaluation(HttpSession session, Long evaluationId){

		GestionEvaluations gestionEvaluations = null;
		Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		
		//Mis à jour de gestionAutoEvaluation
		try{		
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(evaluationId, utilisateur.getId());
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime());
			gestionEvaluations.setGestionEvaluationsNbreModification(gestionEvaluations.getGestionEvaluationsNbreModification()+1);
			gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
		}
		catch (Exception e) {
			// TODO: handle exception
			setErreur("imprevu dans GestionEvaluationDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}
		return gestionEvaluations;
	}


}
