package com.clinkast.cra.forms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.AutoEvaluations;
import com.clinkast.cra.beans.Competences;
import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Dates;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Rapports;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.CompetencesDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.DatesDAOImpl;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;
import com.clinkast.cra.dao.RapportsDAOImpl;
import com.clinkast.cra.dao.UserDaoImpl;

public class FormulaireTypeForm extends AbstractForm {
	public static final String CHAMP_ANNEE = "annee";
	public static final String CHAMP_PERIODE_DE = "periode_de";
	public static final String CHAMP_PERIODE_AU = "periode_au";
	public static final String CHAMP_DATE_ID = "dates_id";
	public static final String IMPREVU = "imprévu";

	/* FormulaireTypeDAO */
	private DatesDAOImpl datesDAOImpl;
	private UserDaoImpl userDaoImpl;
	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	private CompetencesDAOImpl competencesDAOImpl;
	private RapportsDAOImpl rapportsDAOImpl;

	public FormulaireTypeForm(DatesDAOImpl datesDAOImpl,
			UserDaoImpl userDaoImpl,
			AutoEvaluationDAOImpl autoEvaluationDAOImpl,
			GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl,
			CompetencesDAOImpl competencesDAOImpl,
			RapportsDAOImpl rapportsDAOImpl) {
		super();
		this.datesDAOImpl = datesDAOImpl;
		this.userDaoImpl = userDaoImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.competencesDAOImpl = competencesDAOImpl;
		this.rapportsDAOImpl = rapportsDAOImpl;
	}

	public Dates creerDates(HttpServletRequest request) {
		String annee = Constants.getValeurChamp(request, CHAMP_ANNEE);
		String periode_de = Constants.getValeurChamp(request, CHAMP_PERIODE_DE);
		String periode_au = Constants.getValeurChamp(request, CHAMP_PERIODE_AU);

		String periode = periode_de.trim() + " au " + periode_au.trim();
		Dates dates = new Dates();
		dates.setDatesPeriodes(periode);
		dates.setDatesPeriodesDe(periode_de);
		dates.setDatesPeriodesAu(periode_au);
		dates.setDatesAnnee(annee);
		// dates.setDatesCreation(new Date());
		dates.setDatesCreation(new DateTime());
		if (!datesDAOImpl.isExist(dates)) {
			try {
				if (erreurs.isEmpty()) {
					datesDAOImpl.creer(dates);
					resultat = "Une periode a été créée avec succès";
				} else {
					resultat = "Echec de création de la periode";
				}
			} catch (DAOException e) {
				setErreur("imprévu", "Erreur imprévue lors de la création.");
				resultat = "Échec de la création de la date : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
				e.printStackTrace();
			}
		} else {
			setErreur("Erreur", "La période existe déja.");
			resultat = "La période existe déja.";
		}

		return dates;
	}

	/**
	 * Processus qui active le formulaire créé chez tous les consultants
	 * 
	 * @param request
	 * @return
	 */
	public Dates envoyerDates(HttpServletRequest request) {
		/* Recupere la valeur du champ et converti en Long */
		String id = Constants.getValeurChamp(request, CHAMP_DATE_ID);
		Long dates_id = 0L;
		Dates dates = null;
		// compteur de formulaire créé et envoyé
		int compteur = 0;
		dates_id = Long.parseLong(id);
		try {

			// retrouve l'objet en base à partir de son id
			dates = datesDAOImpl.trouver(dates_id);
			dates.setDatesEnvoi(new DateTime());

			// Liste tous les CONSULTANTS
			List<Utilisateur> utilisateurs = userDaoImpl.lister_USER();

			/* Pour chaque consultant */
			for (Utilisateur user : utilisateurs) {
				// crée un formulaire d'auto-evaluation
				AutoEvaluations autoEvaluations = new AutoEvaluations();
				autoEvaluations = autoEvaluationDAOImpl.creer(autoEvaluations);

				/*
				 * Attribuer la table de competences par axes à chacun
				 * autoevaluation ce processus peut etre optimiser, ou le faire
				 * par triggers
				 */
				// Lister toutes la table competences
				List<Competences> listCompetences = competencesDAOImpl.lister();
				// Associe chaque competence à l'auto-evaluation par la liaison
				// Rapport
				for (Competences competences : listCompetences) {
					Rapports rapport = new Rapports(competences,
							autoEvaluations);
					rapportsDAOImpl.creer(rapport);
					rapport = null;
				}

				// Mise en relation du formulaire avec la date et le consultant
				GestionEvaluations gestionEvaluations = new GestionEvaluations();
				gestionEvaluations.setAutoEvaluations(autoEvaluations);
				gestionEvaluations.setDates(dates);
				gestionEvaluations.setUser(user);
				gestionEvaluations
						.setGestionEvaluationsDateReception(new DateTime());
				gestionEvaluations.setGestionEvaluationsNbreModification(0);
				// save
				gestionEvaluationsDAOImpl.creer(gestionEvaluations);
				System.out.println(gestionEvaluations.getUser().getProfil()
						.getNom());
				// compteur
				compteur++;
			}
			// Tous les formulaires sont créés et envoyés au consultant, sauve
			// l'objet dates
			datesDAOImpl.modifier(dates);
			resultat = compteur
					+ " formulaires d'auto_evaluation pour la periode du "
					+ dates.getDatesPeriodes() + " ont été créés et envoyés";
		} catch (DAOException e) {
			// TODO: handle exception
			setErreur(IMPREVU,
					"Erreur imprévue lors du processus de creation et d'envoi des formulaire "
							+ e.getMessage());
			resultat = "Echec lors de la creation et l'envoi des formulaires: une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
		}
		return dates;
	}

}
