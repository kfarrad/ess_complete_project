package com.clinkast.cra.forms;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Role;
import com.clinkast.cra.beans.UserProfil;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.UserDaoImpl;



// TODO: Auto-generated Javadoc
/**
 * The Class UserCreationForm.
 */
public class UserCreationForm extends AbstractForm{

	/** The Constant CHAMP_LOGIN. */
	private static final String CHAMP_LOGIN	     	= "login";

	/** The Constant CHAMP_OLD_PASSWORD. */
	private static final String CHAMP_OLD_PASSWORD 	= "ancienmotdepasse";

	/** The Constant CHAMP_PASSWORD. */
	private static final String CHAMP_PASSWORD  	= "motdepasse";

	/** The Constant CHAMP_CONFIRMATION. */
	private static final String CHAMP_CONFIRMATION  = "confirmation";

	/** The Constant CHAMP_ROLE. */
	private static final String CHAMP_ROLE  		= "typeCompte";

	/** The user dao. */
	private UserDaoImpl           userDao;


	/**
	 * Instantiates a new user creation form.
	 *
	 * @param userDao the user dao
	 */
	public UserCreationForm( UserDaoImpl userDao ) {
		super();
		this.userDao = userDao;
	}

	/**
	 * Gets the erreurs.
	 *
	 * @return the erreurs
	 */
	public Map<String, String> getErreurs() {
		return erreurs;
	}

	/**
	 * Gets the resultat.
	 *
	 * @return the resultat
	 */
	public String getResultat() {
		return resultat;
	}

	/**
	 * Modifier.
	 *
	 * @param request the request
	 * @param id the id
	 * @return the utilisateur
	 */
	public Utilisateur modifier(HttpServletRequest request, Long id)
	{
		String ancienMotDePasse = Constants.getValeurChamp ( request, CHAMP_OLD_PASSWORD );
		String motDePasse = Constants.getValeurChamp( request, CHAMP_PASSWORD );
		String confirmation = Constants.getValeurChamp( request, CHAMP_CONFIRMATION );



		Utilisateur utilisateur = new Utilisateur();
		Utilisateur clientModifier = new Utilisateur();
		traiterAncienMotDePasse( ancienMotDePasse,  id );
		traiterMotsDePasse( motDePasse, confirmation, utilisateur );

		try {
			if ( erreurs.isEmpty() ) {
				utilisateur.setLogin(userDao.trouver(id).getLogin());
				userDao.modifierMotDePasse(utilisateur, id);
				clientModifier = userDao.trouver(id);
				resultat = "Succès de la modification du mot de passe.";
			}
		}catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la modification." );
			resultat ="Échec de la modification de l'utilisateur : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}

		return clientModifier;
	}

	/**
	 * Modifier admin.
	 *
	 * @param request the request
	 * @param id the id
	 * @return the utilisateur
	 */
	public Utilisateur modifierAdmin(HttpServletRequest request, Long id)
	{
		String motDePasse = Constants.getValeurChamp( request, CHAMP_PASSWORD );
		String confirmation = Constants.getValeurChamp( request, CHAMP_CONFIRMATION );



		Utilisateur utilisateur = new Utilisateur();
		Utilisateur clientModifier = new Utilisateur();
		traiterMotsDePasse( motDePasse, confirmation, utilisateur );

		try {
			if ( erreurs.isEmpty() ) {
				utilisateur.setLogin(userDao.trouver(id).getLogin());
				userDao.modifierMotDePasse(utilisateur, id);
				clientModifier = userDao.trouver(id);
				resultat = "Succès de la modification du mot de passe.";
			}
		}catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la modification." );
			resultat ="Échec de la modification de l'utilisateur : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}


		return clientModifier;
	}

	/**
	 * Creer user.
	 *
	 * @param request the request
	 * @param id_profil the id_profil
	 * @return the utilisateur
	 */
	public Utilisateur creerUser( HttpServletRequest request, UserProfil profil) {
		String login = Constants.getValeurChamp( request, CHAMP_LOGIN );
		String motDePasse = Constants.getValeurChamp( request, CHAMP_PASSWORD );
		String confirmation = Constants.getValeurChamp( request, CHAMP_CONFIRMATION );
		String role = Constants.getValeurChamp( request, CHAMP_ROLE );

		Utilisateur utilisateur = new Utilisateur(); 
		traiterLogin( login, utilisateur );
		traiterMotsDePasse( motDePasse, confirmation, utilisateur );
		traiterRole(role, utilisateur);


		try {
			if ( erreurs.isEmpty() ) {
				utilisateur.setProfil(profil);
				userDao.creer( utilisateur, profil.getId() );
				resultat = "Succès de la création de l'utilisateur.";


			} else {
				resultat = "Échec de la création de l'utilisateur.";
			}
		} catch ( DAOException e ) {
			setErreur( "imprévu", "Erreur imprévue lors de la création." );
			resultat ="Échec de la création de l'utilisateur : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}


		return utilisateur;
	}



	/**
	 * Traiter login.
	 *
	 * @param login the login
	 * @param client the client
	 */
	private void traiterLogin( String login, Utilisateur client ) {
		try {
			validationLogin( login );
		} catch ( FormValidationException e ) {
			setErreur( CHAMP_LOGIN, e.getMessage() );
		}
		client.setLogin( login );
	}

	/**
	 * Traiter role.
	 *
	 * @param role the role
	 * @param client the client
	 */
	private void traiterRole( String role, Utilisateur client ) {
		if(role!=null) {    
			client.setRole(Role.valueOf(role ));
		} else {
			setErreur( CHAMP_ROLE, "Vous devez saisir un role" );
		}
	}


	/**
	 * Traiter mots de passe.
	 *
	 * @param motDePasse the mot de passe
	 * @param confirmation the confirmation
	 * @param client the client
	 */
	private void traiterMotsDePasse( String motDePasse, String confirmation, Utilisateur utilisateur ) {
		try {
			validationMotsDePasse(  motDePasse, confirmation );
		} catch ( FormValidationException e ) {
			setErreur( CHAMP_PASSWORD, e.getMessage() );
			setErreur( CHAMP_CONFIRMATION, null );
		}
		utilisateur.setMotDePasse( motDePasse );
	}

	/**
	 * Traiter ancien mot de passe.
	 *
	 * @param motDePasse the mot de passe
	 * @param id the id
	 */
	private void traiterAncienMotDePasse( String motDePasse, Long id ) {
		try {
			validationAncienMotDePasse(motDePasse, id);
		} catch ( FormValidationException e ) {
			setErreur( CHAMP_OLD_PASSWORD, e.getMessage() );

		}

	}




	/**
	 * Validation ancien mot de passe.
	 *
	 * @param motDePasse the mot de passe
	 * @param id the id
	 * @throws FormValidationException the form validation exception
	 */
	private void validationAncienMotDePasse(String motDePasse, Long id) throws FormValidationException {
		if(motDePasse!=null){

			boolean trouve = userDao.trouverPassword(motDePasse, id);
			if(!trouve){			
				throw new FormValidationException( "Ancien mot de passe non Valide, merci d'entrer le bon mot de passe" );
			}
		}
		else throw new FormValidationException( "Merci d'entrer votre ancien mot de passe." );

	}






	/**
	 * Validation login.
	 *
	 * @param login the login
	 * @throws FormValidationException the form validation exception
	 */
	private void validationLogin( String login ) throws FormValidationException {
		if ( login != null && !login.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
			throw new FormValidationException( "Merci de saisir une adresse mail valide." );
		}
		else {
			if ( login == null) throw new FormValidationException( "Merci d'entrer une adresse mail." );
		}
	}

	/**
	 * Validation mots de passe.
	 *
	 * @param motDePasse the mot de passe
	 * @param confirmation the confirmation
	 * @throws FormValidationException the form validation exception
	 */
	private void validationMotsDePasse( String motDePasse, String confirmation ) throws FormValidationException {
		if ( motDePasse != null && confirmation != null ) {
			if ( !motDePasse.equals( confirmation ) ) {
				throw new FormValidationException( "Les mots de passe entrés sont différents, merci de les saisir à nouveau." );
			} else if ( motDePasse.length() < 3 ) {
				throw new FormValidationException( "Les mots de passe doivent contenir au moins 3 caractères." );
			}
		} else {
			throw new FormValidationException( "Merci de saisir et confirmer votre mot de passe." );
		}
	}


}
