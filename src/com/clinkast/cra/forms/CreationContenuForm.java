package com.clinkast.cra.forms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.Contenu;
import com.clinkast.cra.dao.ContenuDaoImp;
import com.clinkast.cra.dao.DAOException;

public class CreationContenuForm extends AbstractForm {

	/** The client dao. */
	private ContenuDaoImp contenuDao;

	public CreationContenuForm(ContenuDaoImp contenuDao) {
		this.contenuDao = contenuDao;
	}

	public void creationContenu(HttpServletRequest request) {

		String titre = Constants.removeSpecChar(Constants.getValeurChamp(
				request, Constants.ATT_TITRE_CONTENU));

		String att_contenu = Constants.removeSpecChar(Constants.getValeurChamp(
				request, Constants.ATT_CONTENU));

		Contenu contenu = new Contenu(titre, att_contenu);
		try {
			contenuDao.creer(contenu);
		} catch (DAOException e) {
			setErreur("imprévu", "Erreur imprévue lors de la création.");
			resultat = "Echec de la création du contenu : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}
	}
	
	public List<Contenu> listerContenu(int debut, int nombreContenu) {
		
		try {
			return contenuDao.lister(debut, nombreContenu);
		} catch (DAOException e) {
			setErreur("imprévu", "Erreur imprévue lors de la création.");
			resultat = "Echec de la création du contenu : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}
		return null;
	}

	public void modificationContenu(HttpServletRequest request, long id) {

		String titre = Constants.removeSpecChar(Constants.getValeurChamp(
				request, Constants.ATT_TITRE_CONTENU));

		String att_contenu = Constants.removeSpecChar(Constants.getValeurChamp(
				request, Constants.ATT_CONTENU));

		Contenu contenu = new Contenu(titre, att_contenu);
		try {
			contenuDao.modifier(contenu, id);
		} catch (DAOException e) {
			setErreur("imprévu", "Erreur imprévue lors de la création.");
			resultat = "Echec de la création du contenu : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
			e.printStackTrace();
		}
	
	}

	

}
