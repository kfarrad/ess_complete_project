package com.clinkast.cra.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;

import com.clinkast.cra.beans.Constants;
import com.clinkast.cra.beans.FormationsSuivies;
import com.clinkast.cra.beans.GestionEvaluations;
import com.clinkast.cra.beans.Utilisateur;
import com.clinkast.cra.dao.AutoEvaluationDAOImpl;
import com.clinkast.cra.dao.DAOException;
import com.clinkast.cra.dao.FormationsSuiviesDAOImpl;
import com.clinkast.cra.dao.GestionEvaluationsDAOImpl;

public class FormationSuiviesForm extends AbstractForm{
	/*	Constantes */
	//public static final String PARAM_ID = "id";
	public static final String CHAMP_ID = "formationSuivies[id_auto]";
	public static final String CHAMP_FMS_INTITULE = "formationSuivies[intitule]";
	public static final String CHAMP_FMS_COMMENTAIRE = "formationSuivies[commanetaire]";
	public static final String CHAMP_FMS_ID = "formationSuivies[id_formationSuivie]";
	public static final String CHAMP_TAB_FMS_ID = "formation_id_formations";

	private AutoEvaluationDAOImpl autoEvaluationDAOImpl;
	private FormationsSuiviesDAOImpl formationsSuiviesDAOImpl;
	private GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl;
	
	public FormationSuiviesForm(GestionEvaluationsDAOImpl gestionEvaluationsDAOImpl, AutoEvaluationDAOImpl autoEvaluationDAOImpl, FormationsSuiviesDAOImpl formationsSuiviesDAOImpl){
		super();
		this.gestionEvaluationsDAOImpl = gestionEvaluationsDAOImpl;
		this.autoEvaluationDAOImpl = autoEvaluationDAOImpl;
		this.formationsSuiviesDAOImpl = formationsSuiviesDAOImpl;
	}
	
	public GestionEvaluations sauveFormationSuivies(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String fms_intitule = Constants.getValeurChamp(request, CHAMP_FMS_INTITULE);
		String fms_commentaire = Constants.getValeurChamp(request, CHAMP_FMS_COMMENTAIRE);
		String fms_id = Constants.getValeurChamp(request, CHAMP_FMS_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;
		
		FormationsSuivies formationsSuivies = new FormationsSuivies();
		
		//Convertir les id en Long
		Long evaluationId = Long.parseLong(autoEvaluationId);
		Long formationSuivies_id = Long.parseLong(fms_id);
		formationsSuivies.setAutoEvaluations(autoEvaluationDAOImpl.trouver(evaluationId));
		formationsSuivies.setFormationsSuiviesIntitule(fms_intitule);
		formationsSuivies.setFormationsSuiviesCommentaires(fms_commentaire);
				
		try{
			//Si l'id n'existe pas alors on cree l'element
			if(formationSuivies_id == 0){
				//nouvelle activité
				formationsSuiviesDAOImpl.creer(formationsSuivies);
				resultat = "La formation suivie a été créé";
			}
			else if(formationSuivies_id != null){
				//on ajoute id
				formationsSuivies.setFormationsSuiviesId(formationSuivies_id);
				//on modifie les attributs de l'activité
				formationsSuiviesDAOImpl.modifier(formationsSuivies);
				resultat = "La formation suivie a été modifié";
			}
			gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
			
			
		}
		catch (DAOException e) {
			// TODO: handle exception
			setErreur("imprevu dans formationsSuiviesDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}		
		return gestionEvaluations;
	}
	
	/*Supprimer une activité*/
	public GestionEvaluations supprimeFormationSuivies(HttpServletRequest request){
		String autoEvaluationId = Constants.getValeurChamp(request, CHAMP_ID);
		String[] deleteId = (String[])request.getParameterValues( CHAMP_TAB_FMS_ID);
		HttpSession session = request.getSession();
        GestionEvaluations gestionEvaluations = null;		
		Long evaluationId = Long.parseLong(autoEvaluationId);
		int compteur = 0;
		
		if(deleteId != null)
		{
			try{
				for(String id : deleteId){
					Long fms_id = Long.parseLong(id);
					FormationsSuivies formationsSuivies = formationsSuiviesDAOImpl.trouver(fms_id);
					formationsSuiviesDAOImpl.supprimer(formationsSuivies);
					compteur ++;
				}
				gestionEvaluations = updateGestionAutoEvaluation(session, evaluationId);
				
				resultat = compteur + (compteur >= 2 ? " formations suivies ont " : " formation suivie a ") + "été supprimé";
			}
			catch (DAOException e) {
				// TODO: handle exception
				setErreur("imprevu dans FormationsSuiviesDAOImpl: ", e.getMessage());
				resultat = e.getMessage();
			}
		}
		return gestionEvaluations;
	}
	
	
	private GestionEvaluations updateGestionAutoEvaluation(HttpSession session, Long evaluationId){

		GestionEvaluations gestionEvaluations = null;
		Utilisateur utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		utilisateur = (Utilisateur)(session.getAttribute(Constants.ATT_SESSION_USER));
		
		//Mis à jour de gestionAutoEvaluation
		try{		
			gestionEvaluations = gestionEvaluationsDAOImpl.trouver_By_KEY(evaluationId, utilisateur.getId());
			gestionEvaluations.setGestionEvaluationsDateModification(new DateTime());
			gestionEvaluations.setGestionEvaluationsNbreModification(gestionEvaluations.getGestionEvaluationsNbreModification()+1);
			gestionEvaluationsDAOImpl.modifier(gestionEvaluations);
		}
		catch (Exception e) {
			// TODO: handle exception
			setErreur("imprevu dans GestionEvaluationDAOImpl: ", e.getMessage());
			resultat = e.getMessage();
		}
		return gestionEvaluations;
	}
	
}
