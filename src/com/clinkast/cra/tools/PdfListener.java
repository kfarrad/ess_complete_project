package com.clinkast.cra.tools;

import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.PDFCreationListener;

import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfWriter;

public class PdfListener implements PDFCreationListener {
        PdfDocument pdfDoc = null;
        @Override
        public void onClose(ITextRenderer itextrenderer) {
                pdfDoc.close();
                
        }

        @Override
        public void preOpen(ITextRenderer itextrenderer) {
                PdfWriter pdfWriter = itextrenderer.getWriter();
                pdfWriter.open();
                pdfDoc = pdfWriter.getDirectContent().getPdfDocument();
                
                //Header
                Phrase phraseheader = new Phrase();   
                phraseheader.add(new Chunk("Salut World", FontFactory.getFont(FontFactory.HELVETICA, 8)));
                HeaderFooter header = new HeaderFooter(phraseheader,false);
                header.setBorder(Rectangle.TOP);
                header.setAlignment(Element.ALIGN_LEFT);
                pdfDoc.setHeader(header);
                //Footer 
                Phrase phrase = new Phrase();   
                phrase.add(new Chunk("CLINKAST\n32 rue de la République 92190 MEUDON\n"
                                + "Votre contact : Stéphane KAMGAING\n"
                                + "+33 1 46 31 44 25", FontFactory.getFont(FontFactory.HELVETICA, 12)));
                HeaderFooter footer = new HeaderFooter(phrase,false);
                footer.setBorder(Rectangle.NO_BORDER);
                              footer.setAlignment(Element.ALIGN_LEFT);
                pdfDoc.setFooter(footer);  
               
                
        }
        
}
